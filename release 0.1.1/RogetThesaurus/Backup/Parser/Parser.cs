using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Schema;


//this namespace is needed because the YACC generator currently still spits it out.
namespace Babel.ParserGenerator { }

namespace JastDev.RogetThesaurus.Parser
{
   public enum ValueType
   {
      None, Verb, Noun, Adjective, Adverb
   }

   public class ThesaurusList<T> : List<T>, IXmlSerializable
   {
      #region IXmlSerializable Members

      public XmlSchema GetSchema()
      {
         return null;
      }



      public void ReadXml(XmlReader reader)
      {
         XmlSerializer valueSerializer = new XmlSerializer(typeof(T));
         bool wasEmpty = reader.IsEmptyElement;

         reader.Read();
         if (wasEmpty) return;
         while (reader.NodeType != XmlNodeType.EndElement)
         {
            T value = (T)valueSerializer.Deserialize(reader);
            Add(value);
            reader.ReadEndElement();
            reader.MoveToContent();
         }
         reader.ReadEndElement();

      }

      public void WriteXml(XmlWriter writer)
      {
         XmlSerializer valueSerializer = new XmlSerializer(typeof(T));

         foreach (T item in this)
         {
            valueSerializer.Serialize(writer, item);
         }
      }
      #endregion
   }

   #region value classes

   public interface IThesaurusNode
   {
      string Title
      {
         get;
         set;
      }
      ulong Index
      {
         get;
         set;
      }
   }

   public class BaseValueMeaningItem
   {
      string fExtraInfo;
      public string ExtraInfo
      {
         get { return fExtraInfo; }
         set { fExtraInfo = value; }
      }
   }

   [XmlRootAttribute("ValueMeaningItem", Namespace = "http://www.jastdev.com", IsNullable = false)]
   public class ValueMeaningItem : BaseValueMeaningItem, IThesaurusNode
   {
      string fText;
      ulong fIndex = ulong.MaxValue;
      ValueMeaningItemRef fContext = null;

      //[XmlText]
      public string Title
      {
         get { return fText; }
         set { fText = value; }
      }
      public ValueMeaningItemRef Context
      {
         get { return fContext; }
         set { fContext = value; }
      }

      public ulong Index
      {
         get { return fIndex; }
         set { fIndex = value; }
      }

      public override string ToString()
      {
         if (Context != null)
         {
            return Title + "(" + Context.ToString() + ")";
         }
         else
         {
            return Title;
         }
      }
   }

   [XmlRootAttribute("ValueMeaningItemRef", Namespace = "http://www.jastdev.com", IsNullable = false)]
   public class ValueMeaningItemRef : BaseValueMeaningItem
   {
      ValueType fType = ValueType.None;
      string fSentence;
      string fNumber;

      public string Number
      {
         get { return fNumber; }
         set { fNumber = value; }
      }

      public string Sentence
      {
         get { return fSentence; }
         set { fSentence = value; }
      }

      public ValueType Type
      {
         get { return fType; }
         set { fType = value; }
      }

      public override string ToString()
      {
         if (Number != null && Number.Length > 0)
         {
            return Number;
         }
         else
         {
            return Type.ToString();
         }
      }
   }

   [XmlRootAttribute("ValueMeaningSubGroup", Namespace = "http://www.jastdev.com", IsNullable = false)]
   public class ValueMeaningSubGroup
   {
      [XmlIgnore]
      public List<BaseValueMeaningItem> fItems = new List<BaseValueMeaningItem>();

      [XmlElement(Type = typeof(ValueMeaningItemRef), ElementName = "Ref")]
      [XmlElement(Type = typeof(ValueMeaningItem), ElementName = "Item")]
      public List<BaseValueMeaningItem> Items
      {
         get { return fItems; }
      }

      [XmlIgnore]
      public string Title
      {
         get
         {
            string iRes = null;
            if (Items.Count > 0)
            {
               return Items[0].ToString();
            }
            return iRes;
         }
      }
   }

   [XmlRootAttribute("ValueMeaningGroup", Namespace = "http://www.jastdev.com", IsNullable = false)]
   public class ValueMeaningGroup
   {
      public ValueMeaningGroup()
      {
         SubGroups.Add(new ValueMeaningSubGroup());
      }
      [XmlIgnore]
      public List<ValueMeaningSubGroup> fSubGroups = new List<ValueMeaningSubGroup>();

      public List<ValueMeaningSubGroup> SubGroups
      {
         get { return fSubGroups; }
      }

      [XmlIgnore]
      public string Title
      {
         get
         {
            string iRes = null;
            if (SubGroups.Count > 0)
            {
               return SubGroups[0].Title;
            }
            return iRes;
         }
      }

      internal void Clean()
      {
         for (int i = SubGroups.Count - 1; i >= 0; i--)
         {
            if (SubGroups[i].Items.Count == 0)
            {
               SubGroups.RemoveAt(i);
            }
         }
      }
   }

   [XmlRootAttribute("ValueMeaning", Namespace = "http://www.jastdev.com", IsNullable = false)]
   public class ValueMeaning
   {
      public ValueMeaning()
      {
         Groups.Add(new ValueMeaningGroup());
      }
      ThesaurusList<ValueMeaningGroup> fGroups = new ThesaurusList<ValueMeaningGroup>();
      ValueType fType;

      public ValueType Type
      {
         get { return fType; }
         set { fType = value; }
      }
      public ThesaurusList<ValueMeaningGroup> Groups
      {
         get { return fGroups; }
         set { fGroups = value; }
      }

      internal void Clean()
      {
         for (int i = Groups.Count - 1; i >= 0; i--)
         {
            Groups[i].Clean();
            if (Groups[i].SubGroups.Count == 0)
            {
               Groups.RemoveAt(i);
            }
         }
      }
   }

   [XmlRootAttribute("Value", Namespace = "http://www.jastdev.com", IsNullable = false)]
   public class Value : IThesaurusNode
   {
      public Value()
      {
         Meanings.Add(new ValueMeaning());
      }


      /// <summary>
      /// removes the last item if it is empty (artifact of the add procedure: there is always 1 in reserve added).
      /// </summary>
      public void Clean()
      {
         for (int i = Meanings.Count - 1; i >= 0; i--)
         {
            Meanings[i].Clean();
            if (Meanings[i].Groups.Count == 0)
            {
               Meanings.RemoveAt(i);
            }
         }
      }
      string fNumber;
      string fTitle;
      string fTitleMeaning;
      ulong fIndex = ulong.MaxValue;
      ThesaurusList<ValueMeaning> fMeanings = new ThesaurusList<ValueMeaning>();
      ThesaurusList<string> fPhrases = new ThesaurusList<string>();

      public string Number
      {
         get { return fNumber; }
         set { fNumber = value; }
      }
      public string Title
      {
         get { return fTitle; }
         set { fTitle = value; }
      }
      public string TitleMeaning
      {
         get { return fTitleMeaning; }
         set { fTitleMeaning = value; }
      }


      //[XmlElement(Type = typeof(ValueMeaning), ElementName = "ValueMeaning")]
      public ThesaurusList<ValueMeaning> Meanings
      {
         get { return fMeanings; }
         set { fMeanings = value; }
      }

      //[XmlElement(Type = typeof(string), ElementName = "string")]
      public ThesaurusList<string> Phrases
      {
         get { return fPhrases; }
         set { fPhrases = value; }
      }

      public ulong Index
      {
         get { return fIndex; }
         set { fIndex = value; }
      }
   }

   [XmlRootAttribute("ResultNode", Namespace = "http://www.jastdev.com", IsNullable = false)]
   public class ResultNode
   {
      [XmlIgnore]
      public LexLocation Location;
      [XmlIgnore]
      public ResultNode Parent = null;
      List<ValuesNode> fItems = new List<ValuesNode>();

      [XmlElement(Type = typeof(ValuesNode), ElementName = "ValuesNode")]
      [XmlElement(Type = typeof(ClassNode), ElementName = "ClassNode")]
      [XmlElement(Type = typeof(SectionNode), ElementName = "SectionNode")]
      [XmlElement(Type = typeof(TitleNode), ElementName = "TitleNode")]
      [XmlElement(Type = typeof(SubTitleNode), ElementName = "SubTitleNode")]
      public List<ValuesNode> Items
      {
         get { return fItems; }
      }

      public virtual void Clean()
      {
         foreach (ResultNode i in Items)
         {
            i.Clean();
         }
      }
   }

   public class ValuesNode : ResultNode, IThesaurusNode
   {
      ThesaurusList<Value> fValues = new ThesaurusList<Value>();
      ulong fIndex = ulong.MaxValue;
      //[XmlElement(Type = typeof(Value), ElementName = "Value")]


      public ThesaurusList<Value> Values
      {
         get { return fValues; }
         set { fValues = value; }
      }

      public override void Clean()
      {
         base.Clean();
         foreach (Value i in Values)
         {
            i.Clean();
         }
      }

      /// <summary>
      /// gets the title of the first Value (if there is any)
      /// </summary>
      [XmlIgnore]
      public virtual string Title
      {
         get
         {
            string iRes =null;
            if (Values.Count > 0)
            {
               return Values[0].Title;
            }
            return iRes;
         }
         set
         {
            throw new NotImplementedException();
         }
      }

      public ulong Index
      {
         get { return fIndex; }
         set { fIndex = value; }
      }
   }

   public class BaseBlockNode : ValuesNode
   {
      string fNumber;
      string fTitle;
      
      public string Number
      {
         get { return fNumber; }
         set { fNumber = value; }
      }
      [XmlText]
      public override string Title
      {
         get { return fTitle; }
         set { fTitle = value; }
      }
   }

   public class ClassNode : BaseBlockNode
   {
   }

   public class SectionNode : BaseBlockNode
   {
   }

   public class TitleNode : BaseBlockNode
   {
   }

   public class SubTitleNode : BaseBlockNode
   {
   }

   #endregion

   partial class Parser
   {
      #region fields

      ErrorHandler fHandler = null;
      TextBox fSource = new TextBox();

      int fCurLevel = 0;
      bool fTreatSectionAsTitle = false;
      ResultNode fRes = null;

      ResultNode fCurPos = null;
      Value fCurValue = null;

      #endregion

      #region Error Handling

      public void SetHandler(ErrorHandler aHandler) { fHandler = aHandler; }


      public void ReportError(string aText, LexLocation aLoc, Severity aSeverity)
      {
         if (fHandler != null)
         {
            fHandler.AddError(aText, aLoc, (int)aSeverity);
         }
      }

      public void ReportError(string aText, LexLocation aLoc)
      {
         ReportError(aText, aLoc, Severity.Error);
      }

      public void ReportFatal(string aText, LexLocation aLoc)
      {
         ReportError(aText, aLoc, Severity.Fatal);
      }

      public void ReportWarning(string aText, LexLocation aLoc)
      {
         ReportError(aText, aLoc, Severity.Warning);
      }

      public void ReportHint(string aText, LexLocation aLoc)
      {
         ReportError(aText, aLoc, Severity.Hint);
      }


      #endregion


      public ResultNode Result
      {
         get
         {
            return fRes;
         }
      }

      public TextBox Source
      {
         get
         {
            return fSource;
         }
         set
         {
            fSource = value;
         }
      }

      protected override void InitParse()
      {
         fRes = new ResultNode();
         fCurPos = fRes;
         fCurLevel = 0;
      }

      #region AddData


      private void PushLevel(ValuesNode iNew)
      {
         fCurValue = null;                                                                //the level is changed, so there can't be a possibl current value anymore, cause the new level hasn't been added a new value
         if (fCurPos != null)
         {
            fCurPos.Items.Add(iNew);
            iNew.Parent = fCurPos;
            fCurPos = iNew;
            fCurLevel++;
         }
      }

      /// <summary>
      /// go up levels to the speceified insertion point in the result tree.
      /// </summary>
      /// <param name="aVal"></param>
      private void PopLevel(int aVal, LexLocation aLoc)
      {
         fCurValue = null;                                                                //the level is changed, so there can't be a possibl current value anymore, cause the new level hasn't been added a new value
         if (aVal > fCurLevel)
         {
            ReportError("curent level is higher than the level where to pop too", aLoc);
            return;
         }
         while (fCurLevel > aVal && fCurPos.Parent != null)
         {
            fCurPos = fCurPos.Parent;
            fCurLevel--;
         }
      }

      void AddClass(LexLocation aLoc, LexLocation aTitle, LexLocation aNumber)
      {
         PopLevel(0, aLoc);
         ClassNode iNew = new ClassNode();
         iNew.Location = aLoc;
         iNew.Title = aTitle.GetValue(fSource) ;
         iNew.Number = aNumber.GetValue(fSource);
         PushLevel(iNew);
         fTreatSectionAsTitle = false;                                                    //whenever a new class starts, we reset this so that sections are added as sections by default
      }

      void AddDivision(LexLocation aLoc, LexLocation aTitle, LexLocation aNumber)
      {
         PopLevel(1, aLoc);
         SectionNode iNew = new SectionNode();
         iNew.Location = aLoc;
         iNew.Title = aTitle.GetValue(fSource);
         iNew.Number = aNumber.GetValue(fSource);
         PushLevel(iNew);
         fTreatSectionAsTitle = true;                                                     //when a division is found, add sections as titles, this is to keep the structurure consistent.
      }

      void AddSection(LexLocation aLoc, LexLocation aTitle, LexLocation aNumber)
      {
         if (fTreatSectionAsTitle == true)
         {
            AddTitle(aLoc, aTitle, aNumber);
         }
         else
         {
            PopLevel(1, aLoc);
            SectionNode iNew = new SectionNode();
            iNew.Location = aLoc;
            iNew.Title = aTitle.GetValue(fSource);
            iNew.Number = aNumber.GetValue(fSource);
            PushLevel(iNew);
         }
      }

      void AddTitle(LexLocation aLoc, LexLocation aTitle, LexLocation aNumber)
      {
         PopLevel(2, aLoc);
         TitleNode iNew = new TitleNode();
         iNew.Location = aLoc;
         iNew.Title = aTitle.GetValue(fSource);
         iNew.Number = aNumber.GetValue(fSource);
         PushLevel(iNew);
      }

      void AddSubTitle(LexLocation aLoc, LexLocation aTitle)
      {
         PopLevel(3, aLoc);
         SubTitleNode iNew = new SubTitleNode();
         iNew.Location = aLoc;
         iNew.Title = aTitle.GetValue(fSource);
         PushLevel(iNew);
      }

      void AddSubTitle(LexLocation aLoc, LexLocation aTitleTitle, LexLocation aTitleNumber, LexLocation aSubTitle)
      {
         string iTitleTitle = aTitleTitle.GetValue(fSource);
         if (fCurPos is SubTitleNode)
         {
            PopLevel(3, aLoc);
         }
         if (fCurPos is BaseBlockNode &&  (fCurPos as BaseBlockNode).Title != iTitleTitle || !(fCurPos is TitleNode)) 
         {
            AddTitle(aLoc, aTitleTitle, aTitleNumber);
         }
         PopLevel(3, aLoc);
         SubTitleNode iNew = new SubTitleNode();
         iNew.Location = aLoc;
         iNew.Title = aSubTitle.GetValue(fSource);
         PushLevel(iNew);
      }

      void AddBlockItem()
      {
         fCurValue = new Value();
         if (fCurPos is ValuesNode)
         {
            (fCurPos as ValuesNode).Values.Add(fCurValue);
         }
         else
         {
            ReportError("Can't create Value node: no current values container node active, BlockLevel: " + fCurLevel.ToString(), scanner.yylloc);
         }
      }
      
      void SetBlockItemTitle(LexLocation aTitle)
      {
         if (fCurValue != null)
         {
            fCurValue.Title = aTitle.GetValue(fSource);
         }
         else
         {
            ReportError("Can't set title: no current value node active, BlockLevel: " + fCurLevel.ToString(), aTitle);
         }
      }

      void SetBlockItemNumber(LexLocation aNumber)
      {
         if (fCurValue != null)
         {
            fCurValue.Number = aNumber.GetValue(fSource);
         }
         else
         {
            ReportError("Can't set Number: no current value node active, BlockLevel: " + fCurLevel.ToString(), aNumber);
         }
      }

      void SetTitleAndMeaning(LexLocation aTitle, LexLocation aMeaning)
      {
         if (fCurValue != null)
         {
            fCurValue.Title = aTitle.GetValue(fSource);
            fCurValue.TitleMeaning = aMeaning.GetValue(fSource);
         }
         else
         {
            ReportError("Can't set TitleMeaning: no current value node active, BlockLevel: " + fCurLevel.ToString(), aMeaning);
         }
      }

      void SetValueMeaningType(ValueType aType)
      {
         if (fCurValue != null)
         {
            if (fCurValue.Meanings.Count > 0)
            {
               fCurValue.Meanings[fCurValue.Meanings.Count - 1].Type = aType;
            }
            else
            {
               ReportError("Can't set ValueType: no current valueMeanings node active, BlockLevel: " + fCurLevel.ToString(), scanner.yylloc);
            }
         }
         else
         {
            ReportError("Can't set ValueType: no current value node active, BlockLevel: " + fCurLevel.ToString(), scanner.yylloc);
         }
      }

      void AddPhrase(LexLocation aLoc)
      {
         if (fCurValue != null)
         {
            fCurValue.Phrases.Add(aLoc.GetValue(fSource));
         }
         else
         {
            ReportError("Can't add phrase: no current value node active, BlockLevel: " + fCurLevel.ToString(), aLoc);
         }
      }

      void AddValueMeaning()
      {
         if (fCurValue != null)
         {
            fCurValue.Meanings.Add(new ValueMeaning());
         }
         else
         {
            ReportError("Can't add ValueMeaning: no current value node active, BlockLevel: " + fCurLevel.ToString(), scanner.yylloc);
         }
      }

      void AddValueMeaningGroup()
      {
         if (fCurValue != null)
         {
            if (fCurValue.Meanings.Count > 0)
            {
               fCurValue.Meanings[fCurValue.Meanings.Count - 1].Groups.Add(new ValueMeaningGroup());
            }
            else
            {
               ReportError("Can't add ValueMeaningGroup: no current valueMeanings node active, BlockLevel: " + fCurLevel.ToString(), scanner.yylloc);
            }
         }
         else
         {
            ReportError("Can't add ValueMeaningGroup: no current value node active, BlockLevel: " + fCurLevel.ToString(), scanner.yylloc);
         }
      }

      void AddValueMeaningSubGroup()
      {
         if (fCurValue != null)
         {
            if (fCurValue.Meanings.Count > 0)
            {
               ValueMeaning iMeaning = fCurValue.Meanings[fCurValue.Meanings.Count - 1];
               if (iMeaning.Groups.Count > 0)
               {
                  iMeaning.Groups[iMeaning.Groups.Count - 1].SubGroups.Add(new ValueMeaningSubGroup());
               }
               else
               {
                  ReportError("Can't add ValueMeaningSubGroup: no current valueMeaningsGroup node active, BlockLevel: " + fCurLevel.ToString(), scanner.yylloc);
               }
            }
            else
            {
               ReportError("Can't add ValueMeaningSubGroup: no current valueMeanings node active, BlockLevel: " + fCurLevel.ToString(), scanner.yylloc);
            }
         }
         else
         {
            ReportError("Can't add ValueMeaningSubGroup: no current value node active, BlockLevel: " + fCurLevel.ToString(), scanner.yylloc);
         }
      }

      void AddValueItem(LexLocation aLoc)
      {
         if (fCurValue != null)
         {
            if (fCurValue.Meanings.Count > 0)
            {
               ValueMeaning iMeaning = fCurValue.Meanings[fCurValue.Meanings.Count - 1];
               if (iMeaning.Groups.Count > 0)
               {
                  ValueMeaningGroup iGroup = iMeaning.Groups[iMeaning.Groups.Count - 1];
                  if (iGroup.SubGroups.Count > 0)
                  {
                     ValueMeaningItem iNew = new ValueMeaningItem();
                     iNew.Title = aLoc.GetValue(fSource);
                     iGroup.SubGroups[iGroup.SubGroups.Count -1].Items.Add(iNew);
                  }
                  else
                  {
                     ReportError("Can't add ValueItem: no current valueMeaningsSubGroup node active, BlockLevel: " + fCurLevel.ToString(), aLoc);
                  }
               }
               else
               {
                  ReportError("Can't add ValueItem: no current valueMeaningsGroup node active, BlockLevel: " + fCurLevel.ToString(), aLoc);
               }
            }
            else
            {
               ReportError("Can't add ValueItem: no current valueMeanings node active, BlockLevel: " + fCurLevel.ToString(), aLoc);
            }
         }
         else
         {
            ReportError("Can't add ValueItem: no current value node active, BlockLevel: " + fCurLevel.ToString(), aLoc);
         }
      }

      void AddValueItemRef(LexLocation aNumber)
      {
         AddValueItemRef(aNumber.GetValue(fSource), null, ValueType.None);
      }

      void AddValueItemRef(LexLocation aNumber, ValueType aType)
      {
         AddValueItemRef(aNumber.GetValue(fSource), null, aType);
      }

      void AddValueItemRef(ValueType aType)
      {
         AddValueItemRef(null, null, aType);
      }
   
      void AddValueItemRef(LexLocation aNumber, LexLocation aSentence)
      {
         if (aNumber != null)
         {
            AddValueItemRef(aNumber.GetValue(fSource), aSentence.GetValue(fSource), ValueType.None);
         }
         else
         {
            AddValueItemRef("", aSentence.GetValue(fSource), ValueType.None);
         }
      }

      void AddValueItemRef(string aNumber, string aSentence, ValueType aType)
      {
         if (fCurValue != null)
         {
            if (fCurValue.Meanings.Count > 0)
            {
               ValueMeaning iMeaning = fCurValue.Meanings[fCurValue.Meanings.Count - 1];
               if (iMeaning.Groups.Count > 0)
               {
                  ValueMeaningGroup iGroup = iMeaning.Groups[iMeaning.Groups.Count - 1];
                  if (iGroup.SubGroups.Count > 0)
                  {
                     ValueMeaningItemRef iNew = new ValueMeaningItemRef();
                     iNew.Number = aNumber;
                     iNew.Sentence = aSentence;
                     iNew.Type = aType;
                     iGroup.SubGroups[iGroup.SubGroups.Count - 1].Items.Add(iNew);
                  }
                  else
                  {
                     ReportError("Can't add ValueItemRef: no current valueMeaningsSubGroup node active, BlockLevel: " + fCurLevel.ToString(), scanner.yylloc);
                  }
               }
               else
               {
                  ReportError("Can't add ValueItemRef: no current valueMeaningsGroup node active, BlockLevel: " + fCurLevel.ToString(), scanner.yylloc);
               }
            }
            else
            {
               ReportError("Can't add ValueItemRef: no current valueMeanings node active, BlockLevel: " + fCurLevel.ToString(), scanner.yylloc);
            }
         }
         else
         {
            ReportError("Can't add ValueItemRef: no current value node active, BlockLevel: " + fCurLevel.ToString(), scanner.yylloc);
         }
      }

      void ChangeLastRefToItem(LexLocation aLoc)
      {
         if (fCurValue != null)
         {
            if (fCurValue.Meanings.Count > 0)
            {
               ValueMeaning iMeaning = fCurValue.Meanings[fCurValue.Meanings.Count - 1];
               if (iMeaning.Groups.Count > 0)
               {
                  ValueMeaningGroup iGroup = iMeaning.Groups[iMeaning.Groups.Count - 1];
                  if (iGroup.SubGroups.Count > 0)
                  {
                     ValueMeaningSubGroup iSub = iGroup.SubGroups[iGroup.SubGroups.Count - 1];
                     ValueMeaningItemRef iLast = iSub.Items[iSub.Items.Count - 1] as ValueMeaningItemRef;
                     ValueMeaningItem iNew = new ValueMeaningItem();
                     iNew.Title = aLoc.GetValue(fSource);
                     iNew.Context = iLast;
                     iSub.Items[iSub.Items.Count - 1] = iNew;
                  }
                  else
                  {
                     ReportError("Can't add ValueItem: no current valueMeaningsSubGroup node active, BlockLevel: " + fCurLevel.ToString(), aLoc);
                  }
               }
               else
               {
                  ReportError("Can't add ValueItem: no current valueMeaningsGroup node active, BlockLevel: " + fCurLevel.ToString(), aLoc);
               }
            }
            else
            {
               ReportError("Can't add ValueItem: no current valueMeanings node active, BlockLevel: " + fCurLevel.ToString(), aLoc);
            }
         }
         else
         {
            ReportError("Can't add ValueItem: no current value node active, BlockLevel: " + fCurLevel.ToString(), aLoc);
         }
      }

      void SetExtraInforForLastItem(LexLocation aLoc)
      {

         if (fCurValue != null)
         {
            if (fCurValue.Meanings.Count > 0)
            {
               ValueMeaning iMeaning = fCurValue.Meanings[fCurValue.Meanings.Count - 1];
               if (iMeaning.Groups.Count > 0)
               {
                  ValueMeaningGroup iGroup = iMeaning.Groups[iMeaning.Groups.Count - 1];
                  if (iGroup.SubGroups.Count > 0)
                  {
                     ValueMeaningSubGroup iSub = iGroup.SubGroups[iGroup.SubGroups.Count - 1];
                     if (iSub.Items.Count > 0)
                     {
                        iSub.Items[iSub.Items.Count - 1].ExtraInfo = aLoc.GetValue(fSource);
                     }
                     else
                     {
                        ReportError("Can't set extra info: no current value item node, BlockLevel: " + fCurLevel.ToString(), aLoc);
                     }
                  }
                  else
                  {
                     ReportError("Can't set extra info: no current valueMeaningsSubGroup node active, BlockLevel: " + fCurLevel.ToString(), aLoc);
                  }
               }
               else
               {
                  ReportError("Can't set extra info: no current valueMeaningsGroup node active, BlockLevel: " + fCurLevel.ToString(), aLoc);
               }
            }
            else
            {
               ReportError("Can't set extra info: no current valueMeanings node active, BlockLevel: " + fCurLevel.ToString(), aLoc);
            }
         }
         else
         {
            ReportError("Can't set extra info: no current value node active, BlockLevel: " + fCurLevel.ToString(), aLoc);
         }
      }

      #endregion

   }
}
