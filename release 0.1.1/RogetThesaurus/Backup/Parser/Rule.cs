using System;
using System.Collections.Generic;
using System.Text;

namespace JastDev.RogetThesaurus.Parser
{
   public class Rule
   {
      public int lhs; // symbol
      public int[] rhs; // symbols

      public Rule(int lhs, int[] rhs)
      {
         this.lhs = lhs;
         this.rhs = rhs;
      }
   }
}
