using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using NeuralNetworkDesigne.RogetThesaurus.Parser;
using NeuralNetworkDesigne.RogetThesaurus.Lexer;
using Microsoft.Win32;
using System.IO;
using System.Xml.Serialization;


namespace RogetThesaurus
{
   /// <summary>
   /// Interaction logic for Window1.xaml
   /// </summary>

   public partial class Window1 : Window
   {
      private ErrorHandler fErrorHandler = new ErrorHandler();
      private Parser fParser = new Parser();
      private Scanner fScanner = new Scanner();
      private StringWriter fTraceLog = new StringWriter();
      private string fText;

      public Window1()
      {
         InitializeComponent();

      }

      void ClickCompile(object aSender, EventArgs e)
      {
         fErrorHandler = new ErrorHandler();
         fParser = new Parser();
         fScanner = new Scanner();
         fTraceLog = new StringWriter();

         fParser.SetHandler(fErrorHandler);
         //fParser.Trace = true;
         fParser.scanner = fScanner;
         fScanner.Handler = fErrorHandler;
         Console.SetError(fTraceLog);

         fScanner.SetSource(TxtCode.Text, 0);
         fParser.Source = TxtCode;                     //he needs this to get the text values from the source.
         try
         {
            fParser.Parse();
            fParser.Result.Clean();
         }
         catch(Exception ex)
         {
            fErrorHandler.AddError("fatal error: " + ex.Message, new LexLocation(0, 0, 0, 0), 0);
         }
         TxtParseSteps.Text = fTraceLog.ToString();
         TreeResult.ItemsSource = fParser.Result.Items;
         LstErrors.ItemsSource = fErrorHandler.SortedErrorList();
         TabErrors.IsSelected = true;
      }

      void ClickOpen(object aSender, EventArgs e)
      {
         OpenFileDialog iDlg = new OpenFileDialog();

         if (iDlg.ShowDialog() == true)
         {
            fText = iDlg.FileName;
            LoadFile();
         }
      }

      private void LoadFile()
      {
         StreamReader iReader = File.OpenText(fText);
         TxtCode.Text = iReader.ReadToEnd();
         iReader.Close();
      }

      void ClicSave(object aSender, EventArgs e)
      {
         SaveFileDialog iDlg = new SaveFileDialog();
         

         iDlg.CheckFileExists = false;
         if (iDlg.ShowDialog() == true)
         {
            StreamWriter iWriter = File.CreateText(iDlg.FileName);
            iWriter.Write(TxtCode.Text);
            iWriter.Close();
         }
      }

      void ClickReload(object aSender, EventArgs e)
      {
         TabText.IsSelected = true;
         LoadFile();
         ClickCompile(aSender, e);
      }

      void ClickStreamtoXml(object aSender, EventArgs e)
      {
         SaveFileDialog iDlg = new SaveFileDialog();
         
         iDlg.CheckFileExists = false;
         if (iDlg.ShowDialog() == true)
         {
            XmlSerializer iSer = new XmlSerializer(typeof(ResultNode));
            TextWriter iWriter = new StreamWriter(iDlg.FileName);
            iSer.Serialize(iWriter, fParser.Result);
            iWriter.Close();
         }
      }
   }
}