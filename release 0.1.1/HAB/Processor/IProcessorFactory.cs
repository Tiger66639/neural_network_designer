﻿namespace NeuralNetworkDesigne.HAB
{
    public interface IProcessorFactory
    {
        Processor GetProcessor();
    }
}