﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Contains all the data required for a single list of code for execution.
    /// </summary>
    /// <remarks>
    /// This class is used in a stack to allow for conditional statements. When we do a processor split, we need
    /// to be able to continue at the correct call location (inside the correct conditional, in such a way that we
    /// can go back again to code at a higher level).
    /// <para>
    /// Doesn't store variable dictionaries, cause they have a different scope. We do store when a dictionary was added
    /// to a frame, this is to allow the removal of the dictionary using a stack algorithm.
    /// </para>
    /// </remarks>
    public class CallFrame
    {
        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="CallFrame"/> class.
        /// </summary>
        public CallFrame()
        {
        }

        public CallFrame(NeuronCluster execSource)
        {
            ExecSource = execSource;
            CodeListType = ExecListType.Children;
            using (ChildrenAccessor iList = execSource.Children)
                Code = iList.ConvertTo<Expression>();
            if (Code == null)
                throw new InvalidOperationException(String.Format("Failed to transform children of cluster {0} to a list of expressions, evaluate branch aborted.", execSource.ToString()));
        }

        #endregion ctor

        #region prop

        /// <summary>
        /// Gets or sets the index of the next expression to execute, found in the <see cref="CallFrame.Code"/> list.
        /// </summary>
        /// <value>The next exp.</value>
        public int NextExp { get; set; }

        /// <summary>
        /// Gets or sets the code to execute in this frame.
        /// </summary>
        /// <value>The code.</value>
        public IList<Expression> Code { get; set; }

        /// <summary>
        /// Gets or sets the type of the code list (rules, children, actions, conditional).
        /// </summary>
        /// <value>The type of the code list.</value>
        public ExecListType CodeListType { get; set; }

        /// <summary>
        /// Gets or sets the neuron that defined the code (through rules, actions, children, conditionalparts,...)
        /// </summary>
        /// <value>The exec source.</value>
        public Neuron ExecSource { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this frame caused a new Variables dictionary. False by default.
        /// </summary>
        /// <value><c>true</c> if [caused new var dict]; otherwise, <c>false</c>.</value>
        public bool CausedNewVarDict { get; set; }

        #endregion prop

        #region functions

        /// <summary>
        /// Updates the call frame stack for the specified processor with regards to the rules of the current frame.
        /// This is used to handle different types of stack walking (like if, case, loop, foreach,...).
        /// </summary>
        /// <remarks>
        /// public version, makes certain that variables values are correctly popped.
        /// </remarks>
        /// <param name="proc">The proc.</param>
        /// <returns><c>true</c> when a frame was removed, otherwise false.</returns>
        public bool UpdateCallFrameStack(Processor proc)
        {
            //we do a catch round the update so that anything failing in this process doesn't fuck up the stack.  When
            //something wrong happens, we simply pollitly let the caller know he needs to move on to the next frame.
            try
            {
                bool iRes = InternalUpdateCallFrameStack(proc);
                if (iRes == true)
                {
                    proc.PopFrame();
                    if (CausedNewVarDict == true)
                        proc.PopVariableValues();
                }
                return iRes;
            }
            catch (Exception e)
            {
                Log.LogError("CallFrame.UpdateCallFrameStack", e.ToString());
                return true;
            }
        }

        /// <summary>
        /// Updates the call frame stack for the specified processor with regards to the rules of the current frame.
        /// This is used to handle different types of stack walking (like if, case, loop, foreach,...).
        /// </summary>
        /// <remarks>
        /// Internal version, allows for complete override on handling the CallFrameStack, withouth having to force
        /// descendents to rewrite the popping of variable values.
        /// </remarks>
        /// <param name="proc">The proc.</param>
        /// <returns><c>true</c> when a frame needs to be removed, otherwise false.</returns>
        protected virtual bool InternalUpdateCallFrameStack(Processor proc)
        {
            return NextExp >= Code.Count;
        }

        /// <summary>
        /// Duplicates this instance.
        /// </summary>
        /// <returns>A new callFrame of the same type, containing the same data.</returns>
        public CallFrame Duplicate()
        {
            CallFrame iRes = Activator.CreateInstance(GetType()) as CallFrame;
            CopyTo(iRes);
            return iRes;
        }

        protected virtual void CopyTo(CallFrame copyTo)
        {
            copyTo.CausedNewVarDict = CausedNewVarDict;
            copyTo.Code = Code;
            copyTo.CodeListType = CodeListType;
            copyTo.ExecSource = ExecSource;
            copyTo.NextExp = NextExp;
        }

        #endregion functions
    }

    public abstract class ConditionalFrame : CallFrame
    {
        /// <summary>
        /// Gets the type of the condition.
        /// </summary>
        /// <value>The type of the condition.</value>
        abstract public Neuron ConditionType { get; }
    }

    /// <summary>
    /// Base class for all frames that make use of all the conditions (if, case, loop, caseloop).
    /// </summary>
    public abstract class ConditionsFrame : ConditionalFrame
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConditionsFrame"/> class.
        /// </summary>
        /// <param name="statement">The statement.</param>
        public ConditionsFrame(ConditionalStatement statement)
        {
            Conditions = statement.Conditions;
            ConditionsCluster = statement.ConditionsCluster;
            CodeListType = ExecListType.Conditional;
            ExecSource = statement.ConditionsCluster;
            Debug.Assert(Conditions != null);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConditionsFrame"/> class.
        /// </summary>
        public ConditionsFrame()
        {
        }

        /// <summary>
        /// Gets or sets the conditions to evaluate as case items.
        /// </summary>
        /// <value>The conditions.</value>
        public IList<ConditionalExpression> Conditions { get; set; }

        /// <summary>
        /// Gets or sets the conditions cluster, used for debugging.
        /// </summary>
        /// <value>The conditions cluster.</value>
        public NeuronCluster ConditionsCluster { get; set; }

        protected override void CopyTo(CallFrame copyTo)
        {
            base.CopyTo(copyTo);
            ConditionsFrame icopyTo = (ConditionsFrame)copyTo;
            icopyTo.Conditions = Conditions;
            icopyTo.ConditionsCluster = ConditionsCluster;
        }
    }

    public class IfFrame : ConditionsFrame
    {
        public IfFrame(ConditionalStatement statement) : base(statement)
        {
        }

        public IfFrame() : base()
        {
        }

        /// <summary>
        /// Gets the type of the condition.
        /// </summary>
        /// <value>The type of the condition.</value>
        public override Neuron ConditionType
        {
            get { return Brain.Current[(ulong)PredefinedNeurons.Normal]; }
        }

        protected override bool InternalUpdateCallFrameStack(Processor proc)
        {
            if (NextExp != -1)
            {
                NextExp = -1;                                                                 //this makes certain that the frame is only run 1 time.
                for (int iIndex = 0; iIndex < Conditions.Count; iIndex++)
                {
                    ConditionalExpression i = Conditions[iIndex];
                    if (proc.EvaluateCondition(null, i, iIndex) == true)
                    {
                        CallFrame iFrame = new CallFrame(i.StatementsCluster);
                        proc.PushFrame(iFrame);
                        return false;
                    }
                }
            }
            return true;                                                      ///when we get here, we went through the entire loop without calling return, so no evalulation passed.
        }
    }

    public class CaseFrame : IfFrame
    {
        public CaseFrame(ConditionalStatement statement) : base(statement)
        {
            CaseItem = statement.CaseItem;
        }

        public CaseFrame() : base()
        {
        }

        /// <summary>
        /// Gets or sets the case item to evaluate during the loop.
        /// </summary>
        /// <value>The case item.</value>
        public Variable CaseItem { get; set; }

        /// <summary>
        /// Gets the type of the condition.
        /// </summary>
        /// <value>The type of the condition.</value>
        public override Neuron ConditionType
        {
            get { return Brain.Current[(ulong)PredefinedNeurons.Case]; }
        }

        protected override bool InternalUpdateCallFrameStack(Processor proc)
        {
            if (NextExp != -1)
            {
                NextExp = -1;                                                                 //this makes certain that the frame is only run 1 time.
                if (CaseItem != null)
                {
                    IEnumerable<Neuron> iCaseValues = Neuron.SolveResultExp(CaseItem, proc);
                    if (iCaseValues != null)
                    {
                        for (int iIndex = 0; iIndex < Conditions.Count; iIndex++)
                        {
                            ConditionalExpression i = Conditions[iIndex];
                            if (proc.EvaluateCondition(iCaseValues, i, iIndex) == true)
                            {
                                CallFrame iFrame = new CallFrame(i.StatementsCluster);
                                proc.PushFrame(iFrame);
                                return false;
                            }
                        }
                    }
                    else
                        Log.LogError("CaseFrame.InternalUpdateCallFrameStack", "Can't perform case: CaseItem is null after execution!");
                }
                else
                    Log.LogError("Processor.PerformCase", "Can't perform case: no case item defined!");
            }
            return true;                                                      ///when we get here, we went through the entire loop without calling return, so no evalulation passed.
        }

        protected override void CopyTo(CallFrame copyTo)
        {
            base.CopyTo(copyTo);
            CaseFrame iCopyTo = (CaseFrame)copyTo;
            iCopyTo.CaseItem = CaseItem;
        }
    }

    /// <summary>
    /// CallFrame for looped frames.  Stores some more data in order to handle conditionals correctly.
    /// </summary>
    public class LoopedCallFrame : ConditionsFrame
    {
        public LoopedCallFrame(ConditionalStatement statement) : base(statement)
        {
        }

        public LoopedCallFrame()
        {
        }

        /// <summary>
        /// Gets the type of the condition.
        /// </summary>
        /// <value>The type of the condition.</value>
        public override Neuron ConditionType
        {
            get { return Brain.Current[(ulong)PredefinedNeurons.Looped]; }
        }

        protected override bool InternalUpdateCallFrameStack(Processor proc)
        {
            for (int iIndex = 0; iIndex < Conditions.Count; iIndex++)
            {
                ConditionalExpression i = Conditions[iIndex];
                if (proc.EvaluateCondition(null, i, iIndex) == true)
                {
                    CallFrame iFrame = new CallFrame(i.StatementsCluster);
                    proc.PushFrame(iFrame);
                    return false;
                }
            }
            return true;                                                      ///when we get here, we went through the entire loop without calling return, so no evalulation passed.
        }
    }

    /// <summary>
    /// CallFrame for looped frames.  Stores some more data in order to handle conditionals correctly.
    /// </summary>
    public class CaseLoopedCallFrame : LoopedCallFrame
    {
        public CaseLoopedCallFrame(ConditionalStatement statement) : base(statement)
        {
            CaseItem = statement.CaseItem;
            Debug.Assert(CaseItem != null);
        }

        public CaseLoopedCallFrame()
        {
        }

        /// <summary>
        /// Gets the type of the condition.
        /// </summary>
        /// <value>The type of the condition.</value>
        public override Neuron ConditionType
        {
            get { return Brain.Current[(ulong)PredefinedNeurons.CaseLooped]; }
        }

        /// <summary>
        /// Gets or sets the case item to evaluate during the loop.
        /// </summary>
        /// <value>The case item.</value>
        public Variable CaseItem { get; set; }

        protected override bool InternalUpdateCallFrameStack(Processor proc)
        {
            IEnumerable<Neuron> iCaseValues = Neuron.SolveResultExp(CaseItem, proc);
            if (iCaseValues != null)
            {
                for (int iIndex = 0; iIndex < Conditions.Count; iIndex++)
                {
                    ConditionalExpression i = Conditions[iIndex];
                    if (proc.EvaluateCondition(iCaseValues, i, iIndex) == true)
                    {
                        CallFrame iNew = new CallFrame(i.StatementsCluster);                          //need to call the code of the conditional part.
                        proc.PushFrame(iNew);
                        return false;
                    }
                }
                proc.PopFrame();                                                          ///when we get here, we went through the entire loop without calling return, so no evalulation passed.
                return true;
            }
            else
            {
                Log.LogError("CaseLoopedCallFrame.PerformCase", "Can't perform case: CaseItem is null after execution!");
                proc.PopFrame();
                return true;
            }
        }

        protected override void CopyTo(CallFrame copyTo)
        {
            base.CopyTo(copyTo);
            CaseLoopedCallFrame icopyTo = (CaseLoopedCallFrame)copyTo;
            icopyTo.CaseItem = CaseItem;
        }
    }

    /// <summary>
    /// CallFrame for ForEach frames.  Stores some more data in order to handle conditionals correctly.
    /// </summary>
    public class ForEachCallFrame : ConditionalFrame
    {
        public ForEachCallFrame(Variable loopItem, List<Neuron> items, NeuronCluster execSource)
        {
            using (ChildrenAccessor iList = execSource.Children)
                Code = iList.ConvertTo<Expression>();
            if (Code == null)
                throw new InvalidOperationException(String.Format("Failed to transform children of cluster {0} to a list of expressions, evaluate branch aborted.", execSource.ToString()));
            LoopItem = loopItem;
            Items = items;
            ExecSource = execSource;
            CodeListType = ExecListType.Children;
        }

        public ForEachCallFrame()
        {
        }

        #region prop

        /// <summary>
        /// Gets or sets the loop item that stores the neuron in the collection.
        /// </summary>
        /// <value>The loop item.</value>
        public Variable LoopItem { get; set; }

        /// <summary>
        /// Gets or sets the items to loop through.
        /// </summary>
        /// <value>The items.</value>
        public List<Neuron> Items { get; set; }

        /// <summary>
        /// Gets or sets the index of the next item that should be returned.
        /// </summary>
        /// <value>The index.</value>
        public int Index { get; set; }

        /// <summary>
        /// Gets the type of the condition.
        /// </summary>
        /// <value>The type of the condition.</value>
        public override Neuron ConditionType
        {
            get { return Brain.Current[(ulong)PredefinedNeurons.ForEach]; }
        }

        #endregion prop

        #region functions

        protected override bool InternalUpdateCallFrameStack(Processor proc)
        {
            if (Index < Items.Count)
            {
                Dictionary<ulong, List<Neuron>> iDict = proc.VariableValues.Peek();
                Debug.Assert(iDict != null);
                iDict[LoopItem.ID] = new List<Neuron>() { Items[Index++] };
                NextExp = 0;                                                                                    //need to reset the exp pos, otherwise we can't loop.
                return false;
            }
            else
                return true;
        }

        protected override void CopyTo(CallFrame copyTo)
        {
            base.CopyTo(copyTo);
            ForEachCallFrame icopyTo = (ForEachCallFrame)copyTo;
            icopyTo.Index = Index;
            icopyTo.Items = Items;
            icopyTo.LoopItem = LoopItem;
        }

        #endregion functions
    }

    /// <summary>
    /// CallFrame for looped frames.  Stores some more data in order to handle conditionals correctly.
    /// </summary>
    public class UntilCallFrame : ConditionalFrame
    {
        #region ctor

        public UntilCallFrame(ConditionalExpression condition)
        {
            Condition = condition;
            ExecSource = condition.StatementsCluster;
            using (ChildrenAccessor iList = condition.StatementsCluster.Children)
                Code = iList.ConvertTo<Expression>();
            if (Code == null)
                throw new InvalidOperationException(String.Format("Failed to transform children of cluster {0} to a list of expressions, evaluate branch aborted.", ExecSource.ToString()));
            CodeListType = ExecListType.Children;
        }

        public UntilCallFrame()
        {
        }

        #endregion ctor

        /// <summary>
        /// Gets the type of the condition.
        /// </summary>
        /// <value>The type of the condition.</value>
        public override Neuron ConditionType
        {
            get { return Brain.Current[(ulong)PredefinedNeurons.Until]; }
        }

        /// <summary>
        /// Gets or sets the condition to check
        /// </summary>
        /// <value>The condition.</value>
        public ConditionalExpression Condition { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this is the first run of the frame or not.  This is important
        /// because an 'untill' condition needs to skip the first test.
        /// </summary>
        /// <value><c>true</c> if [first run]; otherwise, <c>false</c>.</value>
        public bool FirstRun { get; set; }

        protected override bool InternalUpdateCallFrameStack(Processor proc)
        {
            if (FirstRun == false)
            {
                if (proc.EvaluateCondition(null, Condition, 0) == false)
                    return true;
                else
                {
                    NextExp = 0;                                                                        //need to reset the exp pos, otherwise we can't loop.
                    return false;
                }
            }
            else
            {
                FirstRun = false;
                return false;
            }
        }

        protected override void CopyTo(CallFrame copyTo)
        {
            base.CopyTo(copyTo);
            UntilCallFrame icopyTo = (UntilCallFrame)copyTo;
            icopyTo.Condition = Condition;
            icopyTo.FirstRun = FirstRun;
        }
    }
}