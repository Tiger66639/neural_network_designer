﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// A dictionary that is thread safe, used by the procesor to store all the split results.
    /// </summary>
    public class SplitResultsDict : DictThreadSafe<ulong, int>
    {
        /// <summary>
        /// Provides a quick, thread safe way to init the list with the source data. A new splitResultValue is created
        /// for each entry in the source. Value and weight are both copied to the new objects.  A copy is made so the new
        /// list can have original weight values.
        /// </summary>
        /// <param name="source">The source.</param>
        public void InitFrom(IDictionary<ulong, int> source)
        {
            Clear();
            Lock.EnterWriteLock();
            try
            {
                foreach (KeyValuePair<ulong, int> i in source)
                    InternalAdd(i.Key, i.Value);
            }
            finally
            {
                Lock.ExitWriteLock();
            }
        }
    }
}