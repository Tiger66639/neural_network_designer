﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace NeuralNetworkDesigne.HAB
{
    #region Enumbs

    /// <summary>
    /// Used to let inheriters know which list is being executed.
    /// </summary>
    /// <remarks>
    /// Since we always know at compile time which list we are trying to call,
    /// we can easely pass this info along without to much overhead to the
    /// designer for instance.
    /// </remarks>
    public enum ExecListType
    {
        Rules,
        Actions,
        Children,

        /// <summary>
        /// This type is only used by execution frames (visual aids used by the designer) at the moment
        /// </summary>
        Conditional,

        /// <summary>
        /// No code list defined.
        /// </summary>
        None
    }

    /// <summary>
    /// Determins the processing direction that the processor is currently handling.
    /// </summary>
    /// <remarks>
    /// The process direction always starts with input. This indicates that input data is being processed.
    /// At a certain point, the brain can execute an <see cref="OutputIntruction"/>.  This changes the
    /// direction of the process to generate output data.
    /// </remarks>
    public enum ProcessDirection
    {
        Input,
        Output
    }

    #endregion Enumbs

    /// <summary>
    /// Used by the <see cref="Brain"/> object to perform translations from 1 type of <see cref="Neuron"/> to another using
    /// <see cref="Neuron.Rules"/> and <see cref="Neuron.Actions"/>.
    /// </summary>
    /// <remarks>
    /// <para>
    /// A processor tries to solve all the neurons on it's stack for as long as there remains a neuron on it.  If a solve run is complete
    /// for a specific neuron, and there is something still on the stack, it continues to solve that neuron, and so on.
    /// </para>
    /// <para>
    /// The property <see cref="Processor.SplitData"/> determins if it is running as the child of another processor due to a <see cref="Processor.Split"/>
    /// operation. Sub processors are used to solve conflicts between multiple possibilities by trying out different scenarios at the same
    /// time.
    /// </para>
    /// <para>
    /// After the split has been resolved, the last processor continues processing.
    /// </para>
    /// <para>
    /// Descendents should reimplement CreateProcessors.
    /// </para>
    /// <para>
    /// When a processor is finished, the <see cref="Processor.Finished"/> event is raised.
    /// </para>
    /// </remarks>
    public class Processor
    {
        #region internal types

        /// <summary>
        /// Defines the different states that a processor can have. This is primarely used for split, continue, break and exit requests.
        /// </summary>
        public enum State
        {
            Normal,
            Break,
            Exit,
            ExitNeuron,

            /// <summary>
            /// when it was not possible to convert the neuron to someting else.
            /// </summary>
            NotUnderstood
        }

        /// <summary>
        /// Used by the processor to store the contents of all the outgoing links on the currently executing processor.
        /// This is used to build a buffer, so the processor can remember all the links while processing them (this way,
        /// they can be changed, while preserving the execution order of the current run.
        /// </summary>
        internal class LinkContent
        {
            public IEnumerable<Neuron> Info { get; set; }
            public Neuron To { get; set; }
            public Neuron Meaning { get; set; }
        }

        #endregion internal types

        #region fields

        /// <summary>
        /// Stores all the neurons that need to be executed.
        /// </summary>
        private Stack<Neuron> fNeuronStack = new Stack<Neuron>(Settings.InitProcessorStackSize);

        private State fState = State.Normal;
        private Stack<CallFrame> fCallFrameStack = new Stack<CallFrame>();

        private int fCurLinkIndex;
        private List<LinkContent> fCurrentLinks;
        private Neuron fCurrentMeaning;                                                                                  //ref to the meaning being executed.
        private Neuron fCurrentTo;
        private IEnumerable<Neuron> fCurInfo;                                                                            //the list with the current info of the exeucting link.  We store a copy, so the user can delete the currently executing link, but still get to the currently executing data.
        private Neuron fNeuronToSolve;                                                                                   //stores a ref to the neuron we are solving, important for passing along to sub processors.

        private SplitData fSplitData;                                                                                    //contains data for sub procesors, also indicates that this is a sub processor.
        private SplitResultsDict fSplitValues = new SplitResultsDict();                                                  //contains all the split result values currently assigned to this processor.
        private Stack<Dictionary<ulong, List<Neuron>>> fVariableValues = new Stack<Dictionary<ulong, List<Neuron>>>();
        private Dictionary<ulong, List<Neuron>> fGlobalValues = new Dictionary<ulong, List<Neuron>>();
        private Sin fCurrentSin;
        private Neuron fThreadNeuron;
        private AutoResetEvent fThreadBlocker;

        /// <summary>
        /// This static has a unique value for each thread.  It contains which <see cref="Processor"/> is currently
        /// running in the current thread.  This is used by the <see cref="Brain.Add"/> to check for valid
        /// operations.
        /// </summary>
        [ThreadStatic]
        static public Processor CurrentProcessor;                                                                //must be a field, can't use ThreadStatic otherwise.

        #endregion fields

        #region Events

        /// <summary>
        /// Raised when the processor is finished processing.
        /// </summary>
        public event EventHandler Finished;

        #endregion Events

        #region prop

        #region NeuronToSolve

        /// <summary>
        /// Gets the Neuron currently being solved.
        /// </summary>
        /// <remarks>
        /// This can be usefull to do queries from.  It is considered to be an always valid variable (
        /// or better a constant) from within <see cref="Instruction"/>s and search criteria, so from
        /// within neural programs.
        /// </remarks>
        public virtual Neuron NeuronToSolve
        {
            get { return fNeuronToSolve; }
            internal set { fNeuronToSolve = value; }
        }

        #endregion NeuronToSolve

        #region VariableValues

        /// <summary>
        /// Gets the stack of dictionaries containing all the current values for the variables.
        /// </summary>
        public Stack<Dictionary<ulong, List<Neuron>>> VariableValues
        {
            get { return fVariableValues; }
        }

        #endregion VariableValues

        #region GlobalValues

        /// <summary>
        /// Gets the dictionary containing all the global values valid for this processor.
        /// </summary>
        public Dictionary<ulong, List<Neuron>> GlobalValues
        {
            get { return fGlobalValues; }
        }

        #endregion GlobalValues

        #region CurrentState

        /// <summary>
        /// Gets the current state of the brain.
        /// </summary>
        /// <value>The state of the current.</value>
        internal State CurrentState
        {
            get { return fState; }
            set { fState = value; }
        }

        #endregion CurrentState

        #region SplitData

        /// <summary>
        /// Gets or sets the split data.
        /// </summary>
        /// <remarks>
        /// contains data for sub procesors, also indicates that this is a sub processor.
        /// Must be internal so that it can't be seen by reflector.
        /// </remarks>
        /// <value>The split data.</value>
        internal SplitData SplitData
        {
            get { return fSplitData; }
            set { fSplitData = value; }
        }

        #endregion SplitData

        #region SplitValues

        /// <summary>
        /// Gets the dictionary containing all the split result values for this processor. This is a thread safe accessible
        /// dictionary.
        /// </summary>
        /// <remarks>
        /// Because this is a dictionary, it is not possible to store the same neuron 2 times.  This guarantees that the list
        /// always contains unique items.
        /// </remarks>
        /// <value>The split values.</value>
        protected internal SplitResultsDict SplitValues
        {
            get { return fSplitValues; }
        }

        #endregion SplitValues

        #region NextExp

        /// <summary>
        /// Gets/sets the index of the <see cref="Expression"/> that will be executed next.
        /// </summary>
        /// <remarks>
        /// This points to the next expression for the split.
        /// Changing this value will cause a jump in the execution position.
        /// </remarks>
        public int NextExp
        {
            get
            {
                if (CallFrameStack.Count > 0)
                {
                    CallFrame iFrame = CallFrameStack.Peek();
                    if (iFrame != null)
                        return iFrame.NextExp;
                }
                return -1;
            }
            set
            {
                if (CallFrameStack.Count > 0)
                {
                    CallFrame iFrame = CallFrameStack.Peek();
                    if (iFrame != null)
                        iFrame.NextExp = value;
                }
                else
                    throw new InvalidOperationException("Can't change execution positions: no frame present.");
            }
        }

        #endregion NextExp

        #region CurrentExpression

        /// <summary>
        /// Gets the current expression that is being (or just was) executed.
        /// </summary>
        /// <value>The current expression.</value>
        public Expression CurrentExpression
        {
            get
            {
                int iId = NextExp;                                                      //we always point to the next step already when a step is executing (this is done for the split).
                if (CurrentCode != null && iId >= 0 && iId < CurrentCode.Count)
                    return CurrentCode[iId];
                else
                    return null;
            }
        }

        #endregion CurrentExpression

        #region CurrentLink

        /// <summary>
        /// Gets/sets the index of the link that is currently being processed in a <see cref="Processor.Solve"/>  operation.
        /// </summary>
        /// <remarks>
        /// Changing this value will cause a jump in the execution position.
        /// </remarks>
        public int CurrentLink
        {
            get
            {
                return fCurLinkIndex;
            }
            set
            {
                fCurLinkIndex = value;
            }
        }

        #endregion CurrentLink

        #region CurrentLinks

        /// <summary>
        /// Gets the list of all the links that are currently being executed.
        /// </summary>
        /// <remarks>
        /// These are stored at the class level so we can pass them along during a split.  If we don't do this, a sub processor
        /// could get screwed up if the currently executing neuron gets changed when he is picking up after the split.
        /// </remarks>
        internal List<LinkContent> CurrentLinks
        {
            get { return fCurrentLinks; }
            set { fCurrentLinks = value; }
        }

        #endregion CurrentLinks

        #region CurrentSin

        /// <summary>
        /// Gets/sets the Sin that triggered this processor / for which it is generating data.
        /// </summary>
        /// <remarks>
        /// This is stored in the processor cause a processor can only have 1 current sin and is
        /// local for the process.
        /// It is provided so that the <see cref="CurrentSin"/> system variable can easely find this
        /// value.
        /// <para>
        /// This var is automatically filled in once a processor starts.  It can be changed by the
        /// <see cref="OutputInstruction"/> when it turns the processor into an output generator.
        /// </para>
        /// </remarks>
        public Sin CurrentSin
        {
            get
            {
                return fCurrentSin;
            }
            internal set
            {
                fCurrentSin = value;
            }
        }

        #endregion CurrentSin

        #region CurrentMeaning

        /// <summary>
        /// Gets/sets the meaning neuron that is currently being executed in order to solve the <see cref="Processor.NeuronToSolve"/>.
        /// </summary>
        public Neuron CurrentMeaning
        {
            get
            {
                return fCurrentMeaning;
            }
            internal set
            {
                fCurrentMeaning = value;
            }
        }

        #endregion CurrentMeaning

        #region CurrentTo

        /// <summary>
        /// Gets the the neuron pointed too by the currently executing link.
        /// </summary>
        public Neuron CurrentTo
        {
            get { return fCurrentTo; }
            internal set { fCurrentTo = value; }
        }

        #endregion CurrentTo

        #region CurrentInfo

        /// <summary>
        /// Gets the list with the currently valid info neurons or an empty list if there is none.
        /// </summary>
        public IEnumerable<Neuron> CurrentInfo
        {
            get
            {
                if (fCurInfo != null)
                    return fCurInfo;
                else
                    return new List<Neuron>();
            }
            internal set
            {
                fCurInfo = value;
            }
        }

        #endregion CurrentInfo

        #region CurrentCode

        /// <summary>
        /// Gets the list of code that is currently being executed.
        /// </summary>
        /// <remarks>
        /// This is stored for splits.  This way, code before the split argument can change the currently executing code
        /// while still keeping the original code for the remainder of the function as would be the case when there was
        /// no split.  If we don't store this, a split would result in the recalculation of the current code, which might
        /// differ from the original processor's code.
        /// </remarks>
        public IList<Expression> CurrentCode
        {
            get
            {
                if (CallFrameStack.Count > 0)
                {
                    CallFrame iFrame = CallFrameStack.Peek();
                    if (iFrame != null)
                        return iFrame.Code;
                }
                return null;
            }
        }

        #endregion CurrentCode

        #region CurrentExpressionSource

        /// <summary>
        /// Gets the Neuron that contains the code currently being executed.  Which type of code that is being executed (the list), can
        /// be found throught <see cref="Processor.CurrentExecListType"/>
        /// </summary>
        /// <remarks>
        /// This is primarely provided for debuggers and designers.
        /// </remarks>
        public Neuron CurrentExecSource
        {
            get
            {
                if (CallFrameStack.Count > 0)
                {
                    CallFrame iFrame = CallFrameStack.Peek();
                    if (iFrame != null)
                        return iFrame.ExecSource;
                }
                return null;
            }
        }

        #endregion CurrentExpressionSource

        #region CurrentExecListType

        /// <summary>
        /// Gets the type of list that is currently being executed, which is attached to <see cref="Processor.CurrentExpressionSource"/>.
        /// </summary>
        /// <remarks>
        /// This is primarely provided for debuggers and designers.
        /// </remarks>
        public ExecListType CurrentExecListType
        {
            get
            {
                if (CallFrameStack.Count > 0)
                {
                    CallFrame iFrame = CallFrameStack.Peek();
                    if (iFrame != null)
                        return iFrame.CodeListType;
                }
                return ExecListType.None;
            }
        }

        #endregion CurrentExecListType

        #region Neuronstack

        /// <summary>
        /// the data stack.
        /// </summary>
        protected Stack<Neuron> NeuronStack
        {
            get { return fNeuronStack; }
        }

        #endregion Neuronstack

        #region CallStack

        /// <summary>
        /// Gets the stack containing the callframe data (for conditionals and sub routines). (dont use directly)
        /// </summary>
        /// <remarks>
        /// don't use this stack directly, use <see cref="Processor.PushFrame"/> and <see cref="Processor.PopFrame"/> instead.
        /// </remarks>
        private Stack<CallFrame> CallFrameStack
        {
            get { return fCallFrameStack; }
        }

        #endregion CallStack

        /// <summary>
        /// Gets the number of neurons currently on the stack.
        /// </summary>
        /// <value>The nr of items on the stack.</value>
        public int Count
        {
            get { return fNeuronStack.Count; }
        }

        #region ThreadNeuron

        /// <summary>
        /// Gets the Neuron that represents the thread in which this processor is running.
        /// </summary>
        public Neuron ThreadNeuron
        {
            get { return fThreadNeuron; }
            set { fThreadNeuron = value; }
        }

        #endregion ThreadNeuron

        #region ThreadBlocker

        /// <summary>
        /// Gets the object to block, restart the processor.
        /// </summary>
        public AutoResetEvent ThreadBlocker
        {
            get
            {
                if (fThreadBlocker == null)
                    fThreadBlocker = new AutoResetEvent(false);
                return fThreadBlocker;
            }
        }

        #endregion ThreadBlocker

        #endregion prop

        #region Functions

        #region Solve/call/branch

        /// <summary>
        /// Tries to solve all the <see cref="Neuron"/>s currently on the stack untill the stack is empty, untill there is only 1 neuron
        /// left that can't be solved anymore or untill <see cref="Process.Exit"/> is called.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Inheriters should reinplement <see cref="Processor.InternalSolve"/> since this is called by all solving routines (this and
        /// <see cref="Processor.SolveBlocked"/>).
        /// </para>
        /// this method simply starts an internal thread.
        /// </remarks>
        public void Solve()
        {
            Action iFunc = new Action(InternalSolve);
            iFunc.BeginInvoke(null, null);
        }

        public void SolveBlocked()
        {
            Action iFunc = new Action(InternalSolve);
            IAsyncResult iAsyncCall = iFunc.BeginInvoke(null, null);
            iAsyncCall.AsyncWaitHandle.WaitOne();                                                           //we wait untill the sub thread is finnished.
        }

        /// <summary>
        /// pops last item from stack to solve, solves it and continues untill there is nothing more on the stack.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Actual solving is done by <see cref="Processor.SolveStack"/>.  This one mostly takes care of the current processor.
        /// </para>
        /// </remarks>
        private void InternalSolve()
        {
            try
            {
                CurrentProcessor = this;                                                            //we need to indicate that this thread is run for this processor
                try
                {
                    Thread.CurrentThread.Name = "Processor";                                   //this is for easy debugging, so we recognise the thread in VS.
                    SolveStack();
                }
                catch (Exception e)                                                                    //we need a catch all handler since this is the entry point of the thread.
                {
                    Log.LogError("Processor.InternalSolve", string.Format("Internal error: {0}.", e));
                }
                OnFinished();
            }
            finally
            {
                CurrentProcessor = null;
            }
        }

        /// <summary>
        /// Solves the stack from the specified neuron. Used for starting a subprocessor.
        /// </summary>
        /// <param name="from">From.</param>
        internal void SolveStackAfterSplit()
        {
            Thread.CurrentThread.Name = "SubProc for: " + fNeuronToSolve.ID;                                   //this is for easy debugging, so we recognise the thread in VS.
            CurrentProcessor = this;                                                                           //we need to indicate that this thread is run for this processor
            try
            {
                try
                {
                    SolveNeuronAfterSplit();
                    SolveStack();
                }
                catch (Exception e)                                                                          //if there is an exception, we still need to check and update the end of the split.
                {
                    Log.LogError("Processor.InternalSolve", string.Format("Internal error: {0}.", e));
                    if (SplitData != null)                                                                  //when we get here, let it know we are ready.
                        SplitManager.Default.FinishSplit(SplitData);
                }
                OnFinished();
            }
            finally
            {
                CurrentProcessor = null;
                fVariableValues.Pop();                                                                          //remove the dictionary cause the function is ready and it was put on there due to the split.
            }
        }

        /// <summary>
        /// Called when the processor is finished with solving the stack. Raises the <see cref="Processor.Finished"/> event.
        /// </summary>
        protected virtual void OnFinished()
        {
            if (Finished != null)
                Finished(this, EventArgs.Empty);
        }

        /// <summary>
        /// Solves (or tries to solve) all the neurons on the stack.
        /// </summary>
        /// <remarks>
        /// Actual solving is done by another method.  This one mostly handles state changes in the procesor like for exit requests.
        /// The state of the processor is checked before any code is called.
        /// </remarks>
        private void SolveStack()
        {
            Neuron iToSolve;
            bool iLoop = true;
            while (iLoop == true)
            {
                if (NeuronStack.Count > 0 && fState != State.Exit)                                        //we check the exit state before we call any code, cause this function can be called from 'SolveStackFrom', in which case we must be prepared for an exit state.
                {
                    iToSolve = NeuronStack.Pop();
                    CurrentLink = 0;                                                              //solveNeuron doesn't do this cause it allows arbitrary start pos.
                    SolveNeuron(iToSolve);
                }
                else
                {
                    bool iLastOfSplits = false;                                                      //when true, this thread was the last to resolve in a split set.  This is used to determin if we need to continue processing or not.
                    if (SplitData != null)                                                                  //when we get here, let it know the split is ready, so we can check if we need to continue after resolving it.
                        iLastOfSplits = SplitManager.Default.FinishSplit(SplitData);
                    if (NeuronStack.Count != 0 && iLastOfSplits == true && fState != State.Exit)           //we we requested an exit, we always need to quit.
                        fState = State.Normal;                                                        //always reset the state, cause the loop properly exited through the Exit statement.  After the split has been resolved, the stack is empty again, so if there is something on it after resolving the split, the callback put it on there.
                    else
                        iLoop = false;                                                                //we really want to quit.
                }
            }
        }

        /// <summary>
        /// Tries to solve the specified neuron.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Solving is done by walking down every 'To' link in the neuron and pushing this to reference on the stack after which it calls
        /// the <see cref="Link.Meaning"/>'s <see cref="Neuron.Actions"/> list.
        /// </para>
        /// <para>
        /// this is an internal function, cause the output instruction needs to be able to let a neuron be solved
        /// sequential (<see cref="Processor.Solve"/> is async and solves the intire stack.
        /// </para>
        /// </remarks>
        /// <param name="toSolve"></param>
        protected internal virtual void SolveNeuron(Neuron toSolve)
        {
            fVariableValues.Push(new Dictionary<ulong, List<Neuron>>());                     //a new function needs a new dictionary for the variables, so that variable values are local to the function.
            try
            {
                SolveNeuronWithoutVarChange(toSolve);
            }
            finally
            {
                fVariableValues.Pop();                                                                          //remove the dictionary cause the function is ready.
            }
        }

        /// <summary>
        /// Solves the neuron without changing the variable dictionary stack and without initializing to the first
        /// link so that execution can be picked up from the current position.
        /// </summary>
        /// <remarks>
        /// When solving, we need to check if the item needs to be sent to the output sin.  This is the
        /// case if we are in output mode + the current neuron to solve has not outgoing links that it can solve itself
        /// <para>
        /// also sets <see cref="Processor.NeuronToSolve"/>
        /// </para>
        /// </remarks>
        /// <param name="toSolve">To solve.</param>
        private void SolveNeuronWithoutVarChange(Neuron toSolve)
        {
            NeuronToSolve = toSolve;
            //don't init current link cause this way we can start from an arbitrary position like with SolveNeuronFrom
            //also: can't put a 'using' for the accessor to the links round the whole loop, cause then we can't change the currenly changing neuron,
            //wich should be possible.
            CurrentLinks = BuildExecList(toSolve);
            SolveLinks();
        }

        /// <summary>
        /// Solves all the links, stored in <see cref="Processor.CurrentLinks"/>.
        /// </summary>
        private void SolveLinks()
        {
            int iActionsLink = -1;                                                                 //keeps track+stores the expressions of the 'Actions' link for the neuron to solve.  This is handled seperatly.
            try
            {
                while (CurrentLinkIsValid() == true)                                            //using this type of loop allows the processor to easely perform code jumps.
                {
                    SetCurrentPos(CurrentLinks[CurrentLink]);
                    if (CurrentMeaning != null && CurrentMeaning.ID != (ulong)PredefinedNeurons.Actions)
                    {
                        ExecuteMeaning();
                        if (CurrentState == State.Exit)
                            break;
                        if (NeuronToSolve.ID == Neuron.EmptyId)                                          //if CurrentFrom got deleted, we can't process any further and need to stop.  This is a safety precaution.
                            break;
                    }
                    else if (CurrentMeaning == null)
                        Log.LogError("Processor.SolveNeuron", "Internal error: link has an invalid meaning cause it can't be found in the brain.");
                    else if (iActionsLink == -1)                                                       //this is simply a quicker way to find the 'action's ref.  We have to walk through all the links anyway, so we will pass the 'actions' link
                        iActionsLink = CurrentLink;
                    else
                        Log.LogWarning("Processor.SolveNeuronWithoutVarChange", "Found more than 1 link declared as the 'actions' cluster!");     //it's a double 'actions' list, which should not be declared, not illegal, but lets get the user informed.
                    CurrentLink++;                                                                      //if we don't do this, we never get out of the loop.
                    if (CurrentState == State.ExitNeuron)
                        break;
                }
            }
            finally
            {
                CurrentMeaning = null;
                CurrentLink = iActionsLink;
                if (iActionsLink > -1)
                    OnNeuronProcessed(NeuronToSolve, CurrentLinks[iActionsLink].To as NeuronCluster);
                else
                    OnNeuronProcessed(NeuronToSolve, null);
            }
        }

        /// <summary>
        /// Checks if the current link is valid and tries to solve it if it isn't.
        /// </summary>
        /// <returns></returns>
        private bool CurrentLinkIsValid()
        {
            LinkContent iContent;
            if (CurrentLink < CurrentLinks.Count)
                iContent = CurrentLinks[CurrentLink];
            else
                return false;

            while (iContent.To.ID == Neuron.EmptyId || iContent.Meaning.ID == Neuron.EmptyId)               //if, between creation of the link-content list and execution at this stage, the meaning or to neuron have been deleted, don't execute the link, but go to the next one.
            {
                CurrentLink++;
                if (CurrentLink < CurrentLinks.Count)
                    iContent = CurrentLinks[CurrentLink];
                else
                    break;
            }
            return CurrentLink < CurrentLinks.Count;
        }

        private List<LinkContent> BuildExecList(Neuron toSolve)
        {
            List<LinkContent> iRes = new List<LinkContent>();
            using (LinksAccessor iLinksToSolve = toSolve.LinksOut)
            {
                foreach (Link i in iLinksToSolve.Items)
                {
                    LinkContent iNew = new LinkContent();
                    iNew.Meaning = i.Meaning;
                    iNew.To = i.To;
                    using (LinkInfoAccessor iList = i.Info)
                        iNew.Info = iList.ConvertTo<Neuron>();
                    iRes.Add(iNew);
                }
            }
            return iRes;
        }

        /// <summary>
        /// Sets the current pos (meaning, info, to
        /// </summary>
        /// <param name="link">The link.</param>
        private void SetCurrentPos(LinkContent link)
        {
            CurrentMeaning = link.Meaning;                                            //Need to store the meaning and To seperatly, in case the link gets deleted, need the link for the Info
            CurrentInfo = link.Info;
            CurrentTo = link.To;
        }

        /// <summary>
        /// Executes the rules of the neuron on the meaning of the current link.
        /// </summary>
        protected virtual void ExecuteMeaning()
        {
            Process(CurrentMeaning, CurrentMeaning.Rules, ExecListType.Rules);                     //this will continue to process where the parent left off cause the split copied over the current statement position
        }

        /// <summary>
        /// Called when all normall links have been processed except the actions (if any), which are executed here.
        /// </summary>
        /// <remarks>
        /// This can be used to remove the neuron from the stack, but also to indicate that the 'actions' link is being executed.
        /// </remarks>
        /// <param name="toSolve">neuron that was solved.</param>
        /// <param name="cluster">The cluster taht contains the code.</param>
        protected virtual void OnNeuronProcessed(Neuron toSolve, NeuronCluster cluster)
        {
            if (cluster != null)
            {
                IList<Expression> iActions;
                using (ChildrenAccessor iList = cluster.Children)
                    iActions = iList.ConvertTo<Expression>();
                if (iActions != null)                                                          //quick check, if there are no items at this point, we don't try to execute.  This is not thread save, can't block cause ConvertTo does the actual blocking.
                    Process(toSolve, iActions, ExecListType.Actions);
            }
        }

        /// <summary>
        /// Tries to solve the specified neuron starting from the specified link and statement.
        /// </summary>
        /// <param name="toSolve">The neuron that needs solving.</param>
        /// <param name="fromLink">The index of the link that we need to continue processing.</param>
        /// <param name="fromStatement"></param>
        protected virtual void SolveNeuronAfterSplit()
        {
            if (NeuronToSolve != null && NeuronToSolve.ID != Neuron.EmptyId)
            {
                if (CurrentMeaning != null)                                                                        //Currentmeaning is set during the
                {
                    if (CurrentCode != null)
                    {
                        CallFrame iFrame = CallFrameStack.Peek();
                        Debug.Assert(iFrame != null);
                        iFrame.NextExp++;                                                                      //this is not done when a split is performed because it is done after the process statement.
                        iFrame.UpdateCallFrameStack(this);                                                     //same as for NextExp.
                        ProcessFrames();                                                                       //this will continue to process where the parent left off cause the split copied over the current statement position
                    }
                    if (CurrentState == State.Exit)
                        return;
                    if (CurrentState == State.ExitNeuron)                                                     //with an exit neuron, still need to execute the actions that were attached to it.
                    {
                        OnNeuronProcessed(NeuronToSolve, NeuronToSolve.ActionsCluster);
                        return;
                    }
                    CurrentLink++;                                                                            //we need to advance caus current link has been processed, need to do next.
                    SolveLinks();                                                                             //we hook back into the regular procedure, don't need var dict handling, this is done here cause a split makes a duplicate of the original's dict.
                }
                else
                    Log.LogError("Processor.SolveNeuronFrom", "Internal error: Can't find currently executing meaning.");
            }
            else
                Log.LogWarning("Processor.SolveNeuronFrom", "Neuron to solve has been deleted, moving on to next neuron.");
        }

        /// <summary>
        /// Tries to process the list of neurons.
        /// </summary>
        /// <remarks>
        /// Don't init the pos, this way it can be used to start from an arbitrary pos like with SolveNeuronFrom.
        /// </remarks>
        /// <param name="toExec">
        /// The neuron who's code needs executing.  This is not used in this class but is provided
        /// for inheritters, so they can do something with it.
        /// </param>
        protected void Process(Neuron toExec, IList<Expression> toProcess, ExecListType listType)
        {
            if (toExec != null && toExec.ID != Neuron.EmptyId)
            {
                if (toProcess != null)
                {
                    try
                    {
                        CallFrame iFrame = new CallFrame() { ExecSource = toExec, CodeListType = listType, Code = toProcess };
                        PushFrame(iFrame);
                        ProcessFrames();
                    }
                    catch (Exception e)                                                                    //very important to have a catch here, otherwise we can't log the error correctly (don't know anymore who raised the error).
                    {
                        Log.LogError("Processor.Process", e.ToString());
                    }
                }
                else
                    Log.LogWarning("Processor.Process", "Nothing to process.");
            }
            else
                Log.LogWarning("Processor.Process", "Neuron to process has been deleted, moving on to next neuron.");
        }

        private void ProcessFrames()
        {
            if (CallFrameStack.Count > 0)
            {
                CallFrame iFrame = CallFrameStack.Peek();
                while (iFrame != null)                                      //using this type of loop allows the processor to easely perform code jumps.
                {
                    while (iFrame.NextExp >= 0 && NextExp < iFrame.Code.Count)
                    {
                        Expression iExp = iFrame.Code[iFrame.NextExp];
                        Process(iExp);                                                                               //we don't call iExp.Execute directly, this way, a debugger or something else can do something just before and/or after a statement is performed.
                        iFrame.NextExp++;                                                                            //important to do this after process, otherwise the debugger gets screwed.  We can do this, even tough 'process' can change the stack because of the local ref. it does give a problem for the split though, which needs to increment it's last value
                        CallFrame iNew = null;
                        if (CallFrameStack.Count > 0)
                            iNew = CallFrameStack.Peek();                                                              //we always retrieve the frame after an expression, this way, we can move around in the call frame stack.
                        if (iNew != iFrame)                                                                          //we only try to update the CallFrameStack, if there is a new frame or we are at the end of the current one.
                        {
                            iFrame = iNew;
                            break;
                        }
                    }
                    if (iFrame != null)
                    {
                        iFrame.UpdateCallFrameStack(this);
                        if (CallFrameStack.Count > 0)                                                                   //after
                            iFrame = CallFrameStack.Peek();
                        else
                            iFrame = null;
                    }
                }
            }
        }

        /// <summary>
        /// Tries to process a single expression.
        /// </summary>
        /// <param name="toProcess"></param>
        protected virtual void Process(Expression toProcess)
        {
            try
            {
                toProcess.Execute(this);
            }
            catch (Exception e)                                                                    //very important to have a catch here, otherwise we can't log the error correctly (don't know anymore who raised the error).
            {
                Log.LogError("Processor.Process", e.ToString());
            }
        }

        /// <summary>
        /// Converts the neuron cluster's children to expressions and tries to execute them
        /// </summary>
        /// <remarks>
        /// Since this function can also be called from the outside to initiate a run, it must
        /// also assign the current processor, and reset it after it's done.
        /// </remarks>
        public virtual void Call(NeuronCluster cluster)
        {
            Processor iPrev = CurrentProcessor;
            CurrentProcessor = this;                                                                           //we need to indicate that this thread is run for this processor
            try
            {
                if (cluster != null)
                {
                    fVariableValues.Push(new Dictionary<ulong, List<Neuron>>());                        //a new function needs a new dictionary for the variables, so that variable values are local to the function.
                    CallFrame iFrame = new CallFrame(cluster);
                    iFrame.CausedNewVarDict = true;
                    PushFrame(iFrame);
                    ProcessFrames();
                }
                else
                    Log.LogError("Processor.Call", "Failed to perform call: NeuronCluster ref is null.");
            }
            finally
            {
                CurrentProcessor = iPrev;
            }
        }

        #endregion Solve/call/branch

        #region Stack Operations

        /// <summary>
        /// Pushes the frame on the call-frame stack.
        /// </summary>
        /// <param name="frame">The frame.</param>
        public virtual void PushFrame(CallFrame frame)
        {
            CallFrameStack.Push(frame);
        }

        /// <summary>
        /// Pops the last frame on the call-frame stack.
        /// </summary>
        /// <returns></returns>
        public virtual CallFrame PopFrame()
        {
            if (CallFrameStack.Count > 0)
                return CallFrameStack.Pop();
            else
                return null;
        }

        /// <summary>
        /// Clears all the frames from the call-frame stack..
        /// </summary>
        public virtual void ClearFrames()
        {
            CallFrameStack.Clear();
        }

        public virtual void PopVariableValues()
        {
            VariableValues.Pop();
        }

        /// <summary>
        /// Pushes the specified neuron on it's internal stack.
        /// </summary>
        /// <param name="toPush">The neuron to add.</param>
        public virtual void Push(Neuron toPush)
        {
            fNeuronStack.Push(toPush);
        }

        /// <summary>
        /// Removes the last node from the stack.
        /// </summary>
        /// <remarks>
        /// You can't do this directly on a stack list cause other types of processors might want
        /// to do some extra stuf, like a warn a debugger.
        /// </remarks>
        public virtual Neuron Pop()
        {
            if (fNeuronStack.Count > 0)
                return fNeuronStack.Pop();
            else
                Log.LogError("Processor.Pop", "Internal error: no more items on stack.");
            return null;
        }

        /// <summary>
        /// Returns the neuron currently at the top of the Neuron execution stack (neurons that still need to be solved).
        /// </summary>
        /// <returns>The current top of the stack or <see cref="PredefinedNeurons.Empty"/> if there is nothing on the stack.</returns>
        public virtual Neuron Peek()
        {
            if (fNeuronStack.Count > 0)
                return fNeuronStack.Peek();
            else
                return Brain.Current[(ulong)PredefinedNeurons.Empty];
        }

        #endregion Stack Operations

        #region Conditional flow

        /// <summary>
        /// Evaluates the conditional.
        /// </summary>
        /// <param name="iLoopStyle">The i loop style.</param>
        /// <param name="statement">The statement.</param>
        internal void EvaluateConditional(Neuron iLoopStyle, ConditionalStatement statement)
        {
            switch (iLoopStyle.ID)
            {
                case (ulong)PredefinedNeurons.Normal:
                    PerformIf(statement);
                    break;

                case (ulong)PredefinedNeurons.Case:
                    PerformCase(statement);
                    break;

                case (ulong)PredefinedNeurons.Looped:
                    PerformLoop(statement);
                    break;

                case (ulong)PredefinedNeurons.CaseLooped:
                    PerformCaseLooped(statement);
                    break;

                case (ulong)PredefinedNeurons.Until:
                    PerformUntill(statement);
                    break;

                case (ulong)PredefinedNeurons.ForEach:
                    PerformForEach(statement);
                    break;

                default:
                    Log.LogError("Processor.EvaluateConditional", string.Format("Unknown loop style: '{0}'.", iLoopStyle));
                    break;
            }
        }

        /// <summary>
        /// Checks the conditions if there are any empty allowed + if so, that they are at the back.
        /// </summary>
        /// <param name="nrAlowedEmpty">The nr of alowed empty conditions (usually 1 or 0). This is counted from the back.</param>
        private static void CheckConditions(IList<ConditionalExpression> list, int nrAlowedEmpty)
        {
            for (int i = list.Count - 1; i >= 0; i--)
            {
                if (list[i].Condition == null && nrAlowedEmpty <= 0)
                    Log.LogError("Processor.CheckConditions", string.Format("Invalid condition at index: {0}!", list.Count - i - 1));
                nrAlowedEmpty--;
            }
        }

        /// <summary>
        /// uses the result of the condition of each <see cref="ConditionalExpression"/> as the 'for' part
        /// </summary>
        /// <remarks>
        /// Only 1 condition statement allowed.
        /// </remarks>
        private void PerformForEach(ConditionalStatement statement)
        {
            ReadOnlyCollection<ConditionalExpression> iCond = statement.Conditions;
            Variable iLoopItem = statement.LoopItem;
            if (iLoopItem != null)
            {
                if (iCond != null && iCond.Count != 0)
                {
                    //if there are more than 1, it doesn't stop executing it, so only a warning is needed, there is simply to much and invalid info.
                    if (iCond.Count != 1)
                        Log.LogWarning("Processor.PerformForEach", "To many condition expressions defined in group, only first is evaluated in a for - each loop!");
                    CheckConditions(iCond, 0);
                    List<Neuron> iItems = Neuron.SolveResultExp(iCond[0].Condition, this).ToList();
                    ForEachCallFrame iFrame = new ForEachCallFrame(iLoopItem, iItems, iCond[0].StatementsCluster);
                    PushFrame(iFrame);
                }
                else
                    Log.LogError("Processor.PerformForEach", "Can't perform for - each: no conditions defined!");
            }
            else
                Log.LogError("Processor.PerformForEach", "Can't perform for - each: no conditions defined!");
        }

        /// <summary>
        /// Do the statements, check the condition after the perform (done through the callframe).
        /// </summary>
        /// <remarks>
        /// only 1 conditional expressio allowed.
        /// </remarks>
        /// <returns></returns>
        private void PerformUntill(ConditionalStatement statement)
        {
            ReadOnlyCollection<ConditionalExpression> iCond = statement.Conditions;
            if (iCond != null && iCond.Count != 0)
            {
                //if there are more than 1, it doesn't stop executing it, so only a warning is needed, there is simply to much and invalid info.
                if (iCond.Count != 1)
                    Log.LogWarning("Processor.PerformUntill", "To many condition expressions defined in group, only first is evaluated and performed in an untill loop!");
                CheckConditions(iCond, 0);
                UntilCallFrame iFrame = new UntilCallFrame(iCond[0]);
                PushFrame(iFrame);
            }
            else
                Log.LogError("Processor.PerformForEach", "Can't perform for - each: no conditions defined!");
        }

        /// <summary>
        /// Evaluats the conditional expressions like in a looped case statement, using the CallFrameStack.
        /// </summary>
        private void PerformCaseLooped(ConditionalStatement statement)
        {
            ReadOnlyCollection<ConditionalExpression> iCond = statement.Conditions;
            if (iCond != null && iCond.Count != 0)
            {
                CheckConditions(iCond, 1);
                CaseLoopedCallFrame iFrame = new CaseLoopedCallFrame(statement);
                PushFrame(iFrame);
            }
            else
                Log.LogError("Processor.PerformCaseLooped", "Can't perform for - each: no conditions defined!");
        }

        /// <summary>
        /// Evaluats the conditional expressions like in a case statement.
        /// </summary>
        /// <returns>True if there was a valid case conditional expression found, otherwise false.</returns>
        private void PerformCase(ConditionalStatement statement)
        {
            ReadOnlyCollection<ConditionalExpression> iCond = statement.Conditions;
            if (iCond != null && iCond.Count != 0)
            {
                CheckConditions(iCond, 1);
                CaseFrame iFrame = new CaseFrame(statement);
                PushFrame(iFrame);
            }
            else
                Log.LogError("Processor.PerformCase", "Can't perform case: no conditions defined!");
        }

        private void PerformLoop(ConditionalStatement statement)
        {
            ReadOnlyCollection<ConditionalExpression> iCond = statement.Conditions;
            if (iCond != null && iCond.Count != 0)
            {
                CheckConditions(iCond, 1);
                LoopedCallFrame iFrame = new LoopedCallFrame(statement);
                PushFrame(iFrame);
            }
            else
                Log.LogError("Processor.PerformLoop", "Can't perform loop statement: no conditions defined!");
        }

        private void PerformIf(ConditionalStatement statement)
        {
            ReadOnlyCollection<ConditionalExpression> iCond = statement.Conditions;
            if (iCond != null && iCond.Count != 0)
            {
                CheckConditions(iCond, 1);
                IfFrame iFrame = new IfFrame(statement); ;
                PushFrame(iFrame);
            }
            else
                Log.LogError("Processor.PerformIf", "Can't perform if statement: no conditions defined!");
        }

        /// <summary>
        /// Returns the evalulation of the condition.
        /// </summary>
        /// <param name="compareTo">The posssible list of neurons that must be found through the condition.</param>
        /// <param name="expression">The expression to evaluate</param>
        /// <param name="index">The index of the expression in the condition that is being evaluated.  This is provided for the
        /// debugger.</param>
        /// <returns></returns>
        /// <remarks>
        /// If there is a <b>compareTo</b> value specified, a case statement is presumed, and all the
        /// neurons in the compareTo must also be in Condition (after solving a possible expression) and no more.
        /// If compareTo is null, a boolean expression is presumed and it's result is returned or an error logged
        /// and false returned.
        /// When the condition is empty, true is always presumed.
        /// </remarks>
        protected internal virtual bool EvaluateCondition(IEnumerable<Neuron> compareTo, ConditionalExpression expression, int index)
        {
            if (compareTo == null)
            {
                ResultExpression iCond = expression.Condition;
                if (iCond != null)
                {
                    IEnumerable<Neuron> iRes = iCond.GetValue(this);
                    foreach (Neuron i in iRes)
                    {
                        if (i != null && i.ID == (ulong)PredefinedNeurons.False)
                            return false;
                    }
                }
                return true;
            }
            else
            {
                IEnumerable<Neuron> iCond = Neuron.SolveResultExp(expression.Condition, this);
                return compareTo.SequenceEqual(iCond);
            }
        }

        #endregion Conditional flow

        #region Control flow

        /// <summary>
        /// Stops the entire <see cref="Processor.Solve"/> process so that a result node can be returned.
        /// </summary>
        public virtual void Exit()
        {
            ClearFrames();
            CurrentState = State.Exit;
        }

        /// <summary>
        /// goes to the end of the code for solving the current link so that the function stops and continues with the next link.
        /// </summary>
        internal void ExitLink()
        {
            ClearFrames();
        }

        /// <summary>
        /// Exits the current call frame (the cotinue statement).
        /// </summary>
        /// <remarks>
        /// Is done by moving the position of the current frame to the end.
        /// </remarks>
        internal void ExitBranch()
        {
            if (CallFrameStack.Count > 0)
            {
                CallFrame iFrame = CallFrameStack.Peek();
                iFrame.NextExp = int.MaxValue;
            }
            else
                throw new InvalidOperationException("No frames on call stack.");
        }

        /// <summary>
        /// Exits the current conditional (the break statement).
        /// </summary>
        /// <remarks>
        /// This is done by
        /// </remarks>
        internal void ExitConditional()
        {
            if (CallFrameStack.Count > 0)
            {
                CallFrame iLast = PopFrame();                                                             //a break statement should be used in one of the conditional parts.
                while (!(iLast is ForEachCallFrame || iLast is UntilCallFrame || iLast is LoopedCallFrame))
                {
                    if (CallFrameStack.Count > 0)
                        iLast = PopFrame();
                    else
                        throw new InvalidOperationException("Break statement used outside of a conditional.");
                }
            }
            else
                throw new InvalidOperationException("Break statement used outside of a conditional.");
        }

        /// <summary>
        /// Stops solving the current neuron.
        /// </summary>
        internal void ExitNeuron()
        {
            ClearFrames();                                                                  //clearing the entire stack will stop executing the link.
            CurrentState = State.ExitNeuron;                                                          //this stops the entire neuron.
        }

        #endregion Control flow

        #region Sub processor

        /// <summary>
        /// Returns an array of the specified size, filled with processors.
        /// </summary>
        /// <param name="count">The nr of processors to create.</param>
        /// <returns>An array of newly created processors (not initialized).</returns>
        /// <remarks>
        /// Descendents should reimplement this so they can give another type (for debugging).
        /// </remarks>
        public virtual Processor[] CreateProcessors(int count)
        {
            Processor[] iRes = new Processor[count];
            for (int i = 0; i < count; i++)
                iRes[i] = new Processor();
            return iRes;
        }

        /// <summary>
        /// Clones this processor to the specified processors, thereby letting the subprocessors know they are clones.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="iRes">The i res.</param>
        internal void CloneProcessors(Processor[] iRes)
        {
            int iNrOfClones = Count;
            List<Neuron> iTemp = new List<Neuron>(fNeuronStack.Reverse());                                               //we need a temp list cause all copy functions of stack, do a copy last in, first out, so similar as pop order.
            List<CallFrame> iFrames = new List<CallFrame>(CallFrameStack.Reverse());                           //same as NeuronStack.
            foreach (Processor i in iRes)
            {
                //we don't need to copy over execution data cause this
                //is known through the current expression that is link that
                //is evaluated.
                i.SplitData = new SplitData();                                                            //we need to create the split data in order to indicate it is a clone.
                i.SplitData.Processor = i;
                i.CloneCurrentPos(this);
                i.CloneCallStack(iFrames);
                i.CloneNeuronStack(iTemp);
                List<Dictionary<ulong, List<Neuron>>> iTempDicts = new List<Dictionary<ulong, List<Neuron>>>(VariableValues);  //need to make a copy of the stack so we preserve the order of the stack (all 'copy' functions of the stack change the order).
                i.CloneGlobals(this, GlobalValues);
                i.CloneVariables(iTempDicts);                                                             //must be done after all clones have been made, otherwise, variable contents can't be updated correctly.
                i.SplitValues.InitFrom(SplitValues);
            }
        }

        /// <summary>
        /// Clones the global values to this dictionary, taking into account the type of cloning specified in the global.
        /// </summary>
        /// <param name="source">The source.</param>
        private void CloneGlobals(Processor from, Dictionary<ulong, List<Neuron>> source)
        {
            foreach (KeyValuePair<ulong, List<Neuron>> i in source)
            {
                Global iGlobal = Brain.Current[i.Key] as Global;
                if (iGlobal != null)
                {
                    Neuron iSplitReaction = iGlobal.SplitReaction;
                    if (iSplitReaction != null)
                    {
                        if (iSplitReaction.ID == (ulong)PredefinedNeurons.Copy)
                            CopyGlobal(from, iGlobal);
                        else if (iSplitReaction.ID == (ulong)PredefinedNeurons.Duplicate)
                            DuplicateGlobal(from, iGlobal);
                    }
                    //in all other cases, we leave the global empty so that a new value is created.
                }
            }
        }

        /// <summary>
        /// Duplicates the content of the global and stores it in the global values dict.
        /// </summary>
        /// <param name="iGlobal">The i global.</param>
        private void DuplicateGlobal(Processor from, Global value)
        {
            IEnumerable<Neuron> iList = value.GetValue(from);
            List<Neuron> iNewVals = new List<Neuron>();
            foreach (Neuron i in iList)
            {
                if (i.ID != Neuron.EmptyId)
                {
                    Neuron iClone;
                    if (SplitData.Clones.TryGetValue(i.ID, out iClone) == false)                     //if the item has already been cloned, don't need to do it again.
                        iClone = i.Duplicate();
                    SplitData.Clones.Add(i.ID, iClone);                                            //we need to store all the copies as well.
                    iNewVals.Add(iClone);
                }
            }
            value.StoreValue(iNewVals, this);
        }

        /// <summary>
        /// Copies the content of the global, as defined in another processor to this one..
        /// </summary>
        /// <param name="from">The processor to copy the values from.</param>
        /// <param name="value">The global for whom to copy the content.</param>
        private void CopyGlobal(Processor from, Global value)
        {
            IEnumerable<Neuron> iList = value.GetValue(from);
            List<Neuron> iNewVals = new List<Neuron>();
            foreach (Neuron i in iList)                                                                     //need to make certain that any references to cloned neurons are resolved.
                iNewVals.Add(Brain.Current[i.ID]);
            value.StoreValue(iNewVals, this);
        }

        private void CloneCurrentPos(Processor processor)
        {
            NeuronToSolve = processor.NeuronToSolve.Duplicate();                                            //we make a duplicate of the currently solving neuron cause we might change it in the code, which needs to be local.
            SplitData.Clones.Add(processor.NeuronToSolve.ID, NeuronToSolve);                                //we need to keep ref of the cloning process.
            CurrentTo = processor.CurrentTo;
            CurrentInfo = processor.CurrentInfo.ToList();                                                            //the info is also the same, but we must copy the list, otherwise we might loose the contents.
            CurrentLink = processor.CurrentLink;
            CurrentSin = processor.CurrentSin;
            CurrentMeaning = processor.CurrentMeaning;
            CurrentLinks = processor.CurrentLinks;
        }

        private void CloneVariables(List<Dictionary<ulong, List<Neuron>>> iTempDicts)
        {
            for (int counter = iTempDicts.Count - 1; counter >= 0; counter--)
            {
                Dictionary<ulong, List<Neuron>> iNew = new Dictionary<ulong, List<Neuron>>();
                foreach (KeyValuePair<ulong, List<Neuron>> i in iTempDicts[counter])                               //we need to make certain that the content of the variables is updated so that they contain the clones of the stack content.
                {
                    List<Neuron> iVal = new List<Neuron>();
                    foreach (Neuron iNeuron in i.Value)
                    {
                        if (iNeuron.ID != Neuron.EmptyId)
                        {
                            Neuron iFound;
                            if (SplitData.Clones.TryGetValue(iNeuron.ID, out iFound) == true)
                                iVal.Add(iFound);
                            else
                                iVal.Add(iNeuron);
                        }
                    }
                    iNew.Add(i.Key, iVal);
                }
                VariableValues.Push(iNew);
            }
        }

        /// <summary>
        /// Clones the content of the specified stack (provided as a list).
        /// </summary>
        /// <param name="iTemp">The i temp.</param>
        private void CloneNeuronStack(List<Neuron> iTemp)
        {
            foreach (Neuron iNeuron in iTemp)
            {
                Neuron iClone;
                if (SplitData.Clones.TryGetValue(iNeuron.ID, out iClone) == false)
                {
                    iClone = iNeuron.Duplicate();
                    SplitData.Clones.Add(iNeuron.ID, iClone);                                            //we need to store all the copies as well.
                }
                Push(iClone);
            }
        }

        /// <summary>
        /// Clones the call stack.
        /// </summary>
        /// <param name="list">The list of frames to clone.</param>
        private void CloneCallStack(List<CallFrame> list)
        {
            foreach (CallFrame i in list)
            {
                CallFrame iNew = i.Duplicate();
                PushFrame(iNew);
            }
        }

        /// <summary>
        /// Copies the content of the SplitValues to the specified list. Warning: list should already be locked for
        /// writing, this function doesn't do this.
        /// </summary>
        /// <remarks>
        /// Called when a subprocessor is ready and it's result must be copied over.
        /// </remarks>
        /// <param name="list">The neuron list.</param>
        internal void CopyResultsTo(ChildrenAccessor list)
        {
            foreach (KeyValuePair<ulong, int> i in SplitValues)
                list.Add(Brain.Current[i.Key]);
        }

        /// <summary>
        /// Copies the content of this processor to the specified processor. Warning: list should already be locked for
        /// writing, this function doesn't do this.
        /// </summary>
        /// <param name="to">Processor to copy these split values to.</param>
        internal void CopyResultsTo(Processor to)
        {
            SplitResultsDict iTo = to.SplitValues;
            int iItem;
            foreach (KeyValuePair<ulong, int> i in SplitValues)
            {
                if (iTo.TryGetValue(i.Key, out iItem) == true)
                    iTo[i.Key] = Math.Max(iItem, i.Value);
                else
                    iTo.Add(i.Key, i.Value);
            }
        }

        /// <summary>
        /// Checks if we are a sub processor and if so, if we creted a clone for the
        /// specified neuron, if so, we return the local copy.
        /// </summary>
        /// <param name="id">The id of the neuron we which to check for clone</param>
        /// <param name="res">The local copy (clone) of the neuron.</param>
        /// <returns></returns>
        internal bool TryFindNeuron(ulong id, out Neuron res)
        {
            res = null;
            if (SplitData != null)
                return SplitData.Clones.TryGetValue(id, out res);
            return false;
        }

        #endregion Sub processor

        #endregion Functions
    }
}