﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Contains all the data specific for a processor that is taking part in a split.
    /// </summary>
    internal class SplitData
    {
        /// <summary>
        /// A dictionary containing all the clones that were made for this stack.
        /// </summary>
        /// <remarks>
        /// This is a dictionary since it is mostly used for looking up neurons, to see if
        /// they are cloned.
        /// </remarks>
        public Dictionary<ulong, Neuron> Clones = new Dictionary<ulong, Neuron>();

        /// <summary>
        /// Gets or sets the object that manages the split.
        /// </summary>
        /// <value>The head.</value>
        public HeadData Head { get; set; }

        /// <summary>
        /// Gets or sets the processor to which this data item belongs.
        /// </summary>
        /// <value>The processor.</value>
        public Processor Processor { get; set; }
    }
}