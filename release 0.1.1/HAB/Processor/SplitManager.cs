﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// This class is responsible for managing <see cref="Processor"/>s that have been split into many different sub processors.
    /// </summary>
    /// <remarks>
    /// <para>
    /// To let the sub processors run in parallel and let them come back to 1 point when they are done, we use the technique
    /// of chaining 2 function calls after each other.  So every sub processor gets a callback ref to the splitmanager
    /// (<see cref="SplitManager.SubProcessorFinished"/>) that is called when the sub processor is done.  This checks if all processors
    /// are done and only if that condition is satisfied, allows the last processor to continue.
    /// </para>
    /// </remarks>
    internal class SplitManager
    {
        #region Inner types

        /// <summary>
        /// Arguments for the <see cref="SplitManager.Split"/>
        /// </summary>
        internal class SplitArgs
        {
            private List<Neuron> fToSplit = new List<Neuron>();

            /// <summary>
            /// Gets or sets the list of neurons on which we need to split.
            /// </summary>
            /// <remarks>
            /// This should also include the value that should be used for the current processor.
            /// </remarks>
            public List<Neuron> ToSplit
            {
                get
                {
                    return fToSplit;
                }
            }

            /// <summary>
            /// Gets or sets the variable that will store the split result in each processor.
            /// </summary>
            /// <value>The variable.</value>
            public Variable Variable { get; set; }

            /// <summary>
            /// Gets or sets the neuroncluster with the callback code that needs to be called when the split is done.
            /// </summary>
            /// <value>The callback.</value>
            public NeuronCluster Callback { get; set; }

            /// <summary>
            /// Gets or sets the Neuroncluster that will store all the different possible results.
            /// </summary>
            /// <value>The result list.</value>
            public NeuronCluster ResultList { get; set; }

            /// <summary>
            /// Gets or sets the processor for which a split needs to be performed.
            /// </summary>
            /// <value>The processor.</value>
            public Processor Processor { get; set; }
        }

        #endregion Inner types

        #region Fields

        private static SplitManager fDefault;

        #endregion Fields

        #region Default

        /// <summary>
        /// Gets the default splitter for processors.
        /// </summary>
        /// <value>The default.</value>
        static public SplitManager Default
        {
            get
            {
                if (fDefault == null)
                    fDefault = new SplitManager();
                return fDefault;
            }
        }

        #endregion Default

        #region Functions

        /// <summary>
        /// Splits a processor into many different processors of the same kind who have the same stack content
        /// as the original one (cloned). Each sub processor is started.
        /// </summary>
        /// <remarks>
        /// <para>
        /// This technique is used when there are many different possible results valid while solving a <see cref="Neuron"/>
        /// and different paths need to be walked untill only 1 valid remains (or not).
        /// </para>
        /// <para>
        /// The processor's stack gets cloned to each sub processor. Cloned Neurons are deep copies of an original.  That is, they have a
        /// different ID but the same list contents (links, children, values). the  neurons to which those lists point are not cloned.
        /// Cloning is important, cause during a split you often want to check different possiblities for the same values (for instance,
        /// did the word 'cup' mean cup of tea or world cup). This is done by constructing a KnoledgeNeuron (where, what, when,...) which
        /// should already be on the stack, so this gets duplicated.
        /// </para>
        /// <para>
        /// A processor can be split multiple times. For as long as the processor or one of it's sub processors is split with the same
        /// callback function and result list, the initial split is extended so it will also contain the newly created sub processors.
        /// If a new callback is created, the joins are stacked, that is, the last split must be completely solved untill the split in front
        /// of it can be solved.
        /// </para>
        /// </remarks>
        /// <exception cref="BrainException">When the processor is already the head of a split.</exception>
        /// <param name="toStart"></param>
        public void Split(SplitArgs args)
        {
            lock (Brain.Current)                                                                            //we put a lock on this because we can otherwise have a deadlock with code that does try to get a lock on that while we are locking one of of accessors.
            {
                Dictionary<ulong, List<Neuron>> iDict;
                if (args.ToSplit.Count > 1)
                {
                    List<SplitData> iSubs = CreateProcessors(args);
                    HeadData iHead = PrepareRequestor(args, iSubs);
                    for (int i = 0; i < iSubs.Count; i++)
                    {
                        iSubs[i].Head = iHead;
                        Processor iProc = iSubs[i].Processor;
                        iDict = iProc.VariableValues.Peek();
                        iDict[args.Variable.ID] = new List<Neuron>() { args.ToSplit[i] };
                        Action iAction = new Action(iProc.SolveStackAfterSplit);
                        iAction.BeginInvoke(null, null);
                    }
                }
                iDict = args.Processor.VariableValues.Peek();                                                //the last value to split on gets assigned to the calling processor.
                Neuron iLastArg = args.ToSplit[args.ToSplit.Count - 1];
                iDict[args.Variable.ID] = new List<Neuron>() { iLastArg };                                   //need to store the 1 item on which we need to split into the var that was supplied.
            }
        }

        /// <summary>
        /// Finishes the split for the specified split data.  If this is the last, the callback
        /// is called.
        /// </summary>
        /// <param name="args">The args.</param>
        /// <returns>True if the callback was called, otherwise false.</returns>
        internal bool FinishSplit(SplitData args)
        {
            HeadData iHead = args.Head;
            bool iRes = false;
            lock (iHead)                                                                             //we need a lock cause it can be modified from multiple threads (substraced and added when a new split is done)
            {
                iHead.StillActive--;
                if (iHead.StillActive == 0)                                                             //we are done.
                {
                    if (iHead.Requestor != args)
                        iHead.Requestor.Processor.CopyResultsTo(args.Processor);
                    foreach (SplitData i in iHead.SubProcessors)
                    {
                        if (i != args)
                            i.Processor.CopyResultsTo(args.Processor);                                       //store all the split values into the processor that will run the callback so that the weights can be accessed.
                    }
                    using (ChildrenAccessor iList = iHead.ResultList.ChildrenW)
                    {
                        iList.Clear();
                        args.Processor.CopyResultsTo(iList);                                             //also need to copy the result to the result list.
                    }
                    args.Processor.Call(iHead.Callback);                                                //and ask to solve the results. We do a call (so with new vars on the processor) cause the 'join' routine should not have values from the previous processing.
                    iRes = true;
                    if (iHead.Previous != null)
                    {
                        lock (iHead.Previous.Requestor.Head)
                            iHead.Previous.Requestor.Head = iHead.Previous;
                        FinishSplit(iHead.Previous.Requestor);
                    }
                    else
                        args.Processor.SplitData = null;                                                 //we must indicate that the split is over when there is no previous head.
                }
                else
                    args.Processor.SplitData = null;                                                    //we must indicate that no longer split (not last one, which most be handled differently).
            }
            return iRes;
        }

        /// <summary>
        /// Creates or searches for the correct head data that can be used given the specified arguments
        /// and assigns it to the requesting processor. Also makes certain that the requesting processor has
        /// split data.
        /// </summary>
        /// <remarks>
        /// An already existing headData item can be reused if we are trying to split an already split processor
        /// which uses the same callback and result list neurons (because we can use the same callback and list).
        /// </remarks>
        /// <param name="args">The arguments to check while searching/creating the headData.</param>
        /// <param name="subs">The sub processors that should be assigned to the head.</param>
        /// <returns>
        /// A new head data object containing all the data required managing the split, or an already existing
        /// item which can be reused and to which the sub processors have been added.
        /// </returns>
        private HeadData PrepareRequestor(SplitArgs args, List<SplitData> subs)
        {
            SplitData iPrevSplit = args.Processor.SplitData;
            if (iPrevSplit != null &&                                                           //the calling proc is already part of a split, so we need to check if we can reuse the head or if we need to create a new one.
               iPrevSplit.Head.Callback == args.Callback &&
               iPrevSplit.Head.ResultList == args.ResultList)
            {
                lock (iPrevSplit.Head)
                {
                    iPrevSplit.Head.StillActive += subs.Count;                                 //we can't add 1 more here, as we do for the init setup.  The current thread is already included in the count.
                    iPrevSplit.Head.SubProcessors.AddRange(subs);
                    return iPrevSplit.Head;
                }
            }
            else
            {
                HeadData iHeadData = new HeadData();
                iHeadData.StillActive = subs.Count + 1;                                     //this value is assigned to the subdata of the head processor (this one).  + 1 cause we also need to take this processor into account.
                iHeadData.SubProcessors = subs;                                                  //this indicates that this processor becomes a head.
                iHeadData.Callback = args.Callback;
                iHeadData.ResultList = args.ResultList;
                if (iPrevSplit != null)
                {
                    lock (iPrevSplit.Head)
                        iPrevSplit.Head.Previous = iHeadData;
                }
                else
                {
                    args.Processor.SplitData = new SplitData();
                    args.Processor.SplitData.Processor = args.Processor;
                }
                iHeadData.Requestor = args.Processor.SplitData;
                args.Processor.SplitData.Head = iHeadData;
                return iHeadData;
            }
        }

        /// <summary>
        /// Creates sub processors and provides all the data for them so that they are a duplicate of this one.
        /// </summary>
        /// <param name="args">
        /// Determins how many processors, from where to copy,...
        /// </param>
        /// <returns>The processors that were created wrapped into SplitData.</returns>
        private List<SplitData> CreateProcessors(SplitArgs args)
        {
            int iCount = args.ToSplit.Count - 1;
            Processor iProc = args.Processor;

            Processor[] iSubs = iProc.CreateProcessors(iCount);
            iProc.CloneProcessors(iSubs);
            List<SplitData> iRes = (from i in iSubs select i.SplitData).ToList();
            return iRes;
        }

        #endregion Functions
    }
}