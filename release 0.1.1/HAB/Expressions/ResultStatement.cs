﻿using System.Collections.Generic;
using System.Text;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// A <see cref="ResultExpression"/> that retrieves the result from an instruction (like IndexOf).
    /// </summary>
    /// <remarks>
    /// We use a general purpose expression that maps to instructions and don't create custom expressions
    /// directly (for instance, we could have made an IndexOfExpression to get the index of an item), except
    /// than you can't easely extend it without changing the core.  Instructions could be loaded from another
    /// lib, but this can also be done with expressions?
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.ResultStatement, typeof(Neuron))]
    public class ResultStatement : SimpleResultExpression
    {
        /// <summary>
        /// default constructor
        /// </summary>
        public ResultStatement()
        {
        }

        /// <summary>
        /// Creates the proper initial links + registers the object with the brain.
        /// </summary>
        /// <param name="inst">The instruction to execute.</param>
        /// <param name="args">The list of arguments for the instruction.</param>
        public ResultStatement(Instruction inst, NeuronCluster args)
        {
            Brain.Current.MakeTemp(this);                                                                      //we need to be registered to the brain, otherwise the link wont' create.
            Link iNew = new Link(inst, this, (ulong)PredefinedNeurons.Instruction);
            iNew = new Link(args, this, (ulong)PredefinedNeurons.Arguments);
        }

        #region Instruction

        /// <summary>
        /// Gets/sets the <see cref="Neuron"/> of the instruction set item to execute. This
        /// can not be a <see cref="ResultExpression"/>, only real instruction references are allowed.
        /// </summary>
        /// <remarks>
        /// This is usually a reference to the 1st linked object in the <see cref="Neuron.ToLinks"/>.
        /// </remarks>
        public ResultInstruction Instruction
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.Instruction) as ResultInstruction;
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Instruction, value);
            }
        }

        #endregion Instruction

        #region Arguments

        /// <summary>
        /// Gets the list containing all the argument values for the instruction.  These are
        /// <see cref="Neuron.ID"/> values that should point to <see cref="ResultExpression"/> objects.
        /// </summary>
        /// <remarks>
        /// This is a reference to the 2th linked object in the <see cref="Neuron.ToLinks"/>, which should be a cluster.
        /// </remarks>
        public NeuronCluster ArgumentsCluster
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.Arguments) as NeuronCluster;
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Arguments, value);
            }
        }

        #endregion Arguments

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ResultStatement"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.ResultStatement];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            StringBuilder iRes = new StringBuilder();
            Neuron iTemp = Instruction;
            if (iTemp != null)
                iRes.Append(iTemp.ToString());
            else
                iRes.Append("?");                          //we format it with an error sign indicating we didn't find it, error log already done.

            iRes.Append("(");
            NeuronCluster iArgs = ArgumentsCluster;
            if (iArgs != null)
            {
                using (ChildrenAccessor iArgsList = iArgs.Children)
                {
                    for (int i = 0; i < iArgsList.Count; i++)
                    {
                        if (i > 0)
                            iRes.Append(',');
                        iTemp = Brain.Current[iArgsList[i]];
                        if (iTemp != null)
                            iRes.Append(iTemp.ToString());
                        else
                            iRes.Append(string.Format("(!{0}!)", i.ToString()));
                    }
                }
            }
            iRes.Append(")");
            return iRes.ToString();
        }

        /// <summary>
        /// Calcules the result of this expression and returns this as a list.
        /// </summary>
        /// <param name="processor"></param>
        /// <returns>
        /// A list containing all the valid results for the expression.
        /// </returns>
        public override IEnumerable<Neuron> GetValue(Processor processor)
        {
            IEnumerable<Neuron> iRes = null;
            ResultInstruction iInst = Instruction;
            if (iInst != null)
            {
                NeuronCluster iOrArgs = ArgumentsCluster;
                int iRequiredCount;
                List<Neuron> iArgs;
                if (iOrArgs != null)
                {
                    using (ChildrenAccessor iList = iOrArgs.Children)
                    {
                        iArgs = iList.ConvertTo<Neuron>();
                        iRequiredCount = iList.Count;
                    }
                    if (iArgs == null)
                        Log.LogError("ResultStatement.Execute", string.Format("Failed to execute The InstructionId: {0}. Arguments couldn't be converted .", iInst));
                }
                else
                {
                    iArgs = new List<Neuron>();                                                                  //we need to create an empty list to indicate no args.
                    iRequiredCount = 0;
                }

                if (iArgs.Count == iRequiredCount)                                                              //only execute the statement if we have all the arguments.
                {
                    iArgs = ResultExpression.Solve(iArgs, processor);                                            //need to make certain that all expressions have been solved.
                    iRes = iInst.GetValues(processor, iArgs);
                }
            }
            else
                Log.LogError("ResultStatement.Execute", string.Format("Failed to execute The InstructionId '{0}' doesn't map to an instruction object.", Instruction));
            return iRes;
        }
    }
}