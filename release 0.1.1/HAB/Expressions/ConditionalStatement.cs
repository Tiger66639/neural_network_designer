﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// A conditional statement (to compare with if-then-else + do-while + while-do + repeat + for + foreach.
    /// </summary>
    /// <remarks>
    /// Uses the same conditional technique as gene.
    /// <para>
    /// Doesn't do a for loop, this can easely be done with a while loop.
    /// </para>
    /// <para>
    /// - For each loop:
    /// 1 condition expected which should return the list of items to walk through, it's statements are executed.
    /// The item that is currently processed, should be defined with the <see cref="ConditionalGroup.LoopItem"/>
    /// expression.
    /// </para>
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.ConditionalStatement, typeof(Neuron))]
    public class ConditionalStatement : Expression
    {
        #region ctor

        /// <summary>
        /// Default constructor
        /// </summary>
        public ConditionalStatement()
        {
        }

        /// <summary>
        /// Creates a new ConditionalExpression, with the proper links already made.
        /// </summary>
        /// <param name="conditions">The list of conditional expressions to evaluate.</param>
        /// <param name="loopstyle">The type of looping that should be used with this group.</param>
        public ConditionalStatement(NeuronCluster conditions, Neuron loopstyle)
        {
            Link iNew = new Link(conditions, this, (ulong)PredefinedNeurons.Condition);
            iNew = new Link(loopstyle, this, (ulong)PredefinedNeurons.LoopStyle);
        }

        #endregion ctor

        #region Conditions

        /// <summary>
        /// Gets the list of conditions that are contained in this group, as conditional expressions.
        /// </summary>
        /// <remarks>
        /// To edit this list, use the <see cref="Neuron.LinksTo"/> list.
        /// </remarks>
        public ReadOnlyCollection<ConditionalExpression> Conditions
        {
            get
            {
                NeuronCluster iCluster = ConditionsCluster;
                if (iCluster != null)
                {
                    using (ChildrenAccessor iList = iCluster.Children)
                    {
                        List<ConditionalExpression> iExps = iList.ConvertTo<ConditionalExpression>();
                        if (iExps != null)
                            return new ReadOnlyCollection<ConditionalExpression>(iExps);
                        else
                            Log.LogError("ConditionalGroup.Conditions", string.Format("Failed to convert Conditions list of '{0}' to an executable list.", this));
                    }
                }
                return null;
            }
        }

        #endregion Conditions

        #region ConditionsCluster

        /// <summary>
        /// Gets the <see cref="NeuronCluster"/> used to store all the sub statements of this conditional expression.
        /// </summary>
        public NeuronCluster ConditionsCluster
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.Condition) as NeuronCluster;
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Condition, value);
            }
        }

        #endregion ConditionsCluster

        #region LoopStyle

        /// <summary>
        /// Gets/sets the type of looping applied to this conditional groups.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Possible values are: Not, Case, Looped, CaseLooped, ForEach, For, Until
        /// </para>
        /// <para>
        /// This can be a result expression that needs to be solved.  Only a single value
        /// result is allowed.
        /// </para>
        /// </remarks>
        public Neuron LoopStyle
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.LoopStyle);
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.LoopStyle, value);
            }
        }

        #endregion LoopStyle

        #region LoopItem

        /// <summary>
        /// Gets/sets the <see cref="Variable"/> used to store the current value of a for or
        /// foreach loop. Only has to be set if this is a foreach loop.
        /// </summary>
        public Variable LoopItem
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.LoopItem) as Variable;
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.LoopItem, value);
            }
        }

        #endregion LoopItem

        #region CaseItem

        /// <summary>
        /// Gets/sets the <see cref="Variable"/> used to define the item or list of
        /// neurons to compare against in a case statemnt.  This value is only required
        /// if it is a case or looped case.
        /// </summary>
        public Variable CaseItem
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.CaseItem) as Variable;
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.CaseItem, value);
            }
        }

        #endregion CaseItem

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ConditionalStatement"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.ConditionalStatement];
            }
        }

        #endregion TypeOfNeuron

        #region Functions

        /// <summary>
        /// Only 1 loop style is allowed.
        /// </summary>
        /// <param name="handler"></param>
        protected internal override void Execute(Processor handler)
        {
            Neuron iLoopStyle = Neuron.SolveSingleResultExp(LoopStyle, handler);
            if (iLoopStyle != null)
                handler.EvaluateConditional(iLoopStyle, this);
            else
                Log.LogError("ConditionalGroup.Execute", string.Format("'{0}' is not/does not produce a valid loop style.", LoopStyle));
        }

        public override string ToString()
        {
            return "Conditional: " + ID.ToString();
        }

        #endregion Functions
    }
}