﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// A container for expressions.
    /// </summary>
    /// <remarks>
    /// Use this type of expression to group a sequence of expressions together so that they can be
    /// reused together as a single unit, in other code blocks.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.ExpressionsBlock, typeof(Neuron))]
    public class ExpressionsBlock : Expression
    {
        #region StatementsCluster

        /// <summary>
        /// Gets the <see cref="NeuronCluster"/> used to store all the sub statements of this conditional expression.
        /// </summary>
        public NeuronCluster StatementsCluster
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.Statements) as NeuronCluster;
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Statements, value);
            }
        }

        #endregion StatementsCluster

        #region Statements

        /// <summary>
        /// Gets a readonly list with <see cref="Expression"/>s that are the convertion of <see cref="ExpressionsBlock.StatementCluster"/>.
        /// </summary>
        public ReadOnlyCollection<Expression> Statements
        {
            get
            {
                NeuronCluster iCluster = StatementsCluster;
                if (iCluster != null)
                {
                    using (ChildrenAccessor iList = iCluster.Children)
                    {
                        List<Expression> iExps = iList.ConvertTo<Expression>();
                        if (iExps != null)
                            return new ReadOnlyCollection<Expression>(iExps);
                        else
                            Log.LogError("ExpressionsBlock.Statements", string.Format("Failed to convert Statements list of '{0}' to an executable list.", this));
                    }
                }
                return null;
            }
        }

        #endregion Statements

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ExpressionsBlock"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.ExpressionsBlock];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return "EvaluateBlock(" + base.ToString() + ")";
        }

        /// <summary>
        /// Performs the sub statements.
        /// </summary>
        /// <param name="handler"></param>
        /// <returns></returns>
        protected internal override void Execute(Processor handler)
        {
            //handler.Call(StatementsCluster);
            NeuronCluster iStatements = StatementsCluster;
            if (iStatements != null)
            {
                CallFrame iFrame = new CallFrame(StatementsCluster);
                handler.PushFrame(iFrame);
            }
        }
    }
}