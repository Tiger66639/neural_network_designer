﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// A variable implementation that returns the current info section of the link that is being executed.
    /// </summary>
    [NeuronID((ulong)PredefinedNeurons.CurrentInfo)]
    public class CurrentInfo : SystemVariable
    {
        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="CurrentInfo"/> class.
        /// </summary>
        public CurrentInfo()
        {
        }

        #endregion ctor

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="proc">The proc.</param>
        /// <returns></returns>
        public override IEnumerable<Neuron> GetValue(Processor proc)
        {
            return proc.CurrentInfo;
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return "CurrentInfo";
        }
    }
}