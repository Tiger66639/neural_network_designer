﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// An expression that returns a result.
    /// </summary>
    /// <remarks>
    /// A result expression is used by other expressions to retrieve argument values.
    /// Results can be from a search or a Predefined node.
    /// </remarks>

    public abstract class ResultExpression : Expression
    {
        /// <summary>
        /// Calcules the result of this expression and returns this as a list.
        /// </summary>
        /// <returns>A list containing all the valid results for the expression.</returns>
        public abstract IEnumerable<Neuron> GetValue(Processor processor);

        /// <summary>
        /// Returns a list with all the neurons of the input list, except the <see cref="ResultExpressions"/>, which
        /// are replaced with their results.
        /// </summary>
        /// <param name="toConvert">The list to convert all the result expressions from.</param>
        /// <returns>A list of neurons.</returns>
        public static List<Neuron> Solve(IList<Neuron> toConvert, Processor processor)
        {
            List<Neuron> iRes = new List<Neuron>();
            foreach (Neuron i in toConvert)
            {
                ResultExpression iExp = i as ResultExpression;
                if (iExp != null)
                    iRes.AddRange(iExp.GetValue(processor));
                else
                    iRes.Add(i);
            }
            return iRes;
        }
    }
}