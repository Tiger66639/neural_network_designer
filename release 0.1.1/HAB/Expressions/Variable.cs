﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// A Variable result expression always returns a specific neuron (list). It's initial value is specified in <see cref="Variable.Value"/>.
    /// </summary>
    /// <remarks>
    /// <para>
    /// This type of expression can be used to reference somethng and who's reference can change during execution.
    /// </para>
    /// <para>
    /// You can also use a variable expression to encapsulate other expressions if you want to work on the expression object itself
    /// instead of executing it.  For instance, if you want to do a search in the actions lists of a result expression, it will be
    /// executed and it's result will be searched.  To prevent this, incapsulate it in this type of expression.
    /// </para>
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.Variable, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Value)]
    public class Variable : SimpleResultExpression
    {
        #region ctor

        /// <summary>
        /// A constructor that initializes the object with a reference to the specified <see cref="Neuron"/>.
        /// </summary>
        /// <param name="value"></param>
        public Variable(Neuron value)
        {
            Link iNew = new Link(value, this, (ulong)PredefinedNeurons.Value);
        }

        /// <summary>
        /// default constructor.
        /// </summary>
        public Variable()
        {
        }

        #endregion ctor

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.Variable"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.Variable];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Gets/sets the initial value assigned to the variable if there is no value assigned when it is used. This
        /// can be an expression but  doesn't have to be.
        /// </summary>
        public Neuron Value
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.Value);
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Value, value);
            }
        }

        /// <summary>
        /// Gets the current 'processor local' value for the this variable.
        /// </summary>
        /// <remarks>
        /// This function is thread safe.
        /// </remarks>
        /// <param name="variable">The variable for which to return the current value.</param>
        /// <returns></returns>
        public override IEnumerable<Neuron> GetValue(Processor proc)
        {
            List<Neuron> iRes = null;
            if (proc.VariableValues.Count > 0)
            {
                Dictionary<ulong, List<Neuron>> iDict = proc.VariableValues.Peek();
                Debug.Assert(iDict != null);
                lock (iDict)
                {
                    if (iDict.TryGetValue(ID, out iRes) == false)
                    {
                        Neuron iValue = Value;
                        if (iValue != null)
                        {
                            iRes = Variable.SolveResultExp(iValue, proc).ToList();
                            iDict.Add(ID, iRes);
                        }
                    }
                    else
                        iRes.RemoveAll(i => i.ID == Neuron.EmptyId);                               //we need to clean out the list, make certain all neurons that got deleted, are removed of the list.
                }
            }
            if (iRes == null)
                iRes = new List<Neuron>();
            return iRes;
        }

        /// <summary>
        /// Stores the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="proc">The proc.</param>
        protected internal virtual void StoreValue(IEnumerable<Neuron> value, Processor proc)
        {
            Dictionary<ulong, List<Neuron>> iDict = proc.VariableValues.Peek();
            Debug.Assert(iDict != null);
            lock (iDict)
                iDict[this.ID] = value.ToList();
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return "var(" + ID.ToString() + ")";
        }
    }
}