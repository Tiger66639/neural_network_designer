﻿using System.Linq;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// An expression that assigns the right part (after evaluation) to the left part (without evaluation).
    /// This only works for <see cref="TextNeuron"/>, <see cref="IntNeuron"/>, <see cref="DoubleNeuron"/>
    /// or <see cref="Variable"/>s.
    /// </summary>
    /// <remarks>
    /// When from : textNeuron to TextNeuron -> copy text
    ///             IntNeuron to IntNeuron -> copy value
    ///             DoubleNeuron To DoubleNeuron -> copy value
    ///             Variable to Variable -> copy value of second var
    ///                                     Note: when <see cref="Expression.ByReference"/> is true, a ref to the right variable is stored in left.
    ///             Variable to Expression -> Execute expression + store result of expression /
    ///                                       Note: when <see cref="Expression.ByReference"/> is true, a ref to the expression is stored in the var.
    ///             variable to any other -> store reference of object in variable
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.Assignment, typeof(Neuron))]
    public class Assignment : Expression
    {
        #region LeftPart

        /// <summary>
        /// Gets/sets the left part of the expression.
        /// </summary>
        /// <remarks>
        /// <para>
        /// The left part is compared to the <see cref="BoolExpression.RightPart"/> using the <see cref="BoolExpresssion.Operator"/> (which
        /// must be of a known type) to produce a result.
        /// </para>
        /// <para>
        /// If this is an expression, it is first executed before the compare is done.
        /// </para>
        /// </remarks>
        public Neuron LeftPart
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.LeftPart);
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.LeftPart, value);
            }
        }

        #endregion LeftPart

        #region RightPart

        /// <summary>
        /// Gets/sets the right part of the expression.
        /// </summary>
        /// <remarks>
        /// The right part is compared to the <see cref="BoolExpression.LeftPart"/> using the <see cref="BoolExpresssion.Operator"/> (which
        /// must be of a known type) to produce a result.
        /// <para>
        /// If this is an expression, it is first executed before the compare is done.
        /// </para>
        /// </remarks>
        public Neuron RightPart
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.RightPart);
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.RightPart, value);
            }
        }

        #endregion RightPart

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.Assignment"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.Assignment];
            }
        }

        #endregion TypeOfNeuron

        #region Functions

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            Neuron iLeft = LeftPart;
            Neuron iRight = RightPart;
            return (iLeft != null ? iLeft.ToString() : "?") + " = " + (iRight != null ? iRight.ToString() : "?");
        }

        /// <summary>
        /// Inheriters should implement this function for performing the expression.
        /// </summary>
        /// <param name="handler">The processor to execute the statements on.</param>
        /// <remarks>
        /// Implementers don't have to increment the procoessor positions or anything (except for conditional statements offcourse),
        /// this is done by <see cref="Expression.Execute"/>
        /// </remarks>
        protected internal override void Execute(Processor handler)
        {
            Neuron iLeft = LeftPart;
            if (iLeft is TextNeuron)
            {
                TextNeuron iRight = SolveResultExp(RightPart, handler).FirstOrDefault() as TextNeuron;
                if (iRight != null)
                    ((TextNeuron)iLeft).Text = iRight.Text;
                else
                    Log.LogError("Assignment.Execute", string.Format("TextNeuron expected in right part."));
            }
            else if (iLeft is DoubleNeuron)
            {
                DoubleNeuron iRight = SolveResultExp(RightPart, handler).FirstOrDefault() as DoubleNeuron;
                if (iRight != null)
                    ((DoubleNeuron)iLeft).Value = iRight.Value;
                else
                    Log.LogError("Assignment.Execute", string.Format("DoubleNeuron expected in right part."));
            }
            else if (iLeft is IntNeuron)
            {
                IntNeuron iRight = SolveResultExp(RightPart, handler).FirstOrDefault() as IntNeuron;
                if (iRight != null)
                    ((IntNeuron)iLeft).Value = iRight.Value;
                else
                    Log.LogError("Assignment.Execute", string.Format("IntNeuron expected in right part."));
            }
            else if (iLeft is Variable)
            {
                Neuron iRight = RightPart;
                if (iRight is Variable)                                                                                  //if the right is a var, we store the value of the var into left.
                    ((Variable)iLeft).StoreValue(((Variable)iRight).GetValue(handler), handler);
                else
                {                                                                                                        //if right is not a var or it is byref, we ask it to be solved (solving will return the var itself it is byref).
                    if (iRight != null)
                        ((Variable)iLeft).StoreValue(SolveResultExp(iRight, handler), handler);
                    else
                        Log.LogError("Assignment.Execute", string.Format("Neuron expected in right part."));
                }
            }
            else
                Log.LogError("Assignment.Execute", string.Format("Invalid (unassignable) left part: {0}.", iLeft));
        }

        #endregion Functions
    }
}