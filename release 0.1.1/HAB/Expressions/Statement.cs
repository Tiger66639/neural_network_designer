﻿using System.Collections.Generic;
using System.Text;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Contains 1 instruction (expression) that the <see cref="Processor"/> can execute.  It contains a reference to
    /// the instruction object and the arguments.
    /// </summary>
    /// <remarks>
    ///
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.Statement, typeof(Neuron))]
    public class Statement : Expression
    {
        /// <summary>
        /// default constructor
        /// </summary>
        public Statement()
        {
        }

        /// <summary>
        /// Creates the proper initial links and registers it to the brain.
        /// </summary>
        /// <param name="inst">The instruction to execute.</param>
        /// <param name="args">The list of arguments for the instruction.</param>
        public Statement(Instruction inst, NeuronCluster args)
        {
            Brain.Current.Add(this);
            Link iNew = new Link(inst, this, (ulong)PredefinedNeurons.Instruction);
            iNew = new Link(args, this, (ulong)PredefinedNeurons.Arguments);
        }

        #region Instruction

        /// <summary>
        /// Gets/sets the <see cref="Neuron"/> of the instruction set item to execute. This
        /// can not be a <see cref="ResultExpression"/>, in which case it is first resolved, only
        /// real instruction references are allowed.
        /// </summary>
        /// <remarks>
        /// This is usually a reference to the 1st linked object in the <see cref="Neuron.ToLinks"/>.
        /// </remarks>
        public Instruction Instruction
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.Instruction) as Instruction;
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Instruction, value);
            }
        }

        #endregion Instruction

        #region Arguments

        /// <summary>
        /// Gets the list containing all the argument values for the instruction.  These are
        /// <see cref="Neuron.ID"/> values that should point to <see cref="ResultExpression"/> objects.
        /// </summary>
        /// <remarks>
        /// This is a reference to the 2th linked object in the <see cref="Neuron.ToLinks"/>, which should be a cluster.
        /// </remarks>
        public NeuronCluster ArgumentsCluster
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.Arguments) as NeuronCluster;
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Arguments, value);
            }
        }

        #endregion Arguments

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ExpressionsBlock"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.Statement];
            }
        }

        #endregion TypeOfNeuron

        /// <returns>A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.</returns>
        public override string ToString()
        {
            StringBuilder iRes = new StringBuilder();
            Neuron iTemp = Instruction;
            if (iTemp != null)
                iRes.Append(iTemp.ToString());
            else
                iRes.Append("(!EmptyRef!)");

            iRes.Append("(");
            NeuronCluster iArgs = ArgumentsCluster;
            if (iArgs != null)
            {
                using (ChildrenAccessor iArgsList = iArgs.Children)
                {
                    for (int i = 0; i < iArgsList.Count; i++)
                    {
                        if (i > 0)
                            iRes.Append(',');
                        iTemp = Brain.Current[iArgsList[i]];
                        if (iTemp != null)
                            iRes.Append(iTemp.ToString());
                        else
                            iRes.Append(string.Format("(!{0}!)", i.ToString()));
                    }
                }
            }
            iRes.Append(")");
            return iRes.ToString();
        }

        protected internal override void Execute(Processor handler)
        {
            Instruction iInst = Instruction;
            if (iInst != null)
            {
                NeuronCluster iOrArgs = ArgumentsCluster;
                int iRequiredCount;
                List<Neuron> iArgs;
                if (iOrArgs != null)
                {
                    using (ChildrenAccessor iList = iOrArgs.Children)
                    {
                        iArgs = iList.ConvertTo<Neuron>();
                        iRequiredCount = iList.Count;
                    }
                    if (iArgs == null)
                        Log.LogError("Statement.Execute", string.Format("Failed to execute The InstructionId: {0}. Arguments couldn't be converted .", iInst));
                }
                else
                {
                    iArgs = new List<Neuron>();                                                                  //we need to create an empty list to indicate no args.
                    iRequiredCount = 0;
                }
                if (iArgs.Count == iRequiredCount)                                                              //only execute the statement if we have all the arguments.
                {
                    iArgs = ResultExpression.Solve(iArgs, handler);                                              //need to make certain that searchexpressions and bool expressions have been solved.
                    iInst.Execute(handler, iArgs);
                }
            }
            else
                Log.LogError("Statement.Execute", string.Format("Failed to execute The InstructionId '{0}' doesn't map to an instruction object.", Instruction));
        }
    }
}