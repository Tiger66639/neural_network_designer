﻿using System.Text;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// This expression is always a part of a <see cref="ConditionalStatement"/>.  It represents a single branch
    /// of a conditional statement.
    /// </summary>
    /// <remarks>
    /// Execution of the statements of a conditional expression doesn't perform the evaluation to check if the statements need to be
    /// executed, they are always executed by calling this function.  To evaluate, call
    /// <see cref="ConditionalExpression.EvaluateCondition"/>. This is seperate cause we have now way of knowing
    /// how and when it should be evaluated.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.ConditionalPart, typeof(Neuron))]
    public class ConditionalExpression : ExpressionsBlock
    {
        #region ctor

        public ConditionalExpression()
        {
        }

        /// <summary>
        /// Creates a new ConditionalExpression, with the proper links already made.
        /// </summary>
        /// <param name="condition">The condition that determins if the statements are executed or not.</param>
        /// <param name="statements">The list of statements that are executed if the condition is evaluated to true.</param>
        public ConditionalExpression(Variable condition, NeuronCluster statements)
        {
            Link iNew = new Link(condition, this, (ulong)PredefinedNeurons.Condition);
            iNew = new Link(statements, this, (ulong)PredefinedNeurons.Statements);
        }

        #endregion ctor

        /// <summary>
        /// The condition to evaluate.  If this returns true, the <see cref="Expression.Statements"/> are executed.
        /// </summary>
        /// <remarks>
        /// This is a regular neuron and not a specific <see cref="ConditonalExpression"/> cause the condition can be anything.
        /// If the loop is a <see cref="PredefinedNeurons.WhileDo"/> or <see cref="PredefinedNeurons.DoWhile"/>, it should
        /// be a conditionalExpression, howeever, if it is cased, this can be a neuron, or another <see cref="ResultExpression"/>.
        /// </remarks>
        public ResultExpression Condition
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.Condition) as ResultExpression;
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Condition, value);
            }
        }

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ConditionalPart"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.ConditionalPart];
            }
        }

        #endregion TypeOfNeuron

        public override string ToString()
        {
            StringBuilder iStr = new StringBuilder("(");
            ResultExpression iCond = Condition;
            if (iCond != null)
                iStr.Append(iCond.ToString());
            iStr.AppendLine(")");

            return iStr.ToString();
        }
    }
}