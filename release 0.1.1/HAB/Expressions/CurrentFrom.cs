﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// A variable that returns the neuron in the from part of the link being executed.
    /// </summary>
    [NeuronID((ulong)PredefinedNeurons.CurrentFrom)]
    public class CurrentFrom : SystemVariable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CurrentFrom"/> class.
        /// </summary>
        public CurrentFrom()
        {
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="proc">The proc.</param>
        /// <returns></returns>
        public override IEnumerable<Neuron> GetValue(Processor proc)
        {
            List<Neuron> iRes = new List<Neuron>();
            iRes.Add(proc.NeuronToSolve);
            return iRes;
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return "CurrentFrom";
        }
    }
}