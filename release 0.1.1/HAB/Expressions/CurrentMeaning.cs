﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// A variable that returns the neuron in the meaning part of the link being executed.
    /// </summary>
    [NeuronID((ulong)PredefinedNeurons.CurrentMeaning)]
    public class CurrentMeaning : SystemVariable
    {
        public CurrentMeaning()
        {
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="proc">The proc.</param>
        /// <returns></returns>
        public override IEnumerable<Neuron> GetValue(Processor proc)
        {
            List<Neuron> iRes = new List<Neuron>();
            iRes.Add(proc.CurrentMeaning);
            return iRes;
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return "CurrentMeaning";
        }
    }
}