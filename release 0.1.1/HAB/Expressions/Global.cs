﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// An expression type that is able to store a list of neurons.  The contents of a global
    /// remain valid for as long as the processor is alive.
    /// </summary>
    [NeuronID((ulong)PredefinedNeurons.SplitReaction, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Duplicate, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Copy, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Global, typeof(Neuron))]
    public class Global : Variable
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.Variable"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.Variable];
            }
        }

        #endregion TypeOfNeuron

        #region SplitReaction

        /// <summary>
        /// Gets/sets the neuron that identifies how this global should be handled during a processor split.
        /// </summary>
        /// <remarks>
        /// This can be <see cref="PredefinedNeurons.Copy"/>, <see cref="PredefinedNeurons.Duplicate"/> or
        /// <see cref="PredefinedNeurons.Empty"/>.
        /// <para>
        /// The default is 'copy'.
        /// </para>
        /// </remarks>
        public Neuron SplitReaction
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.SplitReaction);
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.SplitReaction, value);
            }
        }

        #endregion SplitReaction

        /// <param name="proc">The proc.</param>
        public override IEnumerable<Neuron> GetValue(Processor proc)
        {
            List<Neuron> iRes = null;
            Dictionary<ulong, List<Neuron>> iDict = proc.GlobalValues;
            Debug.Assert(iDict != null);
            lock (iDict)
            {
                if (iDict.TryGetValue(ID, out iRes) == false)
                {
                    Neuron iValue = Value;
                    if (iValue != null)
                    {
                        iRes = Variable.SolveResultExp(iValue, proc).ToList();
                        iDict.Add(ID, iRes);
                    }
                }
            }
            if (iRes == null)
                iRes = new List<Neuron>();
            return iRes;
        }

        /// <param name="value">The value.</param>
        /// <param name="proc">The proc.</param>
        protected internal override void StoreValue(IEnumerable<Neuron> value, Processor proc)
        {
            Dictionary<ulong, List<Neuron>> iDict = proc.GlobalValues;
            Debug.Assert(iDict != null);
            lock (iDict)
                iDict[this.ID] = value.ToList();
        }
    }
}