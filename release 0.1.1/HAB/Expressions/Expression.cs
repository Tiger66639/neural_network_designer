﻿namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Base class for all code expressions.
    /// </summary>
    /// <remarks>
    /// <para>
    /// An expression differs from an instruction in that an instruction does something while
    /// an expression can point to an instruction but can also be used to control the execution
    /// loop.
    /// </para>
    /// This is also a neuron so that <see cref="Processor"/>s can easely edit, create and filter them.  This also
    /// allows us to use <see cref="NeuronCluster"/>s to store them as functions.
    /// </remarks>
    public abstract class Expression : Neuron
    {
        /// <summary>
        /// Inheriters should implement this function for performing the expression.
        /// </summary>
        /// <remarks>
        /// Implementers don't have to increment the procoessor positions or anything (except for conditional statements offcourse),
        /// this is done by <see cref="Expression.Execute"/>
        /// </remarks>
        /// <param name="handler">The processor to execute the statements on.</param>
        protected internal abstract void Execute(Processor handler);
    }
}