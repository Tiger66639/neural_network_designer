﻿using System;
using System.Timers;
using System.Xml;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Provides timing functionality to the brain.
    /// </summary>
    /// <remarks>
    /// When the timersin is active, the <see cref="Neuron.StatementsCluster"/> of the timersin will be used
    /// as callback for each timer tick.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.IsActive, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Interval, typeof(Neuron))]
    public class TimerSin : ProcessorDependentSin
    {
        #region Fields

        /// <summary>
        /// The timer used to call the code
        /// </summary>
        private Timer fTimer;

        private Double fInterval;

        #endregion Fields

        public TimerSin()
        {
            fTimer = new Timer();
            fTimer.Elapsed += new ElapsedEventHandler(Timer_Elapsed);
        }

        #region Interval

        /// <summary>
        /// Gets/sets the time interval between 2 consecutive executes expressed in milliseconds.
        /// </summary>
        /// <remarks>
        /// When no value is specified, the default of 1 second is used.
        /// </remarks>
        public Double Interval
        {
            get
            {
                return fInterval;
            }
            set
            {
                if (fInterval != value)
                {
                    fInterval = value;
                    fTimer.Interval = value;
                    if (Brain.Current.HasNeuronChangedEvents)
                        Brain.Current.OnNeuronChanged(new NeuronPropChangedEventArgs("Interval", this));
                }
            }
        }

        #endregion Interval

        #region IsActive

        /// <summary>
        /// Gets/sets if this timer is currently active or not.
        /// </summary>
        /// <remarks>
        /// This property is not backed by a neuron cause the brain should not be able to set this value through
        /// a neuron, since this doesn't trigger a change in the timer. This needs to be done through the output system.
        /// </remarks>
        public bool IsActive
        {
            get { return fTimer.Enabled; }
            set
            {
                if (value != fTimer.Enabled)
                {
                    fTimer.Enabled = value;
                    if (Brain.Current.HasNeuronChangedEvents)
                        Brain.Current.OnNeuronChanged(new NeuronPropChangedEventArgs("IsActive", this));
                }
            }
        }

        #endregion IsActive

        #region StatementsCluster

        /// <summary>
        /// Gets/sets the expressions that should be executed when the time has ellapsed.
        /// </summary>
        /// <remarks>
        /// </remarks>
        public NeuronCluster StatementsCluster
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.Statements) as NeuronCluster;
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Statements, value);
            }
        }

        #endregion StatementsCluster

        #region Functions

        /// <summary>
        /// Handles the Elapsed event of the fTimer control.
        /// </summary>
        /// <remarks>
        /// When a tick is passed, we call the '<see cref="Neuron.Actions"/> code attached to this sin. We also check
        /// if the Interval is changed and update here.  We do this here so that the brain can change it's own processing
        /// interval.
        /// </remarks>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            IProcessorFactory iFactory = Factory;
            if (iFactory == null)
            {
                Log.LogError("TimerSin.Timer_Elapsed", "No Processor factory provided!");
                return;
            }
            Processor iProcessor = iFactory.GetProcessor();
            if (iProcessor != null)
            {
                NeuronCluster iStatements = StatementsCluster;
                if (iStatements != null)
                    iProcessor.Call(iStatements);
                else
                    Log.LogWarning("TimerSin.Timer_Elapsed", "No code assigned to Timer (failed to find an ActionsCluster)!");
            }
            else
                Log.LogError("TimerSin.Timer_Elapsed", "Processor factory failed to create processor!");
        }

        /// <summary>
        /// Tries to translate the specified neuron to the output type of the Sin and send it to the outside world.
        /// </summary>
        /// <param name="toSend"></param>
        /// <remarks>
        /// This output function is able to control the state of the timer through commands declared on the neuron.
        /// An instruction is declared with a link from 'toSend' to the command identifier neuron, with a meaning of <see cref="PredfinedNeurons.Instruction"/>.
        /// If the instruction has arguments, these are also added as links with a meaning of <see cref="PredifnedNeurons.Arguments"/>.
        /// Instruction:
        /// - IsActive: determins if the timer is active or not.
        ///   arguments: 1 -> either <see cref="PredefinedNeurons.True"/> or <see cref="PredefinedNeurons.False"/>.
        /// - Interval: determins the time interval between 2 ticks, expressed in milliseconds (double).
        ///   args: 1 -> a <see cref="DoubleNeuron"/> containing the new value to use.
        /// </remarks>
        public override void Output(Neuron toSend)
        {
            Neuron iInstruction = toSend.FindFirstOut((ulong)PredefinedNeurons.Instruction);
            if (iInstruction != null)
            {
                Neuron iArg;
                if (iInstruction.ID == (ulong)PredefinedNeurons.IsActive)
                {
                    iArg = toSend.FindFirstOut((ulong)PredefinedNeurons.Arguments);
                    if (iArg != null)
                    {
                        if (iArg.ID == (ulong)PredefinedNeurons.True)
                            IsActive = true;
                        else if (iArg.ID == (ulong)PredefinedNeurons.False)
                            IsActive = false;
                        else
                            Log.LogError("TimerSin.Output", string.Format("Invalid argument found: {0}, only true or fale allowed.", iArg));
                    }
                    else
                        Log.LogError("TimerSin.Output", string.Format("Failed to find argument for the 'IsActive' instruction."));
                }
                else if (iInstruction.ID == (ulong)PredefinedNeurons.Interval)
                {
                    DoubleNeuron iInterval = toSend.FindFirstOut((ulong)PredefinedNeurons.Arguments) as DoubleNeuron;
                    if (iInterval != null)
                    {
                        if (fTimer.Interval != iInterval.Value)
                            Interval = iInterval.Value;
                    }
                    else
                        Log.LogError("TimerSin.Output", string.Format("Invalid argument found: {0}, double neuron expected.", iInterval));
                }
                else
                    Log.LogError("TimerSin.Output", string.Format("Unknown instruction found: {0}.", iInstruction));
            }
            else
                Log.LogError("TimerSin.Output", string.Format("Failed to find a instruction on {0}.", toSend));
        }

        /// <summary>
        /// Called when the data needs to be saved.
        /// </summary>
        public override void Flush()
        {
            //nothing to do.
        }

        /// <summary>
        /// Reads the class from xml file.
        /// </summary>
        /// <param name="reader"></param>
        public override void ReadXml(XmlReader reader)
        {
            base.ReadXml(reader);
            Interval = XmlStore.ReadElement<double>(reader, "Interval");
            IsActive = XmlStore.ReadElement<bool>(reader, "IsActive");
        }

        /// <summary>
        /// Writes the class to xml files
        /// </summary>
        /// <param name="writer">The xml writer to use</param>
        public override void WriteXml(XmlWriter writer)
        {
            base.WriteXml(writer);
            XmlStore.WriteElement<double>(writer, "Interval", Interval);
            XmlStore.WriteElement<bool>(writer, "IsActive", IsActive);
        }

        /// <summary>
        /// Copies all the data from this neuron to the argument.
        /// </summary>
        /// <param name="copyTo">The object to copy their data to.</param>
        /// <remarks>
        /// By default, it only copies over all of the links (which includes the 'LinksOut' and 'LinksIn' lists.
        /// <para>
        /// Inheriters should reimplement this function and copy any extra information required for their specific type
        /// of neuron.
        /// </para>
        /// </remarks>
        protected override void CopyTo(Neuron copyTo)
        {
            base.CopyTo(copyTo);
            TimerSin iCopyTo = copyTo as TimerSin;
            if (iCopyTo != null)
            {
                iCopyTo.IsActive = IsActive;
                iCopyTo.Interval = Interval;
            }
        }

        #endregion Functions
    }
}