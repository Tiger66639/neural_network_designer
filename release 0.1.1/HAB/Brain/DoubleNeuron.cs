﻿using System;
using System.Xml;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// A neuron that represents a double Nr.
    /// </summary>
    [NeuronID((ulong)PredefinedNeurons.DoubleNeuron, typeof(Neuron))]
    public class DoubleNeuron : ValueNeuron
    {
        private double fValue;

        #region Value

        /// <summary>
        /// Gets/sets the double value that this neuron represents.
        /// </summary>
        public double Value
        {
            get
            {
                return fValue;
            }
            set
            {
                if (fValue != value)
                {
                    fValue = value;
                    IsChanged = true;
                    if (Brain.Current.HasNeuronChangedEvents)
                        Brain.Current.OnNeuronChanged(new NeuronPropChangedEventArgs("Value", this));
                }
            }
        }

        #endregion Value

        #region blob

        /// <summary>
        /// Gets or sets the value of the neuron as a blob.
        /// </summary>
        /// <value>The object encapsulated by the neuron.</value>
        /// <remarks>
        /// This can be used during streaming of the neuron.
        /// </remarks>
        public override object Blob
        {
            get
            {
                return Value;
            }
            set
            {
                if (value is double)
                    Value = (double)value;
                else
                    throw new InvalidOperationException();
            }
        }

        #endregion blob

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.DoubleNeuron"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.DoubleNeuron];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Clears all the data from this instance.
        /// </summary>
        /// <remarks>
        /// This function is automically called when a neuron is deleted.
        /// This includes incomming and outgoing links, clustered by, children (if it is a clusterr), and any possible values.
        /// <para>
        /// Descendents can enhance this function and clean more data.
        /// </para>
        /// </remarks>
        public override void Clear()
        {
            base.Clear();
            Value = 0.0;
        }

        /// <summary>
        /// Copies all the data from this neuron to the argument.
        /// </summary>
        /// <param name="copyTo">The object to copy their data to.</param>
        /// <remarks>
        /// By default, it only copies over all of the links (which includes the 'LinksOut' and 'LinksIn' lists.
        /// <para>
        /// Inheriters should reimplement this function and copy any extra information required for their specific type
        /// of neuron.
        /// </para>
        /// </remarks>
        protected override void CopyTo(Neuron copyTo)
        {
            base.CopyTo(copyTo);
            DoubleNeuron iCopyTo = copyTo as DoubleNeuron;
            if (iCopyTo != null)
                iCopyTo.fValue = fValue;
        }

        public override string ToString()
        {
            return fValue.ToString();
        }

        /// <param name="right">The neuron to compare it with.</param>
        /// <param name="op">The operator to use.</param>
        /// <returns>True if the operator is correct.</returns>
        protected internal override bool CompareWith(Neuron right, Neuron op)
        {
            if (right is DoubleNeuron)
            {
                double iVal = ((DoubleNeuron)right).Value;
                switch (op.ID)
                {
                    case (ulong)PredefinedNeurons.Equal: return Value == iVal;
                    case (ulong)PredefinedNeurons.Smaller: return Value < iVal;
                    case (ulong)PredefinedNeurons.SmallerOrEqual: return Value <= iVal;
                    case (ulong)PredefinedNeurons.Bigger: return Value > iVal;
                    case (ulong)PredefinedNeurons.BiggerOrEqual: return Value >= iVal;
                    case (ulong)PredefinedNeurons.Different: return Value != iVal;
                    default:
                        Log.LogError("DoubleNeuron.CompareWith", string.Format("Invalid operator found: {0}.", op));
                        return false;
                }
            }
            else
                return base.CompareWith(right, op);
        }

        /// <summary>
        /// Reads the class from xml file.
        /// </summary>
        /// <param name="reader"></param>
        public override void ReadXml(XmlReader reader)
        {
            base.ReadXml(reader);
            fValue = XmlStore.ReadElement<double>(reader, "Value");
        }

        /// <summary>
        /// Writes the class to xml files
        /// </summary>
        /// <param name="writer">The xml writer to use</param>
        public override void WriteXml(XmlWriter writer)
        {
            base.WriteXml(writer);
            XmlStore.WriteElement<double>(writer, "Value", fValue);
        }
    }
}