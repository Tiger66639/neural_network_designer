﻿using System;
using System.Xml;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// A neuron that represents an integer Nr.
    /// </summary>
    [NeuronID((ulong)PredefinedNeurons.IntNeuron, typeof(Neuron))]
    public class IntNeuron : ValueNeuron
    {
        private int fValue;

        #region Value

        /// <summary>
        /// Gets/sets the double value that this neuron represents.
        /// </summary>
        public int Value
        {
            get
            {
                return fValue;
            }
            set
            {
                if (fValue != value)
                {
                    fValue = value;
                    IsChanged = true;
                    if (Brain.Current.HasNeuronChangedEvents)
                        Brain.Current.OnNeuronChanged(new NeuronPropChangedEventArgs("Value", this));
                }
            }
        }

        #endregion Value

        #region blob

        /// <summary>
        /// Gets or sets the value of the neuron as a blob.
        /// </summary>
        /// <value>The object encapsulated by the neuron.</value>
        /// <remarks>
        /// This can be used during streaming of the neuron.
        /// </remarks>
        public override object Blob
        {
            get
            {
                return Value;
            }
            set
            {
                if (value is int)
                    Value = (int)value;
                else
                    throw new InvalidOperationException();
            }
        }

        #endregion blob

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.IntNeuron"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.IntNeuron];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Clears all the data from this instance.
        /// </summary>
        /// <remarks>
        /// This function is automically called when a neuron is deleted.
        /// This includes incomming and outgoing links, clustered by, children (if it is a clusterr), and any possible values.
        /// <para>
        /// Descendents can enhance this function and clean more data.
        /// </para>
        /// </remarks>
        public override void Clear()
        {
            base.Clear();
            Value = 0;
        }

        /// <summary>
        /// Copies all the data from this neuron to the argument.
        /// </summary>
        /// <param name="copyTo">The object to copy their data to.</param>
        /// <remarks>
        /// By default, it only copies over all of the links (which includes the 'LinksOut' and 'LinksIn' lists.
        /// <para>
        /// Inheriters should reimplement this function and copy any extra information required for their specific type
        /// of neuron.
        /// </para>
        /// </remarks>
        protected override void CopyTo(Neuron copyTo)
        {
            base.CopyTo(copyTo);
            IntNeuron iCopyTo = copyTo as IntNeuron;
            if (iCopyTo != null)
                iCopyTo.fValue = fValue;
        }

        public override string ToString()
        {
            return fValue.ToString();
        }

        /// <param name="right">The neuron to compare it with.</param>
        /// <param name="op">The operator to use.</param>
        /// <returns>True if the operator is correct.</returns>
        protected internal override bool CompareWith(Neuron right, Neuron op)
        {
            if (right is IntNeuron)
            {
                int iVal = ((IntNeuron)right).Value;
                switch (op.ID)
                {
                    case (ulong)PredefinedNeurons.Equal: return Value == iVal;
                    case (ulong)PredefinedNeurons.Smaller: return Value < iVal;
                    case (ulong)PredefinedNeurons.SmallerOrEqual: return Value <= iVal;
                    case (ulong)PredefinedNeurons.Bigger: return Value > iVal;
                    case (ulong)PredefinedNeurons.BiggerOrEqual: return Value >= iVal;
                    case (ulong)PredefinedNeurons.Different: return Value != iVal;
                    default:
                        Log.LogError("IntNeuron.CompareWith", string.Format("Invalid operator found: {0}.", op));
                        return false;
                }
            }
            else
                return base.CompareWith(right, op);
        }

        public override void ReadXml(XmlReader reader)
        {
            base.ReadXml(reader);
            fValue = XmlStore.ReadElement<int>(reader, "Value");
        }

        /// <param name="writer">The xml writer to use</param>
        public override void WriteXml(XmlWriter writer)
        {
            base.WriteXml(writer);
            XmlStore.WriteElement<int>(writer, "Value", fValue);
        }
    }
}