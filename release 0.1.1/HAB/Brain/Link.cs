﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// A link between 2 <see cref="Neuron"/>s.
    /// </summary>
    /// <remarks>
    /// A link can't be a neuron itself cause temporary links get created and destroyed very often during processing of neurons,
    /// this would tax the system to much.
    /// </remarks>
    public class Link : IXmlSerializable
    {
        #region internal types

        /// <summary>
        /// Arguments used for the <see cref="Link.InternalCreate"/> function.  Have been put in class because they are so many.
        /// </summary>
        public class CreateArgs
        {
            public Neuron To { get; set; }
            public int ToIndex { get; set; }
            public Neuron From { get; set; }
            public int FromIndex { get; set; }
            public Neuron Meaning { get; set; }

            public CreateArgs()
            {
                ToIndex = -1;
                FromIndex = -1;
            }
        }

        #endregion internal types

        #region fields

        private ulong fTo;
        private ulong fFrom;
        private ulong fMeaning;
        private LinkInfoList fInfo = new LinkInfoList();

        #endregion fields

        #region ctor

        /// <summary>
        /// Default constructor.
        /// </summary>
        private Link()
        {
        }

        /// <summary>
        /// Constructor that specifies the 2 neurons that need to be connected, using references to the objects and a meaning.
        /// </summary>
        /// <remarks>
        /// if to, from or meaning have a <see cref="Neuron.TempId"/> identifier, they are first registered.
        /// </remarks>
        /// <param name="to">The source of the link (stored in <see cref="Link.To"/>)</param>
        /// <param name="from">The destination of the link (stored in <see cref="Link.From"/>)</param>
        /// <param name="meaning">the meaning of the link.</param>
        public Link(Neuron to, Neuron from, Neuron meaning)
        {
            CheckArgs(to, from, meaning);
            CreateArgs iArgs = new CreateArgs() { To = to, From = from, Meaning = meaning };
            InternalCreate(iArgs);
        }

        /// <summary>
        /// Constructor that specifies the 2 neurons that need to be connected, using references to the objects and a meaning as ulong.
        /// </summary>
        /// <remarks>
        /// if to or from have a <see cref="Neuron.TempId"/> identifier, they are first registered. Meaning must be a valid registered
        /// id.
        /// </remarks>
        /// <param name="to">The source of the link (stored in <see cref="Link.To"/>)</param>
        /// <param name="from">The destination of the link (stored in <see cref="Link.From"/>)</param>
        /// <param name="meaning">the meaning of the link.</param>
        public Link(Neuron to, Neuron from, ulong meaning)
        {
            Debug.Assert(to != null);
            if (to.ID == Neuron.EmptyId)
                throw new ArgumentException("The neuron has an invalid Id, has it been added to the Brain already?", "to");
            Debug.Assert(from != null);
            if (from.ID == Neuron.EmptyId)
                throw new ArgumentException("The neuron has an invalid Id, has it been added to the Brain already?", "from");
            if (meaning == Neuron.EmptyId)
                throw new ArgumentException("The neuron has an invalid Id, has it been added to the Brain already?", "from");
            CreateArgs iArgs = new CreateArgs() { To = to, From = from, Meaning = Brain.Current[meaning] };
            CheckArgs(to, from, iArgs.Meaning);
            InternalCreate(iArgs);
        }

        public Link(Neuron to, int toIndex, Neuron from, Neuron meaning)
        {
            CheckArgs(to, from, meaning);
            CreateArgs iArgs = new CreateArgs() { To = to, From = from, Meaning = meaning, ToIndex = toIndex };
            InternalCreate(iArgs);
        }

        public Link(Neuron to, Neuron from, int fromIndex, Neuron meaning)
        {
            CheckArgs(to, from, meaning);
            CreateArgs iArgs = new CreateArgs() { To = to, From = from, Meaning = meaning, FromIndex = fromIndex };
            InternalCreate(iArgs);
        }

        public Link(Neuron to, int toIndex, Neuron from, int fromIndex, Neuron meaning)
        {
            CheckArgs(to, from, meaning);
            CreateArgs iArgs = new CreateArgs() { To = to, From = from, Meaning = meaning, FromIndex = fromIndex, ToIndex = toIndex };
            InternalCreate(iArgs);
        }

        /// <summary>
        /// Checks the args for the constructors and throws the correct exceptions when needed.
        /// </summary>
        /// <param name="to">To as neuron</param>
        /// <param name="from">From as neuron</param>
        /// <param name="meaning">The meaning as neuron</param>
        private void CheckArgs(Neuron to, Neuron from, Neuron meaning)
        {
            Debug.Assert(to != null);
            if (to.ID == Neuron.EmptyId)
                throw new ArgumentException("The neuron has an invalid Id, has it been added to the Brain already?", "to");
            Debug.Assert(from != null);
            if (from.ID == Neuron.EmptyId)
                throw new ArgumentException("The neuron has an invalid Id, has it been added to the Brain already?", "from");
            if (meaning.ID == Neuron.EmptyId)
                throw new ArgumentException("The neuron has an invalid Id, has it been added to the Brain already?", "from");
            if (Exists(from, to, meaning.ID) == true)
                throw new BrainException(string.Format("Can't create link from: {0}, to: {1}, meaning:{2}.  Link already exists.", to, from, meaning));
        }

        /// <summary>
        /// An internal constructor used for readin from xml file, only saves the data.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="from"></param>
        /// <param name="meaning"></param>
        internal Link(ulong to, ulong from, ulong meaning)
        {
            fTo = to;
            fFrom = from;
            fMeaning = meaning;
        }

        /// <summary>
        /// Internal constructor used by the processor when the action was cashed.
        /// </summary>
        /// <param name="args"></param>
        internal Link(CreateArgs args)
        {
            InternalCreate(args);
        }

        /// <summary>
        /// Performs the actual init of the object, creates the links, but it only adds the link to the lists if it's valid
        /// for the current processor state.
        /// </summary>
        /// <remarks>
        /// if to, from have a <see cref="Neuron.TempId"/> identifier, they are first registered.
        /// </remarks>
        private void InternalCreate(CreateArgs args)
        {
            if (args.To.ID == Neuron.TempId)
                Brain.Current.Add(args.To);
            if (args.From.ID == Neuron.TempId)
                Brain.Current.Add(args.From);
            fTo = args.To.ID;
            fFrom = args.From.ID;
            fMeaning = args.Meaning.ID;
            args.Meaning.MeaningUsageCount++;

            if (args.ToIndex == -1)
                args.To.AddInboundLink(this);
            else
                args.To.InsertInboundLink(this, args.ToIndex);
            if (args.FromIndex == -1)
                args.From.AddOutgoingLink(this);
            else
                args.From.InsertOutgoingLink(this, args.FromIndex);
            if (Brain.Current.HasLinkChangedEvents == true)
            {
                LinkChangedEventArgs iArgs = new LinkChangedEventArgs()
                {
                    Action = BrainAction.Created,
                    NewTo = args.To.ID,
                    NewFrom = args.From.ID,
                    OriginalSource = this
                };
                Brain.Current.OnLinkChanged(iArgs);
            }
        }

        #endregion ctor

        #region Prop

        #region To

        /// <summary>
        /// Gets/sets the object that this link points to.
        /// </summary>
        /// <value>To.</value>
        /// <remarks>
        /// The getter throws an exception if not found.
        /// </remarks>
        public Neuron To
        {
            get
            {
                return Brain.Current[fTo];
            }
            set
            {
                if ((value == null && fTo != Neuron.EmptyId) || (value != null && fTo != value.ID))
                {
                    if (Exists(From, value, MeaningID) == true)                                      //this statement  is important, it needs to be done before fto is changed.  This way, both neurons are loaded from disk and the link is this object, so we everything stays ok.
                        throw new BrainException(string.Format("Link between from:{0} to: {1}, meaning: {2} already exists.  old To ({3} remains)", FromID, value.ID, MeaningID, fTo));
                    if (Brain.Current.HasLinkChangedEvents == true)
                    {
                        LinkChangedEventArgs iArgs = new LinkChangedEventArgs()
                        {
                            Action = BrainAction.Changed,
                            NewFrom = From.ID,
                            OldFrom = From.ID,
                            NewTo = value.ID,
                            OldTo = fTo,
                            OriginalSource = this
                        };
                        Brain.Current.OnLinkChanged(iArgs);
                    }
                    if (fTo != Neuron.EmptyId)
                    {
                        Neuron iNeuron = Brain.Current[fTo];
                        iNeuron.RemoveInboundLink(this);
                    }
                    if (value != null)
                        fTo = value.ID;
                    else
                        fTo = Neuron.EmptyId;
                    if (fTo != Neuron.EmptyId)
                        value.AddInboundLink(this);
                    TouchIsChanged(fFrom);                                                           //need to make certain that the from neuron also resaves the link cause it has changed.
                }
            }
        }

        /// <summary>
        /// Touches the IsChanged prop of the specified neuron.
        /// </summary>
        /// <remarks>
        /// When a link is changed, we need to make certain that both neurons get signalled as changed,
        /// cause both neurons store the link info.
        /// </remarks>
        /// <param name="item">The item.</param>
        private void TouchIsChanged(ulong item)
        {
            if (item != Neuron.EmptyId)
            {
                Neuron iItem = Brain.Current[item];
                iItem.IsChanged = true;
            }
        }

        #endregion To

        #region ToID

        /// <summary>
        /// Gets the id of the To Neuron
        /// </summary>
        /// <remarks>
        /// Convenience prop for quick access to the id.
        /// </remarks>
        public ulong ToID
        {
            get { return fTo; }
        }

        #endregion ToID

        #region From

        /// <summary>
        /// Gets/sets the id of the object that this link originates from.
        /// </summary>
        /// <remarks>
        /// The getter throws an exception if not found.
        /// </remarks>
        public Neuron From
        {
            get
            {
                return Brain.Current[fFrom];
            }
            set
            {
                if ((value == null && fFrom != Neuron.EmptyId) || (value != null && fFrom != value.ID))
                {
                    if (Exists(value, To, MeaningID) == true)                                                 //this statement  is important, it needs to be done before fto is changed.  This way, both neurons are loaded from disk and the link is this object, so we everything stays ok.
                        throw new BrainException(string.Format("Link between from:{0} to: {1}, meaning: {2} already exists.  old from ({3} remains)", value.ID, ToID, MeaningID, fFrom));
                    if (Brain.Current.HasLinkChangedEvents == true)
                    {
                        LinkChangedEventArgs iArgs = new LinkChangedEventArgs()
                        {
                            Action = BrainAction.Changed,
                            NewFrom = value.ID,
                            OldFrom = fFrom,
                            NewTo = To.ID,
                            OldTo = To.ID,
                            OriginalSource = this
                        };
                        Brain.Current.OnLinkChanged(iArgs);
                    }
                    if (fFrom != Neuron.EmptyId)
                    {
                        Neuron iNeuron = Brain.Current[fFrom];
                        iNeuron.RemoveOutgoingLink(this);
                    }
                    if (value != null)
                        fFrom = value.ID;
                    else
                        fFrom = Neuron.EmptyId;
                    if (fFrom != Neuron.EmptyId)
                        value.AddOutgoingLink(this);
                    TouchIsChanged(fTo);
                }
            }
        }

        #endregion From

        /// <summary>
        /// Gets the id of the FromNeuron
        /// </summary>
        /// <remarks>
        /// Convenience prop for quick access to the id.
        /// </remarks>
        public ulong FromID
        {
            get { return fFrom; }
        }

        #region Meaning

        /// <summary>
        /// Gets/sets the id of the object that defines the meaning of this link.
        /// </summary>
        /// <remarks>
        /// The getter throws an exception if not found.
        /// </remarks>
        public Neuron Meaning
        {
            get
            {
                return Brain.Current[fMeaning];
            }
            set
            {
                if ((value == null && fMeaning != Neuron.EmptyId) || (value != null && fMeaning != value.ID))
                {
                    if (Exists(From, To, value.ID) == true)                                                      //this statmenet is important: it makes certain both neurons are loaded before meaning is changed, so they are both loaded correctly.
                        throw new BrainException(string.Format("Link between from:{0} to: {1}, meaning: {2} already exists.  old meaning ({3} remains)", FromID, ToID, value.ID, MeaningID));
                    if (fMeaning != Neuron.EmptyId)
                        Brain.Current[fMeaning].MeaningUsageCount--;
                    if (value != null)
                    {
                        fMeaning = value.ID;
                        value.MeaningUsageCount++;
                    }
                    else
                        fMeaning = Neuron.EmptyId;
                    TouchIsChanged(fTo);                                                                         //need to let those neurons know the link has changed.
                    TouchIsChanged(fFrom);
                }
            }
        }

        #endregion Meaning

        /// <summary>
        /// Gets the id of the Meaning Neuron
        /// </summary>
        /// <remarks>
        /// Convenience prop for quick access to the id.
        /// </remarks>
        public ulong MeaningID
        {
            get { return fMeaning; }
        }

        #region Info

        public LinkInfoAccessor Info
        {
            get
            {
                return new LinkInfoAccessor(fInfo, AccessorMode.Read, fInfo.Lock);
            }
        }

        public LinkInfoAccessor InfoW
        {
            get
            {
                return new LinkInfoAccessor(fInfo, AccessorMode.Write, fInfo.Lock);
            }
        }

        #endregion Info

        #endregion Prop

        #region Functions

        /// <summary>
        /// Constructor used for duplicating the links of a neuron (<see cref="Neuron.Duplicate"/>.
        /// This method doesn't raise any events to indicate it is constructed, cause it creates
        /// a duplicate of something that already exists.
        /// </summary>
        /// <returns>A new neurn </returns>
        internal Link Duplicate()
        {
            Link iRes = new Link();
            iRes.fTo = fTo;
            iRes.fFrom = fFrom;
            iRes.fMeaning = fMeaning;
            return iRes;
        }

        /// <summary>
        /// Destroys the link between the 2 objects.
        /// </summary>
        /// <remarks>
        /// Is able to handle invalid links.
        /// </remarks>
        public void Destroy()
        {
            if (fTo == Neuron.EmptyId && fFrom == Neuron.EmptyId && fMeaning == Neuron.EmptyId)          //check if the link is actually still existing.
                return;
            Neuron iNeuron;
            Brain.Current.TryFindNeuron(ToID, out iNeuron);                                         //we use this way of retrieving the neuron, otherwise we get an exception if we have an invalid link.  This makes it safer.
            if (iNeuron != null)
                iNeuron.RemoveInboundLink(this);
            Brain.Current.TryFindNeuron(FromID, out iNeuron);                                         //we use this way of retrieving the neuron, otherwise we get an exception if we have an invalid link.  This makes it safer.
            if (iNeuron != null)
                iNeuron.RemoveOutgoingLink(this);
            Brain.Current.TryFindNeuron(MeaningID, out iNeuron);                                         //we use this way of retrieving the neuron, otherwise we get an exception if we have an invalid link.  This makes it safer.
            if (iNeuron != null)
                iNeuron.MeaningUsageCount--;
            if (fInfo != null)
            {
                foreach (ulong i in fInfo)
                {
                    Brain.Current.TryFindNeuron(i, out iNeuron);
                    iNeuron.InfoUsageCount--;
                }
            }

            if (Brain.Current.HasLinkChangedEvents == true)                                           //we raise the event after the link has been broken otherwise some events might work incorrectly.
            {
                LinkChangedEventArgs iArgs = new LinkChangedEventArgs()
                {
                    Action = BrainAction.Removed,
                    OldTo = fTo,
                    OldFrom = fFrom,
                    OriginalSource = this
                };
                Brain.Current.OnLinkChanged(iArgs);
            }
            fTo = Neuron.EmptyId;                                                                     //also need to clear the values from the link object
            fFrom = Neuron.EmptyId;
            fMeaning = Neuron.EmptyId;
            using (LinkInfoAccessor iList = InfoW)
                iList.Clear();
        }

        /// <summary>
        /// Checks if there is already a link in existence between the specified neurons with the specified meaning.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <param name="meaning">The meaning.</param>
        /// <returns>True if the link exists, false otherwise.</returns>
        public static bool Exists(Neuron from, Neuron to, ulong meaningID)
        {
            Neuron iFrom = from;
            using (LinksAccessor iLinksOut = iFrom.LinksOut)
            {
                var iFound = (from i in iLinksOut.Items
                              where i.ToID == to.ID && i.MeaningID == meaningID                      //we check on ID cause this is the least taxing for the system (on the side of the links, they store the id, the ref they need to retrieve from the brain).
                              select i).FirstOrDefault();
                if (iFound != null)
                    return true;
                else
                {
                    using (LinksAccessor iLinksIn = to.LinksIn)
                    {
                        var iSearch2 = (from i in iLinksIn.Items
                                        where i.FromID == iFrom.ID && i.MeaningID == meaningID
                                        select i).FirstOrDefault();
                        return iSearch2 != null;
                    }
                }
            }
        }

        /// <summary>
        /// Recreates the Link between the 2 specified items with the new meaning.  This function should
        /// only be used after <see cref="Link.Destroy"/> was called on a link.
        /// </summary>
        /// <remarks>
        /// This function is usefull if you need to keep a ref to a Link object that can be destroyed and recreted.
        /// </remarks>
        /// <exception cref="BrainException">The link was still valid (<see cref="Link.From"/>, <see cref="Link.To"/>, and
        /// <see cref="Link.Meaning"/>) were still filled in.</exception>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <param name="meaning">The meaning.</param>
        public void Recreate(Neuron from, Neuron to, Neuron meaning)
        {
            if (fFrom != Neuron.EmptyId || fTo != Neuron.EmptyId || fMeaning != Neuron.EmptyId)
                throw new BrainException("The link must have been destroyed before it can be recreated.");

            CheckArgs(to, from, meaning);
            CreateArgs iArgs = new CreateArgs() { To = to, From = from, Meaning = meaning };
            InternalCreate(iArgs);
        }

        #endregion Functions

        #region IXmlSerializable Members

        public XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        /// we reimplement xml reading/writing cause we don't want to call the setters with each read.
        /// </summary>
        public void ReadXml(XmlReader reader)
        {
            bool wasEmpty = reader.IsEmptyElement;

            reader.Read();
            if (wasEmpty) return;

            reader.ReadStartElement("to");
            string iVal = reader.ReadString();
            ulong iConverted = ulong.Parse(iVal);
            fTo = iConverted;
            reader.ReadEndElement();

            reader.ReadStartElement("from");
            iVal = reader.ReadString();
            iConverted = ulong.Parse(iVal);
            fFrom = iConverted;
            reader.ReadEndElement();

            reader.ReadStartElement("meaning");
            iVal = reader.ReadString();
            iConverted = ulong.Parse(iVal);
            fMeaning = iConverted;
            reader.ReadEndElement();

            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("to");
            writer.WriteString(fTo.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("from");
            writer.WriteString(fFrom.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("meaning");
            writer.WriteString(fMeaning.ToString());
            writer.WriteEndElement();
        }

        #endregion IXmlSerializable Members

        /// <summary>
        /// Finds the first link between to and from with the specified meaning and returns it.
        /// </summary>
        /// <param name="to">the To part of the link.</param>
        /// <param name="from">the from part of the link.</param>
        /// <param name="meaning">The meaning of the link.</param>
        /// <returns></returns>
        public static Link Find(Neuron to, Neuron from, Neuron meaning)
        {
            if (to != null && from != null && meaning != null)
            {
                using (LinksAccessor iLinksIn = to.LinksIn)
                {
                    foreach (Link i in iLinksIn.Items)
                    {
                        if (i.FromID == from.ID && i.MeaningID == meaning.ID)                               //we use id's cause this is faster: don't need to get the neuron from the brain.
                            return i;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Finds the index of the link in the indexer list.
        /// </summary>
        /// <param name="from">From part of the link</param>
        /// <param name="to">To part of the link</param>
        /// <param name="meaning">The meaning part of the link</param>
        /// <param name="indexer">The indexer is either 'from' or 'to', it indicates relative to which neuron the index should be returned.</param>
        /// <returns></returns>
        public static int FindIndex(Neuron from, Neuron to, Neuron meaning, Neuron indexer)
        {
            int iCounter = 0;
            if (indexer == from)
            {
                using (LinksAccessor iLinksOut = from.LinksOut)
                {
                    foreach (Link i in iLinksOut.Items)
                    {
                        if (i.ToID == to.ID && i.MeaningID == meaning.ID)
                            return iCounter;
                        iCounter++;
                    }
                }
            }
            else if (indexer == to)
            {
                using (LinksAccessor iLinksIn = to.LinksIn)
                {
                    foreach (Link i in iLinksIn.Items)
                    {
                        if (i.FromID == from.ID && i.MeaningID == meaning.ID)
                            return iCounter;
                        iCounter++;
                    }
                }
            }
            else
                throw new BrainException("Invalid indexer specified, must be equal to either 'to' or 'from'");
            return -1;                                                                                   //-1 indicates not found.
        }
    }
}