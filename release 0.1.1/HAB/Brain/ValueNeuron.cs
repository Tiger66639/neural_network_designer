﻿namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// A base class for all neurons that can contain a value, like a string, double or int.
    /// </summary>
    /// <remarks>
    /// This class is abstract cause it can't be created on it's own.
    /// </remarks>
    public abstract class ValueNeuron : Neuron
    {
        /// <summary>
        /// Gets or sets the value of the neuron as a blob.
        /// </summary>
        /// <remarks>
        /// This can be used during streaming of the neuron.
        /// </remarks>
        /// <value>The object encapsulated by the neuron.</value>
        public abstract object Blob { get; set; }
    }
}