﻿using System;
using System.Diagnostics;
using System.Threading;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Defines the different modes that an accessor can be in.
    /// </summary>
    public enum AccessorMode
    {
        /// <summary>
        /// The accessor is in undefined mode.  This is the initial state.
        /// </summary>
        None,

        /// <summary>
        /// The accessor will only allow reads.
        /// </summary>
        Read,

        /// <summary>
        /// The accessor will only allow writes.
        /// </summary>
        Write
    }

    /// <summary>
    /// Base class for all types that manage multi thread access to list data in the network.
    /// </summary>
    public class Accessor : IDisposable
    {
        #region fields

        private ReaderWriterLockSlim fLock;
        private AccessorMode fMode;

        #endregion fields

        #region ctor/~

        /// <summary>
        /// Initializes a new instance of the <see cref="Accessor"/> class.
        /// </summary>
        /// <param name="mode">The initial mode to use. Can not be <see cref="AccessorMode.None"/> initially.</param>
        internal Accessor(AccessorMode mode, ReaderWriterLockSlim rwLock)
        {
            Debug.Assert(mode != AccessorMode.None);
            Debug.Assert(rwLock != null);
            fLock = rwLock;                                                               //must be set before mode is assigned, cause 'mode' uses the lock.
            Mode = mode;
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="Accessor"/> is reclaimed by garbage collection.
        /// </summary>
        ~Accessor()
        {
            if (Lock.IsReadLockHeld == true || Lock.IsWriteLockHeld == true)
                StopLock();
        }

        #endregion ctor/~

        #region prop

        #region Lock

        /// <summary>
        /// Gets the lock to use for this accessor.
        /// </summary>
        protected ReaderWriterLockSlim Lock
        {
            get { return fLock; }
        }

        #endregion Lock

        #region Mode

        /// <summary>
        /// Gets/sets the mode of the accessor. This determins if writes are allowed or only reads.  Can be changed during
        /// the existence of the accessor, but may result in time outs when changed.
        /// </summary>
        public AccessorMode Mode
        {
            get
            {
                return fMode;
            }
            set
            {
                if (fMode != value)
                {
                    StopLock();
                    fMode = value;
                    StartLock();
                }
            }
        }

        #endregion Mode

        #endregion prop

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            StopLock();
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable Members

        #region Functions

        /// <summary>
        /// Starts the lock in the requested mode.
        /// </summary>
        private void StartLock()
        {
            switch (Mode)
            {
                case AccessorMode.None:                                                                      //don't do anything: the lock isn't turned on.
                    break;

                case AccessorMode.Read:
                    Lock.EnterReadLock();
                    break;

                case AccessorMode.Write:
                    Lock.EnterWriteLock();
                    break;

                default:
                    throw new InvalidOperationException("Unkown lock mode.");                                  //unkownn type.
            }
        }

        /// <summary>
        /// Stops the lock with respect to the mode in which it was opened.
        /// </summary>
        private void StopLock()
        {
            switch (Mode)
            {
                case AccessorMode.None:                                                                         //don't do anything, the lock is off.
                    break;

                case AccessorMode.Read:
                    while (Lock.IsReadLockHeld == true)
                        Lock.ExitReadLock();
                    break;

                case AccessorMode.Write:
                    while (Lock.IsWriteLockHeld == true)
                        Lock.ExitWriteLock();
                    break;

                default:
                    break;
            }
            fMode = AccessorMode.None;                                                                         //always set the field directly cause this is also called by the prop setter.
        }

        #endregion Functions
    }
}