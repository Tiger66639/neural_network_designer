﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Provides thread save access to a list of neurons.
    /// </summary>
    public class NeuronsAccessor : Accessor
    {
        private BrainList fList;

        public NeuronsAccessor(BrainList list, AccessorMode mode, ReaderWriterLockSlim rwLock)
           : base(mode, rwLock)
        {
            List = list;
        }

        #region List

        /// <summary>
        /// Gets the internal list which provides full access to the items.
        /// </summary>
        protected BrainList List
        {
            get { return fList; }
            private set { fList = value; }
        }

        #endregion List

        #region Items

        /// <summary>
        /// Gets the list of items as a read only collection. This is only allowed if the current <see cref="Acessor.Mode"/>
        /// is different from <see cref="AccessorMode.None"/>.
        /// </summary>
        public ReadOnlyCollection<ulong> Items
        {
            get
            {
                if (Mode == AccessorMode.None)
                    throw new InvalidOperationException("Accessor mode not set.");
                return new ReadOnlyCollection<ulong>(fList);
            }
        }

        #endregion Items

        #region Functions

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <remarks>
        /// Checks wether the internal lists are equal, if so, true is returned.
        /// </remarks>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>.</param>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">
        /// The <paramref name="obj"/> parameter is null.
        /// </exception>
        public override bool Equals(object obj)
        {
            NeuronsAccessor iObj = obj as NeuronsAccessor;
            if (iObj != null)
            {
                return iObj.List == List;
            }
            else
                return base.Equals(obj);
        }

        /// <summary>
        /// Removes the specified neuron from the list.  This function is currently used by the DlgFixBrokenRefs for cleaning
        /// up a broken network.  This should be moved into the core dll so that this one can go away.  For now though, just let it be.
        /// </summary>
        /// <param name="toRemove">To remove.</param>
        public void Remove(Neuron toRemove)
        {
            if (Mode != AccessorMode.Write)
                Mode = AccessorMode.Write;
            fList.Remove(toRemove);
        }

        /// <summary>
        /// Removes the specified neuron from the list.  This function is currently used by the DlgFixBrokenRefs for cleaning
        /// up a broken network.  This should be moved into the core dll so that this one can go away.  For now though, just let it be.
        /// </summary>
        /// <param name="toRemove">To remove.</param>
        public void Remove(ulong toRemove)
        {
            if (Mode != AccessorMode.Write)
                Mode = AccessorMode.Write;
            fList.Remove(toRemove);
        }

        #region Convertion

        /// <summary>
        /// Converts a list of ulongs that represent <see cref="Neuron.ID"/>s into neurons.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <exception cref="">Brain.Current may throw an exception, which is caught.</exception>
        /// <returns>null if the convertion failed, otherwise a list with expressions.</returns>
        public List<T> ConvertTo<T>() where T : Neuron
        {
            try
            {
                List<T> iRes = new List<T>(List.Count);
                foreach (ulong i in List)
                {
                    T iFound = Brain.Current[i] as T;
                    if (iFound != null)
                        iRes.Add(iFound);
                    else
                        return null;
                }
                return iRes;
            }
            catch (Exception e)
            {
                Log.LogError("NeuronsAccessor.ConvertTo<>", e.ToString());
                return null;
            }
        }

        #endregion Convertion

        #endregion Functions
    }
}