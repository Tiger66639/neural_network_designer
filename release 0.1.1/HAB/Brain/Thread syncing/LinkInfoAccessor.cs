﻿using System.Threading;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// An <see cref="Accessor"/> specicific for <see cref="Link.LinkInfo"/> data.
    /// </summary>
    public class LinkInfoAccessor : NeuronsRWAccessor
    {
        public LinkInfoAccessor(LinkInfoList list, AccessorMode mode, ReaderWriterLockSlim rwLock)
           : base(list, mode, rwLock)
        {
        }
    }
}