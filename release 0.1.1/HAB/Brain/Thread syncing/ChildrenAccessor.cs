﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Provides thread save, writable access to a list of cluster children.
    /// </summary>
    public class ChildrenAccessor : NeuronsRWAccessor
    {
        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="ChildrenAccessor"/> class.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <param name="mode">The mode.</param>
        /// <param name="rwLock">The rw lock.</param>
        public ChildrenAccessor(ChildList list, AccessorMode mode, ReaderWriterLockSlim rwLock) : base(list, mode, rwLock)
        {
        }

        #endregion ctor

        #region prop

        #region List

        /// <summary>
        /// Gets the internal list which provides full access to the items.
        /// </summary>
        /// <value></value>
        protected new ChildList List
        {
            get { return (ChildList)base.List; }
        }

        #endregion List

        #endregion prop

        /// <summary>
        /// Adds a range of id's to the list in a thread safe way.
        /// </summary>
        /// <param name="range">The range.</param>
        public void AddRange(IEnumerable<Neuron> items)
        {
            if (Mode != AccessorMode.Write)
                Mode = AccessorMode.Write;
            Debug.Assert(Mode == AccessorMode.Write);
            List.AddRange(items);
        }

        internal void RemoveDirect(Neuron item)
        {
            Debug.Assert(Mode == AccessorMode.Write);
            List.RemoveDirect(item);
        }

        internal void InsertDirect(int index, Neuron item)
        {
            Debug.Assert(Mode == AccessorMode.Write);
            List.InsertDirect(index, item);
        }
    }
}