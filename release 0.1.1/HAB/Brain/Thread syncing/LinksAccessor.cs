﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Provides thread save access to a collection of links.
    /// </summary>
    public class LinksAccessor : Accessor
    {
        private List<Link> fList;

        /// <summary>
        /// Initializes a new instance of the <see cref="LinksAccessor"/> class.
        /// </summary>
        /// <param name="list">The list to wrap</param>
        /// <param name="mode">The initial mode of the accessor.</param>
        /// <param name="rwLock">The lock to use for the list.</param>
        internal LinksAccessor(List<Link> list, AccessorMode mode, ReaderWriterLockSlim rwLock)
           : base(mode, rwLock)
        {
            List = list;
        }

        #region List

        /// <summary>
        /// Gets the internal list which provides full access to the items.
        /// </summary>
        protected List<Link> List
        {
            get { return fList; }
            private set { fList = value; }
        }

        #endregion List

        #region Items

        /// <summary>
        /// Gets the list of items as a read only collection. This is only allowed if the current <see cref="Acessor.Mode"/>
        /// is different from <see cref="AccessorMode.None"/>.
        /// </summary>
        public ReadOnlyCollection<Link> Items
        {
            get
            {
                if (Mode == AccessorMode.None)
                    throw new InvalidOperationException("Accessor mode not set.");
                return new ReadOnlyCollection<Link>(fList);
            }
        }

        #endregion Items

        /// <summary>
        /// Adds the specified link to the list in a thread safe manner.
        /// </summary>
        /// <param name="iLink">The i link.</param>
        internal void Add(Link link)
        {
            if (Mode == AccessorMode.Write)
                fList.Add(link);
            else
                throw new InvalidOperationException("Accessor must be in write mode for modifications.");
        }

        /// <summary>
        /// Removes the specified link in a thread safe way.
        /// </summary>
        /// <param name="link">The link.</param>
        internal void Remove(Link link)
        {
            if (Mode == AccessorMode.Write)
                fList.Remove(link);
            else
                throw new InvalidOperationException("Accessor must be in write mode for modifications.");
        }

        internal void Insert(int index, Link link)
        {
            if (Mode == AccessorMode.Write)
                fList.Insert(index, link);
            else
                throw new InvalidOperationException("Accessor must be in write mode for modifications.");
        }
    }
}