﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// A <see cref="NeuronsAccessor"/> that also provides write access to the list through an <see cref="IList"/> interface.
    /// </summary>
    public class NeuronsRWAccessor : NeuronsAccessor, IList<ulong>
    {
        public NeuronsRWAccessor(BrainList list, AccessorMode mode, ReaderWriterLockSlim rwLock)
           : base(list, mode, rwLock)
        {
        }

        #region IList<ulong> Members

        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.Generic.IList`1"/>.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.IList`1"/>.</param>
        /// <returns>
        /// The index of <paramref name="item"/> if found in the list; otherwise, -1.
        /// </returns>
        public int IndexOf(ulong item)
        {
            Debug.Assert(Mode != AccessorMode.None);
            return List.IndexOf(item);
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.Generic.IList`1"/> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="item"/> should be inserted.</param>
        /// <param name="item">The object to insert into the <see cref="T:System.Collections.Generic.IList`1"/>.</param>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        /// 	<paramref name="index"/> is not a valid index in the <see cref="T:System.Collections.Generic.IList`1"/>.
        /// </exception>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.IList`1"/> is read-only.
        /// </exception>
        public void Insert(int index, ulong item)
        {
            if (Mode != AccessorMode.Write)
                Mode = AccessorMode.Write;
            Debug.Assert(Mode == AccessorMode.Write);
            List.Insert(index, item);
        }

        /// <summary>
        /// Performs a thread safe insert using a <see cref="Neuron"/> instead of it's id.  This is a bit faster if the Neuron is known
        /// and we are not in a subprocessor with a duplication of the owner.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        public void Insert(int index, Neuron neuron)
        {
            if (Mode != AccessorMode.Write)
                Mode = AccessorMode.Write;
            Debug.Assert(Mode == AccessorMode.Write);
            List.Insert(index, neuron);
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.Generic.IList`1"/> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        /// 	<paramref name="index"/> is not a valid index in the <see cref="T:System.Collections.Generic.IList`1"/>.
        /// </exception>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.IList`1"/> is read-only.
        /// </exception>
        public void RemoveAt(int index)
        {
            if (Mode != AccessorMode.Write)
                Mode = AccessorMode.Write;
            Debug.Assert(Mode == AccessorMode.Write);
            List.RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets the <see cref="System.UInt64"/> at the specified index.
        /// </summary>
        /// <value></value>
        public ulong this[int index]
        {
            get
            {
                Debug.Assert(Mode != AccessorMode.None);
                return List[index];
            }
            set
            {
                if (Mode != AccessorMode.Write)
                    Mode = AccessorMode.Write;
                Debug.Assert(Mode == AccessorMode.Write);
                List[index] = value;
            }
        }

        #endregion IList<ulong> Members

        #region ICollection<ulong> Members

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        /// </exception>
        public void Add(ulong item)
        {
            if (Mode != AccessorMode.Write)
                Mode = AccessorMode.Write;
            Debug.Assert(Mode == AccessorMode.Write);
            List.Add(item);
        }

        /// <summary>
        /// Adds the specified neuron.
        /// </summary>
        /// <param name="neuron">The neuron.</param>
        public void Add(Neuron neuron)
        {
            if (Mode != AccessorMode.Write)
                Mode = AccessorMode.Write;
            Debug.Assert(Mode == AccessorMode.Write);
            List.Add(neuron);
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        /// </exception>
        public void Clear()
        {
            if (Mode != AccessorMode.Write)
                Mode = AccessorMode.Write;
            Debug.Assert(Mode == AccessorMode.Write);
            List.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1"/> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param>
        /// <returns>
        /// true if <paramref name="item"/> is found in the <see cref="T:System.Collections.Generic.ICollection`1"/>; otherwise, false.
        /// </returns>
        public bool Contains(ulong item)
        {
            Debug.Assert(Mode != AccessorMode.None);
            return List.Contains(item);
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1"/> to an <see cref="T:System.Array"/>, starting at a particular <see cref="T:System.Array"/> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array"/> that is the destination of the elements copied from <see cref="T:System.Collections.Generic.ICollection`1"/>. The <see cref="T:System.Array"/> must have zero-based indexing.</param>
        /// <param name="arrayIndex">The zero-based index in <paramref name="array"/> at which copying begins.</param>
        /// <exception cref="T:System.ArgumentNullException">
        /// 	<paramref name="array"/> is null.
        /// </exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        /// 	<paramref name="arrayIndex"/> is less than 0.
        /// </exception>
        /// <exception cref="T:System.ArgumentException">
        /// 	<paramref name="array"/> is multidimensional.
        /// -or-
        /// <paramref name="arrayIndex"/> is equal to or greater than the length of <paramref name="array"/>.
        /// -or-
        /// The number of elements in the source <see cref="T:System.Collections.Generic.ICollection`1"/> is greater than the available space from <paramref name="arrayIndex"/> to the end of the destination <paramref name="array"/>.
        /// -or-
        /// Type <paramref name="T"/> cannot be cast automatically to the type of the destination <paramref name="array"/>.
        /// </exception>
        public void CopyTo(ulong[] array, int arrayIndex)
        {
            Debug.Assert(Mode != AccessorMode.None);
            List.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// The number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </returns>
        public int Count
        {
            get
            {
                Debug.Assert(Mode != AccessorMode.None);
                return List.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        /// </summary>
        /// <value></value>
        /// <returns>true if the <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only; otherwise, false.
        /// </returns>
        public bool IsReadOnly
        {
            get
            {
                if (Mode != AccessorMode.Write)
                    return false;
                else
                    return List.IsReadOnly;
            }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param>
        /// <returns>
        /// true if <paramref name="item"/> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1"/>; otherwise, false. This method also returns false if <paramref name="item"/> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </returns>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        /// </exception>
        public new bool Remove(ulong item)
        {
            if (Mode != AccessorMode.Write)
                Mode = AccessorMode.Write;
            Debug.Assert(Mode == AccessorMode.Write);
            return List.Remove(item);
        }

        #endregion ICollection<ulong> Members

        #region IEnumerable<ulong> Members

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1"/> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<ulong> GetEnumerator()
        {
            Debug.Assert(Mode != AccessorMode.None);
            return List.GetEnumerator();
        }

        #endregion IEnumerable<ulong> Members

        #region IEnumerable Members

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
        /// </returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            Debug.Assert(Mode != AccessorMode.None);
            return List.GetEnumerator();
        }

        #endregion IEnumerable Members

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1"/> contains a specific value.
        /// </summary>
        /// <param name="toSearch">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param>
        /// <returns>
        /// true if <paramref name="toSearch"/> is found in the <see cref="T:System.Collections.Generic.ICollection`1"/>; otherwise, false.
        /// </returns>
        public bool Contains(Neuron toSearch)
        {
            Debug.Assert(Mode != AccessorMode.None);
            return List.Contains(toSearch);
        }

        /// <summary>
        /// Checks if every item id in the specified list is also in this list.
        /// </summary>
        /// <remarks>
        /// If list doesn't contain any elements, we always return true.
        /// </remarks>
        /// <param name="list">an enumerable containing neuron id's.</param>
        /// <returns>True if all the specified id's are also in this list.</returns>
        public bool Contains(IEnumerable<ulong> list)
        {
            Debug.Assert(Mode != AccessorMode.None);
            return List.Contains(list);
        }
    }
}