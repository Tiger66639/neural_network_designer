﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Contains all the arguments for the <see cref="BrainHelper.GetObject"/> function.
    /// </summary>
    public class GetObjectArgs
    {
        /// <summary>
        /// Gets wether the object was created or already existed.
        /// </summary>
        /// <value><c>true</c> if this instance is new; otherwise, <c>false</c>.</value>
        [XmlIgnore]
        public bool IsNew { get; internal set; }

        /// <summary>
        /// Gets or sets the text that the object need to encapsulate. Use this property or <see cref="GetObjectArgs.TextNeuron"/>
        /// to provide the text to search for.
        /// </summary>
        /// <value>The text.</value>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the text neuron that the object cluster needs to contain. Use this property or <see cref="GetObjectArgs.Text"/>
        /// to provide the text to search for.
        /// </summary>
        /// <value>The text neuron.</value>
        public TextNeuron TextNeuron { get; set; }

        /// <summary>
        /// Gets or sets the ID of the meaning to search for on the object.  The value to where the link points to will be compared with
        /// <see cref="GetObjectArgs.AttachedValue"/>
        /// </summary>
        /// <value>The meaning ID.</value>
        public ulong MeaningID { get; set; }

        /// <summary>
        /// Gets or sets the integer value to which the result object must point to using the meaning specified
        /// in <see cref="GetObjectArgs.MeaningID"/>.  The actual value will be stored in an IntNeuron.
        /// </summary>
        /// <value>The attached value.</value>
        public int AttachedInt { get; set; }
    }

    /// <summary>
    /// Provides some general routines that work on the <see cref="Brain"/>.
    /// </summary>
    public class BrainHelper
    {
        /// <summary>
        /// Creates a neuron cluster in the form of an object containing a textneuron and a 'meaning' neuron.
        /// </summary>
        /// <remarks>
        /// Will always convert the string value to lowercase.
        /// </remarks>
        /// <param name="value">The value for the textneuron. If there is already a textneuron registered with the <see cref="TextSin"/>
        /// for the same value (case insesitive), this neuron is reused.</param>
        /// <param name="includeMeaning">if set to <c>true</c> a neuron that can be used as the meaning for a link or cluster is also created.</param>
        /// <returns>The neuronCluster that was newly created.</returns>
        public static NeuronCluster CreateObject(string value, out Neuron meaning)
        {
            value = value.ToLower();
            NeuronCluster iCluster = new NeuronCluster();
            iCluster.Meaning = (ulong)PredefinedNeurons.Object;
            Brain.Current.Add(iCluster);

            meaning = new Neuron();
            Brain.Current.Add(meaning);
            using (ChildrenAccessor iList = iCluster.ChildrenW)
                iList.Add(meaning);

            ulong iTextId;
            TextNeuron iText;
            if (TextSin.Words.TryGetValue(value, out iTextId) == true)                      //could be that this word already exists, if so, we reuse it, cause an item can only be once in the dict.
                iText = Brain.Current[iTextId] as TextNeuron;
            else
            {
                iText = new TextNeuron();
                iText.Text = value;
                Brain.Current.Add(iText);
                TextSin.Words[value] = iText.ID;
            }
            using (ChildrenAccessor iList = iCluster.ChildrenW)
                iList.Add(iText);
            return iCluster;
        }

        /// <summary>
        /// Creates a new frame and it's evokers cluster and returns both.
        /// </summary>
        /// <param name="evokers">The evokers.</param>
        /// <returns></returns>
        public static NeuronCluster CreateFrame(out NeuronCluster evokers)
        {
            NeuronCluster iRes = new NeuronCluster();
            iRes.Meaning = (ulong)PredefinedNeurons.Frame;
            evokers = new NeuronCluster();
            evokers.Meaning = (ulong)PredefinedNeurons.FrameEvokers;
            Brain.Current.Add(iRes);
            Brain.Current.Add(evokers);
            Link iNew = new Link(evokers, iRes, (ulong)PredefinedNeurons.FrameEvokers);
            return iRes;
        }

        /// <summary>
        /// Gets the cluster with the specified text value and a link with the specified meaning, pointing to the specified value. If
        /// none exists, one is created.
        /// </summary>
        /// <remarks>
        /// This function allows you to search for an 'object' cluster that points to a specific neuron, like a SynSetID of FrameElementID.
        /// </remarks>
        /// <param name="args">The args.</param>
        /// <returns>
        /// A neuroncluster that represents the object
        /// </returns>
        static public NeuronCluster GetObject(GetObjectArgs args)
        {
            NeuronCluster iRes = null;
            ulong iTextId;
            if (args.TextNeuron == null && args.Text != null)                                      //check  if the args are ok
            {
                if (TextSin.Words.TryGetValue(args.Text, out iTextId) == true)
                    args.TextNeuron = Brain.Current[iTextId] as TextNeuron;
            }
            else if (args.TextNeuron == null)
                throw new ArgumentNullException("Either TextNeuron or Text must be provided to search for an object.");

            if (args.TextNeuron != null)
            {
                iRes = FindObject(args.TextNeuron, args.MeaningID, args.AttachedInt);
                if (iRes == null)
                {
                    iRes = CreateObject(args.TextNeuron, args.MeaningID, args.AttachedInt);
                    args.IsNew = true;
                }
            }
            else
            {
                iRes = CreateObject(args.Text, args.MeaningID, args.AttachedInt);
                args.IsNew = true;
            }
            return iRes;
        }

        /// Tries to find the neuron cluster that represents the object for the specified text neuron and synsetid.
        /// If it doesn't find something, it returns null.
        /// </summary>
        /// <param name="textId">The id of the textneuron to search the object for.</param>
        /// <param name="synsedId">The synsed id.</param>
        /// <returns>A neuroncluster that represents the object, or null if nothing is found.</returns>
        static public NeuronCluster FindObject(TextNeuron text, ulong meaningID, int attachedInt)
        {
            if (text != null)
            {
                using (NeuronsAccessor iClusteredBy = text.ClusteredBy)
                {
                    List<NeuronCluster> iClusters = (from i in iClusteredBy.Items
                                                     where ((NeuronCluster)Brain.Current[i]).Meaning == (ulong)PredefinedNeurons.Object
                                                     select ((NeuronCluster)Brain.Current[i])).ToList();
                    foreach (NeuronCluster i in iClusters)
                    {
                        IntNeuron iFound = i.FindFirstOut(meaningID) as IntNeuron;
                        if (iFound != null && iFound.Value == attachedInt)
                            return i;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Creates a new cluster with meaning 'same' and adds a new text neuron to it.
        /// </summary>
        /// <remarks>
        /// Doesn't directly add to textsin dict, but is done indirectly.
        /// </remarks>
        /// <param name="text">The text that should be assigned to the text neuron that is created.</param>
        /// <param name="synsetid">The synsetid of the sense. This is also stored in the object so that we can find it again.  It also
        /// tags it as comming from wordnet.</param>
        /// <returns></returns>
        static public NeuronCluster CreateObject(string text, ulong meaningID, int attachedInt)
        {
            TextNeuron iText = new TextNeuron();
            iText.Text = text;
            Brain.Current.Add(iText);
            return CreateObject(iText, meaningID, attachedInt);
        }

        /// <summary>
        /// Creates a new cluster with meaning 'Object' and adds the textneuron to it. It also attached the specified int value with
        /// a link using the specified meaning.
        /// </summary>
        /// <param name="text">The neuron to add to the cluster.</param>
        /// <param name="synsetid">The value to attach. </param>
        /// <returns>A neuron cluster with a child an a link to an int neuron, all registered.</returns>
        /// <remarks>
        /// Also makes certain that the textneuron is stored in the dictionary of the textsin so that it can be used.
        /// </remarks>
        static public NeuronCluster CreateObject(TextNeuron text, ulong meaningID, int attachedInt)
        {
            NeuronCluster iRes = CreateClusterWithAttachedVal(attachedInt, meaningID);
            iRes.Meaning = (ulong)PredefinedNeurons.Object;
            using (ChildrenAccessor iList = iRes.ChildrenW)
                iList.Add(text);
            TextSin.Words[text.Text.ToLower()] = text.ID;                                                      //always store the data in lowercase.
            return iRes;
        }

        /// <summary>
        /// Creates a NeuronCluster and assigns it the specified value as attached value through a link with
        /// the specified meaning. The cluster doesn't have a meaning.
        /// </summary>
        /// <param name="synsetid">The value to assign to the cluster.</param>
        /// <returns>A cluster with a link to an intneuron, both registered.</returns>
        static public NeuronCluster CreateClusterWithAttachedVal(int val, ulong meaning)
        {
            NeuronCluster iRes = new NeuronCluster();
            Brain.Current.Add(iRes);
            IntNeuron iId = new IntNeuron();
            iId.Value = val;
            Brain.Current.Add(iId);
            Link iLink = new Link(iId, iRes, meaning);
            return iRes;
        }

        /// <summary>
        /// Deletes the object and all the children of the object.
        /// </summary>
        /// <param name="toDelete">To delete.</param>
        static public void DeleteObject(NeuronCluster toDelete)
        {
            IList<Neuron> iChildren;
            using (ChildrenAccessor iList = toDelete.Children)                               //don't wrap foreach, this will delete, but for as long as iList is active, the list is locked for reading.
                iChildren = iList.ConvertTo<Neuron>();
            Brain.Current.Delete(toDelete);
            foreach (Neuron i in iChildren)
                Brain.Current.Delete(i);
        }

        public static NeuronCluster CreateFlow()
        {
            NeuronCluster iCluster = new NeuronCluster();
            iCluster.Meaning = (ulong)PredefinedNeurons.Flow;
            Brain.Current.Add(iCluster);
            return iCluster;
        }
    }
}