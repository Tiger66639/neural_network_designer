﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Base class for all lists in the <see cref="Brain"/>.
    /// </summary>
    public class BrainList : IList<ulong>
    {
        #region fields

        private List<ulong> fList = new List<ulong>();
        private WeakReference fLock;

        #endregion fields

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="BrainList"/> class.
        /// </summary>
        internal BrainList()
        {
        }

        #endregion ctor

        #region prop

        #region LinksInLock

        /// <summary>
        /// Gets the lock for the links In list.
        /// </summary>
        /// <value>The links in lock.</value>
        internal ReaderWriterLockSlim Lock
        {
            get
            {
                ReaderWriterLockSlim iLock;
                if (fLock == null)
                {
                    iLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
                    fLock = new WeakReference(iLock);
                }
                else if (fLock.IsAlive == false)
                {
                    iLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
                    fLock.Target = iLock;
                }
                else
                    iLock = (ReaderWriterLockSlim)fLock.Target;
                if (iLock == null)
                    iLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
                return iLock;
            }
        }

        #endregion LinksInLock

        /// <summary>
        /// Gets the internal list of data
        /// </summary>
        /// <value>The list.</value>
        internal List<ulong> List
        {
            get { return fList; }
        }

        #endregion prop

        #region abstract

        /// <summary>
        /// Gets the accessor for the list.
        /// </summary>
        /// <returns>By default, this returns a <see cref="NeuronsAccessor"/>, but can be changed in any descendent.</returns>
        protected internal virtual NeuronsAccessor GetAccessor()
        {
            return new NeuronsAccessor(this, AccessorMode.Read, Lock);
        }

        #endregion abstract

        #region Contains

        /// <summary>
        /// Checks if every item id in the specified list is also in this list.
        /// </summary>
        /// <remarks>
        /// If list doesn't contain any elements, we always return true.
        /// </remarks>
        /// <param name="list">an enumerable containing neuron id's.</param>
        /// <returns>True if all the specified id's are also in this list.</returns>
        public bool Contains(IEnumerable<ulong> list)
        {
            List<ulong> toSearch = new List<ulong>(list);                                    //we make a copy, so we can remove all the items we have found, this should be faster: only have to run over each list 1 time + a search on the smallest for each from the biggest.
            if (toSearch.Count > 0)
            {
                foreach (ulong i in this)
                    toSearch.Remove(i);
                return toSearch.Count == 0;
            }
            else
                return true;
        }

        #endregion Contains

        #region virtual

        /// <summary>
        /// Performs the actual insertion.
        /// </summary>
        /// <remarks>
        /// Makes certain that everything is regisetered + raises the appropriate events.
        /// </remarks>
        /// <param name="index">The index.</param>
        /// <param name="item">The item.</param>
        protected virtual void InternalInsert(int index, Neuron item)
        {
            if (item.ID == Neuron.TempId)
                Brain.Current.Add(item);
            List.Insert(index, item.ID);
            if (Brain.Current.HasNeuronListChangedEvents)
            {
                NeuronListChangedEventArgs iArgs = new NeuronListChangedEventArgs();
                iArgs.Action = NeuronListChangeAction.Insert;
                iArgs.Index = index;
                iArgs.Item = item;
                iArgs.InternalList = this;
                Brain.Current.OnNeuronListChanged(iArgs);
            }
        }

        /// <summary>
        /// Removes the item at the specified index.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        protected virtual bool InternalRemove(Neuron item, int index)
        {
            if (index != -1)
            {
                if (Brain.Current.HasNeuronListChangedEvents)
                {
                    NeuronListChangedEventArgs iArgs = new NeuronListChangedEventArgs();
                    iArgs.Action = NeuronListChangeAction.Remove;
                    iArgs.Index = index;
                    iArgs.Item = item;
                    iArgs.InternalList = this;
                    Brain.Current.OnNeuronListChanged(iArgs);
                }
                List.RemoveAt(index);
                return true;
            }
            else
                return false;
        }

        #endregion virtual

        #region IList<ulong> Members

        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.Generic.IList`1"/>.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.IList`1"/>.</param>
        /// <returns>
        /// The index of <paramref name="item"/> if found in the list; otherwise, -1.
        /// </returns>
        public int IndexOf(ulong item)
        {
            return fList.IndexOf(item);
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.Generic.IList`1"/> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="item"/> should be inserted.</param>
        /// <param name="item">The object to insert into the <see cref="T:System.Collections.Generic.IList`1"/>.</param>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        /// 	<paramref name="index"/> is not a valid index in the <see cref="T:System.Collections.Generic.IList`1"/>.
        /// </exception>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.IList`1"/> is read-only.
        /// </exception>
        public void Insert(int index, ulong item)
        {
            InternalInsert(index, Brain.Current[item]);
        }

        /// <summary>
        /// Performs an insert using a <see cref="Neuron"/> instead of it's id.  This is a bit faster if the Neuron is known
        /// and we are not in a subprocessor with a duplication of the owner.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        public void Insert(int index, Neuron item)
        {
            InternalInsert(index, item);
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.Generic.IList`1"/> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        /// 	<paramref name="index"/> is not a valid index in the <see cref="T:System.Collections.Generic.IList`1"/>.
        /// </exception>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.IList`1"/> is read-only.
        /// </exception>
        public void RemoveAt(int index)
        {
            InternalRemove(Brain.Current[fList[index]], index);
        }

        /// <summary>
        /// Gets or sets the <see cref="System.UInt64"/> at the specified index.
        /// </summary>
        /// <value></value>
        public ulong this[int index]
        {
            get
            {
                return fList[index];
            }
            set
            {
                fList[index] = value;
            }
        }

        #endregion IList<ulong> Members

        #region ICollection<T> Members

        /// <summary>
        /// Adds a range of identifiefs to the list.
        /// </summary>
        /// <param name="range">The range.</param>
        internal void AddRange(IEnumerable<ulong> range)
        {
            foreach (ulong i in range)
                InternalInsert(fList.Count, Brain.Current[i]);
        }

        /// <summary>
        /// Adds a range of identifiefs to the list.
        /// </summary>
        /// <param name="range">The range.</param>
        internal void AddRange(IEnumerable<Neuron> range)
        {
            foreach (Neuron i in range)
                InternalInsert(fList.Count, i);
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        /// </exception>
        public void Add(ulong item)
        {
            InternalInsert(fList.Count, Brain.Current[item]);
        }

        /// <summary>
        /// internal function that should only be used while loading from xml.  it simply adds the item to the
        /// list without updating any references or checking for current processor state.
        /// </summary>
        /// <param name="item">The value to add.</param>
        internal void AddFromLoad(ulong item)
        {
            fList.Add(item);
        }

        /// <summary>
        /// Adds the id of the specified neuron to the list.  This is a little faster if the neuron is known compared
        /// to useing the index.
        /// </summary>
        /// <param name="item"></param>
        public void Add(Neuron item)
        {
            InternalInsert(fList.Count, item);
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        /// </exception>
        public void Clear()
        {
            fList.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1"/> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param>
        /// <returns>
        /// true if <paramref name="item"/> is found in the <see cref="T:System.Collections.Generic.ICollection`1"/>; otherwise, false.
        /// </returns>
        public bool Contains(ulong item)
        {
            return fList.Contains(item);
        }

        public bool Contains(Neuron item)
        {
            return fList.Contains(item.ID);
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1"/> to an <see cref="T:System.Array"/>, starting at a particular <see cref="T:System.Array"/> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array"/> that is the destination of the elements copied from <see cref="T:System.Collections.Generic.ICollection`1"/>. The <see cref="T:System.Array"/> must have zero-based indexing.</param>
        /// <param name="arrayIndex">The zero-based index in <paramref name="array"/> at which copying begins.</param>
        /// <exception cref="T:System.ArgumentNullException">
        /// 	<paramref name="array"/> is null.
        /// </exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        /// 	<paramref name="arrayIndex"/> is less than 0.
        /// </exception>
        /// <exception cref="T:System.ArgumentException">
        /// 	<paramref name="array"/> is multidimensional.
        /// -or-
        /// <paramref name="arrayIndex"/> is equal to or greater than the length of <paramref name="array"/>.
        /// -or-
        /// The number of elements in the source <see cref="T:System.Collections.Generic.ICollection`1"/> is greater than the available space from <paramref name="arrayIndex"/> to the end of the destination <paramref name="array"/>.
        /// -or-
        /// Type <paramref name="T"/> cannot be cast automatically to the type of the destination <paramref name="array"/>.
        /// </exception>
        public void CopyTo(ulong[] array, int arrayIndex)
        {
            fList.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// The number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </returns>
        public int Count
        {
            get { return fList.Count; }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        /// </summary>
        /// <value></value>
        /// <returns>true if the <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only; otherwise, false.
        /// </returns>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param>
        /// <returns>
        /// true if <paramref name="item"/> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1"/>; otherwise, false. This method also returns false if <paramref name="item"/> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </returns>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        /// </exception>
        public bool Remove(ulong item)
        {
            int iIndex = fList.IndexOf(item);
            if (iIndex > -1)
            {
                Neuron iFound;
                Brain.Current.TryFindNeuron(item, out iFound);                                         //we use this type of retrieval, othewise we get an exception, which we don't wan't cause we also want to be able to remove invalid references so we can fix faulty data.
                return InternalRemove(iFound, iIndex);
            }
            else
                return false;
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public bool Remove(Neuron item)
        {
            if (item == null)
                throw new BrainException("Can't remove null from ID list.");
            int iIndex = fList.IndexOf(item.ID);
            if (iIndex > -1)
                return InternalRemove(item, iIndex);
            else
                return false;
        }

        #endregion ICollection<T> Members

        #region IEnumerable Members

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<ulong> GetEnumerator()
        {
            return fList.GetEnumerator();
        }

        #endregion IEnumerable Members

        #region IEnumerable Members

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
        /// </returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return fList.GetEnumerator();
        }

        #endregion IEnumerable Members
    }
}