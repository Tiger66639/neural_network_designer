﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// A thread safe dictionary.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="V"></typeparam>
    public class DictThreadSafe<TKey, TValue> : IDictionary<TKey, TValue>
    {
        #region fields

        private ReaderWriterLockSlim fLock = new ReaderWriterLockSlim();
        private Dictionary<TKey, TValue> fDict;

        #endregion fields

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="DictThreadSafe&lt;TKey, TValue&gt;"/> class.
        /// </summary>
        public DictThreadSafe()
        {
            fDict = new Dictionary<TKey, TValue>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DictThreadSafe&lt;TKey, TValue&gt;"/> class.
        /// </summary>
        /// <param name="source">All items of this dict will be copied into the new dict as initial values.</param>
        public DictThreadSafe(IDictionary<TKey, TValue> source)
        {
            fDict = new Dictionary<TKey, TValue>(source);
        }

        #endregion ctor

        /// <summary>
        /// Provides access to the lock for descendents.
        /// </summary>
        /// <value>The lock.</value>
        protected ReaderWriterLockSlim Lock
        {
            get { return fLock; }
        }

        #region IDictionary<T,V> Members

        /// <summary>
        /// Adds an element with the provided key and value to the <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </summary>
        /// <param name="key">The object to use as the key of the element to add.</param>
        /// <param name="value">The object to use as the value of the element to add.</param>
        /// <exception cref="T:System.ArgumentNullException">
        /// 	<paramref name="key"/> is null.
        /// </exception>
        /// <exception cref="T:System.ArgumentException">
        /// An element with the same key already exists in the <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </exception>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.IDictionary`2"/> is read-only.
        /// </exception>
        public void Add(TKey key, TValue value)
        {
            fLock.EnterWriteLock();
            try
            {
                fDict.Add(key, value);
            }
            finally
            {
                fLock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Non thread safe add.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        protected void InternalAdd(TKey key, TValue value)
        {
            fDict.Add(key, value);
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.IDictionary`2"/> contains an element with the specified key.
        /// </summary>
        /// <param name="key">The key to locate in the <see cref="T:System.Collections.Generic.IDictionary`2"/>.</param>
        /// <returns>
        /// true if the <see cref="T:System.Collections.Generic.IDictionary`2"/> contains an element with the key; otherwise, false.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">
        /// 	<paramref name="key"/> is null.
        /// </exception>
        public bool ContainsKey(TKey key)
        {
            fLock.EnterReadLock();
            try
            {
                return fDict.ContainsKey(key);
            }
            finally
            {
                fLock.ExitReadLock();
            }
        }

        /// <summary>
        /// Gets an <see cref="T:System.Collections.Generic.ICollection`1"/> containing the keys of the <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// An <see cref="T:System.Collections.Generic.ICollection`1"/> containing the keys of the object that implements <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </returns>
        public ICollection<TKey> Keys
        {
            get
            {
                fLock.EnterReadLock();
                try
                {
                    List<TKey> iRes = fDict.Keys.ToList();
                    return iRes;
                }
                finally
                {
                    fLock.ExitReadLock();
                }
            }
        }

        /// <summary>
        /// Removes the element with the specified key from the <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </summary>
        /// <param name="key">The key of the element to remove.</param>
        /// <returns>
        /// true if the element is successfully removed; otherwise, false.  This method also returns false if <paramref name="key"/> was not found in the original <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">
        /// 	<paramref name="key"/> is null.
        /// </exception>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.IDictionary`2"/> is read-only.
        /// </exception>
        public bool Remove(TKey key)
        {
            fLock.EnterWriteLock();
            try
            {
                return fDict.Remove(key);
            }
            finally
            {
                fLock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Gets the value associated with the specified key.
        /// </summary>
        /// <param name="key">The key whose value to get.</param>
        /// <param name="value">When this method returns, the value associated with the specified key, if the key is found; otherwise, the default value for the type of the <paramref name="value"/> parameter. This parameter is passed uninitialized.</param>
        /// <returns>
        /// true if the object that implements <see cref="T:System.Collections.Generic.IDictionary`2"/> contains an element with the specified key; otherwise, false.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">
        /// 	<paramref name="key"/> is null.
        /// </exception>
        public bool TryGetValue(TKey key, out TValue value)
        {
            fLock.EnterReadLock();
            try
            {
                return fDict.TryGetValue(key, out value);
            }
            finally
            {
                fLock.ExitReadLock();
            }
        }

        /// <summary>
        /// Gets an <see cref="T:System.Collections.Generic.ICollection`1"/> containing the values in the <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// An <see cref="T:System.Collections.Generic.ICollection`1"/> containing the values in the object that implements <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </returns>
        public ICollection<TValue> Values
        {
            get
            {
                fLock.EnterReadLock();
                try
                {
                    List<TValue> iRes = fDict.Values.ToList();
                    return iRes;
                }
                finally
                {
                    fLock.ExitReadLock();
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="TValue"/> with the specified key.
        /// </summary>
        /// <value></value>
        public TValue this[TKey key]
        {
            get
            {
                fLock.EnterReadLock();
                try
                {
                    return fDict[key];
                }
                finally
                {
                    fLock.ExitReadLock();
                }
            }
            set
            {
                fLock.EnterWriteLock();
                try
                {
                    fDict[key] = value;
                }
                finally
                {
                    fLock.ExitWriteLock();
                }
            }
        }

        #endregion IDictionary<T,V> Members

        #region ICollection<KeyValuePair<T,V>> Members

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        /// </exception>
        public void Add(KeyValuePair<TKey, TValue> item)
        {
            fLock.EnterWriteLock();
            try
            {
                fDict.Add(item.Key, item.Value);
            }
            finally
            {
                fLock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        /// </exception>
        public void Clear()
        {
            fLock.EnterWriteLock();
            try
            {
                fDict.Clear();
            }
            finally
            {
                fLock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1"/> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param>
        /// <returns>
        /// true if <paramref name="item"/> is found in the <see cref="T:System.Collections.Generic.ICollection`1"/>; otherwise, false.
        /// </returns>
        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            fLock.EnterReadLock();
            try
            {
                return fDict.ContainsKey(item.Key);
            }
            finally
            {
                fLock.ExitReadLock();
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1"/> to an <see cref="T:System.Array"/>, starting at a particular <see cref="T:System.Array"/> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array"/> that is the destination of the elements copied from <see cref="T:System.Collections.Generic.ICollection`1"/>. The <see cref="T:System.Array"/> must have zero-based indexing.</param>
        /// <param name="arrayIndex">The zero-based index in <paramref name="array"/> at which copying begins.</param>
        /// <exception cref="T:System.ArgumentNullException">
        /// 	<paramref name="array"/> is null.
        /// </exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        /// 	<paramref name="arrayIndex"/> is less than 0.
        /// </exception>
        /// <exception cref="T:System.ArgumentException">
        /// 	<paramref name="array"/> is multidimensional.
        /// -or-
        /// <paramref name="arrayIndex"/> is equal to or greater than the length of <paramref name="array"/>.
        /// -or-
        /// The number of elements in the source <see cref="T:System.Collections.Generic.ICollection`1"/> is greater than the available space from <paramref name="arrayIndex"/> to the end of the destination <paramref name="array"/>.
        /// -or-
        /// Type <paramref name="T"/> cannot be cast automatically to the type of the destination <paramref name="array"/>.
        /// </exception>
        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// The number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </returns>
        public int Count
        {
            get
            {
                fLock.EnterReadLock();
                try
                {
                    return fDict.Count;
                }
                finally
                {
                    fLock.ExitReadLock();
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        /// </summary>
        /// <value></value>
        /// <returns>true if the <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only; otherwise, false.
        /// </returns>
        public bool IsReadOnly
        {
            get
            {
                return fLock.IsWriteLockHeld;
            }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param>
        /// <returns>
        /// true if <paramref name="item"/> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1"/>; otherwise, false. This method also returns false if <paramref name="item"/> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </returns>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        /// </exception>
        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            fLock.EnterWriteLock();
            try
            {
                return fDict.Remove(item.Key);
            }
            finally
            {
                fLock.ExitWriteLock();
            }
        }

        #endregion ICollection<KeyValuePair<T,V>> Members

        #region IEnumerable<KeyValuePair<T,V>> Members

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1"/> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            fLock.EnterReadLock();
            try
            {
                foreach (KeyValuePair<TKey, TValue> i in fDict)
                    yield return i;
            }
            finally
            {
                fLock.ExitReadLock();
            }
        }

        #endregion IEnumerable<KeyValuePair<T,V>> Members

        #region IEnumerable Members

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
        /// </returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            fLock.EnterReadLock();
            try
            {
                foreach (KeyValuePair<TKey, TValue> i in fDict)
                    yield return i;
            }
            finally
            {
                fLock.ExitReadLock();
            }
        }

        #endregion IEnumerable Members

        /// <summary>
        /// Provides a quick, thread safe way to copy all the items from the source into this dictionary.
        /// </summary>
        /// <param name="source">The source.</param>
        public void CopyFrom(IDictionary<TKey, TValue> source)
        {
            fLock.EnterWriteLock();
            try
            {
                foreach (KeyValuePair<TKey, TValue> i in source)
                    Add(i.Key, i.Value);
            }
            finally
            {
                fLock.ExitWriteLock();
            }
        }
    }
}