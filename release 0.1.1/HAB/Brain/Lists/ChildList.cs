﻿namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// A list used by <see cref="NeuronCluster"/>s to store the children. Note: always put an
    /// operation on this list in a lock on the list.
    /// </summary>
    /// <remarks>
    /// This list is not thread save, so ANY operation made on this list (including foreach), should
    /// be performed within a lock statement.  It is not done on the list itself since it is impossible
    /// to reach all situations (for instance indexOf is thread save, replace as well, but if those 2
    /// statements are done in sequence as in get the index of someting and replace it, the operation
    /// is not thread save.  So lock it.
    /// </remarks>
    /// <typeparam name="T">The type of <see cref="Neuron"/>.</typeparam>
    public class ChildList : OwnedBrainList
    {
        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="NeuronList"/> class.
        /// </summary>
        /// <param name="owner">The owner.</param>
        internal ChildList(Neuron owner) : base(owner)
        {
        }

        #endregion ctor

        #region overrides

        /// <summary>
        /// Gets the accessor for the list.
        /// </summary>
        /// <returns>
        /// By default, this returns a <see cref="NeuronsAccessor"/>, but can be changed in any descendent.
        /// </returns>
        protected internal override NeuronsAccessor GetAccessor()
        {
            return new ChildrenAccessor(this, AccessorMode.Read, Lock);                                  //always need read access, even if we only want to know a count.
        }

        /// <summary>
        /// Performs the actual insertion.
        /// </summary>
        /// <remarks>
        /// Makes certain that everything is regisetered + raises the appropriate events.
        /// </remarks>
        /// <param name="index">The index.</param>
        /// <param name="item">The item.</param>
        protected override void InternalInsert(int index, Neuron item)
        {
            base.InternalInsert(index, item);
            NeuronCluster iCluster = Owner as NeuronCluster;
            if (iCluster != null)
            {
                if (item.ID == Neuron.TempId)
                    Brain.Current.Add(item);
                item.AddClusteredBy(iCluster);
            }
        }

        /// <summary>
        /// Removes the item at the specified index.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        protected override bool InternalRemove(Neuron item, int index)
        {
            bool iRes = base.InternalRemove(item, index);
            if (iRes == true)
            {
                NeuronCluster iCluster = Owner as NeuronCluster;
                if (item != null && iCluster != null)
                    item.RemoveClusteredBy(iCluster);
            }
            return iRes;
        }

        /// <summary>
        /// performs an insert and raises the event but doesn't change anything in the neuron being added.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="index">The index.</param>
        internal void InsertDirect(int index, Neuron item)
        {
            base.InternalInsert(index, item);
        }

        /// <summary>
        /// Performs a remove and raises the events but doesn't change anything in the neuron being added.
        /// </summary>
        /// <param name="item">The item.</param>
        internal void RemoveDirect(Neuron item)
        {
            if (item != null)
            {
                int iIndex = IndexOf(item.ID);
                if (iIndex > -1)                                                                 //we do a consistency check to avoid errors, this is an internal function anyway, simply to keep things in sync, so if it wasn't there, the remove is still ok, cause we are in sync again.
                    base.InternalRemove(item, iIndex);
            }
        }

        #endregion overrides
    }
}