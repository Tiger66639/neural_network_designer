﻿namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Represents a <see cref="Neuron"/> that the <see cref="Sin"/>s produce, based on the input they receive.
    /// This object is used to update the brain's memory (<see cref="Brain.this"/>) and send to the Sins for output by the brain.
    /// </summary>
    public class KnoledgeNeuron : Neuron
    {
        //public Neuron Who
        //{
        //   get
        //   {
        //      throw new NotImplementedException();
        //   }
        //}
        //public Neuron Where
        //{
        //   get
        //   {
        //      throw new NotImplementedException();
        //   }
        //}
        //public Neuron How
        //{
        //   get
        //   {
        //      throw new NotImplementedException();
        //   }
        //}
        //public Neuron Action
        //{
        //   get
        //   {
        //      throw new NotImplementedException();
        //   }
        //}
        //public Neuron Why
        //{
        //   get
        //   {
        //      throw new NotImplementedException();
        //   }
        //}
        //public Neuron When
        //{
        //   get
        //   {
        //      throw new NotImplementedException();
        //   }
        //}
    }
}