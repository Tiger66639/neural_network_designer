﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Implements the <see cref="ILongtermMem"/> interface.  It saves the neurons to xml files in the path specified by
    /// <see cref="LongtermMem.NeuronsPath"/>
    /// </summary>
    [XmlRootAttribute("Memory", Namespace = "https://NeuralNetworkDesigne", IsNullable = false)]
    public class LongtermMem : ILongtermMem
    {
        #region fields

        private string fDataPath;

        #endregion fields

        #region DataPath

        /// <summary>
        /// Gets/sets the path to the location where all the neurons are saved as files.
        /// </summary>
        /// <remarks>
        /// the filename is the ID, with extention 'xml'.
        /// </remarks>
        public string DataPath
        {
            get
            {
                return fDataPath;
            }
            set
            {
                fDataPath = value;
            }
        }

        #endregion DataPath

        /// <summary>
        /// Tries to load the neuron with the specified ID
        /// </summary>
        /// <remarks>
        /// <para>
        /// If the id can't be found null is returned.
        /// </para>
        /// the filename is the ID, with extention 'xml'. the path is defined in <see cref="LongtermMem.NeuronsPath"/>
        /// <para>
        /// errors are written to the log.
        /// </para>
        /// </remarks>
        /// <param name="id">The id of the neuron to load.</param>
        /// <returns>The object that was loaded or null if not found.</returns>
        public Neuron GetNeuron(ulong id)
        {
            if (string.IsNullOrEmpty(DataPath) == false)
            {
                string iPath = Path.Combine(DataPath, id.ToString() + ".xml");
                if (File.Exists(iPath) == true)
                {
                    XmlSerializer iSer = new XmlSerializer(typeof(XmlStore));
                    using (StreamReader iReader = new StreamReader(iPath))
                    {
                        XmlStore iTemp = iSer.Deserialize(iReader) as XmlStore;
                        if (iTemp != null)
                            return iTemp.Data;
                        else
                            return null;
                    }
                }
                else
                {
                    if (Settings.LogNeuronNotFoundInLongTermMem == true)
                        Log.LogWarning("LongtermMemory.GetNeuron", "File not found: " + iPath);
                    return null;
                }
            }
            else
            {
                Log.LogError("LongtermMemory.GetNeuron", "Can't search for parked neurons, NeuronsPath not yet defined!");
                return null;
            }
        }

        /// <summary>
        /// Tries to save the neuron to the long time memory.
        /// </summary>
        /// <remarks>
        /// This implementation of <see cref="ILongtermMem"/> stores the neuron to an xml file.
        /// </remarks>
        /// <param name="toSave">The object to save.</param>
        public void SaveNeuron(Neuron toSave)
        {
            if (string.IsNullOrEmpty(DataPath) == true || Directory.Exists(DataPath) == false)
                throw new BrainException("Can't save the Neuron because there is no valid NeuronPath defined.  Please provide the proper setup info.");
            if (toSave.ID == Neuron.EmptyId)
                throw new BrainException("Can't save the Neuron because it has no valid ID.  Has it already been added to the Brain?");
            XmlSerializer iSer = new XmlSerializer(typeof(XmlStore));
            string iPath = Path.Combine(DataPath, toSave.ID.ToString() + ".xml");

            using (TextWriter iWriter = new StreamWriter(iPath))
            {
                XmlStore iTemp = new XmlStore() { Data = toSave };
                iSer.Serialize(iWriter, iTemp);
            }
            toSave.IsChanged = false;                                                                    //it's been saved, so let the neuron know.
        }

        /// <summary>
        /// Removes the specified <see cref="Neuron"/> from the long term memory.
        /// </summary>
        /// <param name="toRemove">The object to remove</param>
        public void RemoveNeuron(Neuron toRemove)
        {
            if (toRemove != null)
                RemoveNeuron(toRemove.ID);
        }

        /// <summary>
        /// Removes the specified <see cref="Neuron"/> from the long term memory.
        /// </summary>
        /// <param name="toRemove">The id of the neuron to remove.</param>
        public void RemoveNeuron(ulong toRemove)
        {
            if (string.IsNullOrEmpty(DataPath) == false)
            {
                string iPath = Path.Combine(DataPath, toRemove.ToString() + ".xml");
                File.Delete(iPath);                                                                          //don't need to check if the file exists, cause the function doesn't throw an exception when it doesn't.
            }
        }

        /// <summary>
        /// Retrieves a list of neurons that form the entry points
        /// </summary>
        /// <param name="type">The type for which to retrieve the data.</param>
        /// <returns></returns>
        public IList<Neuron> GetEntryNeuronsFor(Type type)
        {
            if (DataPath != null)
            {
                List<Neuron> iRes = new List<Neuron>();
                string iPath = Path.Combine(DataPath, "EntryPointsFor" + type.ToString() + ".dat");
                if (File.Exists(iPath) == true)
                {
                    using (FileStream iStr = new FileStream(iPath, FileMode.Open, FileAccess.Read))
                    {
                        using (BinaryReader iReader = new BinaryReader(iStr))
                        {
                            while (iStr.Position != iStr.Length)
                            {
                                ulong iId = iReader.ReadUInt64();
                                try
                                {
                                    Neuron iFound;
                                    if (Brain.Current.TryFindNeuron(iId, out iFound) == true)
                                        iRes.Add(iFound);
                                    else
                                        Log.LogWarning("LongtermMem.GetEntryNeuronsFor", string.Format("{0} not found in brain, skipped as entry point.", iId));
                                }
                                catch (Exception e)
                                {
                                    Log.LogWarning("LongtermMem.GetEntryNeuronsFor", string.Format("Failed to find entry point for Sin type: '{0}' with error: {1}.", type, e));
                                }
                            }
                        }
                    }
                }
                return iRes;
            }
            else
            {
                Log.LogError("LongtermMem.GetEntryNeuronsFor", string.Format("No NeuronsPath defined: failed to find entry point for Sin type {0}.", type.ToString()));
                return null;
            }
        }

        /// <summary>
        /// Stores the list of neurons as the default entry points for the specified type.
        /// </summary>
        /// <param name="list">The list of neurons that need to be stored as entry points.  Note that these Neurons are already
        /// stored as regular neurons in the brain, so you should only store the Id of the neurons.</param>
        /// <param name="type">The Sin type for which to perform the store.</param>
        public void SaveEntryNeuronsFor(IEnumerable<Neuron> list, Type type)
        {
            string iPath = Path.Combine(DataPath, "EntryPointsFor" + type.ToString() + ".dat");
            using (FileStream iStr = new FileStream(iPath, FileMode.Create, FileAccess.Write))
            {
                using (BinaryWriter iWriter = new BinaryWriter(iStr))
                {
                    foreach (Neuron i in list)
                        iWriter.Write(i.ID);
                }
            }
        }

        /// <summary>
        /// Stores the list of neurons as the default entry points for the specified type.
        /// </summary>
        /// <param name="list">The list of neurons that need to be stored as entry points.  Note that these Neurons are already
        /// stored as regular neurons in the brain, so you should only store the Id of the neurons.</param>
        /// <param name="type">The Sin type for which to perform the store.</param>
        public void SaveEntryNeuronsFor(IEnumerable<ulong> list, Type type)
        {
            string iPath = Path.Combine(DataPath, "EntryPointsFor" + type.ToString() + ".dat");
            using (FileStream iStr = new FileStream(iPath, FileMode.Create, FileAccess.Write))
            {
                using (BinaryWriter iWriter = new BinaryWriter(iStr))
                {
                    foreach (ulong i in list)
                        iWriter.Write(i);
                }
            }
        }

        /// <summary>
        /// Gets extra data associated with the specified type and id from the store.
        /// </summary>
        /// <typeparam name="T">The type of the property value that should be read.</typeparam>
        /// <param name="type">The type for which to get the property value.</param>
        /// <param name="id">The id of the property value that should be retrieved.</param>
        /// <returns></returns>
        /// <remarks>
        /// 	<para>
        /// This is primarely used by sins to retrieve extra information from the brain that they require for proper processing.
        /// </para>
        /// 	<para>
        /// The data should be stored in xml format.
        /// </para>
        /// </remarks>
        public T GetProperty<T>(Type type, string id) where T : class
        {
            if (DataPath != null)
            {
                string iPath = Path.Combine(DataPath, type.ToString() + "_" + id + ".xml");
                if (File.Exists(iPath) == true)
                {
                    using (FileStream iStr = new FileStream(iPath, FileMode.Open, FileAccess.Read))
                    {
                        XmlSerializer valueSerializer = new XmlSerializer(typeof(T));
                        return (T)valueSerializer.Deserialize(iStr);
                    }
                }
                return null;
            }
            else
            {
                Log.LogError("Memory.GetProperty", string.Format("No NeuronsPath defined: failed to find property values for Sin type {0} - {1}.", type.ToString(), id));
                return null;
            }
        }

        /// <summary>
        /// Saves extra data associated with the specified type and id from the store.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="id">The id of the property value that should be saved.</param>
        /// <param name="value">The value that needs to be saved.</param>
        /// <remarks>
        /// See <see cref="ILongtermMem.GetProperty"/> for more info.
        /// </remarks>
        public void SaveProperty(Type type, string id, object value)
        {
            string iPath = Path.Combine(DataPath, type.ToString() + "_" + id + ".xml");
            using (FileStream iStr = new FileStream(iPath, FileMode.Create, FileAccess.Write))
            {
                XmlSerializer valueSerializer = new XmlSerializer(value.GetType());
                valueSerializer.Serialize(iStr, value);
            }
        }

        /// <summary>
        /// Copies all the data to the specified location.  This operation doesn't effect the value of <see cref="ILongtermMem.DataPath"/>.
        /// </summary>
        /// <param name="loc">The location to copy the data to.</param>
        public void CopyTo(string loc)
        {
            foreach (string i in Directory.GetFiles(DataPath))                                           //we copy all the files, don't filter on any file type so we make certain we have everything. (SVN stores in subdir which isn't copied over) -> subdirs of SVN gave problems while deleting the sandbox.
                File.Copy(i, Path.Combine(loc, Path.GetFileName(i)));
        }

        /// <summary>
        /// Determines whether the specified id exists.
        /// </summary>
        /// <param name="id">The id to look for.</param>
        /// <returns>
        /// 	<c>true</c> if the id exists in the db; otherwise, <c>false</c>.
        /// </returns>
        public bool IsExistingID(ulong id)
        {
            if (string.IsNullOrEmpty(DataPath) == false)
            {
                string iPath = Path.Combine(DataPath, id.ToString() + ".xml");
                return File.Exists(iPath) == true;
            }
            else
                return false;
        }
    }
}