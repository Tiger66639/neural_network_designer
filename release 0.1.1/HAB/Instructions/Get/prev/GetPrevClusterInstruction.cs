﻿using System.Collections.Generic;
using System.Linq;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Returns the next cluster that owns the specified neuron and has the specified meaning.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// 1: The neuron for which to find a cluster that it belongs to.
    /// 2: the meaning that should be assigned to the cluster that needs to be returned.
    /// 3: the cluster that should be just in front of the result.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.GetPrevClusterInstruction)]
    public class GetPrevClusterInstruction : SingleResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.GetPrevClusterInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.GetPrevClusterInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Gets the actual value.
        /// </summary>
        /// <param name="processor">The processor to use.</param>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The result of the instruction.</returns>
        protected override Neuron InternalGetValue(Processor processor, IList<Neuron> list)
        {
            if (list != null && list.Count >= 3)
            {
                Neuron iChild = list[0];
                Neuron iMeaning = list[1];
                NeuronCluster iNext = list[2] as NeuronCluster;
                if (iChild != null)
                    if (iMeaning != null)
                    {
                        if (iNext != null)
                        {
                            using (NeuronsAccessor iClusteredBy = iChild.ClusteredBy)
                            {
                                var iClusters = (from i in iClusteredBy.Items select Brain.Current[i] as NeuronCluster).Reverse();
                                bool iFound = false;
                                foreach (NeuronCluster i in iClusters)
                                {
                                    if (iFound == false)
                                    {
                                        if (i == iNext)
                                            iFound = true;
                                    }
                                    else if (i.Meaning == iMeaning.ID)
                                        return i;
                                }
                            }
                        }
                        else
                            Log.LogError("GetNextClusterInstruction.InternalGetValue", "Invalid third argument, NeuronCluster expected.");
                    }
                    else
                        Log.LogError("GetNextClusterInstruction.InternalGetValue", "Invalid second argument, Neuron expected, found null.");
                else
                    Log.LogError("GetNextClusterInstruction.InternalGetValue", "Invalid first argument, Neuron expected, found null.");
            }
            else
                Log.LogError("GetNextClusterInstruction.InternalGetValue", "Invalid nr of arguments specified");
            return null;
        }

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 3; }
        }
    }
}