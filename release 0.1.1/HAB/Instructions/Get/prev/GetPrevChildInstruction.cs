﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Gets the prev child of the cluster.
    /// </summary>
    /// <remarks>
    /// Arg:
    /// 1: The cluster for which to return the next child.
    ///    if no children, null is returned.
    /// 2: the next child
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.GetPrevChildInstruction)]
    public class GetPrevChildInstruction : SingleResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.GetPrevChildInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.GetPrevChildInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Gets the actual value.
        /// </summary>
        /// <param name="processor">The processor to use.</param>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The result of the instruction.</returns>
        protected override Neuron InternalGetValue(Processor processor, IList<Neuron> list)
        {
            Neuron iRes = null;
            if (list.Count >= 2)
            {
                NeuronCluster iCluster = list[0] as NeuronCluster;
                if (iCluster != null)
                {
                    Neuron iPrev = list[1];
                    if (iPrev != null)
                    {
                        using (ChildrenAccessor iChildren = iCluster.Children)
                        {
                            int iIndex = iChildren.IndexOf(iPrev.ID);
                            if (iIndex > 0)
                                iRes = Brain.Current[iChildren[iIndex - 1]];
                        }
                    }
                    else
                        Log.LogError("GetPrevChildInstruction.InternalGetValue", "Invalid second argument, Neuron expected, found null.");
                }
                else
                    Log.LogError("GetPrevChildInstruction.InternalGetValue", "Invalid first argument, NeuronCluster expected.");
            }
            else
                Log.LogError("GetPrevChildInstruction.InternalGetValue", "Invalid nr of arguments specified");
            return iRes;
        }

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 2; }
        }
    }
}