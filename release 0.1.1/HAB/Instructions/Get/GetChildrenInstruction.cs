﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Returns all the children of a <see cref="NeuronCluster"/>.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Arguments:
    /// - the cluster from which to return all the children.
    /// </para>
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.GetChildrenInstruction)]
    public class GetChildrenInstruction : MultiResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.GetChildrenInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.GetChildrenInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// performs the task and returns it's result.
        /// </summary>
        /// <param name="processor"></param>
        /// <param name="list">The list of arguments</param>
        /// <returns>The list of results</returns>
        /// <remarks>
        /// Note: when When a result instruction is executed, ( so <see cref="ResultInstruction.Execute"/> is
        /// called instead of <see cref="ResultInstruction.GetValues"/>, the result value(s) are pushed on the
        /// execution stack.
        /// </remarks>
        public override IEnumerable<Neuron> GetValues(Processor processor, IList<Neuron> list)
        {
            if (list != null && list.Count >= 1)
            {
                NeuronCluster iCluster = list[0] as NeuronCluster;
                if (iCluster != null)
                    using (ChildrenAccessor iList = iCluster.Children)
                        return iList.ConvertTo<Neuron>();
                else
                    Log.LogError("GetChildren.GetValues", "First argument should be a NeuronCluster!");
            }
            else
                Log.LogError("GetChildren.GetValues", "No arguments specified");
            return new List<Neuron>();                                                                            //when we get here, simply return an empty result.
        }

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 1; }
        }
    }
}