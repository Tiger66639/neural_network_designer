﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Gets the first child of the cluster.
    /// </summary>
    /// <remarks>
    /// Arg:
    /// 1: The cluster for which to retur the first child.
    ///    if no children, null is returned.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.GetFirstChildInstruction)]
    public class GetFirstChildInstruction : SingleResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.GetFirstChildInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.GetFirstChildInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Gets the actual value.
        /// </summary>
        /// <param name="processor">The processor to use.</param>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The result of the instruction.</returns>
        protected override Neuron InternalGetValue(Processor processor, IList<Neuron> list)
        {
            Neuron iRes = null;
            if (list.Count >= 1)
            {
                NeuronCluster iCluster = list[0] as NeuronCluster;
                if (iCluster != null)
                {
                    using (ChildrenAccessor iChildren = iCluster.Children)
                    {
                        if (iChildren.Count > 0)
                            iRes = Brain.Current[iChildren[0]];
                    }
                }
                else
                    Log.LogError("GetFirstChildInstruction.InternalGetValue", "Invalid first argument, NeuronCluster expected, found null.");
            }
            else
                Log.LogError("GetFirstChildInstruction.InternalGetValue", "Invalid nr of arguments specified");
            return iRes;
        }

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 1; }
        }
    }
}