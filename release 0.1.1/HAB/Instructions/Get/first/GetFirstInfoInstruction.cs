﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Gets the first neuron found on the <see cref="Link.Info"/> section found by the
    /// specified arguments.
    /// </summary>
    /// <remarks>
    /// Arg:
    /// 1: From part
    /// 2: To part
    /// 3: meaning part
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.GetFirstInfoInstruction)]
    public class GetFirstInfoInstruction : SingleResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.GetFirstInfoInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.GetFirstInfoInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Gets the actual value.
        /// </summary>
        /// <param name="processor">The processor to use.</param>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The result of the instruction.</returns>
        protected override Neuron InternalGetValue(Processor processor, IList<Neuron> list)
        {
            Neuron iRes = null;
            if (list.Count >= 3)
            {
                Neuron iFrom = list[0];
                if (iFrom != null)
                {
                    Neuron iTo = list[1];
                    if (iTo != null)
                    {
                        Neuron iMeaning = list[2];
                        if (iMeaning != null)
                        {
                            Link iFound = Link.Find(iTo, iFrom, iMeaning);
                            if (iFound != null)
                            {
                                using (LinkInfoAccessor iInfo = iFound.Info)
                                {
                                    if (iInfo.Count > 0)
                                        iRes = Brain.Current[iInfo[0]];
                                }
                            }
                            else
                                Log.LogWarning("GetFirstInfoInstruction.InternalGetValue", "Link doesn't exist, can't return first info item.");
                        }
                        else
                            Log.LogError("GetFirstInfoInstruction.InternalGetValue", "Invalid third argument, Neuron expected, found null.");
                    }
                    else
                        Log.LogError("GetFirstInfoInstruction.InternalGetValue", "Invalid second argument, Neuron expected, found null.");
                }
                else
                    Log.LogError("GetFirstInfoInstruction.InternalGetValue", "Invalid first argument, Neuron expected, found null.");
            }
            else
                Log.LogError("GetFirstInfoInstruction.InternalGetValue", "Invalid nr of arguments specified");
            return iRes;
        }

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 3; }
        }
    }
}