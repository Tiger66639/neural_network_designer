﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Gets the next neuron found on the <see cref="Link.Info"/> section found by the
    /// specified arguments.
    /// </summary>
    /// <remarks>
    /// Arg:
    /// 1: From part
    /// 2: To part
    /// 3: meaning part
    /// 4: prev item
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.GetNextInfoInstruction)]
    public class GetNextInfoInstruction : SingleResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.GetNextInfoInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.GetNextInfoInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Gets the actual value.
        /// </summary>
        /// <param name="processor">The processor to use.</param>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The result of the instruction.</returns>
        protected override Neuron InternalGetValue(Processor processor, IList<Neuron> list)
        {
            Neuron iRes = null;
            if (list.Count >= 4)
            {
                Neuron iFrom = list[0];
                if (iFrom != null)
                {
                    Neuron iTo = list[1];
                    if (iTo != null)
                    {
                        Neuron iMeaning = list[2];
                        if (iMeaning != null)
                        {
                            Link iFound = Link.Find(iTo, iFrom, iMeaning);
                            if (iFound != null)
                            {
                                Neuron iPrev = list[3];
                                if (iPrev != null)
                                {
                                    using (LinkInfoAccessor iInfo = iFound.Info)
                                    {
                                        int iIndex = iInfo.IndexOf(iPrev.ID);
                                        if (iIndex > -1 && iIndex < iInfo.Count - 1)
                                            iRes = Brain.Current[iInfo[iIndex + 1]];
                                    }
                                }
                                else
                                    Log.LogError("GetNextInfoInstruction.InternalGetValue", "Invalid 4th argument, Neuron expected, found null.");
                            }
                            else
                                Log.LogWarning("GetNextInfoInstruction.InternalGetValue", "Link doesn't exist, can't return first info item.");
                        }
                        else
                            Log.LogError("GetNextInfoInstruction.InternalGetValue", "Invalid third argument, Neuron expected, found null.");
                    }
                    else
                        Log.LogError("GetNextInfoInstruction.InternalGetValue", "Invalid second argument, Neuron expected, found null.");
                }
                else
                    Log.LogError("GetNextInfoInstruction.InternalGetValue", "Invalid first argument, Neuron expected, found null.");
            }
            else
                Log.LogError("GetNextInfoInstruction.InternalGetValue", "Invalid nr of arguments specified");
            return iRes;
        }

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 4; }
        }
    }
}