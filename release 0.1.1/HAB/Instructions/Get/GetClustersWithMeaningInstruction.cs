﻿using System.Collections.Generic;
using System.Linq;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Returns all the clusters with the specified meaning.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// 1: The Neuron for which to return the owning clusters
    /// 2: the neuron that idenfities the meaning of the clusters to find.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.GetClustersWithMeaningInstruction)]
    public class GetClustersWithMeaningInstruction : MultiResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.IndexOfChildInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.GetClustersWithMeaningInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// performs the task and returns it's result.
        /// </summary>
        /// <param name="processor"></param>
        /// <param name="list">The list of arguments</param>
        /// <returns>The list of results</returns>
        /// <remarks>
        /// Note: when When a result instruction is executed, ( so <see cref="ResultInstruction.Execute"/> is
        /// called instead of <see cref="ResultInstruction.GetValues"/>, the result value(s) are pushed on the
        /// execution stack.
        /// </remarks>
        public override IEnumerable<Neuron> GetValues(Processor processor, IList<Neuron> list)
        {
            List<Neuron> iRes = new List<Neuron>();
            if (list != null && list.Count >= 2)
            {
                Neuron iChild = list[0];
                Neuron iMeaning = list[1];
                if (iChild != null)
                    if (iMeaning != null)
                    {
                        using (NeuronsAccessor iClusteredBy = iChild.ClusteredBy)
                        {
                            var iClusters = from i in iClusteredBy.Items select Brain.Current[i] as NeuronCluster;
                            foreach (NeuronCluster i in iClusters)
                            {
                                if (i.Meaning == iMeaning.ID)
                                    iRes.Add(i);
                            }
                        }
                    }
                    else
                        Log.LogError("GetClustersWithMeaningInstruction.InternalGetValue", "Invalid second argument, Neuron expected, found null.");
                else
                    Log.LogError("GetClustersWithMeaningInstruction.InternalGetValue", "Invalid first argument, Neuron expected, found null.");
            }
            else
                Log.LogError("GetClustersWithMeaningInstruction.InternalGetValue", "Invalid nr of arguments specified");
            return iRes;
        }

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 2; }
        }
    }
}