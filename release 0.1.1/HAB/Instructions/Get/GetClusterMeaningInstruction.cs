﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Returns the meaning of a cluster.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// - 1: The cluster for which to return the meaning. If it has no meaning, <see cref="PredefinedNeurons.Empty"/> is returned.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.GetClusterMeaningInstruction)]
    public class GetClusterMeaningInstruction : SingleResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ConditionalPart"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.GetClusterMeaningInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Gets the actual value.
        /// </summary>
        /// <param name="processor">The processor to use.</param>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The result of the instruction.</returns>
        protected override Neuron InternalGetValue(Processor processor, IList<Neuron> list)
        {
            if (list != null && list.Count >= 1)
            {
                NeuronCluster iCluster = list[0] as NeuronCluster;
                if (iCluster != null)
                {
                    if (iCluster.Meaning != Neuron.EmptyId)
                        return Brain.Current[iCluster.Meaning];
                    else
                        return null;
                }
                else
                    Log.LogError("GetClusterMeaningInstruction.GetValues", "First argument should be a NeuronCluster!");
            }
            else
                Log.LogError("GetClusterMeaningInstruction.GetValues", "No arguments specified");
            return null;
        }

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 1; }
        }
    }
}