﻿using System.Collections.Generic;
using System.Linq;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Returns the last cluster that owns the specified neuron and has the specified meaning.
    /// </summary>
    /// <remarks>
    /// <para>
    /// This is a shortcut instruction that combines a few statements (getClusters, loop, check meaning).
    /// </para>
    /// Arguments:
    /// 1: The neuron for which to find a cluster that it belongs to.
    /// 2: the meaning that should be assigned to the cluster that needs to be returned.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.GetLastClusterInstruction)]
    public class GetLastClusterInstruction : SingleResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.GetLastClusterInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.GetLastClusterInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Gets the actual value.
        /// </summary>
        /// <param name="processor">The processor to use.</param>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The result of the instruction.</returns>
        protected override Neuron InternalGetValue(Processor processor, IList<Neuron> list)
        {
            if (list != null && list.Count >= 2)
            {
                Neuron iChild = list[0];
                Neuron iMeaning = list[1];
                if (iChild != null)
                    if (iMeaning != null)
                    {
                        using (NeuronsAccessor iClusteredBy = iChild.ClusteredBy)
                        {
                            var iClusters = (from i in iClusteredBy.Items select Brain.Current[i] as NeuronCluster).Reverse();   //by reversing the order, we get the last one first.
                            foreach (NeuronCluster i in iClusters)
                            {
                                if (i.Meaning == iMeaning.ID)
                                    return i;
                            }
                        }
                    }
                    else
                        Log.LogError("GetLastClusterInstruction.InternalGetValue", "Invalid second argument, Neuron expected, found null.");
                else
                    Log.LogError("GetLastClusterInstruction.InternalGetValue", "Invalid first argument, Neuron expected, found null.");
            }
            else
                Log.LogError("GetLastClusterInstruction.InternalGetValue", "Invalid nr of arguments specified");
            return null;
        }

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 2; }
        }
    }
}