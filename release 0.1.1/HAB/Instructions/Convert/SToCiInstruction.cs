﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Converts a string neuron into a cluster containing int neurons, where each neuron represents a single ascii char of the string.
    /// It is possible to convert multiple items at once.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// - 1 or more string neurons
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.SToCiInstruction)]
    public class SToCiInstruction : MultiResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.SToCiInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.SToCiInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return -1; }
        }

        /// <summary>
        /// performs the task and returns it's result.
        /// </summary>
        /// <param name="processor"></param>
        /// <param name="list">The list of arguments</param>
        /// <returns>The list of results</returns>
        /// <remarks>
        /// Note: when When a result instruction is executed, ( so <see cref="ResultInstruction.Execute"/> is
        /// called instead of <see cref="ResultInstruction.GetValues"/>, the result value(s) are pushed on the
        /// execution stack.
        /// </remarks>
        public override IEnumerable<Neuron> GetValues(Processor processor, IList<Neuron> list)
        {
            if (list != null && list.Count >= 1)
            {
                List<Neuron> iList = new List<Neuron>();
                foreach (Neuron iNeuron in list)
                {
                    TextNeuron iPar = iNeuron as TextNeuron;
                    if (iPar != null)
                    {
                        NeuronCluster iRes = new NeuronCluster();
                        Brain.Current.Add(iRes);
                        using (ChildrenAccessor iResList = iRes.ChildrenW)
                        {
                            foreach (char i in iPar.Text)
                            {
                                IntNeuron iItem = new IntNeuron();
                                iItem.Value = (int)i;
                                Brain.Current.Add(iItem);
                                iResList.Add(iItem);
                            }
                        }
                        iList.Add(iRes);
                    }
                    else
                        Log.LogError("SToCcInstruction.GetValues", "arguments must be text neurons!");
                }
                return iList;
            }
            else
                Log.LogError("SToCcInstruction.GetValues", "Invalid nr of arguments specified!");
            return new List<Neuron>();
        }
    }
}