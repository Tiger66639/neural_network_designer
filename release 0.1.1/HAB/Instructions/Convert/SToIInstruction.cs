﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Converts a string neuron into an integer neuron. When the TextNeuron can't be converted into an int, nothing is returned.
    /// It is possible to convert multiple items at once.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// - a list of string neurons.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.SToIInstruction)]
    public class SToIInstruction : MultiResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.SToIInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.SToIInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return -1; }
        }

        /// <summary>
        /// performs the task and returns it's result.
        /// </summary>
        /// <param name="processor"></param>
        /// <param name="list">The list of arguments</param>
        /// <returns>The list of results</returns>
        /// <remarks>
        /// Note: when When a result instruction is executed, ( so <see cref="ResultInstruction.Execute"/> is
        /// called instead of <see cref="ResultInstruction.GetValues"/>, the result value(s) are pushed on the
        /// execution stack.
        /// </remarks>
        public override IEnumerable<Neuron> GetValues(Processor processor, IList<Neuron> list)
        {
            if (list != null && list.Count >= 1)
            {
                List<Neuron> iList = new List<Neuron>();
                foreach (Neuron iNeuron in list)
                {
                    TextNeuron iPar = iNeuron as TextNeuron;
                    if (iPar != null)
                    {
                        int iVal;
                        if (int.TryParse(iPar.Text, out iVal) == true)
                        {
                            IntNeuron iRes = new IntNeuron();
                            iRes.Value = iVal;
                            Brain.Current.Add(iRes);
                            iList.Add(iRes);
                        }
                        else
                        {
                            Log.LogError("SToIInstruction.GetValues", string.Format("Argument(value {0}) must be a Text neuron that can be converted to an int: {1}", iPar, iPar.Text));
                            return new List<Neuron>();                                     //lost the order of arguments, so invalid state.
                        }
                    }
                    else
                        Log.LogError("SToIInstruction.GetValues", "Arguments must be Text neurons!");
                }
                return iList;
            }
            else
                Log.LogError("SToIInstruction.GetValues", "Invalid nr of arguments specified!");
            return new List<Neuron>();                                                 //when we get here, there was something wrong.  Still need to return something, so return empty.
        }
    }
}