﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Converts a double neuron into a string neuron.
    /// It is possible to convert multiple items at once.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// - a list of double neurons
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.DToSInstruction)]
    public class DToSInstruction : MultiResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.DToSInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.DToSInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return -1; }
        }

        /// <summary>
        /// performs the task and returns it's result.
        /// </summary>
        /// <param name="processor"></param>
        /// <param name="list">The list of arguments</param>
        /// <returns>The list of results</returns>
        /// <remarks>
        /// Note: when When a result instruction is executed, ( so <see cref="ResultInstruction.Execute"/> is
        /// called instead of <see cref="ResultInstruction.GetValues"/>, the result value(s) are pushed on the
        /// execution stack.
        /// </remarks>
        public override IEnumerable<Neuron> GetValues(Processor processor, IList<Neuron> list)
        {
            if (list != null && list.Count >= 1)
            {
                List<Neuron> iList = new List<Neuron>();
                foreach (Neuron iNeuron in list)
                {
                    DoubleNeuron iPar = iNeuron as DoubleNeuron;
                    if (iPar != null)
                    {
                        TextNeuron iRes = new TextNeuron();
                        iRes.Text = iPar.Value.ToString();
                        Brain.Current.Add(iRes);
                        iList.Add(iRes);
                    }
                    else
                        Log.LogError("DToSInstruction.GetValues", "Argument must be double neurons!");
                }
                return iList;
            }
            else
                Log.LogError("DToSInstruction.GetValues", "Invalid nr of arguments specified!");
            return new List<Neuron>();                                                 //when we get here, there was something wrong.  Still need to return something, so return empty.
        }
    }
}