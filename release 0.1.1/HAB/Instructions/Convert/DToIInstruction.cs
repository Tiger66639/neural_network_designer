﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Converts a double neuron into an int neuron.
    /// It is possible to convert multiple items at once.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// - a list of double neurons.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.DToIInstruction)]
    public class DToIInstruction : MultiResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.DToIInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.DToIInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return -1; }
        }

        public override IEnumerable<Neuron> GetValues(Processor processor, IList<Neuron> list)
        {
            if (list != null && list.Count >= 1)
            {
                List<Neuron> iList = new List<Neuron>();
                foreach (Neuron iNeuron in list)
                {
                    DoubleNeuron iPar = iNeuron as DoubleNeuron;
                    if (iPar != null)
                    {
                        IntNeuron iRes = new IntNeuron();
                        iRes.Value = (int)iPar.Value;
                        Brain.Current.Add(iRes);
                        iList.Add(iRes);
                    }
                    else
                        Log.LogError("DToIInstruction.GetValues", "Argument must be double neurons!");
                }
                return iList;
            }
            else
                Log.LogError("DToIInstruction.GetValues", "Invalid nr of arguments specified!");
            return new List<Neuron>();                                                 //when we get here, there was something wrong.  Still need to return something, so return empty.
        }
    }
}