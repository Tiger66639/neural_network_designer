﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Sets the first link out to a new value.
    /// </summary>
    /// <remarks>
    /// arg:
    /// 1: From part
    /// 2: To part          (this becomes the new value)
    /// 3: Meaning part
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.SetFirstOut)]
    public class SetFirstOutInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.SetFirstOut"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.SetFirstOut];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 3; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(Processor processor, IList<Neuron> args)
        {
            if (args.Count >= 3)
            {
                Neuron iFrom = args[0];
                if (iFrom != null)
                {
                    Neuron iTo = args[1];
                    if (iTo != null)
                    {
                        Neuron iMeaning = args[2];
                        if (iMeaning != null)
                            iFrom.SetFirstOutgoingLinkTo(iMeaning.ID, iTo);
                        else
                            Log.LogError("SetFirstOutInstruction.Execute", "Invalid third argument, Neuron expected, found null.");
                    }
                    else
                        Log.LogError("SetFirstOutInstruction.Execute", "Invalid second argument, Neuron expected, found null.");
                }
                else
                    Log.LogError("SetFirstOutInstruction.Execute", "Invalid first argument, Neuron expected, found null.");
            }
            else
                Log.LogError("SetFirstOutInstruction.Execute", "Invalid nr of arguments specified");
        }
    }
}