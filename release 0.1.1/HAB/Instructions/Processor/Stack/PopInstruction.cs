﻿using System.Collections.Generic;
using System.Diagnostics;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// This instruction removes the current <see cref="Neuron"/> from the <see cref="Processor"/>.
    /// </summary>
    [NeuronID((ulong)PredefinedNeurons.PopInstruction)]
    //[XmlRoot("PopInstruction", Namespace = "https://NeuralNetworkDesigne", IsNullable = false)]
    public class PopInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.PopInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.PopInstruction];
            }
        }

        #endregion TypeOfNeuron

        public override int ArgCount
        {
            get { return 0; }
        }

        public override void Execute(Processor processor, IList<Neuron> args)
        {
            Debug.Assert(processor != null);
            processor.Pop();
        }
    }
}