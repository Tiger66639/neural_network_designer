﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Puts the current <see cref="Processor"/>(thread) to sleep and adds the second argument to the first argument
    /// which has to be a cluster.  This is an atomic operation.
    /// </summary>
    /// <remarks>
    /// <para>
    /// After this operation, the second neuron an be used to activate the processor again with the Awake instruction.
    /// </para>
    /// Arguments:
    /// - The cluster to add the item to.
    /// - The neuron that should be used to signal it's reactivation again through the <see cref="AwakeInstruction"/>.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.SuspendInstruction)]
    public class SuspendInstruction : Instruction
    {
        #region Fields

        private static Dictionary<Neuron, Processor> fSuspendedProcessors = new Dictionary<Neuron, Processor>();

        #endregion Fields

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.SuspendInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.SuspendInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 2; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(Processor processor, IList<Neuron> args)
        {
            if (args != null && args.Count >= 2)
            {
                Neuron iIndicator = args[1];
                if (iIndicator == null)
                {
                    Log.LogError("SuspendInstruction.Execute", "item to add is null (second arg).");
                    return;
                }
                if (fSuspendedProcessors.ContainsKey(iIndicator) == false)
                {
                    NeuronCluster iCluster = args[0] as NeuronCluster;
                    if (iCluster != null)
                    {
                        using (ChildrenAccessor iList = iCluster.ChildrenW)
                        {
                            iList.Add(args[1]);
                            Processor.CurrentProcessor.ThreadNeuron = iIndicator;
                            fSuspendedProcessors[iIndicator] = Processor.CurrentProcessor;
                        }
                        Processor.CurrentProcessor.ThreadBlocker.WaitOne();
                    }
                    else
                        Log.LogError("SuspendInstruction.Execute", string.Format("Can't add, Invalid cluster specified: {0}.", args[0]));
                }
                else
                    Log.LogError("SuspendInstruction.Execute", "Neuron is already used as the wait handler for another processor.");
            }
            else
                Log.LogError("SuspendInstruction.Execute", "Invalid nr of arguments specified");
        }

        /// <summary>
        /// Resumes the specified suspended processor.
        /// </summary>
        /// <param name="suspended">The neuron that identifies the suspended processor.</param>
        /// <returns></returns>
        public static bool Resume(Neuron suspended)
        {
            Processor iFound;
            if (fSuspendedProcessors.TryGetValue(suspended, out iFound) == true)
            {
                iFound.ThreadBlocker.Set();                                                         //simply let the AutoResetEvent continue processing.
                return true;
            }
            else
                return false;
        }
    }
}