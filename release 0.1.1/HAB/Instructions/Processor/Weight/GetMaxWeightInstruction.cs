﻿using System.Collections.Generic;
using System.Linq;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Returns the neuron(s) with the maximum weight value in the cluster list.  When there are multiple
    /// neurons with the same maximum result, all of them are returned.  When the list is empty, an empty list is returned.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// 1. The neuronCluster that contains the list of neurons from which we need to return the ones with the biggest weight.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.GetMaxWeightInstruction)]
    public class GetMaxWeightInstruction : MultiResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.GetMaxWeightInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.GetMaxWeightInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// performs the task and returns it's result.
        /// </summary>
        /// <param name="processor"></param>
        /// <param name="list">The list of arguments</param>
        /// <returns>The list of results</returns>
        /// <remarks>
        /// Note: when When a result instruction is executed, ( so <see cref="ResultInstruction.Execute"/> is
        /// called instead of <see cref="ResultInstruction.GetValues"/>, the result value(s) are pushed on the
        /// execution stack.
        /// </remarks>
        public override IEnumerable<Neuron> GetValues(Processor processor, IList<Neuron> list)
        {
            if (list != null && list.Count >= 1)
            {
                NeuronCluster iCluster = list[0] as NeuronCluster;
                if (iCluster != null)
                {
                    List<Neuron> iRes = new List<Neuron>();
                    using (ChildrenAccessor iList = iCluster.Children)
                    {
                        var iSorted = from i in iList.ConvertTo<Neuron>() orderby i.Weight descending select i;
                        int iWeight = 0;
                        foreach (Neuron i in iSorted)                                                                //we still need to get only the neurons that have the maximum value.
                        {
                            if (i.Weight < iWeight)                                                                   //the first time, iWeight = 0, so always smaller or equal to the weight of the first element, so this will always fail, and the value can be init. The next time, iWeight has the correct value.
                                break;
                            else
                            {
                                iWeight = i.Weight;
                                iRes.Add(i);
                            }
                        }
                    }
                    return iRes;
                }
                else
                    Log.LogError("GetMaxWeightInstruction.GetValues", "First argument should be a NeuronCluster!");
            }
            else
                Log.LogError("GetMaxWeightInstruction.GetValues", "No arguments specified");
            return new List<Neuron>();                                                                            //when we get here, simply return an empty result.
        }

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 1; }
        }
    }
}