﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Increases the specified neurons weight by the specified amount.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// 1. The neuron for which to increase the weight.
    /// 2. An intNeuron that defines how much the weight should be changed.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.IncreaseWeightInstruction)]
    public class IncreaseWeightInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.IncreaseWeightInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.IncreaseWeightInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 2; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(Processor processor, IList<Neuron> args)
        {
            if (args != null && args.Count >= 2)
            {
                Neuron iNeuron = args[0];
                if (iNeuron != null)
                {
                    IntNeuron iVal = args[1] as IntNeuron;
                    if (iVal != null)
                    {
                        if (iNeuron.Weight < (iNeuron.Weight + iVal.Value))
                            iNeuron.Weight += iVal.Value;
                        else
                        {
                            iNeuron.Weight = int.MaxValue;
                            Log.LogWarning("IncreaseWeightInstruction.Execute", string.Format("Int overflow encountered while increasing weight of neuron {0}", iNeuron));
                        }
                    }
                    else
                        Log.LogError("IncreaseWeightInstruction.Execute", "IntNeuron expected as second arg.");
                }
                else
                    Log.LogError("IncreaseWeightInstruction.Execute", "Neuron for which to change the weight is not defined (arg 1).");
            }
            else
                Log.LogError("IncreaseWeightInstruction.Execute", "No arguments specified");
        }
    }
}