﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Returns the weight value that was assigned to the specified neuron.
    /// </summary>
    /// <remarks>
    /// Arguments: 1
    /// 1: the neuron for which to return the weight.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.GetWeightInstruction)]
    public class GetWeightInstruction : SingleResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.GetWeightInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.GetWeightInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Gets the actual value.
        /// </summary>
        /// <param name="processor">The processor to use.</param>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The result of the instruction.</returns>
        protected override Neuron InternalGetValue(Processor processor, IList<Neuron> list)
        {
            IntNeuron iRes = null;
            if (list.Count >= 1)
            {
                Neuron iFrom = list[0];
                if (iFrom != null)
                {
                    iRes = new IntNeuron();
                    iRes.Value = iFrom.Weight;
                    Brain.Current.Add(iRes);
                    return iRes;
                }
                else
                    Log.LogError("GetWeightInstruction.InternalGetValue", "Invalid first argument, Neuron expected, found null.");
            }
            else
                Log.LogError("GetWeightInstruction.InternalGetValue", "Invalid nr of arguments specified");
            return Brain.Current[(ulong)PredefinedNeurons.Empty];                                           //if nothing found, return the empty neuron.
        }

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 1; }
        }
    }
}