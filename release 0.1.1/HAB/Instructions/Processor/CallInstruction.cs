﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Sends a neuroncluster's content to the processor for execution.  The cluster should contain
    /// expressions.
    /// </summary>
    [NeuronID((ulong)PredefinedNeurons.CallInstruction)]
    public class CallInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.CallInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.CallInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Any length is possible but at least 2 are required.
        /// </summary>
        public override int ArgCount
        {
            get { return 1; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(Processor processor, IList<Neuron> args)
        {
            if (args != null && args.Count >= 1)
            {
                NeuronCluster iCluster = args[0] as NeuronCluster;
                if (iCluster != null)
                {
                    CallFrame iFrame = new CallFrame(iCluster);
                    processor.PushFrame(iFrame);
                }
                else
                    Log.LogError("CallInstruction.Execute", "First argument should be a neuron cluster.");
            }
            else
                Log.LogError("CallInstruction.Execute", "No arguments specified");
        }
    }
}