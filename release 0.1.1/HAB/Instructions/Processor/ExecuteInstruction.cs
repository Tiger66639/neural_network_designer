﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// An instruction that takes an expression as argument which is which is executed.
    /// </summary>
    /// <remarks>
    /// Arg: - an expression
    /// returns: Any possible result of this exeuction is returned.
    ///
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.ExecuteInstruction)]
    public class ExecuteInstruction : MultiResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ExecuteInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.ExecuteInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// performs the task and returns it's result.
        /// </summary>
        /// <param name="processor"></param>
        /// <param name="list">The list of arguments</param>
        /// <returns>The list of results</returns>
        /// <remarks>
        /// Note: when When a result instruction is executed, ( so <see cref="ResultInstruction.Execute"/> is
        /// called instead of <see cref="ResultInstruction.GetValues"/>, the result value(s) are pushed on the
        /// execution stack.
        /// </remarks>
        public override IEnumerable<Neuron> GetValues(Processor processor, IList<Neuron> list)
        {
            List<Neuron> iRes = new List<Neuron>();
            if (list != null && list.Count >= 1)
            {
                Expression iExp = list[0] as Expression;
                if (iExp != null)
                {
                    if (iExp is ResultExpression)
                    {
                        foreach (Neuron iResItem in ((ResultExpression)iExp).GetValue(processor))
                            iRes.Add(iResItem);
                    }
                    else if (iExp is Expression)
                    {
                        iExp.Execute(processor);
                        iRes.Add(Brain.Current[(ulong)PredefinedNeurons.Empty]);
                    }
                }
                else
                    Log.LogError("ExecuteInstruction.GetValues", "First argument should be an expression!");
            }
            else
                Log.LogError("ExecuteInstruction.GetValues", "No arguments specified");
            return iRes;
        }

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 1; }
        }
    }
}