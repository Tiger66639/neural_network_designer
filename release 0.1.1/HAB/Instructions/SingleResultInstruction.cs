﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// A resultInstruction that only has 1 result.
    /// </summary>
    public abstract class SingleResultInstruction : ResultInstruction
    {
        /// <summary>
        /// performs the task and returns it's result.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Note: when When a result instruction is executed, ( so <see cref="ResultInstruction.Execute"/> is
        /// called instead of <see cref="ResultInstruction.GetValues"/>, the result value(s) are pushed on the
        /// execution stack.
        /// </para>
        /// This method has a default implementation that uses <see cref="ResultInstruction.InternalGetValue"/>
        /// to get the actual result.
        /// </remarks>
        /// <param name="iArgs"></param>
        /// <returns></returns>
        public override IEnumerable<Neuron> GetValues(Processor processor, IList<Neuron> iArgs)
        {
            List<Neuron> iRes = new List<Neuron>();
            Neuron iNeuron = InternalGetValue(processor, iArgs);
            if (iNeuron != null)                                                             //only add something if there is a result
                iRes.Add(iNeuron);
            return iRes;
        }

        public override void Execute(Processor processor, IList<Neuron> args)
        {
            Neuron iRes = InternalGetValue(processor, args);
            processor.Push(iRes);
        }

        /// <summary>
        /// Gets the actual value.
        /// </summary>
        /// <param name="processor">The processor to use.</param>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The result of the instruction.</returns>
        protected abstract Neuron InternalGetValue(Processor processor, IList<Neuron> list);
    }
}