﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Creates new neurons.
    /// </summary>
    /// <remarks>
    /// Expects 1 parameter: the type of neuron that should be created.
    /// The neuron that is returned is only registered as a temp neuron, so it is not permanent, but if it is somehow
    /// stored permanetly, it's id is updated.  This allows for temporary values during code execution.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.NewInstruction)]
    public class NewInstruction : SingleResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.NewInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.NewInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 1; }
        }

        /// <summary>
        /// Creates a new neuron and returns this.
        /// </summary>
        /// <remarks>
        /// Expects 1 argument that defines which neuron to create.  This should be one
        /// of the predefined items:
        /// </remarks>
        /// <param name="list">Contains all the arguments that were passed along.</param>
        /// <returns></returns>
        protected override Neuron InternalGetValue(Processor processor, IList<Neuron> list)
        {
            Neuron iRes = null;
            if (list != null && list.Count >= 1 && list[0] != null)
            {
                Neuron iType = list[0];
                if (iType.ID == (ulong)PredefinedNeurons.Neuron)
                    iRes = new Neuron();
                else if (iType.ID == (ulong)PredefinedNeurons.NeuronCluster)
                    iRes = new NeuronCluster();
                else if (iType.ID == (ulong)PredefinedNeurons.DoubleNeuron)
                    iRes = new DoubleNeuron();
                else if (iType.ID == (ulong)PredefinedNeurons.IntNeuron)
                    iRes = new IntNeuron();
                else if (iType.ID == (ulong)PredefinedNeurons.TextNeuron)
                    iRes = new TextNeuron();
                else if (iType.ID == (ulong)PredefinedNeurons.BoolExpression)
                    iRes = new BoolExpression();
                else if (iType.ID == (ulong)PredefinedNeurons.ConditionalPart)
                    iRes = new ConditionalExpression();
                else if (iType.ID == (ulong)PredefinedNeurons.ConditionalStatement)
                    iRes = new ConditionalStatement();
                else if (iType.ID == (ulong)PredefinedNeurons.ResultStatement)
                    iRes = new ResultStatement();
                else if (iType.ID == (ulong)PredefinedNeurons.SearchExpression)
                    iRes = new SearchExpression();
                else if (iType.ID == (ulong)PredefinedNeurons.Statement)
                    iRes = new Statement();
                else if (iType.ID == (ulong)PredefinedNeurons.Variable)
                    iRes = new Variable();
                else if (iType.ID == (ulong)PredefinedNeurons.Global)
                    iRes = new Global();
                else if (iType.ID == (ulong)PredefinedNeurons.Assignment)
                    iRes = new Assignment();
                else if (iType.ID == (ulong)PredefinedNeurons.ExpressionsBlock)
                    iRes = new ExpressionsBlock();
                else if (iType.ID == (ulong)PredefinedNeurons.ByRefExpression)
                    iRes = new ByRefExpression();
                else
                    Log.LogError("NewInstruction.InternalGetValue", string.Format("Unknown argument found: {0}.  Expected one of the predefined neurons that identifies a type.", list[0]));
            }
            else
                Log.LogError("NewInstruction.InternalGetValue", "No arguments specified");
            if (iRes == null)
                iRes = new Neuron();                                                                //we always return a value
            Brain.Current.MakeTemp(iRes);
            return iRes;
        }
    }
}