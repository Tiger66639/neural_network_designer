﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Creates a cluster of the specified items.  The first argument identifies the meaning of the cluster.
    /// </summary>
    [NeuronID((ulong)PredefinedNeurons.MakeClusterInstruction)]
    public class MakeClusterInstruction : SingleResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.MakeClusterInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.MakeClusterInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Gets the actual value.
        /// </summary>
        /// <param name="processor">The processor to use.</param>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The result of the instruction.</returns>
        protected override Neuron InternalGetValue(Processor processor, IList<Neuron> list)
        {
            NeuronCluster iRes = new NeuronCluster();
            Brain.Current.Add(iRes);
            if (list.Count > 0)
            {
                iRes.Meaning = list[0].ID;
                using (ChildrenAccessor iList = iRes.ChildrenW)
                {
                    for (int i = 1; i < list.Count; i++)
                        iList.Add(list[i]);
                }
            }
            return iRes;
        }

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return -1; }
        }
    }
}