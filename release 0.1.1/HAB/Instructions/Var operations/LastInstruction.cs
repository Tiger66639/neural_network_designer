﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Gets the last item in the argument list.
    /// </summary>
    [NeuronID((ulong)PredefinedNeurons.GetLastInstruction)]
    public class GetLastInstruction : SingleResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.GetLastInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.GetLastInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Gets the actual value.
        /// </summary>
        /// <param name="processor">The processor to use.</param>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The result of the instruction.</returns>
        protected override Neuron InternalGetValue(Processor processor, IList<Neuron> list)
        {
            if (list.Count >= 1)
                return list[list.Count - 1];
            else
                return null;
        }

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return -1; }
        }
    }
}