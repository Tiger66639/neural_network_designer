﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Removes all the info neurons that are attached to a link.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// 1. From part
    /// 2. To part
    /// 3. Meaning part.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.ClearInfoInstruction)]
    public class ClearInfoInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ClearInfoInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.ClearInfoInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 3; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(Processor processor, IList<Neuron> args)
        {
            if (args.Count >= 3)
            {
                if (args[0] == null)
                {
                    Log.LogError("ClearInfoInstruction.Execute", "From part is null (first arg).");
                    return;
                }
                if (args[1] == null)
                {
                    Log.LogError("ClearInfoInstruction.Execute", "To part is null (second arg).");
                    return;
                }
                if (args[2] == null)
                {
                    Log.LogError("ClearInfoInstruction.Execute", "meaning is null (third arg).");
                    return;
                }
                Link iLink = Link.Find(args[1], args[0], args[2]);
                if (iLink != null)
                    using (LinkInfoAccessor iList = iLink.Info)
                        iList.Clear();
                else
                    Log.LogError("ClearInfoInstruction.Execute", string.Format("Could not find link from={0}, to={1}, meaing={2}", args[0], args[1], args[2]));
            }
            else
                Log.LogError("ClearInfoInstruction.Execute", "Invalid nr of arguments specified");
        }
    }
}