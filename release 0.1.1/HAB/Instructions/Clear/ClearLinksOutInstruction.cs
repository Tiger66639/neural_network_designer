﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Removes all the outgoing links from a neuron.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// 1. The neuron for which to remove all the links.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.ClearLinksOutInstruction)]
    public class ClearLinksOutInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ClearLinksOutInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.ClearLinksOutInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 1; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(Processor processor, IList<Neuron> args)
        {
            if (args != null && args.Count >= 1)
            {
                Neuron iNeuron = args[0];
                if (iNeuron != null)
                {
                    using (LinksAccessor iLinksOut = iNeuron.LinksOut)
                    {
                        List<Link> iTemp = new List<Link>(iLinksOut.Items);                                 //we make a copy cause we are going to change the list, which would cause exceptions otherwise.
                        foreach (Link i in iTemp)
                            i.Destroy();
                    }
                }
                else
                    Log.LogError("ClearLinksOutInstruction.Execute", string.Format("Can't clear Links out, Invalid Neuron specified: {0}.", args[0]));
            }
            else
                Log.LogError("ClearLinksOutInstruction.Execute", "Invalid nr of arguments specified");
        }
    }
}