﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Removes all the children from a cluster.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// 1. The cluster for which to remove all the children.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.ClearChildrenInstruction)]
    public class ClearChildrenInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ClearChildrenInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.ClearChildrenInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 1; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(Processor processor, IList<Neuron> args)
        {
            if (args != null && args.Count >= 1)
            {
                NeuronCluster iCluster = args[0] as NeuronCluster;
                if (iCluster != null)
                    using (ChildrenAccessor iList = iCluster.ChildrenW)
                        iList.Clear();
                else
                    Log.LogError("ClearChildrenInstruction.Execute", string.Format("Can't clear children, Invalid cluster specified: {0}.", args[0]));
            }
            else
                Log.LogError("ClearChildrenInstruction.Execute", "Invalid nr of arguments specified");
        }
    }
}