﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// A result instruction that is able to return multiple results.
    /// </summary>
    public abstract class MultiResultInstruction : ResultInstruction
    {
        public override void Execute(Processor processor, IList<Neuron> args)
        {
            IEnumerable<Neuron> iRes = GetValues(processor, args);
            foreach (Neuron i in iRes)
                processor.Push(i);
        }
    }
}