﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Adds an item to a Neuron cluster.
    /// </summary>
    /// <remarks>
    /// Args: - Cluster
    ///       - item to add
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.AddChildInstruction)]
    public class AddChildInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.AddChildInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.AddChildInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 2; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(Processor processor, IList<Neuron> args)
        {
            if (args != null && args.Count >= 2)
            {
                if (args[1] == null)
                {
                    Log.LogError("AddChildInstruction.Execute", "item to add is null (second arg).");
                    return;
                }
                NeuronCluster iCluster = args[0] as NeuronCluster;
                if (iCluster != null)
                    using (ChildrenAccessor iList = iCluster.ChildrenW)
                        iList.Add(args[1]);
                else
                {
                    Log.LogError("AddChildInstruction.Execute", string.Format("Can't add, Invalid cluster specified: {0}.", args[0]));
                    return;
                }
            }
            else
                Log.LogError("AddChildInstruction.Execute", "Invalid nr of arguments specified");
        }
    }
}