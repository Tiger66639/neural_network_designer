﻿namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Same as <see cref="CopyInfoInstruction"/>, but removes the items from the source.
    /// </summary>
    [NeuronID((ulong)PredefinedNeurons.MoveInfoInstruction)]
    public class MoveInfoInstruction : CopyInfoInstruction
    {
        /// <summary>
        /// Performs the actual copy operation.
        /// </summary>
        /// <param name="from">copy from.</param>
        /// <param name="start">The start index to begin copying from</param>
        /// <param name="end">Copy untill this index</param>
        /// <param name="to">Cop to this link.</param>
        /// <param name="insertAt">Start inserting at this pos (-1 means add).</param>
        protected override void CopyInfo(LinkInfoAccessor from, int start, int end, LinkInfoAccessor to, int insertAt)
        {
            from.Mode = AccessorMode.Write;                                //need to make certain from is inwrite mode, which is not the case
            if (insertAt == -1)
            {
                while (start < end)
                {
                    to.Add(from[start]);
                    from.RemoveAt(start);
                    end++;
                }
            }
            else
            {
                while (start < end)
                {
                    to.Insert(insertAt++, from[start]);
                    from.RemoveAt(start);
                    end++;
                }
            }
        }
    }
}