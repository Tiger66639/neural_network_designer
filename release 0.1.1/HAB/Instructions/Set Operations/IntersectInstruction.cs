﻿using System.Collections.Generic;
using System.Linq;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Performs an intersection of the list of clusters.
    /// </summary>
    /// <remarks>
    /// arg: -1 (at least 2 clusters).
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.IntersectInstruction)]
    public class IntersectInstruction : MultiResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.IntersectInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.IntersectInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// performs the task and returns it's result.
        /// </summary>
        /// <param name="processor"></param>
        /// <param name="list">The list of arguments</param>
        /// <returns>The list of results</returns>
        /// <remarks>
        /// Note: when When a result instruction is executed, ( so <see cref="ResultInstruction.Execute"/> is
        /// called instead of <see cref="ResultInstruction.GetValues"/>, the result value(s) are pushed on the
        /// execution stack.
        /// </remarks>
        public override IEnumerable<Neuron> GetValues(Processor processor, IList<Neuron> list)
        {
            if (list != null && list.Count >= 2)
            {
                NeuronCluster a = list[0] as NeuronCluster;
                if (a != null)
                {
                    List<Neuron> iLeft;
                    using (ChildrenAccessor iList = a.Children)
                        iLeft = iList.ConvertTo<Neuron>();
                    for (int i = 1; i < list.Count; i++)
                    {
                        NeuronCluster b = list[i] as NeuronCluster;
                        if (b != null)
                        {
                            List<Neuron> iRight;
                            using (ChildrenAccessor iList = b.Children)
                                iRight = iList.ConvertTo<Neuron>();
                            iLeft = (iLeft.Intersect(iRight)).ToList();
                            if (iLeft.Count == 0)                                                                     //if there are no more items left to check, we might as well stop now.
                                break;
                        }
                        else
                        {
                            Log.LogError("IntersectInstruction.GetValues", "All arguments should be clusters!");
                            return new List<Neuron>();                                                                //illegal condition so simply stop.
                        }
                    }
                    return iLeft;
                }
                else
                    Log.LogError("IntersectInstruction.GetValues", "All arguments should be clusters!");
            }
            else
                Log.LogError("IntersectInstruction.GetValues", "No arguments specified");
            return new List<Neuron>();                                                                            //when we get here, simply return an empty result.
        }

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value>-1</value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return -1; }
        }
    }
}