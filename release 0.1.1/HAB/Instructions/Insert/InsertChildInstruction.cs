﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Inserts an item into a list
    /// </summary>
    /// <remarks>
    /// Args: - Cluster to insert to
    ///       - item to insert
    ///       - position at which to insert.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.InsertChildInstruction)]
    public class InsertChildInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.InsertChildInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.InsertChildInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 3; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(Processor processor, IList<Neuron> args)
        {
            if (args != null && args.Count >= 3)
            {
                int iIndex;
                if (CheckArgsForChildren(args, out iIndex) == true)
                {
                    using (ChildrenAccessor iList = ((NeuronCluster)args[0]).Children)
                    {
                        if (iIndex != -1)                                                                          //if there was no valid index nr, it is added to the back.
                            iList.Insert(iIndex, args[1]);
                        else
                            iList.Add(args[1]);
                    }
                }
            }
            else
                Log.LogError("InsertInstruction.Execute", "Invalid nr of arguments specified");
        }

        /// <summary>
        /// Checks the args for a child insert operation.
        /// </summary>
        /// <param name="args">The args.</param>
        /// <param name="index">returns the index at which the insert should be done.</param>
        /// <returns></returns>
        private bool CheckArgsForChildren(IList<Neuron> args, out int index)
        {
            index = -1;
            if (args.Count >= 3)
            {
                bool iRes = true;
                if (args[1] == null)
                {
                    Log.LogError("AddInstruction.CheckArgsForLink", "item to insert is null (first arg).");
                    iRes = false;
                }
                if (!(args[2] is IntNeuron))
                {
                    Log.LogError("AddInstruction.CheckArgsForLink", "insert position is invalid (second arg).");
                    iRes = false;
                }
                if (!(args[0] is NeuronCluster))
                {
                    Log.LogError("AddInstruction.CheckArgsForLink", "owner of the list is invalid (null or not a cluster) (fourth arg).");
                    iRes = false;
                }
                if (iRes == true)
                    index = ((IntNeuron)args[2]).Value;
                return iRes;
            }
            else
                Log.LogError("AddInstruction.CheckArgsForChildren", "Invalid nr of arguments specified");
            return false;
        }
    }
}