﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Inserts an item into an info list of a link.
    /// </summary>
    /// <remarks>
    /// Args:
    /// 1: from part of link
    /// 2: to part of link
    /// 3: meaning part of link
    /// 4: item to insert
    /// 5: position at which item should be inserted.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.InsertInfoInstruction)]
    public class InsertInfoInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.InsertInfoInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.InsertInfoInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 5; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(Processor processor, IList<Neuron> args)
        {
            if (args != null && args.Count >= 3)
            {
                int iIndex;
                Link iLink;
                if (CheckArgs(args, out iIndex, out iLink) == true)
                {
                    if (iLink != null)
                    {
                        using (LinkInfoAccessor iList = iLink.InfoW)
                        {
                            if (iIndex != -1)
                                iList.Insert(iIndex, args[3]);
                            else
                                iList.Add(args[3]);
                        }
                    }
                    else
                        Log.LogError("InsertInfoInstruction.Execute", "Link not found");
                }
            }
            else
                Log.LogError("InsertInfoInstruction.Execute", "Invalid nr of arguments specified");
        }

        private bool CheckArgs(IList<Neuron> args, out int index, out Link link)
        {
            index = -1;
            link = null;
            if (args.Count >= 5)
            {
                bool iRes = true;
                if (args[0] == null)
                {
                    Log.LogError("InsertInfoInstruction.CheckArgsForLink", "From part is null (first arg).");
                    iRes = false;
                }
                if (args[1] == null)
                {
                    Log.LogError("InsertInfoInstruction.CheckArgsForLink", "To part is null (second arg).");
                    iRes = false;
                }
                if (args[2] == null)
                {
                    Log.LogError("InsertInfoInstruction.CheckArgsForLink", "meaning part is null (third arg).");
                    iRes = false;
                }
                if (args[3] == null)
                {
                    Log.LogError("InsertInfoInstruction.CheckArgsForLink", "item to insert is null (fourth arg).");
                    iRes = false;
                }
                if (!(args[4] is IntNeuron))
                {
                    Log.LogError("InsertInfoInstruction.CheckArgsForLink", "insert position is invalid (fifth arg).");
                    iRes = false;
                }
                if (iRes == true)
                {
                    index = ((IntNeuron)args[1]).Value;
                    link = Link.Find(args[1], args[0], args[2]);
                }
                return iRes;
            }
            else
                Log.LogError("InsertInfoInstruction.CheckArgsForChildren", "Invalid nr of arguments specified");
            return false;
        }
    }
}