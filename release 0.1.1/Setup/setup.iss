#include "scripts\products.iss"

#include "scripts\products\winversion.iss"
#include "scripts\products\fileversion.iss"
//#include "scripts\products\iis.iss"
#include "scripts\products\kb835732.iss"

#include "scripts\products\msi20.iss"
#include "scripts\products\msi31.iss"
#include "scripts\products\ie6.iss"

//#include "scripts\products\dotnetfx11.iss"
//#include "scripts\products\dotnetfx11lp.iss"
//#include "scripts\products\dotnetfx11sp1.iss"

//#include "scripts\products\dotnetfx20.iss"
//#include "scripts\products\dotnetfx20lp.iss"
//#include "scripts\products\dotnetfx20sp1.iss"
//#include "scripts\products\dotnetfx20sp1lp.iss"

#include "scripts\products\dotnetfx35.iss"
#include "scripts\products\dotnetfx35lp.iss"
#include "scripts\products\dotnetfx35sp1.iss"
#include "scripts\products\dotnetfx35sp1lp.iss"

#include "scripts\products\mdac28.iss"
#include "scripts\products\jet4sp8.iss"

[CustomMessages]
win2000sp3_title=Windows 2000 Service Pack 3
winxpsp2_title=Windows XP Service Pack 2


[Setup]
AppName=N�D
Uninstallable=true
DirExistsWarning=no
CreateAppDir=true
OutputDir=bin
OutputBaseFilename=NND Installer-0.1
SourceDir=.
AppCopyright=Copyright � Jan Bogaerts 2009
AppVerName=N�D 0.1
DefaultGroupName=N�D
AllowNoIcons=yes
AppPublisher=Jan Bogaerts
AppVersion=0.1
UninstallDisplayIcon={app}\HAB.Designer.exe
UninstallDisplayName=N�D uninstaller
UsePreviousGroup=yes
UsePreviousAppDir=yes
DefaultDirName={pf}\NND
VersionInfoVersion=0.1
VersionInfoCompany=Jan Bogaerts
VersionInfoCopyright=Copyright � Jan Bogaerts 2009
ShowUndisplayableLanguages=no
LanguageDetectionMethod=uilanguage
InternalCompressLevel=max
SolidCompression=yes
Compression=lzma/max
;required by products
MinVersion=4.1,5.0
PrivilegesRequired=admin
ArchitecturesAllowed=x86
ShowLanguageDialog=auto
WizardImageFile=compiler:WizModernImage-IS.bmp
WizardSmallImageFile=compiler:WizModernSmallImage-IS.bmp

[Languages]
Name: en; MessagesFile: compiler:Default.isl
;Name: de; MessagesFile: compiler:Languages\German.isl

[Tasks]
Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons}; Flags: unchecked
Name: quicklaunchicon; Description: {cm:CreateQuickLaunchIcon}; GroupDescription: {cm:AdditionalIcons}; Flags: unchecked

[Files]
Source: ..\Designer\bin\Release\HAB.Designer.exe; DestDir: {app}
Source: ..\Designer\bin\Release\*.dll; DestDir: {app}
Source: ..\Designer\bin\Release\*.mdf; DestDir: {app}
Source: ..\Designer\bin\Release\*.ldf; DestDir: {app}
Source: ..\Designer\bin\Release\*.config; DestDir: {app}
Source: ..\help\NND.chm; DestDir: {app}
Source: ..\Info\A new Neural Network.pdf; DestDir: {app}

Source: ..\Networks\Echo words\*.*; DestDir: {userdocs}\NND\Demos\Echo_words
Source: ..\Networks\Echo words\Data\*.*; DestDir: {userdocs}\NND\Demos\Echo_words\Data

Source: ..\Networks\Echo letters\*.*; DestDir: {userdocs}\NND\Demos\Echo_letters
Source: ..\Networks\Echo letters\Data\*.*; DestDir: {userdocs}\NND\Demos\Echo_letters\Data

;Source: ..\Networks\WordNetDemo\*.*; DestDir: {userdocs}\NND\Demos\WordNetDemo
;Source: ..\Networks\WordNetDemo\Data\*.*; DestDir: {userdocs}\NND\Demos\WordNetDemo\Data

;Source: ..\Networks\Split_Weight Demo\*.*; DestDir: {userdocs}\NND\Demos\Split_Weight Demo
;Source: ..\Networks\Split_Weight Demo\Data\*.*; DestDir: {userdocs}\NND\Demos\Split_Weight Demo\Data

;Source: ..\Networks\FileManager\*.*; DestDir: {userdocs}\NND\Demos\FileManager
;Source: ..\Networks\FileManager\Data\*.*; DestDir: {userdocs}\NND\Demos\FileManager\Data

Source: ..\Networks\English language Def\*.*; DestDir: {userdocs}\NND\Demos\English_language_Def
Source: ..\Networks\English language Def\Data\*.*; DestDir: {userdocs}\NND\Demos\English_language_Def\Data

Source: ..\Networks\Default\*.*; DestDir: {userdocs}\NND\Templates\Default
Source: ..\Networks\Default\Data\*.*; DestDir: {userdocs}\NND\Templates\Default\Data

Source: ..\FrameNetSin\Data\*.*; DestDir: {userdocs}\NND\FrameNet
;we need to include the docs of framenet as well, cause otherwise it wont open properly.
Source: ..\FrameNetSin\Data\docs\*.*; DestDir: {userdocs}\NND\FrameNet\docs

[Icons]
Name: {group}\N�D; Filename: {app}\HAB.Designer.exe
Name: {group}\Help; Filename: {app}\NND.chm
Name: {group}\A new neural network (Paper); Filename: {app}\A new Neural Network.pdf

Name: {group}\Demos\Echo words; Filename: {app}\HAB.Designer.exe; Parameters: {userdocs}\NND\Demos\echo_words; IconIndex: 0
Name: {group}\Demos\Echo letters; Filename: {app}\HAB.Designer.exe; Parameters: {userdocs}\NND\Demos\echo_letters; IconIndex: 0
;Name: {group}\Demos\WordNet demo; Filename: {app}\HAB.Designer.exe; Parameters: {userdocs}\NND\Demos\WordNetDemo; IconIndex: 0
;Name: {group}\Demos\Split-weight; Filename: {app}\HAB.Designer.exe; Parameters: {userdocs}\NND\Demos\Split_Weight Demo; IconIndex: 0
;Name: {group}\Demos\FileManager; Filename: {app}\HAB.Designer.exe; Parameters: {userdocs}\NND\Demos\FileManager; IconIndex: 0
Name: {group}\Demos\English language Def; Filename: {app}\HAB.Designer.exe; Parameters: {userdocs}\NND\Demos\English_language_Def; IconIndex: 0

Name: {group}\{cm:UninstallProgram,N�D}; Filename: {uninstallexe}
Name: {commondesktop}\N�D; Filename: {app}\HAB.Designer.exe; Tasks: desktopicon
Name: {userappdata}\Microsoft\Internet Explorer\Quick Launch\N�D; Filename: {app}\HAB.Designer.exe; Tasks: quicklaunchicon

[Run]
Filename: {app}\HAB.Designer.exe; Description: {cm:LaunchProgram,N�D}; Flags: nowait postinstall skipifsilent

[Code]
function InitializeSetup(): Boolean;
begin
	initwinversion();

	if (not minspversion(5, 0, 3)) then begin
		MsgBox(FmtMessage(CustomMessage('depinstall_missing'), [CustomMessage('win2000sp3_title')]), mbError, MB_OK);
		exit;
	end;
	if (not minspversion(5, 1, 2)) then begin
		MsgBox(FmtMessage(CustomMessage('depinstall_missing'), [CustomMessage('winxpsp2_title')]), mbError, MB_OK);
		exit;
	end;

	//if (not iis()) then exit;

	msi20('2.0');
	msi31('3.0');
	ie6('5.0.2919');

	//dotnetfx11();
	//dotnetfx11lp();
	//dotnetfx11sp1();

	kb835732();

	//if (minwinversion(5, 0) and minspversion(5, 0, 4)) then begin
	//	dotnetfx20sp1();
	//	dotnetfx20sp1lp();
	//end else begin
	//	dotnetfx20();
	//	dotnetfx20lp();
	//end;

	dotnetfx35();
	dotnetfx35lp();
	dotnetfx35sp1();
	dotnetfx35sp1lp();

	mdac28('2.7');
	jet4sp8('4.0.8015');

	Result := true;
end;
