﻿using System.Collections.Generic;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Framenet
{
    /// <summary>
    /// Data class for Framenet.
    /// </summary>
    /// <remarks>
    /// For datamapping between framenet and wordnet, assign a dictionary to
    /// <see cref="FrameNet.WordNetMap"/>.
    /// </remarks>
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "frames", Namespace = "", IsNullable = false)]
    public class FrameNet : ObservableObject
    {
        #region Inner types

        /// <summary>
        /// contains the word and wordnet id of the word (in it's specific meaning).
        /// </summary>
        /// <remarks>
        /// This class is used to provide a list of possible synsetID's found in wordnet that
        /// can be linked to this lexical unit (the same text and POS value, but a different meaning).
        /// </remarks>
        public class Synset
        {
            /// <summary>
            /// Gets or sets the description of the meaning
            /// </summary>
            /// <value>The description.</value>
            public string Description { get; set; }

            /// <summary>
            /// Gets or sets the SynsetID, found in wordnet.
            /// </summary>
            /// <value>The ID.</value>
            public int ID { get; set; }
        }

        #endregion Inner types

        #region Fields

        private ObservedCollection<Frame> fFrames;
        private string fXMLCreated;
        private List<Frame> fSelectedFrame = new List<Frame>();
        private IDictionary<int, int> fWordNetMapLU;
        private IDictionary<int, int> fWordNetMapFE;
        private bool fWordNetMapLUChanged;
        private bool fWordNetMapFEChanged;

        #endregion Fields

        #region ctor

        public FrameNet()
        {
            fFrames = new ObservedCollection<Frame>(this);
        }

        #endregion ctor

        #region Prop

        #region Frames

        /// <summary>
        /// Gets the frames in this data set.
        /// </summary>
        [XmlElement("frame", Form = XmlSchemaForm.Unqualified)]
        public ObservedCollection<Frame> Frames
        {
            get { return fFrames; }
        }

        #endregion Frames

        #region XMLCreated

        /// <summary>
        /// Gets/sets the date-time at which the xml file was created.
        /// </summary>
        [XmlAttribute()]
        public string XMLCreated
        {
            get
            {
                return fXMLCreated;
            }
            set
            {
                OnPropertyChanging("XMLCreated", fXMLCreated, value);
                fXMLCreated = value;
                OnPropertyChanged("XMLCreated");
            }
        }

        #endregion XMLCreated

        #region WordNetMapLU

        /// <summary>
        /// Gets/sets the dictionary to use for mapping Lemma ids to SynSetIds found in the wordnet db. (Lexical units and lexemes to synsets).
        /// </summary>
        /// <value>A dictionary with key = lemma id and value = wordnet id (synset id).</value>
        /// <remarks>
        /// This data is not read in from the FrameNet database, should be provided seperatly.
        /// </remarks>
        [XmlIgnore]
        public IDictionary<int, int> WordNetMapLU
        {
            get
            {
                return fWordNetMapLU;
            }
            set
            {
                fWordNetMapLU = value;
            }
        }

        #endregion WordNetMapLU

        #region WordNetMapFE

        /// <summary>
        /// Gets/sets the dictionary to use for mapping frame elements to SynSetIds found in the wordnet db. (Lexical units and lexemes to synsets).
        /// </summary>
        [XmlIgnore]
        public IDictionary<int, int> WordNetMapFE
        {
            get
            {
                return fWordNetMapFE;
            }
            set
            {
                fWordNetMapFE = value;
            }
        }

        #endregion WordNetMapFE

        #region WordNetMapLUChanged

        /// <summary>
        /// Gets wether the wordnet map for lexical units/lexemes has changed since it was assigned to the object.
        /// </summary>
        /// <remarks>
        /// This can be used to find out if it needs to be saved because the user added data.
        /// </remarks>
        [XmlIgnore]
        public bool WordNetMapLUChanged
        {
            get
            {
                return fWordNetMapLUChanged;
            }
            internal set
            {
                fWordNetMapLUChanged = value;
            }
        }

        #endregion WordNetMapLUChanged

        #region WordNetMapFEChanged

        /// <summary>
        /// Gets wether the wordnet map for frame elements has changed since it was assigned to the object.
        /// </summary>
        /// <remarks>
        /// This can be used to find out if it needs to be saved because the user added data.
        /// </remarks>
        [XmlIgnore]
        public bool WordNetMapFEChanged
        {
            get { return fWordNetMapFEChanged; }
            internal set { fWordNetMapFEChanged = value; }
        }

        #endregion WordNetMapFEChanged

        #endregion Prop

        #region Functions

        /// <summary>
        /// Gets the word net ID for the specified lemma id.
        /// </summary>
        /// <param name="LemmaID">The lemma ID.</param>
        /// <returns></returns>
        internal int GetWordNetIDFor(int LemmaID)
        {
            if (WordNetMapLU != null)
            {
                int iFound;
                if (WordNetMapLU.TryGetValue(LemmaID, out iFound) == true)
                    return iFound;
            }
            return 0;
        }

        /// <summary>
        /// Gets the word net ID for the specified frame element id.
        /// </summary>
        /// <param name="LemmaID">The lemma ID.</param>
        /// <returns></returns>
        internal int GetWordNetIDForElement(int ID)
        {
            if (WordNetMapFE != null)
            {
                int iFound;
                if (WordNetMapFE.TryGetValue(ID, out iFound) == true)
                    return iFound;
            }
            return 0;
        }

        #endregion Functions
    }
}