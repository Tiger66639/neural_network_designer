﻿namespace NeuralNetworkDesigne.HAB.Framenet
{
    /// <summary>
    /// Base class for frame data elements: provides access to the <see cref="FrameNet"/> root object.
    /// </summary>
    public class FrameCore : OwnedObject
    {
        #region Root

        /// <summary>
        /// Gets the root object of this framenet data set.
        /// </summary>
        public FrameNet Root
        {
            get
            {
                IOwnedObject iRoot = this as IOwnedObject;
                while (iRoot != null && !(iRoot.Owner is FrameNet))
                    iRoot = iRoot.Owner as IOwnedObject;
                if (iRoot != null)
                    return iRoot.Owner as FrameNet;
                else
                    return null;
            }
        }

        #endregion Root
    }
}