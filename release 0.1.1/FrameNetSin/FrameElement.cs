﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Framenet
{
    /// <summary>
    /// Contains all the data for a single frame element in framenet.
    /// </summary>
    public class FrameElement : FrameBase
    {
        #region Fields

        private string fAbbreviation;
        private string fCoreType;
        private string fForeground;
        private string fBackground;
        private int fWordNetID;

        #endregion Fields

        #region Abbreviation

        /// <summary>
        /// Gets/sets the abbreviation used for the frame element.
        /// </summary>
        [XmlAttribute("abbrev")]
        public string Abbreviation
        {
            get
            {
                return fAbbreviation;
            }
            set
            {
                OnPropertyChanging("Abbreviation", fAbbreviation, value);
                fAbbreviation = value;
                OnPropertyChanged("Abbreviation");
            }
        }

        #endregion Abbreviation

        #region CoreType

        /// <summary>
        /// Gets/sets the abbreviation used for the frame element.
        /// </summary>
        [XmlAttribute("coreType")]
        public string CoreType
        {
            get
            {
                return fCoreType;
            }
            set
            {
                OnPropertyChanging("CoreType", fCoreType, value);
                fCoreType = value;
                OnPropertyChanged("CoreType");
            }
        }

        #endregion CoreType

        #region Foreground

        /// <summary>
        /// Gets/sets the abbreviation used for the frame element.
        /// </summary>
        [XmlAttribute("fgColor")]
        public string Foreground
        {
            get
            {
                return fForeground;
            }
            set
            {
                OnPropertyChanging("Foreground", fForeground, value);
                fForeground = value;
                OnPropertyChanged("Foreground");
            }
        }

        #endregion Foreground

        #region Background

        /// <summary>
        /// Gets/sets the abbreviation used for the frame element.
        /// </summary>
        [XmlAttribute("bgColor")]
        public string Background
        {
            get
            {
                return fBackground;
            }
            set
            {
                OnPropertyChanging("Background", fBackground, value);
                fBackground = value;
                OnPropertyChanged("Background");
            }
        }

        #endregion Background

        #region WordNetID

        /// <summary>
        /// Gets/sets the id of the 'synset' that corresponds to the lemma. (both refer to a single meaning of a word (but where the same meaning can be expressed by different words). ex: bunnet - trunk
        /// </summary>
        [XmlAttribute("synsetId")]
        public int WordNetID
        {
            get
            {
                if (fWordNetID == 0)
                {
                    FrameNet iRoot = Root;
                    fWordNetID = Root.GetWordNetIDForElement(ID);
                }
                return fWordNetID;
            }
            set
            {
                if (value != fWordNetID)
                {
                    OnPropertyChanging("WordNetID", fWordNetID, value);
                    fWordNetID = value;
                    OnPropertyChanged("WordNetID");
                    FrameNet iRoot = Root;
                    if (iRoot != null && iRoot.WordNetMapFE != null)
                    {
                        iRoot.WordNetMapFE[ID] = value;
                        iRoot.WordNetMapFEChanged = true;
                    }
                }
            }
        }

        #endregion WordNetID

        #region WordNetIDValues

        /// <summary>
        /// Gets the list of wordNet ID values that can be related to this lexical unit (because they have the same textual representation).
        /// </summary>
        /// <value>The word net ID values.</value>
        [XmlIgnore]
        public IList<FrameNet.Synset> WordNetIDValues
        {
            get
            {
                List<FrameNet.Synset> iRes = new List<FrameNet.Synset>();
                string[] iSplit = Name.Split('.');
                if (iSplit.Length < 2)
                {
                    foreach (wordnetDataSet.WordInfoRow i in WordNetSin.Default.GetWordInfoFor(Name))
                    {
                        FrameNet.Synset iNew = new FrameNet.Synset() { ID = i.synsetid, Description = i.definition };
                        iRes.Add(iNew);
                    }
                }
                else
                {
                    foreach (wordnetDataSet.WordInfoRow i in WordNetSin.Default.GetWordInfoFor(iSplit[0], iSplit[1]))
                    {
                        FrameNet.Synset iNew = new FrameNet.Synset() { ID = i.synsetid, Description = i.definition };
                        iRes.Add(iNew);
                    }
                }
                return iRes;
            }
        }

        #endregion WordNetIDValues
    }
}