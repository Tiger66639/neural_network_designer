﻿using System.Collections.Generic;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Framenet
{
    public class LexUnit : FrameBase
    {
        #region Fields

        private Annotation fAnnotation;
        private ObservedCollection<Lexeme> fLexemes;
        private string fPOS;
        private string fStatus;
        private int fLemmaID;
        private int fWordNetID;

        #endregion Fields

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="LexUnit"/> class.
        /// </summary>
        public LexUnit()
        {
            fLexemes = new ObservedCollection<Lexeme>(this);
        }

        #endregion ctor

        #region Annotation

        /// <summary>
        /// Gets/sets the annotation for the lexical unit.
        /// </summary>
        [XmlElement("annotation", Form = XmlSchemaForm.Unqualified)]
        public Annotation Annotation
        {
            get
            {
                return fAnnotation;
            }
            set
            {
                OnPropertyChanging("Annotation", fAnnotation, value);
                fAnnotation = value;
                OnPropertyChanged("Annotation");
            }
        }

        #endregion Annotation

        #region Lexemes

        /// <summary>
        /// Gets the list of lexemes for this lexical unit.
        /// </summary>
        [XmlArray("lexemes")]
        [XmlArrayItem("lexeme")]
        public ObservedCollection<Lexeme> Lexemes
        {
            get { return fLexemes; }
        }

        #endregion Lexemes

        #region POS

        /// <summary>
        /// Gets/sets the part of speech of this lexical unit.
        /// </summary>
        [XmlAttribute("pos")]
        public string POS
        {
            get
            {
                return fPOS;
            }
            set
            {
                OnPropertyChanging("POS", fPOS, value);
                fPOS = value;
                OnPropertyChanged("POS");
            }
        }

        #endregion POS

        #region Status

        /// <summary>
        /// Gets/sets the part of speech of this lexical unit.
        /// </summary>
        [XmlAttribute("status")]
        public string Status
        {
            get
            {
                return fStatus;
            }
            set
            {
                OnPropertyChanging("Status", fStatus, value);
                fStatus = value;
                OnPropertyChanged("Status");
            }
        }

        #endregion Status

        #region LemmaID

        /// <summary>
        /// Gets/sets the id of the lemma associated with this lexical unit.
        /// </summary>
        /// <remarks>
        /// best seen as the id of an 'object' cluster.
        /// </remarks>
        [XmlAttribute("lemmaId")]
        public int LemmaID
        {
            get
            {
                return fLemmaID;
            }
            set
            {
                OnPropertyChanging("LemmaID", fLemmaID, value);
                fLemmaID = value;
                OnPropertyChanged("LemmaID");
            }
        }

        #endregion LemmaID

        #region WordNetID

        /// <summary>
        /// Gets/sets the id of the 'synset' that corresponds to the lemma. (both refer to a single meaning of a word (but where the same meaning can be expressed by different words). ex: bunnet - trunk
        /// </summary>
        [XmlAttribute("synsetId")]
        public int WordNetID
        {
            get
            {
                if (fWordNetID == 0)
                {
                    FrameNet iRoot = Root;
                    fWordNetID = Root.GetWordNetIDFor(LemmaID);
                }
                return fWordNetID;
            }
            set
            {
                if (value != fWordNetID)
                {
                    OnPropertyChanging("WordNetID", fWordNetID, value);
                    fWordNetID = value;
                    OnPropertyChanged("WordNetID");
                    FrameNet iRoot = Root;
                    if (iRoot != null && iRoot.WordNetMapLU != null)
                    {
                        iRoot.WordNetMapLU[LemmaID] = value;
                        iRoot.WordNetMapLUChanged = true;
                    }
                }
            }
        }

        #endregion WordNetID

        #region WordNetIDValues

        /// <summary>
        /// Gets the list of wordNet ID values that can be related to this lexical unit (because they have the same textual representation).
        /// </summary>
        /// <value>The word net ID values.</value>
        [XmlIgnore]
        public IList<FrameNet.Synset> WordNetIDValues
        {
            get
            {
                List<FrameNet.Synset> iRes = new List<FrameNet.Synset>();
                string[] iSplit = Name.Split('.');
                if (iSplit.Length < 2)
                {
                    foreach (wordnetDataSet.WordInfoRow i in WordNetSin.Default.GetWordInfoFor(Name))
                    {
                        FrameNet.Synset iNew = new FrameNet.Synset() { ID = i.synsetid, Description = i.definition };
                        iRes.Add(iNew);
                    }
                }
                else
                {
                    foreach (wordnetDataSet.WordInfoRow i in WordNetSin.Default.GetWordInfoFor(iSplit[0], iSplit[1]))
                    {
                        FrameNet.Synset iNew = new FrameNet.Synset() { ID = i.synsetid, Description = i.definition };
                        iRes.Add(iNew);
                    }
                }
                return iRes;
            }
        }

        #endregion WordNetIDValues
    }
}