﻿namespace NeuralNetworkDesigne.HAB
{
    public partial class wordnetDataSet
    {
        partial class CompundWordsDataTable
        {
        }

        /// <summary>
        /// Contains all the synsets that are build from multiple words and which contain the specified word.
        /// </summary>
        /// <remarks>
        /// Note: when specifying the word to search for, don't forget to put a % in front and in the back.
        /// </remarks>
        partial class oldCompundWordsDataTable
        {
        }

        /// <summary>
        /// Contains all the synonyms of a specific symset.
        /// </summary>
        partial class SynonymsDataTable
        {
        }

        /// <summary>
        /// Performs a lookup for all the related words (from a specific relationship) of a specific word
        /// sense.
        /// </summary>
        partial class RelatedWordsDataTable
        {
        }

        /// <summary>
        /// Performs a lookup for 1 word to find it's different meanings + small description.
        /// </summary>
        partial class WordInfoDataTable
        {
        }
    }
}