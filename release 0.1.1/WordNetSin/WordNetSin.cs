﻿using NeuralNetworkDesigne.HAB.wordnetDataSetTableAdapters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// A <see cref="Sin"/> which connects to the WordNet database for retrieving textual information.
    /// </summary>
    /// <remarks>
    /// <para>
    /// This sin is used internally by the brain to retrieve more info about text that it doesn't yet understand.
    /// The brain performs this request by sending textneurons to this sin.  Each textneuron will be searched in
    /// the database, and when found, links will be added to it. This means that, if the word is already loaded, it
    /// will be loaded again (and only partly overwritten through the dictionary of the textsin).
    /// </para>
    /// <para>
    /// This sin uses a Sql server database file as it's backing store.
    /// </para>
    /// <para>
    /// Algorithm:
    /// -a word is first searched in the 'WordInfo' datatable (query).  This contains all the senses (meanings) of
    /// the word. For most senses, there is also an example sentence included.
    /// -for each sense of the word, we create an object cluster which contains the textneuron.
    /// -store the sentence part type of the object.
    /// -if the sense contained an example sentence, store this as well (linked to the object) + solved because it
    /// normally describes some characteristic of the object. -> not yet done
    /// -Also search for any compound words that this word contains + create them if they don't yet exist.
    ///
    /// -for each sense, also look up the synonyms in the 'Synonyms' data table, create an object cluster for each synonym
    /// (+ Add it the textneuron) and add it the synonym cluster.
    ///
    /// -next: do a lookup for each sense in the 'RelatedWords' datatable (query).  This contains all the relationships
    /// of the sense with other senses.  We do a lookup in this 'RelatedWords' table for the sense + each record from
    /// 'Relationships'.
    /// -for each relationship, create a new cluster, with as meaning, the relationship.  Lookup the correct object for
    /// each word (lookup synsetid in dict) and add the object to the cluster.
    /// -if the relationship is declared as recursive, perform the operation recursive untill a point is reached that is
    /// already in the database.
    ///
    /// - if the sense is a verb, also handle all of it's verbnet information.
    /// </para>
    /// <para>
    /// During generation, all IntValue neurons are reused. This means that all objects using the same synsetid loaded during the
    /// same run of the application, will use the same neuron as synset id.  If they are loaded on different runs, a new neuron
    /// is created.
    /// </para>
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.WordNetSin)]
    [NeuronID((ulong)PredefinedNeurons.SynSetID, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.CompoundWord, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.WordNetRelationships, typeof(NeuronCluster))]
    public class WordNetSin : Sin
    {
        #region inner types

        /// <summary>
        /// Stores the fields required for searching or creating a relationship cluster. Allows us to group them
        /// together without having to pass a lot of args.
        /// </summary>
        private class RelationshipArgs
        {
            public wordnetDataSet.RelationshipsRow Relation;
            public int Sense;
            public string Word;
            public string Pos;
            public int SynsetId;
            public wordnetDataSet.RelatedWordsDataTable Data;
            public NeuronCluster Result;
            public ulong RelationshipLink;
        }

        #endregion inner types

        #region Fields

        private static WordNetSin fDefault;
        private static Dictionary<string, ulong> fLinkDefs;
        private Dictionary<int, IntNeuron> fIntValues = new Dictionary<int, IntNeuron>();              //dictionary used to store already generated IntNeurons in so that we can reuse id value neurons during generation
        private ReaderWriterLockSlim fIntValuesLock = new ReaderWriterLockSlim();                      //provides multi thread safe access to the IntValues dict, requiered cause an import can be performed from many different threads at the same time.

        #endregion Fields

        #region Events

        /// <summary>
        /// Occurs when the <see cref="WordNetSin"/> has created a new root 'object' cluster for a relationship type.
        /// </summary>
        public event NewRootHandler NewRoot;

        /// <summary>
        /// Occurs when a new neuron was created for a relationship type that did not yet exist.
        /// </summary>
        /// <remarks>
        /// Warning: this event can be called from multiple threads, keep this in mind when processing for UI.
        /// </remarks>
        public event NewRelationshipTypeEventHandler RelationshipTypeCreated;

        /// <summary>
        /// Occurs when a new object was created by WordNet.
        /// </summary>
        /// <remarks>
        /// Warning: this event can be called from multiple threads, keep this in mind when processing for UI.
        /// </remarks>
        public event NewObjectEventHandler ObjectCreated;

        /// <summary>
        /// Occurs when a new object was created by WordNet that represents a compound word (a word that is made up of
        /// several sub words).
        /// </summary>
        /// <remarks>
        /// Warning: this event can be called from multiple threads, keep this in mind when processing for UI.
        /// </remarks>
        public event NewObjectEventHandler CompoundObjectCreated;

        /// <summary>
        /// Occurs when a new cluster with related words is created for an object.
        /// </summary>
        /// <remarks>
        /// Warning: this event can be called from multiple threads, keep this in mind when processing for UI.
        /// </remarks>
        public event NewRelationshipEventHandler RelationshipCreated;

        /// <summary>
        /// Occurs when a new search is started.
        /// </summary>
        public event EventHandler Started;

        /// <summary>
        /// Occurs when a search has finihsed.
        /// </summary>
        public event EventHandler Finihsed;

        #endregion Events

        #region Prop

        #region Default

        /// <summary>
        /// Gets the entry point.
        /// </summary>
        /// <remarks>
        /// A brain usually only has 1 WordNetSin (although theoretically more are allowed). This prop simply provides
        /// quick acces to it.
        /// </remarks>
        public static WordNetSin Default
        {
            get
            {
                if (fDefault == null)                                                                              //try to load the WordNetSin.
                    fDefault = Brain.Current[(ulong)PredefinedNeurons.WordNetSin] as WordNetSin;
                return fDefault;
            }
        }

        #endregion Default

        #region LinkDefs

        /// <summary>
        /// Gets the dictionary containing all the known words with their corresponding Neuron id.
        /// </summary>
        /// <remarks>
        /// this is loaded from
        /// </remarks>
        public static Dictionary<string, ulong> LinkDefs
        {
            get
            {
                if (fLinkDefs == null && Brain.Current != null)
                {
                    fLinkDefs = Brain.Current.Storage.GetProperty<Dictionary<string, ulong>>(typeof(WordNetSin), "LinkDefs");
                    if (fLinkDefs == null)
                        fLinkDefs = new Dictionary<string, ulong>();
                }
                else if (Brain.Current == null)
                    Log.LogError("WordNetSin.LinkDefs", "Can't load LinkDef dictionray, Brain not yet loaded!");
                return fLinkDefs;
            }
        }

        #endregion LinkDefs

        /// <summary>
        /// Gets or sets a value indicating whether to include description info for the lemma when a new object is created.
        /// </summary>
        /// <value><c>true</c> if [include description]; otherwise, <c>false</c>.</value>
        public bool IncludeDescription { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to include compound words in the import process. A compound word is any
        /// word (or group of words) that includes the searchterm, so a %word% serach is done.
        /// </summary>
        /// <remarks>
        /// This value is used for all imports wether they come from requests from the brain or direct function calls.
        /// </remarks>
        /// <value>
        /// 	<c>true</c> if [include compound words]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludeCompoundWords { get; set; }

        #endregion Prop

        #region Event callers

        /// <summary>
        /// Called when <see cref="WordNetSin.RelationshipTypeCreated"/> is raised.
        /// </summary>
        /// <param name="relationship">The relationship for which a neuron was created.</param>
        /// <param name="neuron">The neuron that was created.</param>
        protected virtual void OnRelationshipTypeCreated(string relationship, Neuron neuron)
        {
            if (RelationshipTypeCreated != null)
                RelationshipTypeCreated(this, new NewRelationshipTypeEventArgs() { Neuron = neuron, Relationship = relationship });
        }

        /// <summary>
        /// Called when the <see cref="WordNetSin.ObjectCreated"/> is raised.
        /// </summary>
        /// <param name="value">The value for which a new object was created.</param>
        /// <param name="cluster">The cluster that was created.</param>
        /// <param name="id">The id for which the new object was created.</param>
        protected virtual void OnObjectCreated(string value, NeuronCluster cluster, int id)
        {
            if (ObjectCreated != null)
            {
                string iDesc = null;
                if (IncludeDescription == true)
                {
                    LemmaDescTableAdapter iAdapdter = new LemmaDescTableAdapter();
                    wordnetDataSet.LemmaDescDataTable iData = iAdapdter.GetData(value, id);
                    if (iData.Count > 0)
                        iDesc = iData[0].definition;
                }
                ObjectCreated(this, new NewObjectEventArgs() { Neuron = cluster, Value = value, SynSetId = id, Description = iDesc });
            }
        }

        /// <summary>
        /// Called when the <see cref="WordNetSin.CompoundObjectCreated"/> is raised.
        /// </summary>
        /// <param name="value">The value for which a new object was created.</param>
        /// <param name="cluster">The cluster that was created.</param>
        /// <param name="id">The synsetid for which the new object was created.</param>
        protected virtual void OnCompoundObjectCreated(string value, NeuronCluster cluster, int id)
        {
            if (CompoundObjectCreated != null)
                CompoundObjectCreated(this, new NewObjectEventArgs() { Neuron = cluster, Value = value, SynSetId = id });
        }

        /// <summary>
        /// Called when the <see cref="WordNetSin.CompoundObjectCreated"/> is raised.
        /// </summary>
        /// <param name="cluster">The cluster that was created.</param>
        /// <param name="related">The cluster that is related to the created object.</param>
        /// <param name="id">The synsetid for which the new object was created.</param>
        protected virtual void OnRelationshipCreated(NeuronCluster cluster, NeuronCluster related, int id)
        {
            if (RelationshipCreated != null)
                RelationshipCreated(this, new NewRelationshipEventArgs() { Neuron = cluster, Related = related, SynsetId = id });
        }

        /// <summary>
        /// Called when a search is started. Raises the <see cref="WordNetSin.Started"/> event.
        /// </summary>
        protected virtual void OnStarted()
        {
            if (Started != null)
                Started(this, EventArgs.Empty);
        }

        /// <summary>
        /// Called when a search is finished. Raises the <see cref="WordNetSin.Finished"/> event.
        /// </summary>
        protected virtual void OnFinished()
        {
            if (Finihsed != null)
                Finihsed(this, EventArgs.Empty);
        }

        /// <summary>
        /// Called when a new root is found for a relationship.
        /// </summary>
        /// <param name="relationship">The relationship.</param>
        /// <param name="item">The item.</param>
        protected virtual void OnFoundRoot(Neuron relationship, NeuronCluster item)
        {
            if (NewRoot != null)
                NewRoot(this, new NewRootEventArgs() { Relationship = relationship, Item = item });
        }

        #endregion Event callers

        #region Functions

        #region Overrides

        /// <summary>
        /// Creates an exact duplicate of this Neuron so the <see cref="Processor"/> can perform a split.
        /// </summary>
        /// <returns>
        /// An exact duplicate of the argument, but with a new id.
        /// </returns>
        /// <remarks>
        /// A new id is created for the neuron cause all neurons should have unique numbers.
        /// </remarks>
        public override Neuron Duplicate()
        {
            throw new BrainException("WordNet sins can't be duplicated");
        }

        /// <summary>
        /// Tries to translate the specified neuron to the output type of the Sin and send it to the outside world.
        /// </summary>
        /// <param name="toSend"></param>
        /// <remarks>
        ///  <para>
        ///  Only textneurons are allowed.
        /// </para>
        /// 	<para>
        /// This method is called by the <see cref="Brain"/> itself during/after processing of input.
        /// </para>
        /// </remarks>
        public override void Output(Neuron toSend)
        {
            TextNeuron iText = toSend as TextNeuron;
            if (iText != null)
            {
                Log.LogInfo("WordNetSin.Output", string.Format("Learning about '{0}' from WordNet.", iText.Text));
                SearchOnWordNet(iText);
            }
            else
                Log.LogError("WordNetSin.Output", "Only textneurons are accepted as valid data to output.");
        }

        /// <summary>
        /// Called when the data needs to be saved.
        /// </summary>
        public override void Flush()
        {
            if (fLinkDefs != null && Brain.Current != null)
                Brain.Current.Storage.SaveProperty(typeof(WordNetSin), "LinkDefs", fLinkDefs);
        }

        #endregion Overrides

        /// <summary>
        /// Tries to find the record with specified text and synsetID, and imports all the relationships defined in wordnet
        /// with objects that have already been created (so only loads 1 object with possible relationships, not everything
        /// related to the word).
        /// </summary>
        /// <param name="Text">The text to search.</param>
        /// <param name="SynsetID">The synset ID of the word to import.</param>
        /// <returns></returns>
        public Neuron LoadCompact(string Text, int SynsetID)
        {
            try
            {
                WordInfoTableAdapter iAdapter = new WordInfoTableAdapter();
                wordnetDataSet.WordInfoDataTable iTable = iAdapter.GetFor(Text, SynsetID);
                if (iTable.Count > 0)                                                                  //only process if we found items.
                {
                    wordnetDataSet.WordInfoRow iRow = iTable[0];
                    NeuronCluster iObject = GetObject(Text, SynsetID);
                    StoreSentencePartType(iObject, iRow.pos);
                    StoreExample(iObject, iRow.wsd);
                    BuildSynonymCluster(iObject, SynsetID);
                    //AttachToSynonymsCluster(Text, SynsetID, iObject);
                    AttachRelationshipsFor(iObject, iRow.Sense, SynsetID, Text, iRow.pos);
                    if (iRow.pos == "v")
                        ProcessVerbNet(iObject, iRow);
                    return iObject;
                }
                else
                    Log.LogError("WordNetSing.LoadCompact", string.Format("Failed to find '{0}' (SynsetID: {1}) in wordnet.", Text, SynsetID));
            }
            catch (Exception e)
            {
                Log.LogError("WordNetSing.LoadCompact", string.Format("Failed to load data for the word '{0}' (SynsetID: {2}) into the network with the exception: {1}.", Text, e.ToString(), SynsetID));
            }
            return null;
        }

        /// <summary>
        /// Tries to find the specified text in the WordNet database and load all the related info into the <see cref="Brain"/>.
        /// </summary>
        /// <remarks>
        /// This function catches all exceptions and displays them in the log, so it is a safe entry point into the database.
        /// </remarks>
        /// <param name="toLoad">The string to load. This will be translated into a singled TextNeuron which is searched for.</param>
        public void Load(string toLoad)
        {
            try
            {
                ulong iTextId;
                TextNeuron iText;
                if (TextSin.Words.TryGetValue(toLoad, out iTextId) == true)
                    iText = Brain.Current[iTextId] as TextNeuron;
                else
                {
                    iText = new TextNeuron(toLoad);
                    Brain.Current.Add(iText);
                    TextSin.Words.Add(toLoad, iText.ID);
                }
                if (iText != null)
                {
                    Log.LogInfo("WordNetSin.Output", string.Format("Learning about '{0}' from WordNet.", iText.Text));
                    SearchOnWordNet(iText);
                }
                else
                    Log.LogError("WordNetSin.Output", string.Format("Failed to learn about '{0}' from WordNet for an unkown reason.", toLoad));
            }
            catch (Exception e)
            {
                Log.LogError("WordNetSing.Load", string.Format("Failed to load data for the word '{0}' into the network with the exception: {1}.", toLoad, e.ToString()));
            }
        }

        /// <summary>
        /// Searches the specified text on wordNet and creates all the brain neurons for the result, or indicates
        /// that nothing was found.
        /// </summary>
        /// <param name="text">The text to search. This should always be a single word without spaces (no compound
        /// words allowed), these are automically loaded for single words.  </param>
        private void SearchOnWordNet(TextNeuron text)
        {
            OnStarted();
            try
            {
                bool iIncludeCompound = IncludeCompoundWords;                                       //we store this value before we begin cause the input can take some time and we want the value as it was when we started.
                foreach (wordnetDataSet.WordInfoRow i in GetWordInfoFor(text.Text))
                {
                    NeuronCluster iObj = GetObject(text, i.synsetid);
                    StoreSentencePartType(iObj, i.pos);
                    StoreExample(iObj, i.wsd);
                    CreateSynonymCluster(iObj, i.synsetid);
                    BuildRelationships(iObj, i.Sense, i.synsetid, text.Text, i.pos);
                    if (iIncludeCompound == true)
                        BuildCompoundWords(iObj, i);
                    if (i.pos == "v")
                        ProcessVerbNet(iObj, i);
                }
            }
            finally
            {
                OnFinished();
            }
        }

        /// <summary>
        /// extracts all the verbnet infor from the sql db and inserts it in the neural db.
        /// </summary>
        /// <param name="iObj">The i obj.</param>
        /// <param name="info">All the info for the word (sense).</param>
        private void ProcessVerbNet(NeuronCluster obj, wordnetDataSet.WordInfoRow word)
        {
            Log.LogInfo("Wordnet.ProcessVerbnet", "Can't load verbnet data yet into brain!");
        }

        /// <summary>
        /// Searhces for and builds all the compound words that contain the specified word.
        /// </summary>
        /// <remarks>
        /// Does not check the value of <see cref="WordNetSin.IncludeCompoundWords"/>, will always render the compount words.
        /// </remarks>
        /// <param name="obj">The obj for which to search compound words.</param>
        /// <param name="word">The word to search for.</param>
        private void BuildCompoundWords(NeuronCluster obj, wordnetDataSet.WordInfoRow word)
        {
            CompundWordsTableAdapter iCompoundAdapter = new CompundWordsTableAdapter();
            wordnetDataSet.CompundWordsDataTable iData = iCompoundAdapter.GetData(word.Word.ToLower());
            foreach (wordnetDataSet.CompundWordsRow i in iData)
            {
                if (i.synsetid != word.synsetid)                                                                   //we don't want to store a compound link to itself.
                {
                    NeuronCluster iObj = CreateCompoundWord(i.Word);
                    OnCompoundObjectCreated(i.Word, iObj, i.synsetid);                                              //we got all the args for the event here, so raise from this point.
                    StoreSentencePartType(iObj, i.pos);
                    CreateSynonymCluster(iObj, i.synsetid);
                    BuildRelationships(iObj, i.Sense, i.synsetid, word.Word, word.pos);
                }
            }
        }

        /// <summary>
        /// Creates a Neuron cluster that represents a compound word for the specified string and returns
        /// the corresponding object cluster for the compound word.
        /// </summary>
        /// <remarks>
        /// To ge the elements of the compound word, we use a simple split on ' '.
        /// </remarks>
        /// <param name="text">The text containing the compound word.  </param>
        /// <returns>A neuroncluster which is the object that wraps the compound word cluster.</returns>
        private NeuronCluster CreateCompoundWord(string text)
        {
            text = text.ToLower();                                                                          //need to make certain we work in lower caps.
            NeuronCluster iCompound = new NeuronCluster();
            iCompound.Meaning = (ulong)PredefinedNeurons.CompoundWord;
            Brain.Current.Add(iCompound);
            string[] iWords = text.Split(new char[] { ' ' });                                             //extract all the seperate words.
            foreach (string iWord in iWords)
            {
                TextNeuron iText;
                ulong iTextId;
                if (TextSin.Words.TryGetValue(iWord, out iTextId) == true)
                    iText = Brain.Current[iTextId] as TextNeuron;
                else
                {
                    iText = new TextNeuron();
                    iText.Text = iWord;
                    Brain.Current.Add(iText);
                    TextSin.Words.Add(iWord, iText.ID);
                }
                using (ChildrenAccessor iList = iCompound.ChildrenW)
                    iList.Add(iText);
            }
            NeuronCluster iRes = new NeuronCluster();
            iRes.Meaning = (ulong)PredefinedNeurons.Object;
            Brain.Current.Add(iRes);
            using (ChildrenAccessor iList = iRes.ChildrenW)
                iList.Add(iCompound);
            return iRes;
        }

        /// <summary>
        /// Looks up the related words that are already loaded in the brain for all the relationships defined in wordnet,and makes
        /// certain they are also loaded in the brain.
        /// </summary>
        /// <param name="obj">The object to update</param>
        /// <param name="sense">The sense</param>
        /// <param name="synsetid">The synsetid.</param>
        /// <param name="word">The word.</param>
        /// <param name="pos">The pos.</param>
        private void AttachRelationshipsFor(NeuronCluster obj, int sense, int synsetid, string word, string pos)
        {
            RelationshipsTableAdapter iRelAdapater = new RelationshipsTableAdapter();
            wordnetDataSet.RelationshipsDataTable iRelData = iRelAdapater.GetData();
            foreach (wordnetDataSet.RelationshipsRow i in iRelData)
            {
                RelationshipArgs iArgs = new RelationshipArgs();
                RelatedWordsTableAdapter iWordsAdapter = new RelatedWordsTableAdapter();
                wordnetDataSet.RelatedWordsDataTable iWordsData = iWordsAdapter.GetData(pos, i.linkid, synsetid, word);
                if (iWordsData.Count > 0)
                {
                    iArgs.Pos = pos;
                    iArgs.Word = word;
                    iArgs.Relation = i;
                    iArgs.Sense = sense;
                    iArgs.SynsetId = synsetid;
                    iArgs.Data = iWordsData;
                    iArgs.RelationshipLink = GetLinkDef(i.name);
                    if (FindRelationship(iArgs) == true)
                    {
                        ulong iLinkDef = GetLinkDef(i.name);
                        if (iArgs.Result != null && obj.FindFirstOut(iLinkDef) != iArgs.Result)                          //we check if the link didn't already exist, so that we don't raise inproper events.
                        {
                            obj.SetFirstOutgoingLinkTo(iLinkDef, iArgs.Result);                                   //we use this style of creating the link, this way, if there was a previous link with a wrong value, this is correctly updated.
                            OnRelationshipCreated(obj, iArgs.Result, synsetid);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Creates all the relationships of the word in the brain.
        /// </summary>
        /// <remarks>
        /// A relationship is always stored in the following manner:
        /// - a new neuron cluster is made that contains all the items that are part of the relationship (except
        /// the 'word' itself).
        /// - this cluster gets as meaning, the relationship type.
        /// - a link is made between the word and the new cluster, whith as meaning, the relationship type.
        /// -> this structure allows us to do a quick search in 2 directions: from a given word, find all the objects
        /// that are related in the specified way (find a link with the specified meaning to a cluster).  You can
        /// als search the other way: find the word to which this is 'the relationship type): find a cluster to which
        /// the neuron belongs which has the specified relationship type, find the link 'out' on that cluster with the
        /// relationship type as meaning and you have found the related word.
        /// </remarks>
        /// <param name="obj">The obj that represents the word.</param>
        /// <param name="synsetid">The synsed id to use.</param>
        private void BuildRelationships(NeuronCluster obj, int sense, int synsetid, string word, string pos)
        {
            RelationshipsTableAdapter iRelAdapater = new RelationshipsTableAdapter();
            wordnetDataSet.RelationshipsDataTable iRelData = iRelAdapater.GetData();
            foreach (wordnetDataSet.RelationshipsRow i in iRelData)
            {
                ulong iLinkDef = GetLinkDef(i.name);
                NeuronCluster iRes = GetRelationship(i, sense, synsetid, word, pos);
                if (iRes != null && obj.FindFirstOut(iLinkDef) != iRes)                          //we check if the link didn't already exist, so that we don't raise inproper events.
                {
                    obj.SetFirstOutgoingLinkTo(iLinkDef, iRes);                                   //we use this style of creating the link, this way, if there was a previous link with a wrong value, this is correctly updated.
                    OnRelationshipCreated(obj, iRes, synsetid);
                }
            }
        }

        /// <summary>
        /// Creates the cluster containing all the synonyms.
        /// </summary>
        /// <param name="obj">The object that was created</param>
        /// <param name="sysnetId">The sysnet id, all the synonyms have the same id.</param>
        private void CreateSynonymCluster(NeuronCluster obj, int sysnetId)
        {
            ulong iSynLinkDef = GetLinkDef(Properties.Resources.Synonyms);
            NeuronCluster iSynCluster = obj.FindFirstClusteredBy(iSynLinkDef);
            if (iSynCluster == null)                                                                  //we only create the syncluster if it doesn't exist, this allows us to process already loaded words without overwriting stuff.
            {
                SynonymsTableAdapter iSynonymsAdapter = new SynonymsTableAdapter();
                wordnetDataSet.SynonymsDataTable iSynonymsData = iSynonymsAdapter.GetData(sysnetId);
                foreach (wordnetDataSet.SynonymsRow i in iSynonymsData)
                {
                    NeuronCluster iSyn = GetObject(i.word, i.synsetid);
                    Debug.Assert(iSyn != null);
                    if (iSynCluster == null)                                                                     //there is atleast 1 synonym, so create the cluster cause it hasn't been done yet.
                    {
                        iSynCluster = new NeuronCluster();                                                        //We can always create a new cluster, don't have to look for an already existing one: if the word had already been searched, this object would already have been added to the cluster.
                        iSynCluster.Meaning = iSynLinkDef;
                        Brain.Current.Add(iSynCluster);
                    }
                    using (ChildrenAccessor iList = iSynCluster.ChildrenW)
                        iList.Add(iSyn);
                }
            }
        }

        /// <summary>
        /// Finds all the already loaded objects and creates or finds the cluster containing all the synonyms that the object belongs to
        /// and adds the remaining items to them.
        /// </summary>
        /// <param name="obj">The object that was created</param>
        /// <param name="sysnetId">The sysnet id, all the synonyms have the same id.</param>
        private void BuildSynonymCluster(NeuronCluster obj, int sysnetId)
        {
            ulong iSynLinkDef = GetLinkDef(Properties.Resources.Synonyms);
            NeuronCluster iSynCluster = obj.FindFirstClusteredBy(iSynLinkDef);
            if (iSynCluster == null)                                                                  //we only create the syncluster if it doesn't exist, this allows us to process already loaded words without overwriting stuff.
            {
                SynonymsTableAdapter iSynonymsAdapter = new SynonymsTableAdapter();
                wordnetDataSet.SynonymsDataTable iSynonymsData = iSynonymsAdapter.GetData(sysnetId);

                List<Neuron> iSynonyms = new List<Neuron>();
                foreach (wordnetDataSet.SynonymsRow i in iSynonymsData)
                {
                    NeuronCluster iSyn = FindObject(i.word, i.synsetid);
                    if (iSyn != null)
                    {
                        iSynonyms.Add(iSyn);
                        if (iSynCluster == null)
                            iSynCluster = iSyn.FindFirstClusteredBy(iSynLinkDef);
                    }
                }
                if (iSynCluster == null)                                                                     //there is atleast 1 synonym, so create the cluster cause it hasn't been done yet.
                {
                    iSynCluster = new NeuronCluster();                                                        //We can always create a new cluster, don't have to look for an already existing one: if the word had already been searched, this object would already have been added to the cluster.
                    iSynCluster.Meaning = iSynLinkDef;
                    Brain.Current.Add(iSynCluster);
                    using (ChildrenAccessor iList = iSynCluster.ChildrenW)
                    {
                        foreach (Neuron i in iSynonyms)
                            iList.Add(i);
                    }
                }
                else
                {
                    using (ChildrenAccessor iList = iSynCluster.ChildrenW)
                        iList.Add(obj);
                }
            }
        }

        /// <summary>
        /// Tries to find the 'synonyms' cluster for the specified synset, if there is one, adds the object
        /// to it.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="synsetID">The synset ID.</param>
        /// <param name="obj">The obj.</param>
        private void AttachToSynonymsCluster(string text, int sysnetId, NeuronCluster obj)
        {
            ulong iSynLinkDef = GetLinkDef(Properties.Resources.Synonyms);
            SynonymsTableAdapter iSynonymsAdapter = new SynonymsTableAdapter();
            wordnetDataSet.SynonymsDataTable iSynonymsData = iSynonymsAdapter.GetData(sysnetId);
            foreach (wordnetDataSet.SynonymsRow i in iSynonymsData)
            {
                NeuronCluster iFound = FindObject(i.word, i.synsetid);
                if (iFound != null && iFound != obj)
                {
                    NeuronCluster iSynCluster = obj.FindFirstClusteredBy(iSynLinkDef);
                    if (iSynCluster != null)
                    {
                        using (ChildrenAccessor iList = iSynCluster.ChildrenW)
                            iList.Add(obj);
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the ID of the neuron that represents the link def in the brain. This can be used as the
        /// meaning of a cluster for instance.
        /// </summary>
        /// <remarks>
        /// If there is no neuron yet defined for this type of relationship, one is created and wrapped in an object that also contains
        /// a textNeuron for the key.
        /// </remarks>
        /// <param name="key">The textvalue of the relationship.</param>
        /// <returns>The key of the neuron that represents the relationship.</returns>
        public ulong GetLinkDef(string key)
        {
            ulong iFound;
            if (LinkDefs.TryGetValue(key, out iFound) == false)
            {
                Neuron iNew;
                NeuronCluster iObj = BrainHelper.CreateObject(key, out iNew);                                                                       //we create an intire object
                NeuronCluster iRelCluster = Brain.Current[(ulong)PredefinedNeurons.WordNetRelationships] as NeuronCluster;     //also need to store the linkDef in the Relationships cluster so that the system knows this neuron is a relationship.
                Debug.Assert(iRelCluster != null);
                using (ChildrenAccessor iList = iRelCluster.ChildrenW)
                    iList.Add(iObj);                                                                                //we add the object, not the text directly, this is more inline with how the rest of the system works (2 dec 2008).
                iFound = iNew.ID;
                LinkDefs[key] = iFound;
                OnRelationshipTypeCreated(key, iNew);
            }
            return iFound;
        }

        /// <summary>
        /// Stores the example sentence together with the object + solves the sentence so that it's content is also
        /// stored as a link relationship.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <param name="wsd">The WSD.</param>
        private void StoreExample(NeuronCluster obj, string wsd)
        {
            if (wsd != null)
            {
                Debug.Print("Todo: store the example in the brain.");
                Log.LogWarning("Wordnetsin.StoreExample", "not yet written, oeps!");
            }
        }

        /// <summary>
        /// Assigns the type of the sentence part to the neuron through a link.
        /// </summary>
        /// <remarks>
        /// If theres is a prev value and it is different, it is updated.
        /// </remarks>
        /// <param name="obj">The obj to assign the sentence part type.</param>
        /// <param name="pos">The part of speech (sentence part type) to assign to the item.</param>
        private void StoreSentencePartType(Neuron obj, string pos)
        {
            if (pos == "v")//verb
                obj.SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.POS, Brain.Current[(ulong)PredefinedNeurons.Verb]);
            else if (pos == "n")//noun
                obj.SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.POS, Brain.Current[(ulong)PredefinedNeurons.Noun]);
            else if (pos == "a")//adjective
                obj.SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.POS, Brain.Current[(ulong)PredefinedNeurons.Adjective]);
            else if (pos == "r")//adverb
                obj.SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.POS, Brain.Current[(ulong)PredefinedNeurons.Adverb]);
        }

        #region Object

        /// <summary>
        /// Gets the cluster that represents the object with the specified synset id.  If it can't find it, one is created.
        /// </summary>
        /// <param name="text">The text to search for.</param>
        /// <param name="synsedId">The synsed id.</param>
        /// <returns>A neuroncluster that represents the object</returns>
        public NeuronCluster GetObject(string text, int synsetId)
        {
            NeuronCluster iSyn = null;
            ulong iTextId;
            if (TextSin.Words.TryGetValue(text, out iTextId) == true)
            {
                TextNeuron iText = Brain.Current[iTextId] as TextNeuron;
                iSyn = GetObject(iText, synsetId);
            }
            if (iSyn == null)                                                          //when we get here, there was no syn created yet properly, so try again with a complete new textneuron.
                iSyn = CreateObject(text, synsetId);
            return iSyn;
        }

        /// <summary>
        /// Gets the cluster that represents the object with the specified synset id.  If it can't find it, one is created.
        /// </summary>
        /// <param name="text">The text to search for as a neuron.</param>
        /// <param name="synsedId">The synsed id.</param>
        /// <returns>A neuroncluster that represents the object</returns>
        public NeuronCluster GetObject(TextNeuron text, int synsetId)
        {
            NeuronCluster iSyn = null;
            if (text != null)                                                      //this can be null if its not a TextNeuron.  Can happen if there is some sort of data inconsistency.
            {
                iSyn = FindObject(text, synsetId);
                if (iSyn == null)
                    iSyn = CreateObject(text, synsetId);
            }
            return iSyn;
        }

        /// <summary>
        /// Tries to find the neuron cluster that represents the object for the specified text and synsetid.
        /// If it doesn't find something, it returns null.
        /// </summary>
        /// <param name="text">The text to search for.</param>
        /// <param name="synsedId">The synsed id.</param>
        /// <returns>A neuroncluster that represents the object, or null if nothing is found.</returns>
        public NeuronCluster FindObject(string text, int synsedId)
        {
            ulong iTextId;
            if (TextSin.Words.TryGetValue(text, out iTextId) == true)
            {
                TextNeuron iText = Brain.Current[iTextId] as TextNeuron;
                if (iText != null)
                    return FindObject(iText, synsedId);
            }
            return null;
        }

        /// Tries to find the neuron cluster that represents the object for the specified text neuron and synsetid.
        /// If it doesn't find something, it returns null.
        /// </summary>
        /// <param name="textId">The id of the textneuron to search the object for.</param>
        /// <param name="synsedId">The synsed id.</param>
        /// <returns>A neuroncluster that represents the object, or null if nothing is found.</returns>
        private NeuronCluster FindObject(TextNeuron text, int synsedId)
        {
            if (text != null)
            {
                using (NeuronsAccessor iClusteredBy = text.ClusteredBy)
                {
                    List<NeuronCluster> iClusters = (from i in iClusteredBy.Items
                                                     where ((NeuronCluster)Brain.Current[i]).Meaning == (ulong)PredefinedNeurons.Object
                                                     select ((NeuronCluster)Brain.Current[i])).ToList();
                    foreach (NeuronCluster i in iClusters)
                    {
                        IntNeuron iFound = i.FindFirstOut((ulong)PredefinedNeurons.SynSetID) as IntNeuron;
                        if (iFound != null && iFound.Value == synsedId)
                            return i;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Creates a new cluster with meaning 'same' and adds a new text neuron to it.
        /// </summary>
        /// <remarks>
        /// Doesn't directly add to textsin dict, but is done indirectly.
        /// </remarks>
        /// <param name="text">The text that should be assigned to the text neuron that is created.</param>
        /// <param name="synsetid">The synsetid of the sense. This is also stored in the object so that we can find it again.  It also
        /// tags it as comming from wordnet.</param>
        /// <returns></returns>
        private NeuronCluster CreateObject(string text, int synsetid)
        {
            TextNeuron iText = new TextNeuron();
            iText.Text = text;
            Brain.Current.Add(iText);
            return CreateObject(iText, synsetid);
        }

        /// <summary>
        /// Creates a new cluster with meaning 'same' and adds the textneuron to it.
        /// </summary>
        /// <param name="text">The neuron to add to the cluster.</param>
        /// <param name="synsetid">The synsetid of the sense. This is also stored in the object so that we can find it again.  It also
        /// tags it as comming from wordnet.</param>
        /// <returns></returns>
        /// <remarks>
        /// Also makes certain that the textneuron is stored in the dictionary of the textsin so that it can be used.
        /// </remarks>
        private NeuronCluster CreateObject(TextNeuron text, int synsetid)
        {
            NeuronCluster iRes = CreateSynsetCluster(synsetid);
            iRes.Meaning = (ulong)PredefinedNeurons.Object;
            using (ChildrenAccessor iList = iRes.ChildrenW)
                iList.Add(text);
            TextSin.Words[text.Text.ToLower()] = text.ID;                                                      //always store the data in lowercase.
            OnObjectCreated(text.Text, iRes, synsetid);
            return iRes;
        }

        #endregion Object

        #region Relationships

        /// <summary>
        /// Checks if the relationship cluster already exists, if so, this is returned, otherwise a new one is created.
        /// </summary>
        /// <param name="rel">The relationship for which to get a cluster</param>
        /// <param name="sense">The sense.</param>
        /// <param name="synsetid">The synsed id to use.</param>
        /// <returns></returns>
        private NeuronCluster GetRelationship(wordnetDataSet.RelationshipsRow rel, int sense, int synsetid, string word, string pos)
        {
            RelationshipArgs iArgs = new RelationshipArgs();
            RelatedWordsTableAdapter iWordsAdapter = new RelatedWordsTableAdapter();
            wordnetDataSet.RelatedWordsDataTable iWordsData = iWordsAdapter.GetData(pos, rel.linkid, synsetid, word);
            if (iWordsData.Count > 0)
            {
                iArgs.Pos = pos;
                iArgs.Word = word;
                iArgs.Relation = rel;
                iArgs.Sense = sense;
                iArgs.SynsetId = synsetid;
                iArgs.Data = iWordsData;
                iArgs.RelationshipLink = GetLinkDef(rel.name);
                if (FindRelationship(iArgs) == false)
                    BuildRelationship(iArgs);
                return iArgs.Result;
            }
            return null;
        }

        /// <summary>
        /// Tries to find a cluster that represents the relationship defined in the args for the specified words.
        /// </summary>
        /// <param name="e">The arguments + possible result.</param>
        /// <returns><c>true</c> if the relationship was found, otherwise <c>false</c>.</returns>
        private bool FindRelationship(RelationshipArgs e)
        {
            List<ulong> iCommonClusters = null;
            //we get the object for each word and see which clusters they have in common.
            //when we finally found all the common clusters, we will see if there is any with the
            //correct meaning and the exact number of items as in the data list, this is our result.
            foreach (wordnetDataSet.RelatedWordsRow iRow in e.Data)
            {
                NeuronCluster iWord = FindObject(iRow.word, iRow.synsetid);
                if (iWord == null)                                                                        //the word isn't loaded yet, so the cluster can definitely not exist.
                    return false;
                using (NeuronsAccessor iClusteredBy = iWord.ClusteredBy)
                {
                    if (iCommonClusters == null)
                        iCommonClusters = iClusteredBy.Items.ToList();                                           //we convert to list cause we need to modify it later on.
                    else
                        iCommonClusters = iCommonClusters.Union(iClusteredBy.Items).ToList();
                }
            }
            List<NeuronCluster> iRes = new List<NeuronCluster>();
            foreach (ulong iID in iCommonClusters)                                                       //can't put this loop in a linq statement, cause we need the using clause, if we don't it could be that the ChildrenAccessor is still in memory at a time that we want to add to the list, and this accessor should already be cleaned up.
            {
                NeuronCluster i = (NeuronCluster)Brain.Current[iID];
                using (ChildrenAccessor iChildren = i.Children)
                {
                    if (i.Meaning == e.RelationshipLink && iChildren.Count >= e.Data.Count)
                        iRes.Add(i);
                }
            }
            if (iRes.Count == 1)
            {
                e.Result = iRes[0];
                return true;
            }
            return false;
        }

        /// <summary>
        /// Creates a cluster and fills it with all the objects that are related to it as specified in the rel arg.
        /// </summary>
        /// <param name="rel">The relationship for which to build a cluster</param>
        /// <param name="sense">The sense.</param>
        /// <param name="synsetid">The synsed id to use.</param>
        /// <returns></returns>
        private void BuildRelationship(RelationshipArgs args)
        {
            NeuronCluster iCluster = CreateSynsetCluster(args.SynsetId);
            iCluster.Meaning = args.RelationshipLink;
            foreach (wordnetDataSet.RelatedWordsRow iRow in args.Data)
            {
                NeuronCluster iWord = GetObject(iRow.word, iRow.synsetid);
                Debug.Assert(iWord != null);
                if (args.Relation.recurses == "Y")                                                                             //if this specified as recursive, we need to do so.
                {
                    NeuronCluster iLinkTo = GetRelationship(args.Relation, iRow.Sense, iRow.synsetid, iRow.word, iRow.pos);
                    if (iLinkTo != null)                                                                                     //if there is a cluster up the relationship, continue building the tree, otherwise, let the rest of the system know we found a root item for a recursive relationship.
                    {
                        if (Link.Exists(iWord, iLinkTo, iCluster.Meaning) == false)
                        {
                            Link iLink = new Link(iLinkTo, iWord, iCluster.Meaning);                                           //we use the same meaning as the cluster's to create a link from the object, to it's related objects.
                            OnRelationshipCreated(iWord, iLinkTo, iRow.synsetid);
                        }
                    }
                    else
                        OnFoundRoot(Brain.Current[args.RelationshipLink], iWord);
                }
                using (ChildrenAccessor iList = iCluster.ChildrenW)
                {
                    if (iList.Contains(iWord) == false)                                                             //we check that the cluster doesn't already have the item.  This somehow happens, probably because some items are double checked.
                        iList.Add(iWord);
                }
            }
            args.Result = iCluster;
        }

        /// <summary>
        /// Checks if the specified object contains a link to another object with the specified meaning + that
        /// points to an object that links to the specified synsetid.
        /// </summary>
        /// <param name="item">The item to start the search from.</param>
        /// <param name="meaning">The meaning to search for on the item.</param>
        /// <param name="synsetid">The synsetid that should be found on the 'To' neuron, of the 'meaning' link.</param>
        /// <returns></returns>
        private bool Exists(Neuron item, ulong meaning, int synsetid)
        {
            using (LinksAccessor iLinksOut = item.LinksOut)
            {
                var iList = from i in iLinksOut.Items
                            where i.MeaningID == meaning
                            select i.To;
                foreach (Neuron i in iList)
                {
                    IntNeuron iId = i.FindFirstOut((ulong)PredefinedNeurons.SynSetID) as IntNeuron;
                    if (iId != null && iId.Value == synsetid)
                        return true;
                }
            }
            return false;
        }

        #endregion Relationships

        /// <summary>
        /// Creates a NeuronCluster and assigns it the specified synsetID. The clusterr doesn't have a meaning.
        /// </summary>
        /// <param name="synsetid">The synsetid to assign to the cluster.</param>
        /// <returns>A cluster with a link to an intneuron, both registered.</returns>
        private NeuronCluster CreateSynsetCluster(int synsetid)
        {
            NeuronCluster iRes = new NeuronCluster();
            Brain.Current.Add(iRes);
            IntNeuron iId = null;
            fIntValuesLock.EnterUpgradeableReadLock();
            try
            {
                if (fIntValues.TryGetValue(synsetid, out iId) == false)
                {
                    iId = new IntNeuron();
                    iId.Value = synsetid;
                    Brain.Current.Add(iId);
                    fIntValuesLock.EnterWriteLock();
                    try
                    {
                        fIntValues.Add(synsetid, iId);
                    }
                    finally
                    {
                        fIntValuesLock.ExitWriteLock();
                    }
                }
            }
            finally
            {
                fIntValuesLock.ExitUpgradeableReadLock();
            }
            Link iLink = new Link(iId, iRes, Brain.Current[(ulong)PredefinedNeurons.SynSetID]);
            return iRes;
        }

        #region data retrieving

        /// <summary>
        /// Gets all the different meanings for the specified word.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public IEnumerable<wordnetDataSet.WordInfoRow> GetWordInfoFor(string text)
        {
            WordInfoTableAdapter iWordInfoAdapter = new WordInfoTableAdapter();
            return iWordInfoAdapter.GetData(text);
        }

        public IEnumerable<wordnetDataSet.WordInfoRow> GetWordInfoFor(string text, string pos)
        {
            WordInfoTableAdapter iWordInfoAdapter = new WordInfoTableAdapter();
            return iWordInfoAdapter.GetDataForPOS(text, pos);
        }

        #endregion data retrieving

        /// <summary>
        /// Creates a relationship between the 2 neurons with the specified relationship using the wordnet structure.
        /// </summary>
        /// <remarks>
        /// <para>
        /// If the 'from' neuron already has a link to a cluster for the specified relationship, the item is simply added,
        /// otherwise, a new cluster is created for the relationship and the item is added.
        /// </para>
        /// <para>
        /// Doesn't trigger any wordnet events (regular brain events are triggered like normal).
        /// </para>
        /// </remarks>
        /// <param name="start">From part of pseudo link.</param>
        /// <param name="to">To part of pseudo link.</param>
        /// <param name="relationship">The meaning part of pseudo link.</param>
        public void CreateRelationship(Neuron start, Neuron to, Neuron relationship)
        {
            NeuronCluster iFound;
            using (LinksAccessor iLinksOut = start.LinksOut)
                iFound = (from i in iLinksOut.Items
                          let iCluster = i.To as NeuronCluster
                          where i.MeaningID == relationship.ID && iCluster.Meaning == relationship.ID
                          select iCluster).FirstOrDefault();
            if (iFound == null)
            {
                iFound = new NeuronCluster();
                iFound.Meaning = relationship.ID;
                Brain.Current.Add(iFound);
                Link iLink = new Link(iFound, start, relationship);
            }
            using (ChildrenAccessor iList = iFound.ChildrenW)
                iList.Add(to);
        }

        #endregion Functions
    }
}