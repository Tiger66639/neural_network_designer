﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeuralNetworkDesigne.UndoSystem;
using System.Collections.Specialized;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// A custom undo item that stores changes in a <see cref="NeuronCluster"/>. This is done seperatly, outside the undo event system
   /// because this uses object references to wrappers, while we need neuron references because the wrappers for the neurons get 
   /// created and destroyed.
   /// </summary>
   public abstract class ClusterUndoItem : UndoItem
   {
      public NotifyCollectionChangedAction Action { get; internal set; }

      public NeuronCluster Cluster { get; set; }

      /// <summary>
      /// Checks if this undo item contains data from the specified source object.
      /// </summary>
      /// <param name="source">The object to check for.</param>
      /// <returns>
      /// True if this object contains undo data for the specified object.
      /// </returns>
      /// <remarks>
      /// This function is used by the undo system to clean up undo data from items that are no longer available.  This can happen for
      /// undo data generated from user interface controls which are no longer valid (loaded).
      /// </remarks>
      public override bool StoresDataFrom(object source)
      {
         return Cluster == source;
      }

      public override bool HasSameTarget(UndoItem toCompare)
      {
         ClusterUndoItem iToCompare = toCompare as ClusterUndoItem;
         if (iToCompare != null)
            return iToCompare.Cluster == Cluster && iToCompare.Action == Action;
         else
            return false;
      }
   }

   public abstract class ClusterItemsUndoItem : ClusterUndoItem
   {
      /// <summary>
      /// Gets or sets the items that were removed during the reset.
      /// </summary>
      /// <value>The items.</value>
      public IList<Neuron> Items { get; set; }

      public override bool HasSameTarget(UndoItem toCompare)
      {
         bool iRes = base.HasSameTarget(toCompare);
         if (iRes == true)
         {
            ClusterItemsUndoItem iItem = toCompare as ClusterItemsUndoItem;
            if (iItem != null && iItem.Items.Count == Items.Count)
            {
               foreach (Neuron i in iItem.Items)
                  if (Items.Contains(i) == false)
                     return false;
               return true;
            }
         }
         return false;
      }
   }

   public class ResetClusterUndoItem : ClusterItemsUndoItem
   {

      public ResetClusterUndoItem()
      {
         Action = NotifyCollectionChangedAction.Reset;
      }
      public override void Execute(UndoStore caller)
      {
         AddClusterUndoItem iUndo = new AddClusterUndoItem();
         iUndo.Cluster = Cluster;
         iUndo.Items = Items;
         iUndo.FromReset = true;
         WindowMain.UndoStore.AddCustomUndoItem(iUndo);
         using (ChildrenAccessor iChildren = Cluster.ChildrenW)
         {
            foreach (Neuron i in Items)
               iChildren.Add(i);
         }
      }
   }

   public class AddClusterUndoItem : ClusterItemsUndoItem
   {
      public AddClusterUndoItem()
      {
         FromReset = false;
         Action = NotifyCollectionChangedAction.Add;
      }

      /// <summary>
      /// Gets or sets a value indicating whether the undo came from a reversed reset, in which case we can do a reset again.
      /// False by default.
      /// </summary>
      /// <value><c>true</c> if [from reset]; otherwise, <c>false</c>.</value>
      public bool FromReset { get; set; }

      public override void Execute(UndoStore caller)
      {
         if (FromReset == true)
            DoReset(caller);
         else
            DoRemove(caller);
      }

      private void DoRemove(UndoStore caller)
      {
         RemoveClusterUndoItem iUndo = new RemoveClusterUndoItem();
         iUndo.Cluster = Cluster;
         iUndo.Items = Items;
         WindowMain.UndoStore.AddCustomUndoItem(iUndo);
         using (ChildrenAccessor iChildren = Cluster.ChildrenW)
         {
            iUndo.Index = iChildren.IndexOf(Items[0].ID);                                 //when the action gets reversed, we need to know the index of the first object (usually there is only 1 item, except during a reset).
            if (iUndo.Index != -1)
            {
               foreach (Neuron i in Items)
                  iChildren.Remove(i);
            }
            else
               throw new InvalidOperationException("Item not found in cluster, can't execute the undo item.");
         }
      }

      private void DoReset(UndoStore caller)
      {
         ResetClusterUndoItem iUndo = new ResetClusterUndoItem();
         iUndo.Cluster = Cluster;
         iUndo.Items = Items;
         WindowMain.UndoStore.AddCustomUndoItem(iUndo);
         using (ChildrenAccessor iChildren = Cluster.ChildrenW)
            iChildren.Clear();
      }
   }

   public class RemoveClusterUndoItem : ClusterItemsUndoItem
   {
      /// <summary>
      /// Gets or sets the index at which the first item was located when the remove happened.  -1 indicates to do
      /// an add instead of an insert when reversing the action.
      /// </summary>
      /// <value>The index.</value>
      public int Index { get; set; }

      public RemoveClusterUndoItem()
      {
         Action = NotifyCollectionChangedAction.Remove;
      }

      public override void Execute(UndoStore caller)
      {
         AddClusterUndoItem iUndo = new AddClusterUndoItem();
         iUndo.Cluster = Cluster;
         iUndo.Items = Items;
         WindowMain.UndoStore.AddCustomUndoItem(iUndo);
         using (ChildrenAccessor iChildren = Cluster.ChildrenW)
         {
            if (Index == -1)
            {
               foreach (Neuron i in Items)
                  iChildren.Add(i);
            }
            else
            {
               foreach (Neuron i in Items)
                  iChildren.Insert(Index++, i);
            }
         }
      }
   }

   public class MoveClusterUndoItem : ClusterUndoItem
   {
      /// <summary>
      /// Gets or sets the item that was moved.
      /// </summary>
      /// <value>The item.</value>
      public Neuron Item { get; set; }

      /// <summary>
      /// Gets or sets the index that the item had.
      /// </summary>
      /// <value>The index.</value>
      public int Index { get; set; }

      public MoveClusterUndoItem()
      {
         Action = NotifyCollectionChangedAction.Move;
      }

      public override void Execute(UndoStore caller)
      {
         MoveClusterUndoItem iUndo = new MoveClusterUndoItem();
         iUndo.Cluster = Cluster;
         iUndo.Item = Item;
         WindowMain.UndoStore.AddCustomUndoItem(iUndo);
         using (ChildrenAccessor iChildren = Cluster.ChildrenW)
         {
            int iIndex = iChildren.IndexOf(Item.ID);
            iUndo.Index = iIndex;
            iChildren.RemoveAt(iIndex);
            iChildren.Insert(Index, Item);
         }
      }

      public override bool HasSameTarget(UndoItem toCompare)
      {
         if (base.HasSameTarget(toCompare) == true)
         {
            MoveClusterUndoItem iToCompare = toCompare as MoveClusterUndoItem;
            if (iToCompare != null)
               return iToCompare.Index == Index && iToCompare.Item == Item;
         }
         return false;
         
      }
   }

   public class ReplaceClusterUndoItem: ClusterUndoItem
   {
      public ReplaceClusterUndoItem()
      {
         Action = NotifyCollectionChangedAction.Replace;
      }

      /// <summary>
      /// Gets or sets the item that was replaced.
      /// </summary>
      /// <value>The item.</value>
      public Neuron Item { get; set; }

      /// <summary>
      /// Gets or sets the index that the item had.
      /// </summary>
      /// <value>The index.</value>
      public int Index { get; set; }

      public override void Execute(UndoStore caller)
      {
         ReplaceClusterUndoItem iUndo = new ReplaceClusterUndoItem();
         iUndo.Cluster = Cluster;
         iUndo.Index = Index;
         WindowMain.UndoStore.AddCustomUndoItem(iUndo);
         using (ChildrenAccessor iChildren = Cluster.ChildrenW)
         {
            iUndo.Item = Brain.Current[iChildren[Index]];
            iChildren.RemoveAt(Index);
            iChildren.Insert(Index, Item);
         }
      }

      public override bool HasSameTarget(UndoItem toCompare)
      {
         if (base.HasSameTarget(toCompare) == true)
         {
            MoveClusterUndoItem iToCompare = toCompare as MoveClusterUndoItem;
            if (iToCompare != null)
               return iToCompare.Index == Index && iToCompare.Item == Item;
         }
         return false;
      }
   }
}
