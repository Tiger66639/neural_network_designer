﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeuralNetworkDesigne.UndoSystem;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// This is a special <see cref="UndoItem"/> implementation for extra neuron data, like it's display title.
   /// </summary>
   /// <remarks>
   /// This is a special type of undo item, cause if the neuron got deleted and recreated, it has a different
   /// NeuronData object, which needs to be chaned during the undo operation. This is done by getting the
   /// new <see cref="NeuronData"/> object, instead of the one that created the undo data.
   /// </remarks>
   public class NeuronDataUndoItem: UndoItem
   {

      #region ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="NeuronDataUndoItem"/> class.
      /// </summary>
      public NeuronDataUndoItem(Neuron source, string prop, object value)
      {
         Value = value;
         Source = source;
         Property = prop;
      }

      #endregion

      #region prop

      /// <summary>
      /// Gets the previous value of the property.
      /// </summary>
      public object Value { get; private set; }


      /// <summary>
      /// Gets the name of the property that was changed.
      /// </summary>
      public string Property { get; private set; }

      /// <summary>
      /// Gets or sets the source as a neuron.  We use a neuron, cause the id of the neuron might change if it is deleted.
      /// This way, we always have the latest id.
      /// </summary>
      /// <value>The source.</value>
      public Neuron Source { get; private set; }

      #endregion

      #region Functions

      /// <summary>
      /// Executes the specified a caller.
      /// </summary>
      /// <param name="aCaller">A caller.</param>
      public override void Execute(UndoStore aCaller)
      {
         if (Brain.Current.IsValidID(Source.ID) == true)
         {
            NeuronData iItem = BrainData.Current.NeuronInfo[Source.ID];
            Type iType = iItem.GetType();
            iType.GetProperty(Property).SetValue(iItem, Value, null);
         }
         else
            throw new InvalidOperationException();
      }

      /// <summary>
      /// Checks if this UndoItem has the same target(s) as the undo item to compare.
      /// </summary>
      /// <remarks>
      /// Check if toCompare is also a PropertyUndoItem that points to the same object and the same property.
      /// This is used by the undo system to check for duplicate entries which don't need to be stored if they are within
      /// the <see cref="UndoStore.MaxSecBetweenUndo"/> limit.
      /// </remarks>
      /// <param name="toCompare">An undo item to compare this undo item with.</param>
      /// <returns>True, if they have the same target, otherwise false.</returns>
      public override bool HasSameTarget(UndoItem toCompare)
      {
         NeuronDataUndoItem iPrev = toCompare as NeuronDataUndoItem;
         if (iPrev != null)
            return iPrev.Source == Source && iPrev.Property == Property;
         return false;
      }

      /// <summary>
      /// Checks if this undo item contains data from the specified source object.
      /// </summary>
      /// <remarks>
      /// This function is used by the undo system to clean up undo data from items that are no longer available.  This can happen for
      /// undo data generated from user interface controls which are no longer valid (loaded).
      /// </remarks>
      /// <param name="source">The object to check for.</param>
      /// <returns>True if this object contains undo data for the specified object.</returns>
      public override bool StoresDataFrom(object source)
      {
         return Source == source;
      }

      #endregion
   }
}
