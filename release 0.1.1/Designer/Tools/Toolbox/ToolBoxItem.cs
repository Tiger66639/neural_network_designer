﻿using System;
using System.Xml.Serialization;
using NeuralNetworkDesigne.Data;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// A class that stores all the info for toolbox items.
   /// </summary>
   [XmlInclude(typeof(NeuronToolBoxItem))]
   [XmlInclude(typeof(TypeToolBoxItem))]
   [XmlInclude(typeof(InstructionToolBoxItem))]
   public abstract class ToolBoxItem : OwnedObject
   {



      #region Prop
      /// <summary>
      /// Gets/sets the category value of the <see cref="ToolBoxITem.Neuron"/>
      /// </summary>
      public abstract string Category { get; set; }

      /// <summary>
      /// Gets the title of the toolboxitem.
      /// </summary>
      public abstract string Title { get; }      

      #endregion




      /// <summary>
      /// Retrieves the <see cref="Neuron"/> for this toolbox item.
      /// </summary>
      /// <returns></returns>
      public abstract Neuron GetData();

      /// <summary>
      /// Retrieves the type
      /// </summary>
      /// <returns></returns>
      public abstract Type GetResultType();
   }
}
