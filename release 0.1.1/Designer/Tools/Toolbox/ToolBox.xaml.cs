﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Threading;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Interaction logic for ToolBox.xaml
   /// </summary>
   public partial class ToolBox : UserControl, IWeakEventListener
   {
      public ToolBox()
      {
         InitializeComponent();
         NeuronChangedEventManager.AddListener(Brain.Current, this);
      }

      ~ ToolBox()
      {
         NeuronChangedEventManager.RemoveListener(Brain.Current, this);
      }


      #region AllInstructionsOpen

      /// <summary>
      /// AllInstructionsOpen Dependency Property
      /// </summary>
      public static readonly DependencyProperty AllInstructionsOpenProperty =
          DependencyProperty.Register("AllInstructionsOpen", typeof(bool?), typeof(ToolBox),
              new FrameworkPropertyMetadata(true));

      /// <summary>
      /// Gets or sets the AllInstructionsOpen property.  This dependency property 
      /// indicates wether all groups on the instructions list are collapsed/opened or mixed.
      /// </summary>
      public bool? AllInstructionsOpen
      {
         get { return (bool?)GetValue(AllInstructionsOpenProperty); }
         set { SetValue(AllInstructionsOpenProperty, value); }
      }

      #endregion

      #region AllItemsOpen

      /// <summary>
      /// AllItemsOpen Dependency Property
      /// </summary>
      public static readonly DependencyProperty AllItemsOpenProperty =
          DependencyProperty.Register("AllItemsOpen", typeof(bool?), typeof(ToolBox),
              new FrameworkPropertyMetadata(true));

      /// <summary>
      /// Gets or sets the AllItemsOpen property.  This dependency property 
      /// indicates wether all the groups on the items list are collapsed/opened or mixed.
      /// </summary>
      public bool? AllItemsOpen
      {
         get { return (bool?)GetValue(AllItemsOpenProperty); }
         set { SetValue(AllItemsOpenProperty, value); }
      }

      #endregion




      /// <summary>
      /// When a neuron is removed from the brain, we need to check there is no toolbox item associated with it.  
      /// If so, this needs to be removed.
      /// </summary>
      /// Possible speed optimization here: use single loop for delete
      void Brain_NeuronChanged(object sender, NeuronChangedEventArgs e)
      {
         if (e.Action == BrainAction.Removed)
         {
            //we make a copy of the list of items to remove cause we can't remove items in a loop through the items.
            List<ToolBoxItem> iToRemove = (from i in BrainData.Current.ToolBoxItems
                                           where i is NeuronToolBoxItem && ((NeuronToolBoxItem)i).ItemID == e.OriginalSourceID   //we check ID's here cause this is saver for a delete.  This can otherwise give errors.
                                           select i).ToList();
            foreach (ToolBoxItem i in iToRemove)
               BrainData.Current.ToolBoxItems.Remove(i);
         }
      }

      #region IWeakEventListener Members

      public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(NeuronChangedEventManager))
         {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<object, NeuronChangedEventArgs>(Brain_NeuronChanged), sender, (NeuronChangedEventArgs)e);
            return true;
         }
         else
            return false;
      }

      #endregion

      /// <summary>
      /// Handles the Click event of the BtnReset control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void BtnReset_Click(object sender, RoutedEventArgs e)
      {
         BrainData.Current.ReloadToolboxItems();
      }

      private void MnuItemsCollapsItems_Click(object sender, RoutedEventArgs e)
      {
         AllItemsOpen = null;                                         //to make certain prev value is reset
         AllItemsOpen = false;
      }

      private void MnuItemsExpandItems_Click(object sender, RoutedEventArgs e)
      {
         AllItemsOpen = null;                                         //to make certain prev value is reset
         AllItemsOpen = true;
      }

      private void MnuInstructionsCollapsItems_Click(object sender, RoutedEventArgs e)
      {
         AllInstructionsOpen = null;                                    //to make certain prev value is reset
         AllInstructionsOpen = false;
      }

      private void MnuInstructionsExpandItems_Click(object sender, RoutedEventArgs e)
      {
         AllInstructionsOpen = null;                                    //to make certain prev value is reset
         AllInstructionsOpen = true;
      }



      
   }
}
