﻿
namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// This is a special type of <see cref="NeuronToolBoxItem"/> that is generated automatically
   /// for each <see cref="Instruction"/> in the brain.
   /// </summary>
   public class InstructionToolBoxItem: NeuronToolBoxItem
   {
      public override Neuron GetData()
      {
         NeuronCluster iCluster = new NeuronCluster();
         iCluster.Meaning = (ulong)PredefinedNeurons.ArgumentsList;
         Brain.Current.MakeTemp(iCluster);
         Expression iStatement;
         if (Item is ResultInstruction)
            iStatement = new ResultStatement((Instruction)Item, iCluster);
         else
            iStatement = new Statement((Instruction)Item, iCluster);             //this constructor also registers with the brain, otherwise can't create links.
         return iStatement;
      }
   }
}
