﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using DnD;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Provides drag capabalities to the toolbox.
   /// </summary>
   /// <remarks>
   /// When an item is dragged from the toolbox, the ID and neuron is only stored on the data object
   /// once the drag is finished.
   /// </remarks>
   public class ToolboxDragSourceAdvisor : DragSourceBase
   {

      public ToolboxDragSourceAdvisor()
      {
         SupportedFormat = Properties.Resources.ToolboxItemFormat;                                                        //this is not really used, simply informtive: this is our main data type.
      }

      public override void FinishDrag(UIElement draggedElt, DragDropEffects finalEffects)
      {
         //do nothing
      }

      public override bool IsDraggable(UIElement dragElt)
      {
         FrameworkElement iDragged = (FrameworkElement)dragElt;
         if (iDragged != null)
            return iDragged.DataContext is ToolBoxItem;
         return false;
      }

      /// <summary>
      /// we override cause we put the image to use + the neuron, or a ref to the toolbox item.
      /// </summary>
      public override DataObject GetDataObject(UIElement draggedElt)
      {
         FrameworkElement iDragged = (FrameworkElement)draggedElt;
         DataObject iObj= new DataObject();
         ToolBoxItem iToolboxItem;

         Debug.Assert(SourceUI is ListBoxItem);
         ListBox iSource = ItemsControl.ItemsControlFromItemContainer(SourceUI) as ListBox;
         if (iSource.SelectedItems.Count == 1)                                                                 //need to store single data info.
         {
            iToolboxItem = iDragged.DataContext as ToolBoxItem;
            Debug.Assert(iToolboxItem != null);

            iObj.SetData(Properties.Resources.UIElementFormat, iDragged);
            iObj.SetData(Properties.Resources.ToolboxItemFormat, iToolboxItem);
            iObj.SetData(Properties.Resources.DelayLoadFormat, this);                                          //we store a ref to this object in the data so we can delay create the Neuron when it is really consumed.
            iObj.SetData(Properties.Resources.DelayLoadResultType, this);
            iObj.SetData(Properties.Resources.NeuronIDFormat, (ulong)0);
         }
         return iObj;

      }

      /// <summary>
      /// If this drag source object supports delay loading of the data, descendents should reimplement this function.
      /// </summary>
      public override void DelayLoad(IDataObject data)
      {
         ToolBoxItem iToolboxItem = data.GetData(Properties.Resources.ToolboxItemFormat) as ToolBoxItem;
         Debug.Assert(iToolboxItem != null);
         data.SetData(Properties.Resources.NeuronIDFormat, iToolboxItem.GetData().ID);
      }

      public override Type GetDelayLoadResultType(IDataObject data)
      {
         ToolBoxItem iToolboxItem = data.GetData(Properties.Resources.ToolboxItemFormat) as ToolBoxItem;
         Debug.Assert(iToolboxItem != null);
         return iToolboxItem.GetResultType();
      }
   }
}
