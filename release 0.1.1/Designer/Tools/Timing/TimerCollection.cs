﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Threading;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// An <see cref="ObservableCollection"/> that keeps it's list synchronized with the brain
   /// for all <see cref="SinTimer"/> objects.
   /// </summary>
   public class TimerCollection : ObservableCollection<NeuralTimer>, IWeakEventListener
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="TimerCollection"/> class and automatically fills the list
      /// with all the known timers.
      /// </summary>
      public TimerCollection(): base()
      {
         NeuronChangedEventManager.AddListener(Brain.Current, this);
         var iTimers = from i in Brain.Current.Sins where i is TimerSin select (TimerSin)i;
         foreach (TimerSin i in iTimers)
            Add(new NeuralTimer(i));
      }

      /// <summary>
      /// Releases unmanaged resources and performs other cleanup operations before the
      /// <see cref="TimerCollection"/> is reclaimed by garbage collection.
      /// </summary>
      ~TimerCollection()
      {
         NeuronChangedEventManager.RemoveListener(Brain.Current, this);
      }

      #region IWeakEventListener Members

      /// <summary>
      /// Receives events from the centralized event manager.
      /// </summary>
      /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
      /// <param name="sender">Object that originated the event.</param>
      /// <param name="e">Event data.</param>
      /// <returns>
      /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
      /// </returns>
      public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(NeuronChangedEventManager))
         {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<NeuronChangedEventArgs>(NeuronChanged), (NeuronChangedEventArgs)e);
            return true;
         }
         else
            return false;
      }


      /// <summary>
      /// Handles the neuronchanged event handler.
      /// </summary>
      /// <remarks>
      /// Checks if a neuron changed, if so, and it is contained in our list, than we must update the item in the list
      /// </remarks>
      /// <param name="sender">The sender.</param>
      /// <param name="neuronChangedEventArgs">The <see cref="NeuralNetworkDesigne.HAB.NeuronChangedEventArgs"/> instance containing the event data.</param>
      private void NeuronChanged(NeuronChangedEventArgs e)
      {
         switch (e.Action)
         {
            case BrainAction.Created:
               if (e.OriginalSource is TimerSin)
               {
                  NeuralTimer iFound = (from i in this where i.Item == e.OriginalSource select i).FirstOrDefault();     //we check if it isn't already stored.
                  if (iFound == null)
                     this.Add(new NeuralTimer(e.OriginalSource as TimerSin));
               }
               break;
            case BrainAction.Changed:
               if (e.OriginalSource is TimerSin && !(e is NeuronPropChangedEventArgs) && !(e.NewValue is TimerSin))         //if we change the neuron type to something other than a timer, remove the timersin.
               {
                  NeuralTimer iFound = (from i in this where i.Item == e.OriginalSource select i).FirstOrDefault();
                  Remove(iFound);
               }
               break;
            case BrainAction.Removed:
               if (e.OriginalSource is TimerSin)
               {
                  NeuralTimer iFound = (from i in this where i.Item == e.OriginalSource select i).FirstOrDefault();
                  Remove(iFound);
               }
               break;
            default:
               break;
         }
      }

      #endregion
   }
}
