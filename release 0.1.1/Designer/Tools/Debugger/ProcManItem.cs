﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeuralNetworkDesigne.Data;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Base class for processors, stored by the <see cref="ProcessorManager"/>.  This allows for folders to be displayed.
   /// </summary>
   public abstract class ProcManItem : OwnedObject
   {
      public abstract void GetValuesFor(Variable toDisplay);

      public abstract void ClearValues();
   }
}
