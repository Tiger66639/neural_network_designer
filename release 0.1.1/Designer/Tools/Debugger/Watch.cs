﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeuralNetworkDesigne.Data;
using System.Xml.Serialization;
using NeuralNetworkDesigne.LogService;
using System.Collections.ObjectModel;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// A wrapper class for variables that can be observed by the debugger.
   /// </summary>
   public class Watch: ObservableObject, INeuronInfo, INeuronWrapper
   {

      #region Fields
      NeuronData fNeuronInfo;
      ulong fId;
      ObservableCollection<DebugNeuron> fValues = new ObservableCollection<DebugNeuron>();
      #endregion

      #region ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="Watch"/> class.
      /// </summary>
      public Watch()
      {
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="Watch"/> class.
      /// </summary>
      /// <param name="id">The id.</param>
      public Watch(ulong id)
      {
         ID = id;
      }

      #endregion

      #region ID

      /// <summary>
      /// Gets/sets the id of the variable to wrap.
      /// </summary>
      public ulong ID
      {
         get
         {
            return fId;
         }
         set
         {
            if (fId != value)
            {
               fId = value;
               if (BrainData.Current != null && BrainData.Current.NeuronInfo != null)
                  RegisterNeuron();
            }
         }
      }

      internal void RegisterNeuron()
      {
         fNeuronInfo = BrainData.Current.NeuronInfo[fId];
         if (fNeuronInfo != null && !(fNeuronInfo.Neuron is Variable))                           //if it's not a var, not allowed.    
         {
            fNeuronInfo = null;
            Log.LogError("Watch.ID", "A watch can only wrap variables!");
         }
      }

      #endregion

      /// <summary>
      /// Gets the values for the watch, based on the currently selected processor.
      /// </summary>
      /// <value>The values.</value>
      [XmlIgnore]
      public ObservableCollection<DebugNeuron> Values
      {
         get
         {
            return fValues;
         }
      }

      #region INeuronInfo Members

      /// <summary>
      /// Gets the extra info for the specified neuron.  Can be null.
      /// </summary>
      /// <value></value>
      [XmlIgnore]
      public NeuronData NeuronInfo
      {
         get { return fNeuronInfo; }
      }

      #endregion

      #region INeuronWrapper Members

      /// <summary>
      /// Gets the item.
      /// </summary>
      /// <value>The item.</value>
      [XmlIgnore]
      public Neuron Item
      {
         get { return Brain.Current[ID]; }
      }

      #endregion



      internal void LoadValuesFor(Processor proc)
      {
         fValues.Clear();
         Variable iVar = Brain.Current[ID] as Variable;
         if (iVar != null)
         {
            foreach (Neuron i in iVar.GetValue(proc))
               Values.Add(new DebugNeuron(i));
         }
      }
   }
}
