﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using NeuralNetworkDesigne.Data;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Contains all the data required for a single set of children of a <see cref="DebugNeuron"/>.  This is usually
   /// for the Input links, the output links or the children.
   /// </summary>
   public class DebugNeuronChildren:OwnedObject<DebugNeuron>
   {
      #region fields
      bool fIsLoaded = false;
      ObservableCollection<DebugRef> fChildren = new ObservableCollection<DebugRef>();
      string fName;
      #endregion

      #region ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="DebugNeuronChildren"/> class.
      /// </summary>
      /// <param name="owner">The owner.</param>
      public DebugNeuronChildren(DebugNeuron owner, string name)
      {
         Debug.Assert(owner != null);
         Owner = owner;
         Name = name;
      }

      #endregion


      #region Children

      /// <summary>
      /// Gets the lists of incomming links.
      /// </summary>
      /// <remarks>
      /// This is internally settable so we can quickly remove and clean out the lis if it is no longer needed.
      /// </remarks>
      public ObservableCollection<DebugRef> Children
      {
         get { return fChildren; }
      }

      #endregion

      #region IsLoaded

      /// <summary>
      /// Gets/sets if the content of the list is loaded.
      /// </summary>
      public bool IsLoaded
      {
         get
         {
            return fIsLoaded;
         }
         set
         {
            if (fIsLoaded != value)
            {
               if (value == true)                                                                           //need to clean up.
                  Owner.CreateChildrenFor(this);
               else
                  Children.Clear();
               Owner.UpdateMonitorLinks(this);
               fIsLoaded = value;
               OnPropertyChanged("IsLoaded");
            }
         }
      }

      #endregion

      #region HasChildren

      /// <summary>
      /// Gets if there are children.  Use this when <see cref="IsLoaded"/> to see if there are any children.
      /// </summary>
      /// <remarks>
      /// this property is indicated as changed whenever a link changes where <see cref="DebugNeuron.item"/> is involved.
      /// </remarks>
      public bool HasChildren
      {
         get
         {
            return Owner.HasChildren(this);
         }
      }

      #endregion

      
      #region Name

      /// <summary>
      /// Gets the name of the list to use for display purposes.
      /// </summary>
      public string Name
      {
         get
         {
            return fName;
         }
         private set
         {
            fName = value;
         }
      }

      #endregion

      /// <summary>
      /// Updates the HasChildren value.
      /// </summary>
      internal void UpdateHasChildren()
      {
         OnPropertyChanged("HasChildren");
      }
   }
}
