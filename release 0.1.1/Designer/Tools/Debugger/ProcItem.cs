﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeuralNetworkDesigne.Data;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Windows.Threading;
using System.Windows;
using System.Collections;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// A wrapper class for <see cref="Processor"/> objects, updated for WPF displaying.
   /// </summary>
   public class ProcItem : ProcManItem
   {

      #region Fields

      Processor fToWrap;
      ObservableCollection<DebugNeuron> fValues = new ObservableCollection<DebugNeuron>();
      bool fIsViewOpen;
      GridLength fStackWidth;
      GridLength fLinksToSolveWidth;
      string fName;
      ProcManFolder fFolder;
      #endregion

      #region ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="ProcItem"/> class.
      /// </summary>
      /// <param name="toWrap">The processor to wrap.</param>
      public ProcItem(Processor toWrap)
      {
         fToWrap = toWrap;
         if (toWrap != null)
         {
            toWrap.Finished += new EventHandler(Processor_Finished);
            DebugProcessor iDebugger = toWrap as DebugProcessor;
            if (iDebugger != null)
            {
               iDebugger.Paused += new EventHandler(ProcItem_Paused);
               iDebugger.SubProcessors.CollectionChanged += new NotifyCollectionChangedEventHandler(SubProcessors_CollectionChanged);     //don't need know weak event handler, both objects get out of scope at the same time.
            }
         }
         fValues.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(fValues_CollectionChanged);
      }


      #endregion

      #region Prop

      #region HasValues

      /// <summary>
      /// Gets wether there are currently values available for display.
      /// </summary>
      public bool HasValues
      {
         get { return fValues.Count > 0; }
      }

      #endregion

      #region Processor

      /// <summary>
      /// Gets the processor that is wrapped by this item.
      /// </summary>
      public Processor Processor
      {
         get { return fToWrap; }
      }

      #endregion

      #region Values

      /// <summary>
      /// Gets the list of values that should be displayed for this node.
      /// </summary>
      public ObservableCollection<DebugNeuron> Values
      {
         get { return fValues; }
      }

      #endregion

      #region IsSplit

      /// <summary>
      /// Gets a value indicating whether this instance is split for several other processor items.
      /// </summary>
      /// <value><c>true</c> if this instance is split; otherwise, <c>false</c>.</value>
      public virtual bool IsSplit
      {
         get { return false; }
      }

      #endregion

      #region IsViewOpen

      /// <summary>
      /// Gets/sets the wether the detailed view for this processor is currently opened or not.
      /// </summary>
      public bool IsViewOpen
      {
         get
         {
            return fIsViewOpen;
         }
         set
         {
            if (fIsViewOpen != value)
            {
               fIsViewOpen = value;
               OnPropertyChanged("IsViewOpen");
               if (value == true)
               {
                  WindowMain iMain = (WindowMain)App.Current.MainWindow;
                  iMain.AddItemToOpenDocuments(this);
               }
               else
                  BrainData.Current.OpenDocuments.Remove(this);
            }
         }
      }

      #endregion

      
      #region Name

      /// <summary>
      /// Gets/sets the name of the processor. This is filled in by default as a number, but can be changed to 
      /// track processors more easely.
      /// </summary>
      public string Name
      {
         get
         {
            return fName;
         }
         set
         {
            fName = value;
            OnPropertyChanged("Name");
         }
      }

      #endregion

      
      #region StackWidth

      /// <summary>
      /// Gets/sets the width of the stack data view.
      /// </summary>
      public GridLength StackWidth
      {
         get
         {
            return fStackWidth;
         }
         set
         {
            fStackWidth = value;
            OnPropertyChanged("StackWidth");
         }
      }

      #endregion

      
      #region LinksToSolve

      /// <summary>
      /// Gets/sets the width of the column that displays the links that will be solved by the processor.
      /// </summary>
      public GridLength LinksToSolveWidth
      {
         get
         {
            return fLinksToSolveWidth;
         }
         set
         {
            fLinksToSolveWidth = value;
            OnPropertyChanged("LinksToSolve");
         }
      }

      #endregion

      #region Folder

      /// <summary>
      /// Gets the folder that represents the root position of this processor.
      /// </summary>
      /// <remarks>
      /// When this processor has sub items, it also has a folder that contains this item and all the sub processors.
      /// this is done to better represent the split. When null, there are no sub processors loaded yet.
      /// </remarks>
      public ProcManFolder Folder
      {
         get { return fFolder; }
         internal set { fFolder = value; }
      }

      #endregion


      #endregion

      #region Functions
      /// <summary>
      /// Assigns the value(s) stored in the specified variable (for this processor) to <see cref="ProcItem.Values"/>
      /// </summary>
      /// <remarks>
      /// this is thread safe.
      /// </remarks>
      /// <param name="toDisplay">To display.</param>
      public override void GetValuesFor(Variable toDisplay)
      {
         List<Neuron> iList = toDisplay.GetValue(fToWrap).ToList();
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<List<Neuron>>(DisplayValues), iList);
      }

      /// <summary>
      /// Displays the values.
      /// </summary>
      /// <param name="list">The list.</param>
      void DisplayValues(List<Neuron> list)
      {
         foreach (Neuron i in list)
            Values.Add(new DebugNeuron(i));
      }

      /// <summary>
      /// Handles the CollectionChanged event of the fValues control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
      void fValues_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         OnPropertyChanged("HasValues");
      }

      /// <summary>
      /// Handles the Finished event of all the Processors created by the processor manager.
      /// </summary>
      /// <remarks>
      /// Makes certain that processor objects are removed from the list.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
      void Processor_Finished(object sender, EventArgs e)
      {
         //this function will be called from all kinds of threads, so make certain that we udpate the UI from the UI thread.
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(ProcFinished));
      }

      void ProcFinished()
      {
         RemoveItemFromOwner(this);
         fToWrap.Finished -= new EventHandler(Processor_Finished);
         if (fToWrap is DebugProcessor)
            ((DebugProcessor)fToWrap).Paused -= new EventHandler(ProcItem_Paused);
      }

      /// <summary>
      /// Handles the Paused event of the ProcItem control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
      void ProcItem_Paused(object sender, EventArgs e)
      {
         if (ProcessorManager.Current.SelectedWatchIndex > -1)
         {
            Variable iVar = ProcessorManager.Current.Watches[ProcessorManager.Current.SelectedWatchIndex].NeuronInfo.Neuron as Variable;
            GetValuesFor(iVar);
         }
      } 
      #endregion

      #region CollectionChanged

      /// <summary>
      /// Handles the CollectionChanged event of the SubProcessors control.
      /// </summary>
      /// <remarks>
      /// Do asyn cause this event can be raised from different threads.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
      void SubProcessors_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<NotifyCollectionChangedEventArgs>(InternalCollectionChanged), e);
      }

      /// <summary>
      /// For add:- when 1st sub: create folder and replace this with folder.  Insert this and new child into folder. 
      ///         - when not first: add to folder.
      /// (remove handled by Finished event handler.
      /// For remove: - when still left: remove from folder
      ///             - when only 1 left: remove folder and replace with item left (doesn't have to be this).
      ///             - when this is removed, only remove from folder, but don't remove reference on folder yet, so that the item keeps getting events.
      /// For clean: remove folder and replace with this.
      /// </summary>
      /// <param name="e">The <see cref="NeuralNetworkDesigne.HAB.NeuronChangedEventArgs"/> instance containing the event data.</param>
      void InternalCollectionChanged(NotifyCollectionChangedEventArgs e)
      {
         DebugProcessor iToWrap = (DebugProcessor)fToWrap;                                         //we know this is true
         if (e.Action == NotifyCollectionChangedAction.Add)
         {
            if (Folder == null)
               LoadRootFolder(e.NewItems);
            else
               foreach (Processor i in e.NewItems)
                  Folder.Processors.Add(new ProcItem(i));
         }
      }

      private void RemoveItemFromOwner(ProcManItem toRemove)
      {
         if (toRemove != null)
         {
            ProcManFolder iFolder = Owner as ProcManFolder;
            if (iFolder != null)
            {
               iFolder.Processors.Remove(toRemove);
               if (iFolder.Processors.Count == 1)
               {
                  IProcessorsOwner iFolderOwner = (IProcessorsOwner)iFolder.Owner;
                  int iIndex = iFolderOwner.Processors.IndexOf(iFolder);
                  ProcManItem iToMove = iFolder.Processors[0];                                   //need to remove before we can add to other list, becaus of owner.
                  iFolder.Processors.RemoveAt(0);                                                //this doesn't trigger a complete clear.
                  iFolderOwner.Processors[iIndex] = iToMove;
               }
            }
            else
            {
               IProcessorsOwner iOwner = (IProcessorsOwner)Owner;
               if (iOwner != null)                                                                 //this function also gets called when the processor is finishted, to remove the original processor.
                  iOwner.Processors.Remove(toRemove);
               else
                  throw new InvalidOperationException();
            }
         }
      }

      /// <summary>
      /// Creates a new <see cref="ProcManFolder"/> item to represent all the possible paths of a split.
      /// </summary>
      private void LoadRootFolder(IList newItems)
      {
         if (Folder == null)
         {
            Folder = new ProcManFolder(this);
            IProcessorsOwner iOwner = (IProcessorsOwner)Owner;
            int iIndex = iOwner.Processors.IndexOf(this);
            iOwner.Processors[iIndex] = Folder;
            Folder.Processors.Add(this);
            foreach (Processor i in newItems)
               Folder.Processors.Add(new ProcItem(i));
         }
         else
            throw new InvalidOperationException();
      }

      #endregion

      public override void ClearValues()
      {
         Values.Clear();
      }
   }
}
