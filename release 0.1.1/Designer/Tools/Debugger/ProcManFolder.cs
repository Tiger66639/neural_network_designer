﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeuralNetworkDesigne.Data;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// A folder that containa processor manager items.
   /// </summary>
   public class ProcManFolder : ProcManItem, IProcessorsOwner
   {
      #region fields
      ObservedCollection<ProcManItem> fProcessors;
      ProcItem fSplitStart;
      bool fIsExpanded = true; 

      #endregion

      #region ctor
      /// <summary>
      /// Initializes a new instance of the <see cref="ProcManFolder"/> class.
      /// </summary>
      /// <param name="splitStart">The split start.</param>
      public ProcManFolder(ProcItem splitStart)
      {
         fProcessors = new ObservedCollection<ProcManItem>(this);
         SplitStart = splitStart;
      } 
      #endregion

      #region prop
      #region Processors

      /// <summary>
      /// Gets the list of processors currently running.  These are 'sub items'.
      /// </summary>
      public ObservedCollection<ProcManItem> Processors
      {
         get { return fProcessors; }
      }

      #endregion


      #region SplitStart

      /// <summary>
      /// Gets the processor wrapper from which the split started.  This is required
      /// because it keeps track of all sub processors.  We need the ref to exist for as long
      /// as there are sub processors (the creator processor can dy out, but we still need it's lists' cause
      /// they are warned when changed.
      /// </summary>
      public ProcItem SplitStart
      {
         get { return fSplitStart; }
         internal set { fSplitStart = value; }
      }

      #endregion


      #region IsExpanded

      /// <summary>
      /// Gets/sets the wether the folder is expanded or not.
      /// </summary>
      public bool IsExpanded
      {
         get
         {
            return fIsExpanded;
         }
         set
         {
            fIsExpanded = value;
         }
      }

      #endregion 
      #endregion

      public override void GetValuesFor(Variable toDisplay)
      {
         foreach (ProcManItem i in Processors)
            i.GetValuesFor(toDisplay);
      }

      public override void ClearValues()
      {
         foreach (ProcManItem i in Processors)
            i.ClearValues();
      }
   }
}
