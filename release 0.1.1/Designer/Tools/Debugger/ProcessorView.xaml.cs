﻿using System.Windows.Controls;
using System.Windows;
using System.Windows.Input;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Displays the data for 1 <see cref="DebugProcessor"/>.
   /// </summary>
   public partial class ProcessorView : UserControl
   {
      public ProcessorView()
      {
         InitializeComponent();
      }

      /// <summary>
      /// Handles the Loaded event of the UserControl control.
      /// </summary>
      /// <remarks>
      /// need to make the proc that we wrap, the selected proc, so that the commands work properly.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
      {
         ProcItem iProc = (ProcItem)DataContext;
         ProcessorManager.Current.SelectedProcessor = iProc;
      }


      private void OpenDebuggerInEditor_CanExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
      {
         FrameworkElement iFocused = Keyboard.FocusedElement as FrameworkElement;
         e.CanExecute = e.Parameter is Neuron || (iFocused != null && iFocused.DataContext is INeuronWrapper);
      }

      private void OpenDebuggerInEditor_Executed(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
      { 
         ProcItem iProc = (ProcItem)DataContext;
         LogDebugData iData = DebugProcessor.CreateLogDebugData(iProc.Processor);
         iData.Executed = e.Parameter as Neuron;
         if (iData.Executed == null)
         {
            FrameworkElement iFocused = Keyboard.FocusedElement as FrameworkElement;
            iData.Executed = ((INeuronWrapper)iFocused.DataContext).Item;
         }
         WindowMain.Current.DisplayDebugData(iData);
      }
   }
}
