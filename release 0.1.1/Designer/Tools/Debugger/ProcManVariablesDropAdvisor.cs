﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DnD;
using System.Windows;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// The drop advisor for the <see cref="ProcessorManager"/>'s <see cref="ProcessorManager.Watches"/> list.
   /// </summary>
   public class ProcManVariablesDropAdvisor : DropTargetBase
   {
      /// <summary>
      /// Called when a drop is performed.
      /// </summary>
      /// <param name="obj"></param>
      /// <param name="dropPoint"></param>
      /// <remarks>
      /// modified from original, changed IDataobject to DragEventArgs so that we have more
      /// info about the drop, like on who we are dropping.
      /// </remarks>
      public override void OnDropCompleted(DragEventArgs obj, Point dropPoint)
      {
         if ((obj.AllowedEffects & DragDropEffects.Copy) == DragDropEffects.Copy)
         {
            ulong iId = (ulong)obj.Data.GetData(Properties.Resources.NeuronIDFormat);

            var iFound = (from i in ProcessorManager.Current.Watches where i.ID == iId select i).FirstOrDefault();
            if(iFound == null)
               ProcessorManager.Current.Watches.Add(new Watch(iId));
         }
      }


      /// <summary>
      /// Gets the effect that should be used for the drop operation.
      /// </summary>
      /// <param name="e">The drag event arguments.</param>
      /// <returns>The prefered effect to use.</returns>
      /// <remarks>
      /// By default, this function checks the control key, wen pressed, a copy is done, otherwise a move.
      /// </remarks>
      public override DragDropEffects GetEffect(DragEventArgs e)
      {
         ulong iId = (ulong)e.Data.GetData(Properties.Resources.NeuronIDFormat);
         Neuron iFound;
         if (Brain.Current.TryFindNeuron(iId, out iFound) == true && iFound is Variable)
            return DragDropEffects.Copy;
         return DragDropEffects.None;
      }

      public override bool IsValidDataObject(IDataObject obj)
      {
         return obj.GetDataPresent(Properties.Resources.NeuronIDFormat);
      }
   }
}
