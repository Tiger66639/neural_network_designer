﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows.Threading;
using System.Collections.Specialized;
using System.Linq;
using NeuralNetworkDesigne.WPFLog;
using NeuralNetworkDesigne.LogService;

namespace NeuralNetworkDesigne.HAB.Designer
{
   

   /// <summary>
   /// The different types of debug mode available.
   /// </summary>
   public enum DebugMode
   {
      /// <summary>
      /// No debuging is done, no events are send about the state of the processor.
      /// </summary>
      Off,
      /// <summary>
      /// All events are raised and the debugger stops execution at every step.
      /// </summary>
      Normal,
      /// <summary>
      /// All events are raised and the debugger pauses for a short while before executing the next step.
      /// </summary>
      SlowMotion
   }

   /// <summary>
   /// A <see cref="Processor"/> that provides debug information.
   /// </summary> 
   /// <remarks>
   /// Each processor provides its own blocking mechanism.  So debugging is always at the level of
   /// 1 processor, no jumps between threads.  This allows you to debug a single process step after
   /// step much easier without jumping from one process to the other.
   /// </remarks>
   public class DebugProcessor: Processor, INotifyPropertyChanged
   {
      #region Fields

      bool fPauseForException = false;                                                                //used to let a debugProcessor know that an exception was logged for which we need to break execution.
      bool fPaused = false;                                                                           //a manual switch which stores that the user pressed the pause button. This is used to temporarely block the timer routine that allows the slow motion video play.
      DebugMode fDebugMode;                                                                           //value retrieved on ctor, fixed for entire process run, managed by application settings.
      EventWaitHandle fBreakSwitch;                                                                      //used for step processing, so we can wait for the ui thread to signal a continue..
      bool fBreakNextStep = false;                                                                    //switch used for a step to next expression.
      static Timer fTimer;                                                                            //used for auto play debug mode.
      ObservableCollection<DebugProcessor> fSubProcessors = new ObservableCollection<DebugProcessor>();
      ObservableCollection<DebugNeuron> fStack = new ObservableCollection<DebugNeuron>();
      DebugNeuron fExecutingNeuron;
      ObservableCollection<ExecutionFrame> fExecutionFrames = new ObservableCollection<ExecutionFrame>();
      ExecutionFrame fSelectedFrame;
      bool fIsPaused;
      bool fIsRunning = true;
      ObservableCollection<DebugNeuron> fMeaningsToExec = new ObservableCollection<DebugNeuron>();
      DebugNeuron fSelectedMeaning;
      Expression fNextStatement;
      #endregion

      #region ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="DebugProcessor"/> class.
      /// </summary>
      public DebugProcessor()
      {
         fDebugMode = BrainData.Current.Debugmode;
         if (fDebugMode != DebugMode.Off)
         {
            if (fDebugMode == DebugMode.SlowMotion)
            {
               fBreakSwitch = new AutoResetEvent(true);                                               //for a slow motion, we use an autoreset event, so we don't have to reset after each frame.
               if (fTimer == null)
               {
                  int iMSec = (int)ProcessorManager.Current.PlaySpeed.TotalMilliseconds;
                  fTimer = new Timer(new TimerCallback(OnTimer), null, iMSec, iMSec);
               }
            }else
               fBreakSwitch = new ManualResetEvent(true);                                             //we use manual in all other sits.
         }
         fStack.CollectionChanged += new NotifyCollectionChangedEventHandler(fStack_CollectionChanged);
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="DebugProcessor"/> class.
      /// </summary>
      /// <param name="mode">The initial debug mode to use.</param>
      public DebugProcessor(DebugMode mode)
      {
         fDebugMode = mode;
         if (fDebugMode != DebugMode.Off)
         {
            if (fDebugMode == DebugMode.SlowMotion)
            {
               fBreakSwitch = new AutoResetEvent(true);                                               //for a slow motion, we use an autoreset event, so we don't have to reset after each frame.
               if (fTimer == null)
               {
                  int iMSec = (int)ProcessorManager.Current.PlaySpeed.TotalMilliseconds;
                  fTimer = new Timer(new TimerCallback(OnTimer), null, iMSec, iMSec);
               }
            }
            else
               fBreakSwitch = new ManualResetEvent(true);                                             //we use manual in all other sits.
         }
         fStack.CollectionChanged += new NotifyCollectionChangedEventHandler(fStack_CollectionChanged);
      }

      /// <summary>
      /// Initializes the <see cref="DebugProcessor"/> class so that everything is set up for debugging.
      /// </summary>
      static public void Init()
      {

         WPFLog.WPFLog.Default.PreviewWriteToLog += new LogItemEventHandler(Log_PreviewWriteToLog);         //we use this event cause this is triggered in the same thread as where the log happened, which we need to check if there is a processor present in the thread.
      }

      #endregion

      #region Events

      /// <summary>
      /// Occurs when the processor is paused because of a breakpoint or new step in a slowmotion.
      /// </summary>
      public event EventHandler Paused;

      #endregion

      #region Prop

      #region DebugMode

      /// <summary>
      /// Gets the current debugging mode. This can be used to find out if this is a slowmotion timer.
      /// </summary>
      public DebugMode DebugMode
      {
         get { return fDebugMode; }
         internal set { fDebugMode = value; }
      }

      #endregion

      

      #region ExecutionFrames

      /// <summary>
      /// Gets the list of execution frames for this processor.
      /// </summary>
      /// <remarks>
      /// An <see cref="ExecutionFrame"/> contains all the info of 1
      /// function that is called.  Since a function can call another function, we must keep
      /// a stack of all currently called functions, hence this stack.
      /// </remarks>
      public ObservableCollection<ExecutionFrame> ExecutionFrames
      {
         get { return fExecutionFrames; }
      }

      #endregion

      #region SelectedFrame

      /// <summary>
      /// Gets/sets the currently selected execution frame.
      /// </summary>
      public ExecutionFrame SelectedFrame
      {
         get
         {
            return fSelectedFrame;
         }
         internal set
         {
            if (value != fSelectedFrame)
            {
               if (fSelectedFrame != null)
                  fSelectedFrame.IsSelected = false;
               fSelectedFrame = value;
               if (fSelectedFrame != null)
                  fSelectedFrame.IsSelected = true;
               //App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<string>(OnPropertyChanged), "SelectedFrame");
               OnPropertyChanged("SelectedFrame");                                          //this should be thread save cause property changed works accross threads.
            }
         }
      }

      #endregion

      #region StackDisplay

      /// <summary>
      /// Gets the list of stack items currently used by the processor.
      /// </summary>
      /// <remarks>
      /// This is an observable collection so that the GUI can properly work with it. The actual stack is
      /// managed from another thread.
      /// </remarks>
      public ObservableCollection<DebugNeuron> StackDisplay
      {
         get { return fStack; }
      }

      #endregion

      #region ExecutingNeuron

      /// <summary>
      /// Gets the index on the stack of the neuron currently being executed.
      /// </summary>
      public DebugNeuron ExecutingNeuron
      {
         get
         {
            return fExecutingNeuron;
         }
         internal set
         {
            if (value != fExecutingNeuron)
            {
               if (fExecutingNeuron != null)
                  fExecutingNeuron.IsSelected = false;
               fExecutingNeuron = value;
               if (fExecutingNeuron != null)
                  fExecutingNeuron.IsSelected = true;
               //App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<string>(OnPropertyChanged), "ExecutingNeuron");
               OnPropertyChanged("ExecutingNeuron");                                          //this should be thread save cause property changed works accross threads.
            }
         }
      }

      #endregion

      #region StackSize

      /// <summary>
      /// Gets/sets the number of items currently on the stack.
      /// </summary>
      /// <remarks>
      /// This is used to display in WPF.
      /// </remarks>
      public int StackSize
      {
         get
         {
            return fStack.Count;
         }
      }

      #endregion

      #region IsPaused

      /// <summary>
      /// Gets/sets if the debugger is curently waiting for the user to continue the execution.
      /// </summary>
      /// <remarks>
      /// Use this prop to pause the processor.
      /// </remarks>
      public bool IsPaused
      {
         get
         {
            return fIsPaused;
         }
         set
         {
            if (value != fIsPaused)
            {
               if (value == true)
                  DebugPause();
               else
                  DebugContinue();
            }
         }
      }

      /// <summary>
      /// Sets the actual paused value to the field + raises events.  This function is called from seperate threads.
      /// This is the actual setter so that we have a real view of the value, not what we wanted.
      /// </summary>
      /// <param name="value">if set to <c>true</c> [value].</param>
      void SetPausedValue(bool value)
      {
         fIsPaused = value;
         if (value == true)
         {
            ProcItem iSelected = ProcessorManager.Current.SelectedProcessor;
            if (iSelected != null && iSelected.Processor == this)                      //if this is the currently selected processor, update the state of the visualisations for the selected proc (this will recalcluate the variable values displayed in the editor.
               App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(ProcessorManager.Current.UpdateProcessorStats));        //need async, this function gets called from different threads.
            OnPaused();
         }
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<string>(OnPropertyChanged), "IsPaused");
      }

      #endregion

      #region IsRunning

      /// <summary>
      /// Gets/sets the the processor is running or not.
      /// </summary>
      public bool IsRunning
      {
         get
         {
            return fIsRunning;
         }
         set
         {
            fIsRunning = value;
            OnPropertyChanged("IsRunning");
         }
      }

      #endregion

      #region SubProcessors

      /// <summary>
      /// Gets the list of sub processors of this processor.
      /// </summary>
      public ObservableCollection<DebugProcessor> SubProcessors
      {
         get { return fSubProcessors; }
      }

      #endregion

      #region MeaningsToExec

      /// <summary>
      /// Gets the list of links that are being executed (the links of the currently executing neuron).
      /// </summary>
      public ObservableCollection<DebugNeuron> MeaningsToExec
      {
         get { return fMeaningsToExec; }
      }

      #endregion

      #region SelectedMeaning

      /// <summary>
      /// Gets/sets the neuron - meaning that is currently being executed.
      /// </summary>
      public DebugNeuron SelectedMeaning
      {
         get
         {
            return fSelectedMeaning;
         }
         set
         {
            if (value != fSelectedMeaning)
            {
               if (fSelectedMeaning != null)
                  fSelectedMeaning.IsSelected = false;
               fSelectedMeaning = value;
               if (fSelectedMeaning != null)
                  fSelectedMeaning.IsSelected = true;
               OnPropertyChanged("SelectedMeaning");
            }
         }
      }

      #endregion

      
      #region NextStatement

      /// <summary>
      /// Gets/sets the statement that will be executed next.
      /// </summary>
      public Expression NextStatement
      {
         get
         {
            return fNextStatement;
         }
         set
         {
            if (value != fNextStatement)
            {
               ProcItem iSelectedProc = ProcessorManager.Current.SelectedProcessor;
               if (iSelectedProc != null && iSelectedProc.Processor == this && fNextStatement != null)
                  BrainData.Current.NeuronInfo[fNextStatement.ID].IsNextStatement = false;
               fNextStatement = value;
               if (iSelectedProc != null && iSelectedProc.Processor == this && fNextStatement != null)
                  BrainData.Current.NeuronInfo[fNextStatement.ID].IsNextStatement = true;
               OnPropertyChanged("NextStatement");
            }
         }
      }

      #endregion

      #endregion

      #region functions

      /// <summary>
      /// Handles the CollectionChanged event of the fStack control.
      /// </summary>
      /// <remarks>
      /// When the collection changes, we need to raise the event to indicate that the stacksize is also changed.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
      void fStack_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         OnPropertyChanged("StackSize");
      }

      /// <summary>
      /// Called when the processor is paused. Triggers the event.
      /// </summary>
      protected virtual void OnPaused()
      {
         if (Paused != null)
            Paused(this, EventArgs.Empty);
      }

      /// <summary>
      /// signals the debug processor that the next execution step can be performed.
      /// </summary>
      /// <remarks>
      /// This simply sets an auto event.
      /// </remarks>
      public void DebugContinue()
      {
         if (fBreakSwitch != null)
         {
            fPaused = false; 
            fBreakSwitch.Set();
         }
      }

      public void DebugPause()
      {
         if (fBreakSwitch != null)
         {
            fPaused = true;
            fBreakSwitch.Reset();
         }
      }

      public void DebugStepNext()
      {
         if (fBreakSwitch != null)
         {
            fPaused = false; 
            fBreakNextStep = true;
            fBreakSwitch.Set();
         }
      }

      void OnTimer(object timer)
      {
         if (fPaused == false)
            fBreakSwitch.Set();
      }

      /// <summary>
      /// Handles the PreviewWriteToLog event of the Log control.
      /// </summary>
      /// <remarks>
      /// This checks if there is a processor defined in the thread that changes the list.  If so, the log item is comming from the
      /// execution engine, so attach a tag to it that allows us to find the neuron again that triggered the log item.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="NeuralNetworkDesigne.Log.LogItemEventArgs"/> instance containing the event data.</param>
      /// <returns>True, indicating that the log should always happen.</returns>
      static bool Log_PreviewWriteToLog(object sender, LogItemEventArgs e)
      {
         if (CurrentProcessor != null)
         {
            LogDebugData iLogData = CreateLogDebugData(CurrentProcessor);
            e.Item.Tag = iLogData;
            DebugProcessor iCur = CurrentProcessor as DebugProcessor;
            if (iCur != null && BrainData.Current.BreakOnException == true)
            {
               iCur.fPauseForException = true;
               iCur.DebugPause();
               iCur.SetPausedValue(true);
               App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<LogItem>(DisplayDebugData), e.Item);
            }
         }
         return true;
      }

      /// <summary>
      /// Creates a log-debug-data record for the current state of the processor.
      /// </summary>
      /// <remarks>
      /// Needs to be static so that this can be used for normal processors, and not just debug processors.
      /// </remarks>
      /// <returns>A log object</returns>
      internal static LogDebugData CreateLogDebugData(Processor proc)
      {
         LogDebugData iLogData = new LogDebugData();
         iLogData.Executed = proc.CurrentExpression;
         iLogData.ExecListType = proc.CurrentExecListType;
         iLogData.Solved = proc.NeuronToSolve;
         iLogData.Meaning = proc.CurrentMeaning;
         iLogData.ExecutionSource = proc.CurrentExecSource;
         return iLogData;
      }

      /// <summary>
      /// Displays the debug data. We can't call WindowMain.Current from another thread, so therefor this helper function.
      /// </summary>
      /// <param name="data">The data.</param>
      static void DisplayDebugData(LogItem data)
      {
         WindowMain.Current.DisplayDebugData((LogDebugData)data.Tag);
         System.Windows.MessageBox.Show("Processing has been halted because of the following error: " + data.Text);
      }

      #endregion

      #region Overwrites

      /// <summary>
      /// Returns the evalulation of the condition.
      /// </summary>
      /// <param name="compareTo">The posssible list of neurons that must be found through the condition.</param>
      /// <param name="expression"></param>
      /// <returns></returns>
      /// <remarks>
      /// If there is a <b>compareTo</b> value specified, a case statement is presumed, and all the
      /// neurons in the compareTo must also be in Condition (after solving a possible expression) and no more.
      /// If compareTo is null, a boolean expression is presumed and it's result is returned or an error logged
      /// and false returned.
      /// When the condition is empty, true is always presumed.
      /// </remarks>
      protected override bool EvaluateCondition(IEnumerable<Neuron> compareTo, ConditionalExpression expression, int index)
      {
         try
         {
            HandleBreakPoint(expression, index);
            return base.EvaluateCondition(compareTo, expression, index);
         }
         finally
         {
            fBreakNextStep = false;                                                                                                 //reset after item was executed, this way, a split can copy the fBreakNextStep to the subprocessors (otherwise it doesn't know about it cause it has been reset).
         }
      }

      public override void PushFrame(CallFrame frame)
      {
         if (SelectedFrame != null)
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<int>(SelectedFrame.UpdateNext), SelectedFrame.Frame.NextExp);     //needs to be async cause NextStatement changes NeuronData.IsNextStatement, which is dependent from ProcessorManager.SelectedProcessor, so needs to be changed in user thread for correct syncing
         ExecutionFrame iNew = new ExecutionFrame(frame);
         SelectedFrame = iNew;                                                               //needs to be set in this thread, otherwise we can't set the current pos properly
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<ExecutionFrame>(InternalPushFrame), iNew);
         base.PushFrame(frame);
      }

      public override CallFrame PopFrame()
      {
         if (ExecutionFrames.Count >= 2)
         {
            SelectedFrame = ExecutionFrames[ExecutionFrames.Count - 2];                         //needs to be set in this thread, otherwise we can't set the current pos properly
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<int>(SelectedFrame.UpdateNext), SelectedFrame.Frame.NextExp);     //needs to be async cause NextStatement changes NeuronData.IsNextStatement, which is dependent from ProcessorManager.SelectedProcessor, so needs to be changed in user thread for correct syncing
         }
         else
            SelectedFrame = null;
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(InternalPopFrame));
         return base.PopFrame();
      }

      public override void ClearFrames()
      {
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(ExecutionFrames.Clear));
         SelectedFrame = null;
         base.ClearFrames();
      }

      /// <summary>
      /// Need to update the execution frame stack when a process starts.
      /// </summary>
      /// <param name="toExec"></param>
      /// <param name="toProcess"></param>
      /// <param name="listType"></param>
      void InternalPushFrame(ExecutionFrame toAdd)
      {
         ExecutionFrames.Add(toAdd);  
      }

      void InternalPopFrame()
      {
         ExecutionFrames.RemoveAt(ExecutionFrames.Count - 1);
      }

      /// <summary>
      /// If there is a wait event, it means we need to block execution untill we get a signal to continue.
      /// </summary>
      /// <param name="toProcess"></param>
      protected override void Process(Expression toProcess)
      {
         try
         {
            int iPrevNext = NextExp;
            HandleBreakPoint(toProcess, NextExp);
            base.Process(toProcess);
            if (fPauseForException == true)
            {
               fPauseForException = false;
               HandleBreakPoint(toProcess, iPrevNext);
            }
         }
         finally
         {
            fBreakNextStep = false;                                                                                                 //reset after item was executed, this way, a split can copy the fBreakNextStep to the subprocessors (otherwise it doesn't know about it cause it has been reset).
         }
      }

      private void HandleBreakPoint(Expression toProcess, int index)
      {
         UpdateNextStatement(toProcess);     
         if (fBreakSwitch != null)
         {
            lock (BrainData.Current.BreakPoints)                                                                                 //we need a lock cause an add/remove can be done from another thread.
            {
               if (BrainData.Current.BreakPoints.Contains(toProcess) == true)                                                    //if this is a breakpoint, we always need to pause processing.
               {
                  BrainData.Current.BreakPoints.OnBreakPointReached(toProcess, this);
                  fBreakSwitch.Reset();
               }
            }
            SetPausedValue(true);                                                                                                 //just before we pause, let the ui know.
            fBreakSwitch.WaitOne();
            SetPausedValue(false);
         }
         if (fBreakNextStep == true && fBreakSwitch != null)
            fBreakSwitch.Reset();                                                                                                 //don't reset BreakNextStep here, this is done after the code has been processed, so that the processor knows about the breakOnNextStep when the statement causes a split.
      }

      /// <summary>
      /// Sets the next statement.
      /// </summary>
      /// <param name="next">The next.</param>
      void UpdateNextStatement(Expression next)
      {
         if (SelectedFrame != null)
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<int>(SelectedFrame.UpdateNext), SelectedFrame.Frame.NextExp);     //needs to be async cause NextStatement changes NeuronData.IsNextStatement, which is dependent from ProcessorManager.SelectedProcessor, so needs to be changed in user thread for correct syncing
         NextStatement = next;
      }

      /// <summary>
      /// Call this method if you want to remove something from the internal stack.
      /// for internal use only.
      /// </summary>
      /// <returns></returns>
      /// <remarks>
      /// This method only removes the item from the stack.  It is provided so that
      /// inheriters can do something more during a stack change, like let observers know.
      /// </remarks>
      public override Neuron Pop()
      {
         if (fDebugMode != DebugMode.Off)
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(InternalPopNeuron));
         return base.Pop();
      }

      /// <summary>
      /// removes the last item from the observable colection. This is so it can be done from the UI thread.
      /// </summary>
      void InternalPopNeuron()
      {
         if (fStack.Count > 0)
            fStack.RemoveAt(0);
      }

      /// <summary>
      /// Pushes the specified neuron on it's internal stack.
      /// </summary>
      /// <param name="toPush">The neuron to add.</param>
      public override void Push(Neuron toPush)
      {
         if (fDebugMode != DebugMode.Off)
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<Neuron>(InternalPushNeuron), toPush);
         base.Push(toPush);
      }

      void InternalPushNeuron(Neuron toPush)
      {
         if (fStack.Count > 0)
            fStack.Insert(0, new DebugNeuron(toPush));
         else
            fStack.Add(new DebugNeuron(toPush));
      }

      /// <summary>
      /// Returns an array of the specified size, filled with processors.
      /// </summary>
      /// <param name="count">The nr of processors to create.</param>
      /// <returns>An array of newly created processors (not initialized).</returns>
      /// <remarks>
      /// Descendents should reimplement this so they can give another type (for debugging).
      /// </remarks>
      public override Processor[] CreateProcessors(int count)
      {
         Processor[] iRes = new Processor[count];
         while (count > 0)
         {
            DebugProcessor iSub = new DebugProcessor(DebugMode);
            if (fBreakNextStep == true && iSub.fBreakSwitch != null)
               iSub.DebugPause();
            iRes[count - 1] = iSub;                                                 //-1 cause the index is 1 less than the size.
            fSubProcessors.Add(iSub);                                               //a processor is always created for subprocessors, so we can store them here.
            count--;
         }
         return iRes;
      }


      /// <summary>
      /// Tries to solve the specified neuron.
      /// </summary>
      /// <param name="toSolve"></param>
      /// <remarks>
      /// 	<para>
      /// Solving is done by walking down every 'To' link in the neuron and pushing this to reference on the stack after which it calls
      /// the <see cref="Link.Meaning"/>'s <see cref="Neuron.Actions"/> list.
      /// </para>
      /// 	<para>
      /// this is an internal function, cause the output instruction needs to be able to let a neuron be solved
      /// sequential (<see cref="Processor.Solve"/> is async and solves the intire stack.
      /// </para>
      /// </remarks>
      protected override void SolveNeuron(Neuron toSolve)
      {
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<Neuron>(InternalSolveNeuron), toSolve);
         base.SolveNeuron(toSolve);
      }

      protected override void OnNeuronProcessed(Neuron toSolve, NeuronCluster cluster)
      {
         SelectedMeaning = (from i in MeaningsToExec where i.Item.ID == (ulong)PredefinedNeurons.Actions select i).FirstOrDefault();
         base.OnNeuronProcessed(toSolve, cluster);
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<Neuron>(InternalNeuronProcessed), toSolve);
      }

      void InternalNeuronProcessed(Neuron toSolve)
      {
         DebugNeuron iToRemove = (from i in fStack where i.Item == toSolve select i).FirstOrDefault();         //we need to find the item, can't use ExecutingMeaning, cause this might already have changed (out of sync calls).
         if (iToRemove != null)
            fStack.Remove(iToRemove);
         if (ExecutingNeuron == iToRemove)                                                                     //only remove if still selected, if new neuron is already being processed, this value is changed.
            ExecutingNeuron = null;
      }

      protected override void ExecuteMeaning()
      {
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<int>(InternalExecuteMeaning), CurrentLink);
         base.ExecuteMeaning();
      }

      /// <summary>
      /// Internals the execute meaning.
      /// </summary>
      /// <param name="linkIndex">Index of the link. We pass this along cause this function might be called after the value is
      /// already changed in the processor's thread.  The index is ok, cause the list of links doesn't change during execution.</param>
      void InternalExecuteMeaning(int linkIndex)
      {
         if (MeaningsToExec.Count > linkIndex)
            SelectedMeaning = MeaningsToExec[linkIndex];
         else
            SelectedMeaning = null;
      }

      /// <summary>
      /// When the neuron being solved changed, we need to raise the PropertyChanged event so that the view can update.
      /// </summary>
      /// <param name="toSolve">Not really needed.</param>
      void InternalSolveNeuron(Neuron toSolve)
      {
         ExecutingNeuron = (from i in fStack where i.Item == toSolve select i).FirstOrDefault();
         MeaningsToExec.Clear();
         List<ulong> iMeanings;
         using (LinksAccessor iLinks = toSolve.LinksOut)
            iMeanings = (from i in iLinks.Items select i.MeaningID).ToList();    //we extract the id in a temp list to make certain that we don't cause any dead locks: when retrieving the meaning, we lock the brain, if another thread tries to get a lock on this list, while it has the brain locked, we get a dead lock.  this needs to be avoided.
         foreach (ulong i in iMeanings)
            MeaningsToExec.Add(new DebugNeuron(Brain.Current[i]));
         OnPropertyChanged("NeuronToSolve");
      }

      protected override void OnFinished()
      {
         base.OnFinished();
         IsRunning = false;
         SelectedMeaning = null;
      }

      #endregion

      #region INotifyPropertyChanged Members

      public event PropertyChangedEventHandler PropertyChanged;

      /// <summary>
      /// Raises the <see cref="DebugProcessor.PropertyChanged"/> event.
      /// </summary>
      /// <param name="name"></param>
      protected virtual void OnPropertyChanged(string name)
      {
         if (PropertyChanged != null)
            PropertyChanged(this, new PropertyChangedEventArgs(name));
      }

      #endregion




      /// <summary>
      /// Updates the debug timer used for the slowmotion.
      /// </summary>
      /// <param name="time">The new time to use for slow motion.</param>
      internal static void UpdateDebugTimer(int time)
      {
         if (fTimer != null)
            fTimer.Change(time, time);
      }
   }
}
