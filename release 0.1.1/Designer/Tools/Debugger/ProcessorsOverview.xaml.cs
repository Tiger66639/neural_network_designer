﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Interaction logic for ProcessorsOverview.xaml
   /// </summary>
   public partial class ProcessorsOverview : UserControl
   {
      public ProcessorsOverview()
      {
         InitializeComponent();
      }

      /// <summary>
      /// Handles the Checked event of the RBtnVariable control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void RBtnVariable_Checked(object sender, RoutedEventArgs e)
      {
         Watch iData = ((ButtonBase)sender).DataContext as Watch;
         if (iData != null)
         {
            Variable iVar = Brain.Current[iData.ID] as Variable;
            if (iVar == null)
               throw new InvalidOperationException(string.Format("Variables expected for watches, found invalid type for neuron {0}.",iData.ID));
            ProcessorManager.Current.DisplayValuesFor(iVar);
         }
      }

      private void ToggleVarsClick(object sender, RoutedEventArgs e)
      {
         ProcItemTemplateSelector iSelector = (ProcItemTemplateSelector)TrvValues.ItemTemplateSelector;
         TrvValues.ItemTemplateSelector = null;                                                             //we reset and set again, so the data is redrawn.
         iSelector.DisplayVariables = true;
         TrvValues.ItemTemplateSelector = iSelector;
         ProcessorManager.Current.Displaymode = ProcessorManager.DisplayMode.Variables;
         DataVarProcWatches.Visibility = Visibility.Collapsed;
         ScrollVarWatches.Visibility = Visibility.Visible;
      }

      private void ToggleProcClick(object sender, RoutedEventArgs e)
      {
         ProcItemTemplateSelector iSelector = (ProcItemTemplateSelector)TrvValues.ItemTemplateSelector;
         TrvValues.ItemTemplateSelector = null;                                                             //we reset and set again, so the data is redrawn.
         iSelector.DisplayVariables = false;
         TrvValues.ItemTemplateSelector = iSelector;
         ProcessorManager.Current.Displaymode = ProcessorManager.DisplayMode.Processors;
         DataVarProcWatches.Visibility = Visibility.Visible;
         ScrollVarWatches.Visibility = Visibility.Collapsed;
      }

      /// <summary>
      /// Handles the SelectedItemChanged event of the TrvValues control.
      /// </summary>
      /// <remarks>
      /// Keeps the selected item of the <see cref="ProcessorManager"/> in sync.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedPropertyChangedEventArgs&lt;System.Object&gt;"/> instance containing the event data.</param>
      private void TrvValues_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
      {
         ProcessorManager.Current.SelectedProcessor = e.NewValue as ProcItem;
      }


      #region Delete watch
      private void DeleteWatch_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         FrameworkElement iFocused = Keyboard.FocusedElement as FrameworkElement;
         e.CanExecute = e.Parameter is Watch || (iFocused != null && iFocused.DataContext is Watch);
      }

      private void DeleteWatch_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         Watch iWatch = e.Parameter as Watch;
         if (iWatch == null)
         {
            FrameworkElement iFocused = Keyboard.FocusedElement as FrameworkElement;
            iWatch = iFocused.DataContext as Watch;
         }
         if (iWatch != null)
            ProcessorManager.Current.Watches.Remove(iWatch);
      } 
      #endregion

      /// <summary>
      /// Handles the Click event of the MnuClear control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuClear_Click(object sender, RoutedEventArgs e)
      {
         ProcessorManager.Current.Watches.Clear();
      }

      private void DeleteBreakPoint_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         Expression iExp = e.Parameter as Expression;
         if (iExp == null)
         {
            FrameworkElement iFocused = Keyboard.FocusedElement as FrameworkElement;
            iExp = iFocused.DataContext as Expression;
         }
         BrainData.Current.BreakPoints.Remove(iExp);
      }

      private void DeleteBreakPoint_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         FrameworkElement iFocused = Keyboard.FocusedElement as FrameworkElement;
         e.CanExecute = e.Parameter is Expression || (iFocused != null && iFocused.DataContext is Expression);
      }
   }
}
