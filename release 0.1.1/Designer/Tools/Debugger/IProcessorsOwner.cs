﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeuralNetworkDesigne.Data;

namespace NeuralNetworkDesigne.HAB.Designer
{
   public interface IProcessorsOwner
   {
      /// <summary>
      /// Gets the list of processors.
      /// </summary>
      /// <value>The processors.</value>
      ObservedCollection<ProcManItem> Processors { get; }
   }
}
