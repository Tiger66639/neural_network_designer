﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeuralNetworkDesigne.Data;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Threading;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// This class stores references to all the running processors in order to provide data for them that can be
   /// used in the designer. Also manages the creation of processors for the designer.
   /// </summary>
   /// <remarks>
   /// This is an entry point through the stati <see cref="ProcessorManager.Current"/>.
   /// </remarks>
   public class ProcessorManager : ObservableObject, IProcessorFactory, IProcessorsOwner
   {

      #region inner types

      /// <summary>
      /// The mode in which data is displayed for the debugger. (so we don't calculate everything all the time.
      /// </summary>
      internal enum DisplayMode
      {
         /// <summary>
         /// Variable data is displayed
         /// </summary>
         Variables,
         /// <summary>
         /// Processor data is displayed
         /// </summary>
         Processors,
      }

      #endregion


      #region Fields

      const int MAXDELAY = 400;

      ProcItem fSelectedProcessor;
      static ProcessorManager fCurrent = new ProcessorManager();
      ObservedCollection<ProcManItem> fProcessors;
      int fSelectedWatch = -1;
      DisplayMode fDisplaymode = DisplayMode.Processors;

      #endregion


      #region Ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="ProcessorManager"/> class.
      /// </summary>
      public ProcessorManager()
      {
         fProcessors = new ObservedCollection<ProcManItem>(this);
         fProcessors.CollectionChanged += new NotifyCollectionChangedEventHandler(fProcessors_CollectionChanged);
         BrainData.Current.AfterLoad += new EventHandler(Current_AfterLoad);
      }


      #endregion

      #region Prop

      #region Current

      /// <summary>
      /// Gets the current processor manager.
      /// </summary>
      static public ProcessorManager Current
      {
         get { return fCurrent; }
      }

      #endregion 

      #region Processors

      /// <summary>
      /// Gets the list of processors currently running.  These are 'root' processors, splits are stored in the proc items.
      /// </summary>
      public ObservedCollection<ProcManItem> Processors
      {
         get { return fProcessors; }
      }

      #endregion

      #region SelectedProcessor

      /// <summary>
      /// Gets the curently selected processor item.
      /// </summary>
      /// <remarks>
      /// Is automaically set when the selection is changed on the <see cref="ProcessorsOverview"/> control or when the user control
      /// for a proc item is activated.
      /// </remarks>
      public ProcItem SelectedProcessor
      {
         get { return fSelectedProcessor; }
         internal set 
         {
            if (fSelectedProcessor != null)
            {
               DebugProcessor iProc = fSelectedProcessor.Processor as DebugProcessor;
               if (iProc != null && iProc.NextStatement != null)
                  BrainData.Current.NeuronInfo[iProc.NextStatement.ID].IsNextStatement = false;
            }
            fSelectedProcessor = value;
            if (fSelectedProcessor != null)
            {
               DebugProcessor iProc = fSelectedProcessor.Processor as DebugProcessor;
               if (iProc != null && iProc.NextStatement != null)
                  BrainData.Current.NeuronInfo[iProc.NextStatement.ID].IsNextStatement = true;
            }
            OnPropertyChanged("SelectedProcessor");
            UpdateProcessorStats();
         }
      }

      internal void UpdateProcessorStats()
      {
         if (fSelectedProcessor != null)
         {
            DebugProcessor iProc = fSelectedProcessor.Processor as DebugProcessor;
            if (iProc != null)
            {
               BrainData.Current.Debugmode = iProc.DebugMode;
               if (Displaymode == DisplayMode.Processors)
                  BuildProcData();
            }
            else if (Displaymode == DisplayMode.Processors)
               ClearProcData();
         }
         else if (Displaymode == DisplayMode.Processors)
            ClearProcData();
      }


      private void BuildProcData()
      {
         foreach (Watch i in Watches)                                                           //make certain no watch is displaying any data, there is no selected proc.
            i.LoadValuesFor(fSelectedProcessor.Processor);
      }

      private void ClearVarData()
      {
         foreach (ProcManItem i in Processors)
            i.ClearValues();
      }

      private void ClearProcData()
      {
         foreach (Watch i in Watches)                                                           //make certain no watch is displaying any data, there is no selected proc.
            i.Values.Clear();
      }

      private void BuildVarData()
      {
         ClearVarData();
         if (SelectedWatchIndex > -1)                                                           //only build the var data if there is a selected watch.
         {
            Variable iVar = Watches[SelectedWatchIndex].Item as Variable;
            if (iVar != null)
               DisplayValuesFor(iVar);
         }
      }


      #endregion

      #region Watches

      /// <summary>
      /// Gets the list of variables which should be displayed in a short list for selecting to monitor.
      /// </summary>
      public ObservedCollection<Watch> Watches
      {
         get 
         {
            if (BrainData.Current.DesignerData != null)
               return BrainData.Current.DesignerData.Watches;
            else
               return null;
         }
      }

      #endregion

      #region SelectedWatchIndex

      /// <summary>
      /// Gets/sets the index of the selected variable that needs to be monitored.
      /// </summary>
      public int SelectedWatchIndex
      {
         get
         {
            return fSelectedWatch;
         }
         set
         {
            if (value != fSelectedWatch)
            {
               if (value >= -1 && value < Watches.Count)
               {
                  fSelectedWatch = value;
                  OnPropertyChanged("SelectedWatchIndex");
                  if (value > -1)
                     DisplayValuesFor(Watches[value].NeuronInfo.Neuron as Variable);
                  else
                     DisplayValuesFor(null);
               }
               else
                  throw new IndexOutOfRangeException();
            }
         }
      }

      #endregion

      #region Debugmode

      /// <summary>
      /// Gets/sets the currently selected debug mode.  When a processor is selected, it is the debug mode assigned to
      /// processor, otherwise it indicates the mode used to create new processors with.
      /// </summary>
      public DebugMode Debugmode
      {
         get
         {
            if (SelectedProcessor != null && SelectedProcessor.Processor is DebugProcessor)
               return ((DebugProcessor)SelectedProcessor.Processor).DebugMode;
            else if (BrainData.Current.DesignerData != null)
               return BrainData.Current.Debugmode;
            else
               return DebugMode.Off;
         }
         set
         {
            if (SelectedProcessor != null && SelectedProcessor.Processor is DebugProcessor)
               ((DebugProcessor)SelectedProcessor.Processor).DebugMode = value;
            else if (BrainData.Current.DesignerData != null)
               BrainData.Current.Debugmode = value;
            else
               throw new InvalidOperationException("No data file loaded.");
            OnPropertyChanged("Debugmode");
         }
      }

      #endregion

      #region PlaySpeed

      /// <summary>
      /// Gets/sets the speed at which processor steps are played during a slow motion debug mode.
      /// </summary>
      public TimeSpan PlaySpeed
      {
         get
         {
            if (BrainData.Current.DesignerData != null)
               return BrainData.Current.DesignerData.PlaySpeed;
            return TimeSpan.MinValue;
         }
         set
         {
            if (BrainData.Current.DesignerData != null)
            {
               BrainData.Current.DesignerData.PlaySpeed = value;
               DebugProcessor.UpdateDebugTimer(MAXDELAY - value.Milliseconds);                                       //we need to inverse the value.
               OnPropertyChanged("PlaySpeed");
               OnPropertyChanged("PlaySpeedMSec");
            }
            else
               throw new InvalidOperationException("No data file loaded.");
         }
      }

      #endregion

      #region PlaySpeedMSec
      /// <summary>
      /// Gets or sets the play speed in milli seconds.
      /// </summary>
      /// <value>The play speed M sec.</value>
      public int PlaySpeedMSec
      {
         get { return PlaySpeed.Milliseconds; }
         set
         {
            PlaySpeed = new TimeSpan(0, 0, 0, 0, value);
         }
      } 
      #endregion

      #region HasProcessors

      /// <summary>
      /// Gets if there are curently processors alive.
      /// </summary>
      public bool HasProcessors
      {
         get { return Processors.Count > 0; }
      }

      #endregion

      
      #region Displaymode

      /// <summary>
      /// Gets/sets the mode by which data is displayed. This allows for some optimisations regarding the calculated data.
      /// </summary>
      internal DisplayMode Displaymode
      {
         get
         {
            return fDisplaymode;
         }
         set
         {
            if (fDisplaymode != value)
            {
               fDisplaymode = value;
               switch (value)
               {
                  case DisplayMode.Variables:
                     ClearProcData();
                     BuildVarData();
                     break;
                  case DisplayMode.Processors:
                     BuildProcData();
                     ClearVarData();
                     break;
                  default:
                     throw new InvalidOperationException();
               }
            }
         }
      }

      #endregion

      #endregion

      #region Functions

      /// <summary>
      /// Creates a <see cref="Processor"/> that can be used in calls to <see cref="TextSin.Process"/> or other methods
      /// that start a main input processing thread.
      /// </summary>
      /// <remarks>
      /// This method creates a <see cref="DebugProcessor"/> so that the user can follow the translation process.  It is
      /// also added to the <see cref="BraindData.Processors"/> list so it can be depicted.
      /// <para>
      /// Can be called from all kinds of threads, so all UI interaction must be done async.
      /// </para>
      /// </remarks>
      /// <returns></returns>
      public Processor GetProcessor()
      {
         Processor iRes;
         if (BrainData.Current.Debugmode == DebugMode.Off)
            iRes = new Processor();
         else
            iRes = new DebugProcessor();
         ProcItem iNew = new ProcItem(iRes);
         iNew.Name = Processors.Count.ToString();                                         //provide a default name for the processor.
        App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<ProcItem>(Processors.Add), iNew);
         return iRes;
      }


      /// <summary>
      /// Asks each <see cref="ProcItem"/> in <see cref="ProcessorManager.Processors"/> to calculate and display the current
      /// value(s) for the specified variable.
      /// </summary>
      /// <param name="toDisplay">Variable to display values for.</param>
      public void DisplayValuesFor(Variable toDisplay)
      {
         if (Displaymode == DisplayMode.Variables)
         {
            foreach (ProcManItem i in Processors)
               i.GetValuesFor(toDisplay);
         }
      }

      ///// <summary>
      ///// Updates the selected variable value
      ///// </summary>
      //internal void UpdateSelectedVariable()
      //{
      //   if (SelectedWatchIndex > -1)
      //      DisplayValuesFor(Watches[SelectedWatchIndex].NeuronInfo.Neuron as Variable);
      //}


      #endregion


      #region Event handlers

      /// <summary>
      /// Handles the AfterLoad event of the Current BrainData.
      /// </summary>
      /// <remarks>
      /// When data is loaded, need to raise property changed so that everything gets updated.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
      void Current_AfterLoad(object sender, EventArgs e)
      {
         OnPropertyChanged("Watches");
         OnPropertyChanged("PlaySpeed");
         OnPropertyChanged("PlaySpeedMSec");
         OnPropertyChanged("Debugmode");
      }

      /// <summary>
      /// Handles the CollectionChanged event of the fProcessors control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
      internal void fProcessors_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         if (e.Action == NotifyCollectionChangedAction.Remove && e.OldItems.Contains(SelectedProcessor) == true)
            SelectedProcessor = null;
         else if (e.Action == NotifyCollectionChangedAction.Reset)
            SelectedProcessor = null;
         OnPropertyChanged("HasProcessors");
      }

      #endregion


   }
}
