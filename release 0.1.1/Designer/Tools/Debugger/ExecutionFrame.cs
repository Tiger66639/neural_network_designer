﻿using System.Collections.Generic;
using NeuralNetworkDesigne.Data;
using System.Windows.Threading;
using System;
using System.Linq;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Contains the data of a function that is active in a <see cref="Processor"/>.
   /// </summary>
   /// <remarks>
   /// <para>
   /// This contains:
   /// - the neuron who's code is being executed (usually the '<see cref="Link.Meaning"/>' 
   /// part of a link.
   /// - The code list being executed (immutable).
   /// - the next item to execute (stored in the code items).
   /// </para>
   /// <para>
   /// Because there are a lot of calls to other execution list being made, we need to
   /// keep track of the different values.  That's what this class does.
   /// </para>
   /// </remarks>
   public class ExecutionFrame : OwnedObject, INeuronInfo, INeuronWrapper, IEditorSelection
   {

      #region Fields
      CodeItemCollection fCode;
      Neuron fItem;
      ExecListType fCodeListType;
      int fNextIndex = -1;
      bool fIsSelected;
      DebugNeuron fConditionalType;
      bool fHasConditionalType;
      EditorItemSelectionList<CodeItem> fSelectedCodeItems = new EditorItemSelectionList<CodeItem>();
      CallFrame fFrame;

      #endregion

      #region ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="ExecutionFrame"/> class.
      /// </summary>
      /// <param name="frame">The frame.</param>
      public ExecutionFrame(CallFrame frame)
      {
         switch (frame.CodeListType)
         {
            case ExecListType.Rules:
               fCode = new CodeItemCollection(this, frame.ExecSource.RulesCluster);
               break;
            case ExecListType.Actions:
               fCode = new CodeItemCollection(this, frame.ExecSource.ActionsCluster);
               break;
            case ExecListType.Children:
               fCode = new CodeItemCollection(this, frame.ExecSource as NeuronCluster);
               break;
            case ExecListType.Conditional:
               if (frame is ConditionsFrame)
                  fCode = new CodeItemCollection(this, ((ConditionsFrame)frame).ConditionsCluster);
               else
                  fCode = new CodeItemCollection(this, frame.ExecSource as NeuronCluster);
               fConditionalType = new DebugNeuron(((ConditionalFrame)frame).ConditionType);
               break;
            default:
               break;
         }
         fItem = frame.ExecSource;
         fCodeListType = frame.CodeListType;
         fFrame = frame;
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="ExecutionFrame"/> class, used for the 'if' and case statement'
      /// </summary>
      /// <param name="conditionsCluster">The conditions cluster.</param>
      public ExecutionFrame(NeuronCluster conditionsCluster)
      {
         fCode = new CodeItemCollection(this, conditionsCluster);
         //fConditionalType =
      }


      #endregion

      #region Prop

      /// <summary>
      /// Gets the frame.
      /// </summary>
      /// <value>The frame.</value>
      public CallFrame Frame
      {
         get { return fFrame; }
      }
      
      #region NextIndex

      /// <summary>
      /// Gets/sets the index of the next expression that will be executed.
      /// </summary>
      public int NextIndex
      {
         get
         {
            return fNextIndex;
         }
         set
         {
            if (value != fNextIndex)
            {
               if (fNextIndex > -1)
                  Code[fNextIndex].IsNextStatement = false;
               fNextIndex = value;
               if (fNextIndex > -1)
                  Code[fNextIndex].IsNextStatement = true;
               OnPropertyChanged("NextIndex");                                          //this should be thread save cause property changed works accross threads.
            }
         }
      }

      #endregion

      
      #region IsSelected

      /// <summary>
      /// Gets if this item is selected.
      /// </summary>
      public bool IsSelected
      {
         get
         {
            return fIsSelected;
         }
         internal set
         {
            fIsSelected = value;
            //App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<string>(OnPropertyChanged), "IsSelected");
            OnPropertyChanged("IsSelected");                                          //this should be thread save cause property changed works accross threads.
         }
      }

      #endregion

      #region Code

      /// <summary>
      /// Gets the list of execution items being performed by the processor.
      /// </summary>
      public CodeItemCollection Code
      {
         get { return fCode; }
      }

      #endregion

      #region SelectedItems
      /// <summary>
      /// gets the list with all the selected items.
      /// </summary>
      public IList<CodeItem> SelectedCodeItems
      {
         get
         {
            return fSelectedCodeItems;
         }
      }
      #endregion

      #region SelectedItem
      /// <summary>
      /// gets the first selected item.
      /// </summary>
      /// <remarks>
      /// Used by the ContextMenu of the CodeEditorPage to find the codeItem's IsBreakPoint property (and possibly others).
      /// </remarks>
      public CodeItem SelectedCodeItem
      {
         get
         {
            if (fSelectedCodeItems.Count > 0)
               return fSelectedCodeItems[0];
            else
               return null;
         }
      }
      #endregion

      #region Item

      /// <summary>
      /// Gets the Neuron who's code list is being executed.
      /// </summary>
      public Neuron Item
      {
         get { return fItem; }
      }

      #endregion

      #region CodeListType

      /// <summary>
      /// Identifies the type of the code list for the <see cref="ExecutionFrame.Item"/>.
      /// Possible values: Rules, Actions, Children, conditional.
      /// </summary>
      /// <remarks>
      /// A string is easy to display.
      /// </remarks>
      public ExecListType CodeListType
      {
         get { return fCodeListType; }
      }

      #endregion 

      #region ConditionalType

      /// <summary>
      /// Gets/sets the type of the conditional statement (if/case/...)
      /// </summary>
      public DebugNeuron ConditionalType
      {
         get
         {
            return fConditionalType;
         }
         set
         {
            if (value != fConditionalType)
            {
               fConditionalType = value;
               HasConditionalType = fConditionalType != null;
               OnPropertyChanged("ConditionalType");
            }
         }
      }

      #endregion

      
      #region HasConditionalType

      /// <summary>
      /// Gets wether this item has a conditional type assigned or not.
      /// </summary>
      public bool HasConditionalType
      {
         get
         {
            return fHasConditionalType;
         }
         private set
         {
            fHasConditionalType = value;
            OnPropertyChanged("HasConditionalType");
         }
      }

      #endregion

      #endregion

      #region INeuronInfo Members

      /// <summary>
      /// Gets the extra info for the specified neuron.  Can be null.
      /// </summary>
      /// <value></value>
      public NeuronData NeuronInfo
      {
         get
         {
            return BrainData.Current.NeuronInfo[Item.ID];
         }
      }

      #endregion

      #region IEditorSelection Members

      public System.Collections.IList SelectedItems
      {
         get { return fSelectedCodeItems; }
      }

      public object SelectedItem
      {
         get { return SelectedCodeItem;  }
      }

      #endregion



      internal void UpdateNext(int value)
      {
         if (value < Code.Count)                                                             //can happen: callframe don't check for overflow (they do it differently)
            NextIndex = value;
      }
   }
}
