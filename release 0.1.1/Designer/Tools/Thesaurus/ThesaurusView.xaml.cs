﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Interaction logic for PageRogetThesaurus.xaml
   /// </summary>
   public partial class ThesaurusView : UserControl
   {
      #region inner types

      /// <summary>
      /// Contains all the data for searching the tree of thesaurus items.
      /// </summary>
      class SearchValues
      {
         Dictionary<IThesaurusData, bool> fIsExpandedValues = new Dictionary<IThesaurusData, bool>();
         /// <summary>
         /// Gets or sets the text to search.
         /// </summary>
         /// <value>To search.</value>
         public string ToSearch { get; set; }
         /// <summary>
         /// Gets or sets the item that was selected when the search started.
         /// </summary>
         /// <value>The start.</value>
         public ThesaurusItem Start { get; set; }
         /// <summary>
         /// Gets or sets the item that is currently selected because it was the result of a search.
         /// </summary>
         /// <value>The current.</value>
         public ThesaurusItem Current { get; set; }
         /// <summary>
         /// Gets or sets the root thesaurus item
         /// </summary>
         /// <value>The root.</value>
         public Thesaurus Root { get; set; }


         /// <summary>
         /// Gets the dictionary of <see cref="IThesaurusData.IsExpanded"/> values for each item that was changed.
         /// </summary>
         /// <remarks>
         /// This allows us to open and close thesaurus items as we search, and restore there previous state when done.
         /// </remarks>
         /// <value>The is expanded values.</value>
         public Dictionary<IThesaurusData, bool> IsExpandedValues
         {
            get
            {
               return fIsExpandedValues;
            }
         }
      }

      #endregion

      #region fields

      TreeViewItem fSelected;
      SearchValues fSearchValues;                                 //stores the search info for as long as the same word is being searched.

      #endregion

      #region ctor
      public ThesaurusView()
      {
         InitializeComponent();
      } 

      #endregion


      /// <summary>
      /// Handles the Click event of the SearchNext control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void SearchNext_Click(object sender, RoutedEventArgs e)
      {
         if (fSearchValues == null)
         {
            fSearchValues = new SearchValues();
            fSearchValues.Root = (Thesaurus)DataContext;
            fSearchValues.ToSearch = TxtSearch.Text.ToLower();
            if (fSelected != null)
               fSearchValues.Start = fSelected.DataContext as ThesaurusItem;
            else if (fSearchValues.Root.Items.Count > 0)
               fSearchValues.Start = fSearchValues.Root.Items[0];
         }
         if (fSearchValues.Start != null)
            SearchTree();
         else
            MessageBox.Show("No data to search!");

      }

      /// <summary>
      /// Searches the tree.
      /// </summary>
      /// <param name="from">From.</param>
      /// <param name="toSearch">To search.</param>
      private void SearchTree()
      {
         ThesaurusItem iNext;
         if (fSearchValues.Current != null)
            iNext = GetNext(fSearchValues.Current);
         else
            iNext = fSearchValues.Start;
         if (iNext == null)                                                                     //the end was reached so do a wrap.
            iNext = fSearchValues.Root.Items[0];
         bool iEof = false;
         bool iFirstRun = true;
         while (iEof == false)
         {
            if (iNext.NeuronInfo.DisplayTitle.ToLower().Contains(fSearchValues.ToSearch) == true)
            {
               iNext.IsSelected = true;
               fSearchValues.Current = iNext;
               iEof = true;
            }
            else if (iNext == fSearchValues.Start && iFirstRun == false)                     //if we don't check iFirstRun, we wont skip the first item 
            {
               iEof = true;
               fSearchValues = null;                                                            //we reset the search.
               MessageBox.Show("Start of search has been reached.");
            }
            else
            {
               iFirstRun = false;
               iNext = GetNext(iNext);
               if (iNext == null)                                                                     //the end was reached so do a wrap.
                  iNext = fSearchValues.Root.Items[0];
            }
         }
      }

      /// <summary>
      /// Gets the next thesaurus item compared to the parameter, if the tree was flattened.
      /// </summary>
      /// <param name="from">From.</param>
      /// <returns></returns>
      private ThesaurusItem GetNext(ThesaurusItem from)
      {
         ThesaurusItem iFrom = from;
         if (fSearchValues.IsExpandedValues.ContainsKey(iFrom) == false)                                 //only try to store the val if not yet done, otherwise it has no use cause we would overwrite the original value.
            fSearchValues.IsExpandedValues[iFrom] = iFrom.IsExpanded;
         iFrom.IsExpanded = true;
         if (iFrom.Items.Count > 0)                                                                      //if the item has children, always return the first one, to go into the tree first.  Once there are no items anymore, we go back up.
            return iFrom.Items[0];
         IThesaurusData iOwner = from.Owner;
         int iIndex = iOwner.Items.IndexOf(iFrom);
         while (iOwner != iOwner.Root && iIndex == iOwner.Items.Count - 1)                                //for as long as this is the last item in the list, go 1 owner up.
         {
            iOwner.IsExpanded = fSearchValues.IsExpandedValues[iOwner];                                  //we reset this value once it is done.
            iFrom = (ThesaurusItem)iOwner;
            iOwner = iFrom.Owner;
            iIndex = iOwner.Items.IndexOf(iFrom);
         }
         if (iIndex < iOwner.Items.Count - 1)
            return iOwner.Items[iIndex + 1];
         else
            return null;
      }


      /// <summary>
      /// Keeps track of the currently selected treeview item.
      /// </summary>
      private void TreeItems_Selected(object sender, RoutedEventArgs e)
      {
         fSelected = e.OriginalSource as TreeViewItem;
      }

      private void TxtSearch_KeyDown(object sender, KeyEventArgs e)
      {
         if (e.Key == Key.Enter)
            SearchNext_Click(sender, null);
         else
            fSearchValues = null;                                             //for each char that is changed, a new search needs to be performed.
      }

   }
}
