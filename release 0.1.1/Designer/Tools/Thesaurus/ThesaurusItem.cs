﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeuralNetworkDesigne.Data;
using System.Windows;
using System.Windows.Threading;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// A single item in the thesaurus.
   /// </summary>
   /// <remarks>
   /// <para>
   /// A thesaurus item finds it's children in the following manner:
   /// - check if the wrapped item is owned by clusters that have the meaning = relations, if so, for all clusters:
   /// - look for incomming links on those cluster with the specified meaning, 
   ///   all the starting points form the children.
   /// This algorithm comes from the fact that the relationships are stored in reverse order: an object points to a cluster with
   /// all the related items for 1 relationship, so in reverse order can an object be located in multiple lists.
   /// </para>
   /// <para>
   /// For this reason, we need to do some custom monitoring of links and clusters in order to keep it all in sync.
   /// We need to keep an eye on the cluster content changes to see if our wrapped object is added/removed to items.
   /// We also need to keep an eye on all the links to see if they are changed/deleted. This will also keep track if
   /// any objects are deleted: the links will also be deleted.
   /// </para>
   /// </remarks>
   public class ThesaurusItem : OwnedObject<IThesaurusData>, IThesaurusData, INeuronWrapper, INeuronInfo, IWeakEventListener
   {
      #region Fields

      ObservedCollection<ThesaurusItem> fItems;
      NeuronCluster fToWrap;
      bool fIsExpanded;
      bool fHasItems;
      List<NeuronCluster> fClustersToCheck;
      List<Link> fCheckForLinkChange;
      bool fIsSelected;

      #endregion

      #region ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="ThesaurusItem"/> class.
      /// </summary>
      public ThesaurusItem(NeuronCluster toWrap, Neuron relation)
      {
         fItems = new ObservedCollection<ThesaurusItem>(this);
         fToWrap = toWrap;
         CheckHasItems(relation);
      }

      #endregion

      #region Prop

      #region IsExpanded

      /// <summary>
      /// Gets/sets the wether the list is loaded or not.
      /// </summary>
      public bool IsExpanded
      {
         get
         {
            return fIsExpanded;
         }
         set
         {
            if (value != fIsExpanded)
            {
               fIsExpanded = value;
               OnPropertyChanged("IsExpanded");
               if (value == true)
                  LoadItems(Root.SelectedRelationship);
               else
                  UnloadItems();
            }
         }
      }

      #endregion

      #region IsSelected

      /// <summary>
      /// Gets/sets wether this item is currently the selected one or not.
      /// </summary>
      /// <remarks>
      /// Also updates the <see cref="Thesaurus.SelectedItem"/>
      /// </remarks>
      public bool IsSelected
      {
         get
         {
            return fIsSelected;
         }
         set
         {
            if (value != fIsSelected)
            {
               fIsSelected = value;
               OnPropertyChanged("IsSelected");
               Thesaurus iRoot = Root;
               if (iRoot != null)
                  iRoot.SelectedItem = (value == true ? this : null);
            }
         }
      }

      #endregion

      #region HasItems

      /// <summary>
      /// Gets/sets the wether the item has any children or not.
      /// </summary>
      public bool HasItems
      {
         get
         {
            return fHasItems;
         }
         internal set
         {
            fHasItems = value;
            OnPropertyChanged("HasItems");
         }
      }

      #endregion

      #region IThesaurusData Members

      /// <summary>
      /// Gets the list of items
      /// </summary>
      /// <value></value>
      public ObservedCollection<ThesaurusItem> Items
      {
         get { return fItems; }
      }

      /// <summary>
      /// Gets the root of the thesaurus
      /// </summary>
      /// <value>The root.</value>
      public Thesaurus Root
      {
         get { return Owner.Root; }
      }

      #endregion

      #region Item (INeuronWrapper Members)

      /// <summary>
      /// Gets the item.
      /// </summary>
      /// <value>The item.</value>
      public Neuron Item
      {
         get { return fToWrap; }
      }

      #endregion

      #region NeuronInfo(INeuronInfo Members)

      /// <summary>
      /// Gets the extra info for the specified neuron.  Can be null.
      /// </summary>
      /// <value></value>
      public NeuronData NeuronInfo
      {
         get
         {
            if (fToWrap != null)
               return BrainData.Current.NeuronInfo[fToWrap.ID];
            else
               return null;
         }
      }

      #endregion 
      #endregion


      #region Functions


      /// <summary>
      /// Checks if the item to wrap has any children.
      /// </summary>
      private void CheckHasItems(Neuron relation)
      {
         using (NeuronsAccessor iClusteredByAcc = fToWrap.ClusteredBy)
         {
            var iClusteredBy = from i in iClusteredByAcc.Items
                               let iCluster = Brain.Current[i] as NeuronCluster
                               where iCluster.Meaning == relation.ID
                               select iCluster;

            foreach (NeuronCluster iCluster in iClusteredBy)
            {
               using (LinksAccessor iLinksIn = iCluster.LinksIn)
               {
                  var iLinks = (from i in iLinksIn.Items
                                let iFrom = i.From
                                where i.MeaningID == relation.ID && iFrom is NeuronCluster
                                select iFrom).FirstOrDefault();
                  if (iLinks != null)
                  {
                     HasItems = true;
                     return;
                  }
               }
            }
         }
         HasItems = false;
      }

      /// <summary>
      /// searches for and loads all the items that are related to the wrapped item according to the specified relationship.
      /// </summary>
      /// <param name="relation">The relation.</param>
      private void LoadItems(Neuron relation)
      {
         using (NeuronsAccessor iClusteredByAcc = fToWrap.ClusteredBy)
         {
            fClustersToCheck = (from i in iClusteredByAcc.Items
                                let iCluster = Brain.Current[i] as NeuronCluster
                                where iCluster.Meaning == relation.ID
                                select iCluster).ToList();

            foreach (NeuronCluster iCluster in fClustersToCheck)
            {
               using (LinksAccessor iLinksIn = iCluster.LinksIn)
               {
                  fCheckForLinkChange = (from i in iLinksIn.Items
                                         where i.MeaningID == relation.ID && i.From is NeuronCluster
                                         select i).ToList();
                  foreach (Link iChild in fCheckForLinkChange)
                     Items.Add(new ThesaurusItem((NeuronCluster)iChild.From, relation));
               }
            }
         }
      }

      private void UnloadItems()
      {
         Items.Clear();
         fClustersToCheck = null;
         fCheckForLinkChange = null;
      }

      /// <summary>
      /// Receives events from the centralized event manager.
      /// </summary>
      /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
      /// <param name="sender">Object that originated the event.</param>
      /// <param name="e">Event data.</param>
      /// <returns>
      /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
      /// </returns>
      public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(LinkChangedEventManager))
         {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<LinkChangedEventArgs>(LinkChanged), e);
            return true;
         }
         else if (managerType == typeof(NeuronListChangedEventManager))
         {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<NeuronListChangedEventArgs>(ListChanged), e);
            return true;
         }
         return false;
      }

      private void ListChanged(NeuronListChangedEventArgs e)
      {
         if (fClustersToCheck != null)
         {
            using (NeuronsAccessor iList = e.List)
            {
               var iFound = (from i in fClustersToCheck
                         where i.Children == iList
                         select i).FirstOrDefault();
               if (iFound != null || e.Item == fToWrap)
               {
                  UnloadItems();
                  Thesaurus iRoot = Root;
                  if (iRoot != null)
                     LoadItems(iRoot.SelectedRelationship);
               }
            }
         }
      }

      /// <summary>
      /// called whena link is changed.
      /// </summary>
      /// <param name="e">The <see cref="NeuralNetworkDesigne.HAB.LinkChangedEventArgs"/> instance containing the event data.</param>
      private void LinkChanged(LinkChangedEventArgs e)
      {
         Thesaurus iRoot = Root;
         if (fCheckForLinkChange != null)
         {
            var iFound = (from i in fCheckForLinkChange
                          where i == e.OriginalSource
                          select i).FirstOrDefault();
            if (iFound != null || e.NewTo == fToWrap.ID)
            {
               UnloadItems();
               if (iRoot != null)
                  LoadItems(iRoot.SelectedRelationship);
            }
         }
         if (iRoot != null)
         {
            if (IsSelected == true && e.OldTo == iRoot.Synonyms.Cluster.ID && e.OldFrom == Item.ID)         //the link changed somehow
            {
               ulong iMeaning = WordNetSin.Default.GetLinkDef("synonyms");
               NeuronCluster iCluster = Item.FindFirstOut(iMeaning) as NeuronCluster;
               iRoot.Synonyms = new SynonymsCollection(this, iCluster);
            }
         }
      }

      #endregion
   }
}
