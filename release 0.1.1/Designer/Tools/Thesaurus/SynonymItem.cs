﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeuralNetworkDesigne.Data;
using System.Windows;
using System.Diagnostics;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// A wrapper type for neurons that represents synonyms for the <see cref="Thesaurus.SelectedItem"/>
   /// </summary>
   public class SynonymItem : OwnedObject<ThesaurusItem>, INeuronWrapper, INeuronInfo
   {
      Neuron fToWrap;

      /// <summary>
      /// Initializes a new instance of the <see cref="SynonymItem"/> class.
      /// </summary>
      /// <param name="toWrap">To wrap.</param>
      public SynonymItem(Neuron toWrap)
      {
         Debug.Assert(toWrap != null);
         fToWrap = toWrap;
      }

      #region Item (INeuronWrapper Members)

      /// <summary>
      /// Gets the item.
      /// </summary>
      /// <value>The item.</value>
      public Neuron Item
      {
         get { return fToWrap; }
      }

      #endregion

      #region NeuronInfo (INeuronInfo Members)

      /// <summary>
      /// Gets the extra info for the specified neuron.  Can be null.
      /// </summary>
      /// <value></value>
      public NeuronData NeuronInfo
      {
         get 
         {
            if (fToWrap != null)
               return BrainData.Current.NeuronInfo[fToWrap.ID];
            else
               return null;
         }
      }

      #endregion
   }
}
