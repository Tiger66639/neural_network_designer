﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DnD;
using System.Windows.Controls;
using NeuralNetworkDesigne.ControlFramework.Utility;
using NeuralNetworkDesigne.Data;
using System.Windows;

namespace NeuralNetworkDesigne.HAB.Designer
{
   public class SynonymsListDropAdvisor : DropTargetBase
   {
      #region prop
      #region Target

      public ItemsControl Target
      {
         get
         {
            return TreeHelper.FindInTree<ItemsControl>(TargetUI);
         }
      }

      #endregion

      #region UsePreviewEvents
      /// <summary>
      /// Gets if the preview event versions should be used or not.
      /// </summary>
      /// <remarks>
      /// don't use preview events cause than the sub lists don't get used but only the main list cause this gets the events first,
      /// while we usually want to drop in a sublist.
      /// </remarks>
      public override bool UsePreviewEvents
      {
         get
         {
            return false;
         }
      }
      #endregion

      #region Items

      /// <summary>
      /// Gets the list containing all the code that the UI to which advisor is attached too, displays data for.
      /// </summary>
      public SynonymsCollection Items
      {
         get
         {
            return Target.ItemsSource as SynonymsCollection;
         }
      }

      #endregion
      #endregion

      #region Overrides
      /// <summary>
      /// Raises the <see cref="E:DropCompleted"/> event.
      /// </summary>
      /// <param name="arg">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
      /// <param name="dropPoint">The drop point.</param>
      public override void OnDropCompleted(DragEventArgs arg, Point dropPoint)
      {
         SynonymsCollection iItems = Items;
         ulong iId = (ulong)arg.Data.GetData(Properties.Resources.NeuronIDFormat);
         Neuron iDropped = Brain.Current[iId];

         var iFound = (from i in iItems
                       where
                          i.Item.ID == iId
                       select i).FirstOrDefault();
         if (iFound == null)
         {
            SynonymItem iNew = new SynonymItem(iDropped);
            iItems.Add(iNew);
         }
         else
            throw new InvalidOperationException();
      }


      /// <summary>
      /// Determines whether [is valid data object] [the specified obj].
      /// </summary>
      /// <param name="obj">The obj.</param>
      /// <returns>
      /// 	<c>true</c> if [is valid data object] [the specified obj]; otherwise, <c>false</c>.
      /// </returns>
      public override bool IsValidDataObject(IDataObject obj)
      {
         if (obj.GetDataPresent(Properties.Resources.NeuronIDFormat) == true)
         {
            ulong iId = (ulong)obj.GetData(Properties.Resources.NeuronIDFormat);
            SynonymsCollection iItems = Items;
            using (ChildrenAccessor iList = iItems.Cluster.Children)
               return iItems != null && iList.Contains(iId) == false;
         }
         else
            return false;
      }

      //public override DragDropEffects GetEffect(DragEventArgs e)
      //{
      //   DragDropEffects iRes = base.GetEffect(e);
      //   if (iRes == DragDropEffects.Move)
      //   {
      //      CodeItem iItem = e.Data.GetData(Properties.Resources.CodeItemFormat) as CodeItem;
      //      if (iItem != null && CodeList.Contains(iItem) == true)
      //         return DragDropEffects.Copy;                                                                 //when we move on the same list, the drag source doesn't have to do anything.
      //   }
      //   return iRes;
      //}

      #endregion
   }
}
