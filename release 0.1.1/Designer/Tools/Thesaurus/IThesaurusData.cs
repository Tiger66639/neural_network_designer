﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeuralNetworkDesigne.Data;

namespace NeuralNetworkDesigne.HAB.Designer
{
   public interface IThesaurusData
   {
      /// <summary>
      /// Gets the list of items
      /// </summary>
      ObservedCollection<ThesaurusItem> Items { get; }

      /// <summary>
      /// Gets the root of the thesaurus
      /// </summary>
      /// <value>The root.</value>
      Thesaurus Root { get; }

      /// <summary>
      /// Gets or sets a value indicating whether this instance is expanded.
      /// </summary>
      /// <value>
      /// 	<c>true</c> if this instance is expanded; otherwise, <c>false</c>.
      /// </value>
      bool IsExpanded { get; set; }
   }
}
