﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using NeuralNetworkDesigne.ControlFramework.Utility;
using System;
using System.Windows.Threading;
using NeuralNetworkDesigne.LogService;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Interaction logic for DescriptionView.xaml
   /// </summary>
   public partial class DescriptionView : UserControl, IWeakEventListener
   {
      #region Fields

      bool fIsChanged;                                                              //keeps track if the data in the richtextbox is actually changed since the last data assign.
      bool fIsUpdating;                                                             //set to true when we are updating the toolbar buttons based on the current state of the newly selected items, so that we don't re-apply the values (response of the buttons).

      #endregion

      #region ctor
      /// <summary>
      /// Initializes a new instance of the <see cref="DescriptionView"/> class.
      /// </summary>
      public DescriptionView()
      {
         InitializeComponent();
         AfterLoadEventManager.AddListener(BrainData.Current, this);
      }

      /// <summary>
      /// Releases unmanaged resources and performs other cleanup operations before the
      /// <see cref="DescriptionView"/> is reclaimed by garbage collection.
      /// </summary>
      ~DescriptionView()
      {
         AfterLoadEventManager.RemoveListener(BrainData.Current, this);
      }

      #endregion

      #region prop

      /// <summary>
      /// Gets a value indicating whether the current document was changed or not.
      /// </summary>
      /// <value>
      /// 	<c>true</c> if this instance is changed; otherwise, <c>false</c>.
      /// </value>
      public bool IsChanged
      {
         get { return fIsChanged; }
      }

      /// <summary>
      /// Gets or sets the document that is currently depicted.
      /// </summary>
      /// <remarks>
      /// The get simply returns a shallow copy of the current flowdocument.  The setter uses the reference supplied, doesn't copy the contents.
      /// When null is assigned, the editor is disabled, but the previous data is still visible (although not returned). This is done
      /// so that a text can still be read after something else was selected that doesn't have a description.
      /// </remarks>
      /// <value>The document.</value>
      public FlowDocument Document
      {
         get
         {
            if (DescriptionEditor.IsEnabled == false)
               return null;
            else
               return DescriptionEditor.Document;
         }
         set
         {
            if (value != null)
            {
               DescriptionEditor.Document = value;                                                 //we can assign directly cause all flowDocuments given to this editor should be detached.
               DescriptionEditor.IsEnabled = true;
            }
            else
               DescriptionEditor.IsEnabled = false;
            fIsChanged = false;                                                                    //important: needs to be at the end, cause changing anything to the document, triggers 'TextChanged' which sets changed to true.
         }
      }

      #endregion


      /// <summary>
      /// Handles the SelectionChanged event of the DescriptionEditor control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void DescriptionEditor_SelectionChanged(object sender, RoutedEventArgs e)
      {
         fIsUpdating = true;
         try
         {
            TextRange selectionRange = new TextRange(DescriptionEditor.Selection.Start, DescriptionEditor.Selection.End);
            BtnBold.IsChecked = selectionRange.GetPropertyValue(FontWeightProperty).ToString() == "Bold";
            BtnItalic.IsChecked = selectionRange.GetPropertyValue(FontWeightProperty).ToString() == "Italic";
            BtnUnderline.IsChecked = selectionRange.GetPropertyValue(Inline.TextDecorationsProperty) == TextDecorations.Underline;

            BtnBullets.IsChecked = false;
            BtnNumbers.IsChecked = false;
            DependencyObject iFound = selectionRange.Start.Parent;
            List iList = TreeHelper.FindInLTree<List>(iFound);
            if (iList != null)
            {
               TextMarkerStyle iStyle = iList.MarkerStyle;
               if (iStyle == TextMarkerStyle.Decimal)
                  BtnNumbers.IsChecked = true;
               else if (iStyle != TextMarkerStyle.None)
                  BtnBullets.IsChecked = true;
            }

            CmbFontSize.SelectedValue = selectionRange.GetPropertyValue(FlowDocument.FontSizeProperty);
            CmbFont.SelectedItem = selectionRange.GetPropertyValue(FlowDocument.FontFamilyProperty);

            if (selectionRange.GetPropertyValue(FlowDocument.TextAlignmentProperty).Equals(TextAlignment.Left))
               BtnAlignLeft.IsChecked = true;
            else if (selectionRange.GetPropertyValue(FlowDocument.TextAlignmentProperty).Equals(TextAlignment.Right))
               BtnAlignRight.IsChecked = true;
            else if (selectionRange.GetPropertyValue(FlowDocument.TextAlignmentProperty).Equals(TextAlignment.Justify))
               BtnAlignJustify.IsChecked = true;
            else if (selectionRange.GetPropertyValue(FlowDocument.TextAlignmentProperty).Equals(TextAlignment.Center))
               BtnAlignCenter.IsChecked = true;
         }
         finally
         {
            fIsUpdating = false;
         }
      }

      /// <summary>
      /// Handles the TextInput event of the CmbFontSize control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.TextCompositionEventArgs"/> instance containing the event data.</param>
      private void CmbFontSize_TextInput(object sender, TextCompositionEventArgs e)
      {
         if (fIsUpdating == false)
         {
            double ival;
            if (double.TryParse(e.Text, out ival) == true)
               DescriptionEditor.Selection.ApplyPropertyValue(RichTextBox.FontSizeProperty, ival);
            else
               Log.LogError("DescriptionView.CmbFontSize_TextInput", "Fontsize requires an integer value.");
         }
      }

      /// <summary>
      /// Handles the SelectionChanged event of the CmbFontSize control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
      private void CmbFontSize_SelectionChanged(object sender, SelectionChangedEventArgs e)
      {
         if (fIsUpdating == false)
            DescriptionEditor.Selection.ApplyPropertyValue(RichTextBox.FontSizeProperty, e.AddedItems[0]);
      }

      /// <summary>
      /// Handles the SelectionChanged event of the CmbFont control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
      private void CmbFont_SelectionChanged(object sender, SelectionChangedEventArgs e)
      {
         if (fIsUpdating == false)
            DescriptionEditor.Selection.ApplyPropertyValue(RichTextBox.FontFamilyProperty, e.AddedItems[0]);
      }

      /// <summary>
      /// Handles the TextChanged event of the DescriptionEditor control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Controls.TextChangedEventArgs"/> instance containing the event data.</param>
      private void DescriptionEditor_TextChanged(object sender, TextChangedEventArgs e)
      {
         fIsChanged = true;
      }

      #region IWeakEventListener Members

      public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(AfterLoadEventManager))
         {
            BrainData_AfterLoaded(sender, e);
            return true;
         }
         else
            return false;
      }

      /// <summary>
      /// Handles the AfterLoaded event of the BrainData control.
      /// </summary>
      /// <remarks>
      /// When a new one is loaded, need to make certain that the text of the previous one is gone.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
      private void BrainData_AfterLoaded(object sender, EventArgs e)
      {
         Document = new FlowDocument();                        //first empty it
         Document = null;                                      //setting it to null wont empty it, but will disable it.
         fIsChanged = false;
      }

      #endregion
   }
}
