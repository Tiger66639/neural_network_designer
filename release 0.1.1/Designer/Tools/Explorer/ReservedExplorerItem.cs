﻿
namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// This class repressents an id that has been reserved by the system for later use,
   /// but currently has no meaning yet.
   /// </summary>
   public class ReservedExplorerItem : FreeExplorerItem
   {
      public ReservedExplorerItem(ulong id)
         : base(id)
      {
      }
   }
}
