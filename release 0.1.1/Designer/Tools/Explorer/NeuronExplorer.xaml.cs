﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;
using NeuralNetworkDesigne.LogService;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Interaction logic for NeuronExplorer.xaml
   /// </summary>
   public partial class NeuronExplorer : UserControl, IWeakEventListener
   {
      #region fields

      ObservableCollection<ExplorerItem> fItems = new ObservableCollection<ExplorerItem>();
      ulong fPrevSearched;
      bool fSearchStart;

      #endregion

      #region ctor
      /// <summary>
      /// Initializes a new instance of the <see cref="NeuronExplorer"/> class.
      /// </summary>
      public NeuronExplorer()
      {
         InitializeComponent();
         Brain_Loaded(Brain.Current, EventArgs.Empty);                                                      //when we first start up, we make certain that the init values are always set.  We don't get this event when the application starts without a default project (so a new empty project is used).
         LoadedEventManager.AddListener(Brain.Current, this);
         ClearedEventManager.AddListener(Brain.Current, this);
         NeuronChangedEventManager.AddListener(Brain.Current, this);
         NeuronListChangedEventManager.AddListener(Brain.Current, this);
         CommandBindings.AddRange(App.Current.MainWindow.CommandBindings);                                  //we do this so we can access the command bindings, declared on the main window, even when the form is floating.
         BrainData.Current.AfterLoad += new EventHandler(Current_AfterLoad);
      }

      /// <summary>
      /// Handles the AfterLoad event of the Current control.
      /// </summary>
      /// <remarks>
      /// each time that the defaultMeaningIDs list is changed, we need to re attach the listener cause load creates a new list.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
      void Current_AfterLoad(object sender, EventArgs e)
      {
         CollectionChangedEventManager.AddListener(BrainData.Current.DefaultMeaningIds, this);
      }

      /// <summary>
      /// Releases unmanaged resources and performs other cleanup operations before the
      /// <see cref="NeuronExplorer"/> is reclaimed by garbage collection.
      /// </summary>
      ~NeuronExplorer()
      {
         if (Environment.HasShutdownStarted == false)                                                       //only try to unregister listeners when the app is not shutting down, otherwise it wont work anymore.
         {
            LoadedEventManager.RemoveListener(Brain.Current, this);
            ClearedEventManager.RemoveListener(Brain.Current, this);
            NeuronChangedEventManager.RemoveListener(Brain.Current, this);
            NeuronListChangedEventManager.RemoveListener(Brain.Current, this);
            CollectionChangedEventManager.RemoveListener(BrainData.Current.DefaultMeaningIds, this);
         }
      }

      #endregion

      #region prop

      
      #region Items

      /// <summary>
      /// Gets the list of items that are currently loaded.
      /// </summary>
      public ObservableCollection<ExplorerItem> Items
      {
         get { return fItems; }
      }

      #endregion

      #region IsChildrenLoaded

      /// <summary>
      /// IsChildrenLoaded Dependency Property
      /// </summary>
      public static readonly DependencyProperty IsChildrenLoadedProperty =
          DependencyProperty.Register("IsChildrenLoaded", typeof(bool), typeof(NeuronExplorer),
              new FrameworkPropertyMetadata((bool)false,
                  new PropertyChangedCallback(OnIsChildrenLoadedChanged)));

      /// <summary>
      /// Gets or sets the IsChildrenLoaded property.  This dependency property 
      /// indicates weather we should try to load the Children of the currently selected item or not.
      /// </summary>
      public bool IsChildrenLoaded
      {
         get { return (bool)GetValue(IsChildrenLoadedProperty); }
         set { SetValue(IsChildrenLoadedProperty, value); }
      }

      /// <summary>
      /// Handles changes to the IsChildrenLoaded property.
      /// </summary>
      private static void OnIsChildrenLoadedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
      {
         ((NeuronExplorer)d).OnIsChildrenLoadedChanged(e);
      }

      /// <summary>
      /// Provides derived classes an opportunity to handle changes to the IsChildrenLoaded property.
      /// </summary>
      protected virtual void OnIsChildrenLoadedChanged(DependencyPropertyChangedEventArgs e)
      {
      }

      #endregion

      

      #region IsEditing

      /// <summary>
      /// IsEditing Dependency Property
      /// </summary>
      public static readonly DependencyProperty IsEditingProperty =
          DependencyProperty.Register("IsEditing", typeof(bool), typeof(NeuronExplorer),
              new FrameworkPropertyMetadata((bool)false,
                  new PropertyChangedCallback(OnIsEditingChanged)));

      /// <summary>
      /// Gets or sets the IsEditing property.  This dependency property 
      /// indicates if the currently selected item should be in edit mode or not.
      /// </summary>
      public bool IsEditing
      {
         get { return (bool)GetValue(IsEditingProperty); }
         set { SetValue(IsEditingProperty, value); }
      }

      /// <summary>
      /// Handles changes to the IsEditing property.
      /// </summary>
      private static void OnIsEditingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
      {
         ((NeuronExplorer)d).OnIsEditingChanged(e);
      }

      /// <summary>
      /// Provides derived classes an opportunity to handle changes to the IsEditing property.
      /// </summary>
      protected virtual void OnIsEditingChanged(DependencyPropertyChangedEventArgs e)
      {
         if ((bool)e.NewValue == false)
         {
            IInputElement iCont = LstItems.ItemContainerGenerator.ContainerFromItem(LstItems.SelectedItem) as IInputElement;     
            if (iCont != null)
               Keyboard.Focus(iCont);                                               //need to focus to the container, and not the list so that nav keys keep working.
            else
               LstItems.Focus();
         }
      }

      #endregion


      #region SelectedID

      /// <summary>
      /// Gets or sets the currently selected neuron ID.
      /// </summary>
      /// <value>The selected neuron.</value>
      public ulong SelectedID
      {
         get
         {
            if (LstChildren.SelectedIndex > -1)
               return CurrentScrollPos + (ulong)LstChildren.SelectedIndex;
            else
               return Neuron.EmptyId;
         }
         set
         {
            if (value != Neuron.EmptyId)
            {
               if (value > MaxScrollValue)
               {
                  CurrentScrollPos = (ulong)MaxScrollValue;
                  LstItems.SelectedIndex = (int)(value - (ulong)MaxScrollValue);
               }
               else
               {
                  CurrentScrollPos = value;
                  LstItems.SelectedIndex = 0;
               }
            }
            else
               LstItems.SelectedItems.Clear();
         }
      }

	   #endregion


      #region ItemCount

      /// <summary>
      /// ItemCount Read-Only Dependency Property
      /// </summary>
      private static readonly DependencyPropertyKey ItemCountPropertyKey
          = DependencyProperty.RegisterReadOnly("ItemCount", typeof(ulong), typeof(NeuronExplorer),
              new FrameworkPropertyMetadata((ulong)0, new PropertyChangedCallback(OnItemCountChanged)));

      public static readonly DependencyProperty ItemCountProperty
          = ItemCountPropertyKey.DependencyProperty;

      /// <summary>
      /// Gets the ItemCount property.  This dependency property 
      /// indicates the total number of neurons in the list.
      /// </summary>
      /// <remarks>
      /// this is used by the scrollbar to set the maximum value.
      /// </remarks>
      public ulong ItemCount
      {
         get { return (ulong)GetValue(ItemCountProperty); }
      }

      /// <summary>
      /// Provides a secure method for setting the ItemCount property.  
      /// This dependency property indicates the total number of neurons in the list.
      /// </summary>
      /// <param name="value">The new value for the property.</param>
      protected void SetItemCount(ulong value)
      {
         SetValue(ItemCountPropertyKey, value);
      }

      static void OnItemCountChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
      {
         NeuronExplorer iSender = (NeuronExplorer)sender;
         iSender.SetMaxScrollValue((double)((ulong)e.NewValue - (ulong)iSender.MaxVisible));
      }

      #endregion

      #region CurrentScrollPos

      /// <summary>
      /// CurrentScrollPos Dependency Property
      /// </summary>
      public static readonly DependencyProperty CurrentScrollPosProperty =
          DependencyProperty.Register("CurrentScrollPos", typeof(ulong), typeof(NeuronExplorer),
              new FrameworkPropertyMetadata(Neuron.StartId,
                  new PropertyChangedCallback(OnCurrentScrollPosChanged), new CoerceValueCallback(OnCoerceCurrentScrollValue)));

      /// <summary>
      /// Gets or sets the CurrentScrollPos property.  This dependency property 
      /// indicates the id of the topmost currently visible neuron.
      /// </summary>
      /// <remarks>
      /// This is used by the scrollbar.
      /// </remarks>
      public ulong CurrentScrollPos
      {
         get { return (ulong)GetValue(CurrentScrollPosProperty); }
         set { SetValue(CurrentScrollPosProperty, value); }
      }

      /// <summary>
      /// Handles changes to the CurrentScrollPos property.
      /// </summary>
      private static void OnCurrentScrollPosChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
      {
         ((NeuronExplorer)d).OnCurrentScrollPosChanged(e);
      }

      /// <summary>
      /// Provides derived classes an opportunity to handle changes to the CurrentScrollPos property.
      /// </summary>
      protected virtual void OnCurrentScrollPosChanged(DependencyPropertyChangedEventArgs e)
      {
         ulong iDif;
         if ((ulong)e.NewValue > (ulong)e.OldValue)
            iDif = (ulong)e.NewValue - (ulong)e.OldValue;
         else
            iDif = (ulong)e.OldValue - (ulong)e.NewValue;

         if (iDif >= (ulong)MaxVisible)                                                                 //shortcut: if we scroll more than is visible, we simply jump to the correct part in the list.
            RenderListFrom((ulong)e.NewValue);
         else
         {
            if ((ulong)e.NewValue > (ulong)e.OldValue)                                                      //if newvalue bigger than old -> we scroll down.
            {
               for (ulong i = 0; i < iDif && i + (ulong)e.OldValue + (ulong)MaxVisible < Brain.Current.NextID - 1; i++)//add to back.
                  CreateExplorerItem((ulong)e.OldValue + (ulong)fItems.Count, fItems.Count);                //use fItems.Count to make certain we always use the correct value.
               for (ulong i = 0; i < iDif; i++)                                                             //remove from front.
                  fItems.RemoveAt(0);
            }
            else
            {
               for (ulong i = 1; i <= iDif; i++)                                                           //add to front
                  CreateExplorerItem((ulong)e.OldValue - i, 0);                                            //i is negative so we go to the front of the list.
               while (fItems.Count > MaxVisible + 1)
                  fItems.RemoveAt(fItems.Count - 1);                                                       //remove from end for as long as needed.
            }
         }
      }

      /// <summary>
      /// Need to make certain that currentScrollValue doesn't go over the limit.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="value"></param>
      /// <returns></returns>
      static object OnCoerceCurrentScrollValue(DependencyObject sender, object value)
      {
         if ((ulong)value < 2)
            return (ulong)2;
         else
         {
            NeuronExplorer iSender = (NeuronExplorer)sender;
            ulong iMaxScrollVal = (ulong)iSender.MaxScrollValue;
            if ((ulong)value > iMaxScrollVal)
               return iMaxScrollVal;
         }
         return value;
      }

      #endregion

      #region MaxScrollValue

      /// <summary>
      /// MaxScrollValue Read-Only Dependency Property
      /// </summary>
      private static readonly DependencyPropertyKey MaxScrollValuePropertyKey
          = DependencyProperty.RegisterReadOnly("MaxScrollValue", typeof(double), typeof(NeuronExplorer),
              new FrameworkPropertyMetadata((double)0.0));

      public static readonly DependencyProperty MaxScrollValueProperty
          = MaxScrollValuePropertyKey.DependencyProperty;

      /// <summary>
      /// Gets the MaxScrollValue property.  This dependency property 
      /// indicates the maximimum scroll value.  This is ItemCount - MaxVisible.
      /// </summary>
      public double MaxScrollValue
      {
         get { return (double)GetValue(MaxScrollValueProperty); }
      }

      /// <summary>
      /// Provides a secure method for setting the MaxScrollValue property.  
      /// This dependency property indicates ....
      /// </summary>
      /// <param name="value">The new value for the property.</param>
      protected void SetMaxScrollValue(double value)
      {
         SetValue(MaxScrollValuePropertyKey, value);
      }

      #endregion

      #region MaxVisible

      /// <summary>
      /// MaxVisible Read-Only Dependency Property
      /// </summary>
      private static readonly DependencyPropertyKey MaxVisiblePropertyKey
          = DependencyProperty.RegisterReadOnly("MaxVisible", typeof(int), typeof(NeuronExplorer),
              new FrameworkPropertyMetadata((int)0,
                  new PropertyChangedCallback(OnMaxVisibleChanged)));

      public static readonly DependencyProperty MaxVisibleProperty
          = MaxVisiblePropertyKey.DependencyProperty;

      /// <summary>
      /// Gets the MaxVisible property.  This dependency property 
      /// indicates the maximum nr of <see cref="ExplorerItem"/>s that can be visible
      /// with the current size.
      /// </summary>
      public int MaxVisible
      {
         get { return (int)GetValue(MaxVisibleProperty); }
      }

      /// <summary>
      /// Provides a secure method for setting the MaxVisible property.  
      /// This dependency property indicates how many items can be visible i
      /// </summary>
      /// <param name="value">The new value for the property.</param>
      protected void SetMaxVisible(int value)
      {
         SetValue(MaxVisiblePropertyKey, value);
      }

      /// <summary>
      /// Handles changes to the MaxVisible property.
      /// </summary>
      private static void OnMaxVisibleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
      {
         ((NeuronExplorer)d).OnMaxVisibleChanged(e);
      }

      /// <summary>
      /// Provides derived classes an opportunity to handle changes to the MaxVisible property.
      /// </summary>
      protected virtual void OnMaxVisibleChanged(DependencyPropertyChangedEventArgs e)
      {
         RenderListFrom(CurrentScrollPos);
         SetMaxScrollValue((double)((ulong)ItemCount - (ulong)(int)e.NewValue));
      }

      #endregion

      #region ItemHeight

      /// <summary>
      /// ItemHeight Dependency Property
      /// </summary>
      public static readonly DependencyProperty ItemHeightProperty =
          DependencyProperty.Register("ItemHeight", typeof(double), typeof(NeuronExplorer),
              new FrameworkPropertyMetadata((double)20,
                  new PropertyChangedCallback(OnItemHeightChanged)));

      /// <summary>
      /// Gets or sets the ItemHeight property.  This dependency property 
      /// indicates the height used by each <see cref="ExplorerItem"/> in the list. 
      /// </summary>
      public double ItemHeight
      {
         get { return (double)GetValue(ItemHeightProperty); }
         set { SetValue(ItemHeightProperty, value); }
      }

      /// <summary>
      /// Handles changes to the ItemHeight property.
      /// </summary>
      private static void OnItemHeightChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
      {
         ((NeuronExplorer)d).OnItemHeightChanged(e);
      }

      /// <summary>
      /// Provides derived classes an opportunity to handle changes to the ItemHeight property.
      /// </summary>
      protected virtual void OnItemHeightChanged(DependencyPropertyChangedEventArgs e)
      {
         SetMaxVisible((int)(LstItems.ActualHeight / ItemHeight));
      }

      #endregion

      #endregion

      #region Functions

      #region helpers

      /// <summary>
      /// Clears the list of items and fills it again starting from the specified id value.  Only
      /// fills for MAXVISIBLE nr of values.  Could be improved by calculating the nr of items actually visible.
      /// </summary>
      /// <param name="start">Id value from where to start filling with neurons.</param>
      private void RenderListFrom(ulong start)
      {
         fItems.Clear();
         for (ulong i = start; i < Brain.Current.NextID && i <= start + (ulong)MaxVisible; i++)
            CreateExplorerItem(i, fItems.Count);
      }

      private void CreateExplorerItem(ulong neuron, int index)
      {
         try
         {
            ExplorerItem iNew;
            if (neuron >= (ulong)PredefinedNeurons.EndOfStatic && neuron < (ulong)PredefinedNeurons.Dynamic)
               iNew = new ReservedExplorerItem(neuron);
            else
            {
               Neuron iFound;
               if (Brain.Current.TryFindNeuron(neuron, out iFound) == true)
                  iNew = new NeuronExplorerItem(iFound);
               else if (Brain.Current.IsDeletedID (neuron) == true)
                  iNew = new FreeExplorerItem(neuron);
               else
                  iNew = new InvalidExplorerItem(neuron, "Neuron not found in brain!");
            }
            fItems.Insert(index, iNew);
         }
         catch (Exception e)
         {
            string iEr = e.ToString();
            Log.LogError("NeuronExplorer.RenderListFrom", iEr);
            InvalidExplorerItem iEmpty = new InvalidExplorerItem(neuron, iEr);
            fItems.Insert(index, iEmpty);
         }
      }

      /// <summary>
      /// Changes the neuron.
      /// </summary>
      /// <param name="oldValue">The old value.</param>
      /// <param name="newValue">The new value.</param>
      private void ChangeNeuron(Neuron oldValue, Neuron newValue)
      {
         ExplorerItem iFound = (from i in fItems where i is NeuronExplorerItem && ((NeuronExplorerItem)i).Item == oldValue select i).FirstOrDefault();
         if (iFound != null)
            fItems[fItems.IndexOf(iFound)] = new NeuronExplorerItem(newValue);
      }

      /// <summary>
      /// Removes the neuron.
      /// </summary>
      /// <param name="neuron">The neuron.</param>
      private void RemoveNeuron(Neuron neuron, ulong id)
      {
         List<int> iFound = ((from i in fItems
                              where i is NeuronExplorerItem && ((NeuronExplorerItem)i).Item == neuron
                              select fItems.IndexOf(i))).ToList();     //we need to explicitly convert to a list cause otherwise we get an error in the 'set new item' loop that we are modify the list inside a loop.
         foreach (int i in iFound)
            fItems[i] = new FreeExplorerItem(id);
         SetValue(NeuronExplorer.ItemCountPropertyKey, Brain.Current.NextID);                                  //when an item is removed, this could be the last, so make certain that the max scroll value is also updated.
      }

      /// <summary>
      /// Inserts the neuron.
      /// </summary>
      /// <param name="neuron">The neuron.</param>
      private void InsertNeuron(Neuron neuron)
      {
         ulong iScrollPos = CurrentScrollPos;
         SetItemCount(Brain.Current.NextID);
         if (neuron.ID >= iScrollPos && neuron.ID < (iScrollPos + (ulong)MaxVisible))
         {
            ExplorerItem iNew = new NeuronExplorerItem(neuron);
            if (neuron.ID == (iScrollPos + (ulong)MaxVisible))
               fItems.Add(iNew);
            else
               fItems[(int)(neuron.ID - iScrollPos)] = iNew;
         }
      } 

      #endregion

      #region Event handlers

      /// <summary>
      /// Handles the NeuronListChanged event of the Brain control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="NeuralNetworkDesigne.HAB.NeuronListChangedEventArgs"/> instance containing the event data.</param>
      void Brain_NeuronListChanged(object sender, NeuronListChangedEventArgs e)
      {
         NeuronExplorerItem iSelected = LstItems.SelectedItem as NeuronExplorerItem;
         if (iSelected != null && IsChildrenLoaded == true &&
            iSelected.Item is NeuronCluster && e.IsListFrom((NeuronCluster)iSelected.Item)== true)
         {
            switch (e.Action)
            {
               case NeuronListChangeAction.Insert:
                  ExplorerItem iToInsert = new NeuronExplorerItem(e.Item.ID);
                  if (iSelected.Children == null)
                     iSelected.TryLoadChildren();
                  else
                     iSelected.Children.Insert(e.Index, iToInsert);
                  break;
               case NeuronListChangeAction.Remove:
                  if (iSelected.Children != null)
                     iSelected.Children.RemoveAt(e.Index);
                  break;
               default:
                  break;
            }
         }
      }


      /// <summary>
      /// Handles the NeuronChanged event of the Brain control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="NeuralNetworkDesigne.HAB.NeuronChangedEventArgs"/> instance containing the event data.</param>
      void Brain_NeuronChanged(object sender, NeuronChangedEventArgs e)
      {
         switch (e.Action)
         {
            case BrainAction.Created:                                                                       //check if we are at end of scroll pos, if so, add it and automatically scroll to the next,  always let itemscounter know it changed.
               InsertNeuron(e.OriginalSource);
               break;
            case BrainAction.Changed:
               if (!(e is NeuronPropChangedEventArgs))                                                      //only need to update if the neuron was changed, not one of it's props.
                  ChangeNeuron(e.OriginalSource, e.NewValue);
               break;
            case BrainAction.Removed:
               RemoveNeuron(e.OriginalSource, e.OriginalSourceID);
               break;
            default:
               break;
         }
      }

      /// <summary>
      /// Handles the Cleared event of the Brain.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
      void Brain_Cleared(object sender, EventArgs e)
      {
         Items.Clear();
      }

      /// <summary>
      /// Handles the Loaded event of the Brain.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
      void Brain_Loaded(object sender, EventArgs e)
      {
         SetValue(NeuronExplorer.ItemCountPropertyKey, Brain.Current.NextID);
         ClearValue(NeuronExplorer.CurrentScrollPosProperty);                                                           //we also clear this value cause we go back to the beginning state.
         RenderListFrom(Neuron.StartId);                                                                                 //fill with all the values.
      }
      

      #region Rename


      private void Rename_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         IsEditing = true;
      }

      private void Rename_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         e.CanExecute = LstItems.SelectedItems.Count > 0;
      }
      /// <summary>
      /// Handles the LostFocus event of the TxtTitle control.
      /// </summary>
      /// <remarks>
      /// When looses focus, need to make certain that is editing is turned off.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void TxtTitle_LostKeybFocus(object sender, KeyboardFocusChangedEventArgs e)
      {
         IsEditing = false;
      }

      /// <summary>
      /// Handles the LostFocus event of the TxtTitle control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void TxtTitle_LostFocus(object sender, RoutedEventArgs e)
      {
         IsEditing = false;
      }



      /// <summary>
      /// Handles the PrvKeyDown event of the TxtTitle control.
      /// </summary>
      /// <remarks>
      /// when enter is pressed, need to stop editing.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
      private void TxtTitle_PrvKeyDown(object sender, KeyEventArgs e)
      {
         if (e.Key == Key.Enter || e.Key == Key.Return)
            IsEditing = false;
      } 
      #endregion

      /// <summary>
      /// Need to make certain mouse wheel scrolls work.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void TheCtrl_MouseWheel(object sender, MouseWheelEventArgs e)
      {
         if (e.Delta > 0)
         {
            if (CurrentScrollPos > 2)                                                           //otherwise the scroll is illegal.
               CurrentScrollPos -= (ulong)(e.Delta / 120);
         }
         else if (CurrentScrollPos + (ulong)MaxVisible <= ItemCount + 1)
            CurrentScrollPos += (ulong)(-e.Delta / 120);
      }


      /// <summary>
      /// Whenever the selection changes, we need to let the new item know it should try to load it's children
      /// and the old item's children should be removed.
      /// </summary>
      private void LstItems_SelectionChanged(object sender, SelectionChangedEventArgs e)
      {
         if (e.RemovedItems.Count > 0)
         {
            NeuronExplorerItem iNew = e.RemovedItems[0] as NeuronExplorerItem;                              //this list only supports single selection.
            if (iNew != null)
               iNew.Children = null;
         }
         if (e.AddedItems.Count > 0)
         {
            NeuronExplorerItem iNew = e.AddedItems[0] as NeuronExplorerItem;                                              //this list only supports single selection.
            if (iNew != null)
               iNew.TryLoadChildren();
         }
      }

      
      /// <summary>
      /// Need to check if there are any items currently displayed that are involved in the change, if so, update.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      void DefaultMeaningIds_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         var iPossibleItems = from i in Items 
                              where i is NeuronExplorerItem && ((NeuronExplorerItem)i).Item != null 
                              select (NeuronExplorerItem)i;                                   //some items, like error items have no item, in those cases, the join crashes because of illegal null reference if we don't filter them out first.
         switch (e.Action)
         {
            case NotifyCollectionChangedAction.Add:
               var iRes = from ulong iId in e.NewItems
                          join NeuronExplorerItem iItem in iPossibleItems on iId equals iItem.Item.ID
                          select iItem;
               foreach (NeuronExplorerItem i in iRes)
                  i.SetDefaultMeaning(true);
               break;
            case NotifyCollectionChangedAction.Remove:
               iRes = from ulong iId in e.OldItems
                      join NeuronExplorerItem iItem in iPossibleItems on iId equals iItem.Item.ID
                      select iItem;
               foreach (NeuronExplorerItem i in iRes)
                  i.SetDefaultMeaning(false);
               break;
            case NotifyCollectionChangedAction.Replace:
               iRes = from ulong iId in e.OldItems
                      join NeuronExplorerItem iItem in iPossibleItems on iId equals iItem.Item.ID
                      select iItem;
               foreach (NeuronExplorerItem i in iRes)
                  i.SetDefaultMeaning(false);
               iRes = from ulong iId in e.NewItems
                      join NeuronExplorerItem iItem in iPossibleItems on iId equals iItem.Item.ID
                      select iItem;
               foreach (NeuronExplorerItem i in iRes)
                  i.SetDefaultMeaning(true);
               break;
            case NotifyCollectionChangedAction.Reset:
               foreach (NeuronExplorerItem i in Items)
                  i.SetDefaultMeaning(false);
               break;
            default:
               break;
         }
      }

      /// <summary>
      /// Handles the KeyDown event of the TxtToSearch control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
      private void TxtToSearch_KeyDown(object sender, KeyEventArgs e)
      {
         if (e.Key == Key.Enter)
            BtnSearch_Click(sender, null);
         else
            fSearchStart = true;                                                                                 //reset the search start cause we have a new word to search
      }

      /// <summary>
      /// Handles the Click event of the BtnSearch control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void BtnSearch_Click(object sender, RoutedEventArgs e)
      {
         ulong iId;

         if (fSearchStart == true)                                                                   //this is the start of the search, so try to find the startpoint neuron
         {
            iId = 2;
            fSearchStart = false;
         }
         else
            iId = fPrevSearched + 1;

         while (Brain.Current.IsExistingID(iId) == false && iId < Brain.Current.NextID)
            iId++;

         if (iId < Brain.Current.NextID)
         {
            ulong iNext = BrainData.Current.NeuronInfo.FindNext(iId, TxtToSearch.Text);
            if (iNext != Neuron.EmptyId)
            {
               SelectedID = iNext;
               fPrevSearched = iNext;
               return;
            }
         }
         MessageBox.Show("End of list reached!");
      }

      #endregion 

      #region overrides

      /// <summary>
      /// need to calculate the maximum nr of visible items.
      /// </summary>
      /// <param name="sizeInfo"></param>
      protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
      {
         base.OnRenderSizeChanged(sizeInfo);
         SetMaxVisible((int)(LstItems.ActualHeight / ItemHeight));
      }


      #endregion

      

      #endregion


      #region IWeakEventListener Members

      /// <summary>
      /// Receives events from the centralized event manager.
      /// </summary>
      /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
      /// <param name="sender">Object that originated the event.</param>
      /// <param name="e">Event data.</param>
      /// <returns>
      /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
      /// </returns>
      public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(NeuronChangedEventManager))
         {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<object, NeuronChangedEventArgs>(Brain_NeuronChanged), sender, (NeuronChangedEventArgs)e);
            return true;
         }
         else if (managerType == typeof(NeuronListChangedEventManager))
         {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<object, NeuronListChangedEventArgs>(Brain_NeuronListChanged), sender, (NeuronListChangedEventArgs)e);
            return true;
         }
         else if (managerType == typeof(CollectionChangedEventManager))
         {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<object, NotifyCollectionChangedEventArgs>(DefaultMeaningIds_CollectionChanged), sender, (NotifyCollectionChangedEventArgs)e);
            return true;
         }
         else if (managerType == typeof(ClearedEventManager))
         {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<object, EventArgs>(Brain_Cleared), sender, (EventArgs)e);
            return true;
         }
         else if (managerType == typeof(LoadedEventManager))
         {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<object, EventArgs>(Brain_Loaded), sender, (EventArgs)e);
            return true;
         }
         else
            return false;
      }

      #endregion

      /// <summary>
      /// Handles the Click event of the MenuItemRemove control.
      /// </summary>
      /// <remarks>
      /// Removes the currently selected neuron in the children list, from the cluster in the items list.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MenuItemRemove_Click(object sender, RoutedEventArgs e)
      {
         NeuronExplorerItem iClusterWrap = LstItems.SelectedItem as NeuronExplorerItem;
         if (iClusterWrap != null)
         {
            NeuronCluster iCluster = iClusterWrap.Item as NeuronCluster;
            if (iCluster != null)
            {
               NeuronExplorerItem iItem = LstChildren.SelectedItem as NeuronExplorerItem;
               if (iItem != null)
               {
                  using (ChildrenAccessor iList = iCluster.ChildrenW)
                     iList.Remove(iItem.Item);
               }
            }
         }
      }
   }
}
