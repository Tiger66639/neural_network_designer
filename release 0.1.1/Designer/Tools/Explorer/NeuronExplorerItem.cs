﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using NeuralNetworkDesigne.LogService;

namespace NeuralNetworkDesigne.HAB.Designer
{
   public class NeuronExplorerItem : ExplorerItem, INeuronInfo, INeuronWrapper
   {

      #region fields

      Neuron fItem;
      NeuronData fItemData;
      bool fIsDefaultMeaning;
      ObservableCollection<ExplorerItem> fChildren;                                                      //possible children of the neuronCluster, only created when loaded.

      #endregion

      public NeuronExplorerItem(ulong id): base(id)
      {
         fItem = Brain.Current[id];
         InternalCreate();
      }


      public NeuronExplorerItem(Neuron toWrap)
         : base(toWrap.ID)
      {
         fItem = toWrap;
         InternalCreate();
      }

      private void InternalCreate()
      {
         if (fItem != null)
         {
            fItemData = BrainData.Current.NeuronInfo[fItem.ID];
            Debug.Assert(fItemData != null);                                                          //should be true -> braindata must always return something.
            fIsDefaultMeaning = BrainData.Current.DefaultMeaningIds.Contains(fItem.ID);
         }
         else
            fIsDefaultMeaning = false;
      }

      #region Item

      /// <summary>
      /// Gets the Neuron item that this object wraps.
      /// </summary>
      /// <remarks>
      /// When null, it indicates there was an error found at the specified index.
      /// </remarks>
      public Neuron Item
      {
         get { return fItem; }
      }

      #endregion


      /// <summary>
      /// Gets a value indicating whether this instance has any code associated with it.
      /// </summary>
      /// <value><c>true</c> if this instance has code; otherwise, <c>false</c>.</value>
      public bool HasCode
      {
         get
         {
            Neuron iItem = Item;
            if (iItem != null)
            {
               if (iItem.RulesCluster != null || iItem.ActionsCluster != null)
                  return true;
               else if (iItem is NeuronCluster && ((NeuronCluster)iItem).Children.Count > 0 && ((NeuronCluster)iItem).Meaning == (ulong)PredefinedNeurons.Code) //access to 'Children' should be thread safe, cause the accessor object disposes itself when out of scope + inits into read.
                  return true;
               else if (iItem is ExpressionsBlock && ((ExpressionsBlock)iItem).StatementsCluster != null)
                  return true;
            }
            return false;
         }
      }

      #region IsDefaultMeaning

      /// <summary>
      /// Gets/sets if this item is in the <see cref="BrainData.DefaultMeanings"/> list.
      /// </summary>
      public bool IsDefaultMeaning
      {
         get
         {
            return fIsDefaultMeaning;
         }
         set
         {
            if (fIsDefaultMeaning != value && fItem != null)
            {
               if (value == true)
                  BrainData.Current.DefaultMeaningIds.Add(fItem.ID);
               else
                  BrainData.Current.DefaultMeaningIds.Remove(fItem.ID);
            }
         }
      }

      /// <summary>
      /// Raises the events and stores the new value.  Isn't done in the setter, cause this is called in response
      /// to changes of the <see cref="BrainData.DefaultMeaningIds"/> list.  The setter only updates the list.
      /// </summary>
      /// <param name="value"></param>
      internal void SetDefaultMeaning(bool value)
      {
         OnPropertyChanging("IsDefaultMeaning", fIsDefaultMeaning, value);
         fIsDefaultMeaning = value;
         OnPropertyChanged("IsDefaultMeaning");
      }

      #endregion

      #region NeuronTypeName

      /// <summary>
      /// Gets the name of the type of neuron we wrap.
      /// </summary>
      /// <remarks>
      /// Used as the tooltip of the image
      /// </remarks>
      public string NeuronTypeName
      {
         get
         {
            if (fItem != null)
               return fItem.GetType().Name;
            else
               return null;
         }
      }

      #endregion


      #region Children

      /// <summary>
      /// Gets the list of children assigned to this  <see cref="NeuronCluster"/> (if we are a wrapper for one and the explorer indicates they should be loaded).
      /// </summary>
      /// <remarks>
      /// This list is not automatically loaded, this is done by the <see cref="NeuronExplorer"/> when this item is selected
      /// through <see cref="ExplorerItem.TryLoadChildren"/>.
      /// </remarks>
      public ObservableCollection<ExplorerItem> Children
      {
         get
         {
            return fChildren;
         }
         internal set
         {
            fChildren = value;
            OnPropertyChanged("Children");
         }
      }

      #endregion

      #region INeuronInfo Members

      public NeuronData NeuronInfo
      {
         get { return fItemData; }
      }

      #endregion


      /// <summary>
      /// Tries to load the child explorer items for this explorer item (if we wrap a neuronCluster).
      /// </summary>
      public void TryLoadChildren()
      {
         try
         {
            NeuronCluster iCluster = fItem as NeuronCluster;
            if (iCluster != null)
            {
               using (ChildrenAccessor iList = iCluster.Children)
               {
                  if (iList.Count > 0)
                  {
                     Children = new ObservableCollection<ExplorerItem>();
                     foreach (ulong i in iList)
                        Children.Add(new NeuronExplorerItem(i));
                  }
               }
            }
         }
         catch (Exception e)
         {
            Log.LogError("ExplorerItem.TryLoadChildren", e.ToString());
         }
      }
   }
}
