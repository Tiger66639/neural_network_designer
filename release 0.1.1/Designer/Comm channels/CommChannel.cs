﻿using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Incapsulates the functionality for a communication channel (<see cref="Sin"/>).
    /// </summary>
    [XmlInclude(typeof(AudioChannel))]
    [XmlInclude(typeof(ImageChannel))]
    [XmlInclude(typeof(TextChannel))]
    [XmlInclude(typeof(WordNetChannel))]
    public abstract class CommChannel : OwnedObject, INeuronInfo, INeuronWrapper
    {
        #region Fields

        private bool fIsVisible;

        //string fName;
        private Sin fSin;

        private NeuronData fItemData;

        private SerializableDictionary<string, object> fDictionary = new SerializableDictionary<string, object>();
        private ObservedCollection<ResourceReference> fResources;

        #endregion Fields

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="CommChannel"/> class.
        /// </summary>
        public CommChannel()
        {
            fResources = new ObservedCollection<ResourceReference>(this);
        }

        #endregion ctor

        #region prop

        #region IsVisible

        /// <summary>
        /// Gets/sets the if this comm channel is curently visible or not.
        /// </summary>
        public bool IsVisible
        {
            get
            {
                return fIsVisible;
            }
            set
            {
                if (fIsVisible != value)
                {
                    OnPropertyChanging("IsVisible", fIsVisible, value);
                    fIsVisible = value;
                    UpdateOpenDocuments();
                    OnPropertyChanged("IsVisible");
                }
            }
        }

        internal void UpdateOpenDocuments()
        {
            if (BrainData.Current != null && BrainData.Current.DesignerData != null)                                  //when designerData is not set, not all the data is loaded yet.
            {
                if (IsVisible == true)
                {
                    WindowMain iMain = (WindowMain)App.Current.MainWindow;
                    if (iMain != null)
                        iMain.AddItemToOpenDocuments(this);
                    else if (BrainData.Current != null)
                        BrainData.Current.OpenDocuments.Add(this);                                                           //this actually shows the item
                }
                else
                    BrainData.Current.OpenDocuments.Remove(this);
            }
        }

        #endregion IsVisible

        #region NeuronID

        /// <summary>
        /// Gets/sets the ID of the <see cref="CommChannel.Sin"/>
        /// </summary>
        /// <remarks>
        /// This prop is primarely for streaming.
        /// </remarks>
        public ulong NeuronID
        {
            get
            {
                if (fSin != null)
                    return fSin.ID;
                else
                    return Neuron.EmptyId;
            }
            set
            {
                if (value != Neuron.EmptyId)
                {
                    if (Brain.Current.NeuronCount > value)
                    {
                        SetSin(Brain.Current[value] as Sin);
                        if (fSin == null)
                            Log.LogError("Communication channel", string.Format("Failed to load neuron with id {0} because it is not a Sin. You probably have a corrupt comm channels file.", value));
                    }
                    else
                        Log.LogError("Communication channel", string.Format("Failed to load neuron with id {0} because it is out of range of the brain's content. You probably have a corrupt comm channels file.", value));
                }
                else
                    SetSin(null);
            }
        }

        /// <summary>
        /// Sets the Sensory interface that this object is a wrapper of.
        /// </summary>
        /// <param name="sin">The sin.</param>
        protected virtual void SetSin(Sin sin)
        {
            fSin = sin;
            if (sin != null && BrainData.Current != null && BrainData.Current.NeuronInfo != null)              //neuroninfo can be null when a project is loaded.  SetSin gets called after the entire project is loaded.
                fItemData = BrainData.Current.NeuronInfo[fSin.ID];
            else
                fItemData = null;
        }

        #endregion NeuronID

        #region Sin

        /// <summary>
        /// Gets the sin that this object incapsulates.
        /// </summary>
        /// <value>The sin.</value>
        public Sin Sin
        {
            get { return fSin; }
        }

        #endregion Sin

        #region Dictionary

        /// <summary>
        /// Gets a dictionary that can be used to store extra info used by visualizers of a communication channel.
        /// For instance, <see cref="TextChannelView"/> uses this to store sub screen sizes.
        /// </summary>
        public SerializableDictionary<string, object> Dictionary
        {
            get { return fDictionary; }
        }

        #endregion Dictionary

        #region Resources

        /// <summary>
        /// Gets the list of resources that are defined for this sin.
        /// </summary>
        public ObservedCollection<ResourceReference> Resources
        {
            get { return fResources; }
        }

        #endregion Resources

        #endregion prop

        #region INeuronWrapper Members

        /// <summary>
        /// Gets the item.
        /// </summary>
        /// <value>The item.</value>
        public Neuron Item
        {
            get { return Sin; }
        }

        #endregion INeuronWrapper Members

        #region INeuronInfo Members

        /// <summary>
        /// Gets the extra info for the specified neuron.  Can be null.
        /// </summary>
        /// <value></value>
        public NeuronData NeuronInfo
        {
            get
            {
                if (fItemData == null && fSin != null)                                     //when a project gets loaded, we can't yet load the NeuronData cause it is not yet accessible, so do this in a delayed fashion.
                    fItemData = BrainData.Current.NeuronInfo[fSin.ID];
                return fItemData;
            }
        }

        #endregion INeuronInfo Members
    }
}