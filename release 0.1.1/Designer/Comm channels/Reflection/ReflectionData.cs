﻿using System;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Base class for all the data objects used by the <see cref="ReflectionChannel"/>
    /// </summary>
    /// <typeparam name="T">The type of the owner.</typeparam>
    abstract public class ReflectionData : OwnedObject
    {
        #region fields

        private bool fIsSelected;
        private string fName;
        private bool fIsExpanded;

        #endregion fields

        #region Prop

        #region IsExpanded

        /// <summary>
        /// Gets/sets the wether the item is expanded or not.  This is for wpf.
        /// </summary>
        public bool IsExpanded
        {
            get
            {
                return fIsExpanded;
            }
            set
            {
                fIsExpanded = value;
                OnPropertyChanged("IsExpanded");
            }
        }

        #endregion IsExpanded

        #region IsSelected

        /// <summary>
        /// Gets/sets if the item is selected or not.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return fIsSelected;
            }
            set
            {
                fIsSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        #endregion IsSelected

        #region Name

        /// <summary>
        /// Gets/sets the name of the object
        /// </summary>
        public string Name
        {
            get
            {
                return fName;
            }
            set
            {
                fName = value;
                OnPropertyChanged("Name");
            }
        }

        #endregion Name

        #region Sin

        /// <summary>
        /// Gets the Reflection Sin for which this data object was created.
        /// </summary>
        /// <value>The sin.</value>
        public ReflectionSin Sin
        {
            get
            {
                Object iOwner = Owner;
                while (iOwner is IOwnedObject && !(iOwner is ReflectionChannel))
                    iOwner = ((IOwnedObject)iOwner).Owner;
                ReflectionChannel iChannel = iOwner as ReflectionChannel;
                if (iChannel != null)
                    return iChannel.Sin as ReflectionSin;
                return null;
            }
        }

        #endregion Sin

        #region IsLoaded

        /// <summary>
        /// Gets or sets the function(s)/children are loaded.
        /// </summary>
        /// <remarks>
        /// Setting to null is not processed.
        /// </remarks>
        /// <value><c>true</c>: all the children are loaded - the function is loaded
        ///        <c>false</c>: none of the children are loaded - the function is not loaded.
        ///        <c>null</c>: some loaded - invalid.
        /// </value>
        abstract public bool? IsLoaded { get; set; }

        #endregion IsLoaded

        #endregion Prop

        #region functions

        /// <summary>
        /// Called by a child to let the owner know that the IsLoaded might have changed. This is recursive for as
        /// long as the parent is a ReflectionData object.
        /// </summary>
        /// <remarks>
        /// This is recursive cause, when a leaf has changed, all parents are effected by this.
        /// </remarks>
        public void OnLoadedChanged()
        {
            OnPropertyChanged("IsLoaded");
            ReflectionData iOwner = Owner as ReflectionData;
            if (iOwner != null)
                iOwner.OnLoadedChanged();
        }

        #endregion functions
    }
}