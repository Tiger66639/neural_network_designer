﻿using System.Reflection;
using System.Windows;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Contains all the data to manage a single function in a <see cref="TypeData"/>.
    /// </summary>
    public class FunctionData : ReflectionData, INeuronInfo, INeuronWrapper
    {
        #region Fields

        private MethodInfo fMethod;
        private Neuron fItem;

        #endregion Fields

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="FunctionData"/> class.
        /// </summary>
        public FunctionData(MethodInfo method)
        {
            Method = method;
        }

        #endregion ctor

        #region Prop

        #region Method

        /// <summary>
        /// Gets the method info object that this object wraps.
        /// </summary>
        public MethodInfo Method
        {
            get { return fMethod; }
            internal set
            {
                fMethod = value;
                if (value != null)
                    Name = value.Name;
                else
                    Name = null;
                ReflectionSin iSin = Sin;
                if (iSin != null)
                {
                    ulong iMap = iSin.GetMethodId(value);
                    if (Brain.Current.IsValidID(iMap) == true)
                        fItem = Brain.Current[iMap];
                    else
                        fItem = null;
                }
                else
                    fItem = null;
                OnPropertyChanged("IsLoaded");
                OnPropertyChanged("Method");
                OnPropertyChanged("Item");
            }
        }

        #endregion Method

        #region IsLoaded

        /// <summary>
        /// Gets or sets the function(s)/children are loaded.
        /// </summary>
        /// <value>
        /// 	<c>true</c>: all the children are loaded - the function is loaded
        /// <c>false</c>: none of the children are loaded - the function is not loaded.
        /// <c>null</c>: some loaded - invalid.
        /// </value>
        public override bool? IsLoaded
        {
            get
            {
                return fItem != null;
            }
            set
            {
                ReflectionSin iSin = Sin;
                if (IsLoaded != value && iSin != null && AskForUnload(fItem) == true)
                {
                    OnPropertyChanging("IsLoaded", IsLoaded, value);
                    if (value == true)
                        fItem = iSin.LoadMethod(Method);
                    else
                    {
                        iSin.UnloadMethod(fItem);
                        Brain.Current.Delete(fItem);                                         //don't delete with generation of undo data, this is done by the prop Changing event of this prop.
                    }
                    OnPropertyChanged("IsLoaded");
                    OnPropertyChanged("Item");
                }
            }
        }

        /// <summary>
        /// Checks if there is an item and if so, checks if there are any links on it, if so, it asks to continue with the unload.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        private bool AskForUnload(Neuron item)
        {
            if (item != null)
            {
                using (NeuronsAccessor iClusteredBy = item.ClusteredBy)
                {
                    using (LinksAccessor iLinksOut = item.LinksOut)
                    {
                        using (LinksAccessor iLinksIn = item.LinksIn)
                        {
                            if (iLinksIn.Items.Count > 0 || iLinksOut.Items.Count > 0 || iClusteredBy.Items.Count > 0)
                            {
                                MessageBoxResult iRes = MessageBox.Show("There are links/clusters referencing this neuron, proceed with unloading?", "Unload neuron", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                                return iRes == MessageBoxResult.Yes;
                            }
                        }
                    }
                }
            }
            return true;                                                                  //unload is allowed cause there is no item or no refs to it.
        }

        #endregion IsLoaded

        #region NeuronInfo (INeuronInfo Members)

        /// <summary>
        /// Gets the extra info for the specified neuron.  Can be null.
        /// </summary>
        /// <value></value>
        public NeuronData NeuronInfo
        {
            get
            {
                if (Item != null)
                    return BrainData.Current.NeuronInfo[Item.ID];
                return null;
            }
        }

        #endregion NeuronInfo (INeuronInfo Members)

        #region Item (INeuronWrapper Members)

        /// <summary>
        /// Gets the item.
        /// </summary>
        /// <value>The item.</value>
        public Neuron Item
        {
            get { return fItem; }
        }

        #endregion Item (INeuronWrapper Members)

        #endregion Prop
    }
}