﻿using System;
using System.Linq;
using System.Reflection;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Contains all the
    /// </summary>
    public class AssemblyData : ReflectionData
    {
        #region Fields

        private ObservedCollection<NamespaceData> fChildren;
        private Assembly fAssembly;

        #endregion Fields

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="AssemblyData"/> class.
        /// </summary>
        /// <param name="name">The name of the assembly to load.</param>
        public AssemblyData(string name)
        {
            fChildren = new ObservedCollection<NamespaceData>(this);
            Assembly iAsm = (from i in AppDomain.CurrentDomain.GetAssemblies()
                             where i.FullName == name
                             select i).FirstOrDefault();
            if (iAsm != null)
                LoadAssembly(iAsm);
            else
                Log.LogError("AssemblyData.Create", string.Format("Failed to find assembly with name: '{0}'.", name));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AssemblyData"/> class.
        /// </summary>
        public AssemblyData()
        {
            fChildren = new ObservedCollection<NamespaceData>(this);
        }

        #endregion ctor

        #region prop

        #region Children

        /// <summary>
        /// Gets the list of all the types found in the namespace
        /// </summary>
        public ObservedCollection<NamespaceData> Children
        {
            get { return fChildren; }
        }

        #endregion Children

        #region IsLoaded

        /// <summary>
        /// Gets or sets the function(s)/children are loaded.
        /// </summary>
        /// <value>
        /// 	<c>true</c>: all the children are loaded - the function is loaded
        /// <c>false</c>: none of the children are loaded - the function is not loaded.
        /// <c>null</c>: some loaded - invalid.
        /// </value>
        /// <remarks>
        /// Setting to null is not processed.
        /// </remarks>
        public override bool? IsLoaded
        {
            get
            {
                if (Children.Count > 0)
                {
                    bool? iRes = Children[0].IsLoaded;
                    for (int i = 1; i < Children.Count; i++)
                    {
                        if (Children[i].IsLoaded != iRes)
                            return null;
                    }
                    return iRes;
                }
                else
                    return null;
            }
            set
            {
                if (value.HasValue == true)
                {
                    foreach (NamespaceData i in Children)
                        i.IsLoaded = value;
                    OnLoadedChanged();
                }
            }
        }

        #endregion IsLoaded

        #region Assembly

        /// <summary>
        /// Gets/sets the assembly that this object wraps.
        /// </summary>
        public Assembly Assembly
        {
            get { return fAssembly; }
            set { LoadAssembly(value); }
        }

        #endregion Assembly

        #endregion prop

        #region Functions

        /// <summary>
        /// Loads all the data for the assembly.
        /// </summary>
        /// <param name="iAsm">The assembly to load.</param>
        private void LoadAssembly(Assembly asm)
        {
            fAssembly = asm;
            var iTypes = from i in asm.GetTypes()
                         where i.IsPublic == true
                         orderby i.Namespace
                         select i;
            NamespaceData iNsData = null;
            foreach (Type i in iTypes)
            {
                if (iNsData == null || iNsData.Name != i.Namespace)                                                    //need to create a new namespace item
                {
                    if (iNsData != null && iNsData.Children.Count > 0)                                                  //we only add if there are actually items in the ns -> there were static public functions in the ns.
                        Children.Add(iNsData);
                    iNsData = new NamespaceData();
                    iNsData.Name = i.Namespace;
                }
                TypeData iType = new TypeData(i);
                if (iType.Children.Count > 0)
                    iNsData.Children.Add(iType);
            }
        }

        #endregion Functions
    }
}