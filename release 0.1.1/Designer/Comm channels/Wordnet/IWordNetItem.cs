﻿using System.Collections.ObjectModel;

namespace NeuralNetworkDesigne.HAB.Designer
{
    public interface IWordNetItemOwner
    {
        /// <summary>
        /// Gets the list of children for this item.
        /// </summary>
        ObservableCollection<WordNetItem> Children { get; }
    }
}