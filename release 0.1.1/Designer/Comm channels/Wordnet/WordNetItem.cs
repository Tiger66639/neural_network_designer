﻿using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Represents a single
    /// </summary>
    public class WordNetItem : OwnedObject<IWordNetItemOwner>, IWordNetItemOwner
    {
        #region fields

        private ObservedCollection<WordNetItem> fChildren;
        private bool fIsExpanded;
        private bool? fIsLoaded;
        private ulong fId;                                                                       //the id of the neuron that we wrap when loaded, used to check if it is deleted.
        private string fText;
        private string fDescription;
        private string fPOS;
        private bool fIsSelected;
        private bool fHasItems;
        private int fSynsetID;

        #endregion fields

        #region ctor

        public WordNetItem()
        {
            fChildren = new ObservedCollection<WordNetItem>(this);
        }

        #endregion ctor

        #region Prop

        #region Root

        /// <summary>
        /// Gets the root of the tree.
        /// </summary>
        /// <value>The root.</value>
        public WordNetChannel Root
        {
            get
            {
                IWordNetItemOwner iCur = Owner;
                while (iCur != null && !(iCur is WordNetChannel))
                    iCur = ((WordNetItem)iCur).Owner;
                return iCur as WordNetChannel;
            }
        }

        #endregion Root

        #region SynsetID

        /// <summary>
        /// Gets the SynSetId as defined in wordnet, for this item, so we can easely find it again in the brain.
        /// </summary>
        [XmlIgnore]
        public int SynsetID
        {
            get { return fSynsetID; }
            internal set { fSynsetID = value; }
        }

        #endregion SynsetID

        #region Children

        /// <summary>
        /// Gets the list of children for this item.
        /// </summary>
        public ObservableCollection<WordNetItem> Children
        {
            get { return fChildren; }
        }

        #endregion Children

        #region IsExpanded

        /// <summary>
        /// Gets/sets wether the children are loaded or not.
        /// </summary>
        public bool IsExpanded
        {
            get
            {
                return fIsExpanded;
            }
            set
            {
                if (value != fIsExpanded && HasItems == true)                                      //can only load stuff if there is something to load.
                {
                    fIsExpanded = value;
                    if (value == true)
                        LoadRelatedWordsFor(Text, SynsetID, Root.SelectedRelationship);
                    else
                        Children.Clear();
                    OnPropertyChanged("IsExpanded");
                }
            }
        }

        #endregion IsExpanded

        #region IsLoaded

        /// <summary>
        /// Gets/sets wether this wordnet item is already loaded in the network.
        /// </summary>
        public bool IsLoaded
        {
            get
            {
                if (fIsLoaded.HasValue == false)                                                 //the first time we try to access the 'isloaded' value, we check the actuall val.
                {
                    Neuron iFound = WordNetSin.Default.FindObject(Text, SynsetID);
                    if (iFound != null)
                        fId = iFound.ID;                                                           //we store the id and not the neuron, cause the object can change, but not the id.
                    else
                        fId = Neuron.EmptyId;
                    fIsLoaded = fId != Neuron.EmptyId;
                }
                return fIsLoaded.Value;
            }
            set
            {
                if (value != fIsLoaded)
                {
                    OnPropertyChanging("IsLoaded", fIsLoaded, value);
                    fIsLoaded = value;
                    if (value == true)
                    {
                        Neuron iNew = WordNetSin.Default.LoadCompact(Text, SynsetID);
                        fId = iNew.ID;
                    }
                    else
                        BrainHelper.DeleteObject((NeuronCluster)Brain.Current[fId]);
                    OnPropertyChanged("IsLoaded");
                }
            }
        }

        #endregion IsLoaded

        #region IsSelected

        /// <summary>
        /// Gets/sets wether the item is selected or not.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return fIsSelected;
            }
            set
            {
                fIsSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        #endregion IsSelected

        #region HasItems

        /// <summary>
        /// Gets the wether the word has any children for the current relationship.
        /// </summary>
        [XmlIgnore]
        public bool HasItems
        {
            get { return fHasItems; }
            internal set { fHasItems = value; }
        }

        #endregion HasItems

        #region Text

        /// <summary>
        /// Gets the text for the wordnet item.
        /// </summary>
        [XmlIgnore]
        public string Text
        {
            get { return fText; }
            internal set { fText = value; }
        }

        #endregion Text

        #region POS

        /// <summary>
        /// Gets the part of speech for the item.
        /// </summary>
        [XmlIgnore]
        public string POS
        {
            get { return fPOS; }
            internal set { fPOS = value; }
        }

        #endregion POS

        #region Description

        /// <summary>
        /// Gets the description for the item.
        /// </summary>
        [XmlIgnore]
        public string Description
        {
            get { return fDescription; }
            internal set { fDescription = value; }
        }

        #endregion Description

        #region DisplayTitle

        /// <summary>
        /// Gets the text to display in the view.
        /// </summary>
        /// <value>The display title.</value>
        public string DisplayTitle
        {
            get
            {
                return Text + "(" + POS + "): " + Description;
            }
        }

        #endregion DisplayTitle

        #endregion Prop

        #region Functions

        /// <summary>
        /// Loads all the children that represent the related words for the specified relationship.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="synsetId">The synset id.</param>
        /// <param name="relationship">The relationship.</param>
        public void LoadRelatedWordsFor(string text, int synsetId, int relationship)
        {
            RelatedWordsTableAdapter iWordsAdapter = new RelatedWordsTableAdapter();
            wordnetDataSet.RelatedWordsDataTable iWordsData = iWordsAdapter.GetData(POS, relationship, synsetId, text);
            foreach (wordnetDataSet.RelatedWordsRow iRow in iWordsData)
            {
                WordNetItem iNew = new WordNetItem();
                iNew.Text = iRow.word;
                iNew.POS = iRow.pos;
                iNew.SynsetID = iRow.synsetid;
                iNew.Description = iRow.definition;
                iNew.HasItems = Root.IsRecursiveRelationship && iWordsAdapter.GetNrOfRelatedWordsFor(iRow.pos, iRow.linkid, iRow.synsetid, iRow.word) > 0 ? true : false;
                fChildren.Add(iNew);
            }
        }

        /// <summary>
        /// Loads all the synonyms for the specified synset (single meaning of a word).
        /// </summary>
        /// <param name="synsetId">The synset id.</param>
        public void LoadSynonymsFor(int synsetId)
        {
            SynonymsTableAdapter iSynonymsAdapter = new SynonymsTableAdapter();
            wordnetDataSet.SynonymsDataTable iSynonymsData = iSynonymsAdapter.GetData(synsetId);
            foreach (wordnetDataSet.SynonymsRow i in iSynonymsData)
            {
                WordNetItem iNew = new WordNetItem();
                iNew.Text = i.word;
                iNew.POS = POS;                                                      //no description: it is the same as ours.
                iNew.SynsetID = i.synsetid;
                iNew.HasItems = false;
                fChildren.Add(iNew);
            }
        }

        #endregion Functions

        /// <summary>
        /// Called when a new object is created, check if this wordnet item represents the object and if so, updates 'IsLoaded'.
        /// Also asks each child to do the same.
        /// </summary>
        /// <param name="p">The p.</param>
        /// <param name="p_2">The P_2.</param>
        internal void UpdateForNewObject(int synsetId, string text)
        {
            if (Text == text && SynsetID == SynsetID)
            {
                fIsLoaded = null;
                OnPropertyChanged("IsLoaded");
            }
            foreach (WordNetItem i in Children)
                i.UpdateForNewObject(synsetId, text);
        }

        /// <summary>
        /// Called when a neuron is deled.
        /// </summary>
        /// <param name="id">The id of the neuron that was deleted.</param>
        internal void UpdateForDelete(ulong id)
        {
            if (fId == id)
            {
                fIsLoaded = false;
                OnPropertyChanged("IsLoaded");
            }
            foreach (WordNetItem i in Children)
                i.UpdateForDelete(id);
        }
    }
}