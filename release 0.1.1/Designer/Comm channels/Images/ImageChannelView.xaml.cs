﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Media;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Interaction logic for ImageChannelView.xaml
    /// </summary>
    public partial class ImageChannelView : UserControl
    {
        public ImageChannelView()
        {
            InitializeComponent();
        }

        private List<Color> fColorList;

        #region Colors

        /// <summary>
        /// Gets the list of available colors.
        /// </summary>
        public List<Color> ColorList
        {
            get
            {
                if (fColorList == null)
                {
                    fColorList = new List<Color>();
                    Type iType = typeof(Colors);
                    foreach (PropertyInfo i in iType.GetProperties())                                      //get all the predifined colors out of the class.
                    {
                        if (i.PropertyType == typeof(Color))
                            fColorList.Add((Color)i.GetValue(null, null));
                    }
                    fColorList.Sort(new Comparison<Color>(CompareColor));
                }
                return fColorList;
            }
        }

        /// <summary>
        /// Compares 2 colors so they can be properly sorted.
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        /// <returns></returns>
        private int CompareColor(Color a, Color b)
        {
            //return (a.R - b.R) + (a.G - b.G) + (a.G - b.G);
            if (a.R == b.R)
            {
                if (a.G == b.G)
                    return a.B - b.B;
                else
                    return a.G - b.G;
            }
            else
                return a.R - b.R;
        }

        #endregion Colors
    }
}