﻿using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// A wrapper class for the <see cref="ImageSin"/>.
    /// </summary>
    public class ImageChannel : CommChannel
    {
        #region fields

        private bool fIsProcessing;
        private bool fAsVideo;
        private RenderTargetBitmap fImage;
        private Visual fRenderSource;

        #endregion fields

        #region IsProcessing

        /// <summary>
        /// Gets the if the sin is currently still processing input data or if it is ready to receive new data.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the sin is processing data; otherwise, <c>false</c>, meaning it is ready to receive new data.
        /// </value>
        [XmlIgnore]
        public bool IsProcessing
        {
            get
            {
                return fIsProcessing;
            }
            private set
            {
                fIsProcessing = value;
                OnPropertyChanged("IsProcessing");
            }
        }

        #endregion IsProcessing

        #region AsVideo

        /// <summary>
        /// Gets/sets the wether input should be processed continuasly like a video (or the eye),
        /// </summary>
        public bool AsVideo
        {
            get
            {
                return fAsVideo;
            }
            set
            {
                fAsVideo = value;
                OnPropertyChanged("AsVideo");
            }
        }

        #endregion AsVideo

        #region Width

        /// <summary>
        /// Gets/sets the width in pixels of the image that is supplied to the brain.
        /// </summary>
        [XmlIgnore]
        public int Width
        {
            get
            {
                return ((ImageSin)Sin).Width;
            }
            set
            {
                ImageSin iSin = (ImageSin)Sin;
                if (iSin.Width != value)
                {
                    OnPropertyChanging("Width", iSin.Width, value);
                    iSin.Width = value;
                    CreateImage();
                    OnPropertyChanged("Width");
                }
            }
        }

        #endregion Width

        #region Height

        /// <summary>
        /// Gets/sets the height, expressed in pixels of the image supplied to the brain
        /// </summary>
        [XmlIgnore]
        public int Height
        {
            get
            {
                return ((ImageSin)Sin).Height;
            }
            set
            {
                ImageSin iSin = (ImageSin)Sin;
                if (value != iSin.Height)
                {
                    OnPropertyChanging("Height", iSin.Height, value);
                    iSin.Height = value;
                    CreateImage();
                    OnPropertyChanged("Height");
                }
            }
        }

        #endregion Height

        #region Image

        /// <summary>
        /// Gets the image object used to transform the input visual (usually an inkcanvas), into a bitmap.
        /// </summary>
        /// <remarks>
        /// This object is cashed so that we don't have to recreate it each time.
        /// </remarks>
        [XmlIgnore]
        public RenderTargetBitmap Image
        {
            get { return fImage; }
            internal set
            {
                fImage = value;
                OnPropertyChanged("Image");
            }
        }

        #endregion Image

        #region RenderSource

        /// <summary>
        /// Gets/sets the visual object that serves as the image source (usually an inkcanvas) to be send to the brain.
        /// </summary>
        [XmlIgnore]
        public Visual RenderSource
        {
            get
            {
                return fRenderSource;
            }
            set
            {
                fRenderSource = value;
            }
        }

        #endregion RenderSource

        #region functions

        /// <summary>
        /// Sets the Sensory interface that this object is a wrapper of.
        /// </summary>
        /// <param name="sin">The sin.</param>
        protected override void SetSin(Sin sin)
        {
            ImageSin iPrev = Sin as ImageSin;                                                                           //remove prev handler
            if (iPrev != null)
                iPrev.IsReady -= new OutputEventHandler(sin_IsReady);
            ImageSin iSin = (ImageSin)sin;
            base.SetSin(sin);
            if (iSin != null)
            {
                iSin.IsReady += new OutputEventHandler(sin_IsReady);
                CreateImage();                                                                                                    //also need to make a new image whenever the sin changes.
            }
            else
                Image = null;
        }

        /// <summary>
        /// Creates the render target image.
        /// </summary>
        /// <remarks>
        /// Both Widht and Height must be bigger than 0, otherwise ther is no bitmap created.
        /// </remarks>
        private void CreateImage()
        {
            ImageSin iSin = (ImageSin)Sin;
            if (iSin.Width > 0 && iSin.Height > 0)
                Image = new RenderTargetBitmap(iSin.Width, iSin.Height, 96.0, 96.0, PixelFormats.Default);
            else
                Image = null;
        }

        /// <summary>
        /// Handles the IsReady event of the sin control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void sin_IsReady(object sender, EventArgs e)
        {
            IsProcessing = false;
            if (AsVideo == true)
                SendImage();
        }

        /// <summary>
        /// Sends the <see cref="ImageChannel.RenderSource"/> to the brain.
        /// </summary>
        public void SendImage()
        {
            if (IsProcessing == false)
            {
                IsProcessing = true;
                ImageSin iSin = (ImageSin)Sin;
                Image.Render(RenderSource);
                iSin.Process(Image, ProcessorManager.Current.GetProcessor());
            }
            else
                throw new InvalidOperationException("Previous image hasn't been processed yet.");
        }

        #endregion functions
    }
}