﻿using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Contains all the data required by the <see cref="TextChannel"/> to keep a link between text values displayed on the screen
    /// and neurons in the brain.
    /// </summary>
    public class TextChanellTrackingData : ObservableObject
    {
        #region Fields

        private Neuron[] fData;
        private string fText;
        private List<DebugNeuron> fDebugData;
        private bool fIsSelected;
        private string fOriginator;

        #endregion Fields

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="TextChanellTrackingData"/> class.
        /// </summary>
        /// <param name="text">The text representation of the data.</param>
        /// <param name="data">The data.</param>
        /// <param name="originator">The originator of the data.</param>
        public TextChanellTrackingData(string text, Neuron data, string originator)
        {
            Text = text;
            Originator = originator;
            Data = new Neuron[] { data };
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TextChanellTrackingData"/> class.
        /// </summary>
        /// <param name="text">The text representation of the data.</param>
        /// <param name="data">The data.</param>
        /// <param name="originator">The originator of the data.</param>
        public TextChanellTrackingData(string text, Neuron[] data, string originator)
        {
            Text = text;
            Originator = originator;
            Data = data;
        }

        #endregion ctor

        #region Prop

        #region Text

        /// <summary>
        /// Gets/sets the text value representation of the data.
        /// </summary>
        public string Text
        {
            get { return fText; }
            internal set { fText = value; }
        }

        #endregion Text

        #region Data

        /// <summary>
        /// Gets/sets the neurons that represent the text block in the brain.
        /// </summary>
        public Neuron[] Data
        {
            get
            {
                return fData;
            }
            set
            {
                fData = value;
                DebugData = new List<DebugNeuron>();
                if (fData != null)
                {
                    foreach (Neuron i in fData)
                        DebugData.Add(new DebugNeuron(i));
                }
            }
        }

        #endregion Data

        #region DebugData

        /// <summary>
        /// Gets the debug representation of the <see cref="TextChannelTrackingData.Data"/>.
        /// </summary>
        public List<DebugNeuron> DebugData
        {
            get { return fDebugData; }
            internal set { fDebugData = value; }
        }

        #endregion DebugData

        #region IsSelected

        /// <summary>
        /// Gets/sets the if this item is selected.
        /// </summary>
        /// <remarks>
        /// By making this raise an event, multiple ui items can bind to it so they become selected at the same time.
        /// </remarks>
        public bool IsSelected
        {
            get
            {
                return fIsSelected;
            }
            set
            {
                fIsSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        #endregion IsSelected

        #region Originator

        /// <summary>
        /// Gets the name of the one who caused the event (the pc, you,...)
        /// </summary>
        public string Originator
        {
            get { return fOriginator; }
            internal set { fOriginator = value; }
        }

        #endregion Originator

        #endregion Prop
    }
}