﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// This class displays the info of a <see cref="TextSin"/> and provides interaction with it.
    /// </summary>
    public partial class TextChannelView : UserControl
    {
        #region ctor-dtor

        public TextChannelView()
        {
            InitializeComponent();
        }

        #endregion ctor-dtor

        #region Prop

        /// <summary>
        /// Gets the <see cref="TextSin"/> that we provide a view for.
        /// </summary>
        /// <remarks>
        /// This is retrieved when the <see cref="TextChannel.Channel"/> property is assigned.
        /// </remarks>
        public TextSin Sin { get; private set; }

        #endregion Prop

        #region functions

        private void BtnSend_Click(object sender, RoutedEventArgs e)
        {
            SendTextToSin();
        }

        private void TxtSend_PrvKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
                SendTextToSin();
        }

        /// <summary>
        /// Tries to send the text in the send text box to the Sin (if there is anything to send).
        /// </summary>
        private void SendTextToSin()
        {
            TextChannel iChannel = (TextChannel)DataContext;
            if (iChannel != null)
                iChannel.SendTextToSin(TxtToSend.Text);
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            TextChannel iChannel = (TextChannel)DataContext;
            if (iChannel != null)
                iChannel.ClearData();
        }

        #endregion functions
    }
}