﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Speech.Synthesis;
using System.Windows.Threading;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// A wrapper class for the <see cref="TextSin"/>. Provides some extra props for visualisation.
    /// </summary>
    public class TextChannel : CommChannel
    {
        #region fields

        private ObservableCollection<TextChanellTrackingData> fDialogData = new ObservableCollection<TextChanellTrackingData>();
        private double fInputSplitterValue;
        private double fDialogSplitterValue;
        private double fNeuronSplitterValue;
        private bool fViewIn;
        private bool fViewOut;
        private SpeechSynthesizer fSpeaker;                                                                        //when set, we need to produce audio out.

        //bool fAudioOn;
        private ObservableCollection<TextChanellTrackingData> fOutputData = new ObservableCollection<TextChanellTrackingData>();

        private ObservableCollection<TextChanellTrackingData> fInputData = new ObservableCollection<TextChanellTrackingData>();

        //DebugNeuron fInputNeuron;
        private TextSinProcessMode? fProcessMode;

        #endregion fields

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="TextChannel"/> class.
        /// </summary>
        public TextChannel()
        {
        }

        #endregion ctor

        #region props

        #region InputSplitterValue

        /// <summary>
        /// Gets/sets the size that should be used for the splitter that controls the input dialog.
        /// </summary>
        public double InputSplitterValue
        {
            get
            {
                return fInputSplitterValue;
            }
            set
            {
                fInputSplitterValue = value;
                OnPropertyChanged("InputSplitterValue");
            }
        }

        #endregion InputSplitterValue

        #region DialogSplitterValue

        /// <summary>
        /// Gets/sets the size that should be used for the splitter that controls the conversation dialog.
        /// </summary>
        public double DialogSplitterValue
        {
            get
            {
                return fDialogSplitterValue;
            }
            set
            {
                fDialogSplitterValue = value;
                OnPropertyChanged("DialogSplitterValue");
            }
        }

        #endregion DialogSplitterValue

        #region NeuronSplitterValue

        /// <summary>
        /// Gets/sets the size that should be used for the splitter that seperates the 2 neuron displays.
        /// </summary>
        public double NeuronSplitterValue
        {
            get
            {
                return fNeuronSplitterValue;
            }
            set
            {
                fNeuronSplitterValue = value;
                OnPropertyChanged("NeuronSplitterValue");
            }
        }

        #endregion NeuronSplitterValue

        #region AudioOn

        /// <summary>
        /// Gets/sets wether oudio output is activated or not.
        /// </summary>
        public bool AudioOn
        {
            get
            {
                return fSpeaker != null;
            }
            set
            {
                if (AudioOn != value)
                {
                    if (value == true)
                        fSpeaker = new SpeechSynthesizer();
                    else
                        fSpeaker = null;
                    OnPropertyChanged("AudioOn");
                }
            }
        }

        #endregion AudioOn

        #region ViewIn

        /// <summary>
        /// Gets/sets if the input node view should be visible.
        /// </summary>
        public bool ViewIn
        {
            get
            {
                return fViewIn;
            }
            set
            {
                fViewIn = value;
                OnPropertyChanged("ViewIn");
            }
        }

        #endregion ViewIn

        #region ViewOut

        /// <summary>
        /// Gets/sets if the output node view should be visible.
        /// </summary>
        public bool ViewOut
        {
            get
            {
                return fViewOut;
            }
            set
            {
                fViewOut = value;
                OnPropertyChanged("ViewOut");
            }
        }

        #endregion ViewOut

        #region OutputData

        /// <summary>
        /// Gets the list of tracking data rendered for output events (text received from the brain).
        /// </summary>
        [XmlIgnore]
        public ObservableCollection<TextChanellTrackingData> OutputData
        {
            get { return fOutputData; }
        }

        #endregion OutputData

        #region InputData

        /// <summary>
        /// Gets the list of tracking data rendered for input events (text sent to the brain).
        /// </summary>
        [XmlIgnore]
        public ObservableCollection<TextChanellTrackingData> InputData
        {
            get { return fInputData; }
        }

        #endregion InputData

        #region DialogData

        /// <summary>
        /// Gets the data containing all the dialog statements.
        /// </summary>
        [XmlIgnore]
        public ObservableCollection<TextChanellTrackingData> DialogData
        {
            get { return fDialogData; }
        }

        #endregion DialogData

        #region ProcessMode

        /// <summary>
        /// Gets/sets the the processing mode that should be used.
        /// </summary>
        public TextSinProcessMode? ProcessMode
        {
            get
            {
                return fProcessMode;
            }
            set
            {
                OnPropertyChanging("ProcessMode", fProcessMode, value);
                fProcessMode = value;
                OnPropertyChanged("ProcessMode");
            }
        }

        #endregion ProcessMode

        #endregion props

        #region functions

        /// <summary>
        /// Sets the Sensory interface that this object is a wrapper of.
        /// </summary>
        /// <param name="sin">The sin.</param>
        protected override void SetSin(Sin sin)
        {
            TextSin iSin = sin as TextSin;
            if (iSin != null)
                iSin.TextOut += new OutputEventHandler<string>(Sin_TextOut);
            base.SetSin(sin);
        }

        /// <summary>
        /// Handles the TextOut event of the Sin control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="NeuralNetworkDesigne.HAB.OutputEventArgs&lt;System.String&gt;"/> instance containing the event data.</param>
        private void Sin_TextOut(object sender, OutputEventArgs<string> e)
        {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<OutputEventArgs<string>>(HandleIncommingData), e);
        }

        private void HandleIncommingData(OutputEventArgs<string> e)
        {
            TextChanellTrackingData iData = new TextChanellTrackingData(e.Value, e.Data.ToArray(), "PC");
            OutputData.Add(iData);
            DialogData.Add(iData);
            if (fSpeaker != null)
                fSpeaker.SpeakAsync(e.Value);
        }

        #endregion functions

        /// <summary>
        /// Sends the text to sin.
        /// </summary>
        /// <param name="value">The value.</param>
        internal void SendTextToSin(string value)
        {
            if (string.IsNullOrEmpty(value) == false)
            {
                TextSin iSin = (TextSin)Sin;
                if (iSin != null)
                {
                    Neuron[] iRes;
                    if (ProcessMode.HasValue == false)
                        iRes = iSin.Process(value, ProcessorManager.Current.GetProcessor());
                    else
                        iRes = iSin.Process(value, ProcessorManager.Current.GetProcessor(), ProcessMode.Value);
                    if (iRes != null)
                    {
                        TextChanellTrackingData iData = new TextChanellTrackingData(value, iRes, "You");
                        InputData.Add(iData);
                        DialogData.Add(iData);
                    }
                    else
                        Log.LogError("TextChannel.SendText", "Sin failed to return a neuron for the output text!");
                }
            }
        }

        /// <summary>
        /// Clears all the data from the channel.
        /// </summary>
        internal void ClearData()
        {
            OutputData.Clear();
            InputData.Clear();
            DialogData.Clear();
        }
    }
}