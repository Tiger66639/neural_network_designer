﻿using System;
using System.Windows.Media.Imaging;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// A <see cref="ResourceReference"/> for the <see cref="ImageChannel"/>.
    /// </summary>
    public class ImageReference : ResourceReference
    {
        #region Image

        /// <summary>
        /// Gets the image as a bitmapresource.
        /// </summary>
        public BitmapSource Image
        {
            get
            {
                string iPath = FileName;
                if (string.IsNullOrEmpty(iPath) == false)
                    return new BitmapImage(new Uri(iPath));
                else
                    return null;
            }
        }

        #endregion Image
    }
}