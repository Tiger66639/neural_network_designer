﻿namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// A <see cref="ResourceReference"/> for the <see cref="AudioChannel"/>.
    /// </summary>
    public class AudioReference : ResourceReference
    {
    }
}