﻿using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Stores the path and name of a file that can be used as training data for the brain.
    /// </summary>
    [XmlInclude(typeof(ImageReference))]
    [XmlInclude(typeof(AudioReference))]
    public class ResourceReference : OwnedObject
    {
        private string fFileName;

        #region FileName

        /// <summary>
        /// Gets/sets the name and path of the file.
        /// </summary>
        public string FileName
        {
            get
            {
                return fFileName;
            }
            set
            {
                fFileName = value;
                OnPropertyChanged("FileName");
            }
        }

        #endregion FileName
    }
}