﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// The event manager for the weak event pattern used for the <see cref="BrainData.AfterLoad"/> event.
   /// </summary>
   /// <remarks>
   /// use this class to prevent mem leaks.
   /// </remarks>
   public class AfterLoadEventManager : WeakEventManager
   {
      /// <summary>
      /// Gets the current manager.
      /// </summary>
      /// <value>The current manager.</value>
      private static AfterLoadEventManager CurrentManager
      {
         get
         {
            Type iType = typeof(AfterLoadEventManager);
            AfterLoadEventManager currentManager = (AfterLoadEventManager)WeakEventManager.GetCurrentManager(iType);
            if (currentManager == null)
            {
               currentManager = new AfterLoadEventManager();
               WeakEventManager.SetCurrentManager(iType, currentManager);
            }
            return currentManager;
         }
      }


      /// <summary>
      /// Adds the listener.
      /// </summary>
      /// <param name="source">The source.</param>
      /// <param name="listener">The listener.</param>
      public static void AddListener(BrainData source, IWeakEventListener listener)
      {
         if (Environment.HasShutdownStarted == false)
            CurrentManager.ProtectedAddListener(source, listener);
      }

      /// <summary>
      /// Removes the listener.
      /// </summary>
      /// <param name="source">The source.</param>
      /// <param name="listener">The listener.</param>
      public static void RemoveListener(BrainData source, IWeakEventListener listener)
      {
         if (Environment.HasShutdownStarted == false)
            CurrentManager.ProtectedRemoveListener(source, listener);
      }


      /// <summary>
      /// When overridden in a derived class, starts listening for the event being managed. After <see cref="M:System.Windows.WeakEventManager.StartListening(System.Object)"/>  is first called, the manager should be in the state of calling <see cref="M:System.Windows.WeakEventManager.DeliverEvent(System.Object,System.EventArgs)"/> or <see cref="M:System.Windows.WeakEventManager.DeliverEventToList(System.Object,System.EventArgs,System.Windows.WeakEventManager.ListenerList)"/> whenever the relevant event from the provided source is handled.
      /// </summary>
      /// <param name="source">The source to begin listening on.</param>
      protected override void StartListening(object source)
      {
         BrainData iSource = (BrainData)source;
         iSource.AfterLoad += new EventHandler(Do_AfterLoad);
      }

      /// <summary>
      /// Handles the AfterLoad event of the Do control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
      void Do_AfterLoad(object sender, EventArgs e)
      {
         base.DeliverEvent(sender, e);
      }


      /// <summary>
      /// When overridden in a derived class, stops listening on the provided source for the event being managed.
      /// </summary>
      /// <param name="source">The source to stop listening on.</param>
      protected override void StopListening(object source)
      {
         BrainData iSource = (BrainData)source;
         iSource.AfterLoad -= new EventHandler(Do_AfterLoad);
      }
   }
}
