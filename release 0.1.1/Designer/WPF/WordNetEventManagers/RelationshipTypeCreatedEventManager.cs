﻿using System;
using System.Windows;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// The event manager for the weak event pattern used for the <see cref="WordNetSin.RelationshipTypeCreated"/> event.
   /// </summary>
   /// <remarks>
   /// use this class to prevent mem leaks.
   /// </remarks>
   public class RelationshipTypeCreatedEventManager: WeakEventManager
   {
      /// <summary>
      /// Gets the current manager.
      /// </summary>
      /// <value>The current manager.</value>
      private static RelationshipTypeCreatedEventManager CurrentManager
      {
         get
         {
            Type iType = typeof(RelationshipTypeCreatedEventManager);
            RelationshipTypeCreatedEventManager currentManager = (RelationshipTypeCreatedEventManager)WeakEventManager.GetCurrentManager(iType);
            if (currentManager == null)
            {
               currentManager = new RelationshipTypeCreatedEventManager();
               WeakEventManager.SetCurrentManager(iType, currentManager);
            }
            return currentManager;
         }
      }



      /// <summary>
      /// Adds the listener.
      /// </summary>
      /// <param name="sin">The <see cref="WordNetSin"/> to connect to. </param>
      /// <param name="listener">The listener.</param>
      public static void AddListener(WordNetSin sin, IWeakEventListener listener)
      {
         if (Environment.HasShutdownStarted == false)
            CurrentManager.ProtectedAddListener(sin, listener);
      }

      /// <summary>
      /// Removes the listener.
      /// </summary>
      /// <param name="sin">The <see cref="WordNetSin"/> to connect to. </param>
      /// <param name="listener">The listener.</param>
      public static void RemoveListener(WordNetSin sin, IWeakEventListener listener)
      {
         if (Environment.HasShutdownStarted == false)
            CurrentManager.ProtectedRemoveListener(sin, listener);
      }


      /// <summary>
      /// When overridden in a derived class, starts listening for the event being managed. After <see cref="M:System.Windows.WeakEventManager.StartListening(System.Object)"/>  is first called, the manager should be in the state of calling <see cref="M:System.Windows.WeakEventManager.DeliverEvent(System.Object,System.EventArgs)"/> or <see cref="M:System.Windows.WeakEventManager.DeliverEventToList(System.Object,System.EventArgs,System.Windows.WeakEventManager.ListenerList)"/> whenever the relevant event from the provided source is handled.
      /// </summary>
      /// <param name="source">The source to begin listening on.</param>
      protected override void StartListening(object source)
      {
         WordNetSin iSource = (WordNetSin)source;
         iSource.RelationshipTypeCreated += new NewRelationshipTypeEventHandler(RelationshipTypeCreated);
      }

      /// <summary>
      /// handles the event
      /// </summary>
      /// <param name="sender">The sender.</param>
      /// <param name="e">The <see cref="NeuralNetworkDesigne.HAB.NewRelationshipTypeEventArgs"/> instance containing the event data.</param>
      void RelationshipTypeCreated(object sender, NewRelationshipTypeEventArgs e)
      {
         base.DeliverEvent(sender, e);
      }


      /// <summary>
      /// When overridden in a derived class, stops listening on the provided source for the event being managed.
      /// </summary>
      /// <param name="source">The source to stop listening on.</param>
      protected override void StopListening(object source)
      {
         WordNetSin iSource = (WordNetSin)source;
         iSource.RelationshipTypeCreated -= new NewRelationshipTypeEventHandler(RelationshipTypeCreated);
      }
   }
}
