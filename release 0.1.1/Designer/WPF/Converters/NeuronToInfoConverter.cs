﻿using System;
using System.Windows.Data;

namespace NeuralNetworkDesigne.HAB.Designer
{

   /// <summary>
   /// Converts a neuron or it's id to it's <see cref="NeuronData"/> object so we can display a proper title + get warned when it is changed.
   /// </summary>
   /// <remarks>
   /// <para>
   /// This converter works in 2 ways.
   /// </para>
   /// <para>
   /// Major defect: ToString of <see cref="NeuronData"/> return the displayTitle.  When this is updated, UI's using this converter
   /// aren't updated, so only use in situations where no display names can be changed.
   /// </para>
   /// </remarks>
   class NeuronToInfoConverter : IValueConverter
   {
      #region IValueConverter Members

      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         Neuron iVal = value as Neuron;
         if (iVal != null)
            return BrainData.Current.NeuronInfo[iVal.ID];
         return value;
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         NeuronData iVal = value as NeuronData;
         if (iVal != null)
            return Brain.Current[iVal.ID];
         return value;
      }

      #endregion
   }
}
