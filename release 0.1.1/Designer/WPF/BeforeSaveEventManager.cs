﻿using System;
using System.Windows;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// The event manager for the weak event pattern used for the <see cref="BrainData.BeforeSave"/> event.
   /// </summary>
   /// <remarks>
   /// use this class to prevent mem leaks.
   /// </remarks>
   public class BeforeSaveEventManager: WeakEventManager
   {

      /// <summary>
      /// Gets the current manager.
      /// </summary>
      /// <value>The current manager.</value>
      private static BeforeSaveEventManager CurrentManager
      {
         get
         {
            Type iType = typeof(BeforeSaveEventManager);
            BeforeSaveEventManager currentManager = (BeforeSaveEventManager)WeakEventManager.GetCurrentManager(iType);
            if (currentManager == null)
            {
               currentManager = new BeforeSaveEventManager();
               WeakEventManager.SetCurrentManager(iType, currentManager);
            }
            return currentManager;
         }
      }


      /// <summary>
      /// Adds the listener.
      /// </summary>
      /// <param name="source">The source.</param>
      /// <param name="listener">The listener.</param>
      public static void AddListener(BrainData source, IWeakEventListener listener)
      {
         if (Environment.HasShutdownStarted == false)
            CurrentManager.ProtectedAddListener(source, listener);
      }

      /// <summary>
      /// Removes the listener.
      /// </summary>
      /// <param name="source">The source.</param>
      /// <param name="listener">The listener.</param>
      public static void RemoveListener(BrainData source, IWeakEventListener listener)
      {
         if (Environment.HasShutdownStarted == false)
            CurrentManager.ProtectedRemoveListener(source, listener);
      }


      /// <summary>
      /// When overridden in a derived class, starts listening for the event being managed. After <see cref="M:System.Windows.WeakEventManager.StartListening(System.Object)"/>  is first called, the manager should be in the state of calling <see cref="M:System.Windows.WeakEventManager.DeliverEvent(System.Object,System.EventArgs)"/> or <see cref="M:System.Windows.WeakEventManager.DeliverEventToList(System.Object,System.EventArgs,System.Windows.WeakEventManager.ListenerList)"/> whenever the relevant event from the provided source is handled.
      /// </summary>
      /// <param name="source">The source to begin listening on.</param>
      protected override void StartListening(object source)
      {
         BrainData iSource = (BrainData)source;
         iSource.BeforeSave += new EventHandler(DoBeforeSave);
      }

      /// <summary>
      /// Does the before save.
      /// </summary>
      /// <param name="sender">The sender.</param>
      /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
      void DoBeforeSave(object sender, EventArgs e)
      {
         base.DeliverEvent(sender, e);
      }


      /// <summary>
      /// When overridden in a derived class, stops listening on the provided source for the event being managed.
      /// </summary>
      /// <param name="source">The source to stop listening on.</param>
      protected override void StopListening(object source)
      {
         BrainData iSource = (BrainData)source;
         iSource.BeforeSave -= new EventHandler(DoBeforeSave);
      }

   }
}
