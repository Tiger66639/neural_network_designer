﻿using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Controls;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Interaction logic for WindowOptions.xaml
    /// </summary>
    public partial class WindowOptions : Window
    {
        public WindowOptions()
        {
            InitializeComponent();
            TxtStackSize.Text = Settings.InitProcessorStackSize.ToString();
            TxtTemplateFile.Text = Properties.Settings.Default.DefaultTemplatePath;
            CheckAutoSave.IsChecked = Properties.Settings.Default.AutoSave;
            TxtAutoSaveInterval.Text = Properties.Settings.Default.AutoSaveInterval.Minutes.ToString();
            TxtSandboxPath.Text = ProjectManager.Default.SandboxLocation;
            TxtFrameNetPath.Text = Properties.Settings.Default.FrameNetPath;
        }

        private void OnClickMore(object sender, RoutedEventArgs e)
        {
            Button iSender = e.OriginalSource as Button;

            if (iSender != null)
            {
                TextBox iText = iSender.Tag as TextBox;
                if (iText != null)
                {
                    System.Windows.Forms.FolderBrowserDialog iDialog = new System.Windows.Forms.FolderBrowserDialog();

                    iDialog.Description = iText.Tag as string;
                    iDialog.SelectedPath = iText.Text;
                    System.Windows.Forms.DialogResult iRes = iDialog.ShowDialog();
                    if (iRes == System.Windows.Forms.DialogResult.OK)
                        iText.Text = iDialog.SelectedPath;
                }
            }
        }

        private void OnClickOk(object aSender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        /// <summary>
        /// update all bindings + save all values back to the static fields (to which you can't bind).
        /// If there is an invalid value somewhere, cancel the close operation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (DialogResult == true)
            {
                if (int.TryParse(TxtStackSize.Text, out Settings.InitProcessorStackSize) == false)
                {
                    MessageBox.Show("Please provide a valid integer for 'Initial stack size'!");
                    DialogResult = false;
                    e.Cancel = true;
                    return;
                }

                int iMinutes;
                if (int.TryParse(TxtAutoSaveInterval.Text, out iMinutes) == false)
                {
                    MessageBox.Show("Please provide a valid integer for 'auto save time interval'!");
                    DialogResult = false;
                    e.Cancel = true;
                    return;
                }

                ProjectManager.Default.SandboxLocation = TxtSandboxPath.Text;
                Properties.Settings.Default.DefaultTemplatePath = TxtTemplateFile.Text;
                Properties.Settings.Default.FrameNetPath = TxtFrameNetPath.Text;

                Properties.Settings.Default.AutoSave = (CheckAutoSave.IsChecked.HasValue == true && CheckAutoSave.IsChecked == true);
                Properties.Settings.Default.AutoSaveInterval = new TimeSpan(0, iMinutes, 0);
                ((App)App.Current).UpdateAutoSave();
            }
        }

        /// <summary>
        /// Handles the Click event of the BtnFrameNetPath control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void BtnTemplateFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog iDlg = new OpenFileDialog();
            iDlg.Filter = "Xml files(*.xml)|*.xml|All(*.*)|*.*";
            iDlg.FileName = TxtTemplateFile.Text;
            iDlg.Multiselect = false;
            bool? iRes = iDlg.ShowDialog(this);
            if (iRes.HasValue == true && iRes.Value == true)
                TxtTemplateFile.Text = iDlg.FileName;
        }
    }
}