﻿using System;
using System.Windows;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Interaction logic for DlgStringQuestion.xaml
    /// </summary>
    public partial class DlgStringQuestion : Window
    {
        public DlgStringQuestion()
        {
            InitializeComponent();
        }

        #region Question

        /// <summary>
        /// Identifies the Question Dependency property.
        /// </summary>
        public static readonly DependencyProperty QuestionProperty =
              DependencyProperty.Register("Question", typeof(string), typeof(DlgStringQuestion), new FrameworkPropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the Question.
        /// </summary>
        public string Question
        {
            get { return (string)GetValue(QuestionProperty); }
            set { SetValue(QuestionProperty, value); }
        }

        #endregion Question

        #region Answer

        /// <summary>
        /// Identifies the Answer Dependency property.
        /// </summary>
        public static readonly DependencyProperty AnswerProperty =
              DependencyProperty.Register("Answer", typeof(string), typeof(DlgStringQuestion), new FrameworkPropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the Question.
        /// </summary>
        public string Answer
        {
            get { return (string)GetValue(AnswerProperty); }
            set { SetValue(AnswerProperty, value); }
        }

        #endregion Answer

        private void OnClickOk(object aSender, EventArgs e)
        {
            DialogResult = true;
        }
    }
}