﻿using System.Collections.Generic;
using System.Windows;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Interaction logic for DlgInspectExpression.xaml
    /// </summary>
    public partial class DlgInspectExpression : Window
    {
        private List<DebugNeuron> fItems;
        private DebugNeuron fExpression;

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="DlgInspectExpression"/> class.
        /// </summary>
        public DlgInspectExpression(ResultExpression expression, List<DebugNeuron> items)
        {
            Expression = new DebugNeuron(expression);
            Items = items;
            InitializeComponent();
        }

        #endregion ctor

        #region Items

        /// <summary>
        /// Gets the list of items that were returned by the expression.
        /// </summary>
        public List<DebugNeuron> Items
        {
            get { return fItems; }
            internal set { fItems = value; }
        }

        #endregion Items

        #region Expression

        /// <summary>
        /// Gets the expression for whom we display all the data.
        /// </summary>
        public DebugNeuron Expression
        {
            get { return fExpression; }
            internal set { fExpression = value; }
        }

        #endregion Expression
    }
}