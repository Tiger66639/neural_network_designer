﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Threading;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Displays a dialog that allows the user to edit the part of speech value assigned to all the object clusters
    /// in the brain.
    /// </summary>
    /// <remarks>
    /// When this dialog is opened, a snap shot of the brain is taken.
    /// </remarks>
    public partial class DlgPOSEditor : Window
    {
        #region inner types

        private class ObjectItem : ObservableObject
        {
            #region Fields

            private Neuron fPOS;
            private string fDisplayTitle;

            #endregion Fields

            /// <summary>
            /// Gets or sets the cluster being wrapped.
            /// </summary>
            /// <value>The cluster.</value>
            public NeuronCluster Cluster { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this instance is changed.
            /// </summary>
            /// <value>
            /// 	<c>true</c> if this instance is changed; otherwise, <c>false</c>.
            /// </value>
            public bool IsChanged { get; set; }

            #region DisplayTitle

            /// <summary>
            /// Gets/sets the display title of the item.
            /// </summary>
            /// <remarks>
            /// Since this is a snapshot, we don't need direct linke to NeuronInfo.displayTitle, instead, we need to update.
            /// IsCHanged.
            /// </remarks>
            public string DisplayTitle
            {
                get
                {
                    if (fDisplayTitle == null)
                        fDisplayTitle = BrainData.Current.NeuronInfo[Cluster.ID].DisplayTitle;
                    return fDisplayTitle;
                }
                set
                {
                    fDisplayTitle = value;
                    OnPropertyChanged("DisplayTitle");
                    IsChanged = true;
                }
            }

            #endregion DisplayTitle

            #region POS

            /// <summary>
            /// Gets/sets the type of grammar assigned to the neuron wrapped by this object.
            /// </summary>
            public Neuron POS
            {
                get
                {
                    return fPOS;
                }
                set
                {
                    fPOS = value;
                    OnPropertyChanged("POS");
                    IsChanged = true;
                }
            }

            #endregion POS

            public override string ToString()
            {
                return DisplayTitle;
            }
        }

        #endregion inner types

        public DlgPOSEditor()
        {
            InitializeComponent();
            Action iLoadData = new Action(LoadData);
            iLoadData.BeginInvoke(null, null);
        }

        /// <summary>
        /// Called when [click ok].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void OnClickOk(object sender, RoutedEventArgs e)
        {
            List<ObjectItem> iList = (List<ObjectItem>)DataContext;
            Debug.Assert(iList != null);
            var iToChange = from i in iList where i.IsChanged == true select i;
            foreach (ObjectItem i in iToChange)
            {
                i.Cluster.SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.POS, i.POS);
                BrainData.Current.NeuronInfo[i.Cluster.ID].DisplayTitle = i.DisplayTitle;
            }
            DialogResult = true;
        }

        /// <summary>
        /// Loads all the object clusters into a temp structure that can easely be edited.
        /// </summary>
        /// <remarks>
        /// We deliberatly store a ref to the neurons, but don't provide updates to this temp list since we want to make
        /// a snapsshot of the state, as it was when the dialog was opened.
        /// </remarks>
        private void LoadData()
        {
            List<ObjectItem> iRes = new List<ObjectItem>();
            for (ulong i = 0; i < Brain.Current.NextID; i++)
            {
                NeuronCluster iCluster;
                Neuron iFound;
                if (Brain.Current.TryFindNeuron(i, out iFound) == true)
                {
                    iCluster = iFound as NeuronCluster;
                    if (iCluster != null && iCluster.Meaning == (ulong)PredefinedNeurons.Object)
                    {
                        ObjectItem iNew = new ObjectItem();
                        iNew.Cluster = iCluster;
                        iNew.POS = iCluster.FindFirstOut((ulong)PredefinedNeurons.POS);
                        iRes.Add(iNew);
                    }
                }
            }
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<object>(SetDataContext), iRes);         //need to pass the result back to the main thread.
        }

        private void SetDataContext(object values)
        {
            DataContext = values;
        }
    }
}