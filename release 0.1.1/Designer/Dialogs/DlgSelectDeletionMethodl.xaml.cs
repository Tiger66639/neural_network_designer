﻿using System;
using System.Windows;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Interaction logic for DlgDeleteSpecial.xaml
    /// </summary>
    public partial class DlgSelectDeletionMethod : Window
    {
        private DeletionMethod fNeuronDeletionMethod = DeletionMethod.Delete;
        private DeletionMethod fBranchHandling = DeletionMethod.Nothing;

        public DlgSelectDeletionMethod()
        {
            InitializeComponent();
        }

        #region NeuronDeletionMethod

        /// <summary>
        /// Gets the deletion method that should be applied to the selected items (Default is delete permantly).
        /// </summary>
        public DeletionMethod NeuronDeletionMethod
        {
            get { return fNeuronDeletionMethod; }
            internal set { fNeuronDeletionMethod = value; }
        }

        #endregion NeuronDeletionMethod

        #region BranchHandling

        /// <summary>
        /// Gets the way that the branches of the deleted neurons should be handled (default is nothing).
        /// </summary>
        public DeletionMethod BranchHandling
        {
            get { return fBranchHandling; }
            internal set { fBranchHandling = value; }
        }

        #endregion BranchHandling

        private void SubGroup_Checked(object sender, RoutedEventArgs e)
        {
            if (sender == BtnNothing)
                BranchHandling = DeletionMethod.Nothing;
            else if (sender == BtnSubDeleteNoRef)
                BranchHandling = DeletionMethod.DeleteIfNoRef;
            else if (sender == BtnSubDelete)
                BranchHandling = DeletionMethod.Delete;
            else
                throw new InvalidOperationException();
        }

        private void DeleteGroup_Checked(object sender, RoutedEventArgs e)
        {
            if (GrpSub != null)
                GrpSub.IsEnabled = true;
            if (sender == BtnRemove)
            {
                GrpSub.IsEnabled = false;
                NeuronDeletionMethod = DeletionMethod.Remove;
            }
            else if (sender == BtnDeleteNoRef)
                NeuronDeletionMethod = DeletionMethod.DeleteIfNoRef;
            else if (sender == BtnDelete)
                NeuronDeletionMethod = DeletionMethod.Delete;
            else
                throw new InvalidOperationException();
        }

        private void OnClickOk(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}