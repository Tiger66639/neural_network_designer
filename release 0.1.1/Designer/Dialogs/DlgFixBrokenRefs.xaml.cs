﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Threading;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Interaction logic for DlgFixBrokenRefs.xaml
    /// </summary>
    public partial class DlgFixBrokenRefs : Window
    {
        #region Fields

        private ObservableCollection<string> fLogItems = new ObservableCollection<string>();
        private bool fStopProcessing = false;                                                                   //switch used to stop the processing.

        #endregion Fields

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="DlgFixBrokenRefs"/> class.
        /// </summary>
        public DlgFixBrokenRefs()
        {
            InitializeComponent();
            CurrentPos = 0;
            Maximum = Brain.Current.NextID;
        }

        #endregion ctor

        #region Prop

        #region CurrentPos

        /// <summary>
        /// CurrentPos Dependency Property
        /// </summary>
        public static readonly DependencyProperty CurrentPosProperty =
            DependencyProperty.Register("CurrentPos", typeof(ulong), typeof(DlgFixBrokenRefs),
                new FrameworkPropertyMetadata((ulong)0));

        /// <summary>
        /// Gets or sets the CurrentPos property.  This dependency property
        /// indicates the id of the current neuron being processed.
        /// </summary>
        public ulong CurrentPos
        {
            get { return (ulong)GetValue(CurrentPosProperty); }
            set { SetValue(CurrentPosProperty, value); }
        }

        #endregion CurrentPos

        #region Maximum

        /// <summary>
        /// Maximum Dependency Property
        /// </summary>
        public static readonly DependencyProperty MaximumProperty =
            DependencyProperty.Register("Maximum", typeof(ulong), typeof(DlgFixBrokenRefs),
                new FrameworkPropertyMetadata((ulong)0));

        /// <summary>
        /// Gets or sets the Maximum property.  This dependency property
        /// indicates how many neurons need to be processed in total.
        /// </summary>
        public ulong Maximum
        {
            get { return (ulong)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }

        #endregion Maximum

        #region LogItems

        /// <summary>
        /// Gets the list of log items that need to shown to the user.
        /// </summary>
        public ObservableCollection<string> LogItems
        {
            get { return fLogItems; }
        }

        #endregion LogItems

        #endregion Prop

        private void OnClickStart(object sender, RoutedEventArgs e)
        {
            if (BtnStartStop.Tag == null)
            {
                LogItems.Clear();
                CurrentPos = 0;
                BtnStartStop.Content = "Stop";
                BtnStartStop.Tag = this;                                                               //we use a pointer, this is faster to check instead of boxing the val
                BtnStartStop.ToolTip = "Stop the process.";
                BtnClose.IsEnabled = false;
                Action iThread = new Action(ProcessItems);
                AsyncCallback iDoWhenDone = new AsyncCallback(ProcessFinished);
                IAsyncResult iAsync = iThread.BeginInvoke(iDoWhenDone, null);
            }
            else
                fStopProcessing = true;
        }

        /// <summary>
        /// Processes all the neurons. This allows us to call this function async.
        /// </summary>
        private void ProcessItems()
        {
            for (ulong i = Neuron.StartId; i < Brain.Current.NextID; i++)
            {
                try
                {
                    if (fStopProcessing == true)                                                                    //check if a stop was requested.
                        break;
                    Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action<ulong>(SetCurPos), i);              //we use a high priority so that it will always get updated.
                    Neuron iFound;
                    if (Brain.Current.TryFindNeuron(i, out iFound) == true)                                         //the neuron exists, so check it is ok.
                        CheckNeuron(iFound);
                }
                catch (Exception e)
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("failed to clean '{0}', error: {1}.", i, e.ToString()));
                }
            }
        }

        /// <summary>
        /// Checks all the data of the neuron.
        /// </summary>
        /// <param name="toCheck">The neuron to check.</param>
        private void CheckNeuron(Neuron toCheck)
        {
            using (LinksAccessor iLinksIn = toCheck.LinksIn)
                CheckLinks(iLinksIn.Items, "incomming link", toCheck);                                            //we check both links in and out, cause we want to verify 'all' the data.
            using (LinksAccessor iLinksOut = toCheck.LinksOut)
                CheckLinks(iLinksOut.Items, "outgoing link", toCheck);
            using (NeuronsAccessor iClusteredBy = toCheck.ClusteredByW)
            {
                CheckClusteredByList(iClusteredBy, "ClusteredBy", toCheck);
                CheckListType(iClusteredBy.Items, typeof(NeuronCluster), "ClusteredBy", toCheck);
            }
            NeuronCluster iToCheck = toCheck as NeuronCluster;
            if (iToCheck != null)
            {
                using (ChildrenAccessor iListToCheck = iToCheck.Children)
                {
                    CheckList(iListToCheck, "Children", toCheck);
                    CheckChildren(iToCheck, iListToCheck);
                }
            }
            CheckClusteredBy(toCheck);                                                                         //also need to check if we don't have any illegal references to 'clusters'
        }

        /// <summary>
        /// Checks if all the references defined in the list are valid and removes the ones that arent.
        /// </summary>
        /// <param name="list">The list.</param>
        private void CheckClusteredByList(NeuronsAccessor list, string desc, Neuron toCheck)
        {
            var iToRemove = (from i in list.Items
                             where Brain.Current.IsExistingID(i) == false
                             select i).ToList();
            foreach (ulong i in iToRemove)
            {
                try
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("Removing '{0}' from the {1} in {2}: ID doesn't exist.", i, desc, toCheck.ID));
                    list.Remove(i);
                }
                catch (Exception e)
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("failed to delete item from {0}: {1}.", desc, e.ToString()));
                }
            }
        }

        /// <summary>
        /// Checks if the children of a neuronCluster all have the correct back references in their 'ClusteredBy' list.
        /// We must make certain that items who are included multiple times, have multiple backlinks.
        /// </summary>
        /// <param name="toCheck">To check.</param>
        private void CheckChildren(NeuronCluster toCheck, ChildrenAccessor listToCheck)
        {
            var iToCheck = from i in listToCheck                                                    //we need to find out how many times each id occures in the children list
                           group i by i into SameIds
                           select new
                           {
                               ID = SameIds.Key,
                               Count = (from u in SameIds select 1).Count()
                           };
            foreach (var i in iToCheck)
            {
                Neuron iNeuron;
                int iCount;
                if (Brain.Current.TryFindNeuron(i.ID, out iNeuron) == true)
                {
                    using (NeuronsAccessor iClusteredBy = iNeuron.ClusteredBy)
                        iCount = (from u in iClusteredBy.Items where u == toCheck.ID select 1).Count();
                }
                else
                    iCount = 0;                                                                            //the item doesn't exist, so remove all references.
                RemoveClusteredBy(toCheck, iNeuron, i.Count, ref iCount, i.ID);
                while (iCount < i.Count)                                                                  //remove any refs in child that are to many
                {
                    try
                    {
                        Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("Removing '{0}' from '{1}' in 'Children': missing reference in child.ClusteredBy to cluster.", i.ID, toCheck.ID));
                        listToCheck.Mode = AccessorMode.Write;
                        listToCheck.Remove(i.ID);
                    }
                    catch (Exception e)
                    {
                        Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("failed to delete item '{0}' from '{1}': {2}.", i.ID, toCheck.ID, e));
                    }
                    iCount++;
                }
            }
        }

        private void RemoveClusteredBy(NeuronCluster toCheck, Neuron iNeuron, int i, ref int iCount, ulong id)
        {
            if (iNeuron != null)
            {
                using (NeuronsAccessor iClusteredBy = iNeuron.ClusteredByW)
                {
                    while (iCount > i)                                                                  //remove any refs in child that are to many
                    {
                        try
                        {
                            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("Removing '{0}' from '{1}' in 'ClusteredBy': to many back references to cluster.", toCheck.ID, id));
                            iClusteredBy.Remove(toCheck);
                        }
                        catch (Exception e)
                        {
                            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("failed to delete item '{1}' from '{0}': {2}.", id, toCheck.ID, e));
                        }
                        iCount--;
                    }
                }
            }
        }

        /// <summary>
        /// Similar as <see cref="DlgFixBrokenRefs.CheckChildren"/>. Checks if each cluster declared in 'ClusteredBy' of the argument,
        /// also has a backreference.  If not, the reference gets removed.  Keeps count of multiple references to the same cluster.
        /// </summary>
        /// <param name="toCheck">To check.</param>
        private void CheckClusteredBy(Neuron toCheck)
        {
            using (NeuronsAccessor iClusteredBy = toCheck.ClusteredByW)
            {
                var iToCheck = from i in iClusteredBy.Items                                                    //we need to find out how many times each id occures in the children list
                               group i by i into SameIds
                               select new
                               {
                                   ID = SameIds.Key,
                                   Count = (from u in SameIds select 1).Count()
                               };
                foreach (var i in iToCheck)
                {
                    Neuron iNeuron;
                    NeuronCluster iCluster = null;
                    int iCount = 0;
                    if (Brain.Current.TryFindNeuron(i.ID, out iNeuron) == true)
                    {
                        iCluster = iNeuron as NeuronCluster;
                        if (iCluster != null)
                            using (ChildrenAccessor iList = iCluster.Children)
                                iCount = (from u in iList where u == toCheck.ID select 1).Count();
                    }
                    while (iCount > i.Count)                                                                  //remove any refs in child that are to many
                    {
                        try
                        {
                            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("Removing '{0}' from '{1}' in 'Children': missing reference in child.ClusteredBy to cluster.", i.ID, toCheck.ID));
                            using (ChildrenAccessor iList = iCluster.ChildrenW)
                                iList.Remove(toCheck);
                        }
                        catch (Exception e)
                        {
                            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("failed to delete item '{1}' from '{0}': {2}.", i.ID, toCheck.ID, e));
                        }
                        iCount--;
                    }
                    while (iCount < i.Count)                                                                  //remove any refs in child that are to many
                    {
                        try
                        {
                            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("Removing '{0}' from '{1}' in 'ClusteredBy': to many back references to cluster.", toCheck.ID, i.ID));
                            iClusteredBy.Remove(i.ID);
                        }
                        catch (Exception e)
                        {
                            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("failed to delete item '{0}' from '{1}': {2}.", i.ID, toCheck.ID, e));
                        }
                        iCount++;
                    }
                }
            }
        }

        /// <summary>
        /// Checks all the links in the list.
        /// </summary>
        /// <remarks>
        /// We check if the to, from an meaning are valid refs + all the items in the info list.
        /// We also check if the link is to be found in both from and to neurons (so accessible from both sides, if not the case, fix this).
        /// </remarks>
        /// <param name="list">The list.</param>
        private void CheckLinks(IEnumerable<Link> list, string direction, Neuron toCheck)
        {
            List<Link> iList = new List<Link>(list);                                                        //we make a copy of the list cause we are about to modify it, which wont work on the list directly.
            foreach (Link i in iList)
            {
                try
                {
                    using (LinksAccessor iFromLinksOut = i.From.LinksOut)
                    {
                        if (Brain.Current.IsExistingID(i.FromID) == false || iFromLinksOut.Items.Contains(i) == false)
                        {
                            //we need to let the user know.
                            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("Deleting {0} defined in {1}: 'from' pointed to invalid neuron: {2}.", direction, toCheck.ID, i.FromID));
                            i.Destroy();
                            continue;
                        }
                    }
                    using (LinksAccessor iToLinksIn = i.To.LinksIn)
                    {
                        if (Brain.Current.IsExistingID(i.ToID) == false || iToLinksIn.Items.Contains(i) == false)
                        {
                            //we need to let the user know.
                            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("Deleting {0} defined in {1}: 'to' pointed to invalid neuron: {2}.", direction, toCheck.ID, i.ToID));
                            i.Destroy();
                            continue;
                        }
                    }
                    if (Brain.Current.IsExistingID(i.MeaningID) == false)
                    {
                        //we need to let the user know.
                        Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("Deleting {0} defined in {1}: 'meaning' pointed to invalid neuron: {2}.", direction, toCheck.ID, i.MeaningID));
                        i.Destroy();
                        continue;
                    }
                    using (LinkInfoAccessor iInfoList = i.InfoW)
                        CheckList(iInfoList, "info list", toCheck);
                }
                catch (Exception e)
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("failed to delete link: {0}.", e.ToString()));
                }
            }
        }

        /// <summary>
        /// Checks if all the references defined in the list are valid and removes the ones that arent.
        /// </summary>
        /// <param name="list">The list.</param>
        private void CheckList(IList<ulong> list, string desc, Neuron toCheck)
        {
            var iToRemove = (from i in list
                             where Brain.Current.IsExistingID(i) == false
                             select i).ToList();
            foreach (ulong i in iToRemove)
            {
                try
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("Removing '{0}' from the {1} in {2}: ID doesn't exist.", i, desc, toCheck.ID));
                    list.Remove(i);
                }
                catch (Exception e)
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("failed to delete item from {0}: {1}.", desc, e.ToString()));
                }
            }
        }

        /// <summary>
        /// Checks if the type of the items in a list is correct and removes all items from the list that don't match.
        /// </summary>
        /// <param name="list">The list with items to check.</param>
        /// <param name="type">The type.</param>
        private void CheckListType(IList<ulong> list, Type type, string desc, Neuron toCheck)
        {
            var iToRemove = (from i in list
                             let neuron = Brain.Current[i]
                             where neuron.GetType() != type
                             select i).ToList();
            foreach (ulong i in iToRemove)
            {
                try
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("Removing '{0}' from the {1} in {2}: not a '{3}'.", i, desc, toCheck.ID, type.ToString()));
                    list.Remove(i);
                }
                catch (Exception e)
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(LogItems.Add), string.Format("failed to delete item from {0}: {1}.", desc, e.ToString()));
                }
            }
        }

        /// <summary>
        /// Called when the async <see cref="DlgFixBrokenRefs.ProcessItems"/> is finished.  Updates the buttons and stuff
        /// to indicate that we are done.
        /// </summary>
        /// <param name="result">The result.</param>
        private void ProcessFinished(IAsyncResult result)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(InternalProcessFinished));
        }

        private void InternalProcessFinished()
        {
            BtnStartStop.Content = "Start";
            BtnStartStop.ToolTip = "Start the process.";
            BtnClose.IsEnabled = true;
            BtnStartStop.Tag = null;                                                                  //always let the system know we canallow a new start.
            LogItems.Add("All neurons processed!");
        }

        /// <summary>
        /// Sets the current pos (for async calls).
        /// </summary>
        /// <param name="id">The id.</param>
        private void SetCurPos(ulong id)
        {
            CurrentPos = id;
        }
    }
}