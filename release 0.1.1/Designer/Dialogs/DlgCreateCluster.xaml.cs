﻿using System.Windows;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Interaction logic for DlgCreateCluster.xaml
    /// </summary>
    /// <remarks>
    /// Use the <see cref="DlgCreateCluster.CreateCluster"/> function to display the dialog.
    /// </remarks>
    public partial class DlgCreateCluster : Window
    {
        private string fTitle;
        private ulong fMeaning;

        private DlgCreateCluster()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Creates a cluster and returns it.
        /// </summary>
        /// <remarks>
        /// The cluster is added to the brain before it is returned.
        /// </remarks>
        /// <param name="name">A possible default value for the name.</param>
        /// <returns>A cluster or null if the action was aborted.</returns>
        public static NeuronCluster CreateCluster(string name, Window owner)
        {
            DlgCreateCluster iDlg = new DlgCreateCluster();
            iDlg.TxtTitle.Text = name;
            iDlg.Owner = owner;
            bool? iRes = iDlg.ShowDialog();
            if (iRes.HasValue == true && iRes.Value == true)
            {
                NeuronCluster iCluster = new NeuronCluster();
                Brain.Current.Add(iCluster);
                iCluster.Meaning = iDlg.fMeaning;
                BrainData.Current.NeuronInfo[iCluster.ID].DisplayTitle = iDlg.fTitle;
                return iCluster;
            }
            return null;
        }

        private void OnClickOk(object sender, RoutedEventArgs e)
        {
            fTitle = TxtTitle.Text;
            if (CmbMeaning.SelectedValue != null)
                fMeaning = (ulong)CmbMeaning.SelectedValue;
            else
                fMeaning = Neuron.EmptyId;
            DialogResult = true;
        }
    }
}