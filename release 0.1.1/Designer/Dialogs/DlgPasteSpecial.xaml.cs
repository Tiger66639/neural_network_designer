﻿using System.Windows;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Interaction logic for DlgPasteSpecial.xaml
    /// </summary>
    public partial class DlgPasteSpecial : Window
    {
        private bool fDuplicate = false;

        public DlgPasteSpecial()
        {
            InitializeComponent();
        }

        private void OnClickOk(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        #region Duplicate

        /// <summary>
        /// Gets the wether a duplicate should be created or a reference, according to the user's choises.
        /// </summary>
        public bool Duplicate
        {
            get { return fDuplicate; }
            internal set { fDuplicate = value; }
        }

        #endregion Duplicate

        private void Duplicate_Checked(object sender, RoutedEventArgs e)
        {
            Duplicate = true;
        }

        private void Reference_Checked(object sender, RoutedEventArgs e)
        {
            Duplicate = false;
        }
    }
}