﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Provides xml streaming for an ObservedCollection(ulong) collection.  Without it, <see cref="BrainData.DefaultMeaningIds"/>
    /// doesn't load properly.
    /// </summary>
    public class NeuronIDCollection : ObservedCollection<ulong>, IXmlSerializable, IWeakEventListener
    {
        #region ctor-dtor

        /// <summary>
        /// Initializes a new instance of the <see cref="NeuronIDCollection"/> class.
        /// </summary>
        /// <param name="owner">The owner of the list.</param>
        public NeuronIDCollection(object owner)
           : base(owner)
        {
            NeuronChangedEventManager.AddListener(Brain.Current, this);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NeuronIDCollection"/> class.
        /// </summary>
        /// <param name="items">The list to copy the initial items from.</param>
        public NeuronIDCollection(List<ulong> items)
           : base(null, items)
        {
            NeuronChangedEventManager.AddListener(Brain.Current, this);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NeuronIDCollection"/> class.
        /// </summary>
        public NeuronIDCollection()
        {
            NeuronChangedEventManager.AddListener(Brain.Current, this);
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="NeuronIDCollection"/> is reclaimed by garbage collection.
        /// </summary>
        ~NeuronIDCollection()
        {
            NeuronChangedEventManager.RemoveListener(Brain.Current, this);
        }

        #endregion ctor-dtor

        #region IXmlSerializable Members

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            bool wasEmpty = reader.IsEmptyElement;

            reader.Read();
            if (wasEmpty) return;
            while (reader.NodeType != XmlNodeType.EndElement)
            {
                reader.ReadStartElement("ID");
                Add(ulong.Parse(reader.ReadString()));
                reader.ReadEndElement();
                //reader.MoveToContent();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            foreach (ulong i in this)
            {
                writer.WriteStartElement("ID");
                writer.WriteString(i.ToString());
                writer.WriteEndElement();
            }
        }

        #endregion IXmlSerializable Members

        #region IWeakEventListener Members

        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            if (managerType == typeof(NeuronChangedEventManager))
            {
                Current_NeuronChanged(sender, (NeuronChangedEventArgs)e);
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Event handler for <see cref="Brain.NeuronChanged"/>
        /// </summary>
        /// <remarks>
        /// Do asyn cause this event can be raised from different threads.
        /// </remarks>
        private void Current_NeuronChanged(object sender, NeuronChangedEventArgs e)
        {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<NeuronChangedEventArgs>(InternalNeuronChanged), e);
        }

        /// <summary>
        /// for replace: replaces the item if it is in this list
        /// fore remove: removes the item.
        /// </summary>
        /// <param name="e">The <see cref="NeuralNetworkDesigne.HAB.NeuronChangedEventArgs"/> instance containing the event data.</param>
        private void InternalNeuronChanged(NeuronChangedEventArgs e)
        {
            Neuron iSender = e.OriginalSource;
            if (iSender != null)
            {
                if (e.Action == BrainAction.Removed)
                    this.Remove(e.OriginalSourceID);                                                 //simply try to remove the item.
            }
        }

        #endregion IWeakEventListener Members
    }
}