﻿using System;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Contains all the data for the designer for easy streaming.
    /// </summary>
    /// <remarks>
    /// All the streamable data for the <see cref="BrainData"/> object is stored in a different
    /// class so that the braindata can easely load different data by replacing a field.
    /// </remarks>
    public class DesignerDataFile
    {
        /// <summary>
        /// Gets or sets the name of the project
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        public DebugMode Debugmode { get; set; }

        public bool BreakOnException { get; set; }

        public BreakPointCollection BreakPoints { get; set; }

        public TimeSpan PlaySpeed { get; set; }

        public ObservedCollection<ToolBoxItem> ToolBoxItems { get; set; }

        public NeuronIDCollection DefaultMeaningIds { get; set; }

        public NeuronInfoDictionary NeuronInfo { get; set; }

        //public ObservedCollection<MindMap> MindMaps { get; set; }

        public NeuronCollection<Instruction> Instructions { get; set; }

        public NeuronCollection<Neuron> Operators { get; set; }

        public NeuronCollection<Neuron> GrammarTypes { get; set; }

        /// <summary>
        /// Gets or sets the variables that need to be monitored for this project.
        /// </summary>
        /// <value>The watches.</value>
        public ObservedCollection<Watch> Watches { get; set; }

        public CommChannelCollection CommChannels { get; set; }

        /// <summary>
        /// Gets or sets the list of frame editors.
        /// </summary>
        /// <value>The frame editors.</value>
        //public ObservedCollection<FrameEditor> FrameEditors { get; set; }

        /// <summary>
        /// Gets or sets the list of flows declared in the project.
        /// </summary>
        //public ObservedCollection<Flow> Flows { get; set; }

        /// <summary>
        /// Gets or sets all the editors stored in the project. This is a tree like structure.
        /// </summary>
        /// <value>The editors.</value>
        public EditorCollection Editors { get; set; }

        /// <summary>
        /// Gets or sets the thesaurus data (relationships between objects) stored for this project.
        /// </summary>
        /// <value>The thesaurus.</value>
        public Thesaurus Thesaurus { get; set; }
    }
}