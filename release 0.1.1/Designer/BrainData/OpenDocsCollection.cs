﻿using System.Collections.ObjectModel;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// A collection that is able to store all the documents that are curently open.
    /// </summary>
    /// <remarks>
    /// Makes certaint that any code editors are properly loaded when displayed + unloaded when not visual.
    /// </remarks>
    public class OpenDocsCollection : ObservableCollection<object>
    {
        /// <summary>
        /// Inserts an item into the collection at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="item"/> should be inserted.</param>
        /// <param name="item">The object to insert.</param>
        protected override void InsertItem(int index, object item)
        {
            if (item is CodeEditor)
                ((CodeEditor)item).IsLoaded = true;
            base.InsertItem(index, item);
        }

        /// <summary>
        /// Removes all items from the collection.
        /// </summary>
        protected override void ClearItems()
        {
            foreach (object i in this)
                if (i is CodeEditor)
                    ((CodeEditor)i).IsLoaded = false;
            base.ClearItems();
        }

        /// <summary>
        /// Replaces the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to replace.</param>
        /// <param name="item">The new value for the element at the specified index.</param>
        protected override void SetItem(int index, object item)
        {
            CodeEditor iPrev = this[index] as CodeEditor;
            if (iPrev != null)
                iPrev.IsLoaded = false;
            if (item is CodeEditor)
                ((CodeEditor)item).IsLoaded = true;
            base.SetItem(index, item);
        }

        /// <summary>
        /// Removes the item at the specified index of the collection.
        /// </summary>
        /// <param name="index">The zero-based index of the element to remove.</param>
        protected override void RemoveItem(int index)
        {
            CodeEditor iPrev = this[index] as CodeEditor;
            base.RemoveItem(index);
            if (iPrev != null)
                iPrev.IsLoaded = false;
        }
    }
}