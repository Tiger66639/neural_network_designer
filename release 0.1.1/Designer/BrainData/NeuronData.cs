﻿using System.IO;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// A class that stores extra data for neurons, like a title and description.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Because NeuronData should only be saved when there actually is data available, the property setters
    /// will make certain that the object is registered with the <see cref="BrainData"/> as soon as there
    /// is data.
    /// </para>
    /// </remarks>
    public class NeuronData : OwnedObject, IXmlSerializable, IDescriptionable
    {
        #region fields

        private Neuron fNeuron;
        private string fTitle;
        private string fCategory;
        private string fDescription;
        private bool fIsNextStatement;

        #endregion fields

        #region ctor

        /// <summary>
        /// Normal constructor, with id.
        /// </summary>
        /// <param name="id">The id of the neuron to wrap.</param>
        public NeuronData(ulong id)
        {
            if (id != Neuron.EmptyId && id != Neuron.TempId)
                fNeuron = Brain.Current[id];
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NeuronData"/> class.
        /// </summary>
        /// <param name="item">The item.</param>
        public NeuronData(Neuron item)
        {
            fNeuron = item;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NeuronData"/> class.
        /// </summary>
        public NeuronData()
        {
        }

        /// <summary>
        /// When we get destroyed (garbage cleanup), we remove from the dictionary of the braindata.
        /// </summary>
        ~NeuronData()
        {
            if (BrainData.Current.NeuronInfo != null)
                BrainData.Current.NeuronInfo.RemoveTemp(this);
        }

        #endregion ctor

        #region Prop

        #region Title

        /// <summary>
        /// Gets/sets the title of the neuron.  To display, use <see cref="DisplaytTitle"/> instead.
        /// </summary>
        /// <remarks>
        /// only returns the physically stored name, no composite name.  Primarely used for storage.
        /// </remarks>
        public string Title
        {
            get
            {
                return fTitle;
            }
            set
            {
                if (fTitle != value)
                {
                    OnPropertyChanging("Title", fTitle, value);
                    fTitle = value;
                    OnPropertyChanged("Title");
                    OnPropertyChanged("DisplayTitle");
                }
            }
        }

        #endregion Title

        #region DisplayTitle

        /// <summary>
        /// Gets/sets the title that should be displayed.
        /// </summary>
        /// <remarks>
        /// Primarely used for display.
        /// we check the title of the neuron, if not found, a default title is generated: for textneurons, the text, otherwise
        /// the type name + id.
        /// <para>
        /// when set, we always set to the brainData.
        /// </para>
        /// </remarks>
        [XmlIgnore]
        public string DisplayTitle
        {
            get
            {
                string iRes = Title;
                if (string.IsNullOrEmpty(iRes) == true)
                {
                    if (fNeuron is TextNeuron)
                        iRes = ((TextNeuron)fNeuron).Text + (" - " + fNeuron.ID.ToString());
                    else if (fNeuron is DoubleNeuron)
                        iRes = ((DoubleNeuron)fNeuron).Value.ToString();
                    else if (fNeuron is IntNeuron)
                        iRes = ((IntNeuron)fNeuron).Value.ToString();
                    else if (fNeuron is Expression)
                        iRes = fNeuron.ToString() + (" - " + fNeuron.ID.ToString());
                    else
                        iRes = fNeuron.GetType().Name + (" - " + fNeuron.ID.ToString());
                }
                return iRes;
            }
            set
            {
                if (value != Title)
                {
                    Title = value;
                    if (fNeuron is TextNeuron)
                        ((TextNeuron)fNeuron).Text = value;
                    else if (fNeuron is DoubleNeuron)
                    {
                        double iVal;
                        if (double.TryParse(value, out iVal) == true)
                            ((DoubleNeuron)fNeuron).Value = iVal;
                    }
                    else if (fNeuron is IntNeuron)
                    {
                        int iVal;
                        if (int.TryParse(value, out iVal) == true)
                            ((IntNeuron)fNeuron).Value = iVal;
                    }
                    Save();
                }
            }
        }

        #endregion DisplayTitle

        #region ID

        /// <summary>
        /// Gets the id of the object we are storing extra info for.
        /// </summary>
        public ulong ID
        {
            get
            {
                if (fNeuron != null)
                    return fNeuron.ID;
                else
                    return Neuron.EmptyId;
            }
            internal set
            {
                if (value == Neuron.EmptyId)
                    fNeuron = null;
                else
                    fNeuron = Brain.Current[value];
            }
        }

        #endregion ID

        #region Neuron

        /// <summary>
        /// Gets the neuron taht this object wraps.
        /// </summary>
        /// <value>The neuron.</value>
        public Neuron Neuron
        {
            get { return fNeuron; }
        }

        #endregion Neuron

        #region DescriptionTitle

        /// <summary>
        /// Gets a title that the description editor can use to display in the header.
        /// </summary>
        /// <value></value>
        public string DescriptionTitle
        {
            get { return DisplayTitle; }
        }

        #endregion DescriptionTitle

        #region Description

        /// <summary>
        /// Gets/sets a possible description for the neuron.
        /// </summary>
        /// <remarks>
        /// This value is stored as a string internally to save room, otherwise the app mem explodes.
        /// </remarks>
        public FlowDocument Description
        {
            get
            {
                if (fDescription != null)
                {
                    StringReader stringReader = new StringReader(fDescription);
                    XmlReader xmlReader = XmlReader.Create(stringReader);
                    return XamlReader.Load(xmlReader) as FlowDocument;
                }
                else
                    return Helper.CreateDefaultFlowDoc();
            }
            set
            {
                string iVal = XamlWriter.Save(value);
                if (fDescription != iVal)
                {
                    OnPropertyChanging("DescriptionText", fDescription, value);                   //when set, we only update through the xml text format.
                    fDescription = iVal;
                    OnPropertyChanged("Description");
                    //OnPropertyChanged("DescriptionText");
                    Save();
                }
            }
        }

        #endregion Description

        #region DescriptionText

        /// <summary>
        /// Gets/sets the description in xml format.
        /// </summary>
        public string DescriptionText
        {
            get
            {
                return fDescription;
            }
            set
            {
                OnPropertyChanging("DescriptionText", fDescription, value);
                fDescription = value;
                OnPropertyChanged("DescriptionText");
            }
        }

        #endregion DescriptionText

        #region Category

        /// <summary>
        /// Gets/sets the category of the neuron.
        /// </summary>
        /// <remarks>
        /// This is primarely used by <see cref="Neuron"/>s used on the
        /// toolbox.
        /// </remarks>
        public string Category
        {
            get
            {
                return fCategory;
            }
            set
            {
                OnPropertyChanging("Category", fCategory, value);
                fCategory = value;
                OnPropertyChanged("Category");

                Save();
            }
        }

        #endregion Category

        #region InDictionary

        /// <summary>
        /// Gets/sets the if this item is stored in it's corresponding dictionary.
        /// </summary>
        /// <remarks>
        /// Currently only valid for <see cref="TextNeuron"/>s.
        /// Also see <see cref="NeuronData.IsDictionaryItem"/>.
        /// </remarks>
        public bool InDictionary
        {
            get
            {
                TextNeuron iText = fNeuron as TextNeuron;
                if (iText != null)
                    return TextSin.Words.ContainsKey(iText.Text.ToLower());                                        //always use the lowercase rep to store in dict.
                return false;
            }
            set
            {
                if (value == true)
                {
                    TextNeuron iText = fNeuron as TextNeuron;
                    if (iText != null)
                        TextSin.Words[iText.Text.ToLower()] = ID;
                }
                else
                {
                    TextNeuron iText = fNeuron as TextNeuron;
                    if (iText != null)
                        TextSin.Words.Remove(iText.Text.ToLower());
                }
            }
        }

        #endregion InDictionary

        #region IsDictionaryItem

        /// <summary>
        /// Gets if this item can be put in a dictionary or not.
        /// </summary>
        /// <remarks>
        /// Currently, only text neurons are supported.
        /// </remarks>
        public bool IsDictionaryItem
        {
            get { return fNeuron is TextNeuron; }
        }

        #endregion IsDictionaryItem

        #region IsNextStatement

        /// <summary>
        /// Gets/sets the wether this neuron is the next statement for the currently selected processor
        /// (in <see cref="ProcessorManager.SelectedProcessor"/>).
        /// </summary>
        public bool IsNextStatement
        {
            get
            {
                return fIsNextStatement;
            }
            set
            {
                fIsNextStatement = value;
                OnPropertyChanged("IsNextStatement");
            }
        }

        #endregion IsNextStatement

        #endregion Prop

        #region functions

        /// <summary>
        /// Called when [property changing].
        /// </summary>
        /// <param name="aProp">A prop.</param>
        /// <param name="aOld">A old.</param>
        /// <param name="aNew">A new.</param>
        protected override void OnPropertyChanging(string aProp, object aOld, object aNew)
        {
            NeuronDataUndoItem iUndoData = new NeuronDataUndoItem(Neuron, aProp, aOld);
            WindowMain.UndoStore.AddCustomUndoItem(iUndoData);
        }

        /// <summary>
        /// saves this object into <see cref="BrainData"/>
        /// </summary>
        public void Save()
        {
            BrainData.Current.NeuronInfo.Commit(this);
        }

        /// <summary>
        /// An easy way to Store the intitial description for a neuron with a simple string. It will automatically
        /// format it to xaml.
        /// </summary>
        /// <param name="value">The value.</param>
        public void StoreDescription(string value)
        {
            fDescription = string.Format("<FlowDocument xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\"><Paragraph TextAlignment=\"Left\" FontFamily=\"Segoe UI\" FontSize=\"12\" NumberSubstitution.CultureSource=\"User\">{0}</Paragraph></FlowDocument>", value);
        }

        /// <summary>
        /// Called when a property of the underlying <see cref="Neuron"/> has
        /// changed.  This can have an effect on the display title, so it needs
        /// to be updated.
        /// </summary>
        /// <param name="p"></param>
        internal void NeuronPropChanged(string prop)
        {
            OnPropertyChanged("DisplayTitle");
        }

        /// <summary>
        /// Called when the neuron itself is changed.
        /// </summary>
        /// <param name="neuron">The new neuron.</param>
        internal void NeuronChanged(Neuron neuron)
        {
            fNeuron = neuron;
            OnPropertyChanged("DisplayTitle");
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return DisplayTitle;
        }

        #endregion functions

        #region IXmlSerializable Members

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            bool wasEmpty = reader.IsEmptyElement;

            reader.Read();
            if (wasEmpty) return;

            reader.ReadStartElement("ID");
            ID = ulong.Parse(reader.ReadString());
            reader.ReadEndElement();

            if (reader.IsEmptyElement == false)
            {
                reader.ReadStartElement("Title");
                fTitle = reader.ReadString();                                           //write to field, otherwise we call save again through property setter, which we dont want
                reader.ReadEndElement();
            }
            else
                reader.ReadStartElement("Title");

            if (reader.IsEmptyElement == false)
            {
                reader.ReadStartElement("Category");
                fCategory = reader.ReadString();                                        //write to field, otherwise we call save again through property setter, which we dont want
                reader.ReadEndElement();
            }
            else
                reader.ReadStartElement("Category");

            if (reader.IsEmptyElement == false)
                fDescription = reader.ReadOuterXml();
            else
                reader.ReadStartElement("FlowDocument");

            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("ID");
            writer.WriteString(ID.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("Title");
            writer.WriteString(Title);
            writer.WriteEndElement();

            writer.WriteStartElement("Category");
            writer.WriteString(Category);
            writer.WriteEndElement();

            if (fDescription != null)
                writer.WriteRaw(fDescription);
            else
            {
                writer.WriteStartElement("FlowDocument");
                writer.WriteEndElement();
            }
        }

        #endregion IXmlSerializable Members
    }
}