﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// A helper class that manages the both the designer and the brain data in regards to loading and saving.
    /// </summary>
    public class ProjectManager : ObservableObject
    {
        #region Fields

        private const int MAXFILECOUNT = 10;

        /// <summary>
        /// The name of the project file containing all the designer data.
        /// </summary>
        public const string DESIGNERFILE = "BrainDesigner.xml";

        /// <summary>
        /// The name of the project file containing info about the network itself.
        /// </summary>
        public const string NETWORKFILE = "Brain.xml";

        private static ProjectManager fDefault;
        private static string fLocation;
        private ObservableCollection<string> fLastOpened;
        private string fSandboxLocation;
        private bool fSandboxRunning;
        private bool fIsSandBox;
        private bool fDataError = false;

        #endregion Fields

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectManager"/> class.
        /// </summary>
        public ProjectManager()
        {
            Brain.Current.Changed += new EventHandler(Project_Changed);
            BrainData.Current.AfterLoad += new EventHandler(ProjectData_AfterLoad);
            BrainData.Current.BeforeSave += new EventHandler(ProjectData_BeforeSave);
            SandboxLocation = Properties.Settings.Default.SandboxPath;
            if (string.IsNullOrEmpty(SandboxLocation) == true)
                SandboxLocation = Path.Combine(Path.GetTempPath(), "DNN\\Sandbox");              //init the sandbox path to something usefull
            fLastOpened = new ObservableCollection<string>();
            if (Properties.Settings.Default.LastOpened != null)
            {
                foreach (string i in Properties.Settings.Default.LastOpened)
                    fLastOpened.Add(i);
            }
        }

        #endregion ctor

        #region Prop

        #region ProjectChanged

        /// <summary>
        /// Gets/sets if the project has been changed since the last save.
        /// </summary>
        public bool ProjectChanged
        {
            get
            {
                return Brain.Current.IsChanged || BrainData.Current.IsChanged;
            }
        }

        #endregion ProjectChanged

        #region LastOpened

        /// <summary>
        /// Gets the list of last opened project paths.
        /// </summary>
        /// <value>The last opened.</value>
        public ObservableCollection<string> LastOpened
        {
            get
            {
                return fLastOpened;
            }
        }

        #endregion LastOpened

        #region Location

        /// <summary>
        /// Gets/sets the location of the currently opened project.
        /// </summary>
        /// <remarks>
        /// This needs to be a local prop (not static) cause the UI needs to gets warned when it changes so that it can display this. And the
        /// UI requires a normal prop.
        /// </remarks>
        [XmlIgnore]
        public string Location
        {
            get
            {
                return fLocation;
            }
            private set
            {
                fLocation = value;
                OnPropertyChanged("Location");
            }
        }

        #endregion Location

        #region SandboxLocation

        /// <summary>
        /// Gets/sets the location to copy projects to that need to be started in a sandbox environment.
        /// </summary>
        public string SandboxLocation
        {
            get
            {
                return fSandboxLocation;
            }
            set
            {
                fSandboxLocation = value;
                OnPropertyChanged("SandboxLocation");
            }
        }

        #endregion SandboxLocation

        #region SandboxRunning

        /// <summary>
        /// Gets the wether the sandbox is running or not.
        /// </summary>
        [XmlIgnore]
        public bool SandboxRunning
        {
            get { return fSandboxRunning; }
            internal set
            {
                fSandboxRunning = value;
                OnPropertyChanged("SandboxRunning");
            }
        }

        #endregion SandboxRunning

        #region IsSandBox

        /// <summary>
        /// Gets/sets wether this project instance is a sandbox of another project.
        /// </summary>
        public bool IsSandBox
        {
            get
            {
                return fIsSandBox;
            }
            set
            {
                if (fIsSandBox != value)
                {
                    fIsSandBox = value;
                    OnPropertyChanged("IsSandBox");
                    if (value == true)
                        Brain.Current.Storage.DataPath = Path.Combine(Location, "Data");                                                     //we must update the data path of the project cause we couldn't do this during the save while building the sandbox (the save would have put some files in the wrong place, which broke the copy).
                }
            }
        }

        #endregion IsSandBox

        #region Default

        static public ProjectManager Default
        {
            get
            {
                if (fDefault == null)
                    fDefault = new ProjectManager();
                return fDefault;
            }
        }

        #endregion Default

        #region DataError

        /// <summary>
        /// Gets if the data load function failed.
        /// </summary>
        /// <remarks>
        /// This is used when saving the data to see if it is ok to save it.  When the data file was not correctly read (and so nothing
        /// was loaded), we don't want to overwrite it, cause than we loose all the info. We also use this to provide a visual indication
        /// about the error.
        /// </remarks>
        [XmlIgnore]
        public bool DataError
        {
            get { return fDataError; }
            internal set
            {
                if (fDataError != value)
                {
                    fDataError = value;
                    OnPropertyChanged("DataError");
                }
            }
        }

        #endregion DataError

        #endregion Prop

        #region Common interface

        /// <summary>
        /// Creates a new project. If there is a template defined, this is loaded as the new brain.
        /// </summary>
        public void CreateNew()
        {
            if (ProjectChanged == true)
            {
                MessageBoxResult iRes = MessageBox.Show("The project has changed, perform a save first?", "New project", MessageBoxButton.YesNoCancel);
                if (iRes == MessageBoxResult.Yes)
                    Save();
                else if (iRes == MessageBoxResult.Cancel)
                    return;
            }
            CreateOrLoadFromTemplate();
        }

        /// <summary>
        /// Creates or loada a new project from template.
        /// </summary>
        private void CreateOrLoadFromTemplate()
        {
            DataError = false;                                                                                       //when initially creating a new project, there is no data error.
            Cursor iPrev = Mouse.OverrideCursor;
            Mouse.OverrideCursor = Cursors.Wait;
            WindowMain.UndoStore.UndoStateStack.Push(UndoState.Blocked);                                             //during creation of the new project, we block any changes to the undo system, cause they are invalid (it's a new, fresh project).
            NeuronStorageMode iPrevStorageMode = Settings.StorageMode;
            Settings.StorageMode = NeuronStorageMode.AlwaysInMem;                                                   //we need to change the settings to keep everything in mem when creating a new project since we don't know the path yet.  If we don't do this, the template neurons get lost.
            try
            {
                try
                {
                    string iPath = Designer.Properties.Settings.Default.DefaultTemplatePath;
                    if (string.IsNullOrEmpty(iPath) == false)
                    {
                        ClearProject();
                        LoadBrain(iPath);
                        Brain.Current.TouchMem();
                        Brain.Current.Storage.DataPath = null;                                                                       //there is no place yet to save the braindata, so reset to null.
                        if (BrainUndoMonitor.IsInstantiated == false)                                                         //we need to make certain the the brain undo monitor registers it's
                            BrainUndoMonitor.Instantiate();
                        LoadDesigner(iPath);
                        Log.LogInfo("ProjectManager.CreateNew", string.Format("Succesfully created new project from template {0}.", iPath));
                    }
                    else
                    {
                        ClearProject();
                        Brain.New();
                        if (BrainUndoMonitor.IsInstantiated == false)                                                         //we need to make certain the the brain undo monitor registers it's
                            BrainUndoMonitor.Instantiate();
                        BrainData.New();
                    }
                }
                catch (Exception e)
                {
                    Log.LogError("ProjectManager.CreateNew", e.ToString());
                    DataError = true;
                }
            }
            finally
            {
                WindowMain.UndoStore.UndoStateStack.Pop();
                Mouse.OverrideCursor = iPrev;
                Settings.StorageMode = iPrevStorageMode;
            }
            Location = null;                                                                                      //it's a new project, but we used the load to get it, so reset the location
        }

        /// <summary>
        /// Shows a dialog to the user so he can select to open a new project.  The operation can also be canceled.
        /// </summary>
        public void Open()
        {
            System.Windows.Forms.FolderBrowserDialog iDialog = new System.Windows.Forms.FolderBrowserDialog();

            if (string.IsNullOrEmpty(Location) == false)
                iDialog.SelectedPath = Location;
            iDialog.Description = "Please select the project directory.";
            iDialog.ShowNewFolderButton = true;
            System.Windows.Forms.DialogResult iRes = iDialog.ShowDialog();
            if (iRes == System.Windows.Forms.DialogResult.OK)
                Open(iDialog.SelectedPath);
        }

        /// <summary>
        /// Opens the project in the specified path.
        /// </summary>
        /// <remarks>
        /// Makes certain that the previous project is saved.
        /// </remarks>
        /// <param name="path">The path.</param>
        public void Open(string path)
        {
            if (ProjectChanged == true)
            {
                MessageBoxResult iSaveRes = MessageBox.Show("The project has changed, perform a save first?", "Open project", MessageBoxButton.YesNoCancel);
                if (iSaveRes == MessageBoxResult.Yes)
                    Save();
                else if (iSaveRes == MessageBoxResult.Cancel)
                    return;
            }
            DataError = false;                                                            //need to reset the data errors when we open another project.
            LoadProject(path);
            AddLastOpened(path);
        }

        /// <summary>
        /// Adds path to the list of last opened paths, making certain that when there are no doubles in the list + not to many.
        /// </summary>
        /// <param name="path">The path.</param>
        private void AddLastOpened(string path)
        {
            int iIndex = LastOpened.IndexOf(path);
            if (iIndex > -1)                                                                                   //already in list, move to front.
                LastOpened.Move(iIndex, 0);
            else
            {
                LastOpened.Insert(0, path);
                if (LastOpened.Count > MAXFILECOUNT)
                    LastOpened.RemoveAt(LastOpened.Count - 1);
            }
        }

        /// <summary>
        /// Tries to save the project to the current <see cref="ProjectManager.Location"/>. If there is no current location,
        /// a SaveAs is performed.
        /// </summary>
        /// <returns><c>true</c> if the user canceled the operation (from a saveAs, or with data error), otherwise false.</returns>
        public bool Save()
        {
            bool iRes = false;
            if (string.IsNullOrEmpty(Default.Location) == true)                //if there is no path defined, do a save as.
                iRes = SaveAs();                                                  //we don't check data error before SaveAs, cause this does the check also.
            else
            {
                if (DataError == true)
                {
                    MessageBoxResult iMbRes = MessageBox.Show("There were errors loading the project, saving might loose data, continue?", "Save project", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    if (iMbRes == MessageBoxResult.No)
                        return true;
                }
                SaveProject();
            }
            return iRes;
        }

        /// <summary>
        /// Asks the user the new location to save the brain to.  If there was a previous location
        /// </summary>
        /// <returns>True if the user canceled the operation.</returns>
        public bool SaveAs()
        {
            if (DataError == true)
            {
                MessageBoxResult iMbRes = MessageBox.Show("There were errors loading the project, saving might loose data, continue?", "Save project", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (iMbRes == MessageBoxResult.No)
                    return true;
            }

            System.Windows.Forms.FolderBrowserDialog iDialog = new System.Windows.Forms.FolderBrowserDialog();
            iDialog.Description = "Please select the project directory.";
            iDialog.ShowNewFolderButton = true;
            if (string.IsNullOrEmpty(Location) == false)
                iDialog.SelectedPath = Location;
            System.Windows.Forms.DialogResult iRes = iDialog.ShowDialog();
            if (iRes == System.Windows.Forms.DialogResult.OK)
            {
                Cursor iPrev = Mouse.OverrideCursor;
                Mouse.OverrideCursor = Cursors.Wait;
                try
                {
                    if (string.IsNullOrEmpty(Default.Location) == false)                                              //if there was a previous path defined, we need to copy all the data to the new location.
                    {
                        if (Location != iDialog.SelectedPath)                                       //only need to move if there is a new path: important, cause moving cleans out the new location first.
                            MoveDataTo(iDialog.SelectedPath);
                    }
                    else
                    {
                        Location = iDialog.SelectedPath;                                            //so we know the path next time.
                        Brain.Current.Storage.DataPath = PreparePathForProject(Location);
                    }
                }
                finally
                {
                    Mouse.OverrideCursor = iPrev;
                }
                SaveProject();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Starts the project in a sandbox for testing.
        /// </summary>
        /// <remarks>
        /// Before we can start the sandbox, we need save the project.
        /// </remarks>
        public void StartSandBox()
        {
            CleanSandbox();
            string iDataPath = PreparePathForProject(SandboxLocation);
            if (Save() == false)                                                                            //save returns true if the user canceled the operation.
            {
                CopyProjectTo(SandboxLocation, iDataPath);
                ProcessStartInfo iStartInfo = new ProcessStartInfo();
                iStartInfo.FileName = Assembly.GetExecutingAssembly().Location;
                iStartInfo.Arguments = SandboxLocation + " sandbox";                                         //the 'sandbox' arg lets the sub process know it's a sandbox.
                Process iStarted = Process.Start(iStartInfo);
                iStarted.EnableRaisingEvents = true;                                                         //we need to be warned when the process stops, so we need to set this.
                iStarted.Exited += new EventHandler(Sandbox_Exited);
                SandboxRunning = true;
                Log.LogInfo("ProjectManager.StartSandbox", "Sandbox succesfully started.");
            }
        }

        #endregion Common interface

        #region Project changed updating

        /// <summary>
        /// Cleans the sandbox directory from any data.
        /// </summary>
        /// <remarks>
        /// Currently simply deletes the temp dir.
        /// </remarks>
        internal void CleanSandbox()
        {
            if (Directory.Exists(SandboxLocation) == true)                                               //could be that it is non existing, this raises an error.
                Directory.Delete(SandboxLocation, true);                                                  //we do a clean before we copy all the data to this dir.
        }

        /// <summary>
        /// Handles the Exited event of the Sandbox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void Sandbox_Exited(object sender, EventArgs e)
        {
            SandboxRunning = false;
            Log.LogInfo("ProjectManager.StartSandbox", "Sandbox project has been closed.");
        }

        /// <summary>
        /// Handles the BeforeSave event of the BrainData.
        /// </summary>
        /// <remarks>
        /// The first time that the undo data changes after a project has been saved, we need to update the project status.
        /// </remarks>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ProjectData_BeforeSave(object sender, EventArgs e)
        {
            WindowMain.UndoStore.UndoStoreChanged += new UndoStoreHandler(UndoStore_UndoStoreChanged);
        }

        private void UndoStore_UndoStoreChanged(object sender, UndoStoreEventArgs e)
        {
            if (e.List == WindowMain.UndoStore.UndoData)
            {
                OnPropertyChanged("ProjectChanged");
                WindowMain.UndoStore.UndoStoreChanged -= new UndoStoreHandler(UndoStore_UndoStoreChanged);
            }
        }

        /// <summary>
        /// Handles the AfterLoad event of the BrainData.
        /// </summary>
        /// <remarks>
        /// The first time that the undo data changes after a project has been loaded, we need to update the project status.
        /// </remarks>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ProjectData_AfterLoad(object sender, EventArgs e)
        {
            WindowMain.UndoStore.UndoStoreChanged += new UndoStoreHandler(UndoStore_UndoStoreChanged);
        }

        /// <summary>
        /// Handles the Changed event of the Brain.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void Project_Changed(object sender, EventArgs e)
        {
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(OnPropertyChanged), "ProjectChanged");
        }

        #endregion Project changed updating

        #region Helpers

        /// <summary>
        /// Creates a new, empty project.
        /// </summary>
        public void CreateProject()
        {
            WindowMain.UndoStore.UndoStateStack.Push(UndoState.Blocked);                                             //during creation of the new project, we block any changes to the undo system, cause they are invalid (it's a new, fresh project).
            try
            {
                try
                {
                    ClearProject();
                    Brain.New();
                    if (BrainUndoMonitor.IsInstantiated == false)                                                         //we need to make certain the the brain undo monitor registers it's
                        BrainUndoMonitor.Instantiate();
                    BrainData.New();
                }
                catch (Exception e)
                {
                    Log.LogError("ProjectManager.CreateNewProject", e.ToString());
                    DataError = true;
                }
            }
            finally
            {
                WindowMain.UndoStore.UndoStateStack.Pop();
            }
        }

        /// <summary>
        /// Loads the brain data and designer data from disk.
        /// If the new location doesn't contain a brain and/or designer file, a new one is created.
        /// </summary>
        /// <remarks>
        /// Doesn't check if the previous project needs to be saved, simply clears it.
        /// </remarks>
        /// <param name="from">the directory to get the data from.</param>
        public void LoadProject(string from)
        {
            Cursor iPrev = Mouse.OverrideCursor;
            Mouse.OverrideCursor = Cursors.Wait;
            WindowMain.UndoStore.UndoStateStack.Push(UndoState.Blocked);                                             //during creation of the new project, we block any changes to the undo system, cause they are invalid (it's a new, fresh project).
            try
            {
                try
                {
                    ClearProject();
                    if (string.IsNullOrEmpty(from) == false)
                    {
                        LoadBrain(from);
                        if (BrainUndoMonitor.IsInstantiated == false)                                                         //we need to make certain the the brain undo monitor registers it's
                            BrainUndoMonitor.Instantiate();
                        LoadDesigner(from);
                        Location = from;
                        Log.LogInfo("ProjectManager.LoadProject", string.Format("Project '{0}' succesfully loaded.", from));
                    }
                    else
                        Log.LogError("ProjectManager.LoadProject", "Path to load is not defined.");
                }
                catch (Exception e)
                {
                    Log.LogError("ProjectManager.LoadProject", e.ToString());
                    DataError = true;
                }
            }
            finally
            {
                WindowMain.UndoStore.UndoStateStack.Pop();
                Mouse.OverrideCursor = iPrev;
            }
        }

        /// <summary>
        /// Saves the project to the current <see cref="ProjectManager.Location"/>.
        /// </summary>
        public void SaveProject()
        {
            Cursor iPrev = Mouse.OverrideCursor;
            try
            {
                Mouse.OverrideCursor = Cursors.Wait;
                try
                {
                    string iPath = Path.Combine(Location, NETWORKFILE);
                    Brain.Current.Save(iPath);
                    iPath = Path.Combine(Location, DESIGNERFILE);
                    using (FileStream iStr = new FileStream(iPath, FileMode.Create, FileAccess.ReadWrite))
                        BrainData.Current.Save(iStr);
                    Log.LogInfo("ProjectManager.SaveProject", string.Format("Project '{0}' succesfully saved.", Location));
                    OnPropertyChanged("ProjectChanged");
                    AddLastOpened(Location);
                }
                finally
                {
                    Mouse.OverrideCursor = iPrev;
                }
            }
            catch (Exception e)
            {
                Log.LogError("ProjectManager.SaveProject", e.ToString());
                DataError = true;
            }
        }

        /// <summary>
        /// Clears all the data from the project.
        /// </summary>
        private static void ClearProject()
        {
            WindowMain.UndoStore.UndoData.Clear();                                                             //make certain there is no undo data available from the previous project.
            BrainData.Current.Clear();                                                             //we release the designer data before the brain data so that the designer doesn't flip because all neurons are removed.
            Brain.Current.Clear();
        }

        /// <summary>
        /// Loads the designer part of the project
        /// </summary>
        /// <param name="from">From.</param>
        private static void LoadDesigner(string from)
        {
            string iPath = Path.Combine(from, DESIGNERFILE);
            if (File.Exists(iPath) == true)
            {
                using (FileStream iStr = new FileStream(iPath, FileMode.Open, FileAccess.Read))
                    BrainData.Load(iStr);
            }
            else
            {
                BrainData.New();
                Log.LogWarning("ProjectManager.LoadDesigner", string.Format("{0} not found, created new brain!", iPath));
            }
        }

        /// <summary>
        /// Loads the brain part of the project
        /// </summary>
        /// <param name="from">The location from where to load the data.</param>
        private static void LoadBrain(string from)
        {
            string iPath = Path.Combine(from, NETWORKFILE);
            if (File.Exists(iPath) == true)
            {
                Brain.Load(iPath);
            }
            else
            {
                string iSubPath = PreparePathForProject(from);
                Brain.New();
                Brain.Current.Storage.DataPath = iSubPath;
                Log.LogWarning("ProjectManager.LoadBrain", string.Format("{0} not found, created new brain!", iPath));
            }
        }

        /// <summary>
        /// Saves the settings used by the ProjectManager to the application settings.
        /// </summary>
        internal void SaveSettings()
        {
            if (Properties.Settings.Default.LastOpened != null)
                Properties.Settings.Default.LastOpened.Clear();
            else
                Properties.Settings.Default.LastOpened = new System.Collections.Specialized.StringCollection();
            Properties.Settings.Default.LastOpened.AddRange(LastOpened.ToArray());
            Properties.Settings.Default.SandboxPath = SandboxLocation;
        }

        /// <summary>
        /// Copies the entire project to the new directory
        /// </summary>
        /// <param name="loc">The location to copy the project to.</param>
        public void CopyProjectTo(string loc, string dataLoc)
        {
            File.Copy(Path.Combine(Location, DESIGNERFILE), Path.Combine(loc, DESIGNERFILE));
            File.Copy(Path.Combine(Location, NETWORKFILE), Path.Combine(loc, NETWORKFILE));
            Brain.Current.Storage.CopyTo(dataLoc);
        }

        /// <summary>
        /// Copies all the project data to new location.  The brain data is also moved, keeping the same sub dir name for the brain
        /// data.
        /// </summary>
        /// <param name="newLoc">The new loc.</param>
        private void MoveDataTo(string loc)
        {
            ILongtermMem iStorage = Brain.Current.Storage;
            Debug.Assert(iStorage != null);
            string iDataPath = PreparePathForProject(loc);
            iStorage.CopyTo(iDataPath);
            iStorage.DataPath = iDataPath;
            Location = loc;
        }

        /// <summary>
        /// Prepares the path for the project and returns the data path to use for the new location.
        /// </summary>
        /// <remarks>
        /// Checks if the location exists + wether it is a sub path called 'data'.  If not, both are created.
        /// </remarks>
        /// <param name="loc">The path to prepare for the project.</param>
        /// <returns>The data path for the new loc</returns>
        private static string PreparePathForProject(string loc)
        {
            string iDataPath = Path.Combine(loc, "Data");
            if (Directory.Exists(loc) == false)
                Directory.CreateDirectory(loc);
            if (Directory.Exists(iDataPath) == true)                                         //we delete all files in the data path when we prepare a location.  We do this to make certain that any previous data is gone.
            {
                foreach (string i in Directory.GetFiles(iDataPath))
                    File.Delete(i);
            }
            else
                Directory.CreateDirectory(iDataPath);

            return iDataPath;
        }

        #endregion Helpers
    }
}