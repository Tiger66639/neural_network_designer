﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Designer
{
    #region events

    public class BreakPointEventArgs : EventArgs
    {
        /// <summary>
        /// The breakpoint for which this event was raised.
        /// </summary>
        public Expression BreakPoint { get; set; }

        /// <summary>
        /// The proccessor that triggered the event. (this is usually a debug processor.
        /// </summary>
        public Processor Processor { get; set; }
    }

    /// <summary>
    /// A delegate for events raised by the <see cref="BreakPointCollection"/> containing the expression for which the event
    /// was raised.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void BreakPointEventHandler(object sender, BreakPointEventArgs e);

    #endregion events

    /// <summary>
    /// A collection used to store breakpoints.
    /// </summary>
    public class BreakPointCollection : ICollection<Expression>, INotifyCollectionChanged, IXmlSerializable, IWeakEventListener
    {
        private Dictionary<ulong, Expression> fItems = new Dictionary<ulong, Expression>();                                    //we use a dict for fast searching.

        /// <summary>
        /// Raised when a breakpoint has been reached and a debugger is waiting for an action.
        /// </summary>
        public event BreakPointEventHandler BreakPointReached;

        internal void OnBreakPointReached(Expression toProcess, Processor processor)
        {
            if (BreakPointReached != null)
                BreakPointReached(this, new BreakPointEventArgs() { BreakPoint = toProcess, Processor = processor });
        }

        #region ICollection<Expression> Members

        /// <summary>
        /// Add the item as a breakpoint.
        /// </summary>
        /// <param name="item"></param>
        public void Add(Expression item)
        {
            if (fItems.ContainsKey(item.ID) == false)
            {
                fItems.Add(item.ID, item);
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
            }
        }

        /// <summary>
        /// Remove all the breakpoints.
        /// </summary>
        public void Clear()
        {
            fItems.Clear();
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        /// <summary>
        /// Check if the specified item has a breakpoint declard.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(Expression item)
        {
            return fItems.ContainsKey(item.ID);
        }

        /// <summary>
        /// Not implemented
        /// </summary>
        public void CopyTo(Expression[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns the number of breakpoints.
        /// </summary>
        public int Count
        {
            get { return fItems.Count; }
        }

        /// <summary>
        /// this list is not read only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the specified breakpoint.
        /// </summary>
        /// <param name="item">The item to remove.</param>
        /// <returns>true if it succeeed, otherwise false.</returns>
        public bool Remove(Expression item)
        {
            if (fItems.ContainsKey(item.ID) == true)
            {
                List<Expression> iTemp = fItems.Values.ToList();
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, iTemp.IndexOf(item)));
                return fItems.Remove(item.ID);
            }
            return false;
        }

        #endregion ICollection<Expression> Members

        #region IEnumerable<Expression> Members

        /// <summary>
        /// Gets an enumerator for the breakpoints.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<Expression> GetEnumerator()
        {
            return fItems.Values.GetEnumerator();
        }

        #endregion IEnumerable<Expression> Members

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return fItems.Values.GetEnumerator();
        }

        #endregion IEnumerable Members

        #region INotifyCollectionChanged Members

        /// <summary>
        /// Raised when a new breakpoint is added or removed.
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs args)
        {
            if (CollectionChanged != null)
                CollectionChanged(this, args);
        }

        #endregion INotifyCollectionChanged Members

        #region IXmlSerializable Members

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            bool wasEmpty = reader.IsEmptyElement;

            reader.Read();
            if (wasEmpty) return;
            while (reader.NodeType != XmlNodeType.EndElement)
            {
                reader.ReadStartElement("ID");
                string iVal = reader.ReadString();
                ulong iConverted = ulong.Parse(iVal);
                Expression iFound = Brain.Current[iConverted] as Expression;
                if (iFound != null)
                    fItems.Add(iFound.ID, iFound);
                else
                    Log.LogError("BreakPoints.Read", string.Format("Expression with ID {0} not found in brain.", iConverted));

                reader.ReadEndElement();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            foreach (ulong i in fItems.Keys)
            {
                writer.WriteStartElement("ID");
                writer.WriteString(i.ToString());
                writer.WriteEndElement();
            }
        }

        #endregion IXmlSerializable Members

        #region IWeakEventListener Members

        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            if (managerType == typeof(NeuronChangedEventManager))
            {
                Current_NeuronChanged(sender, (NeuronChangedEventArgs)e);
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Event handler for <see cref="Brain.NeuronChanged"/>
        /// </summary>
        /// <remarks>
        /// Do asyn cause this event can be raised from different threads.
        /// </remarks>
        private void Current_NeuronChanged(object sender, NeuronChangedEventArgs e)
        {
            Expression iSender = e.OriginalSource as Expression;
            if (iSender != null && fItems.ContainsKey(iSender.ID) == true)
                App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<NeuronChangedEventArgs>(InternalNeuronChanged), e);
        }

        /// <summary>
        /// for replace: replaces the item if it is in this list
        /// fore remove: removes the item.
        /// </summary>
        /// <param name="e">The <see cref="NeuralNetworkDesigne.HAB.NeuronChangedEventArgs"/> instance containing the event data.</param>
        private void InternalNeuronChanged(NeuronChangedEventArgs e)
        {
            Expression iSender = e.OriginalSource as Expression;
            Debug.Assert(iSender != null);
            if (e.Action == BrainAction.Removed)
                Remove(iSender);                                                             //simply try to remove the item.
            else if (e.Action == BrainAction.Changed && !(e is NeuronPropChangedEventArgs))     //it could also be that the properties of an item are simply changed, don't need to respond to this.
            {
                object iPrev = fItems[e.OriginalSourceID];
                fItems[e.OriginalSourceID] = (Expression)e.NewValue;
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, e.NewValue, iPrev));
            }
        }

        #endregion IWeakEventListener Members
    }
}