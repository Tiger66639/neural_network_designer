﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Data;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// The entry point for the designer regarding data.
    /// </summary>
    public class BrainData : ObservableObject
    {
        #region fields

        private static BrainData fCurrent;
        private DesignerDataFile fDesignerData;
        private object fLastUndoItem;                                                                                          //this is used to check if the data has changed since the last save.

        //ObservableCollection<DebugProcessor> fProcessors = new ObservableCollection<DebugProcessor>();
        private ObservableCollection<CodeEditor> fCodeEditors = new ObservableCollection<CodeEditor>();                     //don't use observedCollection, cause than an add/remove of a codeEditor would also be added to the undo/redo list, which we don't want.

        private OpenDocsCollection fOpenDocuments = new OpenDocsCollection();
        private IList<EditorBase> fCurrentEditorsList;
        private List<Neuron> fLinkLists;
        private List<Neuron> fClusterLists;
        private int fLearnCount;
        private ObservableCollection<ToolBoxItem> fInstructionToolBoxItems;

        #endregion fields

        #region Events

        /// <summary>
        /// Raised just before the Brain data specific to the app is saved.
        /// </summary>
        public event EventHandler BeforeSave;

        /// <summary>
        /// Raised just after the Brain data specific to the application is loaded.
        /// </summary>
        public event EventHandler AfterLoad;

        #endregion Events

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="BrainData"/> class.
        /// </summary>
        public BrainData()
        {
            fCodeEditors.CollectionChanged += new NotifyCollectionChangedEventHandler(fCodeEditors_CollectionChanged);
            CollectionChanging += new NotifyCollectionChangingEventHandler(BrainData_CollectionChanging);
        }

        #endregion ctor

        #region Prop

        #region Name

        /// <summary>
        /// Gets/sets the name of the project
        /// </summary>
        public string Name
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.Name;
                return null;
            }
            set
            {
                if (DesignerData != null)
                {
                    OnPropertyChanging("Name", DesignerData.Name, value);
                    DesignerData.Name = value;
                    OnPropertyChanged("Name");
                }
                else
                    throw new InvalidOperationException("No data file loaded.");
            }
        }

        #endregion Name

        #region DesignerData

        /// <summary>
        /// Gets/sets the data specific to the designer.
        /// </summary>
        /// <remarks>
        /// This includes a lot of the properties exposed by this class. This property makes certain that when a new
        /// value is loaded, old data is unloaded and new data is properly registered.
        /// </remarks>
        public DesignerDataFile DesignerData
        {
            get
            {
                return fDesignerData;
            }
            set
            {
                if (value != fDesignerData)
                {
                    if (fDesignerData != null)
                        Unregister(fDesignerData);
                    fDesignerData = value;
                    if (fDesignerData != null)
                        Register(fDesignerData);
                    Type iType = typeof(DesignerDataFile);                                              //for each property in the designerdata, raise the event that it is changed.
                    foreach (PropertyInfo iProp in iType.GetProperties())
                        OnPropertyChanged(iProp.Name);
                }
            }
        }

        #endregion DesignerData

        #region CommChannels

        /// <summary>
        /// Gets the list of communcation channels that are available for the currently loaded <see cref="Brain"/>.
        /// </summary>
        public CommChannelCollection CommChannels
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.CommChannels;
                return null;
            }
        }

        #endregion CommChannels

        #region Debugmode

        /// <summary>
        /// Gets/sets which level of debugging should be used by the <see cref="DebugProcessor"/>s used
        /// by this application.
        /// </summary>
        public DebugMode Debugmode
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.Debugmode;
                return DebugMode.Off;
            }
            set
            {
                if (DesignerData != null)
                {
                    DesignerData.Debugmode = value;
                    OnPropertyChanged("Debugmode");
                }
                else
                    throw new InvalidOperationException("No data file loaded.");
            }
        }

        #endregion Debugmode

        #region BreakOnException

        /// <summary>
        /// Gets/sets wether the processor should break when an exception occurs (error is logged).
        /// </summary>
        public bool BreakOnException
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.BreakOnException;
                return false;
            }
            set
            {
                if (DesignerData != null)
                {
                    DesignerData.BreakOnException = value;
                    OnPropertyChanged("BreakOnException");
                }
                else
                    throw new InvalidOperationException("No data file loaded.");
            }
        }

        #endregion BreakOnException

        #region BreakPoints

        /// <summary>
        /// Gets the list of all the expressions that are defined as break points.
        /// Remember to lock this field each time you use it cause this is accessed acrorss multiple threads (by the processors and the designer).
        /// </summary>
        public BreakPointCollection BreakPoints
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.BreakPoints;
                return null;
            }
        }

        #endregion BreakPoints

        #region ToolBoxItems

        /// <summary>
        /// Gets the list of toolbox items Loaded in the designer.
        /// </summary>
        /// <remarks>
        /// this only stores the <see cref="NeuronToolBoxItem"/>s, not all of them,
        /// caus <see cref="TypeToolBoxitem"/>s are hardcoded.
        /// </remarks>
        public ObservedCollection<ToolBoxItem> ToolBoxItems
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.ToolBoxItems;
                return null;
            }
        }

        #endregion ToolBoxItems

        #region InstructionToolBoxItems

        /// <summary>
        /// Gets the list of instructions as toolbox items.
        /// </summary>
        public ObservableCollection<ToolBoxItem> InstructionToolBoxItems
        {
            get
            {
                if (fInstructionToolBoxItems == null)
                {
                    fInstructionToolBoxItems = new ObservableCollection<ToolBoxItem>();
                    LoadInstructionsView();
                }
                return fInstructionToolBoxItems;
            }
            internal set
            {
                fInstructionToolBoxItems = value;
                OnPropertyChanged("InstructionToolBoxItems");
                OnPropertyChanged("InstructionsView");
            }
        }

        #endregion InstructionToolBoxItems

        #region InstructionsView

        public ListCollectionView InstructionsView
        {
            get
            {
                ListCollectionView iRes = new ListCollectionView(InstructionToolBoxItems);
                iRes.SortDescriptions.Add(new SortDescription("Category", ListSortDirection.Ascending));
                PropertyGroupDescription iDesc = new PropertyGroupDescription();
                iDesc.PropertyName = "Category";
                iRes.GroupDescriptions.Add(iDesc);
                return iRes;
            }
        }

        #endregion InstructionsView

        #region CodeEditors

        /// <summary>
        /// Gets the list of known code editor objects
        /// </summary>
        /// <remarks>
        /// this seperate list is required and needs to be stored in xml cause the 'OpenDocuments' list uses this
        /// to find the actual object as it only stores references.
        /// </remarks>
        [XmlIgnore]
        public ObservableCollection<CodeEditor> CodeEditors
        {
            get { return fCodeEditors; }
        }

        #endregion CodeEditors

        #region DefaultMeaningIds

        /// <summary>
        /// Gets the list of neuron id's that are frequently used as meaning neurons.
        /// </summary>
        /// <remarks>
        /// This list allows for fast editing of meaning values.
        /// This is the format in which it is saved, it is provided here, in case.
        /// </remarks>
        public NeuronIDCollection DefaultMeaningIds
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.DefaultMeaningIds;
                return null;
            }
        }

        #endregion DefaultMeaningIds

        #region Default meanings

        /// <summary>
        /// Gets a sorted list (on DisplayTitle) with all the neurons that are prefefined as meanings of links.
        /// </summary>
        /// <remarks>
        /// Don't keep this list alife, it gets recreated each time you call the getter,
        /// this is because the brain might unload memory objects, so they shouldn't be
        /// kept to long in mem.
        /// </remarks>
        [XmlIgnore]
        public List<Neuron> DefaultMeanings
        {
            get
            {
                List<Neuron> iRes = (from i in DefaultMeaningIds
                                     orderby BrainData.Current.NeuronInfo[i].DisplayTitle
                                     select Brain.Current[i]).ToList();
                return iRes;
            }
        }

        #endregion Default meanings

        #region Current

        /// <summary>
        /// Gets the extra data required by the designer for the <see cref="Brain"/>.
        /// </summary>
        public static BrainData Current
        {
            get
            {
                if (fCurrent == null)
                    fCurrent = new BrainData();
                return fCurrent;
            }
            private set
            {
                if (fCurrent != value)
                {
                    fCurrent = null;                                                  //important: when we get the undostore, it tries to create it if it doesn't exist + register this class, so we need  to be null when this is retrieved for the first time.
                    UndoStore iStore = WindowMain.UndoStore;
                    if (iStore != null)
                        WindowMain.UndoStore.Register(value);                          //important: need to do this before fCurrent is assigned
                    fCurrent = value;
                }
            }
        }

        #endregion Current

        #region OpenDocuments

        /// <summary>
        /// Gets the list of currently open documents.
        /// </summary>
        /// <remarks>
        /// </remarks>
        public OpenDocsCollection OpenDocuments
        {
            get
            {
                return fOpenDocuments;
            }
        }

        #endregion OpenDocuments

        #region NeuronInfo

        /// <summary>
        /// Gets the dictionary with all the objects containing extra info for the neurons.
        /// </summary>
        /// <remarks>
        /// When a neuron is created, there is no neurondata automatically created to go with it.
        /// Whenever an object
        /// </remarks>
        public NeuronInfoDictionary NeuronInfo
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.NeuronInfo;
                return null;
            }
        }

        #endregion NeuronInfo

        #region Editors

        /// <summary>
        /// Gets the Tree of editors defined in the current project.
        /// </summary>
        /// <remarks>
        /// This list contains all the root iems.  Some can be folders, so a tree like structure is possible.
        /// This is important for some events.
        /// </remarks>
        public EditorCollection Editors
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.Editors;
                return null;
            }
        }

        #endregion Editors

        #region CurrentEditorsList

        /// <summary>
        /// Gets/sets the currently active editors list.  This can be the root list (see <see cref="BrainData.Editors"/>)
        /// or a child folder node.
        /// </summary>
        public IList<EditorBase> CurrentEditorsList
        {
            get
            {
                return fCurrentEditorsList;
            }
            set
            {
                fCurrentEditorsList = value;
                OnPropertyChanged("CurrentEditorsList");
            }
        }

        #endregion CurrentEditorsList

        #region Instructions

        /// <summary>
        /// Gets the list of all the available instructions in the brain.
        /// </summary>
        /// <remarks>
        /// This property is used by the <see cref="CodeEditors"/>
        /// </remarks>
        public NeuronCollection<Instruction> Instructions
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.Instructions;
                return null;
            }
        }

        #endregion Instructions

        #region Operators

        /// <summary>
        /// Gets the list of all the available operators (as in Add, substract, equals,...) in the brain.
        /// </summary>
        /// <remarks>
        /// This property is used by the <see cref="CodeEditors"/> in some templates.
        /// </remarks>
        public NeuronCollection<Neuron> Operators
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.Operators;
                return null;
            }
        }

        #endregion Operators

        #region GrammarTypes

        /// <summary>
        /// Gets the list of all the neurons that represent the different grammar types (part of speech).
        /// </summary>
        public NeuronCollection<Neuron> GrammarTypes
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.GrammarTypes;
                return null;
            }
        }

        #endregion GrammarTypes

        #region FrameElementImportances

        /// <summary>
        /// Gets the list of neurons that can be used as frame element importances.
        /// </summary>
        /// <remarks>
        /// This is a helper prop so that frame element editors provide the same list of values.
        /// </remarks>
        /// <value>The frame element importances.</value>
        [XmlIgnore]
        public IList<Neuron> FrameElementImportances
        {
            get
            {
                List<Neuron> iRes = new List<Neuron>();
                iRes.Add(Brain.Current[(ulong)PredefinedNeurons.Frame_Core]);
                iRes.Add(Brain.Current[(ulong)PredefinedNeurons.Frame_peripheral]);
                iRes.Add(Brain.Current[(ulong)PredefinedNeurons.Frame_extra_thematic]);
                return iRes;
            }
        }

        #endregion FrameElementImportances

        #region PosValues

        /// <summary>
        /// Gets the list of possible pos values.
        /// </summary>
        /// <remarks>
        /// This is a helper prop so that editors can provide a drop down list.
        /// </remarks>
        [XmlIgnore]
        public IList<Neuron> PosValues
        {
            get
            {
                List<Neuron> iRes = new List<Neuron>();
                iRes.Add(Brain.Current[(ulong)PredefinedNeurons.Verb]);
                iRes.Add(Brain.Current[(ulong)PredefinedNeurons.Noun]);
                iRes.Add(Brain.Current[(ulong)PredefinedNeurons.Adverb]);
                iRes.Add(Brain.Current[(ulong)PredefinedNeurons.Adjective]);
                iRes.Add(Brain.Current[(ulong)PredefinedNeurons.Preposition]);
                iRes.Add(Brain.Current[(ulong)PredefinedNeurons.Conjunction]);
                iRes.Add(Brain.Current[(ulong)PredefinedNeurons.Intersection]);
                iRes.Add(Brain.Current[(ulong)PredefinedNeurons.Article]);
                iRes.Add(Brain.Current[(ulong)PredefinedNeurons.Determiner]);
                iRes.Add(Brain.Current[(ulong)PredefinedNeurons.Complementizer]);
                return iRes;
            }
        }

        #endregion PosValues

        #region Thesaurus

        /// <summary>
        /// Gets the thesaurus root neurons for the network.
        /// </summary>
        public Thesaurus Thesaurus
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.Thesaurus;
                return null;
            }
        }

        #endregion Thesaurus

        #region LinkLists

        /// <summary>
        /// Gets a list containing all the neurons used to identify lists of links that can be searched.  This is currently
        /// 'In' and 'Out'.
        /// </summary>
        /// <remarks>
        /// This is static info (core feature of the brain), so don't serialize.
        /// This prop is provided for xaml, is used on CodeEditor.
        /// </remarks>
        [XmlIgnore]
        public List<Neuron> LinkLists
        {
            get
            {
                if (fLinkLists == null)
                {
                    fLinkLists = new List<Neuron>();
                    fLinkLists.Add(Brain.Current[(ulong)PredefinedNeurons.In]);
                    fLinkLists.Add(Brain.Current[(ulong)PredefinedNeurons.Out]);
                }
                return fLinkLists;
            }
        }

        #endregion LinkLists

        #region ClusterLists

        /// <summary>
        /// Gets the list of neurons that represent lists related to clusters that can be searched. This is currently 'Chilren' and
        /// 'Clusters'.
        /// </summary>
        /// <remarks>
        /// This is static info (core feature of the brain), so don't serialize.
        /// This prop is provided for xaml, is used on CodeEditor.
        /// </remarks>
        [XmlIgnore]
        public List<Neuron> ClusterLists
        {
            get
            {
                if (fClusterLists == null)
                {
                    fClusterLists = new List<Neuron>();
                    fClusterLists.Add(Brain.Current[(ulong)PredefinedNeurons.Children]);
                    fClusterLists.Add(Brain.Current[(ulong)PredefinedNeurons.Clusters]);
                }
                return fClusterLists;
            }
        }

        #endregion ClusterLists

        #region IsChanged

        /// <summary>
        /// Gets the if the project data has been changed since the last save.
        /// </summary>
        /// <remarks>
        /// We use a little trick to see if the data is changed: we keep track of the last undo item
        /// </remarks>
        public bool IsChanged
        {
            get
            {
                if (WindowMain.UndoStore.UndoData.Count > 0)
                    return fLastUndoItem != WindowMain.UndoStore.UndoData[WindowMain.UndoStore.UndoData.Count - 1];
                else
                    return fLastUndoItem != null;
            }
        }

        #endregion IsChanged

        #region LearnCount

        /// <summary>
        /// Gets or sets the nr of units that are currently learning.
        /// </summary>
        /// <remarks>
        /// Used to adjust the <see cref="BrainData.IsLearning"/> value.
        /// </remarks>
        /// <value>The learn count.</value>
        internal int LearnCount
        {
            get { return fLearnCount; }
            set
            {
                fLearnCount = value;
                OnPropertyChanged("IsLearning");
            }
        }

        #endregion LearnCount

        #region IsLearning

        /// <summary>
        /// Gets wether the network is currently learning (aqcuiring) data from an external resource like wordnet, framenet, the internet,...
        /// </summary>
        /// <remarks>
        /// To change this value, use <see cref="BrainData.LearnCount"/>.
        /// </remarks>
        public bool IsLearning
        {
            get
            {
                return LearnCount > 0;
            }
        }

        #endregion IsLearning

        #endregion Prop

        #region Functions

        /// <summary>
        /// Unregisters the specified data.
        /// </summary>
        /// <remarks>
        /// Also resets the root <see cref="BrainData.CurrentEditorsList"/>.
        /// </remarks>
        /// <param name="data">The data.</param>
        private void Unregister(DesignerDataFile data)
        {
            data.CommChannels.Owner = null;
            data.ToolBoxItems.Owner = null;
            data.DefaultMeaningIds.Owner = null;
            data.Watches.Owner = null;
            data.Editors.Owner = null;
            CurrentEditorsList = null;
        }

        /// <summary>
        /// Registers the specified data.
        /// </summary>
        /// <remarks>
        /// Also sets the root <see cref="BrainData.CurrentEditorsList"/>.
        /// </remarks>
        /// <param name="data">The data.</param>
        private void Register(DesignerDataFile data)
        {
            data.CommChannels.Owner = this;
            foreach (CommChannel i in data.CommChannels)
                i.UpdateOpenDocuments();
            data.ToolBoxItems.Owner = this;
            data.DefaultMeaningIds.Owner = this;
            data.Watches.Owner = this;
            foreach (Watch i in data.Watches)
                i.RegisterNeuron();
            data.Editors.Owner = this;
            if (data.Thesaurus == null)                                                                     //to handle files that were created before the thesaurus was stored there, create a new one.
                data.Thesaurus = new Thesaurus();
            CurrentEditorsList = data.Editors;
            SortInstructions(data.Instructions);
            LoadInstructionsView();
            foreach (NeuronEditor i in data.Editors.AllNeuronEditors())                                     //neuronEditors need to be registered as well after loading
            {
                i.RegisterItem();
                if (i is CodeEditor)                                                                          //if we want to retrieve the correct code editors from the project, they also need to be loaded properly.
                    CodeEditors.Add((CodeEditor)i);
            }
        }

        private void SortInstructions(NeuronCollection<Instruction> list)
        {
            //we sort the instruction list, each time we load. this is saved to ensure the list is sorted.
            List<Instruction> iTemp = list.OrderBy(p => BrainData.Current.NeuronInfo[p.ID].DisplayTitle).ToList();       //we need to convert cause we are going to modify the list.
            list.Clear();
            foreach (Instruction i in iTemp)
                list.Add(i);
        }

        /// <summary>
        /// Loads all the instructions into a sorted view, ready for display.
        /// </summary>
        private void LoadInstructionsView()
        {
            if (Instructions != null)
            {
                foreach (Neuron i in Instructions)
                {
                    InstructionToolBoxItem iNew = new InstructionToolBoxItem();
                    iNew.Item = i;
                    InstructionToolBoxItems.Add(iNew);
                }
                OnPropertyChanged("InstructionsView");
            }
        }

        /// <summary>
        /// Creates a new Current object and loads the values for the properties from the specified stream.
        /// </summary>
        public static void Load(Stream aSource)
        {
            XmlSerializer iSer = new XmlSerializer(typeof(DesignerDataFile));
            iSer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);

            Current.DesignerData = (DesignerDataFile)iSer.Deserialize(aSource);
            Current.OnLoaded();
        }

        /// <summary>
        /// Raises the event.
        /// </summary>
        protected virtual void OnLoaded()
        {
            if (AfterLoad != null)
                AfterLoad(this, new EventArgs());
        }

        public void Save(Stream aDest)
        {
            OnBeforeSave();
            try
            {
                XmlSerializer iSer = new XmlSerializer(typeof(DesignerDataFile));
                using (TextWriter iWriter = new StreamWriter(aDest))
                    iSer.Serialize(iWriter, DesignerData);
                if (WindowMain.UndoStore.UndoData.Count > 0)
                    fLastUndoItem = WindowMain.UndoStore.UndoData[WindowMain.UndoStore.UndoData.Count - 1];
                else
                    fLastUndoItem = null;
            }
            catch (Exception e)
            {
                Log.LogError("BrainData.Save", e.Message);
            }
            OnAfterSave();
        }

        /// <summary>
        /// Performs custom actions after saving.
        /// </summary>
        private void OnAfterSave()
        {
            foreach (CodeEditor i in CodeEditors)                                                  //remove all the temp toolboxitems for the variables that can be used in code editors.
                i.UnParkRegisteredVariables();
        }

        /// <summary>
        /// Allows to perform + performs various actions before the data is saved.  Calls the apropriate event handlers.
        /// </summary>
        protected virtual void OnBeforeSave()
        {
            foreach (CodeEditor i in CodeEditors)                                                  //remove all the temp toolboxitems for the variables that can be used in code editors.
                i.ParkRegisteredVariables();

            if (BeforeSave != null)
                BeforeSave(this, new EventArgs());
        }

        #endregion Functions

        #region eventhandlers

        #region List syncing

        /// <summary>
        /// Called when the CodeEditors collection changes.
        /// </summary>
        /// <remarks>
        /// - Whenever an item is removed from this list, we need to make certain
        /// it is also removed from the open documents.
        /// </remarks>
        private void fCodeEditors_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            CleanOpenDocuments<CodeEditor>(e.Action, e.OldItems);
        }

        /// <summary>
        /// Handles the CollectionChanging event of the BrainData control.
        /// </summary>
        /// <remarks>
        /// We handle changes in the lists of the <see cref="DesignerDataFile"/> here so that we can
        /// handle tree structures correctly (if we attach to the list itself, it doesn't get called recurivelly).
        /// This recursiveness is important for the Editors list, cause it is a tree.
        /// </remarks>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="NeuralNetworkDesigne.UndoSystem.Interfaces.CollectionChangingEventArgs"/> instance containing the event data.</param>
        private void BrainData_CollectionChanging(object sender, CollectionChangingEventArgs e)
        {
            if (e.OrignalSource == CommChannels)
                CleanOpenDocuments(e);
            else if (e.OrignalSource == DefaultMeaningIds)
                OnPropertyChanged("DefaultMeanings");
            else if (e.OrignalSource == Editors)
                Editors_CollectionChanging(sender, e);
        }

        /// <summary>
        /// Handles the CollectionChanging event of the Editors control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="NeuralNetworkDesigne.UndoSystem.Interfaces.CollectionChangingEventArgs"/> instance containing the event data.</param>
        private void Editors_CollectionChanging(object sender, CollectionChangingEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Remove:
                    ((EditorBase)e.Item).RemoveEditorFrom(OpenDocuments);
                    break;

                case NotifyCollectionChangedAction.Replace:
                    ((EditorBase)e.Item).RemoveEditorFrom(OpenDocuments);
                    break;

                case NotifyCollectionChangedAction.Reset:
                    foreach (EditorBase i in e.Items)
                        i.RemoveEditorFrom(OpenDocuments);
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Cleans the list of open documents with regards to the specified action and involved items.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="e">The <see cref="NeuralNetworkDesigne.UndoSystem.Interfaces.CollectionChangingEventArgs"/> instance containing the event data.</param>
        private void CleanOpenDocuments(CollectionChangingEventArgs e)
        {
            if (OpenDocuments != null)                                                                   //this can be null if a clean is performed when there was no previous newPRoject called.
            {
                if (e.Action == NotifyCollectionChangedAction.Remove)
                    OpenDocuments.Remove(e.Item);
                else if (e.Action == NotifyCollectionChangedAction.Reset)
                {
                    foreach (object i in e.Items)
                        OpenDocuments.Remove(i);
                }
            }
        }

        /// <summary>
        /// Cleans the list of open documents with regards to the specified action and involved items.
        /// </summary>
        /// <typeparam name="T">The type of neuron that should be cleaned from the open documents.</typeparam>
        /// <param name="action">The action to perform: simple remove or complete reset.</param>
        /// <param name="toRemove">The items to remove.</param>
        private void CleanOpenDocuments<T>(NotifyCollectionChangedAction action, IList toRemove)
        {
            if (OpenDocuments != null)                                                                   //this can be null if a clean is performed when there was no previous newPRoject called.
            {
                if (action == NotifyCollectionChangedAction.Remove)
                {
                    foreach (object i in toRemove)
                        OpenDocuments.Remove(i);
                }
                else if (action == NotifyCollectionChangedAction.Reset)
                {
                    var iToRemove = (from i in OpenDocuments where i is T select i).ToList();
                    foreach (object i in iToRemove)
                        OpenDocuments.Remove(i);
                }
            }
        }

        #endregion List syncing

        private static void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            //We don't show an error for xsi:type attributes which are used to specify the type.
            if (e.Name != "xsi:type")
                Log.LogError("BrainData.Load", "L: " + e.LineNumber.ToString() + "P: " + e.LinePosition.ToString() + ": unknown node in stream: " + e.Name + "/n contains: " + e.Text);
        }

        #endregion eventhandlers

        /// <summary>
        /// Clears all the data from this instance.
        /// </summary>
        public void Clear()
        {
            DesignerData = null;
            fCodeEditors.Clear();
            OpenDocuments.Clear();                                                                       //just to be save that there are no open documents left
            InstructionToolBoxItems = null;                                                              //also need to reset the instructions toolbox so that all items get reloaded when a new project is loaded.
        }

        /// <summary>
        /// Creates a new <see cref="BrainData.Current"/>
        /// </summary>
        /// <remarks>
        /// Items created:
        /// -all default toolbox items
        /// -the wordnetSin channel.
        /// </remarks>
        public static void New()
        {
            DesignerDataFile iData = new DesignerDataFile();                                                   //load the default (empty) data.
            iData.Name = "New project";
            iData.CommChannels = new CommChannelCollection();
            iData.BreakPoints = new BreakPointCollection();
            iData.ToolBoxItems = new ObservedCollection<ToolBoxItem>();
            iData.PlaySpeed = new TimeSpan(0, 0, 0, 2, 0);
            iData.DefaultMeaningIds = new NeuronIDCollection();
            iData.NeuronInfo = new NeuronInfoDictionary();
            iData.Instructions = new NeuronCollection<Instruction>();
            iData.Operators = new NeuronCollection<Neuron>();
            iData.Watches = new ObservedCollection<Watch>();
            iData.Editors = new EditorCollection();
            iData.Thesaurus = new Thesaurus();
            Current.DesignerData = iData;

            Current.LoadDefaultToolBoxItems();

            WordNetChannel iWordNet = new WordNetChannel();
            iWordNet.NeuronID = (ulong)PredefinedNeurons.WordNetSin;
            iWordNet.NeuronInfo.DisplayTitle = "WordNet";
            Current.CommChannels.Add(iWordNet);
        }

        /// <summary>
        /// Loads all the default toolbox items, which are the <see cref="TypeToolBoxItem"/>s.
        /// </summary>
        private void LoadDefaultToolBoxItems()
        {
            LoadToolBoxItemFor(typeof(Neuron), "General", "Neuron");
            LoadToolBoxItemFor(typeof(NeuronCluster), "General", "Cluster");
            LoadToolBoxItemFor(typeof(TextNeuron), "General", "Text neuron");
            LoadToolBoxItemFor(typeof(TextSin), "General", "Text Sin");
            LoadToolBoxItemFor(typeof(IntNeuron), "General", "Int neuron");
            LoadToolBoxItemFor(typeof(DoubleNeuron), "General", "Double neuron");
            LoadToolBoxItemFor(typeof(KnoledgeNeuron), "General", "Knoledge neuron");

            LoadToolBoxItemFor(typeof(Assignment), "Code", "Assignment");
            LoadToolBoxItemFor(typeof(Statement), "Code", "Statement");
            LoadToolBoxItemFor(typeof(ResultStatement), "Code", "Result statement");
            LoadToolBoxItemFor(typeof(ExpressionsBlock), "Code", "Code block");
            LoadToolBoxItemFor(typeof(ConditionalStatement), "Code", "Condional statement");
            LoadToolBoxItemFor(typeof(ConditionalExpression), "Code", "condional part");
            LoadToolBoxItemFor(typeof(BoolExpression), "Code", "bool expression");
            LoadToolBoxItemFor(typeof(SearchExpression), "Code", "search expression");
            LoadToolBoxItemFor(typeof(Variable), "Code", "Variable");
            LoadToolBoxItemFor(typeof(Global), "Code", "Global");
            LoadToolBoxItemFor(typeof(ByRefExpression), "Code", "ByRef");

            LoadNeuronToolBoxItemFor((ulong)PredefinedNeurons.Empty, "Global values", "Empty");
            LoadNeuronToolBoxItemFor((ulong)PredefinedNeurons.BeginTextBlock, "Global values", "BeginTextBlock");
            LoadNeuronToolBoxItemFor((ulong)PredefinedNeurons.EndTextBlock, "Global values", "EndTextBlock");
            LoadNeuronToolBoxItemFor((ulong)PredefinedNeurons.CurrentSin, "Global values", "CurrentSin");
            LoadNeuronToolBoxItemFor((ulong)PredefinedNeurons.CurrentFrom, "Global values", "CurrentFrom");
            LoadNeuronToolBoxItemFor((ulong)PredefinedNeurons.CurrentTo, "Global values", "CurrentTo");
            LoadNeuronToolBoxItemFor((ulong)PredefinedNeurons.CurrentMeaning, "Global values", "CurrentMeaning");
            LoadNeuronToolBoxItemFor((ulong)PredefinedNeurons.CurrentInfo, "Global values", "CurrentInfo");
            LoadNeuronToolBoxItemFor((ulong)PredefinedNeurons.True, "Global values", "True");
            LoadNeuronToolBoxItemFor((ulong)PredefinedNeurons.False, "Global values", "False");

            LoadDefaultInstructions();

            LoadOperatorToolBoxItemFor((ulong)PredefinedNeurons.Equal, "Operators", "==");
            LoadOperatorToolBoxItemFor((ulong)PredefinedNeurons.Smaller, "Operators", "<");
            LoadOperatorToolBoxItemFor((ulong)PredefinedNeurons.Bigger, "Operators", ">");
            LoadOperatorToolBoxItemFor((ulong)PredefinedNeurons.SmallerOrEqual, "Operators", "<=");
            LoadOperatorToolBoxItemFor((ulong)PredefinedNeurons.BiggerOrEqual, "Operators", ">=");
            LoadOperatorToolBoxItemFor((ulong)PredefinedNeurons.Different, "Operators", "!=");
            LoadOperatorToolBoxItemFor((ulong)PredefinedNeurons.Contains, "Operators", "Contains");
            LoadOperatorToolBoxItemFor((ulong)PredefinedNeurons.And, "Operators", "&&");
            LoadOperatorToolBoxItemFor((ulong)PredefinedNeurons.Or, "Operators", "||");
        }

        private void LoadDefaultInstructions()
        {
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.AddChildInstruction, "Add", "Add Child");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.AddLinkInstruction, "Add", "Add Link");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.AddInfoInstruction, "Add", "Add Info");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ChangeLinkFrom, "Change", "Change link From");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ChangeLinkTo, "Change", "Change link To");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ChangeLinkMeaning, "Change", "Change link meaning");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ClearChildrenInstruction, "Clear", "Clear children");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ClearInfoInstruction, "Clear", "Clear info");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ClearLinksInInstruction, "Clear", "Clear links in");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ClearLinksOutInstruction, "Clear", "Clear links out");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.SToCcInstruction, "Convert", "SToCc");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.SToCiInstruction, "Convert", "SToCi");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.SToIInstruction, "Convert", "SToI");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.SToDInstruction, "Convert", "SToD");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.CcToSInstruction, "Convert", "CcToS");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.CiToSInstruction, "Convert", "CiToS");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.IToSInstruction, "Convert", "IToS");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.DToSInstruction, "Convert", "DToS");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.IToDInstruction, "Convert", "IToD");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.DToIInstruction, "Convert", "DToI");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetFirstChildInstruction, "Get first", "Get first child");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetFirClusterInstruction, "Get first", "Get first cluster");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetFirstInfoInstruction, "Get first", "Get first info");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetFirstInInstruction, "Get first", "Get first in");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetFirstOut, "Get first", "Get first out");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetLastChildInstruction, "Get last", "Get last child");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetLastClusterInstruction, "Get last", "Get last cluster");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetLastInfoInstruction, "Get last", "Get last info");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetLastInInstruction, "Get last", "Get last in");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetLastOutInstruction, "Get last", "Get last out");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetNextChildInstruction, "Get next", "Get next child");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetNextClusterInstruction, "Get next", "Get next cluster");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetNextInfoInstruction, "Get next", "Get next info");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetNextInInstruction, "Get next", "Get next in");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetNextOutInstruction, "Get next", "Get next out");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetPrevChildInstruction, "Get prev", "Get prev child");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetPrevClusterInstruction, "Get prev", "Get prev cluster");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetPrevInfoInstruction, "Get prev", "Get prev info");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetPrevInInstruction, "Get prev", "Get prev in");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetPrevOutInstruction, "Get prev", "Get prev out");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetChildrenInstruction, "Get", "Get children");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetChildrenOfTypeInstruction, "Get", "Get children of Type X");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetClustersInstruction, "Get", "Get clusters");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetClustersWithMeaningInstruction, "Get", "Get clusters with meaning X");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetClusterMeaningInstruction, "Get", "Get cluster meaning");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.IndexOfChildInstruction, "Index of", "IndexOf child");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.IndexOfInfoInstruction, "Index of", "IndexOf info");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.IndexOfLinkInstruction, "Index of", "IndexOf link");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.InsertChildInstruction, "Insert", "Insert Child");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.InsertLinkInstruction, "Insert", "Insert Link");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.InsertInfoInstruction, "Insert", "Insert info");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.CopyChildrenInstruction, "Copy/move", "Copy children");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.CopyInfoInstruction, "Copy/move", "Copy info");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.MoveChildrenInstruction, "Copy/move", "Move children");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.MoveInfoInstruction, "Copy/move", "Move info");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.NewInstruction, "Neurons", "New");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.DeleteInstruction, "Neurons", "Delete");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.DuplicateInstruction, "Neurons", "Duplicate");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.TypeOfInstruction, "Neurons", "TypeOf");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ExecuteInstruction, "Neurons", "Execute");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.MakeClusterInstruction, "Neurons", "Make cluster");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.SplitInstruction, "Processor split", "Split");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetSplitResultsInstruction, "Processor split", "Get Split results");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ClearSplitResultsInstruction, "Processor split", "Clear split results");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.RemoveSplitResultInstruction, "Processor split", "Remove split result");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.AddSplitResultInstruction, "Processor split", "Add split result");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.PeekInstruction, "Processor stack", "Peek");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.PopInstruction, "Processor stack", "Pop");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.PushInstruction, "Processor stack", "Push");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.StackCountInstruction, "Processor stack", "Stack count");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetWeightInstruction, "Processor weight", "Get Weight");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetMaxWeightInstruction, "Processor weight", "Get Max Weight");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.IncreaseWeightInstruction, "Processor weight", "Increase Weight");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.DecreaseWeightInstruction, "Processor weight", "Decrease Weight");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ResetWeightInstruction, "Processor weight", "Reset Weight");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ContinueInstruction, "Processor", "Continue");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.BreakInstruction, "Processor", "Break");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.AwakeInstruction, "Processor", "Awake");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.SuspendInstruction, "Processor", "Suspend");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ExecuteInstruction, "Processor", "Execute");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.OutputInstruction, "Processor", "Output");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.SolveInstruction, "Processor", "Solve");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.CallInstruction, "Processor", "Call");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.BlockedSolveInstruction, "Processor", "Blocked Solve");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ExitLinkInstruction, "Processor", "Exit Link");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ExitNeuronInstruction, "Processor", "Exit Neuron");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ExitSolveInstruction, "Processor", "Exit Solve");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.WarningInstruction, "Processor", "Warning");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ErrorInstruction, "Processor", "Error");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.CompleteSequenceInstruction, "Processor", "Complete sequence");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.RemoveChildInstruction, "Remove", "Remove child");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.RemoveLinkInstruction, "Remove", "Remove link");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.RemoveInfoInstruction, "Remove", "Remove info");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.SetClusterMeaningInstruction, "Set", "Set cluster meaning");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.SetFirstOut, "Set", "Set first link Out");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.UnionInstruction, "Set operations", "Union");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.IntersectInstruction, "Set operations", "Intersect");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.InterleafInstruction, "Set operations", "Interleaf");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ReverseInstruction, "Set operations", "Reverse");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.CountInstruction, "var operations", "Count");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetFirstInstruction, "var operations", "First");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetLastInstruction, "var operations", "Last");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ClearInstruction, "var operations", "Clear");
        }

        private void LoadNeuronToolBoxItemFor(ulong item, string category, string title)
        {
            NeuronToolBoxItem iNew = new NeuronToolBoxItem();
            iNew.Item = Brain.Current[item];
            iNew.NeuronInfo.Category = category;
            iNew.NeuronInfo.DisplayTitle = title;
            ToolBoxItems.Add(iNew);
        }

        private void LoadOperatorToolBoxItemFor(ulong item, string category, string title)
        {
            NeuronToolBoxItem iNew = new NeuronToolBoxItem();
            iNew.Item = Brain.Current[item];
            iNew.NeuronInfo.Category = category;
            iNew.NeuronInfo.DisplayTitle = title;
            ToolBoxItems.Add(iNew);
            Operators.Add(iNew.Item);
        }

        /// <summary>
        /// Loads the instruction tool box item for.
        /// </summary>
        /// <remarks>
        /// Don't actually create the toolbox item, this is created dynamically later on.
        /// </remarks>
        /// <param name="item">The item.</param>
        /// <param name="category">The category.</param>
        /// <param name="title">The title.</param>
        private void LoadInstructionToolBoxItemFor(ulong item, string category, string title)
        {
            try
            {
                NeuronData iData = NeuronInfo[item];
                iData.Category = category;
                iData.DisplayTitle = title;
                Instructions.Add(iData.Neuron as Instruction);
            }
            catch (Exception e)
            {
                Log.LogError("BrainData.LoadInstructionToolBoxItemFor", e.ToString());
            }
        }

        /// <summary>
        /// Creates a typed toolbox item, sets everything up and adds it to the list of toolbox items.
        /// </summary>
        /// <param name="type">The type for which we create a toolbox item.</param>
        /// <param name="category">The category name to assign to the toolbox item.</param>
        private void LoadToolBoxItemFor(Type type, string category, string title)
        {
            TypeToolBoxItem iNew = new TypeToolBoxItem();
            iNew.ItemType = type;
            iNew.DisplayTitle = title;
            iNew.Category = category;
            ToolBoxItems.Add(iNew);
        }

        /// <summary>
        /// Reloads all the toolbox items so that the toolbox is back to the default state.
        /// </summary>
        public void ReloadToolboxItems()
        {
            if (DesignerData != null)                                                                 //we can only reset if there is any data.
            {
                Instructions.Clear();
                Operators.Clear();                                                                     //operator list is also rebuild.
                ToolBoxItems.Clear();
                LoadDefaultToolBoxItems();
                SortInstructions(Instructions);
                InstructionToolBoxItems.Clear();                                                       //don't need to completele delete, can simply rebuild the list.
                LoadInstructionsView();
            }
        }

        /// <summary>
        /// Shortcut to register ALL NeuronData objects directly to the root.
        /// </summary>
        /// <param name="item">The item.</param>
        internal void RegisterNeuronData(NeuronData item)
        {
            RegisterChild(item);
        }
    }
}