﻿namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// An interface for all objects that are wrappers for a neuron.
    /// </summary>
    public interface INeuronWrapper
    {
        Neuron Item { get; }
    }
}