﻿#pragma checksum "..\..\..\..\Editors\MindMap\ItemResizer.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "F5AC20BE8A8C9664124B719FA365D5D0"
//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.42000
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace NeuralNetworkDesigne.HAB.Designer {
    
    
    /// <summary>
    /// ItemResizer
    /// </summary>
    public partial class ItemResizer : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 4 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal NeuralNetworkDesigne.HAB.Designer.ItemResizer This;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/HAB.Designer;component/editors/mindmap/itemresizer.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.This = ((NeuralNetworkDesigne.HAB.Designer.ItemResizer)(target));
            return;
            case 2:
            
            #line 34 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragDelta += new System.Windows.Controls.Primitives.DragDeltaEventHandler(this.LTDragged);
            
            #line default
            #line hidden
            
            #line 35 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragStarted += new System.Windows.Controls.Primitives.DragStartedEventHandler(this.Thumb_DragStarted);
            
            #line default
            #line hidden
            
            #line 36 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragCompleted += new System.Windows.Controls.Primitives.DragCompletedEventHandler(this.Thumb_DragCompleted);
            
            #line default
            #line hidden
            return;
            case 3:
            
            #line 42 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragDelta += new System.Windows.Controls.Primitives.DragDeltaEventHandler(this.TDragged);
            
            #line default
            #line hidden
            
            #line 43 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragStarted += new System.Windows.Controls.Primitives.DragStartedEventHandler(this.Thumb_DragStarted);
            
            #line default
            #line hidden
            
            #line 44 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragCompleted += new System.Windows.Controls.Primitives.DragCompletedEventHandler(this.Thumb_DragCompleted);
            
            #line default
            #line hidden
            return;
            case 4:
            
            #line 48 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragDelta += new System.Windows.Controls.Primitives.DragDeltaEventHandler(this.RTDragged);
            
            #line default
            #line hidden
            
            #line 49 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragStarted += new System.Windows.Controls.Primitives.DragStartedEventHandler(this.Thumb_DragStarted);
            
            #line default
            #line hidden
            
            #line 50 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragCompleted += new System.Windows.Controls.Primitives.DragCompletedEventHandler(this.Thumb_DragCompleted);
            
            #line default
            #line hidden
            return;
            case 5:
            
            #line 57 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragDelta += new System.Windows.Controls.Primitives.DragDeltaEventHandler(this.LDragged);
            
            #line default
            #line hidden
            
            #line 58 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragStarted += new System.Windows.Controls.Primitives.DragStartedEventHandler(this.Thumb_DragStarted);
            
            #line default
            #line hidden
            
            #line 59 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragCompleted += new System.Windows.Controls.Primitives.DragCompletedEventHandler(this.Thumb_DragCompleted);
            
            #line default
            #line hidden
            return;
            case 6:
            
            #line 66 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragDelta += new System.Windows.Controls.Primitives.DragDeltaEventHandler(this.RDragged);
            
            #line default
            #line hidden
            
            #line 67 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragStarted += new System.Windows.Controls.Primitives.DragStartedEventHandler(this.Thumb_DragStarted);
            
            #line default
            #line hidden
            
            #line 68 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragCompleted += new System.Windows.Controls.Primitives.DragCompletedEventHandler(this.Thumb_DragCompleted);
            
            #line default
            #line hidden
            return;
            case 7:
            
            #line 73 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragDelta += new System.Windows.Controls.Primitives.DragDeltaEventHandler(this.LBDragged);
            
            #line default
            #line hidden
            
            #line 74 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragStarted += new System.Windows.Controls.Primitives.DragStartedEventHandler(this.Thumb_DragStarted);
            
            #line default
            #line hidden
            
            #line 75 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragCompleted += new System.Windows.Controls.Primitives.DragCompletedEventHandler(this.Thumb_DragCompleted);
            
            #line default
            #line hidden
            return;
            case 8:
            
            #line 82 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragDelta += new System.Windows.Controls.Primitives.DragDeltaEventHandler(this.BDragged);
            
            #line default
            #line hidden
            
            #line 83 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragStarted += new System.Windows.Controls.Primitives.DragStartedEventHandler(this.Thumb_DragStarted);
            
            #line default
            #line hidden
            
            #line 84 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragCompleted += new System.Windows.Controls.Primitives.DragCompletedEventHandler(this.Thumb_DragCompleted);
            
            #line default
            #line hidden
            return;
            case 9:
            
            #line 89 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragDelta += new System.Windows.Controls.Primitives.DragDeltaEventHandler(this.RBDragged);
            
            #line default
            #line hidden
            
            #line 90 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragStarted += new System.Windows.Controls.Primitives.DragStartedEventHandler(this.Thumb_DragStarted);
            
            #line default
            #line hidden
            
            #line 91 "..\..\..\..\Editors\MindMap\ItemResizer.xaml"
            ((System.Windows.Controls.Primitives.Thumb)(target)).DragCompleted += new System.Windows.Controls.Primitives.DragCompletedEventHandler(this.Thumb_DragCompleted);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

