﻿#pragma checksum "..\..\..\..\Editors\MindMap\MindMapNoteView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "2C29EE8985AE83B81A9AF3DC82314AEF"
//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.42000
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

using DnD;
using NeuralNetworkDesigne.HAB.Designer;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace NeuralNetworkDesigne.HAB.Designer {
    
    
    /// <summary>
    /// MindMapNoteView
    /// </summary>
    public partial class MindMapNoteView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 170 "..\..\..\..\Editors\MindMap\MindMapNoteView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RichTextBox RtfEditor;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/HAB.Designer;component/editors/mindmap/mindmapnoteview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Editors\MindMap\MindMapNoteView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 4 "..\..\..\..\Editors\MindMap\MindMapNoteView.xaml"
            ((NeuralNetworkDesigne.HAB.Designer.MindMapNoteView)(target)).Loaded += new System.Windows.RoutedEventHandler(this.UserControl_Loaded);
            
            #line default
            #line hidden
            
            #line 5 "..\..\..\..\Editors\MindMap\MindMapNoteView.xaml"
            ((NeuralNetworkDesigne.HAB.Designer.MindMapNoteView)(target)).DataContextChanged += new System.Windows.DependencyPropertyChangedEventHandler(this.UserControl_DataContextChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.RtfEditor = ((System.Windows.Controls.RichTextBox)(target));
            
            #line 171 "..\..\..\..\Editors\MindMap\MindMapNoteView.xaml"
            this.RtfEditor.Loaded += new System.Windows.RoutedEventHandler(this.NoteEditorLoaded);
            
            #line default
            #line hidden
            
            #line 172 "..\..\..\..\Editors\MindMap\MindMapNoteView.xaml"
            this.RtfEditor.LostFocus += new System.Windows.RoutedEventHandler(this.NoteEditor_LostFocus);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

