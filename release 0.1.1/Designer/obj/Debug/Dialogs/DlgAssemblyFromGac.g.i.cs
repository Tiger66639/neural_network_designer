﻿#pragma checksum "..\..\..\Dialogs\DlgAssemblyFromGac.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "7BD9DFDA492808A8347BC022154363B5"
//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.42000
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

using NeuralNetworkDesigne.HAB;
using NeuralNetworkDesigne.HAB.Designer.Properties;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace NeuralNetworkDesigne.HAB.Designer {
    
    
    /// <summary>
    /// DlgAssemblyFromGac
    /// </summary>
    public partial class DlgAssemblyFromGac : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\..\Dialogs\DlgAssemblyFromGac.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal NeuralNetworkDesigne.HAB.Designer.DlgAssemblyFromGac @this;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\Dialogs\DlgAssemblyFromGac.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem TabLoaded;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\Dialogs\DlgAssemblyFromGac.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox LstLoaded;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\Dialogs\DlgAssemblyFromGac.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem TabGAC;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\Dialogs\DlgAssemblyFromGac.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox LstGAC;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/HAB.Designer;component/dialogs/dlgassemblyfromgac.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Dialogs\DlgAssemblyFromGac.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.@this = ((NeuralNetworkDesigne.HAB.Designer.DlgAssemblyFromGac)(target));
            return;
            case 2:
            
            #line 25 "..\..\..\Dialogs\DlgAssemblyFromGac.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.OnClickOk);
            
            #line default
            #line hidden
            return;
            case 3:
            this.TabLoaded = ((System.Windows.Controls.TabItem)(target));
            return;
            case 4:
            this.LstLoaded = ((System.Windows.Controls.ListBox)(target));
            return;
            case 5:
            this.TabGAC = ((System.Windows.Controls.TabItem)(target));
            return;
            case 6:
            this.LstGAC = ((System.Windows.Controls.ListBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

