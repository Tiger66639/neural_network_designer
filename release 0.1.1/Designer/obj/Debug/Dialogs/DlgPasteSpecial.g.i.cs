﻿#pragma checksum "..\..\..\Dialogs\DlgPasteSpecial.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "925DF9F8E71D064AD24CC3AC3A0087C4"
//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.42000
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace NeuralNetworkDesigne.HAB.Designer {
    
    
    /// <summary>
    /// DlgPasteSpecial
    /// </summary>
    public partial class DlgPasteSpecial : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 5 "..\..\..\Dialogs\DlgPasteSpecial.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal NeuralNetworkDesigne.HAB.Designer.DlgPasteSpecial @this;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\Dialogs\DlgPasteSpecial.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton BtnReference;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\Dialogs\DlgPasteSpecial.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton BtnDuplicate;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/HAB.Designer;component/dialogs/dlgpastespecial.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Dialogs\DlgPasteSpecial.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.@this = ((NeuralNetworkDesigne.HAB.Designer.DlgPasteSpecial)(target));
            return;
            case 2:
            
            #line 23 "..\..\..\Dialogs\DlgPasteSpecial.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.OnClickOk);
            
            #line default
            #line hidden
            return;
            case 3:
            this.BtnReference = ((System.Windows.Controls.RadioButton)(target));
            
            #line 44 "..\..\..\Dialogs\DlgPasteSpecial.xaml"
            this.BtnReference.Checked += new System.Windows.RoutedEventHandler(this.Reference_Checked);
            
            #line default
            #line hidden
            return;
            case 4:
            this.BtnDuplicate = ((System.Windows.Controls.RadioButton)(target));
            
            #line 47 "..\..\..\Dialogs\DlgPasteSpecial.xaml"
            this.BtnDuplicate.Checked += new System.Windows.RoutedEventHandler(this.Duplicate_Checked);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

