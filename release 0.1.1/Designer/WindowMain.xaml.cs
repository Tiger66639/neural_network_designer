﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;
using NeuralNetworkDesigne.DockingControl.Controls;
using NeuralNetworkDesigne.UndoSystem;
using NeuralNetworkDesigne.WPFLog;
using NeuralNetworkDesigne.ControlFramework.Controls;
using NeuralNetworkDesigne.ControlFramework.Utility;
using NeuralNetworkDesigne.LogService;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Interaction logic for Window1.xaml
   /// </summary>
   public partial class WindowMain : Window, IWeakEventListener
   {

      #region fields

      IDescriptionable fCurrentDesc;                                                 //a ref to the object currently displayed in the global richtextbox description editor (so we can save the data again to it).
      INeuronInfo fCurrentNeuronInfo;                                               //goes together with fCurrentDoc.  If this field is set, it indicates the neuroninfo from which we are depicting a description (doesn't have to be filled in).

      static UndoStore fUndoStore;                                                  //stores a reference to the undo store that should be used.
      Help fHelp;

      #endregion


      #region ctor -dtor
      /// <summary>
      /// Initializes a new instance of the <see cref="WindowMain"/> class.
      /// </summary>
      public WindowMain()
      {
         BeforeSaveEventManager.AddListener(BrainData.Current, this);
         fHelp = new Help();
         fHelp.DefaultHelpFile = "NND.chm";
         InitializeComponent();
      }

      /// <summary>
      /// Releases unmanaged resources and performs other cleanup operations before the
      /// <see cref="WindowMain"/> is reclaimed by garbage collection.
      /// </summary>
      ~WindowMain()
      {
         BeforeSaveEventManager.RemoveListener(BrainData.Current, this);
      }

      #endregion

      #region prop


      static public UndoStore UndoStore
      {
         get 
         {
            if (fUndoStore == null)
            {
               fUndoStore = new UndoStore();
               fUndoStore.UndoStoreChanged += new UndoStoreHandler(fUndoStore_UndoStoreChanged);

               if (BrainData.Current != null)
                  fUndoStore.Register(BrainData.Current);
            }
            return fUndoStore; 
         }
      }

      static void fUndoStore_UndoStoreChanged(object sender, UndoStoreEventArgs e)
      {
         CollectionUndoItem iUndo = e.Item as CollectionUndoItem;
         if (iUndo != null)
         {
            
         }
      }
   



      #endregion

      #region Current

      /// <summary>
      /// Gets the current main window.
      /// </summary>
      public static WindowMain Current
      {
         get { return (WindowMain)App.Current.MainWindow; }
      }

      #endregion

      #region Helpers

      /// <summary>
      /// Flushes the description data back to where it came from.
      /// </summary>
      /// <param name="focused">The Richtextbox that was focused, if any.</param>
      private void FlushDescriptionData(RichTextBox focused)
      {
         if (focused != null && VwDescription.IsChanged == true)
         {
            if(fCurrentDesc !=null)
               fCurrentDesc.Description = VwDescription.Document;
         }
      } 

      /// <summary>
      /// Adds the item to brain taking care of the undo data.
      /// </summary>
      /// <param name="value">The neuron to add to the brain.</param>
      public static void AddItemToBrain(Neuron value)
      {
         Brain.Current.Add(value);
         NeuronUndoItem iUndoData = new NeuronUndoItem() { Neuron = value, ID = value.ID, Action = BrainAction.Created };
         WindowMain.UndoStore.AddCustomUndoItem(iUndoData);
      }

      /// <summary>
      /// Deletes the item from brain taking care of the undo data.
      /// </summary>
      /// <param name="toDelete">The neuron to delete from the brain.</param>
      /// 
      public static void DeleteItemFromBrain(Neuron toDelete)
      {
         NeuronUndoItem iUndoData = new NeuronUndoItem() { Neuron = toDelete, ID = toDelete.ID, Action = BrainAction.Removed };
         UndoStore.AddCustomUndoItem(iUndoData);
         if (Brain.Current.Delete(toDelete) == false)
         {
            string iMsg = string.Format("Neuron can't be deleted: {0}.", toDelete);
            MessageBox.Show(iMsg, "Delete neuron", MessageBoxButton.OK, MessageBoxImage.Error);
            Log.LogError("WindowMain.DeleteItemFromBrain", iMsg);
         }
      }

      /// <summary>
      /// Deletes the item from brain recursivelly. All Neurons pointed to from the LinksOut section of the
      /// neuron being deleted will also be removed.
      /// </summary>
      /// <remarks>
      /// <para>
      /// Removing a branch (so a neuron and all neurons that link to it) is usefull to remove peaces of code
      /// for instance.
      /// </para>
      /// <para>
      /// This function always creates an undo block so that all neurons removed by this operation can be recreated.
      /// </para>
      /// </remarks>
      /// <param name="toDelete">To delete.</param>
      /// <param name="forced">if set to <c>false</c> only neurons from the branch will be removed that don't have any references
      /// anymore (starting from the leaf, so when all leafs are removed, the parent is evaluated.</param>
      public static void DeleteBranchFromBrain(Neuron toDelete, bool forced)
      {
         UndoStore.BeginUndoGroup(false);
         try
         {
            DeleteBranchChildren(toDelete, forced);
            DeleteNeuron(toDelete);
         }
         finally
         {
            UndoStore.EndUndoGroup();
         }
         
      }

      /// <summary>
      /// Deletes a single neuron and generates undo data.
      /// </summary>
      /// <param name="toDelete">The item to delete.</param>
      private static void DeleteNeuron(Neuron toDelete)
      {
         if (toDelete.CanBeDeleted == true)
         {
            NeuronUndoItem iUndoData = new NeuronUndoItem() { Neuron = toDelete, ID = toDelete.ID, Action = BrainAction.Removed };
            UndoStore.AddCustomUndoItem(iUndoData);
            Brain.Current.Delete(toDelete);
         }
      }

      /// <summary>
      /// deletes all the neurons related to 'TodDelete' that should also be deleted (because they are children of 'toDelete'
      /// or they receive a link from 'toDelete' and have no other incomming links (this is checked recursivelly).
      /// </summary>
      /// <param name="toDelete">To delete.</param>
      /// <param name="forced">if set to <c>true</c> linked to items will always be deleted, also if they have more than 1 incomming link.</param>
      public static void DeleteBranchChildren(Neuron toDelete, bool forced)
      {
         bool iCountOk = false;
         bool iClusteredByOk = false;
         List<Link> iLinksOut;
         using (LinksAccessor iToDelLinksOut = toDelete.LinksOut)                                                    //only way to access it thread safe
            iLinksOut = new List<Link>(iToDelLinksOut.Items);                                                  //we make a copy cause we are going to delete items, if we don't make a copy, we would get an error that we are trying to modify the list inside a foreach.
         foreach (Link i in iLinksOut)
         {
            if (forced == false)
            {
               using (LinksAccessor iToLinksIn = i.To.LinksIn)                                                             //we need to access this info in a thread safe way. Offcourse, if we want to delete the item, we can't keep the reader lock in the same thread cause that blocks, so we need to do it this way.
                  iCountOk = iToLinksIn.Items.Count == 1;
               using (NeuronsAccessor iClusteredBy = i.To.ClusteredBy)                                                             //we need to access this info in a thread safe way. Offcourse, if we want to delete the item, we can't keep the reader lock in the same thread cause that blocks, so we need to do it this way.            
                  iClusteredByOk = iClusteredBy.Items.Count == 0;
            }
            if (forced == true || (iCountOk == true && iClusteredByOk == true))
            {
               Neuron iTo = i.To;
               DeleteBranchChildren(iTo, forced);
               DeleteNeuron(iTo);
            }
         }
         NeuronCluster iToDelete = toDelete as NeuronCluster;
         if (iToDelete != null)
         {
            List<Neuron> iChildren;
            using (ChildrenAccessor iList = iToDelete.Children)
               iChildren = iList.ConvertTo<Neuron>();
            foreach (Neuron i in iChildren)
            {
               if (forced == false)
               {
                  using (LinksAccessor iLinksIn = i.LinksIn)                                                             //we need to access this info in a thread safe way. Offcourse, if we want to delete the item, we can't keep the reader lock in the same thread cause that blocks, so we need to do it this way.
                     iCountOk = iLinksIn.Items.Count == 0;
                  using (NeuronsAccessor iClusteredBy = i.ClusteredBy)                                                             //we need to access this info in a thread safe way. Offcourse, if we want to delete the item, we can't keep the reader lock in the same thread cause that blocks, so we need to do it this way.            
                     iClusteredByOk = iClusteredBy.Items.Count == 1;
               }
               if (forced == true || (i.CanBeDeleted == true && iCountOk == true && iClusteredByOk == true))                           //only remove children that are nowhere else used.
               {
                  DeleteBranchChildren(i, forced);
                  DeleteNeuron(i);
               }
            }
         }
      }

      #endregion

      #region commands

      //#region Redo
      ///// <summary>
      ///// Handles the CanExecute event of the Redo control.
      ///// </summary>
      ///// <remarks>
      ///// We override Undo/Redo so we can change the state of the MindMap to 'Undoing', which is important to block certain 
      ///// automatic data rendering of the <see cref="MindMapItemCollection"/> container. 
      ///// </remarks>
      ///// <param name="sender">The source of the event.</param>
      ///// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      //private void Redo_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      //{
      //   WindowMain.UndoStore.Redo_CanExecute(sender, e);
      //}

      ///// <summary>
      ///// Handles the Executed event of the Redo control.
      ///// </summary>
      ///// <remarks>
      ///// We override Undo/Redo so we can change the state of the MindMap to 'Undoing', which is important to block certain 
      ///// automatic data rendering of the <see cref="MindMapItemCollection"/> container. 
      ///// </remarks>
      ///// <param name="sender">The source of the event.</param>
      ///// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      //private void Redo_Executed(object sender, ExecutedRoutedEventArgs e)
      //{
      //   foreach (MindMap i in BrainData.Current.MindMaps)
      //      i.CurrentState = EditorState.Undoing;
      //   MindMap iMap = (MindMap)DataContext;
      //   try
      //   {
      //      WindowMain.UndoStore.Redo_OnExecuted(sender, e);
      //   }
      //   finally
      //   {
      //      foreach (MindMap i in BrainData.Current.MindMaps)
      //         i.CurrentState = EditorState.Loaded;
      //   }
      //}
      //#endregion

      //#region Undo
      ///// <summary>
      ///// Handles the CanExecute event of the Undo control.
      ///// </summary>
      ///// <remarks>
      ///// We override Undo/Redo so we can change the state of the MindMap to 'Undoing', which is important to block certain 
      ///// automatic data rendering of the <see cref="MindMapItemCollection"/> container. 
      ///// </remarks>
      ///// <param name="sender">The source of the event.</param>
      ///// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      //private void Undo_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      //{
      //   WindowMain.UndoStore.Undo_CanExecute(sender, e);
      //}

      ///// <summary>
      ///// Handles the Executed event of the Undo control.
      ///// </summary>
      ///// <remarks>
      ///// We override Undo/Redo so we can change the state of the MindMap to 'Undoing', which is important to block certain 
      ///// automatic data rendering of the <see cref="MindMapItemCollection"/> container. 
      ///// </remarks>
      ///// <param name="sender">The source of the event.</param>
      ///// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      //private void Undo_Executed(object sender, ExecutedRoutedEventArgs e)
      //{
      //   foreach (MindMap i in BrainData.Current.MindMaps)
      //      i.CurrentState = EditorState.Undoing;
      //   try
      //   {
      //      WindowMain.UndoStore.Undo_OnExecuted(sender, e);
      //   }
      //   finally
      //   {
      //      foreach (MindMap i in BrainData.Current.MindMaps)
      //         i.CurrentState = EditorState.Loaded;
      //   }
      //}
      //#endregion 

      #region ViewCode
      /// <summary>
      /// Shows the code editor for the specified <see cref="Neuron"/>.  If there
      /// was already an editor opened for this item, it is made active.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void ViewCode_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         Neuron iNeuron = (Neuron)e.Parameter;
         if (iNeuron == null)
         {
            FrameworkElement iFocused = Keyboard.FocusedElement as FrameworkElement;
            if (iFocused != null && iFocused.DataContext is INeuronWrapper)
               iNeuron = ((INeuronWrapper)iFocused.DataContext).Item;
         }
         ViewCodeForNeuron(iNeuron);
      }

      /// <summary>
      /// Shows the code for the neuron.
      /// </summary>
      /// <param name="neuron">The euron.</param>
      /// <returns>The code editor that represents the code.</returns>
      public CodeEditor ViewCodeForNeuron(Neuron neuron)
      {
         CodeEditor iEditor = null;
         if (neuron != null)
         {
            iEditor = (from i in BrainData.Current.CodeEditors
                       where i.Item == neuron
                       select i).FirstOrDefault();                                                                //we first check if the code editor is already open.
            if (iEditor == null)
            {
               iEditor = new CodeEditor(neuron);
               BrainData.Current.CodeEditors.Add(iEditor);                                                        //this will automatically trigger and add to the open docuements list.
               BrainData.Current.OpenDocuments.Add(iEditor);
            }
            else
            {
               if (BrainData.Current.OpenDocuments.Contains(iEditor) == false)
                  BrainData.Current.OpenDocuments.Add(iEditor);
            }
            ToolFrame iFrame = TfData.ItemContainerGenerator.ContainerFromItem(iEditor) as ToolFrame;
            if (iFrame != null)
               iFrame.IsSelected = true;
         }
         else
            MessageBox.Show("Can't find selected neuron!");
         return iEditor;
      }

      /// <summary>
      /// Handles the CanExecute event of the ViewCode control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void ViewCode_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         FrameworkElement iFocused = Keyboard.FocusedElement as FrameworkElement;
         e.CanExecute = e.Parameter is Neuron || (iFocused != null && iFocused.DataContext is INeuronWrapper);
      } 

      #endregion

      private void Sync_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         Neuron iNeuron = (Neuron)e.Parameter;
         if (iNeuron == null)
         {
            FrameworkElement iFocused = Keyboard.FocusedElement as FrameworkElement;
            if (iFocused != null && iFocused.DataContext is INeuronWrapper)
               iNeuron = ((INeuronWrapper)iFocused.DataContext).Item;
         }
         SyncExplorerToNeuron(iNeuron);
      }

      /// <summary>
      /// Syncs the explorer to specified neuron.
      /// </summary>
      /// <param name="iNeuron">The i neuron.</param>
      public void SyncExplorerToNeuron(Neuron iNeuron)
      {
         if (iNeuron != null)
         {
            DockingControl.Controls.DockingControl.SetIsSelected(MainExplorer, true);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<ulong>(SyncMainExplorerTo), iNeuron.ID);
         }
         else
            MessageBox.Show("Can't find selected neuron!");
      }

      /// <summary>
      /// Syncs the main explorer to the specified id.  Allows us to do this async.
      /// </summary>
      /// <param name="id">The id.</param>
      void SyncMainExplorerTo(ulong id)
      {
         MainExplorer.SelectedID = id;
         MainExplorer.LstChildren.Focus();
      }


      #region Change to

      /// <summary>
      /// Handles the CanExecute event of the ChangeTo control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void ChangeTo_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         FrameworkElement iFocused = Keyboard.FocusedElement as FrameworkElement;
         FrameworkElement iSource = e.OriginalSource as FrameworkElement;
         e.CanExecute = e.Parameter is Type && ((iFocused != null && iFocused.DataContext is INeuronWrapper) || (iSource != null && iSource.DataContext is INeuronWrapper) );
      }

      /// <summary>
      /// Handles the Executed event of the ChangeTo control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void ChangeTo_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         Type iType = (Type)e.Parameter;
         Neuron iNeuron = null;
         FrameworkElement iFocused = Keyboard.FocusedElement as FrameworkElement;
         if (iFocused != null && iFocused.DataContext is INeuronWrapper)
            iNeuron = ((INeuronWrapper)iFocused.DataContext).Item;
         else
         {
            FrameworkElement iSource = e.OriginalSource as FrameworkElement;
            if(iSource != null && iSource.DataContext is INeuronWrapper)
               iNeuron = ((INeuronWrapper)iSource.DataContext).Item;
         }

         if (iNeuron != null)
         {
            if (AskOkForNeuronChange(iNeuron, iType) == true)
               iNeuron = iNeuron.ChangeTypeTo(iType);
         }
         else
            MessageBox.Show("Can't find selected neuron!");
      }

      /// <summary>
      /// Asks the ok from the user to change the neuron to the new type.
      /// </summary>
      /// <remarks>
      /// We set this in a seperate function cause we need to check to convertion -> if it is allowed + what message text
      /// to display to the user.
      /// </remarks>
      /// <param name="neuron">The neuron.</param>
      /// <param name="type">The type.</param>
      /// <returns>True if the user said ok, false otherwise.</returns>
      private bool AskOkForNeuronChange(Neuron neuron, Type type)
      {
         string iMsg;
         if (neuron is NeuronCluster || neuron is ValueNeuron)                                        //these are all the types that, when changed will loose data.
            iMsg = string.Format("Changing the neuron: {0} to a {1} will loose some data, continue?", neuron, type.Name);
         else
            iMsg = string.Format("Change the neuron: {0} to a {1}?", neuron, type.Name);
         MessageBoxResult iRes = MessageBox.Show(iMsg, "Change neuron type", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes);
         return iRes == MessageBoxResult.Yes;
      }

      #endregion


      #region DeleteNeuron

      /// <summary>
      /// Handles the CanExecute event of the DeleteNeuron control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void DeleteNeuron_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         Neuron iToDelete = e.Parameter as Neuron;
         if (iToDelete == null)
         {
            FrameworkElement iFocused = Keyboard.FocusedElement as FrameworkElement;
            if (iFocused != null && iFocused.DataContext is INeuronWrapper)
               iToDelete = ((INeuronWrapper)iFocused.DataContext).Item;
         }
         e.CanExecute = iToDelete != null && iToDelete.CanBeDeleted == true;
      }

      /// <summary>
      /// Handles the Executed event of the DeleteNeuron command.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void DeleteNeuron_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         Neuron iToDelete = e.Parameter as Neuron;
         if (iToDelete == null)
         {
            FrameworkElement iFocused = Keyboard.FocusedElement as FrameworkElement;
            if (iFocused != null && iFocused.DataContext is INeuronWrapper)
               iToDelete = ((INeuronWrapper)iFocused.DataContext).Item;
         }
         if (iToDelete != null)
         {
            MessageBoxResult iRes = MessageBox.Show(string.Format("Delete neuron: '{0}'?", iToDelete), "Delete neuron", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.No);
            if (iRes == MessageBoxResult.OK)
            {
               WindowMain.UndoStore.BeginUndoGroup(false);                                                          //we group all the data together so a single undo command cand restore the previous state.
               try
               {
                  DeleteItemFromBrain(iToDelete);
               }
               finally
               {
                  Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(WindowMain.UndoStore.EndUndoGroup));          //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled.
               }
            }
         }
      }

      #endregion

      #region RunNeuron

      private void RunNeuron_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         Neuron iToRun = e.Parameter as Neuron;
         if (iToRun == null)
         {
            FrameworkElement iFocused = Keyboard.FocusedElement as FrameworkElement;
            if (iFocused != null && iFocused.DataContext is INeuronWrapper)
               iToRun = ((INeuronWrapper)iFocused.DataContext).Item;
         }
         e.CanExecute = iToRun != null;
      }

      private void RunNeuron_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         Neuron iToRun = e.Parameter as Neuron;
         if (iToRun == null)
         {
            FrameworkElement iFocused = Keyboard.FocusedElement as FrameworkElement;
            if (iFocused != null && iFocused.DataContext is INeuronWrapper)
               iToRun = ((INeuronWrapper)iFocused.DataContext).Item;
         }
         if (iToRun != null)
         {
            NeuronCluster iClusterToRun = iToRun as NeuronCluster;
            Processor iProc;
            if (iClusterToRun != null)
            {
               MessageBoxResult iRes = MessageBox.Show(string.Format("Run the cluster as function (no will solve it)?"), "Delete neuron", MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
               if (iRes == MessageBoxResult.Yes)
               {
                  iProc = ProcessorManager.Current.GetProcessor();
                  iProc.Call(iClusterToRun);
               }
               else if (iRes == MessageBoxResult.Cancel)
                  return;
            }
            iProc = ProcessorManager.Current.GetProcessor();                                          //by default we solve it.
            iProc.Push(iToRun);
            iProc.Solve();
         }
      }

      #endregion

      #region Sandbox
      /// <summary>
      /// Handles the CanExecute event of the Sandbox command.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void Sandbox_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         e.CanExecute = ProjectManager.Default.SandboxRunning == false && 
                        ProjectManager.Default.IsSandBox == false &&
                        string.IsNullOrEmpty(ProjectManager.Default.Location) == false && 
                        string.IsNullOrEmpty(ProjectManager.Default.SandboxLocation) == false;
      }

      /// <summary>
      /// Handles the Executed event of the Sandbox command.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void Sandbox_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         ProjectManager.Default.StartSandBox();
      } 
      #endregion

      #region Continue debug
      private void ContinueDebug_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         ProcItem iItem = ProcessorManager.Current.SelectedProcessor;
         if (iItem != null)
         {
            DebugProcessor iProc = iItem.Processor as DebugProcessor;
            if (iProc != null)
               e.CanExecute = iProc.IsPaused;
            else
               e.CanExecute = false;
         }
      }

      private void ContinueDebug_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         ProcItem iItem = ProcessorManager.Current.SelectedProcessor;
         if (iItem != null)
         {
            DebugProcessor iProc = iItem.Processor as DebugProcessor;
            iProc.DebugContinue();
         }
         
      } 
      #endregion

      #region Pause debug
      private void PauseDebug_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         ProcItem iItem = ProcessorManager.Current.SelectedProcessor;
         if (iItem != null)
         {
            DebugProcessor iProc = iItem.Processor as DebugProcessor;
            if (iProc != null)
               e.CanExecute = !iProc.IsPaused || iProc.DebugMode == DebugMode.SlowMotion;
            else
               e.CanExecute = false;
         }
      }

      private void PauseDebug_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         ProcItem iItem = ProcessorManager.Current.SelectedProcessor;
         if (iItem != null)
         {
            DebugProcessor iProc = iItem.Processor as DebugProcessor;
            iProc.DebugPause();
         }
      } 
      #endregion

      #region Step next debug
      private void StepNextDebug_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         ProcItem iItem = ProcessorManager.Current.SelectedProcessor;
         if (iItem != null)
         {
            DebugProcessor iProc = iItem.Processor as DebugProcessor;
            if (iProc != null)
               e.CanExecute = iProc.IsPaused;
            else
               e.CanExecute = false;
         }
      }

      private void StepNextDebug_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         ProcItem iItem = ProcessorManager.Current.SelectedProcessor;
         if (iItem != null)
         {
            DebugProcessor iProc = iItem.Processor as DebugProcessor;
            iProc.DebugStepNext();
         }
      } 
      #endregion

      #region Inspect expression

      /// <summary>
      /// Handles the CanExecute event of the InspectExpression control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void InspectExpression_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         if (e.Parameter != null)
            e.CanExecute = e.Parameter is ResultExpression;
         else
         {
            FrameworkElement iFocused = Keyboard.FocusedElement as FrameworkElement;
            if(iFocused != null)
            {
               INeuronWrapper iWrapper = iFocused.DataContext as INeuronWrapper;
               e.CanExecute = iWrapper != null && iWrapper.Item is ResultExpression;
               return;
            }
            e.CanExecute = false;
         }
      }

      /// <summary>
      /// Handles the Executed event of the InspectExpression control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void InspectExpression_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         ResultExpression iExp = null;
         if (e.Parameter != null)
            iExp = (ResultExpression)e.Parameter;
         else
         {
            FrameworkElement iFocused = Keyboard.FocusedElement as FrameworkElement;
            INeuronWrapper iWrapper = iFocused.DataContext as INeuronWrapper;
            iExp = iWrapper.Item as ResultExpression;
         }
         if (iExp != null && ProcessorManager.Current.SelectedProcessor != null)
         {
            List<DebugNeuron> iItems = (from i in iExp.GetValue(ProcessorManager.Current.SelectedProcessor.Processor)
                                        select new DebugNeuron(i)).ToList();
            DlgInspectExpression iRes = new DlgInspectExpression(iExp, iItems);
            iRes.Owner = App.Current.MainWindow;
            iRes.Show();
         }
      }

      #endregion


      #region Clear BreakPoints
      private void ClearBreakPoints_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         e.CanExecute = BrainData.Current != null && BrainData.Current.BreakPoints != null && BrainData.Current.BreakPoints.Count > 0;
      }

      private void ClearBreakPoints_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         BrainData.Current.BreakPoints.Clear();
      } 
      #endregion

      #region ImportFromFramenet

      /// <summary>
      /// Handles the Executed event of the ImportFrameNetData control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void ImportFrameNetData_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         DlgImportFrameNet iDlg = new DlgImportFrameNet();
         iDlg.Owner = this;
         if (e.Parameter != null)
         {
            iDlg.ImportInto = (FrameEditor)e.Parameter;
            iDlg.ShowImportInto = false;
         }
         iDlg.Show();                                  
      } 

                       //we don't show dialog, this way, the user can still browse the data while selecting what to import.

      #endregion
     
      #endregion

      #region Event handlers

      #region Menu

      /// <summary>
      /// Handles the Click event of the MnuExit menu item.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuExit_Click(object sender, RoutedEventArgs e)
      {
         App.Current.Shutdown();
      }

      /// <summary>
      /// Handles the Click event of the MnuRecentProject menu item.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuRecentProject_Click(object sender, RoutedEventArgs e)
      {
         if (e.OriginalSource != MnuRecentProjects)                                       //need to make certain that we don't do something when the user clicks on 'Recently opened'.
         {
            MenuItem iSender = (MenuItem)e.OriginalSource;
            string iPath = iSender.Header as string;
            ProjectManager.Default.Open(iPath);
         }
      }

      /// <summary>
      /// Handles the Click event of the MnuOptions control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuOptions_Click(object sender, RoutedEventArgs e)
      {
         WindowOptions iDlg = new WindowOptions();
         iDlg.Owner = this;
         iDlg.ShowDialog();
      }
      #endregion

      #region General



      

      /// <summary>
      /// Checks if the application can be closed, and ask the appropriate questions in cases it doesn't know.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
      {
         if (ProjectManager.Default.ProjectChanged == true)
         {
            MessageBoxResult iRes = MessageBox.Show("The project has been changed, save first?", "Quit application!", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
            if (iRes == MessageBoxResult.Yes)
               ProjectManager.Default.Save();
            e.Cancel = iRes == MessageBoxResult.Cancel;
         }
         //if (e.Cancel == false)
         //   VwThesaurus.SaveData();
      } 
      #endregion


      #region File

      #region Save
      /// <summary>
      /// Handles the Executed event of the Save command.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void Save_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         ProjectManager.Default.Save();
      }


      private void SaveAs_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         ProjectManager.Default.SaveAs();
      }


      #endregion

      #region New
      /// <summary>
      /// Handles the Executed event of the New command.
      /// </summary>
      /// <remarks>
      /// Simply starts a new process that is the same as this one.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void New_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         ProjectManager.Default.CreateNew();
         VwDescription.Document = null;                                                //when a new item is created, need to make certain that the desc editor is also empty.
      } 
      #endregion


      #region Open
      /// <summary>
      /// Handles the Executed event of the Open command.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void Open_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         ProjectManager.Default.Open();
      }

      /// <summary>
      /// Handles the CanExecute event of the Open control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void Open_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         e.CanExecute = (ProjectManager.Default.SandboxRunning == false);
      }

      #endregion

      #endregion

      #region Insert
      /// <summary>
      /// Handles the Click event of the MnuNewCommChannel control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuNewCommChannel_Click(object sender, RoutedEventArgs e)
      {
         TextSin iNew = new TextSin();
         iNew.Text = "New text channel";
         WindowMain.UndoStore.BeginUndoGroup(true);                                                        //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
         try
         {
            WindowMain.AddItemToBrain(iNew);
            TextChannel iView = new TextChannel();
            iView.NeuronID = iNew.ID;
            iView.IsVisible = true;
            //iView.Name = iNew.Name;
            BrainData.Current.CommChannels.Add(iView);                                                            //this stores the item in the brain.
            ToolFrame iFrame = TfData.ItemContainerGenerator.ContainerFromItem(iView) as ToolFrame;
            if (iFrame != null)
               iFrame.IsSelected = true;
         }
         finally
         {
            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(WindowMain.UndoStore.EndUndoGroup));          //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled.
         }
      }


      /// <summary>
      /// Handles the Click event of the MnuNewImageChannel menu item.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuNewImageChannel_Click(object sender, RoutedEventArgs e)
      {
         ImageSin iNew = new ImageSin();
         iNew.Text = "New Image channel";
         WindowMain.UndoStore.BeginUndoGroup(true);                                                        //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
         try
         {
            WindowMain.AddItemToBrain(iNew);
            ImageChannel iView = new ImageChannel();
            iView.NeuronID = iNew.ID;
            iView.IsVisible = true;
            //iView.Name = iNew.Name;
            BrainData.Current.CommChannels.Add(iView);                                                            //this stores the item in the brain.
            ToolFrame iFrame = TfData.ItemContainerGenerator.ContainerFromItem(iView) as ToolFrame;
            if (iFrame != null)
               iFrame.IsSelected = true;
         }
         finally
         {
            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(WindowMain.UndoStore.EndUndoGroup));          //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled.
         }
      }


      /// <summary>
      /// Handles the Click event of the MnuNewAudioChannel menu item.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuNewAudioChannel_Click(object sender, RoutedEventArgs e)
      {
         TextSin iNew = new TextSin();
         iNew.Text = "New text channel";
         WindowMain.UndoStore.BeginUndoGroup(true);                                                        //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
         try
         {
            WindowMain.AddItemToBrain(iNew);
            TextChannel iView = new TextChannel();
            iView.NeuronID = iNew.ID;
            iView.IsVisible = true;
            //iView.Name = iNew.Name;
            BrainData.Current.CommChannels.Add(iView);                                                            //this stores the item in the brain.
            ToolFrame iFrame = TfData.ItemContainerGenerator.ContainerFromItem(iView) as ToolFrame;
            if (iFrame != null)
               iFrame.IsSelected = true;
         }
         finally
         {
            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(WindowMain.UndoStore.EndUndoGroup));          //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled.
         }
      }


      /// <summary>
      /// Handles the Click event of the MnuNewReflectionChannel menu item.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuNewReflectionChannel_Click(object sender, RoutedEventArgs e)
      {
         ReflectionSin iNew = new ReflectionSin();
         WindowMain.UndoStore.BeginUndoGroup(true);                                                        //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
         try
         {
            WindowMain.AddItemToBrain(iNew);
            iNew.Text = "Reflection channel - " + iNew.ID;
            ReflectionChannel iView = new ReflectionChannel();
            iView.NeuronID = iNew.ID;
            iView.IsVisible = true;
            BrainData.Current.CommChannels.Add(iView);                                                            //this stores the item in the brain.
            ToolFrame iFrame = TfData.ItemContainerGenerator.ContainerFromItem(iView) as ToolFrame;
            if (iFrame != null)
               iFrame.IsSelected = true;
         }
         finally
         {
            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(WindowMain.UndoStore.EndUndoGroup));          //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled.
         }
      }

      /// <summary>
      /// Handles the Executed event of the NewMindMap control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void NewMindMap_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup();
         try
         {
            if (BrainData.Current.CurrentEditorsList != null)
            {
               MindMap iNew = new MindMap();
               iNew.Name = "New mind map";
               BrainData.Current.CurrentEditorsList.Add(iNew);
               AddItemToOpenDocuments(iNew);
            }
            else
               throw new InvalidOperationException("There is no editors list selected to put new items in.");
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      }

      public void AddItemToOpenDocuments(object item)
      {
         BrainData.Current.OpenDocuments.Add(item);                                                           //this actually shows the item
         ToolFrame iFrame = TfData.ItemContainerGenerator.ContainerFromItem(item) as ToolFrame;
         if (iFrame != null)
            iFrame.IsSelected = true;
         
      }


      /// <summary>
      /// Handles the Executed event of the NewFrameEditor control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void NewFrameEditor_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup();
         try
         {
            if (BrainData.Current.CurrentEditorsList != null)
            {
               FrameEditor iNew = new FrameEditor();
               iNew.Name = "New frame editor";
               BrainData.Current.CurrentEditorsList.Add(iNew);
               BrainData.Current.OpenDocuments.Add(iNew);                                                           //this actually shows the item
               ToolFrame iFrame = TfData.ItemContainerGenerator.ContainerFromItem(iNew) as ToolFrame;
               if (iFrame != null)
                  iFrame.IsSelected = true;
            }
            else
               throw new InvalidOperationException("There is no editors list selected to put new items in.");
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      }

      /// <summary>
      /// Handles the Executed event of the NewFlow control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void NewFlow_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup();
         try
         {
            if (BrainData.Current.CurrentEditorsList != null)
            {
               FlowEditor iNew = new FlowEditor();
               iNew.Name = "New Flow Editor";
               BrainData.Current.CurrentEditorsList.Add(iNew);
               BrainData.Current.OpenDocuments.Add(iNew);                                                           //this actually shows the item
               ToolFrame iFrame = TfData.ItemContainerGenerator.ContainerFromItem(iNew) as ToolFrame;
               if (iFrame != null)
                  iFrame.IsSelected = true;
            }
            else
               throw new InvalidOperationException("There is no editors list selected to put new items in.");
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      }


      /// <summary>
      /// Handles the Executed event of the NewFolder control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void NewFolder_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup();
         try
         {
            if (BrainData.Current.CurrentEditorsList != null)
            {
               EditorFolder iNew = new EditorFolder();
               iNew.Name = "New folder";
               BrainData.Current.CurrentEditorsList.Add(iNew);
            }
            else
               throw new InvalidOperationException("There is no editors list selected to put new items in.");
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      }

      /// <summary>
      /// Handles the Executed event of the NewCodeCluster control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void NewCodeCluster_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup();
         try
         {
            if (BrainData.Current.CurrentEditorsList != null)
            {
               NeuronCluster iCluster = new NeuronCluster();
               iCluster.Meaning = (ulong)PredefinedNeurons.Code;
               AddItemToBrain(iCluster);
               CodeEditor iCode = new CodeEditor(iCluster);
               iCode.Name = "New code cluster";
               BrainData.Current.CurrentEditorsList.Add(iCode);
               BrainData.Current.OpenDocuments.Add(iCode);                                                           //this actually shows the item
               ToolFrame iFrame = TfData.ItemContainerGenerator.ContainerFromItem(iCode) as ToolFrame;
               if (iFrame != null)
                  iFrame.IsSelected = true;
            }
            else
               throw new InvalidOperationException("There is no editors list selected to put new items in.");
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      }

      #endregion

      #region Docking control
      /// <summary>
      /// Trigged whenever an item inside the docking control gets keyboard focus.  This is done to check
      /// if the new selected item has as data context, something who's description can be edited.
      /// </summary>
      /// <remarks>
      /// This function makes a copy of the DocumentFlow, this way, it can be displayed in multiple editors (like a note and the global view).
      /// </remarks>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void DockingControl_GotFocus(object sender, RoutedEventArgs e)
      {
         if (e.OriginalSource != VwDescription.DescriptionEditor)                                                      //don't do anything if we are moving to the global editor.
         {
            FrameworkElement iSender = e.OriginalSource as FrameworkElement;
            object iData = null;
            if (iSender is ToolFrame)                                                                    //need to do convertion, in case the editor space (background) is selected, which usually gives the toolframe, so get it's contents.
               iData = ((ToolFrame)iSender).SelectedContent;
            else if (iSender != null)
               iData = iSender.DataContext;
            //if (iData is IEditorSelection)                                                               //if it's an editor and there is an item selected, use that as the data.
            //{
            //   IEditorSelection iEditor = (IEditorSelection)iData;
            //   if (iEditor.SelectedItem != null)
            //      iData = iEditor.SelectedItem;
            //}
            if (iData != null)
            {
               IDescriptionable iFound = null;

               fCurrentNeuronInfo = iData as INeuronInfo;
               if (fCurrentNeuronInfo != null && fCurrentNeuronInfo.NeuronInfo != null)
                  iFound = fCurrentNeuronInfo.NeuronInfo;
               else
                  iFound = iData as IDescriptionable;
               if (iFound != null)                                                              //if we haven't found anything, we simply leave the previous itemm visible.
               {
                  VwDescription.Document = iFound.Description;
                  fCurrentDesc = iFound;
                  ToolFrame.SetTitle(VwDescription, "Description: " + iFound.DescriptionTitle);
               }
            }
            else
            {
               VwDescription.Document = null;
               ToolFrame.SetTitle(VwDescription, "Description");
            }
         }
      }



      /// <summary>
      /// Need to save the data back to the flowdocument when old focus is the global editor.  If old focused is the
      /// editor of the current document, save to global.
      /// </summary>
      private void DockingControlContent_PrvLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
      {
         FlushDescriptionData(e.OldFocus as RichTextBox);
      }

      #endregion

      private void Button_Click(object sender, RoutedEventArgs e)
      {
         GC.Collect();
      }

      /// <summary>
      /// Handles the MouseDoubleClick event of the LogItem control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
      private void LogItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
      {
         ListViewItem iSender = sender as ListViewItem;
         if (iSender != null && iSender.Content is LogItem)
         {
            LogItem iItem = (LogItem)iSender.Content;
            DisplayDebugData(iItem.Tag as LogDebugData);
         }
      }

      /// <summary>
      /// Opens an editor to display the debug data.
      /// </summary>
      /// <param name="data">The data.</param>
      internal void DisplayDebugData(LogDebugData data)
      {
         if (data != null)
         {
            CodeEditor iEditor;
            if (data.Meaning != null)
               iEditor = ViewCodeForNeuron(data.Meaning);
            else
               iEditor = ViewCodeForNeuron(data.ExecutionSource);
            if (iEditor != null)
            {
               iEditor.SelectedListType = data.ExecListType;
               iEditor.IsLoaded = true;                                                                  //need to make certain that the editor is loaded, even before it is made visible, otherwise, we can't find the items.
               CodeEditorPage iPage = iEditor.EntryPoints[iEditor.SelectedIndex];
               iPage.Select(data.Executed);
            }
            SyncExplorerToNeuron(data.Executed);
         }
      }
      

      #endregion

      #region IWeakEventListener Members

      /// <summary>
      /// Receives events from the centralized event manager.
      /// </summary>
      /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
      /// <param name="sender">Object that originated the event.</param>
      /// <param name="e">Event data.</param>
      /// <returns>
      /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
      /// </returns>
      public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(BeforeSaveEventManager))
         {
            FlushDescriptionData(Keyboard.FocusedElement as RichTextBox);        //Whenever the data gets saved, we need to make certain that everything gets flushed ok, like the description box.
            return true;
         }
         else
            return false;
      }

      #endregion

      private void BtnCloseTlFrame_Click(object sender, RoutedEventArgs e)
      {
         FrameworkElement iSender = (FrameworkElement)sender;
         BrainData.Current.OpenDocuments.Remove(iSender.DataContext);
      }

      /// <summary>
      /// Handles the Click event of the MnuRemoveBrokenRef menu item.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuRemoveBrokenRef_Click(object sender, RoutedEventArgs e)
      {
         DlgFixBrokenRefs iDlg = new DlgFixBrokenRefs();
         iDlg.Owner = this;
         iDlg.ShowDialog();
      }

      /// <summary>
      /// Handles the Click event of the MnuEditPOS menu item.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuEditPOS_Click(object sender, RoutedEventArgs e)
      {
         DlgPOSEditor iDlg = new DlgPOSEditor();
         iDlg.Owner = this;
         iDlg.ShowDialog();
      }

      private void MnuHelpAbout_Click(object sender, RoutedEventArgs e)
      {
         DlgAbout iDlg = new DlgAbout();
         iDlg.Owner = this;
         iDlg.ShowDialog();
      }

      private void MnuSearchHelp_Click(object sender, RoutedEventArgs e)
      {
         fHelp.ShowHelpSearch();
      }

      private void MnuContentsHelp_Click(object sender, RoutedEventArgs e)
      {
         fHelp.ShowHelpContents();
      }

      private void MnuIndexHelp_Click(object sender, RoutedEventArgs e)
      {
         fHelp.ShowHelpIndex("");
      }

      private void Help_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         e.CanExecute = Keyboard.FocusedElement is UIElement;
      }

      private void Help_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         fHelp.ShowHelpFor(Keyboard.FocusedElement as UIElement);
      }

      private void MnuItemCloseDocument_Click(object sender, RoutedEventArgs e)
      {
         UIElement iItem = (UIElement)e.OriginalSource;

         ContextMenu iMenu = TreeHelper.FindInTree<ContextMenu>(iItem);
         ToolFrame iFrame = (ToolFrame)iMenu.PlacementTarget;
         CommChannel iChannel = iFrame.DataContext as CommChannel;                                 //commchannels need to be closed differently.
         if (iChannel != null)
            iChannel.IsVisible = false;
         else
            BrainData.Current.OpenDocuments.Remove(iFrame.DataContext);
      }

      /// <summary>
      /// Handles the Click event of the MnuItemClear control.
      /// </summary>
      /// <remarks>
      /// Clears all the log data.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuItemClear_Click(object sender, RoutedEventArgs e)
      {
         WPFLog.WPFLog.Default.Items.Clear();
      }

      


      

   }
}
