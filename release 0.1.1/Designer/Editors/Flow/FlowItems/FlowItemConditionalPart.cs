﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// A single list of items within a loop or option.
   /// </summary>
   public class FlowItemConditionalPart : FlowItemBlock
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="FlowItemConditionalPart"/> class.
      /// </summary>
      /// <param name="toWrap">To wrap.</param>
      public FlowItemConditionalPart(NeuronCluster toWrap): base(toWrap)
      {

      }
   }
}
