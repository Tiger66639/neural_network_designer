﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// The base class for flow items like statics, and conditional items.
   /// </summary>
   public class FlowItem: EditorItem
   {
      public FlowItem(Neuron towrap): base(towrap)
      {

      }
   }
}
