﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using System.Collections.Specialized;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// A loop or option.
   /// </summary>
   public class FlowItemConditional : FlowItemBlock
   {
      #region fields
      bool fIsLooped;
      string fBackImage;
      string fFrontImage;
      bool fIsSelectionRequired;
      /// <summary>
      /// Default width of a single line.
      /// </summary>
      const double LignWidth = 4.3;
      double fSignWidth = LignWidth;
      #endregion

      #region ctor
      /// <summary>
      /// Initializes a new instance of the <see cref="FlowItemConditional"/> class.
      /// </summary>
      /// <param name="toWrap">To wrap.</param>
      public FlowItemConditional(NeuronCluster toWrap)
         : base(toWrap)
      {
         Items.CollectionChanged += new NotifyCollectionChangedEventHandler(Items_CollectionChanged);
         SignWidth = LignWidth + (Items.Count * LignWidth);                                                    //when initially created, set the signwidh, based on the nr of items loaded.  if we don't do this, the init is incorrect for loaded items.
      }

      #endregion

      #region prop


      #region IsLooped

      /// <summary>
      /// Gets/sets wether the conditional flow item is looped or not.
      /// </summary>
      public bool IsLooped
      {
         get
         {
            return fIsLooped;
         }
         set
         {
            if (value != fIsLooped)
            {
               InternalSetIsLooped(value);
               InternalChange = true;
               try
               {
                  SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.FlowItemIsLoop, value);
               }
               finally
               {
                  InternalChange = false;
               }
            }
         }
      }


      #endregion 

      #region IsSelectionRequired

      /// <summary>
      /// Gets/sets wether the conditional statement requires a selection or not.
      /// </summary>
      public bool IsSelectionRequired
      {
         get
         {
            return fIsSelectionRequired;
         }
         set
         {
            InternalSetIsSelectionRequired(value);
            InternalChange = true;
            try
            {
               SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.FlowItemRequiresSelection, value);
            }
            finally
            {
               InternalChange = false;
            }
            
         }
      }

      


      #endregion

      #region FrontImage

      /// <summary>
      /// Gets the string to put in front of the conditional, to indicate it is a loop '{' or option '['
      /// </summary>
      public object FrontImage
      {
         get 
         {
            if (IsSelected == false)
               return App.Current.FindResource(fFrontImage);
            else
               return App.Current.FindResource("Selected" + fFrontImage);
         }
         internal set 
         { 
            fFrontImage = (string)value;
            OnPropertyChanged("FrontImage");
         }
      }

      #endregion

      #region BackSign

      /// <summary>
      /// Gets the string to put in the back of the conditional, to indicate it is a loop '}' or option ']'
      /// </summary>
      public object BackImage
      {
         get
         {
            if (IsSelected == false)
               return App.Current.FindResource(fBackImage);
            else
               return App.Current.FindResource("Selected" + fBackImage);
         }
         set
         {
            fBackImage = (string)value;
            OnPropertyChanged("BackImage");
         }
      }

      #endregion

      
      #region SignWidth

      /// <summary>
      /// Gets/sets the height that should be applied to the front and back sign.  This is calculated based on the nr of 
      /// items in the collection * 8.
      /// </summary>
      public double SignWidth
      {
         get
         {
            return fSignWidth;
         }
         set
         {
            fSignWidth = value;
            OnPropertyChanged("SignWidth");
         }
      }

      #endregion

      #endregion


      #region Functions


      /// <summary>
      /// Updates the root object's <see cref="IEditorSelection.SelectedItems"/> list so that everything is up to date.
      /// </summary>
      /// <remarks>
      /// When the selection of this object is changed, we need to update the images used for front and backsign.
      /// </remarks>
      /// <param name="value">if set to <c>true</c> [value].</param>
      internal protected override void SetSelected(bool value)
      {
         base.SetSelected(value);
         OnPropertyChanged("FrontImage");
         OnPropertyChanged("BackImage");
      }

      void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         SignWidth = LignWidth + (Items.Count * LignWidth);
      } 

      /// <summary>
      /// Stores the actual value for IsLooped and raises the PropertyChanged.
      /// </summary>
      /// <param name="value">if set to <c>true</c> [value].</param>
      private void InternalSetIsLooped(bool value)
      {
         fIsLooped = value;
         OnPropertyChanged("IsLooped");
         if (value == true)
         {
            FrontImage = "LoopLeft";
            BackImage = "LoopRight";
         }
         else
         {
            FrontImage = "OptionLeft";
            BackImage = "OptionRight";
         }
      }

      /// <summary>
      /// Stores the actual value for IsSelectionRequired and raises the PropertyChanged.
      /// </summary>
      /// <param name="value">if set to <c>true</c> [value].</param>
      private void InternalSetIsSelectionRequired(bool value)
      {
         fIsSelectionRequired = value;
         OnPropertyChanged("IsSelectionRequired");
      }

      /// <summary>
      /// descendents that need to update links that changed can do this through this function.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      protected override void InternalLinkChanged(Link sender, LinkChangedEventArgs e)
      {
         base.InternalLinkChanged(sender, e);
         if (sender.MeaningID == (ulong)PredefinedNeurons.FlowItemIsLoop)
            if (e.Action == BrainAction.Removed)
               InternalSetIsLooped(false);
            else
               InternalSetIsLooped(e.NewTo == (ulong)PredefinedNeurons.True);
      }

      protected override void OnItemChanged(Neuron value)
      {
         base.OnItemChanged(value);
         Neuron iFound = Item.FindFirstOut((ulong)PredefinedNeurons.FlowItemIsLoop);
         InternalSetIsLooped(iFound != null && iFound.ID == (ulong)PredefinedNeurons.True);
         iFound = Item.FindFirstOut((ulong)PredefinedNeurons.FlowItemRequiresSelection);
         InternalSetIsSelectionRequired(iFound != null && iFound.ID == (ulong)PredefinedNeurons.True);
      }

      #endregion
   }
}
