﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// A flow item that represents static content: other neurons.
   /// </summary>
   public class FlowItemStatic: FlowItem
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="FlowItemStatic"/> class.
      /// </summary>
      /// <param name="toWrap">The item to wrap.</param>
      public FlowItemStatic(Neuron toWrap): base(toWrap)
      {

      }

      protected override void OnItemChanged(Neuron value)
      {
         base.OnItemChanged(value);
         NeuronCluster iVal = value as NeuronCluster;
         IsLink = iVal != null && iVal.Meaning == (ulong)PredefinedNeurons.Flow;
            
      }

      bool fIsLink;
      #region IsLink

      /// <summary>
      /// Gets wether this static item references another flow.
      /// </summary>
      public bool IsLink
      {
         get
         {
            return fIsLink;
         }
         internal set
         {
            fIsLink = value;
            OnPropertyChanged("IsLink");
         }
      }

      #endregion
   }
}
