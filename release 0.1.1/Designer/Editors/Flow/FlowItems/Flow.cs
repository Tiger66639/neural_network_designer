﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeuralNetworkDesigne.Data;
using System.Collections;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Threading;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Provides a way to visually express interoperability between <see cref="Frame"/>s over a period
   /// of time.  Ex: conversation flow.
   /// </summary>
   public class Flow : NeuronEditor, IEditorSelection
   {
      #region fields

      FlowItemCollection fItems;
      EditorItemSelectionList<FlowItem> fSelectedItems = new EditorItemSelectionList<FlowItem>();
      bool fIsSelected;
      UIElement fPopupTarget;
      bool fPopupDoesInsert;
      bool fPopupIsOpen;
      bool fIsFloating;

      #endregion

      #region Prop

      #region InternalChange
      /// <summary>
      /// a switch that determins if the class is doing an internal change or not. 
      /// This is used by the LinkChanged event manager to see if there needs to be
      /// an update in response to a linkchange or not.
      /// </summary>
      protected bool InternalChange { get; set; } 
      #endregion

      #region Icon
      /// <summary>
      /// Gets the resource path to the icon that should be used for this editor.  This is usually class specific.
      /// </summary>
      /// <value></value>
      public override string Icon
      {
         get { return "/Images/Flow/flow.png"; }
      }
      #endregion

      #region  IsSelected

      /// <summary>
      /// Gets/sets if the flow is currently selected or not.
      /// </summary>
      /// <remarks>
      /// Used to select the item visually, must be bound to from xaml.
      /// We override, cause we don't want to update the editors list.
      /// </remarks>
      [XmlIgnore]
      public override bool IsSelected
      {
         get
         {
            return fIsSelected;
         }
         set
         {
            fIsSelected = value;
            OnPropertyChanged("IsSelected");
         }
      }

      #endregion

      public override string NamePrefix
      {
         get { return "Flow"; }
      }

      

      #region Items

      /// <summary>
      /// Gets the list of flow items defined in this flow.
      /// </summary>
      public FlowItemCollection Items
      {
         get { return fItems; }
         internal set 
         { 
            fItems = value;
            OnPropertyChanged("Items");
         }
      }

      #endregion

      #region ItemID
      /// <summary>
      /// Gets or sets the ID of the item that this object provides an editor for.
      /// </summary>
      /// <value>The ID of the item.</value>
      /// <remarks>
      /// When read from xml, use <see cref="NeuronEditor.RegisterItem"/> to properly attach the item.
      /// </remarks>
      public override ulong ItemID
      {
         get
         {
            return base.ItemID;
         }
         set
         {
            base.ItemID = value;
            if (Item != null)
               LoadItems();
         }
      } 
      #endregion

      
      #region PopupDoesInsert

      /// <summary>
      /// Gets/sets the wether the popup does an insert or an add.  This prop is provided so that the editor can easely link this
      /// data with the visualizer for this object.
      /// </summary>
      public bool PopupDoesInsert
      {
         get
         {
            return fPopupDoesInsert;
         }
         set
         {
            fPopupDoesInsert = value;
            OnPropertyChanged("PopupDoesInsert");
         }
      }

      #endregion


      
      #region PopupTarget

      /// <summary>
      /// Gets/sets the the target of the pupup. This prop is provided so that the editor can easely link this
      /// data with the visualizer for this object.
      /// </summary>
      public UIElement PopupTarget
      {
         get
         {
            return fPopupTarget;
         }
         set
         {
            fPopupTarget = value;
            OnPropertyChanged("PopupTarget");
         }
      }

      #endregion

      
      #region PopupIsOpen

      /// <summary>
      /// Gets/sets wether the popup should be opened or closed. This prop is provided so that the editor can easely link this
      /// data with the visualizer for this object.
      /// </summary>
      public bool PopupIsOpen
      {
         get
         {
            return fPopupIsOpen;
         }
         set
         {
            fPopupIsOpen = value;
            OnPropertyChanged("PopupIsOpen");
         }
      }

      #endregion

      #region IsFloating

      /// <summary>
      /// Gets/sets wether the conditional statement requires a selection or not.
      /// </summary>
      public bool IsFloating
      {
         get
         {
            return fIsFloating;
         }
         set
         {
            InternalSetIsFloating(value);
            InternalChange = true;
            try
            {
               EditorsHelper.SetFirstOutgoingLinkTo(Item, (ulong)PredefinedNeurons.FlowIsFloating, value);
            }
            finally
            {
               InternalChange = false;
            }

         }
      }


      #endregion

      #region IEditorSelection Members

      /// <summary>
      /// Gets the list of selected items.
      /// </summary>
      /// <value>The selected items.</value>
      [XmlIgnore]
      public IList SelectedItems
      {
         get { return fSelectedItems; }
      }

      /// <summary>
      /// Gets the currently selected item. If there are multiple selections, the first is returned.
      /// </summary>
      /// <value></value>
      [XmlIgnore]
      public FlowItem SelectedItem
      {
         get
         {
            if (fSelectedItems.Count > 0)
               return fSelectedItems[0];
            else
               return null;
         }
      }
      #endregion

      #region IEditorSelection Members


      /// <summary>
      /// Gets/sets the currently selected item. If there are multiple selections, the first is returned.
      /// </summary>
      /// <value></value>
      [XmlIgnore]
      object IEditorSelection.SelectedItem
      {
         get { return SelectedItem; }
      }

      #endregion

      #endregion


      #region Functions

      private void InternalSetIsFloating(bool value)
      {
         fIsFloating = value;
         OnPropertyChanged("IsFloating");
      }

      public override bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(LinkChangedEventManager))
         {
            if (((LinkChangedEventArgs)e).IsEnvolved(Item) == true)                                            //only call if the wrapped item was changed.
               App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<LinkChangedEventArgs>(Item_LinkChanged), e);
            return true;
         }
         else
            return base.ReceiveWeakEvent(managerType, sender, e);
      }

      void Item_LinkChanged(LinkChangedEventArgs e)
      {
         if(e.OriginalSource.MeaningID == (ulong)PredefinedNeurons.FlowIsFloating)
            if (e.Action == BrainAction.Removed)
               InternalSetIsFloating(false);
            else
               InternalSetIsFloating(e.NewTo == (ulong)PredefinedNeurons.True);
      }

      /// <summary>
      /// Registers the item that was read from xml.
      /// </summary>
      /// <remarks>
      /// This must be called when the editor is read from xml.  In that situation, the
      /// brainData isn't always loaded properly yet.  At this point, this can be resolved.
      /// It is called by the brainData.
      /// </remarks>
      public override void RegisterItem()
      {
         base.RegisterItem();
         LoadItems();
         InternalSetIsFloating(Item.FindFirstOut((ulong)PredefinedNeurons.FlowIsFloating) == Brain.Current[(ulong)PredefinedNeurons.True]);
      }

      /// <summary>
      /// Builds a <see cref="FlowItem"/> for each neuron in the wrapped cluster.
      /// </summary>
      private void LoadItems()
      {
         Items = new FlowItemCollection(this, (NeuronCluster)Item);
      }

      /// <summary>
      /// Gets the next flow item relative to the first argument that has the specified text in the display title.  
      /// The second argument defines the start, when this flow item is reached, the search should stop and 'start' is returned.
      /// </summary>
      /// <param name="toSearch">The text to search in the display title.</param>
      /// <param name="from">From.</param>
      /// <returns>The next</returns>
      internal FlowItem GetNext(string toSearch, FlowItem from)
      {
         FlowItemCollection iCol = GetCollection(from);
         FlowItem iCur = from;
         bool iFoundBlock = false;                                                  //when we find a sub block, need to prevent that we go up a level again at the end of the loop.
         int iIndex = 0;
         if (iCur is FlowItemBlock)
            iCol = ((FlowItemBlock)iCur).Items;
         else
            iIndex = iCol.IndexOf(iCur) + 1;
         while (iCol != null)
         {
            iFoundBlock = false;
            while (iIndex < iCol.Count)
            {
               iCur = iCol[iIndex];
               if (iCur == from)
                  return null;
               if (iCur.NeuronInfo.DisplayTitle.ToLower().Contains(toSearch) == true)
                  return iCur;
               if (iCur is FlowItemBlock)
               {
                  iCol = ((FlowItemBlock)iCur).Items;
                  iIndex = 0;
                  iFoundBlock = true;
                  break;
               }
               iIndex++;
            }
            if (iFoundBlock == false)
            {
               if (iCur.Owner is FlowItem)
               {
                  iCur = (FlowItem)iCur.Owner;
                  iCol = GetCollection(iCur);
                  iIndex = iCol.IndexOf(iCur) + 1;
               }
               else
               {
                  iCol = Items;                                                              //go to the start of the search.
                  iIndex = iCol.IndexOf(iCur) + 1;
                  if (iIndex < iCol.Count)
                     iCur = iCol[iIndex];
                  else
                     return null;
               }
            }
         }
         return null;
      }

      private FlowItemCollection GetCollection(FlowItem from)
      {
         if (from.Owner is Flow)
            return ((Flow)from.Owner).Items;
         else if (from.Owner is FlowItemBlock)
            return ((FlowItemBlock)from.Owner).Items;
         else
            return null;
      }

      #region Clipboard
      protected override void CopyToClipboard(DataObject data)
      {
         List<ulong> iValues = (from i in fSelectedItems
                                    select i.Item.ID).ToList();

         if (iValues.Count == 1)
            data.SetData(Properties.Resources.NeuronIDFormat, iValues[0]);
         else
            data.SetData(Properties.Resources.MultiNeuronIDFormat, iValues);
      }

      public override bool CanCopyToClipboard()
      {
         return fSelectedItems.Count > 0;
      }

      public override bool CanPasteSpecialFromClipboard()
      {
         return false;
      }

      public override void PasteSpecialFromClipboard()
      {
         throw new NotImplementedException();
      }

   
      public override bool CanPasteFromClipboard()
      {
         if (base.CanPasteFromClipboard() == true)
         {
            return Clipboard.ContainsData(Properties.Resources.MultiNeuronIDFormat) || Clipboard.ContainsData(Properties.Resources.NeuronIDFormat);
         }
         return false;
      }

      /// <summary>
      /// Pastes the data from the clipboard.
      /// </summary>
      /// <remarks>
      /// Statics are simply linked, conditionals and conditional parts are duplicated.  This is to ensure that the parsing algorithm
      /// works properly: if you simply link, you can do for instance the following: the first and last item in a list are links of
      /// the same object, only the first item is scanned in, so it is an invalid scan cause the rest is not found, but because the first
      /// is also linked as the last item, it is seen as 'complete'.  This is prevented if we use all original items.
      /// </remarks>
      public override void PasteFromClipboard()
      {
         List<ulong> iItems = null;
         if (Clipboard.ContainsData(Properties.Resources.MultiNeuronIDFormat) == true)
            iItems = Clipboard.GetData(Properties.Resources.MultiNeuronIDFormat) as List<ulong>;
         else
         {
            iItems = new List<ulong>();
            iItems.Add((ulong)Clipboard.GetData(Properties.Resources.NeuronIDFormat));
         }
         if (iItems != null)
         {
            WindowMain.UndoStore.BeginUndoGroup(false);
            try
            {
               FlowItem iSelected = SelectedItem;
               SelectedItems.Clear();
               foreach (ulong i in iItems)
               {
                  Neuron iNeuron = Brain.Current[i];
                  if (iNeuron is NeuronCluster)
                  {
                     NeuronCluster iCluster = (NeuronCluster)iNeuron;
                     if (iCluster.Meaning == (ulong)PredefinedNeurons.FlowItemConditional || iCluster.Meaning == (ulong)PredefinedNeurons.FlowItemConditionalPart)
                        iNeuron = iCluster.Duplicate();
                  }
                  FlowItem iNew = EditorsHelper.CreateFlowItemFor(iNeuron);
                  EditorsHelper.AddFlowItem(iNew, iSelected, this);
                  iSelected = iNew;
               }
            }
            finally
            {
               WindowMain.UndoStore.EndUndoGroup();
            }
         }
      }
      #endregion 
      #endregion

      #region delete
      public override void Delete()
      {
         MessageBoxResult iRes = MessageBox.Show("Remove the selected neurons from the designer and from the brain when no longer referenced?", "Delete items", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
         if (iRes == MessageBoxResult.OK)
         {
            List<FlowItem> iToRemove = fSelectedItems.ToList();                                        //we make a copy just in case we don't brake any loop code.
            iToRemove.Reverse();                                                                            //we reverse the list so that we remove the last code item first.  This allows for better consistency in editing.
            WindowMain.UndoStore.BeginUndoGroup(false);                                                          //we group all the data together so a single undo command cand restore the previous state.
            try
            {
               foreach (FlowItem i in iToRemove)
                  DeleteOrRemoveFlowItem(i, DeletionMethod.DeleteIfNoRef);
            }
            finally
            {
               WindowMain.UndoStore.EndUndoGroup();
            }
         }
      }

      private void DeleteOrRemoveFlowItem(FlowItem toRemove, DeletionMethod branchHandling)
      {
         if (toRemove.Owner == this)
            EditorsHelper.RemoveOrDeleteFromCluster<FlowItem>(toRemove, Items, branchHandling);
         else if (toRemove.Owner is FlowItemBlock)
            ((FlowItemBlock)toRemove.Owner).DeleteOrRemoveChild(toRemove, branchHandling);
         else
            MessageBox.Show("Unkown owner type, can't Delete!");
      }

      public override bool CanDelete()
      {
         return fSelectedItems.Count > 0;
      }

      public override void DeleteSpecial()
      {
         DlgSelectDeletionMethod iDlg = new DlgSelectDeletionMethod();
         bool? iDlgRes = iDlg.ShowDialog();
         if (iDlgRes.HasValue == true && iDlgRes.Value == true)
         {
            List<FlowItem> iToRemove = fSelectedItems.ToList();                                        //we make a copy just in case we don't brake any loop code.
            WindowMain.UndoStore.BeginUndoGroup(false);                                                          //we group all the data together so a single undo command cand restore the previous state.
            try
            {
               foreach (FlowItem i in iToRemove)
                  HandleNeuronForDeleteSpecial(i, iDlg.NeuronDeletionMethod, iDlg.BranchHandling);
            }
            finally
            {
               WindowMain.UndoStore.EndUndoGroup();
            }
         }
      }

      private void HandleNeuronForDeleteSpecial(FlowItem toRemove, DeletionMethod deletionMethod, DeletionMethod branchHandling)
      {
         switch (deletionMethod)
         {
            case DeletionMethod.Remove:
               RemoveFlowItem(toRemove);
               break;
            case DeletionMethod.DeleteIfNoRef:
               DeleteOrRemoveFlowItem(toRemove, branchHandling);
               break;
            case DeletionMethod.Delete:
               if (toRemove.Item.CanBeDeleted == true)
               {
                  bool iDelete;
                  using (LinksAccessor iLinksIn = toRemove.Item.LinksIn)
                  {
                     using (NeuronsAccessor iClusteredBy = toRemove.Item.ClusteredBy)
                        iDelete = (iLinksIn.Items.Count == 1 && iClusteredBy.Items.Count == 0)
                                 || (iLinksIn.Items.Count == 0 && iClusteredBy.Items.Count == 1);
                  }
                  if (iDelete == true)
                     EditorsHelper.ProcessBranch(toRemove.Item, branchHandling);
                  WindowMain.DeleteItemFromBrain(toRemove.Item);
               }
               else
                  MessageBox.Show(string.Format("Neuron {0} can't be deleted because it is used as a meaning or info.", toRemove), "Delete", MessageBoxButton.OK, MessageBoxImage.Warning);
               break;
            default:
               throw new InvalidOperationException();
         }
      }

      private void RemoveFlowItem(FlowItem toRemove)
      {
         if (toRemove.Owner == this)
         {
            if (Items.Remove(toRemove) == false)
               MessageBox.Show("Failed to remove the item from the main code list!");
         }
         else if (toRemove.Owner is FlowItemBlock)
            ((FlowItemBlock)toRemove.Owner).RemoveChildFromCode(toRemove);
         else
            MessageBox.Show("Unkown owner type, can't Remove!");
      }

      public override bool CanDeleteSpecial()
      {
         return CanDelete();
      } 
      #endregion
   }
}
