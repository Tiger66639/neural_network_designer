﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeuralNetworkDesigne.Data;
using System.Xml;
using System.Windows;
using System.Windows.Threading;
using System.Xml.Serialization;
using System.Diagnostics;

namespace NeuralNetworkDesigne.HAB.Designer
{
   public class FlowEditor : EditorBase, IWeakEventListener
   {
      #region Fields

      ObservedCollection<Flow> fFlows;
      Flow fSelectedFlow;
      bool fDisplayAsList = false;
      

      #endregion

      #region ctor
      /// <summary>
      /// Initializes a new instance of the <see cref="FlowEditor"/> class.
      /// </summary>
      public FlowEditor()
      {
         NeuronChangedEventManager.AddListener(Brain.Current, this);
         fFlows = new ObservedCollection<Flow>(this);
      }


      ~FlowEditor()
      {
         NeuronChangedEventManager.RemoveListener(Brain.Current, this);
      }
      #endregion

      #region Prop

      #region Icon
      /// <summary>
      /// Gets the resource path to the icon that should be used for this editor.  This is usually class specific.
      /// </summary>
      /// <value></value>
      public override string Icon
      {
         get { return "/Images/Flow/flow.png"; }
      }
      #endregion

      #region DescriptionTitle
      /// <summary>
      /// Gets a title that the description editor can use to display in the header.
      /// </summary>
      /// <value></value>
      public override string DescriptionTitle
      {
         get { return Name + " - Flow Editor"; }
      }
      #endregion


      #region Flows

      /// <summary>
      /// Gets the list of frames
      /// </summary>
      public ObservedCollection<Flow> Flows
      {
         get { return fFlows; }
      }

      #endregion

      #region SelectedFlow

      /// <summary>
      /// Gets/sets the currently selected flow.
      /// </summary>
      /// <remarks>
      /// This is provided so that we can change the active flow easely from code.
      /// </remarks>
      public Flow SelectedFlow
      {
         get
         {
            return fSelectedFlow;
         }
         set
         {
            fSelectedFlow = value;
            OnPropertyChanged("SelectedFlow");
         }
      }

      #endregion

      #region DisplayAsList

      /// <summary>
      /// Gets/sets wether the flows are displayed in a list or in a single detail view.
      /// </summary>
      public bool DisplayAsList
      {
         get
         {
            return fDisplayAsList;
         }
         set
         {
            if (value != fDisplayAsList)
            {
               fDisplayAsList = value;
              
               OnPropertyChanged("DisplayAsList");
            }
         }
      }

      #endregion

      

      #endregion

      #region functions


      /// <summary>
      /// Creates the correct <see cref="FlowItem"/> for the specified neuron.
      /// </summary>
      /// <param name="toWrap">To wrap.</param>
      /// <returns></returns>
      public static FlowItem CreateFlowItemFor(Neuron toWrap)
      {
         NeuronCluster iToWrap = toWrap as NeuronCluster;
         if (iToWrap != null)
         {
            if (iToWrap.Meaning == (ulong)PredefinedNeurons.FlowItemConditional)
               return new FlowItemConditional(iToWrap);
            else if (iToWrap.Meaning == (ulong)PredefinedNeurons.FlowItemConditionalPart)
               return new FlowItemConditionalPart(iToWrap);
            else
               return new FlowItemStatic(toWrap);
         }
         else
            return new FlowItemStatic(toWrap);
      }

      /// <summary>
      /// need to reasign the list and the selected item so that they are change, this will force a complete redraw.
      /// </summary>
      internal void ReDrawFlows()
      {
         Flow iSelected = SelectedFlow;
         ObservedCollection<Flow> iList = fFlows;
         fFlows = null;
         OnPropertyChanged("Flows");
         SelectedFlow = null;
         fFlows = iList;
         OnPropertyChanged("Flows");
         SelectedFlow = iSelected;
      }

      #endregion

      #region Xml

      /// <summary>
      /// Reads the fields/properties of the class.
      /// </summary>
      /// <param name="reader">The reader.</param>
      /// <returns>
      /// True if the item was properly read, otherwise false.
      /// </returns>
      /// <remarks>
      /// This function is called for each element that is found, so this function should check which element it is
      /// and only read that element accordingly.
      /// </remarks>
      protected override bool ReadXmlInternal(XmlReader reader)
      {
         if (reader.Name == "Flow")
         {
            XmlSerializer iNeuronSer = new XmlSerializer(typeof(Flow));
            Flow iNode = (Flow)iNeuronSer.Deserialize(reader);
            Flows.Add(iNode);
            return true;
         }

         else
            return base.ReadXmlInternal(reader);
      }

      /// <summary>
      /// Converts an object into its XML representation.
      /// </summary>
      /// <param name="writer">The <see cref="T:System.Xml.XmlWriter"/> stream to which the object is serialized.</param>
      public override void WriteXml(XmlWriter writer)
      {
         base.WriteXml(writer);
         XmlSerializer iFlowSer = new XmlSerializer(typeof(Flow));
         foreach (Flow i in Flows)
            iFlowSer.Serialize(writer, i);
      }

      #endregion 

      #region IWeakEventListener
      /// <summary>
      /// Receives events from the centralized event manager.
      /// </summary>
      /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
      /// <param name="sender">Object that originated the event.</param>
      /// <param name="e">Event data.</param>
      /// <returns>
      /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
      /// </returns>
      public virtual bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(NeuronChangedEventManager))
         {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<NeuronChangedEventArgs>(Item_NeuronChanged), e);
            return true;
         }
         else
            return false;
      }

      /// <summary>
      /// Called when the wrapped item is changed. When removed, makes certain that the <see cref="BrainData.CodeEditors"/> list is updated.
      /// </summary>
      /// <param name="e">The <see cref="NeuralNetworkDesigne.HAB.NeuronChangedEventArgs"/> instance containing the event data.</param>
      protected virtual void Item_NeuronChanged(NeuronChangedEventArgs e)
      {
         if (e.Action == BrainAction.Removed)
         {
            var iFound = (from i in Flows where i.Item == e.OriginalSource select i).FirstOrDefault();
            if (iFound != null)
               Flows.Remove(iFound);
         }
      }


      #endregion


      #region Clipboard
      /// <summary>
      /// Copies the selected data to the clipboard.
      /// </summary>
      protected override void CopyToClipboard(DataObject data)
      {
         data.SetData(Designer.Properties.Resources.NeuronIDFormat, SelectedFlow.ItemID);
      }

      public override bool CanCopyToClipboard()
      {
         return SelectedFlow != null;
      }

      public override bool CanPasteSpecialFromClipboard()
      {
         return CanPasteFromClipboard();
      }

      public override void PasteSpecialFromClipboard()
      {
         throw new NotImplementedException();
      }

      public override void CutToClipboard()
      {
         CopyToClipboard();
         RemoveSelectedFlow();
      }

      public override bool CanPasteFromClipboard()
      {
         if (base.CanPasteFromClipboard() == true)
         {
            if (Clipboard.ContainsData(Designer.Properties.Resources.NeuronIDFormat) == true)
            {
               ulong iId = (ulong)Clipboard.GetData(Designer.Properties.Resources.NeuronIDFormat);
               NeuronCluster iCluster = Brain.Current[iId] as NeuronCluster;
               if (iCluster != null)
                  return iCluster.Meaning == (ulong)PredefinedNeurons.Flow;
            }
            else if (Clipboard.ContainsData(Designer.Properties.Resources.MultiNeuronIDFormat) == true)
            {
               List<ulong> iList = (List<ulong>)Clipboard.GetData(Designer.Properties.Resources.MultiNeuronIDFormat);
               int iCount = (from i in iList
                             let iNeuron = Brain.Current[i]
                             where iNeuron is NeuronCluster && ((NeuronCluster)iNeuron).Meaning == (ulong)PredefinedNeurons.Flow
                             select i).Count();
               return iCount == iList.Count;
            }
         }
         return false;
      }

      public override void PasteFromClipboard()
      {
         if (Clipboard.ContainsData(Designer.Properties.Resources.NeuronIDFormat) == true)
         {
            ulong iId = (ulong)Clipboard.GetData(Designer.Properties.Resources.NeuronIDFormat);
            Flow iFlow = new Flow();
            iFlow.ItemID = iId;
            Flows.Add(iFlow);
         }
         else if (Clipboard.ContainsData(Designer.Properties.Resources.MultiNeuronIDFormat) == true)
         {
            List<ulong> iList = (List<ulong>)Clipboard.GetData(Designer.Properties.Resources.MultiNeuronIDFormat);
            foreach (ulong i in iList)
            {
               Flow iFlow = new Flow();
               iFlow.ItemID = i;
               Flows.Add(iFlow);
            }
         }
      } 
      #endregion

      public override void Delete()
      {
         Flow iToDelete = SelectedFlow;
         Debug.Assert(iToDelete != null);
         MessageBoxResult iRes = MessageBox.Show(string.Format("Remove flow: {0} from the designer and from the network when no longer referenced?", iToDelete.Name), "Delete", MessageBoxButton.YesNo, MessageBoxImage.Question);
         if (iRes == MessageBoxResult.Yes)
         {
            WindowMain.UndoStore.BeginUndoGroup(false);                                                          //we group all the data together so a single undo command cand restore the previous state.
            try
            {
               RemoveSelectedFlow();
               EditorsHelper.DeleteWhenNotUsed(iToDelete.Item, DeletionMethod.DeleteIfNoRef);
            }
            finally
            {
               WindowMain.UndoStore.EndUndoGroup();
            }
         }
      }

      private void RemoveSelectedFlow()
      {
         Flow iToRemove = SelectedFlow;
         int iIndex = Flows.IndexOf(iToRemove);
         Flows.RemoveAt(iIndex);
         if (iIndex < Flows.Count)
            SelectedFlow = Flows[iIndex];
         else if (Flows.Count > 0)
            SelectedFlow = Flows[Flows.Count - 1];
         else
            SelectedFlow = null;
      }

      /// <summary>
      /// Checks if a delete can be performed on this editor.
      /// </summary>
      /// <returns></returns>
      public override bool CanDelete()
      {
         return SelectedFlow != null;
      }

      /// <summary>
      /// Deletes all the selected items on this editor after the user has selected extra deletion options.
      /// </summary>
      public override void DeleteSpecial()
      {
         DlgSelectDeletionMethod iDlg = new DlgSelectDeletionMethod();
         bool? iDlgRes = iDlg.ShowDialog();
         if (iDlgRes.HasValue == true && iDlgRes.Value == true)
         {
            Flow iToDelete = SelectedFlow;
            Debug.Assert(iToDelete != null);
            WindowMain.UndoStore.BeginUndoGroup(false);                                                          //we group all the data together so a single undo command cand restore the previous state.
            try
            {
               switch (iDlg.NeuronDeletionMethod)
               {
                  case DeletionMethod.Remove:
                     RemoveSelectedFlow();
                     break;
                  case DeletionMethod.DeleteIfNoRef:
                     RemoveSelectedFlow();
                     EditorsHelper.ProcessBranch(iToDelete.Item, iDlg.BranchHandling);
                     EditorsHelper.DeleteWhenNotUsed(iToDelete.Item, DeletionMethod.DeleteIfNoRef);
                     break;
                  case DeletionMethod.Delete:
                     if (iToDelete.Item.CanBeDeleted == true)
                     {
                        bool iDelete;
                        using (LinksAccessor iLinksIn = iToDelete.Item.LinksIn)
                        {
                           using (NeuronsAccessor iClusteredBy = iToDelete.Item.ClusteredBy)
                              iDelete = (iLinksIn.Items.Count == 1 && iClusteredBy.Items.Count == 0)
                                       || (iLinksIn.Items.Count == 0 && iClusteredBy.Items.Count == 1);
                        }
                        if (iDelete == true)
                           EditorsHelper.ProcessBranch(iToDelete.Item, iDlg.BranchHandling);
                        WindowMain.DeleteItemFromBrain(iToDelete.Item);
                     }
                     else
                        MessageBox.Show(string.Format("Neuron {0} can't be deleted because it is used as a meaning or info.", iToDelete), "Delete", MessageBoxButton.OK, MessageBoxImage.Warning);
                     break;
                  default:
                     throw new InvalidOperationException();
               }
            }
            finally
            {
               WindowMain.UndoStore.EndUndoGroup();
            }
         }
      }

      /// <summary>
      /// Determines whether a delete special can be performed
      /// </summary>
      /// <returns>
      /// 	<c>true</c> if this instance can do a special delete; otherwise, <c>false</c>.
      /// </returns>
      public override bool CanDeleteSpecial()
      {
         return CanDelete();
      }
   }
}
