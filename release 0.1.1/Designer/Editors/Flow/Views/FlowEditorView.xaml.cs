﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Interaction logic for FlowEditorView.xaml
   /// </summary>
   public partial class FlowEditorView : CtrlEditorBase
   {
      FlowItem fFirstFound = null;

      public FlowEditorView(): base()
      {
         InitializeComponent();
      }

      #region IsEditing

      /// <summary>
      /// IsEditing Dependency Property
      /// </summary>
      public static readonly DependencyProperty IsEditingProperty =
          DependencyProperty.Register("IsEditing", typeof(bool), typeof(FlowEditorView),
              new FrameworkPropertyMetadata((bool)false,
                  new PropertyChangedCallback(OnIsEditingChanged)));

      /// <summary>
      /// Gets or sets the IsEditing property.  This dependency property 
      /// indicates wether the currently selected item in the list is in edit mode or not.
      /// </summary>
      public bool IsEditing
      {
         get { return (bool)GetValue(IsEditingProperty); }
         set { SetValue(IsEditingProperty, value); }
      }

      /// <summary>
      /// Handles changes to the IsEditing property.
      /// </summary>
      private static void OnIsEditingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
      {
         ((FlowEditorView)d).OnIsEditingChanged(e);
      }

      /// <summary>
      /// Provides derived classes an opportunity to handle changes to the IsEditing property.
      /// </summary>
      protected virtual void OnIsEditingChanged(DependencyPropertyChangedEventArgs e)
      {
         //if ((bool)e.NewValue == false)
         //{
         //   if (fSelected != null)
         //      Keyboard.Focus(fSelected);                                               //need to focus to the container, and not the list so that nav keys keep working.
         //   else
         //      LstItems.Focus();
         //}
      }

      #endregion


      #region Readonly textboxes

      /// <summary>
      /// Handles the Click event of the EditTextBox control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void EditTextBox_Click(object sender, RoutedEventArgs e)
      {
         IsEditing = true;
      }


      /// <summary>
      /// Handles the LostFocus event of the TxtTitle control.
      /// </summary>
      /// <remarks>
      /// When looses focus, need to make certain that is editing is turned off.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void TxtTitle_LostKeybFocus(object sender, KeyboardFocusChangedEventArgs e)
      {
         IsEditing = false;
      }

      /// <summary>
      /// Handles the LostFocus event of the TxtTitle control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void TxtTitle_LostFocus(object sender, RoutedEventArgs e)
      {
         IsEditing = false;
      }



      /// <summary>
      /// Handles the PrvKeyDown event of the TxtTitle control.
      /// </summary>
      /// <remarks>
      /// when enter is pressed, need to stop editing.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
      private void TxtTitle_PrvKeyDown(object sender, KeyEventArgs e)
      {
         if (e.Key == Key.Enter || e.Key == Key.Return)
            IsEditing = false;
      }

      #endregion

      #region Commands
      #region Rename
      /// <summary>
      /// Handles the Executed event of the Rename control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void Rename_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         IsEditing = true;
      }

      /// <summary>
      /// Handles the CanExecute event of the Rename control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void Rename_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         e.CanExecute = LstItems != null && LstItems.SelectedItem != null;
      } 
      #endregion


      #region CanExecute
      /// <summary>
      /// Handles the CanExecute event of the InsertFlowItem Command.
      /// </summary>
      /// <remarks>
      /// We can add a flow item when the editor has a flow assigned.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void InsertFlowItem_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         if (LstItems != null)
         {
            Flow iFlow = LstItems.SelectedItem as Flow;
            if (iFlow != null)
            {
               e.CanExecute = iFlow.SelectedItem != null;
               return;
            }
         }
         e.CanExecute = false;
      }

      private void AddFlowItem_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         FlowEditor iEditor = (FlowEditor)DataContext;
         e.CanExecute = iEditor != null && iEditor.SelectedFlow != null;
      } 
      #endregion

      #region Option
      /// <summary>
      /// Handles the Executed event of the InsertOption Command.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void InsertOption_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will trigger multiple undo items
         try
         {
            Flow iFlow = (Flow)LstItems.SelectedItem;
            FlowItemConditional iNew = EditorsHelper.MakeFlowOption();
            EditorsHelper.InsertFlowItem(iNew, (FlowItem)iFlow.SelectedItem);
            iFlow.SelectedItems.Clear();
            iNew.IsSelected = true;                                                             //we do this, cause the insert is done with ctrl pressed, which does an 'add selected', so the previous is not removed from the list, which we don't want.
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      }

      private void AddOption_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will trigger multiple undo items
         try
         {
            Flow iFlow = (Flow)LstItems.SelectedItem;
            FlowItemConditional iNew = EditorsHelper.MakeFlowOption();
            EditorsHelper.AddFlowItem(iNew, (FlowItem)iFlow.SelectedItem, iFlow);
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      } 
      #endregion



      #region Loop
      /// <summary>
      /// Handles the Executed event of the InsertLoop Command.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void InsertLoop_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will trigger multiple undo items
         try
         {
            Flow iFlow = (Flow)LstItems.SelectedItem;
            FlowItemConditional iNew = EditorsHelper.MakeFlowLoop();
            EditorsHelper.InsertFlowItem(iNew, (FlowItem)iFlow.SelectedItem);
            iFlow.SelectedItems.Clear();
            iNew.IsSelected = true;                                                             //we do this, cause the insert is done with ctrl pressed, which does an 'add selected', so the previous is not removed from the list, which we don't want.
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      }

      private void AddLoop_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will trigger multiple undo items
         try
         {
            Flow iFlow = (Flow)LstItems.SelectedItem;
            FlowItemConditional iNew = EditorsHelper.MakeFlowLoop();
            EditorsHelper.AddFlowItem(iNew, (FlowItem)iFlow.SelectedItem, iFlow);
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      } 
      #endregion

      #region static
      /// <summary>
      /// Handles the Executed event of the InsertStatic Command.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void InsertStatic_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         Flow iFlow = (Flow)LstItems.SelectedItem;
         Debug.Assert(iFlow != null);
         iFlow.PopupDoesInsert = true;
         iFlow.PopupTarget = (UIElement)e.OriginalSource;
         iFlow.PopupIsOpen = true;
      }

      private void AddStatic_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         Flow iFlow = (Flow)LstItems.SelectedItem;
         Debug.Assert(iFlow != null);
         iFlow.PopupDoesInsert = false;
         iFlow.PopupTarget = (UIElement)e.OriginalSource;
         iFlow.PopupIsOpen = true;
      } 
      #endregion

      #region New object
      private void AddNewObject_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         DlgStringQuestion iIn = new DlgStringQuestion();
         iIn.Owner = App.Current.MainWindow;
         iIn.Question = "Object name:";
         iIn.Answer = "New object";
         iIn.Title = "New object";
         if ((bool)iIn.ShowDialog() == true)
         {
            WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will trigger multiple undo items
            try
            {
               Flow iFlow = (Flow)LstItems.SelectedItem;
               Neuron iMeaning;
               NeuronCluster iCluster = BrainHelper.CreateObject(iIn.Answer, out iMeaning);
               FlowItemStatic iStatic = new FlowItemStatic(iCluster);
               iStatic.NeuronInfo.DisplayTitle = iIn.Answer;
               EditorsHelper.AddFlowItem(iStatic, (FlowItem)iFlow.SelectedItem, iFlow);
            }
            finally
            {
               WindowMain.UndoStore.EndUndoGroup();
            }
         }
      }

      private void InsertNewObject_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         DlgStringQuestion iIn = new DlgStringQuestion();
         iIn.Owner = App.Current.MainWindow;
         iIn.Question = "Object name:";
         iIn.Answer = "New object";
         iIn.Title = "New object";
         if ((bool)iIn.ShowDialog() == true)
         {
            WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will trigger multiple undo items
            try
            {
               Flow iFlow = (Flow)LstItems.SelectedItem;
               Neuron iMeaning;
               NeuronCluster iCluster = BrainHelper.CreateObject(iIn.Answer, out iMeaning);
               FlowItemStatic iStatic = new FlowItemStatic(iCluster);
               iStatic.NeuronInfo.DisplayTitle = iIn.Answer;
               EditorsHelper.InsertFlowItem(iStatic, (FlowItem)iFlow.SelectedItem);
            }
            finally
            {
               WindowMain.UndoStore.EndUndoGroup();
            }
         }
      } 
      #endregion

      #region New neuron
      private void InsertNewNeuron_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         DlgStringQuestion iIn = new DlgStringQuestion();
         iIn.Owner = App.Current.MainWindow;
         iIn.Question = "Neuron name:";
         iIn.Answer = "New neuron";
         iIn.Title = "New neuron";
         if ((bool)iIn.ShowDialog() == true)
         {
            WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will trigger multiple undo items
            try
            {
               Flow iFlow = (Flow)LstItems.SelectedItem;
               Neuron iNew = new Neuron();
               WindowMain.AddItemToBrain(iNew);
               FlowItemStatic iStatic = new FlowItemStatic(iNew);
               iStatic.NeuronInfo.DisplayTitle = iIn.Answer;
               EditorsHelper.InsertFlowItem(iStatic, (FlowItem)iFlow.SelectedItem);
            }
            finally
            {
               WindowMain.UndoStore.EndUndoGroup();
            }
         }
      }

      private void AddNewNeuron_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         DlgStringQuestion iIn = new DlgStringQuestion();
         iIn.Owner = App.Current.MainWindow;
         iIn.Question = "Neuron name:";
         iIn.Answer = "New neuron";
         iIn.Title = "New neuron";
         if ((bool)iIn.ShowDialog() == true)
         {
            WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will trigger multiple undo items
            try
            {
               Flow iFlow = (Flow)LstItems.SelectedItem;
               Neuron iNew = new Neuron();
               WindowMain.AddItemToBrain(iNew);
               FlowItemStatic iStatic = new FlowItemStatic(iNew);
               iStatic.NeuronInfo.DisplayTitle = iIn.Answer;
               EditorsHelper.AddFlowItem(iStatic, (FlowItem)iFlow.SelectedItem, iFlow);
            }
            finally
            {
               WindowMain.UndoStore.EndUndoGroup();
            }
         }
      } 
      #endregion

      #region Conditional part
      /// <summary>
      /// Handles the CanExecute event of the InsertCondPart control.
      /// </summary>
      /// <remarks>
      /// Almost similar as for the other inserts, except when a listbox item is focused, in that case we need to check if the
      /// flow item is contained in a Flow-item-Conditional.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void InsertCondPart_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         if (LstItems != null)
         {
            Flow iFlow = LstItems.SelectedItem as Flow;
            if (iFlow != null && iFlow.SelectedItem != null)
            {
               e.CanExecute = iFlow.SelectedItem is FlowItemBlock | iFlow.SelectedItem.Owner is FlowItemBlock;
               return;
            }
         }
         e.CanExecute = false;
      }

      private void InsertCondPart_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will trigger multiple undo items
         try
         {
            Flow iFlow = (Flow)LstItems.SelectedItem;
            FlowItem iSelected = iFlow.SelectedItem;
            iFlow.SelectedItems.Clear();                                                        //we do this manually for the | cause this is inserted/added using 'shift + \' -> this means that the shift is presssed & SetIsSelected interprets this as if we want to add to the selection, which we don't want.
            FlowItemConditionalPart iNew = EditorsHelper.MakeFlowCondPart();
            EditorsHelper.InsertFlowItem(iNew, iSelected);
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      }

      private void AddCondPart_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will trigger multiple undo items
         try
         {
            Flow iFlow = (Flow)LstItems.SelectedItem;
            FlowItem iSelected = iFlow.SelectedItem;
            iFlow.SelectedItems.Clear();                                                        //we do this manually for the | cause this is inserted/added using 'shift + \' -> this means that the shift is presssed & SetIsSelected interprets this as if we want to add to the selection, which we don't want.
            FlowItemConditionalPart iNew = EditorsHelper.MakeFlowCondPart();
            EditorsHelper.AddFlowItem(iNew, iSelected, iFlow);
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      }

      #endregion 

      #region Change option/loop
      private void ChangeOptionToLoop_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will trigger multiple undo items
         try
         {
            Flow iFlow = LstItems.SelectedItem as Flow;
            if (iFlow != null && iFlow.SelectedItem != null)
            {
               FlowItemConditional iCond;
               if (iFlow.SelectedItem is FlowItemConditional)
                  iCond = (FlowItemConditional)iFlow.SelectedItem;
               else
                  iCond = iFlow.SelectedItem.FindFirstOwner<FlowItemConditional>();
               if (iCond != null)
                  iCond.IsLooped = true;
            }
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      }

      private void ChangeLoopToOption_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will trigger multiple undo items
         try
         {
            Flow iFlow = LstItems.SelectedItem as Flow;
            if (iFlow != null && iFlow.SelectedItem != null)
            {
               FlowItemConditional iCond;
               if (iFlow.SelectedItem is FlowItemConditional)
                  iCond = (FlowItemConditional)iFlow.SelectedItem;
               else
                  iCond = iFlow.SelectedItem.FindFirstOwner<FlowItemConditional>();
               if (iCond != null)
                  iCond.IsLooped = false;
            }
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      } 
      #endregion


      #region ToggleFlowLoopSelectionRequirement

      private void ToggleFlowLoopSelectionRequirement_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         if (LstItems != null)
         {
            Flow iFlow = LstItems.SelectedItem as Flow;
            if (iFlow != null && iFlow.SelectedItem != null)
            {
               FlowItemConditional iCond;
               if (iFlow.SelectedItem is FlowItemConditional)
                  iCond = (FlowItemConditional)iFlow.SelectedItem;
               else
                  iCond = iFlow.SelectedItem.FindFirstOwner<FlowItemConditional>();
               e.CanExecute = iCond != null;
               if (e.CanExecute == true)
                  BtnToggleSelection.IsChecked = iCond.IsSelectionRequired;
               else
                  BtnToggleSelection.IsChecked = false;
               return;
            }
         }
         e.CanExecute = false;
         BtnToggleSelection.IsChecked = false;
      }

      /// <summary>
      /// Handles the Executed event of the ToggleFlowLoopSelectionRequirement control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void ToggleFlowLoopSelectionRequirement_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will trigger multiple undo items
         try
         {
            Flow iFlow = LstItems.SelectedItem as Flow;
            if (iFlow != null && iFlow.SelectedItem != null)
            {
               FlowItemConditional iCond;
               if (iFlow.SelectedItem is FlowItemConditional)
                  iCond = (FlowItemConditional)iFlow.SelectedItem;
               else
                  iCond = iFlow.SelectedItem.FindFirstOwner<FlowItemConditional>();
               if (iCond != null)
                  iCond.IsSelectionRequired = !iCond.IsSelectionRequired;
            }
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      } 
      #endregion

      
      /// <summary>
      /// Handles the Executed event of the AddFlow control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void AddFlow_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will trigger multiple undo items
         try
         {
            FlowEditor iEditor = (FlowEditor)DataContext;
            Flow iFlow = EditorsHelper.MakeFlow();
            if (iFlow != null)
            {
               iEditor.Flows.Add(iFlow);
               iFlow.IsSelected = true;
               //            CtrlEditor.LstItems.Focus();
            }
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      }

      #region NextPage
      private void NextPage_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         FlowEditor iEditor = (FlowEditor)DataContext;
         Debug.Assert(iEditor != null);
         if (iEditor.SelectedFlow != null)
         {
            int iIndex = iEditor.Flows.IndexOf(iEditor.SelectedFlow);
            e.CanExecute = iIndex < iEditor.Flows.Count - 1;
         }
         else
            e.CanExecute = false;
      }

      private void NextPage_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowEditor iEditor = (FlowEditor)DataContext;
         Debug.Assert(iEditor != null);
         int iIndex = iEditor.Flows.IndexOf(iEditor.SelectedFlow);
         iEditor.SelectedFlow = iEditor.Flows[iIndex + 1];
      } 
      #endregion

      #region PrevPage
      private void PrevPage_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         FlowEditor iEditor = (FlowEditor)DataContext;
         Debug.Assert(iEditor != null);
         if (iEditor.SelectedFlow != null)
         {
            int iIndex = iEditor.Flows.IndexOf(iEditor.SelectedFlow);
            e.CanExecute = iIndex > 0;
         }
         else
            e.CanExecute = false;
      }

      private void PrevPage_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowEditor iEditor = (FlowEditor)DataContext;
         Debug.Assert(iEditor != null);
         int iIndex = iEditor.Flows.IndexOf(iEditor.SelectedFlow);
         iEditor.SelectedFlow = iEditor.Flows[iIndex - 1];
      } 
      #endregion
      

      
      

      
      #endregion



      /// <summary>
      /// Handles the SelectionChanged event of the CmbStaticItemDisplayMode control.
      /// </summary>
      /// <remarks>
      /// We invalidate the intire editor so that all the items will be redrawn with the new correct value.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
      private void CmbStaticItemDisplayMode_SelectionChanged(object sender, SelectionChangedEventArgs e)
      {
         FlowEditor iEditor = (FlowEditor)DataContext;
         if (iEditor != null)
            iEditor.ReDrawFlows();
 
      }

      #region Search
      
      private void BtnSearch_Click(object sender, RoutedEventArgs e)
      {
         string iToSearch = TxtToSearch.Text.ToLower();
         FlowEditor iEditor = (FlowEditor)DataContext;
         Flow iFlow = iEditor.SelectedFlow;
         int iIndex = 0;
         int iStartIndex = 0;
         if (iFlow != null)
            iStartIndex = iIndex = iEditor.Flows.IndexOf(iFlow);
         else if (iFlow == null && iEditor.Flows.Count > 0)
            iFlow = iEditor.Flows[0];
         FlowItem iSelected = null;
         if (iFlow != null)
         {
            iSelected = iFlow.SelectedItem ?? iFlow.Items[0];
            FlowItem iStart = null;
            while (iFlow != null)
            {
               if (iFlow.Items.Count > 0)                                                                      //only try to search if there is something to search.
               {
                  if (iSelected == null)
                     iSelected = iFlow.Items[0];
                  FlowItem iNext = iFlow.GetNext(iToSearch, iSelected);
                  if (iStart == null)                                                                       //the first time we get here, start is not yet found, so we set it here, this way, the next time we get to the first found item, we know we have found all items and need to quite.
                     iStart = iNext;
                  else if (iNext == iStart)
                  {
                     MessageBox.Show("End of search reached!");
                     return;
                  }
                  if (iNext != null)
                  {
                     if (fFirstFound == null)
                        fFirstFound = iNext;
                     else if (fFirstFound == iNext)
                     {
                        MessageBox.Show("End of search reached!");
                        fFirstFound = null;
                        return;
                     }
                     iFlow.IsSelected = true;
                     iNext.IsSelected = true;
                     return;
                  }
               }
               iIndex++;
               if (iIndex < iEditor.Flows.Count)
                  iFlow = iEditor.Flows[iIndex];
               else
               {
                  iFlow = iEditor.Flows[0];
                  iIndex = 0;
               }
               iSelected = null;
               if (iIndex == iStartIndex)
               {
                  MessageBox.Show("End of search reached!");
                  break;
               }
            }
         }
         MessageBox.Show("No items found!");                                                                //when we get here, nothing was found.
      } 
      

      private void TxtToSearch_KeyDown(object sender, KeyEventArgs e)
      {
         if (e.Key == Key.Enter || e.Key == Key.Return)
            BtnSearch_Click(sender, null);
         else
            fFirstFound = null;
      }

      #endregion


   }
}
