﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Interaction logic for FlowItemStaticView.xaml
   /// </summary>
   public partial class FlowItemStaticView : ContentControl
   {
      public FlowItemStaticView()
      {
         InitializeComponent();
      }

      /// <summary>
      /// Handles the MouseDoubleClick event of the ContentControl control.
      /// </summary>
      /// <remarks>
      /// Navigate to the link, if possible.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
      private void ContentControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
      {
         FlowItemStatic iStatic = (FlowItemStatic)DataContext;
         if (iStatic != null && iStatic.IsLink == true)
         {
            FlowEditor iEditor = iStatic.FindFirstOwner<FlowEditor>();
            Debug.Assert(iEditor != null);
            Flow iFound = (from i in iEditor.Flows where i.Item == iStatic.Item select i).FirstOrDefault();
            if (iFound == null)
            {
              MessageBoxResult iRes = MessageBox.Show(string.Format("The flow '{0}' is not yet included in this editor, do so now?", iStatic.NeuronInfo.DisplayTitle), "Go to flow", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.OK);
              if (iRes == MessageBoxResult.OK)
              {
                 iFound = new Flow();
                 iFound.ItemID = iStatic.Item.ID;
                 iEditor.Flows.Add(iFound);
              }
            }
            if (iFound != null)
               iEditor.SelectedFlow = iFound;
         }
      }
   }
}
