﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Provides custom logic for the <see cref="FlowItemStaticView"/> to select the appropriate template
   /// </summary>
   public class StaticFlowItemContentSelector: DataTemplateSelector
   {
      /// <summary>
      /// When overridden in a derived class, returns a <see cref="T:System.Windows.DataTemplate"/> based on custom logic.
      /// </summary>
      /// <param name="item">The data object for which to select the template.</param>
      /// <param name="container">The data-bound object.</param>
      /// <returns>
      /// Returns a <see cref="T:System.Windows.DataTemplate"/> or null. The default value is null.
      /// </returns>
      public override DataTemplate SelectTemplate(object item, DependencyObject container)
      { 
         FrameworkElement iCont = container as FrameworkElement;
         if (Properties.Settings.Default.FlowItemDisplayMode == 0)
            return iCont.TryFindResource("FlowItemStaticNormalView") as DataTemplate;
         else if (Properties.Settings.Default.FlowItemDisplayMode == 1)
            return iCont.TryFindResource("FlowItemStaticLeftIcon") as DataTemplate;
         else if (Properties.Settings.Default.FlowItemDisplayMode == 2)
            return iCont.TryFindResource("FlowItemStaticUnderIcon") as DataTemplate;
         else
            return base.SelectTemplate(item, container);
      }
   }
}
