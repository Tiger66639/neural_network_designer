﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeuralNetworkDesigne.Data;
using System.Windows.Documents;
using System.Xml;
using System.Windows;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// The data class for the frame editor.
   /// </summary>
   public class FrameEditor : EditorBase
   {
      #region fields
      ObservedCollection<Frame> fFrames;
      Frame fSelectedFrame;
      #endregion

      #region ctor
      /// <summary>
      /// Initializes a new instance of the <see cref="FrameEditor"/> class.
      /// </summary>
      public FrameEditor()
      {
         fFrames = new ObservedCollection<Frame>(this);
      } 
      #endregion

      #region Prop
      #region Frames

      /// <summary>
      /// Gets the list of frames
      /// </summary>
      public ObservedCollection<Frame> Frames
      {
         get { return fFrames; }
      }

      #endregion


      #region Icon
      /// <summary>
      /// Gets the resource path to the icon that should be used for this editor.  This is usually class specific.
      /// </summary>
      /// <value></value>
      public override string Icon
      {
         get { return "/Images/Frame/Frame.png"; }
      }
      #endregion

      #region SelectedFrame

      /// <summary>
      /// Gets/sets the currently selected frame.
      /// </summary>
      public Frame SelectedFrame
      {
         get
         {
            return fSelectedFrame;
         }
         set
         {
            fSelectedFrame = value;
            OnPropertyChanged("SelectedFrame");
         }
      }

      #endregion

      #region DescriptionTitle
      /// <summary>
      /// Gets a title that the description editor can use to display in the header.
      /// </summary>
      /// <value></value>
      public override string DescriptionTitle
      {
         get { return Name + " - Frame Editor"; }
      } 
      #endregion

      #endregion

      #region Functions

      /// <summary>
      /// Reads the fields/properties of the class.
      /// </summary>
      /// <param name="reader">The reader.</param>
      /// <returns>
      /// True if the item was properly read, otherwise false.
      /// </returns>
      /// <remarks>
      /// This function is called for each element that is found, so this function should check which element it is
      /// and only read that element accordingly.
      /// </remarks>
      protected override bool ReadXmlInternal(XmlReader reader)
      {
         if (reader.Name == "Frame")
         {
            ulong id = XmlStore.ReadElement<ulong>(reader, "Frame");
            NeuronCluster iCluster = Brain.Current[id] as NeuronCluster;
            Frame iFrame = new Frame(iCluster);
            Frames.Add(iFrame);
            return true;
         }

         else
            return base.ReadXmlInternal(reader);
      }

      /// <summary>
      /// Converts an object into its XML representation.
      /// </summary>
      /// <param name="writer">The <see cref="T:System.Xml.XmlWriter"/> stream to which the object is serialized.</param>
      public override void WriteXml(XmlWriter writer)
      {
         base.WriteXml(writer);
         foreach (Frame i in Frames)
            XmlStore.WriteElement<ulong>(writer, "Frame", i.Item.ID);
      }

      #endregion

      #region Clipboard
      protected override void CopyToClipboard(DataObject data)
      {
         throw new NotImplementedException();
      }

      public override bool CanCopyToClipboard()
      {
         throw new NotImplementedException();
      }

      public override bool CanPasteSpecialFromClipboard()
      {
         throw new NotImplementedException();
      }

      public override void PasteSpecialFromClipboard()
      {
         throw new NotImplementedException();
      }



      public override bool CanPasteFromClipboard()
      {
         if (base.CanPasteFromClipboard() == true)
         {
         }
         throw new NotImplementedException();

      }

      public override void PasteFromClipboard()
      {
         throw new NotImplementedException();
      } 
      #endregion

      public override void Delete()
      {
         throw new NotImplementedException();
      }

      public override bool CanDelete()
      {
         throw new NotImplementedException();
      }

      public override void DeleteSpecial()
      {
         throw new NotImplementedException();
      }

      public override bool CanDeleteSpecial()
      {
         throw new NotImplementedException();
      }
   }
}
