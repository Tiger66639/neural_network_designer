﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// A wrapper class for a <see cref="NeuronCluster"/> that describes a known order of <see cref="FrameElement"/>s in a <see cref="Frame"/>.
   /// </summary>
   public class FrameSequence : FrameItemBase
   {
      #region ctor
      FrameElementCollection fItems;
      ObservableCollection<FrameElement> fNotUsedItems = new ObservableCollection<FrameElement>();  
      #endregion

      #region ctor
      /// <summary>
      /// Initializes a new instance of the <see cref="FrameOrder"/> class.
      /// </summary>
      /// <param name="towrap">The towrap.</param>
      public FrameSequence(Neuron toWrap)
         : base(toWrap)
      {
         fItems = new FrameElementCollection(null, toWrap as NeuronCluster);                                //we can't own this list, cause that would cause us to also own the frame elements, which we don't, the frame does, so no owner.
         fItems.CollectionChanged += new NotifyCollectionChangedEventHandler(fItems_CollectionChanged);
      }

      #endregion


      #region Prop
      #region Items

      /// <summary>
      /// Gets the list of frame elements that are declared in this sequence.
      /// </summary>
      public FrameElementCollection Items
      {
         get { return fItems; }
      }

      #endregion


      #region NotUsedItems

      /// <summary>
      /// Gets the list of frame elements not yet used by this item.
      /// </summary>
      public ObservableCollection<FrameElement> NotUsedItems
      {
         get { return fNotUsedItems; }
      }

      #endregion

      #region Owner
      /// <summary>
      /// Gets or sets the owner.
      /// </summary>
      /// <remarks>
      /// When changed, we need to monitor the list of possible 'FrameElements'.
      /// </remarks>
      /// <value>The owner.</value>
      public override INeuronWrapper Owner
      {
         get
         {
            return base.Owner;
         }
         set
         {
            if (value != base.Owner)
            {
               Frame iOwner = base.Owner as Frame;
               if (iOwner != null)
               {
                  CollectionChangedEventManager.RemoveListener(iOwner.Elements, this);
                  NotUsedItems.Clear();
               }
               base.Owner = value;
               iOwner = base.Owner as Frame;                                                          //need to recalculate the owner cause it has changed.
               if (iOwner != null)
               {
                  foreach (FrameElement i in iOwner.Elements)
                     NotUsedItems.Add(i);
                  CollectionChangedEventManager.AddListener(iOwner.Elements, this);
               }
            }
         }
      }
      #endregion 
      #endregion


      #region Functions

      /// <summary>
      /// Receives events from the centralized event manager.
      /// </summary>
      /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
      /// <param name="sender">Object that originated the event.</param>
      /// <param name="e">Event data.</param>
      /// <returns>
      /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
      /// </returns>
      public override bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(CollectionChangedEventManager))
         {
            Elements_CollectionChanged(sender, (NotifyCollectionChangedEventArgs)e);
            return true;
         }
         return false;
      }

      /// <summary>
      /// Handles the CollectionChanged event of the fItems control.
      /// </summary>
      /// <remarks>
      /// Keeps the frame elements of the owner (frame) in sync with our 2 lists.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
      void Elements_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         fItems.CollectionChanged -= new NotifyCollectionChangedEventHandler(fItems_CollectionChanged);     //this function can change the content of fItems, in which case, we don't want other parts to update.
         try
         {
            switch (e.Action)
            {
               case NotifyCollectionChangedAction.Add:
                  foreach (FrameElement i in e.NewItems)
                     NotUsedItems.Add(i);
                  break;
               case NotifyCollectionChangedAction.Move:
                  break;
               case NotifyCollectionChangedAction.Remove:
                  foreach (FrameElement i in e.OldItems)
                  {
                     if (NotUsedItems.Remove(i) == false)
                        Items.Remove(i);
                  }
                  break;
               case NotifyCollectionChangedAction.Replace:
                  int iIndex = 0;
                  foreach (FrameElement i in e.OldItems)
                  {
                     int iPos = NotUsedItems.IndexOf(i);
                     if (iPos == -1)
                     {
                        iPos = Items.IndexOf(i);
                        if (iPos != -1)
                           Items[iPos] = (FrameElement)e.NewItems[iIndex++];
                        else
                           throw new InvalidOperationException();
                     }
                     else
                        NotUsedItems[iPos] = (FrameElement)e.NewItems[iIndex++];
                  }
                  break;
               case NotifyCollectionChangedAction.Reset:
                  NotUsedItems.Clear();
                  Items.Clear();
                  break;
               default:
                  break;
            }
         }
         finally
         {
            fItems.CollectionChanged += new NotifyCollectionChangedEventHandler(fItems_CollectionChanged);
         }
      }

      /// <summary>
      /// Handles the CollectionChanged event of the fItems control.
      /// </summary>
      /// <remarks>
      /// Need to keep the list of not used items in sync.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
      void fItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         switch (e.Action)
         {
            case NotifyCollectionChangedAction.Add:
               foreach (FrameElement i in e.NewItems)
                  NotUsedItems.Remove(i);
               break;
            case NotifyCollectionChangedAction.Move:
               break;
            case NotifyCollectionChangedAction.Remove:
               foreach (FrameElement i in e.OldItems)
                  NotUsedItems.Add(i);
               break;
            case NotifyCollectionChangedAction.Replace:
               break;
            case NotifyCollectionChangedAction.Reset:
               break;
            default:
               break;
         }
      } 

      #endregion
   }
}
