﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DnD;
using System.Windows.Controls;
using System.Diagnostics;
using System.Windows;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Drop advisor for the child items of <see cref="FrameEvokerCollection"/> objects.  
   /// </summary>
   public class FrameEvokerListItemDropAdvisor : DropTargetBase
   {
      #region prop

      #region UsePreviewEvents
      /// <summary>
      /// Gets if the preview event versions should be used or not.
      /// </summary>
      /// <remarks>
      /// don't use preview events cause than the sub lists don't get used but only the main list cause this gets the events first,
      /// while we usually want to drop in a sublist.
      /// </remarks>
      public override bool UsePreviewEvents
      {
         get
         {
            return false;
         }
      }
      #endregion

      #region Items

      /// <summary>
      /// Gets the list containing all the code that the UI to which advisor is attached too, displays data for.
      /// </summary>
      public FrameEvokerCollection Items
      {
         get
         {
            ItemsControl iItemsControl = ItemsControl.ItemsControlFromItemContainer(TargetUI);
            Debug.Assert(iItemsControl != null);
            return iItemsControl.ItemsSource as FrameEvokerCollection;
         }
      }

      #endregion
      #endregion

      #region Overrides
      public override void OnDropCompleted(DragEventArgs arg, Point dropPoint)
      {
         FrameEvoker iItem = arg.Data.GetData(Properties.Resources.FrameEvokerFormat) as FrameEvoker;
         if (iItem != null && Items.Contains(iItem) == true && (arg.Effects & DragDropEffects.Move) == DragDropEffects.Move)
            TryMoveItem(arg, dropPoint, iItem);
         else
            TryCreateNewCodeItem(arg, dropPoint);                                                  //we are not moving around an item, so add new code item.
      }


      public override bool IsValidDataObject(IDataObject obj)
      {
         return obj.GetDataPresent(Properties.Resources.FrameEvokerFormat)
                || obj.GetDataPresent(Properties.Resources.NeuronIDFormat);
      }

      #endregion

      private void TryMoveItem(DragEventArgs arg, Point dropPoint, FrameEvoker iItem)
      {
         FrameEvoker iDroppedOn = ((FrameworkElement)TargetUI).DataContext as FrameEvoker;
         FrameEvokerCollection iList = Items;
         int iNewIndex = iList.IndexOf(iDroppedOn);
         int iOldIndex = iList.IndexOf(iItem);
         if (iOldIndex == -1)
         {
            FrameEvoker iNew = new FrameEvoker(iItem.Item);                                                         //we always create a new item, even with a move, cause we don't know how the item was being dragged (with shift pressed or not), this is the savest and easiest way.
            iList.Insert(iNewIndex, iNew);
         }
         else
         {
            iList.Move(iOldIndex, iNewIndex);
            arg.Effects = DragDropEffects.None;
         }
      }

      private void TryCreateNewCodeItem(DragEventArgs obj, Point dropPoint)
      {
         ulong iId = (ulong)obj.Data.GetData(Properties.Resources.NeuronIDFormat);
         FrameEvoker iNew = new FrameEvoker(Brain.Current[iId]);
         FrameEvoker iDroppedOn = ((FrameworkElement)TargetUI).DataContext as FrameEvoker;
         Debug.Assert(iNew != null && iDroppedOn != null);
         FrameEvokerCollection iList = Items;
         Debug.Assert(iList != null);
         iList.Insert(iList.IndexOf(iDroppedOn), iNew);
      }
   }
}

