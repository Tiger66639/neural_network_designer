﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// A wrapper class for <see cref="NeuronCluster"/>s that reperesent objects which trigger the frame they are assigned to.
   /// </summary>
   public class FrameEvoker : FrameItemBase
   {
      #region ctor/~
      /// <summary>
      /// Initializes a new instance of the <see cref="FrameOrder"/> class.
      /// </summary>
      /// <param name="towrap">The towrap.</param>
      public FrameEvoker(Neuron toWrap)
         : base(toWrap)
      {
         LinkChangedEventManager.AddListener(Brain.Current, this);
      }

      ~FrameEvoker()
      {
         LinkChangedEventManager.RemoveListener(Brain.Current, this);
      } 
      #endregion

      #region POS

      /// <summary>
      /// Gets/sets the part of speech value for the item being wrapped.
      /// </summary>
      public Neuron POS
      {
         get
         {
            return Item.FindFirstOut((ulong)PredefinedNeurons.POS);
         }
         set
         {
            Neuron iCur = POS;
            if (iCur != value)
            {
               OnPropertyChanging("POS", iCur, value);
               Item.SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.POS, value);
            }
            
         }
      }

      #endregion

      /// <summary>
      /// Receives events from the centralized event manager.
      /// </summary>
      /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
      /// <param name="sender">Object that originated the event.</param>
      /// <param name="e">Event data.</param>
      /// <returns>
      /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
      /// </returns>
      public override bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(LinkChangedEventManager))
         {
            LinkChanged(sender, (LinkChangedEventArgs)e);
            return true;
         }
         return false;
      }

      /// <summary>
      /// Called when a link has been changed, checks if this is the pos link on our wrapped object, if so, raise the property changed
      /// so that everybody knows about the change.
      /// </summary>
      /// <param name="sender">The sender.</param>
      /// <param name="e">The <see cref="NeuralNetworkDesigne.HAB.LinkChangedEventArgs"/> instance containing the event data.</param>
      private void LinkChanged(object sender, LinkChangedEventArgs e)
      {
         if (e.OriginalSource.FromID == Item.ID && e.OriginalSource.MeaningID == (ulong)PredefinedNeurons.POS)
            OnPropertyChanged("POS");
      }
   }
}
