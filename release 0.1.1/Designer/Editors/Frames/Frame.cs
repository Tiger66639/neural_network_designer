﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeuralNetworkDesigne.Data;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// A wrapper for a <see cref="NeuronCluster"/> that represents a Frame.
   /// </summary>
   /// <remarks>
   /// A frame contains information about a single sentence: what are it's possible elements, the allowed order of 
   /// elements + triggers.
   /// </remarks>
   public class Frame : OwnedObject<FrameEditor>, INeuronWrapper, INeuronInfo, IWeakEventListener
   {

      FrameElementCollection fElements;
      FrameOrderCollection fSequences;
      FrameEvokerCollection fEvokers;

      #region ctor
      /// <summary>
      /// Initializes a new instance of the <see cref="Frame"/> class.
      /// </summary>
      public Frame(NeuronCluster toWrap)
      {
         Debug.Assert(toWrap != null);
         fElements = new FrameElementCollection(this, toWrap);

         NeuronCluster iFound = toWrap.FindFirstOut((ulong)PredefinedNeurons.FrameSequences) as NeuronCluster;
         if (iFound != null)
            fSequences = new FrameOrderCollection(this, iFound);
         else
            fSequences = new FrameOrderCollection(this, (ulong)PredefinedNeurons.FrameSequences);

         iFound = toWrap.FindFirstOut((ulong)PredefinedNeurons.FrameEvokers) as NeuronCluster;
         if (iFound != null)
            fEvokers = new FrameEvokerCollection(this, iFound);
         else
            fEvokers = new FrameEvokerCollection(this, (ulong)PredefinedNeurons.FrameEvokers);
         LinkChangedEventManager.AddListener(Brain.Current, this);
         NeuronChangedEventManager.AddListener(Brain.Current, this);
      }

      ~Frame()
      {
         LinkChangedEventManager.RemoveListener(Brain.Current, this);
         NeuronChangedEventManager.RemoveListener(Brain.Current, this);
      }

      #endregion

      #region prop
      #region Elements

      /// <summary>
      /// Gets the list of frame elements declared in the frame.
      /// </summary>
      public FrameElementCollection Elements
      {
         get { return fElements; }
      }

      #endregion 

      #region Sequences

      /// <summary>
      /// Gets the list of FrameOrder objects which represent known frame element sequences.
      /// </summary>
      public FrameOrderCollection Sequences
      {
         get { return fSequences; }
      }

      #endregion

      #region Evokers

      /// <summary>
      /// Gets the list of evokers for this frame.
      /// </summary>
      public FrameEvokerCollection Evokers
      {
         get { return fEvokers; }
      }

      #endregion

      #region Item (INeuronWrapper Members)

      /// <summary>
      /// Gets the item.
      /// </summary>
      /// <value>The item.</value>
      public Neuron Item
      {
         get { return fElements.Cluster; }
      }

      #endregion

      #region NeuronInfo(INeuronInfo Members)

      /// <summary>
      /// Gets the extra info for the specified neuron.  Can be null.
      /// </summary>
      /// <value></value>
      public NeuronData NeuronInfo
      {
         get { return BrainData.Current.NeuronInfo[fElements.Cluster.ID]; }
      }

      #endregion

      #endregion

      #region Functions

      /// <summary>
      /// Receives events from the centralized event manager.
      /// </summary>
      /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
      /// <param name="sender">Object that originated the event.</param>
      /// <param name="e">Event data.</param>
      /// <returns>
      /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
      /// </returns>
      public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(LinkChangedEventManager))
         {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<LinkChangedEventArgs>(LinkChanged), e);         
            return true;
         }
         else if (managerType == typeof(NeuronChangedEventManager))
         {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<NeuronChangedEventArgs>(NeuronChanged), e);
            return true;
         }
         return false;
      }

      /// <summary>
      /// Called when a link has been changed, checks if this is the pos link on our wrapped object, if so, raise the property changed
      /// so that everybody knows about the change.
      /// </summary>
      /// <param name="sender">The sender.</param>
      /// <param name="e">The <see cref="NeuralNetworkDesigne.HAB.LinkChangedEventArgs"/> instance containing the event data.</param>
      private void LinkChanged(LinkChangedEventArgs e)
      {
         if (e.OriginalSource.FromID == Item.ID && e.OriginalSource.MeaningID == (ulong)PredefinedNeurons.POS)
            OnPropertyChanged("POS");
      }

      private void NeuronChanged(NeuronChangedEventArgs e)
      {
         if (e.OriginalSource == Item)                                        //this item is deleted
         {
            if (e.Action == BrainAction.Removed)                              //note: don't change the owner's list, it does this automatically.
               fElements = null;
            else if (e.Action == BrainAction.Changed)
            {
               fElements = new FrameElementCollection(this, (NeuronCluster)e.NewValue);
               OnPropertyChanged("Elements");
            }
         }
         else if (e.OriginalSource == Evokers.Cluster)
         {
            if (e.Action == BrainAction.Removed)
               fEvokers = null;
            else if (e.Action == BrainAction.Changed)
               fEvokers = new FrameEvokerCollection(this, (NeuronCluster)e.NewValue);
            OnPropertyChanged("Evokers");
         }
         else if (e.OriginalSource == Sequences.Cluster)
         {
            if (e.Action == BrainAction.Removed)
               fSequences = null;
            else if (e.Action == BrainAction.Changed)
               fSequences = new FrameOrderCollection(this, (NeuronCluster)e.NewValue);
            OnPropertyChanged("Evokers");
         }
      }

      #endregion

   }
}
