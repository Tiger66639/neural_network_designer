﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Interaction logic for CtrlCodeEditorPage.xaml
    /// </summary>
    public partial class CtrlCodeEditorPage : UserControl
    {
        #region Fields

        //List<NeuronToolBoxItem> fRegisteredToolboxItems;
        private CodeEditorPage fPage;

        #endregion Fields

        #region ctor

        public CtrlCodeEditorPage()
        {
            InitializeComponent();
            DataContextChanged += new DependencyPropertyChangedEventHandler(CtrlCodeEditorPage_DataContextChanged);
        }

        #endregion ctor

        #region Event handlers

        private void CtrlCodeEditorPage_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            fPage = DataContext as CodeEditorPage;
        }

        ///// <summary>
        ///// When this control gets loaded, and there are no toolbox items created yet for the list of variables on this
        ///// page, do so now.
        ///// </summary>
        //private void UserControl_Loaded(object sender, RoutedEventArgs e)
        //{
        //   CreateToolBoxItems();
        //}

        ///// <summary>
        ///// If the list of toolbox items is not yet removed from the braindata, do so now
        ///// </summary>
        //private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        //{
        //   RemoveToolBoxItems();
        //}

        //void RegisteredVariables_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        //{
        //   CodeEditorPage iPage = (CodeEditorPage)DataContext;
        //   switch (e.Action)
        //   {
        //      case NotifyCollectionChangedAction.Add:
        //         foreach (Variable i in e.NewItems)
        //            AddVariable(i, iPage);
        //         break;
        //      case NotifyCollectionChangedAction.Remove:
        //         foreach (Variable i in e.OldItems)
        //            RemoveVariable(i, iPage);
        //         break;
        //      case NotifyCollectionChangedAction.Replace:
        //         break;
        //      case NotifyCollectionChangedAction.Reset:
        //         foreach (ToolBoxItem i in fRegisteredToolboxItems)
        //            BrainData.Current.ToolBoxItems.Remove(i);
        //         fRegisteredToolboxItems.Clear();
        //         break;
        //      default:
        //         break;
        //   }
        //}

        private void ListBox_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftShift) == false && Keyboard.IsKeyDown(Key.RightShift) == false)
            {
                ItemsControl iSender = (ItemsControl)sender;
                CodeEditorPage iPage = (CodeEditorPage)iSender.DataContext;
                iPage.SelectedItems.Clear();
            }
        }

        #endregion Event handlers

        #region Commands

        #region Order

        private void MoveUp_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            CodeEditorPage iPage = (CodeEditorPage)DataContext;
            e.CanExecute = iPage.CanMoveUpFor(iPage.Items);
        }

        private void MoveDown_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            CodeEditorPage iPage = (CodeEditorPage)DataContext;
            e.CanExecute = iPage.CanMoveDownFor(iPage.Items);
        }

        private void MoveUp_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeEditorPage iPage = (CodeEditorPage)DataContext;
            iPage.MoveUpFor(iPage.Items);
        }

        private void MoveDown_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeEditorPage iPage = (CodeEditorPage)DataContext;
            iPage.MoveDownFor(iPage.Items);
        }

        private void MoveToEnd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeEditorPage iPage = (CodeEditorPage)DataContext;
            iPage.MoveToEndFor(iPage.Items);
        }

        private void MoveToHome_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeEditorPage iPage = (CodeEditorPage)DataContext;
            iPage.MoveToStartFor(iPage.Items);
        }

        #endregion Order

        #region ViewCode

        /// <summary>
        /// Handles the Executed event of the RemoveCmd control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void ViewCodeCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeItem iSelected = fPage.SelectedItem;
            if (iSelected != null)
                ((WindowMain)App.Current.MainWindow).ViewCodeForNeuron(iSelected.Item);
        }

        #endregion ViewCode

        #region Sync

        /// <summary>
        /// Handles the Executed event of the SyncCmd control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void SyncCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeItem iSelected = fPage.SelectedItem;
            if (iSelected != null)
                ((WindowMain)App.Current.MainWindow).SyncExplorerToNeuron(iSelected.Item);
        }

        #endregion Sync

        #region Rename

        /// <summary>
        /// Handles the Executed event of the RenameCmd control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void RenameCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeEditorPage iPage = (CodeEditorPage)DataContext;
            Debug.Assert(iPage != null);
            DlgStringQuestion iIn = new DlgStringQuestion();
            iIn.Owner = App.Current.MainWindow;
            iIn.Question = "new title:";
            iIn.Answer = iPage.SelectedItem.NeuronInfo.DisplayTitle;
            iIn.Title = "Change code item title";
            if ((bool)iIn.ShowDialog() == true)
                iPage.SelectedItem.NeuronInfo.DisplayTitle = iIn.Answer;
        }

        #endregion Rename

        #endregion Commands

        private void ViewCodeCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = fPage != null && fPage.SelectedItems.Count > 0;
        }

        private void UserControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Focus();
        }

        #region functions

        //private void CreateToolBoxItems()
        //{
        //   if (fRegisteredToolboxItems == null && fPage != null)
        //   {
        //      fRegisteredToolboxItems = new List<NeuronToolBoxItem>();
        //      foreach (Variable i in fPage.RegisteredVariables)
        //         AddVariable(i, fPage);
        //      fPage.RegisteredVariables.CollectionChanged += new NotifyCollectionChangedEventHandler(RegisteredVariables_CollectionChanged);
        //   }
        //}

        ///// <summary>
        ///// Adds a variable to the list of toolbox items of the brain so they can appear on any toolbox controls.
        ///// </summary>
        ///// <param name="variable"></param>
        ///// <param name="page"></param>
        //private void AddVariable(Variable variable, CodeEditorPage page)
        //{
        //   NeuronToolBoxItem iNew = new NeuronToolBoxItem();
        //   iNew.Item = variable;
        //   iNew.Category = string.Format("Variables for {0} - {1}", BrainData.Current.NeuronInfo[page.Item.ID].DisplayTitle, page.Title);
        //   BrainData.Current.ToolBoxItems.Add(iNew);
        //   fRegisteredToolboxItems.Add(iNew);
        //}

        //private void RemoveToolBoxItems()
        //{
        //   if (fRegisteredToolboxItems != null && fPage != null)
        //   {
        //      fPage.RegisteredVariables.CollectionChanged -= new NotifyCollectionChangedEventHandler(RegisteredVariables_CollectionChanged);
        //      foreach (NeuronToolBoxItem i in fRegisteredToolboxItems)
        //         BrainData.Current.ToolBoxItems.Remove(i);
        //      fRegisteredToolboxItems = null;
        //   }
        //}

        //private void RemoveVariable(Variable i, CodeEditorPage iPage)
        //{
        //   NeuronToolBoxItem iToolboxItem = (from iReg in fRegisteredToolboxItems
        //                                     where iReg.Item == i
        //                                     select iReg).FirstOrDefault();
        //   if (iToolboxItem != null)
        //   {
        //      BrainData.Current.ToolBoxItems.Remove(iToolboxItem);
        //      fRegisteredToolboxItems.Remove(iToolboxItem);
        //   }
        //}

        #endregion functions
    }
}