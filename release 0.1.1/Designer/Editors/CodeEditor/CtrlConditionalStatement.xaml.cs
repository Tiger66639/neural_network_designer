﻿using System;
using System.Windows.Controls;
using System.Windows.Input;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Interaction logic for CtrlConditionalStatement.xaml
    /// </summary>
    public partial class CtrlConditionalStatement : CtrlEditorItem
    {
        public CtrlConditionalStatement()
        {
            InitializeComponent();
        }

        private void LoopStyle_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                NeuronInfo iInfo = e.AddedItems[0] as NeuronInfo;
                LoopStylePart.Content = new CodeItemStatic(iInfo.Item);
            }
            else if (e.RemovedItems.Count > 0)
                LoopStylePart.Content = null;
        }



        #region Order

        private void MoveUp_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            CodeItemConditionalStatement iContext = (CodeItemConditionalStatement)DataContext;
            CodeEditorPage iPage = iContext.Root as CodeEditorPage;
            e.CanExecute = iPage != null && iPage.CanMoveUpFor(iContext.Children);
        }

        private void MoveDown_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            CodeItemConditionalStatement iContext = (CodeItemConditionalStatement)DataContext;
            CodeEditorPage iPage = iContext.Root as CodeEditorPage;
            e.CanExecute = iPage != null && iPage.CanMoveDownFor(iContext.Children);
        }

        private void MoveUp_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeItemConditionalStatement iContext = (CodeItemConditionalStatement)DataContext;
            CodeEditorPage iPage = iContext.Root as CodeEditorPage;
            if (iPage != null)
                iPage.MoveUpFor(iContext.Children);
            else
                throw new NotSupportedException();
        }

        private void MoveDown_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeItemConditionalStatement iContext = (CodeItemConditionalStatement)DataContext;
            CodeEditorPage iPage = iContext.Root as CodeEditorPage;
            if (iPage != null)
                iPage.MoveDownFor(iContext.Children);
            else
                throw new NotSupportedException();
        }

        private void MoveToEnd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeItemConditionalStatement iContext = (CodeItemConditionalStatement)DataContext;
            CodeEditorPage iPage = iContext.Root as CodeEditorPage;
            if (iPage != null)
                iPage.MoveToEndFor(iContext.Children);
            else
                throw new NotSupportedException();
        }

        private void MoveToHome_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeItemConditionalStatement iContext = (CodeItemConditionalStatement)DataContext;
            CodeEditorPage iPage = iContext.Root as CodeEditorPage;
            if (iPage != null)
                iPage.MoveToStartFor(iContext.Children);
            else
                throw new NotSupportedException();
        }

        #endregion Order
    }
}