﻿using System.Windows.Controls;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Interaction logic for CtrlGlobal.xaml
    /// </summary>
    public partial class CtrlGlobal : CtrlEditorItem
    {
        public CtrlGlobal()
        {
            InitializeComponent();
        }

        private void SplitReaction_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                NeuronInfo iInfo = e.AddedItems[0] as NeuronInfo;
                SplitReactionPart.Content = new CodeItemStatic(iInfo.Item);
            }
            else if (e.RemovedItems.Count > 0)
                SplitReactionPart.Content = null;
        }
    }
}