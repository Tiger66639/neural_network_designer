﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Interaction logic for CodeEditor.xaml
    /// </summary>
    public partial class CodeEditorView : CtrlEditorBase
    {
        private CodeEditor fEditor;                                                                             //datacontext is lost when unloaded.

        public CodeEditorView() : base()
        {
            InitializeComponent();
        }

        private void RunNeuron_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Processor iProc = ProcessorManager.Current.GetProcessor();
            CodeEditorPage iPage = TabPages.SelectedItem as CodeEditorPage;
            Action<NeuronCluster> iCallAsync = new Action<NeuronCluster>(iProc.Call);
            iCallAsync.BeginInvoke(iPage.Items.Cluster, null, null);                                     //we have to call async, otherwise the ui can get stuck because the debugger is doing a thread sync by waiting.
        }

        private void RunNeuron_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void MnuAddCodePage_Click(object sender, RoutedEventArgs e)
        {
            FrameworkElement iSender = e.OriginalSource as FrameworkElement;
            if (iSender != null)
            {
                NeuronData iData = iSender.DataContext as NeuronData;
                AddNewCodePage(iData);
            }
        }

        private void AddNewCodePage(NeuronData iData)
        {
            if (iData != null)
            {
                CodeEditor iEditor = (CodeEditor)DataContext;
                Debug.Assert(iEditor != null);
                iEditor.EntryPoints.Add(new CodeEditorPage(iData.DisplayTitle, iEditor.Item, iData.ID));         //only initiate the cluster when items are assigned to it.
                iEditor.SelectedIndex = iEditor.EntryPoints.Count - 1;
            }
        }

        private void LstPossiblePages_MouseDoubleClick(object sender, RoutedEventArgs e)
        {
            AddNewCodePage(LstPossiblePages.SelectedItem as NeuronData);
            PopupAddPage.IsOpen = false;
        }
    }
}