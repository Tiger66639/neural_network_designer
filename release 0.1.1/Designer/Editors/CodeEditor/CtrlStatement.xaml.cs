﻿using System;
using System.Windows.Input;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Interaction logic for CtrlStatement.xaml
    /// </summary>
    public partial class CtrlStatement : CtrlEditorItem
    {
        public CtrlStatement()
        {
            InitializeComponent();
        }

        #region Order

        private void MoveUp_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            CodeItemResultStatement iContext = (CodeItemResultStatement)DataContext;
            CodeEditorPage iPage = iContext.Root as CodeEditorPage;
            e.CanExecute = iPage != null && iPage.CanMoveUpFor(iContext.Arguments);
        }

        private void MoveDown_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            CodeItemResultStatement iContext = (CodeItemResultStatement)DataContext;
            CodeEditorPage iPage = iContext.Root as CodeEditorPage;
            e.CanExecute = iPage != null && iPage.CanMoveDownFor(iContext.Arguments);
        }

        private void MoveUp_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeItemResultStatement iContext = (CodeItemResultStatement)DataContext;
            CodeEditorPage iPage = iContext.Root as CodeEditorPage;
            if (iPage != null)
                iPage.MoveUpFor(iContext.Arguments);
            else
                throw new NotSupportedException();
        }

        private void MoveDown_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeItemResultStatement iContext = (CodeItemResultStatement)DataContext;
            CodeEditorPage iPage = iContext.Root as CodeEditorPage;
            if (iPage != null)
                iPage.MoveDownFor(iContext.Arguments);
            else
                throw new NotSupportedException();
        }

        private void MoveToEnd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeItemResultStatement iContext = (CodeItemResultStatement)DataContext;
            CodeEditorPage iPage = iContext.Root as CodeEditorPage;
            if (iPage != null)
                iPage.MoveToEndFor(iContext.Arguments);
            else
                throw new NotSupportedException();
        }

        private void MoveToHome_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeItemResultStatement iContext = (CodeItemResultStatement)DataContext;
            CodeEditorPage iPage = iContext.Root as CodeEditorPage;
            if (iPage != null)
                iPage.MoveToStartFor(iContext.Arguments);
            else
                throw new NotSupportedException();
        }

        #endregion Order
    }
}