﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace NeuralNetworkDesigne.HAB.Designer
{
    public partial class CommonCodeItemsControls : ResourceDictionary
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommonCodeItemsControls"/> class.
        /// </summary>
        public CommonCodeItemsControls()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the MouseLeftButtonDown event of the CodeItemDropTargetGrid control.
        /// </summary>
        /// <remarks>
        /// We need to move and keep focus to the item that raised the mouse down event so that a paste works.  This needs a specific type of
        /// object to be focused.
        /// </remarks>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void CodeItemDropTargetGrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement iOrSource = sender as FrameworkElement;
            if (iOrSource != null)
            {
                ContentPresenter iPresenter = iOrSource.Tag as ContentPresenter;           //only try to capture the focus if there is no content.  Otherwise we leave the content item to capture it.Otherwise we can't see the debug data.
                if (iPresenter == null || iPresenter.Content == null)
                {
                    iOrSource.Focus();
                    e.Handled = true;                                                       //if we don't do this, the event goes up to the editor item, which will also try to get focus.
                }
            }
        }
    }
}