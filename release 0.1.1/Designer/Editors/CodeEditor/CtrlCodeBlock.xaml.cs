﻿using System;
using System.Windows.Input;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Interaction logic for CtrlCodeBlock.xaml
    /// </summary>
    public partial class CtrlCodeBlock : CtrlEditorItem
    {
        public CtrlCodeBlock()
        {
            InitializeComponent();
        }

        #region Order

        private void MoveUp_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            CodeItemCodeBlock iContext = (CodeItemCodeBlock)DataContext;
            if (iContext != null)
            {
                CodeEditorPage iPage = iContext.Root as CodeEditorPage;
                e.CanExecute = iPage != null && iPage.CanMoveUpFor(iContext.Children);
            }
        }

        private void MoveDown_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            CodeItemCodeBlock iContext = (CodeItemCodeBlock)DataContext;
            if (iContext != null)
            {
                CodeEditorPage iPage = iContext.Root as CodeEditorPage;
                e.CanExecute = iPage != null && iPage.CanMoveDownFor(iContext.Children);
            }
        }

        private void MoveUp_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeItemCodeBlock iContext = (CodeItemCodeBlock)DataContext;
            CodeEditorPage iPage = iContext.Root as CodeEditorPage;
            if (iPage != null)
                iPage.MoveUpFor(iContext.Children);
            else
                throw new NotSupportedException();
        }

        private void MoveDown_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeItemCodeBlock iContext = (CodeItemCodeBlock)DataContext;
            CodeEditorPage iPage = iContext.Root as CodeEditorPage;
            if (iPage != null)
                iPage.MoveDownFor(iContext.Children);
            else
                throw new NotSupportedException();
        }

        private void MoveToEnd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeItemCodeBlock iContext = (CodeItemCodeBlock)DataContext;
            CodeEditorPage iPage = iContext.Root as CodeEditorPage;
            if (iPage != null)
                iPage.MoveToEndFor(iContext.Children);
            else
                throw new NotSupportedException();
        }

        private void MoveToHome_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CodeItemCodeBlock iContext = (CodeItemCodeBlock)DataContext;
            CodeEditorPage iPage = iContext.Root as CodeEditorPage;
            if (iPage != null)
                iPage.MoveToStartFor(iContext.Children);
            else
                throw new NotSupportedException();
        }

        #endregion Order
    }
}