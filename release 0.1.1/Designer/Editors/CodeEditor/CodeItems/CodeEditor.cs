﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Encapsulates the 2 NeuronLists from a <see cref="Neuron"/> containing executable items for editing in a <see cref="CodeEditor"/>.
    /// If the item being encapsulated is a <see cref="NeuronCluster"/>, the children of the cluster are also depicted in a code view,
    /// if possible.
    /// </summary>
    public class CodeEditor : NeuronEditor
    {
        private ObservedCollection<CodeEditorPage> fEntryPoints;                               //rules, actions + possibly children goes in here
        private int fSelectedIndex;
        private bool? fShowInProject;
        private bool fIsLoaded;

        #region ctor -dtor

        /// <summary>
        /// Constructor with the item to wrap.
        /// </summary>
        /// <param name="toWrap"></param>
        public CodeEditor(Neuron toWrap) : base()
        {
            if (toWrap == null)
                throw new ArgumentNullException();
            fEntryPoints = new ObservedCollection<CodeEditorPage>(this);
            ItemID = toWrap.ID;

            if (NeuronInfo != null)                                                               //also build the name correctly, based on the display title of the item to wrap.
                Name = NamePrefix + ": " + NeuronInfo.DisplayTitle;
            else
                Name = NamePrefix + ":";
        }

        /// <summary>
        /// Default empty constructor, for streaming.
        /// </summary>
        public CodeEditor() : base()
        {
            fEntryPoints = new ObservedCollection<CodeEditorPage>(this);
        }

        #endregion ctor -dtor

        #region Prop

        #region Icon

        public override string Icon
        {
            get { return "/Images/ViewCode_Enabled.png"; }
        }

        #endregion Icon

        #region EntryPoints

        /// <summary>
        /// Gets all the entry points defined by the neuron that this object wraps.
        /// </summary>
        [XmlIgnore]
        public ObservedCollection<CodeEditorPage> EntryPoints
        {
            get { return fEntryPoints; }
        }

        #endregion EntryPoints

        /// <summary>
        /// Gets the text that should be added to the front of the name to display as default value.
        /// </summary>
        /// <value>The name prefix.</value>
        public override string NamePrefix
        {
            get { return "Code"; }
        }

        #region ItemID

        /// <summary>
        /// Gets/sets the Id of the <see cref="Neuron"/> we are encapsulating.
        /// </summary>
        /// <remarks>
        /// This is for streaming.
        /// </remarks>
        public override ulong ItemID
        {
            get
            {
                return base.ItemID;
            }
            set
            {
                if (value != ItemID)
                {
                    base.ItemID = value;
                    if (IsLoaded == true)
                        LoadEntryPoints();
                }
            }
        }

        #endregion ItemID

        #region ShowInProject

        /// <summary>
        /// Gets/sets wether this code editor is displayed as a shortcut in the project tree.
        /// </summary>
        public bool ShowInProject
        {
            get
            {
                if (fShowInProject.HasValue == false)
                    fShowInProject = BrainData.Current.Editors.AllCodeEditors().Contains(this);
                return fShowInProject.Value;
            }
            set
            {
                if (value != fShowInProject)
                {
                    fShowInProject = value;
                    if (value == true)
                        BrainData.Current.CurrentEditorsList.Add(this);
                    else
                        BrainData.Current.Editors.RemoveRecursive(this);
                    OnPropertyChanged("ShowInProject");
                }
            }
        }

        #endregion ShowInProject

        #region PossiblePages

        /// <summary>
        /// Gets a list of neuron data objects represening neurons that can still be used as meaning for a link from the currently
        /// displayed item and a new code cluster.
        /// </summary>
        /// <value>The possible pages.</value>
        public List<NeuronData> PossiblePages
        {
            get
            {
                List<Neuron> iShowing = (from i in EntryPoints select Brain.Current[i.Items.MeaningID]).ToList();
                var iFound = (from i in BrainData.Current.DefaultMeanings
                              where iShowing.Contains(i) == false
                              select BrainData.Current.NeuronInfo[i.ID]).ToList();
                return iFound;
            }
        }

        #endregion PossiblePages

        #region SelectedIndex

        /// <summary>
        /// Gets/sets the index of the page that is currently selected.
        /// </summary>
        /// <remarks>
        /// We store this so that this is remembered when the user switches screen.
        /// </remarks>
        public int SelectedIndex
        {
            get
            {
                return fSelectedIndex;
            }
            set
            {
                if (value != fSelectedIndex && value != -1)                                            //we don't want to loose the index value, so we skip -1, if we didn't do that, this prop would have no result.
                {
                    fSelectedIndex = value;
                    OnPropertyChanged("SelectedIndex");
                }
            }
        }

        #endregion SelectedIndex

        #region SelectedListType

        /// <summary>
        /// Gets or sets the type of the selected list. This allows you to change the currently selected tab,
        /// based on debug info.
        /// </summary>
        /// <value>The type of the selected list.</value>
        public ExecListType SelectedListType
        {
            get
            {
                if (EntryPoints[SelectedIndex].Items.MeaningID == (ulong)PredefinedNeurons.Rules)
                    return ExecListType.Rules;
                else if (EntryPoints[SelectedIndex].Items.MeaningID == (ulong)PredefinedNeurons.Actions)
                    return ExecListType.Actions;
                else
                    return ExecListType.Children;
            }
            set
            {
                ulong iToSearch;
                switch (value)
                {
                    case ExecListType.Rules:
                        iToSearch = (ulong)PredefinedNeurons.Rules;
                        break;

                    case ExecListType.Actions:
                        iToSearch = (ulong)PredefinedNeurons.Actions;
                        break;

                    case ExecListType.Children:
                        iToSearch = (ulong)PredefinedNeurons.Statements;
                        break;

                    default:
                        iToSearch = Neuron.EmptyId;
                        break;
                }
                if (iToSearch == Neuron.EmptyId)
                {
                    for (int i = 0; i < EntryPoints.Count; i++)
                    {
                        if (EntryPoints[i].Items.MeaningID == iToSearch)
                        {
                            SelectedIndex = i;
                            break;
                        }
                    }
                }
            }
        }

        #endregion SelectedListType

        #region IsLoaded

        /// <summary>
        /// Gets/sets the wether the contents of the neuron are loaded or not.  This is a speed optimisation, to
        /// make certain there aren't to many event handlers working.
        /// </summary>
        public bool IsLoaded
        {
            get
            {
                return fIsLoaded;
            }
            set
            {
                if (fIsLoaded != value)
                {
                    fIsLoaded = value;
                    if (value == true)
                        LoadEntryPoints();
                    else
                        ClearEntryPoints();
                    OnPropertyChanged("IsLoaded");
                }
            }
        }

        #endregion IsLoaded

        #endregion Prop

        #region functions

        /// <summary>
        /// Registers the item that was read from xml.
        /// </summary>
        /// <remarks>
        /// This must be called when the editor is read from xml.  In that situation, the
        /// brainData isn't always loaded properly yet.  At this point, this can be resolved.
        /// It is called by the brainData.
        /// </remarks>
        public override void RegisterItem()
        {
            base.RegisterItem();
            if (IsLoaded == true)
                LoadEntryPoints();
        }

        /// <summary>
        /// Clears all the entry points.
        /// </summary>
        private void ClearEntryPoints()
        {
            WindowMain.UndoStore.UndoStateStack.Push(NeuralNetworkDesigne.UndoSystem.UndoState.Blocked);              //while clearing the entrypoints, we don't want to generate undo data.
            try
            {
                fEntryPoints.Clear();
            }
            finally
            {
                WindowMain.UndoStore.UndoStateStack.Pop();
            }
        }

        /// <summary>
        /// Loads the CodeEditorPages for the current item.
        /// </summary>
        private void LoadEntryPoints()
        {
            WindowMain.UndoStore.UndoStateStack.Push(NeuralNetworkDesigne.UndoSystem.UndoState.Blocked);              //while building the entrypoints, we don't want to generate undo data.
            try
            {
                ClearEntryPoints();
                if (Item != null)
                {
                    NeuronCluster iCluster;

                    if (Item is NeuronCluster)
                        fEntryPoints.Add(new CodeEditorPage("Children (this list)", Item, Neuron.EmptyId, (NeuronCluster)Item));
                    else if (Item is ExpressionsBlock)
                    {
                        iCluster = ((ExpressionsBlock)Item).StatementsCluster;
                        if (iCluster != null)
                            fEntryPoints.Add(new CodeEditorPage("Statements", Item, (ulong)PredefinedNeurons.Statements, iCluster));
                        else
                            fEntryPoints.Add(new CodeEditorPage("Statements", Item, (ulong)PredefinedNeurons.Statements));
                    }
                    else if (Item is TimerSin)
                    {
                        iCluster = ((TimerSin)Item).StatementsCluster;
                        if (iCluster != null)
                            fEntryPoints.Add(new CodeEditorPage("Statements", Item, (ulong)PredefinedNeurons.Statements, iCluster));
                        else
                            fEntryPoints.Add(new CodeEditorPage("Statements", Item, (ulong)PredefinedNeurons.Statements));
                    }
                    AddSinCode();
                    AddRulesPage();
                    AddActionsPage();
                    AddCodePages();
                }
            }
            finally
            {
                WindowMain.UndoStore.UndoStateStack.Pop();
            }
        }

        /// <summary>
        /// Adds all the code clusters that have not yet been loaded by the predefined items.
        /// </summary>
        private void AddCodePages()
        {
            using (LinksAccessor iLinks = Item.LinksOut)
            {
                foreach (Link i in iLinks.Items)
                {
                    NeuronCluster iTo = i.To as NeuronCluster;
                    if (iTo != null && iTo.Meaning == (ulong)PredefinedNeurons.Code)
                    {
                        var iFound = (from iPage in EntryPoints where iPage.Items.Cluster == iTo select iPage).FirstOrDefault();
                        if (iFound == null)
                            EntryPoints.Add(new CodeEditorPage(BrainData.Current.NeuronInfo[i.MeaningID].DisplayTitle, Item, i.MeaningID, iTo));
                    }
                }
            }
        }

        /// <summary>
        /// Checks if the wrapeed neuron is a sin, if so, adds a page for input actions.
        /// </summary>
        private void AddSinCode()
        {
            if (Item is Sin)
            {
                NeuronCluster iCluster = ((Sin)Item).ActionsForInput;
                if (iCluster != null)
                    fEntryPoints.Add(new CodeEditorPage("Input actions", Item, (ulong)PredefinedNeurons.ActionsForInput, iCluster));
                else
                    fEntryPoints.Add(new CodeEditorPage("Input actions", Item, (ulong)PredefinedNeurons.ActionsForInput));
            }
        }

        /// <summary>
        /// Adds the rules page.
        /// </summary>
        private void AddRulesPage()
        {
            NeuronCluster iCluster = Item.RulesCluster;
            if (iCluster == null)
                fEntryPoints.Add(new CodeEditorPage("Rules", Item, (ulong)PredefinedNeurons.Rules));
            else
                fEntryPoints.Add(new CodeEditorPage("Rules", Item, (ulong)PredefinedNeurons.Rules, iCluster));
        }

        /// <summary>
        /// Adds the actions page.
        /// </summary>
        private void AddActionsPage()
        {
            NeuronCluster iCluster = Item.ActionsCluster;
            if (iCluster == null)
                fEntryPoints.Add(new CodeEditorPage("Actions", Item, (ulong)PredefinedNeurons.Actions));
            else
                fEntryPoints.Add(new CodeEditorPage("Actions", Item, (ulong)PredefinedNeurons.Actions, iCluster));
        }

        /// <summary>
        /// Asks each page on this code editor to remove all their registered variables (toolbox items).
        /// </summary>
        public void ParkRegisteredVariables()
        {
            foreach (CodeEditorPage i in EntryPoints)
                i.ParkVariables();
        }

        public void UnParkRegisteredVariables()
        {
            foreach (CodeEditorPage i in EntryPoints)
                i.UnParkVariables();
        }

        #endregion functions

        #region IWeakEventListener Members

        protected override void Item_NeuronChanged(NeuronChangedEventArgs e)
        {
            base.Item_NeuronChanged(e);
            if (e.Action == BrainAction.Removed)
                BrainData.Current.CodeEditors.Remove(this);
        }

        #endregion IWeakEventListener Members

        #region Clipboard

        protected override void CopyToClipboard(DataObject data)
        {
            List<ulong> iValues = (from i in EntryPoints[SelectedIndex].SelectedItems
                                   select i.Item.ID).ToList();

            data.SetData(Properties.Resources.MultiNeuronIDFormat, iValues);
        }

        public override bool CanCopyToClipboard()
        {
            if (SelectedIndex > -1)
                return EntryPoints[SelectedIndex].SelectedItems.Count > 0;
            else
                return false;
        }

        public override bool CanPasteSpecialFromClipboard()
        {
            return false;
        }

        public override void PasteSpecialFromClipboard()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Determines whether this instance can paste from the clipboard].
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if this instance can paste from the clipboard; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanPasteFromClipboard()
        {
            if (base.CanPasteFromClipboard() == true)
            {
                if (SelectedIndex > -1)
                    return Clipboard.ContainsData(Properties.Resources.MultiNeuronIDFormat) || Clipboard.ContainsData(Properties.Resources.NeuronIDFormat);
                else
                    return false;
            }
            return false;
        }

        public override void PasteFromClipboard()
        {
            FrameworkElement iFocused = Keyboard.FocusedElement as FrameworkElement;
            object iTarget = null;
            if (iFocused != null)
                iTarget = iFocused.Tag;
            if (iTarget is ContentPresenter)                                                         //we paste on a drop target for property values.
                DoPropetyPaste((ContentPresenter)iTarget);
            else if (iTarget is ItemsControl)                                                      //we paste on an argument list or other form of list.
                DoArgPaste(((ItemsControl)iTarget).ItemsSource as CodeItemCollection);
            else
                DoCodePaste();                                                                      //we dropped on something that requires on insert or add
        }

        /// <summary>
        /// Does a paste on a property place holder.  This paste will only handle the 1st item
        /// on the clipboard.
        /// </summary>
        /// <param name="pasteOn">The paste on.</param>
        private void DoPropetyPaste(ContentPresenter pasteOn)
        {
            List<ulong> iItems = GetDataAsListFromClipboard();
            if (iItems != null && iItems.Count > 0)
            {
                WindowMain.UndoStore.BeginUndoGroup(false);
                try
                {
                    Neuron iNeuron = Brain.Current[iItems[0]];
                    CodeItem iNew = EditorsHelper.CreateCodeItemFor(iNeuron);
                    pasteOn.Content = iNew;
                }
                finally
                {
                    WindowMain.UndoStore.EndUndoGroup();
                }
            }
        }

        private void DoArgPaste(CodeItemCollection col)
        {
            List<ulong> iItems = GetDataAsListFromClipboard();
            if (iItems != null)
            {
                WindowMain.UndoStore.BeginUndoGroup(false);
                try
                {
                    foreach (ulong i in iItems)
                    {
                        Neuron iNeuron = Brain.Current[i];
                        CodeItem iNew = EditorsHelper.CreateCodeItemFor(iNeuron);
                        col.Add(iNew);
                    }
                }
                finally
                {
                    WindowMain.UndoStore.EndUndoGroup();
                }
            }
        }

        private void DoCodePaste()
        {
            List<ulong> iItems = GetDataAsListFromClipboard();
            if (iItems != null)
            {
                CodeItemCollection iCol = null;
                int iIndex = -1;
                CodeItem iPasteOn = EntryPoints[SelectedIndex].SelectedItem;
                if (iPasteOn != null)
                {
                    ICodeItemsOwner iOwner = iPasteOn.Owner as ICodeItemsOwner;
                    while (iOwner != null)
                    {
                        iCol = iOwner.Items;
                        iIndex = iCol.IndexOf(iPasteOn);
                        if (iIndex != -1)                                                                   //if the owner has children, but also other items, than we have a problem, should not be possible, but just in case, to have a working algorithm, we go up 1 owner.
                            break;
                        else
                            iPasteOn = iPasteOn.Owner as CodeItem;
                    }
                }
                if (iIndex == -1)
                    iCol = EntryPoints[SelectedIndex].Items;
                WindowMain.UndoStore.BeginUndoGroup(false);
                try
                {
                    foreach (ulong i in iItems)
                    {
                        Neuron iNeuron = Brain.Current[i];
                        CodeItem iNew = EditorsHelper.CreateCodeItemFor(iNeuron);
                        if (iIndex == -1)
                            iCol.Add(iNew);
                        else
                        {
                            iCol.Insert(iIndex, iNew);
                            iIndex++;                                                                           //need to add the next item after the currently added one.
                        }
                    }
                }
                finally
                {
                    WindowMain.UndoStore.EndUndoGroup();
                }
            }
        }

        private List<ulong> GetDataAsListFromClipboard()
        {
            List<ulong> iItems = null;
            if (Clipboard.ContainsData(Properties.Resources.MultiNeuronIDFormat) == true)
                iItems = Clipboard.GetData(Properties.Resources.MultiNeuronIDFormat) as List<ulong>;
            else
            {
                iItems = new List<ulong>();
                iItems.Add((ulong)Clipboard.GetData(Properties.Resources.NeuronIDFormat));
            }
            return iItems;
        }

        #endregion Clipboard

        #region Delete

        /// <summary>
        /// Deletes all the selected items on this editor.
        /// </summary>
        public override void Delete()
        {
            MessageBoxResult iRes = MessageBox.Show("Remove the selected neurons from the designer and from the brain when no longer referenced?", "Delete items", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (iRes == MessageBoxResult.OK)
            {
                CodeEditorPage iPage = EntryPoints[SelectedIndex];
                Debug.Assert(iPage != null);

                List<CodeItem> iToRemove = iPage.SelectedItems.ToList();                                        //we make a copy just in case we don't brake any loop code.
                iToRemove.Reverse();                                                                            //we reverse the list so that we remove the last code item first.  This allows for better consistency in editing.
                WindowMain.UndoStore.BeginUndoGroup(false);                                                          //we group all the data together so a single undo command cand restore the previous state.
                try
                {
                    foreach (CodeItem i in iToRemove)
                        DeleteOrRemoveCodeItem(i, DeletionMethod.DeleteIfNoRef);
                }
                finally
                {
                    WindowMain.UndoStore.EndUndoGroup();
                }
            }
        }

        /// <summary>
        /// Checks if a delete can be performed on this editor.
        /// </summary>
        /// <returns></returns>
        public override bool CanDelete()
        {
            if (SelectedIndex < EntryPoints.Count)
            {
                CodeEditorPage iPage = EntryPoints[SelectedIndex];
                Debug.Assert(iPage != null);
                return iPage.SelectedItems.Count > 0;
            }
            return false;
        }

        /// <summary>
        /// Deletes all the selected items on this editor after the user has selected extra deletion options.
        /// </summary>
        public override void DeleteSpecial()
        {
            DlgSelectDeletionMethod iDlg = new DlgSelectDeletionMethod();
            iDlg.Owner = App.Current.MainWindow;
            bool? iDlgRes = iDlg.ShowDialog();
            if (iDlgRes.HasValue == true && iDlgRes.Value == true)
            {
                CodeEditorPage iPage = EntryPoints[SelectedIndex];
                Debug.Assert(iPage != null);
                List<CodeItem> iToRemove = iPage.SelectedItems.ToList();                                        //we make a copy just in case we don't brake any loop code.
                WindowMain.UndoStore.BeginUndoGroup(false);                                                          //we group all the data together so a single undo command cand restore the previous state.
                try
                {
                    foreach (CodeItem i in iToRemove)
                        HandleNeuronForDeleteSpecial(i, iDlg.NeuronDeletionMethod, iDlg.BranchHandling);
                }
                finally
                {
                    WindowMain.UndoStore.EndUndoGroup();
                }
            }
        }

        private void HandleNeuronForDeleteSpecial(CodeItem toRemove, DeletionMethod deletionMethod, DeletionMethod branchHandling)
        {
            switch (deletionMethod)
            {
                case DeletionMethod.Remove:
                    RemoveCodeItem(toRemove);
                    break;

                case DeletionMethod.DeleteIfNoRef:
                    DeleteOrRemoveCodeItem(toRemove, branchHandling);
                    break;

                case DeletionMethod.Delete:
                    if (toRemove.Item.CanBeDeleted == true)
                    {
                        bool iDelete;
                        using (LinksAccessor iLinksIn = toRemove.Item.LinksIn)
                        {
                            using (NeuronsAccessor iClusteredBy = toRemove.Item.ClusteredBy)
                                iDelete = (iLinksIn.Items.Count == 1 && iClusteredBy.Items.Count == 0)
                                         || (iLinksIn.Items.Count == 0 && iClusteredBy.Items.Count == 1);
                        }
                        if (iDelete == true)
                            EditorsHelper.ProcessBranch(toRemove.Item, branchHandling);
                        WindowMain.DeleteItemFromBrain(toRemove.Item);
                    }
                    else
                        MessageBox.Show(string.Format("Neuron {0} can't be deleted because it is used as a meaning or info.", toRemove), "Delete", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;

                default:
                    throw new InvalidOperationException();
            }
        }

        private void RemoveCodeItem(CodeItem toRemove)
        {
            CodeEditorPage iPage = EntryPoints[SelectedIndex];
            Debug.Assert(iPage != null);

            if (toRemove.Owner == iPage)
            {
                if (iPage.Items.Remove(toRemove) == false)
                    MessageBox.Show("Failed to remove the item from the main code list!");
            }
            else if (toRemove.Owner is CodeItem)
                ((CodeItem)toRemove.Owner).RemoveChildFromCode(toRemove);
            else
                MessageBox.Show("Unkown owner type, can't Remove!");
        }

        private void DeleteOrRemoveCodeItem(CodeItem toRemove, DeletionMethod branchHandling)
        {
            CodeEditorPage iPage = EntryPoints[SelectedIndex];
            Debug.Assert(iPage != null);

            if (toRemove.Owner == iPage)
                EditorsHelper.RemoveOrDeleteFromCluster<CodeItem>(toRemove, iPage.Items, branchHandling);
            else if (toRemove.Owner is CodeItem)
                ((CodeItem)toRemove.Owner).DeleteOrRemoveChild(toRemove, branchHandling);
            else
                MessageBox.Show("Unkown owner type, can't Delete!");
        }

        /// <summary>
        /// Determines whether a delete special can be performed
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if this instance can do a special delete; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanDeleteSpecial()
        {
            return CanDelete();
        }

        #endregion Delete
    }
}