﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// A single page for a code editor.  A page contains the code listing of 1 cluster (rules, actions, children,..)
    /// </summary>
    public class CodeEditorPage : OwnedObject, INeuronWrapper, IEditorSelection, ICodeItemsOwner
    {
        #region fields

        private ulong fLinkMeaning;
        private string fTitle;
        private CodeItemCollection fItems;
        private Neuron fRoot;
        private EditorItemSelectionList<CodeItem> fSelectedItems = new EditorItemSelectionList<CodeItem>();
        private ObservableCollection<Variable> fRegisteredVariables = new ObservableCollection<Variable>();
        private List<Variable> fParkedVariables;                                                                   //required for backing up the registeredVariables during a save procedure.

        #endregion fields

        #region ctor

        /// <summary>
        /// Constructor for when only the id is known of the cluster containing all the code.
        /// </summary>
        /// <param name="title">The title of the page.</param>
        /// <param name="root">The neuron that owns the cluster with the code (required for possibly creating the link between the neuron and the code.</param>
        /// <param name="clusterId">The id of the cluster to use.</param>
        public CodeEditorPage(string title, Neuron root, ulong linkId)
        {
            InternalCreate(title, root, linkId);
            Items = new CodeItemCollection(this, linkId);
        }

        /// <summary>
        /// Constructor for when the cluster is already created.
        /// </summary>
        /// <param name="title">The title of the page.</param>
        /// <param name="root">The neuron that owns the cluster with the code (required for possibly creating the link between the neuron and the code.</param>
        /// <param name="clusterId">The cluster to use.</param>
        public CodeEditorPage(string title, Neuron root, ulong linkId, NeuronCluster cluster)
        {
            InternalCreate(title, root, linkId);
            Items = new CodeItemCollection(this, cluster);
        }

        /// <summary>
        /// Internally creates the instance
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="root">The root.</param>
        private void InternalCreate(string title, Neuron root, ulong linkId)
        {
            Title = title;
            fRoot = root;
            fLinkMeaning = linkId;
        }

        #endregion ctor

        #region Prop

        #region Title

        /// <summary>
        /// Gets the title of this page.
        /// </summary>
        public string Title
        {
            get { return fTitle; }
            internal set { fTitle = value; }
        }

        #endregion Title

        #region Items

        /// <summary>
        /// Gets the list of code items for this page.
        /// </summary>
        public CodeItemCollection Items
        {
            get { return fItems; }
            internal set { fItems = value; }
        }

        #endregion Items

        #region LinkMeaning

        /// <summary>
        /// Gets the id of the neuron assigned to the meaning of the link between the code cluster,wrapped by this page, and the neuron that links to it.
        /// </summary>
        public ulong LinkMeaning
        {
            get { return fLinkMeaning; }
            internal set { fLinkMeaning = value; }
        }

        #endregion LinkMeaning

        #region SelectedItems

        /// <summary>
        /// gets the list with all the selected items.
        /// </summary>
        public IList<CodeItem> SelectedItems
        {
            get
            {
                return fSelectedItems;
            }
        }

        #endregion SelectedItems

        #region SelectedItem

        /// <summary>
        /// gets the first selected item.
        /// </summary>
        /// <remarks>
        /// Used by the ContextMenu of the CodeEditorPage to find the codeItem's IsBreakPoint property (and possibly others).
        /// </remarks>
        public CodeItem SelectedItem
        {
            get
            {
                if (fSelectedItems.Count > 0)
                    return fSelectedItems[0];
                else
                    return null;
            }
        }

        #endregion SelectedItem

        #region RegisteredVariables

        /// <summary>
        /// Gets the list of all the variables that have been used in this neural function.
        /// </summary>
        /// <remarks>
        /// this list gets populated through the <see cref="CodeItemVariable"/>'s which add/remove their neuron
        /// during construction/destruction.  This list is than monitored for changes so we can update the
        /// brainData's list of toolbox items.
        /// </remarks>
        public ObservableCollection<Variable> RegisteredVariables
        {
            get { return fRegisteredVariables; }
        }

        #endregion RegisteredVariables

        #region INeuronWrapper Members

        /// <summary>
        /// This property actually returns the owner of this cluster (the neuron for which this list is a rules or actions list).
        /// </summary>
        public Neuron Item
        {
            get { return fRoot; }
        }

        #endregion INeuronWrapper Members

        #region IEditorSelection Members

        /// <summary>
        /// Gets the list of selected items.
        /// </summary>
        /// <value>The selected items.</value>
        IList IEditorSelection.SelectedItems
        {
            get { return fSelectedItems; }
        }

        /// <summary>
        /// Gets/sets the currently selected item. If there are multiple selections, the first is returned.
        /// </summary>
        /// <value></value>
        object IEditorSelection.SelectedItem
        {
            get
            {
                return SelectedItem;
            }
        }

        #endregion IEditorSelection Members

        #endregion Prop

        #region Functions

        /// <summary>
        /// Temporarely removes all the registered variables so that the corresponding toolbox items are also removed from the BrainData.
        /// To put the variables back, use <see cref="CodeEditorPAge.UnParkVariables"/>.
        /// </summary>
        public void ParkVariables()
        {
            fParkedVariables = new List<Variable>(fRegisteredVariables);
            fRegisteredVariables.Clear();
        }

        public void UnParkVariables()
        {
            foreach (Variable i in fParkedVariables)
                fRegisteredVariables.Add(i);
            fParkedVariables = null;
        }

        #region Order

        /// <summary>
        /// Checks if the MoveDown command can be executed on the currently selected items if all items
        /// are supposed to be in the specified list.
        /// </summary>
        /// <param name="list">The list from which all the selected items should come from if a move is allowed.</param>
        /// <returns>True if there are selected items and all come from the specified list.</returns>
        internal bool CanMoveDownFor(CodeItemCollection list)
        {
            if (SelectedItems.Count > 0)
            {
                foreach (CodeItem i in SelectedItems)
                {
                    int iIndex = list.IndexOf(i);
                    if (iIndex == -1 || iIndex == list.Count)
                        return false;
                }
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Checks if the MoveUp command can be executed on the currently selected items if all items
        /// are supposed to be in the specified list.
        /// </summary>
        /// <param name="list">The list from which all the selected items should come from if a move is allowed.</param>
        /// <returns>True if there are selected items and all come from the specified list.</returns>
        internal bool CanMoveUpFor(CodeItemCollection list)
        {
            if (SelectedItems.Count > 0)
            {
                foreach (CodeItem i in SelectedItems)
                {
                    int iIndex = list.IndexOf(i);
                    if (iIndex == -1 || iIndex == 0)
                        return false;
                }
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Moves the currently selected items 1 up in the specified list.
        /// </summary>
        /// <param name="list">The list that should own all the currently sected items (not checked,
        /// use <see cref="CanMoveUpFor"/> for that</param>
        internal void MoveUpFor(CodeItemCollection list)
        {
            List<CodeItem> iToMove = (from i in SelectedItems select i).ToList();               //need to make a copy of the list so we can modify it.
            foreach (CodeItem i in iToMove)
            {
                int iIndex = list.IndexOf(i);
                list.Move(iIndex, iIndex - 1);                                           //move up in the designer is move to the front of the list.
            }
        }

        internal void MoveDownFor(CodeItemCollection list)
        {
            List<CodeItem> iToMove = (from i in SelectedItems select i).ToList();      //need to make a copy of the list so we can modify it.
            foreach (CodeItem i in iToMove)
            {
                int iIndex = list.IndexOf(i);
                list.Move(iIndex, iIndex + 1);                                           //move up in the designer is move to the front of the list.
            }
        }

        #endregion Order

        internal void MoveToEndFor(CodeItemCollection list)
        {
            List<CodeItem> iToMove = (from i in SelectedItems select i).ToList();      //need to make a copy of the list so we can modify it.
            int iCount = list.Count - 1;
            foreach (CodeItem i in iToMove)
            {
                int iIndex = list.IndexOf(i);
                list.Move(iIndex, iCount);
                iCount--;
            }
        }

        internal void MoveToStartFor(CodeItemCollection list)
        {
            List<CodeItem> iToMove = (from i in SelectedItems select i).ToList();      //need to make a copy of the list so we can modify it.
            int iCount = 0;
            foreach (CodeItem i in iToMove)
            {
                int iIndex = list.IndexOf(i);
                list.Move(iIndex, iCount);
                iCount++;
            }
        }

        #endregion Functions

        /// <summary>
        /// Makes all the items that represent the specified neuron on this page selected
        /// </summary>
        /// <param name="neuron">The neuron.</param>
        public void Select(Neuron neuron)
        {
            SelectedItems.Clear();
            foreach (CodeItem i in Items)
                i.Select(neuron);
        }
    }
}