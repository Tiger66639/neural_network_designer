﻿using System.Windows;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Represents a code item in a <see cref="CodeList"/>.
    /// </summary>
    public class CodeItem : EditorItem
    {
        #region fields

        private bool fIsBreakPoint;
        private bool fIsNextStatement = false;

        #endregion fields

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="CodeItem"/> class.
        /// </summary>
        /// <param name="toWrap">The item to wrap.</param>
        public CodeItem(Neuron toWrap) : base(toWrap)
        {
        }

        /// <summary>
        /// For static code item
        /// </summary>
        public CodeItem() : base()
        {
        }

        #endregion ctor

        #region prop

        #region IsBreakPoint

        /// <summary>
        /// Gets/sets if the processor should pause on this line.
        /// </summary>
        public bool IsBreakPoint
        {
            get
            {
                return fIsBreakPoint;
            }
            set
            {
                Expression iItem = Item as Expression;
                if (iItem != null)                                                                           //we could wrap a static neuron.
                {
                    OnPropertyChanging("IsBreakPoint", fIsBreakPoint, value);
                    fIsBreakPoint = value;
                    if (value == true)
                        BrainData.Current.BreakPoints.Add(iItem);
                    else
                        BrainData.Current.BreakPoints.Remove(iItem);
                    OnPropertyChanged("IsBreakPoint");
                }
            }
        }

        #endregion IsBreakPoint

        #region IsNextStatement

        /// <summary>
        /// Gets/sets wether this is the next statement to be executed or not.
        /// </summary>
        public bool IsNextStatement
        {
            get
            {
                return fIsNextStatement;
            }
            internal set
            {
                fIsNextStatement = value;
                OnPropertyChanged("IsNextStatement");                                          //this should be thread save cause property changed works accross threads.
            }
        }

        #endregion IsNextStatement

        #endregion prop

        /// <summary>
        /// Called when the <see cref="CodeItem.Item"/> has changed.
        /// </summary>
        /// <param name="value">The value.</param>
        protected override void OnItemChanged(Neuron value)
        {
            if (value is Expression)
            {
                if (BrainData.Current.BreakPoints.Contains((Expression)value) == true)
                    IsBreakPoint = true;
            }
            base.OnItemChanged(value);
        }

        /// <summary>
        /// Check if this item or any of it's children wraps the specified neuron, if so, the item is made selected.
        /// </summary>
        /// <param name="neuron">The neuron.</param>
        public virtual void Select(Neuron neuron)
        {
            if (Item == neuron)
            {
                IEditorSelection iRoot = Root;
                if (iRoot != null)
                {
                    if (iRoot.SelectedItems.Count < 3)
                    {
                        iRoot.SelectedItems.Add(this);                                                             //we use this type of adding to make certain we can add multiple items.
                        Visualize();
                    }
                    else
                        MessageBox.Show("Only the first 3 items have been visualized to prevent stack overflow, more are present in this page through.");
                }
            }
        }

        /// <summary>
        /// Makes certain that this item is visually displayed, so non of it's owner's are collapsed (through
        /// <see cref="ExpandableCodeItem"/>s.
        /// </summary>
        public void Visualize()
        {
            CodeItem iOwner = Owner as CodeItem;
            while (iOwner != null)
            {
                ExpandableCodeItem iExpandable = iOwner as ExpandableCodeItem;
                if (iExpandable != null)
                    iExpandable.IsExpanded = true;
                iOwner = iOwner.Owner as CodeItem;
            }
        }
    }
}