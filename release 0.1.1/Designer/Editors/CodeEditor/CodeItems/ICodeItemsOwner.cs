﻿namespace NeuralNetworkDesigne.HAB.Designer
{
    public interface ICodeItemsOwner
    {
        CodeItemCollection Items { get; }
    }
}