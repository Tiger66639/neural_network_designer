﻿using System;
using System.Windows.Threading;

namespace NeuralNetworkDesigne.HAB.Designer
{
    public class CodeItemAssignment : CodeItem
    {
        #region fields

        private CodeItemResult fLeftPart;
        private CodeItemResult fRightPart;

        #endregion fields

        #region ctor - dtor

        /// <summary>
        /// Initializes a new instance of the <see cref="CodeItemAssignment"/> class.
        /// </summary>
        /// <param name="toWrap">To wrap.</param>
        public CodeItemAssignment(Assignment toWrap) : base(toWrap)
        {
        }

        #endregion ctor - dtor

        #region prop

        #region NotHasLeftPart

        /// <summary>
        /// Gets the if the loopitem is present or not.
        /// </summary>
        public bool NotHasLeftPart
        {
            get { return ((Assignment)Item).LeftPart == null; }
        }

        #endregion NotHasLeftPart

        #region LeftPart

        /// <summary>
        /// Gets/sets the left part of the boolean expression.
        /// </summary>
        public CodeItemResult LeftPart
        {
            get
            {
                return fLeftPart;
            }
            set
            {
                if (fLeftPart != value)
                {
                    InternalSetLeftPart(value);
                    InternalChange = true;
                    try
                    {
                        SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.LeftPart, value);
                    }
                    finally
                    {
                        InternalChange = false;
                    }
                }
            }
        }

        /// <summary>
        /// only stores the new value, warns the undo system + the UI
        /// </summary>
        /// <param name="value">The new value.</param>
        private void InternalSetLeftPart(CodeItemResult value)
        {
            if (fLeftPart != null)
                UnRegisterChild(fLeftPart);
            fLeftPart = value;
            if (fLeftPart != null)
                RegisterChild(fLeftPart);
            OnPropertyChanged("LeftPart");
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<string>(OnPropertyChanged), "NotHasLeftPart");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event.
        }

        #endregion LeftPart

        #region NotHasRightPart

        /// <summary>
        /// Gets the if the loopitem is present or not.
        /// </summary>
        public bool NotHasRightPart
        {
            get { return ((Assignment)Item).RightPart == null; }
        }

        #endregion NotHasRightPart

        #region RightPart

        /// <summary>
        /// Gets/sets the right part of the bool expression
        /// </summary>
        public CodeItemResult RightPart
        {
            get
            {
                return fRightPart;
            }
            set
            {
                if (fRightPart != value)
                {
                    InternalSetRightPart(value);
                    InternalChange = true;
                    try
                    {
                        SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.RightPart, value);
                    }
                    finally
                    {
                        InternalChange = false;
                    }
                }
            }
        }

        private void InternalSetRightPart(CodeItemResult value)
        {
            if (fRightPart != null)
                UnRegisterChild(fRightPart);
            fRightPart = value;
            if (fRightPart != null)
                RegisterChild(fRightPart);
            OnPropertyChanged("RightPart");
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<string>(OnPropertyChanged), "NotHasRightPart");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event.
        }

        #endregion RightPart

        #endregion prop

        #region Functions

        /// <summary>
        /// Check if this item or any of it's children wraps the specified neuron, if so, the item is made selected.
        /// </summary>
        /// <param name="neuron">The neuron.</param>
        public override void Select(Neuron neuron)
        {
            base.Select(neuron);
            CodeItem iItem = RightPart;
            if (iItem != null)
                iItem.Select(neuron);

            iItem = LeftPart;
            if (iItem != null)
                iItem.Select(neuron);
        }

        /// <summary>
        /// Called when the <see cref="CodeItem.Item"/> has changed.
        /// </summary>
        /// <param name="value">The value.</param>
        protected override void OnItemChanged(Neuron value)
        {
            base.OnItemChanged(value);
            Assignment iToWrap = value as Assignment;
            if (iToWrap != null)
            {
                Neuron iFound = iToWrap.LeftPart;
                if (iFound != null)
                    InternalSetLeftPart((CodeItemResult)EditorsHelper.CreateCodeItemFor(iFound));
                iFound = iToWrap.RightPart;
                if (iFound != null)
                    InternalSetRightPart((CodeItemResult)EditorsHelper.CreateCodeItemFor(iFound));
            }
        }

        /// <summary>
        /// descendents that need to update links that changed can do this through this function.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void InternalLinkChanged(Link sender, LinkChangedEventArgs e)
        {
            base.InternalLinkChanged(sender, e);
            if (sender.MeaningID == (ulong)PredefinedNeurons.LeftPart)
                if (e.Action == BrainAction.Removed)
                    InternalSetLeftPart(null);
                else
                    InternalSetLeftPart((CodeItemResult)EditorsHelper.CreateCodeItemFor(Brain.Current[e.NewTo]));
            else if (sender.MeaningID == (ulong)PredefinedNeurons.RightPart)
                if (e.Action == BrainAction.Removed)
                    InternalSetRightPart(null);
                else
                    InternalSetRightPart((CodeItemResult)EditorsHelper.CreateCodeItemFor(Brain.Current[e.NewTo]));
        }

        /// <summary>
        /// Removes the current code item from the code list, but not the actual neuron that represents the code
        /// item, this stays in the brain, it is simply no longer used in this code list.
        /// </summary>
        /// <param name="child"></param>
        public override void RemoveChildFromCode(EditorItem child)
        {
            if (LeftPart == child)
                LeftPart = null;
            else if (RightPart == child)
                RightPart = null;
            else
                base.RemoveChildFromCode(child);
        }

        /// <summary>
        /// Deletes the specified item.  When the item doesn't have any other incomming links or parent clusters, it is also
        /// removed from the brain.
        /// </summary>
        /// <param name="child">The child.</param>
        public override void DeleteOrRemoveChild(EditorItem child, DeletionMethod branchHandling)
        {
            if (LeftPart == child)
            {
                LeftPart = null;
                EditorsHelper.DeleteWhenNotUsed(child.Item, branchHandling);
            }
            else if (RightPart == child)
            {
                RightPart = null;
                EditorsHelper.DeleteWhenNotUsed(child.Item, branchHandling);
            }
            else
                base.DeleteOrRemoveChild(child, branchHandling);
        }

        #endregion Functions
    }
}