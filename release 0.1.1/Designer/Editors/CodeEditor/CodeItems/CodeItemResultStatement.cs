﻿namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// A wrapper for result statent neurons.
    /// </summary>
    public class CodeItemResultStatement : CodeItemResult, ICodeItemsOwner
    {
        private CodeItemCollection fArguments;

        #region ctor-dtor

        /// <summary>
        /// Initializes a new instance of the <see cref="CodeItemResultStatement"/> class.
        /// </summary>
        /// <param name="toWrap">To wrap.</param>
        public CodeItemResultStatement(ResultStatement toWrap)
           : base(toWrap)
        {
        }

        #endregion ctor-dtor

        #region Instruction

        /// <summary>
        /// Gets/sets the instruction to use.
        /// </summary>
        /// <remarks>
        /// Wrapper for <see cref="Statement.Instruction"/> to provide undo info and ui updating.
        /// </remarks>
        public ResultInstruction Instruction
        {
            get
            {
                return ((ResultStatement)Item).Instruction;
            }
            set
            {
                OnPropertyChanging("Instruction", ((ResultStatement)Item).Instruction, value);
                ((ResultStatement)Item).Instruction = value;
                //PropertyChanged event is raised by the Link_Changed event handler, making certain that all updates are correctly done.
            }
        }

        #endregion Instruction

        #region Arguments

        /// <summary>
        /// Gets the list of arguments for this statement.
        /// </summary>
        public CodeItemCollection Arguments
        {
            get { return fArguments; }
            internal set
            {
                if (fArguments != value)
                {
                    fArguments = value;
                    OnPropertyChanged("Arguments");
                }
            }
        }

        #endregion Arguments

        #region Functions

        /// <summary>
        /// Called when the <see cref="CodeItem.Item"/> has changed.
        /// </summary>
        /// <param name="value">The value.</param>
        protected override void OnItemChanged(Neuron value)
        {
            base.OnItemChanged(value);
            fArguments = null;
            LoadChildren();
        }

        /// <summary>
        /// descendents that need to update links that changed can do this through this function.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void InternalLinkChanged(Link sender, LinkChangedEventArgs e)
        {
            base.InternalLinkChanged(sender, e);
            if (sender.MeaningID == (ulong)PredefinedNeurons.Instruction)
                OnPropertyChanged("Instruction");
            else if (sender.MeaningID == (ulong)PredefinedNeurons.Arguments)
                LoadChildren();
        }

        private void LoadChildren()
        {
            NeuronCluster iArgCluster = ((ResultStatement)Item).ArgumentsCluster;
            if (fArguments == null || iArgCluster != fArguments.Cluster)
            {
                if (iArgCluster != null)
                    Arguments = new CodeItemCollection(this, iArgCluster);
                else
                    Arguments = new CodeItemCollection(this, (ulong)PredefinedNeurons.Arguments);
            }
        }

        /// <summary>
        /// Removes the current code item from the code list, but not the actual neuron that represents the code
        /// item, this stays in the brain, it is simply no longer used in this code list.
        /// </summary>
        /// <param name="child"></param>
        public override void RemoveChildFromCode(EditorItem child)
        {
            CodeItem iChild = (CodeItem)child;
            if (Arguments.Remove(iChild) == false)
                base.RemoveChildFromCode(child);
        }

        /// <summary>
        /// Deletes the specified item.  When the item doesn't have any other incomming links or parent clusters, it is also
        /// removed from the brain.
        /// </summary>
        /// <param name="child">The child.</param>
        public override void DeleteOrRemoveChild(EditorItem child, DeletionMethod branchHandling)
        {
            EditorsHelper.RemoveOrDeleteFromCluster<CodeItem>((CodeItem)child, Arguments, branchHandling);
        }

        /// <summary>
        /// Check if this item or any of it's children wraps the specified neuron, if so, the item is made selected.
        /// </summary>
        /// <param name="neuron">The neuron.</param>
        public override void Select(Neuron neuron)
        {
            base.Select(neuron);
            foreach (CodeItem i in Arguments)
                i.Select(neuron);
        }

        #endregion Functions

        #region ICodeItemsOwner Members

        CodeItemCollection ICodeItemsOwner.Items
        {
            get { return Arguments; }
        }

        #endregion ICodeItemsOwner Members
    }
}