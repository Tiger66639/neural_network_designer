﻿namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// A code item that adds support for an <see cref="ExpandableCodeItem.IsExpanded"/> property.
    /// </summary>
    public class ExpandableCodeItem : CodeItem
    {
        #region fields

        private bool fIsExpanded = true;

        #endregion fields

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="ExpandableCodeItem"/> class.
        /// </summary>
        /// <param name="toWrap">To wrap.</param>
        public ExpandableCodeItem(Neuron toWrap) : base(toWrap)
        {
        }

        #endregion ctor

        #region IsExpanded

        /// <summary>
        /// Gets/sets if the item is expanded or only the header is showing.
        /// </summary>
        /// <remarks>
        /// This is usefull to remember the state of the code block, otherwise it always shows collapsed.
        /// </remarks>
        public bool IsExpanded
        {
            get
            {
                return fIsExpanded;
            }
            set
            {
                fIsExpanded = value;
                OnPropertyChanged("IsExpanded");
            }
        }

        #endregion IsExpanded
    }
}