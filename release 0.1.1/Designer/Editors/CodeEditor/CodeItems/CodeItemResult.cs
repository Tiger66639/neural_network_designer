﻿namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// A base class for all code items that return a result like a variable or search expression.
    /// </summary>
    public class CodeItemResult : CodeItem
    {
        public CodeItemResult(ResultExpression toWrap) : base(toWrap)
        {
        }

        public CodeItemResult()
        {
        }
    }
}