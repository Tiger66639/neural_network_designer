﻿namespace NeuralNetworkDesigne.HAB.Designer
{
    public class CodeItemStatic : CodeItemResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CodeItemStatic"/> class.
        /// </summary>
        /// <param name="toWrap">The item to wrap.</param>
        public CodeItemStatic(Neuron toWrap)
        {
            Item = toWrap;
        }
    }
}