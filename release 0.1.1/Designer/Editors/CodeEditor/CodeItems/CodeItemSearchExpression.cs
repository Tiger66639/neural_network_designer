﻿using System;
using System.Windows.Threading;

namespace NeuralNetworkDesigne.HAB.Designer
{
    public class CodeItemSearchExpression : CodeItemResult
    {
        #region Fields

        private CodeItemResult fInfoToSearchFor;
        private CodeItemResult fListToSearch;
        private CodeItemResult fSearchFor;
        private CodeItemResult fToSearch;

        #endregion Fields

        #region ctor-dtor

        /// <summary>
        /// Initializes a new instance of the <see cref="CodeItemSearchExpression"/> class.
        /// </summary>
        /// <param name="toWrap">To wrap.</param>
        public CodeItemSearchExpression(SearchExpression toWrap)
           : base(toWrap)
        {
        }

        #endregion ctor-dtor

        #region Prop

        #region InfoToSearchFor

        /// <summary>
        /// Gets/sets the info to search for.
        /// </summary>
        /// <remarks>
        /// Should only be visible if we are searching an info list.
        /// </remarks>
        public CodeItemResult InfoToSearchFor
        {
            get
            {
                return fInfoToSearchFor;
            }
            set
            {
                if (fInfoToSearchFor != value)
                {
                    InternalSetInfoToSearchFor(value);
                    InternalChange = true;
                    try
                    {
                        SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.InfoToSearchFor, value);
                    }
                    finally
                    {
                        InternalChange = false;
                    }
                }
            }
        }

        private void InternalSetInfoToSearchFor(CodeItemResult value)
        {
            if (fInfoToSearchFor != null)
                UnRegisterChild(fInfoToSearchFor);
            fInfoToSearchFor = value;
            if (fInfoToSearchFor != null)
                RegisterChild(fInfoToSearchFor);
            OnPropertyChanged("InfoToSearchFor");
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<string>(OnPropertyChanged), "NotHasInfoToSearchFor");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event
        }

        #endregion InfoToSearchFor

        #region NotHasInfoToSearchFor

        /// <summary>
        /// Gets if there is an InfoToSearchFor item.
        /// </summary>
        public bool NotHasInfoToSearchFor
        {
            get
            {
                return fInfoToSearchFor == null;
            }
        }

        #endregion NotHasInfoToSearchFor

        #region ListToSearch

        /// <summary>
        /// Gets/sets the identifier for the list to search (to, from, info,...)
        /// </summary>
        public CodeItemResult ListToSearch
        {
            get
            {
                return fListToSearch;
            }
            set
            {
                InternalSetListToSearch(value);
                InternalChange = true;
                try
                {
                    SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.ListToSearch, value);
                }
                finally
                {
                    InternalChange = false;
                }
            }
        }

        private void InternalSetListToSearch(CodeItemResult value)
        {
            if (fListToSearch != null)
                UnRegisterChild(fListToSearch);
            fListToSearch = value;
            if (fListToSearch != null)
                RegisterChild(fListToSearch);
            OnPropertyChanged("ListToSearch");
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<string>(OnPropertyChanged), "NotHasListToSearch");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event
        }

        #endregion ListToSearch

        #region NotHasListToSearch

        /// <summary>
        /// Gets if there is an ListToSearch item.
        /// </summary>
        public bool NotHasListToSearch
        {
            get
            {
                return fListToSearch == null;
            }
        }

        #endregion NotHasListToSearch

        #region SearchFor

        /// <summary>
        /// Gets/sets the item(s) to search for.
        /// </summary>
        public CodeItemResult SearchFor
        {
            get
            {
                return fSearchFor;
            }
            set
            {
                InternalSetSearchFor(value);
                InternalChange = true;
                try
                {
                    SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.SearchFor, value);
                }
                finally
                {
                    InternalChange = false;
                }
            }
        }

        private void InternalSetSearchFor(CodeItemResult value)
        {
            if (fSearchFor != null)
                UnRegisterChild(fSearchFor);
            fSearchFor = value;
            if (fSearchFor != null)
                RegisterChild(fSearchFor);
            OnPropertyChanged("SearchFor");
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<string>(OnPropertyChanged), "NotHasSearchFor");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event
        }

        #endregion SearchFor

        #region NotHasSearchFor

        /// <summary>
        /// Gets if there is an SearchFor item.
        /// </summary>
        public bool NotHasSearchFor
        {
            get
            {
                return fSearchFor == null;
            }
        }

        #endregion NotHasSearchFor

        #region ToSearch

        /// <summary>
        /// Gets/sets the object who's list should be searched.
        /// </summary>
        public CodeItemResult ToSearch
        {
            get
            {
                return fToSearch;
            }
            set
            {
                InternalSetToSearch(value);
                InternalChange = true;
                try
                {
                    SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.ToSearch, value);
                }
                finally
                {
                    InternalChange = false;
                }
            }
        }

        private void InternalSetToSearch(CodeItemResult value)
        {
            if (fToSearch != null)
                UnRegisterChild(fToSearch);
            fToSearch = value;
            if (fToSearch != null)
                RegisterChild(fToSearch);
            OnPropertyChanged("ToSearch");
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<string>(OnPropertyChanged), "NotHasToSearch");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event
        }

        #endregion ToSearch

        #region HasToSearch

        /// <summary>
        /// Gets if there is an ToSearch item.
        /// </summary>
        public bool NotHasToSearch
        {
            get
            {
                return fToSearch == null;
            }
        }

        #endregion HasToSearch

        #endregion Prop

        #region Functions

        /// <summary>
        /// Check if this item or any of it's children wraps the specified neuron, if so, the item is made selected.
        /// </summary>
        /// <param name="neuron">The neuron.</param>
        public override void Select(Neuron neuron)
        {
            base.Select(neuron);
            CodeItem iItem = InfoToSearchFor;
            if (iItem != null)
                iItem.Select(neuron);

            iItem = SearchFor;
            if (iItem != null)
                iItem.Select(neuron);

            iItem = ToSearch;
            if (iItem != null)
                iItem.Select(neuron);

            iItem = ListToSearch;
            if (iItem != null)
                iItem.Select(neuron);
        }

        /// <summary>
        /// Called when the <see cref="CodeItem.Item"/> has changed.
        /// </summary>
        /// <param name="value">The value.</param>
        protected override void OnItemChanged(Neuron value)
        {
            base.OnItemChanged(value);
            SearchExpression iToWrap = value as SearchExpression;
            if (iToWrap != null)
            {
                Neuron iFound = iToWrap.InfoToSearchFor;
                if (iFound != null)
                    InternalSetInfoToSearchFor((CodeItemResult)EditorsHelper.CreateCodeItemFor(iFound));
                iFound = iToWrap.ListToSearch;
                if (iFound != null)
                    InternalSetListToSearch((CodeItemResult)EditorsHelper.CreateCodeItemFor(iFound));
                iFound = iToWrap.SearchFor;
                if (iFound != null)
                    InternalSetSearchFor((CodeItemResult)EditorsHelper.CreateCodeItemFor(iFound));
                iFound = iToWrap.ToSearch;
                if (iFound != null)
                    InternalSetToSearch((CodeItemResult)EditorsHelper.CreateCodeItemFor(iFound));
            }
        }

        /// <summary>
        /// descendents that need to update links that changed can do this through this function.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void InternalLinkChanged(Link sender, LinkChangedEventArgs e)
        {
            base.InternalLinkChanged(sender, e);
            if (sender.MeaningID == (ulong)PredefinedNeurons.InfoToSearchFor)
                if (e.Action == BrainAction.Removed)
                    InternalSetInfoToSearchFor(null);
                else
                    InternalSetInfoToSearchFor((CodeItemResult)EditorsHelper.CreateCodeItemFor(Brain.Current[e.NewTo] as Expression));
            else if (sender.MeaningID == (ulong)PredefinedNeurons.ListToSearch)
                if (e.Action == BrainAction.Removed)
                    InternalSetListToSearch(null);
                else
                    InternalSetListToSearch((CodeItemResult)EditorsHelper.CreateCodeItemFor(Brain.Current[e.NewTo] as Expression));
            else if (sender.MeaningID == (ulong)PredefinedNeurons.SearchFor)
                if (e.Action == BrainAction.Removed)
                    InternalSetSearchFor(null);
                else
                    InternalSetSearchFor((CodeItemResult)EditorsHelper.CreateCodeItemFor(Brain.Current[e.NewTo] as Expression));
            else if (sender.MeaningID == (ulong)PredefinedNeurons.ToSearch)
                if (e.Action == BrainAction.Removed)
                    InternalSetToSearch(null);
                else
                    InternalSetToSearch((CodeItemResult)EditorsHelper.CreateCodeItemFor(Brain.Current[e.NewTo] as Expression));
        }

        /// <summary>
        /// Removes the current code item from the code list, but not the actual neuron that represents the code
        /// item, this stays in the brain, it is simply no longer used in this code list.
        /// </summary>
        /// <param name="child"></param>
        public override void RemoveChildFromCode(EditorItem child)
        {
            if (InfoToSearchFor == child)
                InfoToSearchFor = null;
            else if (ListToSearch == child)
                ListToSearch = null;
            else if (SearchFor == child)
                SearchFor = null;
            else if (ToSearch == child)
                ToSearch = null;
            else
                base.RemoveChildFromCode(child);
        }

        /// <summary>
        /// Deletes the specified item.  When the item doesn't have any other incomming links or parent clusters, it is also
        /// removed from the brain.
        /// </summary>
        /// <param name="child">The child.</param>
        public override void DeleteOrRemoveChild(EditorItem child, DeletionMethod branchHandling)
        {
            if (InfoToSearchFor == child)
            {
                InfoToSearchFor = null;
                EditorsHelper.DeleteWhenNotUsed(child.Item, branchHandling);
            }
            else if (ListToSearch == child)
            {
                ListToSearch = null;
                EditorsHelper.DeleteWhenNotUsed(child.Item, branchHandling);
            }
            else if (SearchFor == child)
            {
                SearchFor = null;
                EditorsHelper.DeleteWhenNotUsed(child.Item, branchHandling);
            }
            else if (ToSearch == child)
            {
                ToSearch = null;
                EditorsHelper.DeleteWhenNotUsed(child.Item, branchHandling);
            }
            else
                base.DeleteOrRemoveChild(child, branchHandling);
        }

        #endregion Functions
    }
}