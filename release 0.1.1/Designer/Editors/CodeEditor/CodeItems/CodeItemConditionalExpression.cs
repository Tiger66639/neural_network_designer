﻿using System;
using System.Windows.Threading;

namespace NeuralNetworkDesigne.HAB.Designer
{
    public class CodeItemConditionalExpression : CodeItemCodeBlock
    {
        #region internal types

        private class InternalLinkChangedEventArgs
        {
            public LinkChangedEventArgs EventArgs { get; set; }
        }

        #endregion internal types

        #region fields

        private CodeItemResult fCondition;

        #endregion fields

        #region ctor - dtor

        /// <summary>
        /// Initializes a new instance of the <see cref="CodeItemConditionalExpression"/> class.
        /// </summary>
        /// <param name="toWrap">To wrap.</param>
        public CodeItemConditionalExpression(ConditionalExpression toWrap)
           : base(toWrap)
        {
        }

        #endregion ctor - dtor

        #region Prop

        #region Condition

        /// <summary>
        /// Gets/sets the Condition that needs to be evaluated to true for executing all the statements.
        /// </summary>
        /// <remarks>
        /// This is usually a <see cref="CodeItemBooleanExpression"/> but could also be a variable or static.
        /// </remarks>
        public CodeItemResult Condition
        {
            get
            {
                return fCondition;
            }
            set
            {
                if (fCondition != value)
                {
                    InternalSetCondition(value);
                    InternalChange = true;
                    try
                    {
                        SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Condition, value);
                    }
                    finally
                    {
                        InternalChange = false;
                    }
                }
            }
        }

        private void InternalSetCondition(CodeItemResult value)
        {
            if (fCondition != null)
                UnRegisterChild(fCondition);
            fCondition = value;
            if (fCondition != null)
                RegisterChild(fCondition);
            OnPropertyChanged("Condition");
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<string>(OnPropertyChanged), "NotHasCondition");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event.
        }

        #endregion Condition

        #region NotHasCondition

        /// <summary>
        /// Gets the if there is a condition or not (true when not).
        /// </summary>
        public bool NotHasCondition
        {
            get { return fCondition == null; }
        }

        #endregion NotHasCondition

        #endregion Prop

        #region Functions

        /// <summary>
        /// Check if this item or any of it's children wraps the specified neuron, if so, the item is made selected.
        /// </summary>
        /// <param name="neuron">The neuron.</param>
        public override void Select(Neuron neuron)
        {
            base.Select(neuron);
            CodeItem iItem = Condition;
            if (iItem != null)
                iItem.Select(neuron);
        }

        /// <summary>
        /// Called when the <see cref="CodeItem.Item"/> has changed.
        /// </summary>
        /// <param name="value">The value.</param>
        protected override void OnItemChanged(Neuron value)
        {
            base.OnItemChanged(value);
            ConditionalExpression iToWrap = value as ConditionalExpression;
            if (iToWrap != null)
            {
                ResultExpression iFound = iToWrap.Condition;
                if (iFound != null)
                    InternalSetCondition((CodeItemResult)EditorsHelper.CreateCodeItemFor(iFound));
            }
        }

        /// <summary>
        /// descendents that need to update links that changed can do this through this function.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void InternalLinkChanged(Link sender, LinkChangedEventArgs e)
        {
            base.InternalLinkChanged(sender, e);
            if (sender.MeaningID == (ulong)PredefinedNeurons.Condition)
                if (e.Action == BrainAction.Removed)
                    InternalSetCondition(null);
                else
                    InternalSetCondition((CodeItemResult)EditorsHelper.CreateCodeItemFor(Brain.Current[e.NewTo] as Expression));
            else
                base.InternalLinkChanged(sender, e);
        }

        /// <summary>
        /// Removes the current code item from the code list, but not the actual neuron that represents the code
        /// item, this stays in the brain, it is simply no longer used in this code list.
        /// </summary>
        /// <param name="child"></param>
        public override void RemoveChildFromCode(EditorItem child)
        {
            if (Condition == child)
                Condition = null;
            else
                base.RemoveChildFromCode(child);
        }

        /// <summary>
        /// Deletes the specified item.  When the item doesn't have any other incomming links or parent clusters, it is also
        /// removed from the brain.
        /// </summary>
        /// <param name="child">The child.</param>
        public override void DeleteOrRemoveChild(EditorItem child, DeletionMethod branchHandling)
        {
            if (Condition == child)
            {
                Condition = null;
                EditorsHelper.DeleteWhenNotUsed(child.Item, branchHandling);
            }
            else
                base.DeleteOrRemoveChild(child, branchHandling);
        }

        #endregion Functions
    }
}