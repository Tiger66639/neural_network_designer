﻿namespace NeuralNetworkDesigne.HAB.Designer
{
    public class CodeItemCodeBlock : ExpandableCodeItem, ICodeItemsOwner
    {
        #region fields

        private CodeItemCollection fChildren;

        #endregion fields

        /// <summary>
        /// Initializes a new instance of the <see cref="CodeItemCodeBlock"/> class.
        /// </summary>
        /// <param name="toWrap">To wrap.</param>
        public CodeItemCodeBlock(ExpressionsBlock toWrap)
           : base(toWrap)
        {
            IsExpanded = false;
        }

        #region Children

        /// <summary>
        /// Gets the list of statements for this conditional expression.
        /// </summary>
        public CodeItemCollection Children
        {
            get { return fChildren; }
            private set
            {
                if (fChildren != value)
                {
                    fChildren = value;
                    OnPropertyChanged("Children");
                }
            }
        }

        #endregion Children

        #region Functions

        /// <summary>
        /// Called when the <see cref="CodeItem.Item"/> has changed.
        /// </summary>
        /// <param name="value">The value.</param>
        protected override void OnItemChanged(Neuron value)
        {
            base.OnItemChanged(value);
            fChildren = null;                                                                         //item changed, so children always need to be reloaded.
            LoadChildren();
        }

        /// <summary>
        /// descendents that need to update links that changed can do this through this function.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void InternalLinkChanged(Link sender, LinkChangedEventArgs e)
        {
            base.InternalLinkChanged(sender, e);
            if (sender.MeaningID == (ulong)PredefinedNeurons.Statements)
                LoadChildren();
        }

        /// <summary>
        /// Loads the children.
        /// </summary>
        private void LoadChildren()
        {
            NeuronCluster iStatements = ((ExpressionsBlock)Item).StatementsCluster;
            if (fChildren == null || iStatements != fChildren.Cluster)
            {
                if (iStatements != null)
                    Children = new CodeItemCollection(this, iStatements);
                else
                    Children = new CodeItemCollection(this, (ulong)PredefinedNeurons.Statements);
            }
        }

        /// <summary>
        /// Removes the current code item from the code list, but not the actual neuron that represents the code
        /// item, this stays in the brain, it is simply no longer used in this code list.
        /// </summary>
        /// <param name="child"></param>
        public override void RemoveChildFromCode(EditorItem child)
        {
            CodeItem iChild = (CodeItem)child;
            if (Children.Remove(iChild) == false)
                base.RemoveChildFromCode(iChild);
        }

        /// <summary>
        /// Deletes the specified item.  When the item doesn't have any other incomming links or parent clusters, it is also
        /// removed from the brain.
        /// </summary>
        /// <param name="child">The child.</param>
        public override void DeleteOrRemoveChild(EditorItem child, DeletionMethod branchHandling)
        {
            EditorsHelper.RemoveOrDeleteFromCluster<CodeItem>((CodeItem)child, Children, branchHandling);
        }

        /// <summary>
        /// Check if this item or any of it's children wraps the specified neuron, if so, the item is made selected.
        /// </summary>
        /// <param name="neuron">The neuron.</param>
        public override void Select(Neuron neuron)
        {
            base.Select(neuron);
            foreach (CodeItem i in Children)
                i.Select(neuron);
        }

        #endregion Functions

        #region ICodeItemsOwner Members

        CodeItemCollection ICodeItemsOwner.Items
        {
            get { return Children; }
        }

        #endregion ICodeItemsOwner Members
    }
}