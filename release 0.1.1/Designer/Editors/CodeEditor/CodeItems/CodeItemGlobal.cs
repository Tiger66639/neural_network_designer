﻿using System;
using System.Windows.Threading;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Wraps a global expression.
    /// </summary>
    public class CodeItemGlobal : CodeItemVariable
    {
        #region Fields

        private CodeItemResult fSplitReaction;

        #endregion Fields

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="CodeItemGlobal"/> class.
        /// </summary>
        /// <param name="toWrap">To wrap.</param>
        public CodeItemGlobal(Global toWrap)
           : base(toWrap)
        {
        }

        #endregion ctor

        #region Prop

        #region SplitReaction

        /// <summary>
        /// Gets/sets the operator to use for the boolean expression
        /// </summary>
        public CodeItemResult SplitReaction
        {
            get
            {
                return fSplitReaction;
            }
            set
            {
                if (fSplitReaction != value)
                {
                    InternalSetSplitReaction(value);
                    InternalChange = true;
                    try
                    {
                        SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.SplitReaction, value);
                    }
                    finally
                    {
                        InternalChange = false;
                    }
                }
            }
        }

        private void InternalSetSplitReaction(CodeItemResult value)
        {
            if (fSplitReaction != null)
                UnRegisterChild(fSplitReaction);
            fSplitReaction = value;
            if (fSplitReaction != null)
                RegisterChild(fSplitReaction);
            OnPropertyChanged("SplitReaction");
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<string>(OnPropertyChanged), "NotHasSplitReaction");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event.
        }

        #endregion SplitReaction

        #region NotHasSplitReaction

        /// <summary>
        /// Gets the if the operator is present or not.
        /// </summary>
        public bool NotHasNotHasSplitReaction
        {
            get { return ((Global)Item).SplitReaction == null; }
        }

        #endregion NotHasSplitReaction

        #endregion Prop

        #region Overrides

        /// <summary>
        /// Check if this item or any of it's children wraps the specified neuron, if so, the item is made selected.
        /// </summary>
        /// <param name="neuron">The neuron.</param>
        public override void Select(Neuron neuron)
        {
            base.Select(neuron);
            CodeItem iItem = SplitReaction;
            if (iItem != null)
                iItem.Select(neuron);
        }

        /// <summary>
        /// Called when the <see cref="CodeItem.Item"/> has changed.
        /// </summary>
        /// <param name="value">The value.</param>
        protected override void OnItemChanged(Neuron value)
        {
            base.OnItemChanged(value);
            Global iToWrap = value as Global;
            if (iToWrap != null)
            {
                Neuron iFound = iToWrap.SplitReaction;
                if (iFound != null)
                    InternalSetSplitReaction((CodeItemResult)EditorsHelper.CreateCodeItemFor(iFound));
            }
        }

        /// <summary>
        /// descendents that need to update links that changed can do this through this function.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void InternalLinkChanged(Link sender, LinkChangedEventArgs e)
        {
            base.InternalLinkChanged(sender, e);
            if (sender.MeaningID == (ulong)PredefinedNeurons.SplitReaction)
                if (e.Action == BrainAction.Removed)
                    InternalSetSplitReaction(null);
                else
                    InternalSetSplitReaction((CodeItemResult)EditorsHelper.CreateCodeItemFor(Brain.Current[e.NewTo]));
        }

        public override void RemoveChildFromCode(EditorItem child)
        {
            if (SplitReaction == child)
                SplitReaction = null;
            else
                base.RemoveChildFromCode(child);
        }

        /// <summary>
        /// Deletes the specified item.  When the item doesn't have any other incomming links or parent clusters, it is also
        /// removed from the brain.
        /// </summary>
        /// <param name="child">The child.</param>
        public override void DeleteOrRemoveChild(EditorItem child, DeletionMethod branchHandling)
        {
            if (SplitReaction == child)
            {
                SplitReaction = null;
                EditorsHelper.DeleteWhenNotUsed(child.Item, branchHandling);
            }
            else
                base.DeleteOrRemoveChild(child, branchHandling);
        }

        #endregion Overrides
    }
}