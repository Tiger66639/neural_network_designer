﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using DnD;

namespace NeuralNetworkDesigne.HAB.Designer
{
   class CodeItemDragAdvisor : DragSourceBase
   {

      public CodeItemDragAdvisor()
      {
         SupportedFormat = Properties.Resources.NeuronIDFormat;                                                        //this is not really used, simply informtive: this is our main data type.
      }

      #region UsePreviewEvents
      /// <summary>
      /// Gets if the preview event versions should be used or not.
      /// </summary>
      /// <remarks>
      /// don't use preview events cause than the sub drop points don't get used but only the main list cause this gets the events first,
      /// while we usually want to drop in a sub drop point.
      /// </remarks>
      public override bool UsePreviewEvents
      {
         get
         {
            return false;
         }
      }
      #endregion

      /// <summary>
      /// When true, drags will be done with the actual neuron, they won't be wrapped inside a CodeItem.
      /// </summary>
      /// <remarks>
      /// This is required cause some props map directly to the neuron (see <see cref="CodeitemConditionalStatement.CaseItem"/>).
      /// -> no longer used, still suppported.
      /// </remarks>
      public bool IsRaw { get; set; }


      #region Presenter

      /// <summary>
      /// Gets the content presenter that should be used to display the data on.
      /// </summary>
      public ContentPresenter Presenter
      {
         get
         {
            return ((FrameworkElement)SourceUI).Tag as ContentPresenter;
         }
      }

      #endregion

      /// <summary>
      /// Finishes the drag.
      /// </summary>
      /// <param name="draggedElt">The dragged elt.</param>
      /// <param name="finalEffects">The final effects.</param>
      public override void FinishDrag(UIElement draggedElt, DragDropEffects finalEffects)
      {
         if ((finalEffects & DragDropEffects.Move) == DragDropEffects.Move)
         {
            ContentPresenter iPresenter = Presenter;
            Debug.Assert(iPresenter != null);
            iPresenter.Content = null;
         }
      }

      /// <summary>
      /// We can drag when there is a content in the presenter.
      /// </summary>
      public override bool IsDraggable(UIElement dragElt)
      {
         ContentPresenter iPresenter = Presenter;
         Debug.Assert(iPresenter != null);
         return iPresenter.Content != null;
      }

      /// <summary>
      /// we override cause we put the image to use + an ulong if it is a neuron, or a ref to the mind map item.
      /// If the item is a link, we also store which side of the link it was, so we can adjust it again (+ update it).
      /// </summary>
      public override DataObject GetDataObject(UIElement draggedElt)
      {
         FrameworkElement iDragged = (FrameworkElement)draggedElt;
         DataObject iObj = new DataObject();

         ContentPresenter iPresenter = Presenter;
         Debug.Assert(iPresenter != null);

         iObj.SetData(Properties.Resources.UIElementFormat, iDragged);
         if (IsRaw == false)
         {
            CodeItem iContent = (CodeItem)iPresenter.Content;
            iObj.SetData(Properties.Resources.CodeItemFormat, iContent);
            iObj.SetData(Properties.Resources.NeuronIDFormat, iContent.Item.ID);
         }
         else
         {
            Neuron iNeuron = (Neuron)iPresenter.Content;
            iObj.SetData(Properties.Resources.NeuronIDFormat, iNeuron.ID);
         }

         return iObj;

      }
   }
}
