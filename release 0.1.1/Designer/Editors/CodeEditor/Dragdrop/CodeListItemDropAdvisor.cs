﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using DnD;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Drop advisor for the child items of <see cref="CodeItemCollection"/> objects.  
   /// </summary>
   public class CodeListItemDropAdvisor : DropTargetBase
   {
      #region prop

      #region UsePreviewEvents
      /// <summary>
      /// Gets if the preview event versions should be used or not.
      /// </summary>
      /// <remarks>
      /// don't use preview events cause than the sub lists don't get used but only the main list cause this gets the events first,
      /// while we usually want to drop in a sublist.
      /// </remarks>
      public override bool UsePreviewEvents
      {
         get
         {
            return false;
         }
      }
      #endregion

      #region CodeList

      /// <summary>
      /// Gets the list containing all the code that the UI to which advisor is attached too, displays data for.
      /// </summary>
      public CodeItemCollection CodeList
      {
         get
         {
            ItemsControl iItemsControl = ItemsControl.ItemsControlFromItemContainer(TargetUI);
            Debug.Assert(iItemsControl != null);
            return iItemsControl.ItemsSource as CodeItemCollection;
         }
      }

      #endregion
      #endregion

      #region Overrides
      public override void OnDropCompleted(DragEventArgs arg, Point dropPoint)
      {
         CodeItem iItem = arg.Data.GetData(Properties.Resources.CodeItemFormat) as CodeItem;
         if (iItem != null && CodeList.Contains(iItem) == true && (arg.Effects & DragDropEffects.Move) == DragDropEffects.Move)
         {
            TryMoveItem(arg, dropPoint, iItem);
         }
         else
            TryCreateNewCodeItem(arg, dropPoint);                                                  //we are not moving around an item, so add new code item.
      }


      public override bool IsValidDataObject(IDataObject obj)
      {
         return obj.GetDataPresent(Properties.Resources.CodeItemFormat)
                || obj.GetDataPresent(Properties.Resources.NeuronIDFormat);
      }

      #endregion

      private void TryMoveItem(DragEventArgs arg, Point dropPoint, CodeItem iItem)
      {
         CodeItem iDroppedOn = ((FrameworkElement)TargetUI).DataContext as CodeItem;
         CodeItemCollection iList = CodeList;
         int iNewIndex = iList.IndexOf(iDroppedOn);
         int iOldIndex = iList.IndexOf(iItem);
         if (iOldIndex == -1)
         {
            CodeItem iNew = EditorsHelper.CreateCodeItemFor(iItem.Item);                                                         //we always create a new item, even with a move, cause we don't know how the item was being dragged (with shift pressed or not), this is the savest and easiest way.
            iList.Insert(iNewIndex, iNew);
         }
         else
         {
            iList.Move(iOldIndex, iNewIndex);
            arg.Effects = DragDropEffects.None;
         }
      }

      private void TryCreateNewCodeItem(DragEventArgs obj, Point dropPoint)
      {
         ulong iId = (ulong)obj.Data.GetData(Properties.Resources.NeuronIDFormat);
         CodeItem iNew = EditorsHelper.CreateCodeItemFor(Brain.Current[iId]);
         CodeItem iDroppedOn = ((FrameworkElement)TargetUI).DataContext as CodeItem;
         Debug.Assert(iNew != null && iDroppedOn != null);
         CodeItemCollection iCodeList = CodeList;
         Debug.Assert(iCodeList != null);
         iCodeList.Insert(iCodeList.IndexOf(iDroppedOn), iNew);
      }
   }
}
