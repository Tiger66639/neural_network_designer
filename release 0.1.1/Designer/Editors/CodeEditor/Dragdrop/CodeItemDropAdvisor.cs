﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using DnD;

namespace NeuralNetworkDesigne.HAB.Designer
{
   public class CodeItemDropAdvisor: DropTargetBase
   {
      #region UsePreviewEvents
      /// <summary>
      /// Gets if the preview event versions should be used or not.
      /// </summary>
      /// <remarks>
      /// don't use preview events cause than the sub drop points don't get used but only the main list cause this gets the events first,
      /// while we usually want to drop in a sub drop point.
      /// </remarks>
      public override bool UsePreviewEvents
      {
         get
         {
            return false;
         }
      }
      #endregion

      /// <summary>
      /// When true, drops will be done by the actual neuron, they won't be wrapped inside a CodeItem.
      /// </summary>
      /// <remarks>
      /// This is required cause some props map directly to the neuron (see <see cref="CodeitemConditionalStatement.CaseItem"/>).
      /// -> no longer used for CaseItem, still suppported.
      /// </remarks>
      public bool IsRaw { get; set; }

      /// <summary>
      /// Gets/sets the type of code editor object that can be accepted.  
      /// </summary>
      /// <remarks>
      /// This property is used to check for valid drop point when a code item is being dragged around
      /// By default this is empty, indicating all types are allowed.  When set, only objects from this type (or descendents) are allowed).
      /// </remarks>
      public Type AllowedType { get; set; }

      /// <summary>
      /// Gets/sets the type of neuron object that can be accepted.  
      /// </summary>
      /// <remarks>
      /// This property is used to check for valid drop point when a neuron is being dragged that has no corresponding code item.
      /// By default this is empty, indicating all types are allowed.  When set, only objects from this type (or descendents) are allowed).
      /// </remarks>
      public Type AllowedNeuronType { get; set; }

      #region Item

      /// <summary>
      /// Gets the code item that is the datacontext of the target of this drop advisor.
      /// </summary>
      /// <remarks>
      /// Only valid when IsRaw = false
      /// </remarks>
      public CodeItem Item
      {
         get
         {
            return ((FrameworkElement)TargetUI).DataContext as CodeItem;
         }
      }

      #endregion

      #region Neuron

      /// <summary>
      /// Gets the neuron that is the datacontext of the target of this drop advisor.
      /// </summary>
      /// <remarks>
      /// Only valid when IsRaw = true
      /// </remarks>
      public Neuron Neuron
      {
         get
         {
            return ((FrameworkElement)TargetUI).DataContext as Neuron;
         }
      }

      #endregion

      #region Presenter

      /// <summary>
      /// Gets the content presenter that should be used to display the data on.
      /// </summary>
      public ContentPresenter Presenter
      {
         get
         {
            return ((FrameworkElement)TargetUI).Tag as ContentPresenter;
         }
      }

      #endregion


      #region Overrides
      public override void OnDropCompleted(DragEventArgs arg, Point dropPoint)
      {
         CodeItem iItem = arg.Data.GetData(Properties.Resources.CodeItemFormat) as CodeItem;
         if (iItem == null || IsRaw == true)                                                //we are not moving around an item, so add new code item. Also, when raw, we can't work with code items, instead we work on the neuron directly.
            TryCreateNewCodeItem(arg, dropPoint);
         else if ((arg.Effects & DragDropEffects.Move) == DragDropEffects.Move)
            TryMoveItem(arg, dropPoint, iItem);
         else
            TryCreateNewCodeItem(arg, dropPoint);
      }

      /// <summary>
      /// moves the code item.  This is actually a fake move, there is always a new object created.
      /// This is done so we can easely create duplicate visual items that point to the same code item, which
      /// is automatically done when the dragsource doesn't remove the item.
      /// </summary>
      private void TryMoveItem(DragEventArgs arg, Point dropPoint, CodeItem iItem)
      {
         CodeItem iNew = EditorsHelper.CreateCodeItemFor(iItem.Item);
         Debug.Assert(iNew != null);
         Presenter.Content = iNew;
      }

      private void TryCreateNewCodeItem(DragEventArgs obj, Point dropPoint)
      {
         ulong iId = (ulong)obj.Data.GetData(Properties.Resources.NeuronIDFormat);
         ContentPresenter iPresenter = Presenter;

         if (IsRaw == false)
         {
            CodeItem iNew = EditorsHelper.CreateCodeItemFor(Brain.Current[iId]);
            Debug.Assert(iNew != null);
            iPresenter.Content = iNew;
         }
         else
            iPresenter.Content = Brain.Current[iId];

      }

      public override bool IsValidDataObject(IDataObject obj)
      {
         bool iRes = false;
         Type iResultType = null;
         if (obj.GetDataPresent(Properties.Resources.DelayLoadResultType) == true)
         {
            DragSourceBase iSource = obj.GetData(Properties.Resources.DelayLoadResultType) as DragSourceBase;
            Debug.Assert(iSource != null);
            iResultType = iSource.GetDelayLoadResultType(obj);
         }

         if (IsRaw == false)
         {
            if (obj.GetDataPresent(Properties.Resources.CodeItemFormat) == true)
            {
               CodeItem iItem = obj.GetData(Properties.Resources.CodeItemFormat) as CodeItem;
               if (AllowedType != null)
               {
                  if (iResultType == null)
                     iResultType = iItem.GetType();
                  iRes = Presenter.Content != iItem && AllowedType.IsAssignableFrom(iResultType);
               }
               else
                  iRes = Presenter.Content != iItem;
            }
            else if(obj.GetDataPresent(Properties.Resources.NeuronIDFormat) == true)
            {
               Neuron iNeuron = null;
               if (iResultType == null)
               {
                  ulong iNeuronID = (ulong)obj.GetData(Properties.Resources.NeuronIDFormat);
                  iNeuron = Brain.Current[iNeuronID];
                  iResultType = iNeuron.GetType();
               }
               if (AllowedNeuronType != null)
                  iRes = AllowedNeuronType.IsAssignableFrom(iResultType);
               else if (AllowedType != null && iNeuron != null)
               {
                  iResultType = EditorsHelper.GetCodeItemTypeFor(iNeuron);                    //we create a quick temp item, to see if this is of the good type
                  iRes = AllowedType.IsAssignableFrom(iResultType);
               }
               else
                  iRes = true;
            }
         }
         else
         {
            iRes = obj.GetDataPresent(Properties.Resources.NeuronIDFormat);
            if (iRes == true)
            {
               if (iResultType == null)
               {
                  ulong iNeuron = (ulong)obj.GetData(Properties.Resources.NeuronIDFormat);
                  iResultType = Brain.Current[iNeuron].GetType();
               }
               iRes = AllowedNeuronType.IsAssignableFrom(iResultType);
            }
         }
         return iRes;
      }



      #endregion

   }
}
