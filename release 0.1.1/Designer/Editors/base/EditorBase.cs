﻿using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// Keeps track of the current state of the mindmap, is used to check if the object is being loaded or has already been so.
    /// </summary>
    public enum EditorState
    {
        Loading,
        Loaded,
        Undoing
    }

    /// <summary>
    /// Base class for all editor objects.
    /// </summary>
    abstract public class EditorBase : NamedObject, IDescriptionable, IXmlSerializable
    {
        #region Fields

        private EditorState fCurrentState = EditorState.Loaded;                                                    //for when first created.
        private string fDescription;
        private bool fIsSelected;

        #endregion Fields

        #region Prop

        #region Icon

        /// <summary>
        /// Gets the resource path to the icon that should be used for this editor.  This is usually class specific.
        /// </summary>
        public abstract string Icon
        {
            get;
        }

        #endregion Icon

        #region IsSelected

        /// <summary>
        /// Gets/sets the wether the editor is currently selected in the overview list.
        /// When an editor is selected, it's owning list is automatically assigned to
        /// become the <see cref="BrainData.CurrentEditorsList"/>
        /// </summary>
        /// <remarks>
        /// This is provided so that a folder can make itself 'active' when selected.
        /// </remarks>
        public virtual bool IsSelected
        {
            get
            {
                return fIsSelected;
            }
            set
            {
                if (value != fIsSelected)
                {
                    if (value == true)
                    {
                        EditorFolder iOwner = Owner as EditorFolder;
                        if (iOwner != null)
                            BrainData.Current.CurrentEditorsList = iOwner.Items;
                        else
                            BrainData.Current.CurrentEditorsList = BrainData.Current.Editors;
                    }
                    fIsSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }

        #endregion IsSelected

        #region CurrentState

        /// <summary>
        /// Gets/sets the current state of the object.  This is used by MindMapItemCollection.
        /// </summary>
        internal EditorState CurrentState
        {
            get
            {
                if (fCurrentState == EditorState.Loaded &&
                   (WindowMain.UndoStore.CurrentState == UndoState.Redoing)
                   || WindowMain.UndoStore.CurrentState == UndoState.Undoing)
                    return EditorState.Undoing;
                else
                    return fCurrentState;
            }
            set
            {
                fCurrentState = value;
            }
        }

        #endregion CurrentState

        #region IDescriptionable Members

        #region Description title

        /// <summary>
        /// Gets a title that the description editor can use to display in the header.
        /// </summary>
        /// <value></value>
        public virtual string DescriptionTitle
        {
            get { return Name + " - Editor"; }
        }

        #endregion Description title

        #region Description

        /// <summary>
        /// Gets/sets the description of the graph.
        /// </summary>
        /// <remarks>
        /// Could be that this prop needs to be streamed manually with xmlWriter.WriteRaws.
        /// </remarks>
        public FlowDocument Description
        {
            get
            {
                if (fDescription != null)
                {
                    StringReader stringReader = new StringReader(fDescription);
                    XmlReader xmlReader = XmlReader.Create(stringReader);
                    return XamlReader.Load(xmlReader) as FlowDocument;
                }
                else
                    return Helper.CreateDefaultFlowDoc();
            }
            set
            {
                string iVal = XamlWriter.Save(value);
                if (fDescription != iVal)
                {
                    fDescription = iVal;
                    OnPropertyChanged("Description");
                }
            }
        }

        #endregion Description

        #endregion IDescriptionable Members

        #endregion Prop

        #region IXmlSerializable Members

        /// <summary>
        /// This method is reserved and should not be used. When implementing the IXmlSerializable interface, you should return null (Nothing in Visual Basic) from this method, and instead, if specifying a custom schema is required, apply the <see cref="T:System.Xml.Serialization.XmlSchemaProviderAttribute"/> to the class.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Xml.Schema.XmlSchema"/> that describes the XML representation of the object that is produced by the <see cref="M:System.Xml.Serialization.IXmlSerializable.WriteXml(System.Xml.XmlWriter)"/> method and consumed by the <see cref="M:System.Xml.Serialization.IXmlSerializable.ReadXml(System.Xml.XmlReader)"/> method.
        /// </returns>
        public XmlSchema GetSchema()
        {
            return null;
        }

        public virtual void ReadXml(XmlReader reader)
        {
            CurrentState = EditorState.Loading;
            try
            {
                string iName = reader.Name;
                bool wasEmpty = reader.IsEmptyElement;

                reader.Read();
                if (wasEmpty) return;

                while (reader.Name != iName)
                {
                    if (ReadXmlInternal(reader) == false)                                      //if for some reason, we failed to read the item, log an error, and advance to the next item so that we don't get in a loop.
                    {
                        Log.LogError("EditorBase.ReadXml", string.Format("Failed to read xml element {0} in stream.", reader.Name));
                        reader.Skip();
                    }
                }
                reader.ReadEndElement();
            }
            finally
            {
                CurrentState = EditorState.Loaded;
            }
        }

        /// <summary>
        /// Reads the fields/properties of the class.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns>True if the item was properly read, otherwise false.</returns>
        /// <remarks>
        /// This function is called for each element that is found, so this function should check which element it is
        /// and only read that element accordingly.
        /// </remarks>
        protected virtual bool ReadXmlInternal(XmlReader reader)
        {
            if (reader.Name == "FlowDocument")
            {
                if (reader.IsEmptyElement == false)
                    fDescription = reader.ReadOuterXml();
                else
                    reader.ReadStartElement("FlowDocument");
                return true;
            }
            else if (reader.Name == "Name")
            {
                Name = XmlStore.ReadElement<string>(reader, "Name");
                return true;
            }
            return false;
        }

        /// <summary>
        /// Converts an object into its XML representation.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Xml.XmlWriter"/> stream to which the object is serialized.</param>
        public virtual void WriteXml(XmlWriter writer)
        {
            XmlStore.WriteElement<string>(writer, "Name", Name);
            if (fDescription != null)
                writer.WriteRaw(fDescription);
            else
            {
                writer.WriteStartElement("FlowDocument");
                writer.WriteEndElement();
            }
        }

        #endregion IXmlSerializable Members

        /// <summary>
        /// Removes the editor from the specified list.
        /// </summary>
        /// <remarks>
        /// This is a virtual function so that some editors can have multiple open documents (like a folder).
        /// </remarks>
        /// <param name="list">The list.</param>
        public virtual void RemoveEditorFrom(IList list)
        {
            list.Remove(this);
        }

        internal void RemoveFromOwner()
        {
            EditorFolder iFolder = Owner as EditorFolder;
            if (iFolder != null)
                iFolder.Items.Remove(this);
            else
                BrainData.Current.Editors.Remove(this);
        }

        #region clipboard

        /// <summary>
        /// Copies the selected data to the clipboard.
        /// </summary>
        public void CopyToClipboard()
        {
            DataObject iData = new DataObject();
            iData.SetData(Properties.Resources.APPREFFORMAT, Process.GetCurrentProcess().Id);
            CopyToClipboard(iData);
            Clipboard.SetDataObject(iData);
        }

        protected abstract void CopyToClipboard(DataObject data);

        /// <summary>
        /// Determines whether this instance can copy the selected data to the clipboard].
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if this instance can copy to the clipboard; otherwise, <c>false</c>.
        /// </returns>
        public abstract bool CanCopyToClipboard();

        /// <summary>
        /// Determines whether this instance can paste special from the clipboard.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if this instance can paste special from the clipboard; otherwise, <c>false</c>.
        /// </returns>
        public abstract bool CanPasteSpecialFromClipboard();

        /// <summary>
        /// Pastes the data in a special way from the clipboard.
        /// </summary>
        public abstract void PasteSpecialFromClipboard();

        /// <summary>
        /// Determines whether this instance can cut the selected data to the clipboard].
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if this instance can cut to the clipboard; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool CanCutToClipboard()
        {
            return CanCopyToClipboard() && CanDelete();
        }

        /// <summary>
        /// Cuts the selected data to the clipboard.
        /// </summary>
        public virtual void CutToClipboard()
        {
            CopyToClipboard();
            Delete();
        }

        /// <summary>
        /// Determines whether this instance can paste from the clipboard].
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if this instance can paste from the clipboard; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool CanPasteFromClipboard()
        {
            if (Clipboard.ContainsData(Properties.Resources.APPREFFORMAT) == true)
                return (int)Clipboard.GetData(Properties.Resources.APPREFFORMAT) == Process.GetCurrentProcess().Id;
            return false;
        }

        /// <summary>
        /// Pastes the data from the clipboard.
        /// </summary>
        public abstract void PasteFromClipboard();

        #endregion clipboard

        #region Delete

        /// <summary>
        /// Deletes all the selected items on this editor.
        /// </summary>
        public abstract void Delete();

        /// <summary>
        /// Checks if a delete can be performed on this editor.
        /// </summary>
        public abstract bool CanDelete();

        /// <summary>
        /// Deletes all the selected items on this editor after the user has selected extra deletion options.
        /// </summary>
        public abstract void DeleteSpecial();

        /// <summary>
        /// Determines whether a delete special can be performed
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if this instance can do a special delete; otherwise, <c>false</c>.
        /// </returns>
        public abstract bool CanDeleteSpecial();

        #endregion Delete
    }
}