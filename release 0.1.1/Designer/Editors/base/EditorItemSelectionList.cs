﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NeuralNetworkDesigne.HAB.Designer
{
    internal class EditorItemSelectionList<T> : IList<T>, IList where T : EditorItem
    {
        private List<T> fItems = new List<T>();

        #region IList<EditorItem> Members

        public int IndexOf(T item)
        {
            return fItems.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            if (item != null)
                item.SetSelected(true);
            fItems.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            if (index > -1 && index > fItems.Count)
            {
                if (fItems[index] != null)
                    fItems[index].SetSelected(false);
                fItems.RemoveAt(index);
            }
            else
                throw new IndexOutOfRangeException();
        }

        public T this[int index]
        {
            get
            {
                return fItems[index];
            }
            set
            {
                if (fItems[index] != null)
                    fItems[index].SetSelected(false);
                fItems[index] = value;
                if (fItems[index] != null)
                    fItems[index].SetSelected(true);
            }
        }

        #endregion IList<EditorItem> Members

        #region ICollection<CodeItem> Members

        public void Add(T item)
        {
            if (item != null)
                item.SetSelected(true);
            fItems.Add(item);
        }

        public void Clear()
        {
            foreach (T i in this)
            {
                if (i != null)
                    i.SetSelected(false);
            }
            fItems.Clear();
        }

        public bool Contains(T item)
        {
            return fItems.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            fItems.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return fItems.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(T item)
        {
            int iIndex = fItems.IndexOf(item);
            if (iIndex > -1 && item != null)
            {
                fItems[iIndex].SetSelected(false);
                fItems.RemoveAt(iIndex);
                return true;
            }
            else
                return false;
        }

        #endregion ICollection<CodeItem> Members

        #region IEnumerable<CodeItem> Members

        public IEnumerator<T> GetEnumerator()
        {
            return fItems.GetEnumerator();
        }

        #endregion IEnumerable<CodeItem> Members

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return fItems.GetEnumerator();
        }

        #endregion IEnumerable Members

        #region IList Members

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.IList"/>.
        /// </summary>
        /// <param name="value">The <see cref="T:System.Object"/> to add to the <see cref="T:System.Collections.IList"/>.</param>
        /// <returns>
        /// The position into which the new element was inserted.
        /// </returns>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.IList"/> is read-only.
        /// -or-
        /// The <see cref="T:System.Collections.IList"/> has a fixed size.
        /// </exception>
        public int Add(object value)
        {
            T iItem = value as T;
            if (iItem != null)
            {
                Add(iItem);
                return fItems.Count;
            }
            else
                throw new NotSupportedException();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.IList"/> contains a specific value.
        /// </summary>
        /// <param name="value">The <see cref="T:System.Object"/> to locate in the <see cref="T:System.Collections.IList"/>.</param>
        /// <returns>
        /// true if the <see cref="T:System.Object"/> is found in the <see cref="T:System.Collections.IList"/>; otherwise, false.
        /// </returns>
        public bool Contains(object value)
        {
            T iItem = value as T;
            if (iItem != null)
                return Contains(iItem);
            else
                throw new NotSupportedException();
        }

        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList"/>.
        /// </summary>
        /// <param name="value">The <see cref="T:System.Object"/> to locate in the <see cref="T:System.Collections.IList"/>.</param>
        /// <returns>
        /// The index of <paramref name="value"/> if found in the list; otherwise, -1.
        /// </returns>
        public int IndexOf(object value)
        {
            T iItem = value as T;
            if (iItem != null)
                return IndexOf(iItem);
            else
                throw new NotSupportedException();
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList"/> at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="value"/> should be inserted.</param>
        /// <param name="value">The <see cref="T:System.Object"/> to insert into the <see cref="T:System.Collections.IList"/>.</param>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        /// 	<paramref name="index"/> is not a valid index in the <see cref="T:System.Collections.IList"/>.
        /// </exception>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.IList"/> is read-only.
        /// -or-
        /// The <see cref="T:System.Collections.IList"/> has a fixed size.
        /// </exception>
        /// <exception cref="T:System.NullReferenceException">
        /// 	<paramref name="value"/> is null reference in the <see cref="T:System.Collections.IList"/>.
        /// </exception>
        public void Insert(int index, object value)
        {
            T iItem = value as T;
            if (iItem != null)
                Insert(index, iItem);
            else
                throw new NullReferenceException();
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList"/> has a fixed size.
        /// </summary>
        /// <value></value>
        /// <returns>true if the <see cref="T:System.Collections.IList"/> has a fixed size; otherwise, false.
        /// </returns>
        public bool IsFixedSize
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.IList"/>.
        /// </summary>
        /// <param name="value">The <see cref="T:System.Object"/> to remove from the <see cref="T:System.Collections.IList"/>.</param>
        /// <exception cref="T:System.NotSupportedException">
        /// The <see cref="T:System.Collections.IList"/> is read-only.
        /// -or-
        /// The <see cref="T:System.Collections.IList"/> has a fixed size.
        /// </exception>
        public void Remove(object value)
        {
            T iItem = value as T;
            if (iItem != null)
                Remove(iItem);
            else
                throw new NotSupportedException();
        }

        /// <summary>
        /// Gets or sets the <see cref="System.Object"/> at the specified index.
        /// </summary>
        /// <value></value>
        object IList.this[int index]
        {
            get
            {
                return this[index];
            }
            set
            {
                this[index] = value as T;
            }
        }

        #endregion IList Members

        #region ICollection Members

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection"/> to an <see cref="T:System.Array"/>, starting at a particular <see cref="T:System.Array"/> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array"/> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection"/>. The <see cref="T:System.Array"/> must have zero-based indexing.</param>
        /// <param name="index">The zero-based index in <paramref name="array"/> at which copying begins.</param>
        /// <exception cref="T:System.ArgumentNullException">
        /// 	<paramref name="array"/> is null.
        /// </exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        /// 	<paramref name="index"/> is less than zero.
        /// </exception>
        /// <exception cref="T:System.ArgumentException">
        /// 	<paramref name="array"/> is multidimensional.
        /// -or-
        /// <paramref name="index"/> is equal to or greater than the length of <paramref name="array"/>.
        /// -or-
        /// The number of elements in the source <see cref="T:System.Collections.ICollection"/> is greater than the available space from <paramref name="index"/> to the end of the destination <paramref name="array"/>.
        /// </exception>
        /// <exception cref="T:System.ArgumentException">
        /// The type of the source <see cref="T:System.Collections.ICollection"/> cannot be cast automatically to the type of the destination <paramref name="array"/>.
        /// </exception>
        public void CopyTo(Array array, int index)
        {
            T[] iArray = array as T[];
            if (iArray != null)
                fItems.CopyTo(iArray, index);
            else
                throw new NotSupportedException();
        }

        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection"/> is synchronized (thread safe).
        /// </summary>
        /// <value></value>
        /// <returns>true if access to the <see cref="T:System.Collections.ICollection"/> is synchronized (thread safe); otherwise, false.
        /// </returns>
        public bool IsSynchronized
        {
            get { return false; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection"/>.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// An object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection"/>.
        /// </returns>
        public object SyncRoot
        {
            get { return this; }
        }

        #endregion ICollection Members
    }
}