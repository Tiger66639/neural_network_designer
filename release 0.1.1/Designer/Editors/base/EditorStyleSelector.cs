﻿using System.Windows;
using System.Windows.Controls;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// A style selector to help the treeview select a style for a folder or an editor.
    /// </summary>
    public class EditorStyleSelector : StyleSelector
    {
        public override Style SelectStyle(object item, DependencyObject container)
        {
            ItemsControl iList = ItemsControl.ItemsControlFromItemContainer(container);
            if (item is EditorFolder)
                return iList.TryFindResource("FolderStyle") as Style;
            else
                return iList.TryFindResource("EditorStyle") as Style;
        }
    }
}