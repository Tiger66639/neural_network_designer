﻿using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Input;

namespace NeuralNetworkDesigne.HAB.Designer
{
    /// <summary>
    /// A base class for user controls that represent editors.  It provides default copy/paste/delete actions.
    /// </summary>
    public class CtrlEditorBase : UserControl
    {
        public CtrlEditorBase()
        {
            CommandBinding iNew = new CommandBinding(ApplicationCommands.Copy, new ExecutedRoutedEventHandler(Copy_Executed), new CanExecuteRoutedEventHandler(Copy_CanExecute));
            CommandBindings.Add(iNew);
            iNew = new CommandBinding(ApplicationCommands.Paste, new ExecutedRoutedEventHandler(Paste_Executed), new CanExecuteRoutedEventHandler(Paste_CanExecute));
            CommandBindings.Add(iNew);
            iNew = new CommandBinding(ApplicationCommands.Cut, new ExecutedRoutedEventHandler(Cut_Executed), new CanExecuteRoutedEventHandler(Cut_CanExecute));
            CommandBindings.Add(iNew);

            iNew = new CommandBinding(App.PasteSpecialCmd, new ExecutedRoutedEventHandler(PasteSpecial_Executed), new CanExecuteRoutedEventHandler(PasteSpecial_CanExecute));
            CommandBindings.Add(iNew);

            iNew = new CommandBinding(ApplicationCommands.Delete, new ExecutedRoutedEventHandler(Delete_Executed), new CanExecuteRoutedEventHandler(Delete_CanExecute));
            CommandBindings.Add(iNew);

            iNew = new CommandBinding(App.DeleteSpecialCmd, new ExecutedRoutedEventHandler(DeleteSpecial_Executed), new CanExecuteRoutedEventHandler(DeleteSpecial_CanExecute));
            CommandBindings.Add(iNew);

            KeyBinding iKey = new KeyBinding(App.DeleteSpecialCmd, Key.Delete, ModifierKeys.Control);
            InputBindings.Add(iKey);
        }

        #region Copy

        /// <summary>
        /// Handles the Executed event of the Copy control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void Copy_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            EditorBase iEditor = (EditorBase)DataContext;
            Debug.Assert(iEditor != null);
            iEditor.CopyToClipboard();
        }

        /// <summary>
        /// Handles the CanExecute event of the Copy control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        private void Copy_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            EditorBase iEditor = (EditorBase)DataContext;
            e.CanExecute = iEditor != null && iEditor.CanCopyToClipboard();
        }

        #endregion Copy

        #region Paste

        /// <summary>
        /// Handles the Executed event of the Paste control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void Paste_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            EditorBase iEditor = (EditorBase)DataContext;
            Debug.Assert(iEditor != null);
            iEditor.PasteFromClipboard();
        }

        /// <summary>
        /// Handles the CanExecute event of the Paste control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        private void Paste_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            EditorBase iEditor = (EditorBase)DataContext;
            e.CanExecute = iEditor != null && iEditor.CanPasteFromClipboard();
        }

        #endregion Paste

        #region Cut

        /// <summary>
        /// Handles the Executed event of the Cut control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void Cut_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            EditorBase iEditor = (EditorBase)DataContext;
            Debug.Assert(iEditor != null);
            iEditor.CutToClipboard();
        }

        /// <summary>
        /// Handles the CanExecute event of the Cut control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        private void Cut_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            EditorBase iEditor = (EditorBase)DataContext;
            e.CanExecute = iEditor != null && iEditor.CanCutToClipboard();
        }

        #endregion Cut

        #region PasteSpecial

        /// <summary>
        /// Handles the Executed event of the PasteSpecial control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void PasteSpecial_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            EditorBase iEditor = (EditorBase)DataContext;
            Debug.Assert(iEditor != null);
            iEditor.PasteSpecialFromClipboard();
        }

        /// <summary>
        /// Handles the CanExecute event of the PasteSpecial control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        private void PasteSpecial_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            EditorBase iEditor = (EditorBase)DataContext;
            e.CanExecute = iEditor != null && iEditor.CanPasteSpecialFromClipboard();
        }

        #endregion PasteSpecial

        #region Delete

        /// <summary>
        /// Handles the Executed event of the Delete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            EditorBase iEditor = (EditorBase)DataContext;
            Debug.Assert(iEditor != null);
            iEditor.Delete();
        }

        /// <summary>
        /// Handles the CanExecute event of the Delete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        private void Delete_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            EditorBase iEditor = (EditorBase)DataContext;
            e.CanExecute = iEditor != null && iEditor.CanDelete();
        }

        #endregion Delete

        #region DeleteSpecial

        /// <summary>
        /// Handles the Executed event of the Delete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void DeleteSpecial_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            EditorBase iEditor = (EditorBase)DataContext;
            Debug.Assert(iEditor != null);
            iEditor.DeleteSpecial();
        }

        /// <summary>
        /// Handles the CanExecute event of the Delete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        private void DeleteSpecial_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            EditorBase iEditor = (EditorBase)DataContext;
            e.CanExecute = iEditor != null && iEditor.CanDeleteSpecial();
        }

        #endregion DeleteSpecial
    }
}