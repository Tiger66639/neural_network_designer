﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using NeuralNetworkDesigne.ControlFramework.Utility;
using NeuralNetworkDesigne.Data;
using NeuralNetworkDesigne.LogService;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Interaction logic for LinkOverlay.xaml
   /// </summary>
   public partial class LinkOverlay : UserControl
   {

      ObservableCollection<LinkOverlayItem> fEditablePoints = new ObservableCollection<LinkOverlayItem>();
      Point fMenuOpenedAt;                                                                                  //stores the location where the menu opened, used to add points.
      bool fInternalEdit = false;                                                                           //keeps track if a change in the points collection was done by this class or not (for updating items).
      MindMapLink fLink;

      public LinkOverlay()
      {
         InitializeComponent();
      }

      #region Points

      /// <summary>
      /// Points Dependency Property
      /// </summary>
      public static readonly DependencyProperty PointsProperty =
          DependencyProperty.Register("Points", typeof(PointCollection), typeof(LinkOverlay),
              new FrameworkPropertyMetadata((PointCollection)null,
                  new PropertyChangedCallback(OnPointsChanged)));

      /// <summary>
      /// Gets or sets the Points property.  This dependency property 
      /// indicates the list of points that we are providing an overlay for.
      /// </summary>
      public PointCollection Points
      {
         get { return (PointCollection)GetValue(PointsProperty); }
         set { SetValue(PointsProperty, value); }
      }

      /// <summary>
      /// Handles changes to the Points property.
      /// </summary>
      private static void OnPointsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
      {
         ((LinkOverlay)d).OnPointsChanged(e);
      }

      /// <summary>
      /// Provides derived classes an opportunity to handle changes to the Points property.
      /// </summary>
      protected virtual void OnPointsChanged(DependencyPropertyChangedEventArgs e)
      {
         if (fInternalEdit == false && e.NewValue != null)
         {
            fEditablePoints.Clear();
            PointCollection iNew = (PointCollection)e.NewValue;
            for (int i = 1; i < iNew.Count - 1; i++)
               fEditablePoints.Add(new LinkOverlayItem(i, iNew));
         }
      }

      #endregion


      #region EditablePoints

      /// <summary>
      /// Gets the list of points that can be dragged around (who's values can change) -> all but first and last.
      /// </summary>
      public ObservableCollection<LinkOverlayItem> EditablePoints
      {
         get { return fEditablePoints; }
      }

      #endregion

      #region Link

      /// <summary>
      /// Gets/sets the link for which we provide an overlay.
      /// </summary>
      /// <remarks>
      /// This is required for editing it's properties and deleting it.
      /// </remarks>
      public MindMapLink Link
      {
         get
         {
            return fLink;
         }
         set
         {
            fLink = value;
            Points = fLink.InternalPoints;
         }
      }

      #endregion

      private void Thumb_DragDelta(object sender, DragDeltaEventArgs e)
      {
         fInternalEdit = true;
         try
         {
            LinkOverlayItem fItem = ((Thumb)sender).DataContext as LinkOverlayItem;
            if (fItem != null)
               fItem.ChangeValues(e.HorizontalChange, e.VerticalChange);
         }
         finally
         {
            fInternalEdit = false;
         }
      }

      /// <summary>
      /// Inserts a point in the collection.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void AddPoint_Click(object sender, RoutedEventArgs e)
      {
         fInternalEdit = true;
         try
         {
            PointCollection iPoints = Points;
            int iInsertAt = iPoints.Count - 1;                                                                        //default insert is just before last.
            //need to find where to insert. this is done by finding the 2 points that are just bigger and smaller.
            for (int i = 0; i < iPoints.Count - 1; i++)
            {
               Point iLeft = iPoints[i];
               Point iRight = iPoints[i + 1];
               if ((iLeft.X <= fMenuOpenedAt.X && iRight.X >= fMenuOpenedAt.X && iLeft.Y <= fMenuOpenedAt.Y && iRight.Y >= fMenuOpenedAt.Y)
                  || (iLeft.X >= fMenuOpenedAt.X && iRight.X <= fMenuOpenedAt.X && iLeft.Y >= fMenuOpenedAt.Y && iRight.Y <= fMenuOpenedAt.Y))
               {
                  iInsertAt = i + 1;
                  break;
               }

            }
            iPoints.Insert(iInsertAt, fMenuOpenedAt);
            foreach (LinkOverlayItem i in fEditablePoints)
            {
               if (i.Index >= iInsertAt)
                  i.Index++;
            }
            fEditablePoints.Add(new LinkOverlayItem(iInsertAt, iPoints));
         }
         finally
         {
            fInternalEdit = false;
         }
      }

      /// <summary>
      /// Remvoes the selected point from the list
      /// </summary>
      private void RemovePoint_Click(object sender, RoutedEventArgs e)
      {
         //Need to get the LinkOverlayItem for the index nr of the point to remove.
         ContextMenu iMenu = TreeHelper.FindInTree<ContextMenu>(sender as DependencyObject);
         if (iMenu != null)
         {
            Thumb iThumb = iMenu.PlacementTarget as Thumb;
            if (iThumb != null)
            {
               LinkOverlayItem iItem = iThumb.DataContext as LinkOverlayItem;
               if (iItem != null)
               {
                  Points.RemoveAt(iItem.Index);
                  fEditablePoints.Remove(iItem);
               }
            }
         }
      }

      /// <summary>
      /// Need to keep track of where the mouse was when the context menu opened, for adding points.
      /// </summary>
      private void ContextMenu_Opened(object sender, RoutedEventArgs e)
      {
         fMenuOpenedAt = Mouse.GetPosition(MainContainer);
      }

      private void ShowProp_Click(object sender, RoutedEventArgs e)
      {
         try
         {
            MindMap iMap = Link.Owner;
            DlgLink iDlg = new DlgLink();
            iDlg.Owner = Window.GetWindow(this);
            iDlg.ToList = (from i in iMap.Items
                           where i is MindMapNeuron
                           orderby ((MindMapNeuron)i).NeuronInfo.DisplayTitle
                           select (MindMapNeuron)i).ToList();          //extract all the neurons because we can make connections between those.
            iDlg.FromList = iDlg.ToList;                                                                             //from list and to list is the same.
            iDlg.SelectedFrom = Link.FromMindMapItem;
            iDlg.SelectedTo = Link.ToMindMapItem;
            iDlg.SelectedMeaning = Link.Meaning;
            
            bool? iRes = iDlg.ShowDialog();
            if (iRes.HasValue && iRes.Value == true)
            {
               WindowMain.UndoStore.BeginUndoGroup(true);                                                        //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
               try
               {
                  Link.FromMindMapItem = iDlg.SelectedFrom;
                  Link.ToMindMapItem = iDlg.SelectedTo;                                                               //guaranteed to be valid if dialog closed with ok.;
                  Link.Meaning = iDlg.SelectedMeaning; ;
               }
               finally
               {
                  WindowMain.UndoStore.EndUndoGroup();
               }
            }
         }
         catch (Exception iEx)
         {
            string iMsg = iEx.ToString();
            MessageBox.Show("failed to update the link:\n" + iMsg, "Link", MessageBoxButton.OK, MessageBoxImage.Error);
         }
         
      }

      private void DeleteLink_Click(object sender, RoutedEventArgs e)
      {

         if (fLink != null)
            fLink.Destroy();
         else
            Log.LogError("LinkOverlay.DeleteLink_Click", "Internal error: no link assigned");
      }

      private void RemoveLink_Click(object sender, RoutedEventArgs e)
      {
         if (fLink != null)
            fLink.Remove();
         else
            Log.LogError("LinkOverlay.RemoveLink_Click", "Internal error: no link assigned");
      }


   }

   public class LinkOverlayAdorner : Adorner
   {

      LinkOverlay fContent;
      VisualCollection visualChildren;


      public LinkOverlayAdorner(MindMapLink toControl, UIElement adornedElt)
         : base(adornedElt)
      {
         visualChildren = new VisualCollection(this);

         fContent = new LinkOverlay();
         fContent.Link  = toControl;
         visualChildren.Add(fContent);
      }

      protected override Size MeasureOverride(Size constraint)
      {
         fContent.Measure(constraint);
         return fContent.DesiredSize;
      }

      protected override Size ArrangeOverride(Size finalSize)
      {
         fContent.Arrange(new Rect(finalSize));
         return finalSize;
      }

      protected override Visual GetVisualChild(int index)
      {
         return fContent;
      }

      protected override int VisualChildrenCount
      {
         get
         {
            return 1;
         }
      }

      public void Clear()
      {
         fContent.Points = null;                                                                         //need to remove this ref,otherwise it keeps getting updated, even if the overlay is gone.
         visualChildren.Clear();
         fContent = null;
      }

   }

   /// <summary>
   /// A wrapper for a point in the link line that can be edited.s
   /// </summary>
   public class LinkOverlayItem : ObservableObject
   {
      int fIndex;                                                                            //the index nr of the item we are editing.

      
      PointCollection fList;                                                                 //the list that contains the points that need editing (backref).

      public LinkOverlayItem(int index, PointCollection list)
      {
         fIndex = index;
         fList = list;
      }

      /// <summary>
      /// Gets/sets  the index that this object encapsulates.
      /// </summary>
      public int Index
      {
         get { return fIndex; }
         set { fIndex = value; } 
      }

      #region X

      /// <summary>
      /// Gets/sets the name of the object
      /// </summary>
      public double X
      {
         get
         {
            return fList[fIndex].X;
         }
         set
         {
            fList[fIndex] = new Point(value, Y);
            OnPropertyChanged("X");
         }
      }
      #endregion

      #region Y

      /// <summary>
      /// Gets/sets the name of the object
      /// </summary>
      public double Y
      {
         get
         {
            return fList[fIndex].Y;
         }
         set
         {
            fList[fIndex] = new Point(X, value);
            OnPropertyChanged("Y");
         }
      }
      #endregion

      internal void ChangeValues(double horChange, double verChange)
      {
         Point iPrev = fList[fIndex];
         fList[fIndex] = new Point(iPrev.X + horChange, iPrev.Y + verChange);
         OnPropertyChanged("X");
         OnPropertyChanged("Y");
      }
   }

}
