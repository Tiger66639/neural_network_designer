﻿using System.IO;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// An item that can be displayed on a <see cref="MindMap"/> and that represents a small textual note.
   /// </summary>
   /// <remarks>
   /// The contents of the note is declared with the <see cref="MindMapItem.Description"/> property.
   /// </remarks>
   public class MindMapNote : PositionedMindMapItem, IXmlSerializable
   {

      #region fields
      string fTitle; 
      #endregion

      public override string DescriptionTitle
      {
         get
         {
            return "Note: " + Title;
         }
      }

      #region Title

      /// <summary>
      /// Gets/sets the title of this note
      /// </summary>
      public string Title
      {
         get
         {
            return fTitle;
         }
         set
         {
            OnPropertyChanging("Title", fTitle, value);
            fTitle = value;
            OnPropertyChanged("Title");
         }
      }

      #endregion


      #region IXmlSerializable Members

      public XmlSchema GetSchema()
      {
         return null;
      }

      public void ReadXml(XmlReader reader)
      {
         bool wasEmpty = reader.IsEmptyElement;

         reader.Read();
         if (wasEmpty) return;

         
         Height = XmlStore.ReadElement<double>(reader, "Height");
         Width = XmlStore.ReadElement<double>(reader, "Width");
         X = XmlStore.ReadElement<double>(reader, "X");
         Y = XmlStore.ReadElement<double>(reader, "Y");
         Title = XmlStore.ReadElement<string>(reader, "Title");

         if (reader.IsEmptyElement == false)
         {
            string iDesc = reader.ReadOuterXml();
            XmlTextReader iReader = new XmlTextReader(new StringReader(iDesc));
            Description = XamlReader.Load(iReader) as FlowDocument;
         }
         else
            reader.ReadStartElement("FlowDocument");

         reader.ReadEndElement();
      }

      public void WriteXml(XmlWriter writer)
      {
         XmlStore.WriteElement<double>(writer, "Height", Height);
         XmlStore.WriteElement<double>(writer, "Width", Width);
         XmlStore.WriteElement<double>(writer, "X", X);
         XmlStore.WriteElement<double>(writer, "Y", Y);
         XmlStore.WriteElement<string>(writer, "Title", Title);
         if (Description != null)
            XamlWriter.Save(Description, writer);
         else
         {
            writer.WriteStartElement("FlowDocument");
            writer.WriteEndElement();
         }
      }

      #endregion
   }
}
