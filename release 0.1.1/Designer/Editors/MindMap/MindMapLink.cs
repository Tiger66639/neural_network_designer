﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Threading;
using System.Xml;
//using System.Windows.Threading;
using System.Xml.Schema;
using System.Xml.Serialization;
using NeuralNetworkDesigne.LogService;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /*
    * check: http://www.dev102.com/2009/05/25/creating-gapped-and-bulleted-shapes-in-wpfsilverlight/
    * for implementing bullets on the lines in WPF.
    * 
    */

   /// <summary>
   /// An item that can be displayed on a <see cref="MindMap"/> and that represents a link between 2 <see cref="Neuron"/>s.
   /// </summary>
   /// <remarks>
   /// For the link start or link end to be draggable, the items in the data template need to have a tag value of 'start' or 
   /// 'end'.
   /// <para>
   /// This class can be optimized if some calculations were cashed, and only recalculated if invalidated by a change in the points.
   /// Best candiates are the angle calculations.
   /// </para>
   /// </remarks>
   [XmlInclude(typeof(MindMapNeuron))]
   [XmlInclude(typeof(MindMapNote))]
   [XmlInclude(typeof(MindMapLink))]
   [XmlInclude(typeof(MindMapCluster))]
   public class MindMapLink: MindMapItem, IXmlSerializable                             //don't implement iNeuronInfo for meaning ref cause this screws up the descriptio editor.
   {
      

      #region Fields
      PointCollection fPoints = new PointCollection();
      ulong fTo;                                                                       //fto and fFrom store the actual value in such a way that we are not dependent of any MindMap objects or neuron objects, just the reference, most flexible, best for storage.
      ulong fFrom;
      MindMapNeuron fToMindMapItem;                                                    //stores the actual mindmap item that we link to, for fast access, not persisted.
      MindMapNeuron fFromMindMapItem;                                                  //stores the actual mindmap item that we link to, for fast access, not persisted.
      Link fLink;                                                                      //the link, so we can quickly change the link values.

      double fEndAngle;
      double fStartAngle;
      bool fInternalChange = false;                                                      //stores who changes the list of points, this object or someone else (the overlay).
      Point fExtraInfoPos;

      static double fMargin = 16;                                                       //the number of points to stay away from the border.

      PropertyChangedEventHandler fFromChanged;
      PropertyChangedEventHandler fToChanged;

      #endregion

      #region ctor

      public MindMapLink()
      {
         fPoints.Changed += new EventHandler(fPoints_Changed);
         fFromChanged = new PropertyChangedEventHandler(from_PropertyChanged);
         fToChanged = new PropertyChangedEventHandler(to_PropertyChanged);
      }


      #endregion

      #region prop
      #region Points

      /// <summary>
      /// Gets/sets the list of points that represent this link.
      /// </summary>
      /// <remarks>
      /// The first and last item are fixed, and determined by the starting and ending point.
      /// <para>
      /// Note: makes a clone of the points, otherwise the linke wont update when this property value 
      /// changes. Probably because the UI freezes it.
      /// </para>
      /// </remarks>
      public PointCollection Points
      {
         get
         {
            return fPoints.Clone();                                                                   //we make a clone, otherwise the linke wont update when this property value changes.
         }
         set
         {
            OnPropertyChanging("Points", fPoints, value);
            fPoints = value;
            OnPropertyChanged("Points");
         }
      }

      /// <summary>
      /// Gets the actual points, not a clone. Used for editing the points directly.
      /// </summary>
      internal PointCollection InternalPoints
      {
         get
         {
            return fPoints;
         }
      }

      #endregion

      /// <summary>
      /// Gets the link that this object wraps.
      /// </summary>
      public Link Link
      {
         get { return fLink; }
      }
      
      #region Margin

      /// <summary>
      /// Gets/sets the margin that should be applied between the geometry of the item and the link.
      /// </summary>
      /// <remarks>
      /// This is a static because it should normally not be changed, but this way, it is still possible
      /// </remarks>
      [XmlIgnore]
      static public double Margin
      {
         get
         {
            return fMargin;
         }
         set
         {
            fMargin = value;
         }
      }

      #endregion

      #region StartPoint

      /// <summary>
      /// Gets the start point of the point collection, which is the center of the 'from' neuron.
      /// </summary>
      /// <remarks>
      /// This is a convenience prop for wpf so a custom images can be used to indicate
      /// the End.
      /// </remarks>
      [XmlIgnore]
      public Point StartPoint
      {
         get
         {
            if (fPoints != null && fPoints.Count > 0)
               return fPoints[0];
            else
               throw new InvalidOperationException();
         }
      }

      #endregion

      #region StartAngle
      /// <summary>
      /// Gets the angle that should be applied to the start point in a datatemplate so that it appears
      /// as if it is following the direction of the link line.  This is a calculated
      /// value.
      /// </summary>
      /// <remarks>
      /// This value is the inverse of the angle between the 'start' (centerpoint) and 'end'. This is done
      /// cause the arrow needs the inverse value.
      /// </remarks>
      [XmlIgnore]
      public double StartAngle
      {
         get
         {
            return fStartAngle;
         }
         internal set
         {
            if (value != fEndAngle)
            {
               fStartAngle = value;
               OnPropertyChanged("StartAngle");
            }
         }
      } 
      #endregion

      #region EndPoint

      /// <summary>
      /// Gets the End point of the point collection, which is the center of the 'to' neuron.
      /// </summary>
      /// <remarks>
      /// This is a convenience prop for wpf so a custom images can be used to indicate
      /// the End.
      /// </remarks>
      [XmlIgnore]
      public Point EndPoint
      {
         get
         {
            if (fPoints != null && fPoints.Count > 0)
               return fPoints[fPoints.Count - 1];
            else
               throw new InvalidOperationException();
         }
      }

      #endregion

      #region EndAngle
      /// <summary>
      /// Gets the angle that should be applied to the end point in a datatemplate so that it appears
      /// as if it is following the direction of the link line.  This is a calculated
      /// value.
      /// </summary>
      /// <remarks>
      /// This value is the inverse of the angle between the 'end' (centerpoint) and 'start'. This is done
      /// cause the arrow needs the inverse value.
      /// </remarks>
      [XmlIgnore]
      public double EndAngle
      {
         get
         {
            return fEndAngle;
         }
         internal set
         {
            if (value != fEndAngle)
            {
               fEndAngle = value;
               OnPropertyChanged("EndAngle");
            }
         }
      } 
      #endregion

      #region To

      /// <summary>
      /// Gets/sets the neuron id from where the link starts.
      /// </summary>
      /// <remarks>
      /// Only used for streaming or setting up a link when not yet resolved.
      /// </remarks>
      public ulong To
      {
         get
         {
            return fTo;
         }
         set
         {
            if (ToMindMapItem != null)
               throw new InvalidOperationException("Please use the ToMindMapItem property when the link has been resolved for setting this value.");
            OnPropertyChanging("To", fTo, value);
            fTo = value;
            if (fLink != null)                                                               //when the object is being read from xml, link is not yet set, so this doesn't change the vals
               fLink.To = Brain.Current[value];
            OnPropertyChanged("To");
         }
      }

      #endregion

      #region From

      /// <summary>
      /// Gets/sets the neuron id to where the link goes.
      /// </summary>
      public ulong From
      {
         get
         {
            return fFrom;
         }
         set
         {
            if (ToMindMapItem != null)
               throw new InvalidOperationException("Please use the ToMindMapItem property when the link has been resoved for setting this value.");
            OnPropertyChanging("From", fFrom, value);
            fFrom = value;
            if (fLink != null)                                                               //when the object is being read from xml, link is not yet set, so this doesn't change the val
               fLink.From = Brain.Current[value];
            OnPropertyChanged("From");
         }
      }

      #endregion

      #region ToMindMapItem

      /// <summary>
      /// Gets/sets the mind map item from which this link starts.
      /// </summary>
      /// <remarks>
      /// Only available when link is resolved. Use this property to change this value.
      /// </remarks>
      [XmlIgnore]
      public MindMapNeuron ToMindMapItem
      {
         get
         {
            return fToMindMapItem;
         }
         set
         {
            if (fToMindMapItem != value)
            {
               if (fToMindMapItem != null)
                  fToMindMapItem.PropertyChanged -= fToChanged;
               OnPropertyChanging("ToMindMapItem", fToMindMapItem, value);
               fToMindMapItem = value;
               if (fToMindMapItem != null)
               {
                  fToMindMapItem.PropertyChanged += fToChanged;
                  to_PropertyChanged(fToMindMapItem, new PropertyChangedEventArgs("X"));                //we call this function to update the new pos
                  if (fLink != null)                                                               //when the object is being read from xml, link is not yet set, so this doesn't change the vals
                     fLink.To = value.Item;
                  fTo = value.ItemID;
               }
               else if (fLink != null)
               {
                  fLink.To = null;
                  fTo = Neuron.EmptyId;
               }
               OnPropertyChanged("ToMindMapItem");
               OnPropertyChanged("To");
            }
         }
      }

      #endregion

      #region FromMindMapItem

      /// <summary>
      /// Gets/sets the mind map item to which this link goes to.
      /// </summary>
      /// <remarks>
      /// Only available when link is resolved. Use this property to change this value.
      /// </remarks>
      [XmlIgnore]
      public MindMapNeuron FromMindMapItem
      {
         get
         {
            return fFromMindMapItem;
         }
         set
         {
            if (fFromMindMapItem != value)
            {
               if (fFromMindMapItem != null)
                  fFromMindMapItem.PropertyChanged -= fFromChanged;
               OnPropertyChanging("FromMindMapItem", fFromMindMapItem, value);
               fFromMindMapItem = value;
               if (fFromMindMapItem != null)
               {
                  fFromMindMapItem.PropertyChanged += fFromChanged;
                  from_PropertyChanged(fToMindMapItem, new PropertyChangedEventArgs("X"));                //we call this function to update the new pos
                  if (fLink != null)                                                               //when the object is being read from xml, link is not yet set, so this doesn't change the vals
                     fLink.From = value.Item;
                  fFrom = fFromMindMapItem.ItemID;
               }
               else if (fLink != null)
               {
                  fLink.From = null;
                  fFrom = Neuron.EmptyId;
               }
               OnPropertyChanged("FromMindMapItem");
               OnPropertyChanged("From");
            }
         }
      }

      #endregion

      #region Meaning

      /// <summary>
      /// Gets/sets the meaning of this relationship.
      /// </summary>
      /// <remarks>
      /// This property is only known/usable when the link is resolved.
      /// It is not stored in xml cause it is retrieved /assigned directly to the link object.
      /// </remarks>
      [XmlIgnore]
      public Neuron Meaning
      {
         get
         {
            if (fLink != null)
               return fLink.Meaning;
            else
               throw new InvalidOperationException("Can't retrieve meaning, link not yet resolved.");
         }
         set
         {
            if (fLink != null)
            {
               if (fLink.Meaning != value)
               {
                  OnPropertyChanging("Meaning", fLink.Meaning, value);
                  fLink.Meaning = value;
                  OnPropertyChanged("Meaning");
                  OnPropertyChanged("MeaningInfo");
               }
            }
            else
               throw new InvalidOperationException("Can't assign a new meaning, link not yet resolved.");
         }
      }

      public NeuronData MeaningInfo
      {
         get
         {
            if (fLink != null)
               return BrainData.Current.NeuronInfo[fLink.MeaningID];
            return null;
         }
      }
      

      #endregion 

      #region ExtraInfoPos

      /// <summary>
      /// Gets/sets the position of the meaning and info of the link.
      /// </summary>
      public Point ExtraInfoPos
      {
         get
         {
            return fExtraInfoPos;
         }
         set
         {
            fExtraInfoPos = value;
            OnPropertyChanged("ExtraInfoPos");
         }
      }

      #endregion

      #region Description Title
      /// <summary>
      /// Gets the description title.
      /// </summary>
      /// <value>The description title.</value>
      public override string DescriptionTitle
      {
         get
         {
            return "Mindmap link";
         }
      } 
      #endregion


      /// <summary>
      /// this is used to determin a custom ordening for saving the items in the list.  This is used to make certain that
      /// link objects are at the back of the list (so that they can load properly).
      /// </summary>
      /// <value>The index of the type.</value>
      protected internal override int TypeIndex
      {
         get
         {
            return int.MaxValue;
         }
      }

      #endregion

      #region functions

      /// <summary>
      /// Finds the link object and registers the event handlers so we can monitor a move of objets
      /// </summary>
      /// <param name="list">The list.</param>
      /// <returns>True if the operation succeeded, otherwise false.</returns>
      internal bool ResolveLink(MindMapItemCollection list)
      {
         if (fToMindMapItem != null)
            fToMindMapItem.PropertyChanged -= fToChanged;
         if (fFromMindMapItem != null)
            fFromMindMapItem.PropertyChanged -= fFromChanged;
         fToMindMapItem = null;
         fFromMindMapItem = null;
         foreach (MindMapItem i in list)
         {
            MindMapNeuron iNeuron = i as MindMapNeuron;
            if (iNeuron != null)
            {
               if (iNeuron.ItemID == fTo && fToMindMapItem == null)                                              //don't want double event handlers.
               {
                  iNeuron.PropertyChanged += fToChanged;
                  fToMindMapItem = iNeuron;
               }
               else if (iNeuron.ItemID == fFrom && fFromMindMapItem == null)
               {
                  iNeuron.PropertyChanged += fFromChanged;
                  fFromMindMapItem = iNeuron;
               }
               if (fFromMindMapItem != null && fToMindMapItem != null)
                  break;
            }
         }
         using (LinksAccessor iLinksOut = fFromMindMapItem.Item.LinksOut)
            fLink = (from i in iLinksOut.Items where i.ToID == fTo select i).FirstOrDefault();
         if (fLink == null)
         {
            ulong iFromId = fFromMindMapItem != null ? fFromMindMapItem.ItemID : 0;
            ulong iToId = fToMindMapItem != null ? fFromMindMapItem.ItemID : 0;
            Log.LogWarning("MindMapLink.ResolveLink", string.Format("failed to find link from '{0}', to '{1}'!", iFromId, iToId)); 
            return false;
         }
         return true;
      }

      /// <summary>
      /// Removes any event handlers from the to and from mind map items + resets the link and mindmap item references.
      /// </summary>
      internal void DisconnectLink()
      {
         if (fFromMindMapItem != null)
            fFromMindMapItem.PropertyChanged -= fFromChanged;
         if (fToMindMapItem != null)
            fToMindMapItem.PropertyChanged -= fToChanged;
         fFromMindMapItem = null;
         fToMindMapItem = null;
         fLink = null;
      }

      /// <returns>A deep copy of this object.</returns>
      public override MindMapItem Duplicate()
      {
         MindMapLink iRes = (MindMapLink)base.Duplicate();
         iRes.fPoints = new PointCollection(fPoints);
         iRes.fTo = fTo;
         iRes.fFrom = fFrom;
         return iRes;
      }


      /// <summary>
      /// Sets up everything so that the link can be displayed between the specified 2 points + generates undo data for the event.
      /// </summary>
      /// <param name="link">The link object between the 2 neurons.</param>
      /// <param name="from">The mindmap item that represents the from part.</param>
      /// <param name="to">The mind map item that represents the to part.</param>
      internal void CreateLink(Link link, MindMapNeuron from, MindMapNeuron to)
      {
         fTo = link.ToID;
         fFrom = link.FromID;
         fLink = link;

         Point iStart = new Point(from.X + (from.Width / 2), from.Y + (from.Height / 2));
         Point iEnd = new Point(to.X + (to.Width / 2), to.Y + (to.Height / 2));
         double iStartAngle = Helper.GetAnlge(iStart, iEnd);
         double iEndAngle = Helper.GetAnlge(iEnd, iStart);

         fInternalChange = true;
         try
         {
            fPoints.Clear();
            fPoints.Add(GetGeometryPoint(from, iStartAngle));
            fPoints.Add(GetGeometryPoint(to, iEndAngle));
            StartAngle = 360 - iStartAngle;                                            //the arrow in the UI acutally needs the inverse value.
            EndAngle = 360 - iEndAngle;

            ExtraInfoPos = new Point((fPoints[0].X + fPoints[1].X) / 2,         //25+.. -> we need to put the text above th line a little
                                     (fPoints[0].Y + fPoints[1].Y) / 2);

         }
         finally
         {
            fInternalChange = false;
         }

         fToMindMapItem = to;
         fFromMindMapItem = from;
         fFromMindMapItem.PropertyChanged += fFromChanged;
         fToMindMapItem.PropertyChanged += fToChanged;
      }

      /// <summary>
      /// whenever the mindmaap neurom moves, we need to move as well.
      /// </summary>
      /// <remarks>
      /// Note: this function is also called to change the start when the to neuron is changed.
      /// </remarks>
      void to_PropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(ToChanged));        //we call async, cause otherwise, the angle doesn't always get calculated correctly, this solves some, not always.
      }

      void ToChanged()
      {
         if (fLink != null)
         {
            fInternalChange = true;
            try
            {
               Point iEnd = new Point(ToMindMapItem.X + (ToMindMapItem.Width / 2), ToMindMapItem.Y + (ToMindMapItem.Height / 2));
               Point iStart;
               if (fPoints.Count == 2)
               {
                  iStart = new Point(FromMindMapItem.X + (FromMindMapItem.Width / 2), FromMindMapItem.Y + (FromMindMapItem.Height / 2));
                  double iStartAngle = Helper.GetAnlge(iStart, iEnd);
                  fPoints[0] = GetGeometryPoint(FromMindMapItem, iStartAngle);
                  StartAngle = 360 - iStartAngle;                                            //the arrow in the UI acutally needs the inverse value.
               }
               else
                  iStart = fPoints[fPoints.Count - 2];

               Double iEndAngle = Helper.GetAnlge(iEnd, iStart);
               fPoints[fPoints.Count - 1] = GetGeometryPoint(ToMindMapItem, iEndAngle);
               EndAngle = 360 - iEndAngle;
            }
            finally
            {
               fInternalChange = false;
            }
         }
      }

      /// <summary>
      /// whenever the mindmaap neurom moves, we need to move as well.
      /// </summary>
      /// <remarks>
      /// Note: this function is also called to change the start when the from neuron is changed.
      /// </remarks>
      void from_PropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(FromChanged));        //we call async, cause otherwise, the angle doesn't always get calculated correctly, this solves some, not always.

      }

      void FromChanged()
      {
         if (fLink != null)
         {
            fInternalChange = true;
            try
            {
               Point iEnd;
               Point iStart = new Point(FromMindMapItem.X + (FromMindMapItem.Width / 2), FromMindMapItem.Y + (FromMindMapItem.Height / 2));

               if (fPoints.Count == 2)
               {
                  iEnd = new Point(ToMindMapItem.X + (ToMindMapItem.Width / 2), ToMindMapItem.Y + (ToMindMapItem.Height / 2));
                  double iEndAngle = Helper.GetAnlge(iEnd, iStart);
                  fPoints[1] = GetGeometryPoint(ToMindMapItem, iEndAngle);
                  EndAngle = 360 - iEndAngle;
               }
               else
                  iEnd = fPoints[1];

               double iStartAngle = Helper.GetAnlge(iStart, iEnd);
               fPoints[0] = GetGeometryPoint(FromMindMapItem, iStartAngle);
               StartAngle = 360 - iStartAngle;                                            //the arrow in the UI acutally needs the inverse value.
            }
            finally
            {
               fInternalChange = false;
            }
         }
      }

      /// <summary>
      /// If the first, last or the one after first/before last is changed, we need to update.
      /// For first and last: the startPoint and EndPoint props, for the others, we need
      /// to calculate the rotation that needs to be applied to any object so that it looks as if 
      /// it follows the line.
      /// </summary>
      /// <remarks>
      /// Unfortunatly, we don't get info on which item is changed, so update all.
      /// </remarks>
      void fPoints_Changed(object sender, EventArgs e)
      {
         if (fInternalChange == false)
         {
            ToChanged();
            FromChanged();
         }

         if (fPoints.Count > 1)
         {
            int iCenter = (fPoints.Count - 1) / 2;
            ExtraInfoPos = new Point(((fPoints[iCenter].X + fPoints[iCenter + 1].X) / 2),         //25+.. -> we need to put the text above th line a little
                                        (fPoints[iCenter].Y + fPoints[iCenter + 1].Y) / 2);
         }

         OnPropertyChanged("StartPoint");
         OnPropertyChanged("EndPoint");
         //OnPropertyChanged("StartAngle");
         //OnPropertyChanged("EndAngle");
         OnPropertyChanged("Points");

      }

      /// <summary>
      /// Gets the point that is located at the specified angle relative to the top of the neuron.
      /// </summary>
      /// <remarks>
      /// <para>
      /// All mindmap neurons defines a path that represents their outer border. if the start of this
      /// path would be at the angle 0 degrees, this function returns the point at the specified angle.
      /// </para>
      /// <para>
      /// There is no direct function to do this, so instead we create a temp list of points at a fixed
      /// distance of each other (360 -> 1 for each degree), calculate the angle of this point and we return
      /// find the closest match.
      /// </para>
      /// </remarks>
      /// <param name="item"></param>
      /// <param name="angle"></param>
      /// <returns></returns>
      Point GetGeometryPoint(MindMapNeuron item, double angle)
      {
         if (item.Shape != null)
         {
            EdgePoint iResult = null;
            double iResDif = 0.0;

            foreach (EdgePoint i in item.EdgePoints)
            {
               if (iResult != null)
               {
                  double iNewDif = Math.Abs(angle - i.Angle);
                  if (iNewDif < iResDif)
                  {
                     iResDif = iNewDif;
                     iResult = i;
                  }
               }
               else
               {
                  iResult = i;
                  iResDif = Math.Abs(angle - i.Angle);
               }
            }
            return iResult.Point;
         }
         return new Point();
      } 
      #endregion




      #region IXmlSerializable Members

      public XmlSchema GetSchema()
      {
         return null;
      }

      public void ReadXml(XmlReader reader)
      {
         bool wasEmpty = reader.IsEmptyElement;

         reader.Read();
         if (wasEmpty) return;

         reader.ReadStartElement("To");
         To = ulong.Parse(reader.ReadString());
         reader.ReadEndElement();

         reader.ReadStartElement("From");
         From = ulong.Parse(reader.ReadString());
         reader.ReadEndElement();

         string iDesc = reader.ReadOuterXml();
         XmlTextReader iReader = new XmlTextReader(new StringReader(iDesc));
         Description = XamlReader.Load(iReader) as FlowDocument;

         iDesc = reader.ReadOuterXml();
         iReader = new XmlTextReader(new StringReader(iDesc));
         fPoints = XamlReader.Load(iReader) as PointCollection;
         fPoints.Changed += new EventHandler(fPoints_Changed);

         reader.ReadEndElement();
      }

      public void WriteXml(XmlWriter writer)
      {
         //writer.WriteAttributeString("xsi:type","MindMapLink");                                 //need to identify this neuron for the mindmap.

         writer.WriteStartElement("To");
         writer.WriteString(To.ToString());
         writer.WriteEndElement();

         writer.WriteStartElement("From");
         writer.WriteString(From.ToString());
         writer.WriteEndElement();

         if (Description != null)
            XamlWriter.Save(Description, writer);
         else
         {
            writer.WriteStartElement("Description");
            writer.WriteEndElement();
         }

         if (fPoints != null)
            XamlWriter.Save(fPoints, writer);
         else
         {
            writer.WriteStartElement("Point");
            writer.WriteEndElement();
         }
      }

      #endregion


      /// <summary>
      /// Destroys this instance, thereby also destroying the link between the 2 neurons while generating undo data.
      /// </summary>
      public void Destroy()
      {
         LinkUndoItem iUndoData = new LinkUndoItem() { Action = BrainAction.Removed, Item = Link };
         WindowMain.UndoStore.BeginUndoGroup(true);                                                                                 //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
         try
         {
            WindowMain.UndoStore.AddCustomUndoItem(iUndoData);
            Link.Destroy();                                                                                                         //this will indirectly also remove the item from the mindmap.
         }
         finally
         {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(WindowMain.UndoStore.EndUndoGroup));  //we call async cause the Link.Destroy() raises an event which is handled async as well.
         }
      }

      internal void Remove()
      {
         if(Owner != null)
            Owner.Items.Remove(this);
      }
   }
}
