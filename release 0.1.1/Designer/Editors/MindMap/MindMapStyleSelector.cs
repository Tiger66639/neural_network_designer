﻿using System.Windows;
using System.Windows.Controls;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// a style selector used by the mind map to return the correct style for an object or for a link.
   /// </summary>
   public class MindMapStyleSelector: StyleSelector
   {
      public override Style SelectStyle(object item, DependencyObject container)
      {
         FrameworkElement iCont = container as FrameworkElement;
         if (iCont != null)
         {
            if (item is MindMapLink)
               return iCont.TryFindResource("MindMapLinkStyle") as Style;
            else
               return iCont.TryFindResource("MindMapObjectStyle") as Style;
         }
         return null;
      }
   }
}
