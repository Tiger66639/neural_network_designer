﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DnD;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Provides logic for dragging items from a canvas used by a mindmap view.
   /// </summary>
   /// <remarks>
   /// Supports for single item drag of neuron and links, but only multi item support for neurons.
   /// </remarks>
   class MindMapDragSourceAdvisor: DragSourceBase
   {

      #region fiellds
      List<MindMapItem> fSelected = new List<MindMapItem>(); 
      #endregion

      #region ctor
      /// <summary>
      /// Initializes a new instance of the <see cref="MindMapDragSourceAdvisor"/> class.
      /// </summary>
      public MindMapDragSourceAdvisor()
         : base(Properties.Resources.NeuronIDFormat)             //this is not really used, simply informtive: this is our main data type.
      {
      } 
      #endregion

      /// <summary>
      /// Gets if the preview event versions should be used or not.
      /// </summary>
      /// <value></value>
      public override bool UsePreviewEvents
      {
         get
         {
            return false;
         }
      }

      /// <summary>
      /// Finishes the drag.
      /// </summary>
      /// <remarks>
      /// We copy over the selected items again to the listbox, this allows the user to drag the same
      /// items again without selecting them.
      /// </remarks>
      /// <param name="draggedElt">The dragged elt.</param>
      /// <param name="finalEffects">The final effects.</param>
      public override void FinishDrag(UIElement draggedElt, DragDropEffects finalEffects)
      {
         ListBox iSource = Source;
         if (iSource != null)
         {
            iSource.SelectedItems.Clear();
            foreach (MindMapItem i in fSelected)
               iSource.SelectedItems.Add(i);
         }
      }

      ListBox Source
      {
         get
         {
            return ItemsControl.ItemsControlFromItemContainer(((FrameworkElement)SourceUI).TemplatedParent) as ListBox;
         }
      }

      /// <summary>
      /// We can drag all objects, except links, those can only be dragged if the start or end part is selected.
      /// </summary>
      /// <remarks>
      /// The tag contains the info to indicate a start or end point.
      /// <para>
      /// This function also copies the selected items over from the listbox into a private list.  We do this because
      /// this function is called before the selection is changed on the listbox.  if the user wants to move multiple
      /// items, this is the only moment we can capture this info.
      /// </para>
      /// </remarks>
      /// <param name="dragElt"></param>
      /// <returns></returns>
      public override bool IsDraggable(UIElement dragElt)
      {
         FrameworkElement iDragged = (FrameworkElement)dragElt;
         if (iDragged != null)
         {
            fSelected.Clear();
            MindMapLink iLink = iDragged.DataContext as MindMapLink;
            if (iLink != null)
            {
               if (string.Equals(iDragged.Tag, "start") || string.Equals(iDragged.Tag, "end"))
               {
                  fSelected.Add(iLink);
                  return true;
               }
            }
            else
            {
               MindMapItem iItem = ((FrameworkElement)dragElt).DataContext as MindMapItem;
               if (iItem != null)
               {
                  int iIndex = Source.SelectedItems.IndexOf(iItem);
                  if (iIndex < 0 && Keyboard.IsKeyDown(Key.LeftCtrl) == false && Keyboard.IsKeyDown(Key.RightCtrl) == false)
                     Source.SelectedItems.Clear();                                                             //we clear the items just before a drag if the control key isn't pressed.  This is to prevent previously selected items from unwantingly being moved.
                  if (Source.SelectedItems.Count > 0)
                     fSelected.AddRange(from MindMapItem i in Source.SelectedItems select i);                  //SelectedItems is list with objects, fSelected is generic list 
                  if (iIndex > 0)
                     fSelected.RemoveAt(iIndex); 
                  if (iIndex != 0)                                                                             //the currently selected item must be the first in the list.
                     fSelected.Insert(0, iItem);
                  return true;
               }
            }
         }
         return false;
      }

      /// <summary>
      /// we override cause we put the image to use + an ulong if it is a neuron, or a ref to the mind map item.
      /// If the item is a link, we also store which side of the link it was, so we can adjust it again (+ update it).
      /// </summary>
      public override DataObject GetDataObject(UIElement draggedElt)
      {
         FrameworkElement iDragged = (FrameworkElement)draggedElt;
         DataObject iObj = new DataObject();

         ListBox iSource = Source;
         if (iSource != null)
         {
            if (fSelected.Count == 1)                                                                          //need to store single data info.
            {
               MindMapItem iData = fSelected[0];
               Debug.Assert(iData != null);

               iObj.SetData(Properties.Resources.UIElementFormat, iDragged);
               iObj.SetData(Properties.Resources.MindMapItemFormat, iData);
               if (iData is MindMapNeuron)
                  iObj.SetData(Properties.Resources.NeuronIDFormat, ((MindMapNeuron)iData).ItemID);
               else if (iData is MindMapLink)
                  iObj.SetData(Properties.Resources.MindMapLinkSide, iDragged.Tag);
            }
            else
            {                                                                                                     //need to store list info of all the dragged items.
               List<UIElement> iSelectedUI = new List<UIElement>();
               List<PositionedMindMapItem> iSelected = new List<PositionedMindMapItem>();
               foreach (MindMapItem i in fSelected)
               {
                  iSelectedUI.Add((UIElement)iSource.ItemContainerGenerator.ContainerFromItem(i));
                  if (i is PositionedMindMapItem)
                     iSelected.Add((PositionedMindMapItem)i);
                  else
                     throw new InvalidOperationException("Can't drag links and other objects at once.");
               }
               iObj.SetData(Properties.Resources.MultiMindMapItemFormat, iSelected);                              //need to set the data is PositionedMindMapItems, cause that's easier to work with in other parts.
               iObj.SetData(Properties.Resources.MultiUIElementFormat, iSelectedUI);
               List<ulong> iSelectedID = (from i in fSelected
                                          where i is MindMapNeuron
                                          select ((MindMapNeuron)i).ItemID).ToList();
               iObj.SetData(Properties.Resources.MultiNeuronIDFormat, iSelectedID);
            }
         }
         return iObj;

      }
   }
}
