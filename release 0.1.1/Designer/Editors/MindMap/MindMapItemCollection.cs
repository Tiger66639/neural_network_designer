﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using NeuralNetworkDesigne.Data;
using NeuralNetworkDesigne.LogService;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// A collection of <see cref="MindMapItem"/>s.
   /// </summary>
   /// <remarks>
   /// This collection makes certain that whenever a MindMapLink is added, it is resolved.
   /// Whenever a neuron is added, all it's existing links are drawn on the map, when it
   /// is removed, all it's links are also removed. 
   /// </remarks>
   public class MindMapItemCollection: ObservedCollection<MindMapItem, MindMap>
   {

      #region ctor
      public MindMapItemCollection(MindMap owner)
         : base(owner)
      {

      }

      public MindMapItemCollection()
         : base()
      {

      } 
      #endregion



      
      /// <summary>
      /// <para>  
      /// We only allow unique nuerons on a mindmap, so when we want to add a neuron, check that it doesn't already exist.
      /// </para>
      /// <para>
      /// Only unique mindmap links are allowed, so if a duplicate is found, an error is thrown.
      /// </para>
      /// When a mind map link is added, we try to resolve it's neuron link so it can get positional info from the mind map neurons it
      /// should link to.
      /// We also make certain that all links are created for newly added neuron objects.
      /// The ZIndex is also assigned, according to it's index value.
      /// </summary>
      /// <param name="index">The index at which the item is inserted.</param>
      /// <param name="item">The item being added.</param>
      protected override void InsertItem(int index, MindMapItem item)
      {
         MindMapLink iItem = item as MindMapLink;
         if (iItem != null)
         {
            var iFound = (from i in this
                          where i is MindMapLink && ((MindMapLink)i).To == iItem.To && ((MindMapLink)i).From == iItem.From
                          select (MindMapLink)i).FirstOrDefault();
            if (iFound != null)
            {
               ShowError(string.Format("Link between '{0}' and '{1}' already on mind map!", iFound.ToMindMapItem.Item, iFound.FromMindMapItem.Item ));
               return;
            }
            if (iItem.Link == null)                                                                //if the link isn't resolved yet,do it now.  
               if (iItem.ResolveLink(this) == false)                                               //If the link can't be resolved, we don't add the item.
               {
                  Log.LogWarning("MindMapItemCollection.InsertItem", "Removed Link from mindmap.");
                  return;
               }
         }
         MindMapNeuron iNeuron = item as MindMapNeuron;
         if (iNeuron != null)                                                                      //check if the operation is legal in case it is a neuron item (not yet on the mindmap).
         {
            MindMapItem iAlreadyOnMap = (from i in this where i is MindMapNeuron && ((MindMapNeuron)i).ItemID == iNeuron.ItemID select i).FirstOrDefault();
            if (iAlreadyOnMap != null)
            {
               ShowError(string.Format("Neuron '{0}' already on mindmap, can't have duplicate items on map (otherwise links don't work properly).", iNeuron.Item.ToString()));
               return;
            }
         }
         if (Owner.CurrentState == EditorState.Loaded)                                              //when we are loading from xml, we don't want to adjust the ZIndex.
            item.ZIndex = index;
         base.InsertItem(index, item);
         if (Owner != null && Owner.CurrentState == EditorState.Loaded)                             //when loading, from disk, don't want to auto create existing links cause we are recovering a previous state, not building a new one.
            UpdateNeuronAfterInsert(iNeuron);
      }

      /// <summary>
      /// Updates the mindmap neuron after it has been inserted to the list.
      /// </summary>
      /// <param name="neuron">The item.</param>
      private void UpdateNeuronAfterInsert(MindMapNeuron neuron)
      {
         if (neuron != null)                                                                      //we are adding a neuron, so check all it's to and from links for items that are already depicted on the map.
         {
            List<ulong> iExisting = (from i in this 
                                     where i is MindMapNeuron 
                                     select ((MindMapNeuron)i).ItemID).ToList();
            Owner.ShowExistingIn(neuron, iExisting, false);
            Owner.ShowExistingOut(neuron, iExisting, false);
            if (neuron is MindMapCluster)
               ((MindMapCluster)neuron).LoadChildren();                                           //if it's a cluster, we also need to search all the mindmap neurons that are a child of the cluster.                             
            var iClusters = from i in this 
                            where i is MindMapCluster 
                            select (MindMapCluster)i;
            neuron.AssignToClusters(iClusters);                                                   //must also check if the newly added item belongs to any clusters
         }
      }


      private void ShowError(string msg)
      {
         MessageBox.Show(msg, "Invalid operation", MessageBoxButton.OK, MessageBoxImage.Error);
         Log.LogError("MindMapItemCollection.InsertItem", msg);
      }

      /// <summary>
      /// must make certain that links are at the back.
      /// </summary>
      /// <param name="oldIndex"></param>
      /// <param name="newIndex"></param>
      protected override void MoveItem(int oldIndex, int newIndex)
      {
         if (this[oldIndex] is MindMapLink)
         {
            while (newIndex < Count - 1 && !(this[newIndex] is MindMapLink))                           //move the index up untill we found something ok.
               newIndex++;
         }
         base.MoveItem(oldIndex, newIndex);
      }

      /// <summary>
      /// check if we are removing a neuron, if so remove all it's links as well.
      /// If we are removing a link, let it know, so it can properly clean up an events or other.
      /// </summary>
      /// <param name="index"></param>
      protected override void RemoveItem(int index)
      {
         MindMapItem iToRemove = this[index];
         MindMapNeuron iNeuron = iToRemove as MindMapNeuron;
         if (iNeuron != null)                                                                      //need to remove all the links that this point refers to.
         {
            for (int i = 0; i < Count; i++)
            {
               MindMapLink iItem = this[i] as MindMapLink;
               if (iItem != null && (iItem.To == iNeuron.ItemID || iItem.From == iNeuron.ItemID))
                  RemoveAt(i);
               if (iNeuron.ChildCount > 0)                                                         //if this item belongs to a mindmap cluster, we also need to brake that relationship.
               {
                  MindMapCluster iCluster = this[i] as MindMapCluster;
                  if (iCluster != null)
                     iCluster.Children.Remove(iNeuron);
               }
            }
         }
         else
         {
            MindMapLink ilink = iToRemove as MindMapLink;
            if (ilink != null)
               ilink.DisconnectLink();
         }
         base.RemoveItem(index);                                                                   //this needs to be done after all links are removed for the undo system (this reverses the action, so the links must be removed before the neuron, so that they are recreated after it.
      }

      ///// <summary>
      ///// Makes certain that the specified link is stored after the item.
      ///// </summary>
      ///// <remarks>
      ///// This doesn't generate any undo data cause this action gets repeated on every change and should not be undoable (if this is done,
      ///// it doubles the amount of undo data each time).
      ///// </remarks>
      ///// <param name="link">The link.</param>
      ///// <param name="item">The item.</param>
      //internal void MoveLinkAfter(MindMapLink link, MindMapNeuron item)
      //{
      //   int iIndex = IndexOf(link);
      //   if (iIndex > -1)
      //      base.MoveItemDirect(iIndex, Count - 1);
      //   else
      //      throw new ArgumentOutOfRangeException("link");
      //}
   }
}
