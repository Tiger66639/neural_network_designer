﻿using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// A <see cref="MindMapItem"/> that stores position info.
   /// </summary>
   public class PositionedMindMapItem: MindMapItem
   {

      #region Fields

      double fY;
      double fX;

      double fWidth;
      double fHeight; 

      #endregion


      #region Prop
      #region X

      /// <summary>
      /// Gets/sets the horizontal offset of the item on the graph.
      /// </summary>
      /// <remarks>
      /// virtual so that <see cref="MindMapCluster"/> can check and change val as desired, depending on children.
      /// </remarks>
      public virtual double X
      {
         get
         {
            return fX;
         }
         set
         {
            if (fX != value)
            {
               if (value < 0)
                  value = 0;
               if (Owner != null && value + Width > Owner.Width)
                  Owner.Width = value + Width + Owner.WidthOverhead;
               OnPropertyChanging("X", fX, value);
               fX = value;
               OnPropertyChanged("X");
            }
         }
      }

      #endregion

      #region Y

      /// <summary>
      /// Gets/sets the vertical offset of the item on the graph.
      /// </summary>
      /// <remarks>
      /// virtual so that <see cref="MindMapCluster"/> can check and change val as desired, depending on children.
      /// </remarks>
      public virtual double Y
      {
         get
         {
            return fY;
         }
         set
         {
            if (fY != value)
            {
               if (value < 0)
                  value = 0;
               if (Owner != null && value + Height > Owner.Height)
                  Owner.Height = value + Height + Owner.HeightOverhead;
               OnPropertyChanging("Y", fY, value);
               fY = value;
               OnPropertyChanged("Y");
            }
         }
      }

      #endregion

      #region Width

      /// <summary>
      /// Gets/sets the width of the item.
      /// </summary> 
      /// <remarks>
      /// virtual so that <see cref="MindMapCluster"/> can check and change val as desired, depending on children.
      /// </remarks>
      /// <value>The width.</value>
      public virtual double Width
      {
         get
         {
            return fWidth;
         }
         set
         {
            if (fWidth != value)
            {
               if (value <= 4)                                                            //make certain we can't make it to small.
                  value = 4;
               if (Owner != null && value + X > Owner.Width - Owner.WidthOverhead)        //take the extra added empty width into account
                  Owner.Width = value + X + Owner.WidthOverhead;
               OnPropertyChanging("Width", fWidth, value);
               fWidth = value;
               OnPropertyChanged("Width");
            }
         }
      }

      #endregion


      #region Height

      /// <summary>
      /// Gets/sets the height of the item.
      /// </summary>
      /// <remarks>
      /// virtual so that <see cref="MindMapCluster"/> can check and change val as desired, depending on children.
      /// </remarks>
      public virtual double Height
      {
         get
         {
            return fHeight;
         }
         set
         {
            if (fHeight != value)
            {
               if (value <= 4)                                                            //make certain we can't make it to small.
                  value = 4;
               if (Owner != null && value + Y > Owner.Height - Owner.HeightOverhead)
                  Owner.Height = value + Y + Owner.HeightOverhead;
               OnPropertyChanging("Height", fHeight, value);
               fHeight = value;
               OnPropertyChanged("Height");
            }
         }
      }

      #endregion

      /// <summary>
      /// Gets the owning <see cref="MindMap"/> of the item.
      /// </summary>
      /// <remarks>
      /// we override cause when the owner is set, we need to update it's width and height.
      /// </remarks>
      [XmlIgnore]
      public override MindMap Owner
      {
         get
         {
            return base.Owner;
         }
         set
         {
            base.Owner = value;
            if (value != null)
            {
               if (Height + Y > value.Height)
                  value.Height = Height + Y;
               if (Width + X > value.Width)
                  value.Width = Width + X;
            }
         }
      }

      /// <returns>A deep copy of this object.</returns>
      public override MindMapItem Duplicate()
      {
         PositionedMindMapItem iRes = (PositionedMindMapItem)base.Duplicate();
         iRes.X = X;
         iRes.Y = Y;
         iRes.Width = Width;
         iRes.Height = Height;
         return iRes;
      }

      #endregion


      /// <summary>
      /// Sets the position in 1 call by simulating a drag operation..
      /// </summary>
      /// <remarks>
      /// <para>
      /// Normally, a drag operation isn't different from setting the x and y props.  However, when
      /// a cluster is dragged, it's children should also be moved.  This is done through this call.
      /// </para>
      /// This is a virtual function so that descendents can change some of the behaviour a drop operation
      /// does on a positioned mindmap item.
      /// </remarks>
      /// <param name="x">The x pos</param>
      /// <param name="y">The y pos.</param>
      public virtual void SetPositionFromDrag(double x, double y)
      {
         X = x;
         Y = y;
      }
   }
}
