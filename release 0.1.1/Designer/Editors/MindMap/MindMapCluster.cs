﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// A <see cref="MindMapNeuron"/> specific for clusters.  Provides some extra functionality to handle children.
   /// </summary>
   public class MindMapCluster: MindMapNeuron, IWeakEventListener
   {
      #region Fields
      object fChildren;
      ObservableCollection<ulong> fCircularRefs = new ObservableCollection<ulong>();
      bool fIsDragging = false;                                                                                      //a switch used by SetPositionFromDrag and X and Y to skip adjusting x,y when they are changed.

      /// <summary>
      /// Determins if items are automatically added to the cluster when they are not on the mindmap or not.
      /// </summary>
      /// <remarks>
      /// Should be configurable from options window
      /// </remarks>
      public static bool AutoAddItemsToMindMapCluter = true;                        

      #endregion

      #region ctor-dtor
      /// <summary>
      /// Initializes a new instance of the <see cref="MindMapCluster"/> class.
      /// </summary>
      /// <param name="item">The item.</param>
      internal MindMapCluster(NeuronCluster item)
         : base(item)
      {
         Create();
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="MindMapCluster"/> class.
      /// </summary>
      /// <remarks>
      /// Required for serialization, should be fixed later on
      /// </remarks>
      public MindMapCluster()
      {
         Create();
      }

      /// <summary>
      /// Creates this instance: general code for all constructors.
      /// </summary>
      private void Create()
      {
         NeuronListChangedEventManager.AddListener(Brain.Current, this);
         NeuronChangedEventManager.AddListener(Brain.Current, this);
      }

      /// <summary>
      /// Releases unmanaged resources and performs other cleanup operations before the
      /// <see cref="MindMapCluster"/> is reclaimed by garbage collection.
      /// </summary>
      ~ MindMapCluster()
      {
         if (Environment.HasShutdownStarted == false)
         {
            NeuronListChangedEventManager.RemoveListener(Brain.Current, this);
            NeuronChangedEventManager.RemoveListener(Brain.Current, this);
         }
      }

      #endregion

      #region Children

      /// <summary>
      /// Gets/sets the list of children that are displayed in this neuron.
      /// </summary>
      /// <remarks>
      /// <para>
      /// This prop is formatted with a get + as list-ulong- for streaming. This way,
      /// we can use the list as a lookup.
      /// Warning: this should be changed, so that the list can't be modified from outside at runtime (only on load of the data)
      /// </para>
      /// <para>
      /// We don't need to make an observable list from this cause the children are never really managed by this object, only referenced so
      /// we can monitor changes in them -> however, we need to access this list of child MindMapNeurons far to often, so best to keep a
      /// sub list.
      /// </para>
      /// </remarks>
      public NeuronIDCollection ChildrenIds
      {
         get
         {
            if (fChildren is NeuronIDCollection)
               return (NeuronIDCollection)fChildren;
            else
            {
               var iList = from i in Children select i.ItemID;
               return new NeuronIDCollection(iList.ToList());
            }
         }
         set 
         {
            if (fChildren == null)
               fChildren = value;                                                                           //we store this temporarely, so the first time that the children are accessed, the values are copied.  This is done cause the Owner prop is not yet set when the object is loading from xmla
            else
               throw new InvalidOperationException("object already loaded, can't set childrenId's.");
         }
      }

      #endregion

      
      #region Children

      /// <summary>
      /// Gets the list of children assigned to this cluster.
      /// </summary>
      [XmlIgnore]
      public ObservableCollection<MindMapNeuron> Children
      {
         get 
         {
            if (fChildren is NeuronIDCollection && Owner != null)
            {
               PrepareChildren();
            }
            else 
               if (fChildren == null)
                  fChildren = new ObservableCollection<MindMapNeuron>();                                                //if the object is freshly created, and not loaded from disk, we need to init the list
            return (ObservableCollection<MindMapNeuron>)fChildren; 
         }
      }

      #endregion

      #region CircularRefs

      /// <summary>
      /// Gets the list of circular references found in this neuron.
      /// </summary>
      /// <remarks>
      /// This is not persisted cause this situation can change during storage, so it best to recalculate.
      /// </remarks>
      [XmlIgnore]
      public ObservableCollection<ulong> CircularRefs
      {
         get { return fCircularRefs; }
      }

      #endregion

      #region Item
      /// <summary>
      /// Gets/sets the neuron that this mind map item represents.
      /// </summary>
      /// <remarks>
      /// Checks if the item being assigned, is a Cluster.
      /// </remarks>
      /// <value></value>
      [XmlIgnore]
      public override Neuron Item
      {
         get
         {
            return base.Item;
         }
         set
         {
            if (value is NeuronCluster || value == null)
               base.Item = value;
            else
               throw new InvalidOperationException("NeuronCluster expected.");
         }
      } 
      #endregion

      #region Cluster

      /// <summary>
      /// Gets/sets the cluster, same as <see cref="MindMapCluster.Item"/>, but with c
      /// </summary>
      [XmlIgnore]
      public NeuronCluster Cluster
      {
         get
         {
            return (NeuronCluster)base.Item;
         }
         set
         {
            base.Item = value;
         }
      }

      #endregion

      #region Meaning

      /// <summary>
      /// Gets/sets the meaning that is assigned to the cluster.
      /// </summary>
      public ulong Meaning
      {
         get
         {
            return Cluster.Meaning;
         }
         set
         {
            if (Cluster.Meaning != value)
            {
               OnPropertyChanging("Meaning", Cluster.Meaning, value);
               Cluster.Meaning = value;
               OnPropertyChanged("Meaning");
            }
         }
      }

      #endregion

      #region X
      /// <summary>
      /// Gets/sets the horizontal offset of the item on the graph.
      /// </summary>
      /// <value></value>
      /// <remarks>
      /// virtual so that <see cref="MindMapCluster"/> can check and change val as desired, depending on children.
      /// </remarks>
      public override double X
      {
         get
         {
            return base.X;
         }
         set
         {
            if (fIsDragging == false && fChildren != null)              // we also check for fChildren != null cause this tells us if the object is loading from xml or not.  when fChildren == null -> still loading, so don't adjust anything.
            {
               foreach (MindMapNeuron i in Children)
               {
                  if (i.X < value)                                       //if the new value is to small, recalculate the max allowed change.
                     value = i.X;
               }
            }
            base.X = value;
         }
      } 
      #endregion

      #region Y
      /// <summary>
      /// Gets/sets the vertical offset of the item on the graph.
      /// </summary>
      /// <value></value>
      /// <remarks>
      /// virtual so that <see cref="MindMapCluster"/> can check and change val as desired, depending on children.
      /// </remarks>
      public override double Y
      {
         get
         {
            return base.Y;
         }
         set
         {
            if (fIsDragging == false && fChildren != null)              // we also check for fChildren != null cause this tells us if the object is loading from xml or not.  when fChildren == null -> still loading, so don't adjust anything.
            {
               foreach (MindMapNeuron i in Children)
               {
                  if (i.Y < value)                                       //if the new value is to small, recalculate the max allowed change.
                     value = i.Y;
               }
            }
            base.Y = value;
         }
      } 
      #endregion

      #region Height
      /// <summary>
      /// Gets/sets the height of the item.
      /// </summary>
      /// <value></value>
      /// <remarks>
      /// virtual so that <see cref="MindMapCluster"/> can check and change val as desired, depending on children.
      /// </remarks>
      public override double Height
      {
         get
         {
            return base.Height;
         }
         set
         {
            if (fIsDragging == false && fChildren != null)              // we also check for fChildren != null cause this tells us if the object is loading from xml or not.  when fChildren == null -> still loading, so don't adjust anything.
            {
               foreach (MindMapNeuron i in Children)
               {
                  if (i.Y + i.Height > Y + value)                                       //if the new value is to small, recalculate the max allowed change.
                     value = (i.Y + i.Height) - Y;
               }
            }
            base.Height = value;
         }
      } 
      #endregion

      #region Width
      /// <summary>
      /// Gets/sets the width of the item.
      /// </summary>
      /// <value>The width.</value>
      /// <remarks>
      /// virtual so that <see cref="MindMapCluster"/> can check and change val as desired, depending on children.
      /// </remarks>
      public override double Width
      {
         get
         {
            return base.Width;
         }
         set
         {
            if (fIsDragging == false && fChildren != null)              // we also check for fChildren != null cause this tells us if the object is loading from xml or not.  when fChildren == null -> still loading, so don't adjust anything.
            {
               foreach (MindMapNeuron i in Children)
               {
                  if (i.X + i.Width > X + value)                                       //if the new value is to small, recalculate the max allowed change.
                     value = (i.X + i.Width) - X;
               }
            }
            base.Width = value;
         }
      } 
      #endregion

      #region functions

      /// <summary>
      /// Sets the position in 1 call by simulating a drag operation..
      /// </summary>
      /// <param name="x">The x pos</param>
      /// <param name="y">The y pos.</param>
      /// <remarks>
      /// 	<para>
      /// Normally, a drag operation isn't different from setting the x and y props.  However, when
      /// a cluster is dragged, it's children should also be moved.  This is done through this call.
      /// </para>
      /// This is a virtual function so that descendents can change some of the behaviour a drop operation
      /// does on a positioned mindmap item.
      /// </remarks>
      public override void SetPositionFromDrag(double x, double y)
      {

         fIsDragging = true;
         try
         {
            double iXDiv = x - X;
            double iYDiv = y - Y;
            foreach (MindMapNeuron i in Children)
               i.SetPositionFromDrag(i.X + iXDiv, i.Y + iYDiv);
            base.SetPositionFromDrag(x, y);
         }
         finally
         {
            fIsDragging = false;
         }
      }

      /// <summary>
      /// Loads the children.
      /// </summary>
      public void LoadChildren()
      {
         List<MindMapNeuron> iItems = (from i in Owner.Items
                                       where i is MindMapNeuron
                                       select (MindMapNeuron)i).ToList();
         List<Neuron> iChildren;
         using (ChildrenAccessor iList = Cluster.Children)
            iChildren = iList.ConvertTo<Neuron>();                    //we need to have a list cause we need to have the count.
         List<MindMapNeuron> iExistingMM = (from i in iItems
                                            where iChildren.Contains(i.Item) == true
                                            select i).ToList();
         if (AutoAddItemsToMindMapCluter == false)
         {
            Owner.ShowChildren(this, null, iExistingMM);
            foreach (MindMapNeuron i in iExistingMM)
            {
               Children.Add(i);
               MonditorChild(i);
            }
         }
         else
         {
            List<Neuron> iAllNeurons = (from i in iItems select i.Item).ToList();         //just a helper for the next query.
            List<Neuron> iListToAdd = (from i in iChildren                                //need to calculate all the neurons that need to be created.
                                       where iAllNeurons.Contains(i) == false 
                                       select i).ToList();
            Owner.ShowChildren(this, iListToAdd, iExistingMM);
            foreach (MindMapNeuron i in iExistingMM)                                         //don't need to recalculate this list, newly created items are automatically monitored.
            {
               Children.Add(i);
               MonditorChild(i);
            }
         }
      }

      /// <summary>
      /// Creates a new mindmap item for the specified neuron.
      /// </summary>
      /// <param name="neuron">The neuron.</param>
      /// <returns>a new mindMapNeuron/Cluster.</returns>
      private MindMapNeuron CreateNewFor(Neuron neuron)
      {
         MindMapNeuron iNew = MindMapNeuron.CreateFor(neuron);
         iNew.X = X + 4 + (Children.Count * 44);                        //width of new item = with of this + border + nr of children already added + spacing between each.
         iNew.Width = 40;
         iNew.Height = 30;
         iNew.Y = Y;                        
         return iNew;
      }

      /// <summary>
      /// Called when a property of this cluster is changed (should be already checked that it is for this cluster).
      /// </summary>
      /// <remarks>
      /// Updates the Meaning.
      /// </remarks>
      /// <param name="sender">The sender.</param>
      /// <param name="e">The <see cref="NeuralNetworkDesigne.HAB.NeuronPropChangedEventArgs"/> instance containing the event data.</param>
      void PropChanged(object sender, NeuronPropChangedEventArgs e)
      {
         OnPropertyChanged("Meaning");
      }

      /// <summary>
      /// Called when the list of Children are changed.
      /// </summary>
      /// <remarks>
      /// We need to filter, make certain that the event was for the cluster that we ar wrapping.
      /// </remarks>
      /// <param name="sender">The sender.</param>
      /// <param name="e">The <see cref="NeuralNetworkDesigne.HAB.NeuronListChangedEventArgs"/> instance containing the event data.</param>
      void ChildrenChanged(object sender, NeuronListChangedEventArgs e)
      {
         if (e.IsListFrom(Cluster) == true)
         {
            switch (e.Action)
            {
               case NeuronListChangeAction.Insert:
                  MonitorChild(e.Item);
                  break;
               case NeuronListChangeAction.Remove:
                  UnMonitorChild(e.Item);
                  break;
               default:
                  break;
            }
         }
      }

      /// <summary>
      /// Prepares the children after a load from stream.
      /// </summary>
      /// <remarks>
      /// Throws an error if called at other time than after load
      /// </remarks>
      internal void PrepareChildren()
      {
         NeuronIDCollection iChildren = (NeuronIDCollection)fChildren;                                            //this will fail if not just loaded from xml file.
         fChildren = new ObservableCollection<MindMapNeuron>();
         var iContent = from i in Owner.Items
                        where i is MindMapNeuron && iChildren.Contains(((MindMapNeuron)i).ItemID) == true
                        select (MindMapNeuron)i;
         foreach (MindMapNeuron i in iContent)
         {
            ((ObservableCollection<MindMapNeuron>)fChildren).Add(i);
            MonditorChild(i);
         }
      }

      /// <summary>
      /// Stops montiroing the child.
      /// </summary>
      /// <param name="neuron">The neuron.</param>
      private void UnMonitorChild(Neuron neuron)
      {
         MindMapNeuron iFound = (from i in Children where i.Item == neuron select i).FirstOrDefault();
         if (iFound != null)
         {
            iFound.ChildCount--;
            if (iFound != null)
            {
               iFound.PropertyChanged -= new PropertyChangedEventHandler(Child_PropertyChanged);
               if (iFound is MindMapCluster)                                                                      //make certain any circular refs are broken.
                  ((MindMapCluster)iFound).fCircularRefs.Remove(ItemID);
            }
            Children.Remove(iFound);
            fCircularRefs.Remove(neuron.ID);                                                                      //also make certain that any circular ref is broken.
         }
      }

      /// <summary>
      /// starts Monitoring the child.
      /// </summary>
      /// <param name="neuron">The neuron.</param>
      private void MonitorChild(Neuron neuron)
      {
         MindMapNeuron iFound = (from i in Owner.Items
                                 where i is MindMapNeuron && ((MindMapNeuron)i).Item == neuron
                                 select (MindMapNeuron)i).FirstOrDefault();
         if (iFound == null && AutoAddItemsToMindMapCluter == true)
         {
            iFound = CreateNewFor(neuron);
            Owner.Items.Add(iFound);
         }
         if (iFound != null)
         {                                                  //we only add, if there is a mindmap item found.
            Children.Add(iFound);
            MonditorChild(iFound);
         }

      }

      /// <summary>
      /// starts Monitoring the child.
      /// </summary>
      /// <remarks>
      /// Also checks for circular references.
      /// </remarks>
      /// <param name="child">The child.</param>
      internal void MonditorChild(MindMapNeuron child)
      {
         AdjustRectForChild(child);
         child.PropertyChanged += new PropertyChangedEventHandler(Child_PropertyChanged);
         child.ChildCount++;

         NeuronCluster iCluster = child.Item as NeuronCluster;
         if (iCluster != null && iCluster.Children.Contains(Item) == true)
         {
            if (fCircularRefs.Contains(iCluster.ID) == false)
            {
               fCircularRefs.Add(iCluster.ID);
               ((MindMapCluster)child).CircularRefs.Add(this.ItemID);                                    //if child.Item is NeuronCluster, child must be MindMapCluster
            }
         }
      }

      /// <summary>
      /// Adjusts the rect of this control so that the child fits into it + ZIndex is adjusted.
      /// </summary>
      /// <param name="item">The item we need to adjust to.</param>
      private void AdjustRectForChild(MindMapNeuron item)
      {
         if (X > item.X)
            X = item.X;
         double iItemVal = item.X + item.Width;
         double iVal = X + Width;
         if (iVal < iItemVal)
            Width += iItemVal - iVal;
         if (Y > item.Y)
            Y = item.Y;
         iItemVal = item.Y + item.Height;
         iVal = Y + Height;
         if (iVal < iItemVal)
            Height += iItemVal - iVal;
         Owner.MoveAfter(this, item);
      }

      /// <summary>
      /// Handles the PropertyChanged event of the Child control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
      void Child_PropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         if (fIsDragging == false && Owner != null)                                                         //we also check for owner cause this could get called because the item is being removed because of an undo, in which case this gets triggered because MindMapNeuron.Shape is being reset to null.
            AdjustRectForChild((MindMapNeuron)sender);
      }

      #endregion

      #region IWeakEventListener Members

      /// <summary>
      /// Receives events from the centralized event manager.
      /// </summary>
      /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
      /// <param name="sender">Object that originated the event.</param>
      /// <param name="e">Event data.</param>
      /// <returns>
      /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
      /// </returns>
      public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(NeuronListChangedEventManager))
         {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<object, NeuronListChangedEventArgs>(ChildrenChanged), sender, (NeuronListChangedEventArgs)e);
            return true;
         }
         else if (managerType == typeof(NeuronChangedEventManager))
         {
            if(e is NeuronPropChangedEventArgs && sender == Cluster)
               App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<object, NeuronPropChangedEventArgs>(PropChanged), sender, (NeuronPropChangedEventArgs)e);
            return true;
         }
         else
            return false;
      }

      

      #endregion


      /// <summary>
      /// Replaces the child.
      /// </summary>
      /// <remarks>
      /// When the MindMap representation of a neuron is changed, and this neuron is a child of this cluster, we need to update
      /// the mindMap representation of the child.
      /// </remarks>
      /// <param name="oldValue">The old child.</param>
      /// <param name="newValue">The new child.</param>
      internal void ReplaceChild(MindMapNeuron oldValue, MindMapNeuron newValue)
      {
         int iIndex = Children.IndexOf(oldValue);
         if (iIndex > -1)
         {
            oldValue.PropertyChanged -= new PropertyChangedEventHandler(Child_PropertyChanged);
            if (oldValue is MindMapCluster && !(newValue is MindMapCluster))                             //if the old item was a cluster, it could be there was a circular ref, if the new item is no longer a cluster, this is solved.
               fCircularRefs.Remove(newValue.ItemID);
            if (newValue != null)
            {
               newValue.PropertyChanged += new PropertyChangedEventHandler(Child_PropertyChanged);
               Children[iIndex] = newValue;
            }
            else
               Children.Remove(oldValue);
         }
      }
   }
}
