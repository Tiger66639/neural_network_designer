﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using NeuralNetworkDesigne.Data;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Allows the user to select all the visible links for a single neuron on a mindmap. 
   /// </summary>
   public partial class DlgSelectLinks : Window
   {
      #region Internal types

      /// <summary>
      /// Helper class for displaying all the links.
      /// </summary>
      public class LinkItem: ObservableObject
      {
         public LinkItem(bool isVisible, Link link)
         {
            fIsVisible = isVisible;
            fOriginalIsVisible = isVisible;
            Link = link;
         }

         #region fields
         bool fIsVisible;
         bool fOriginalIsVisible;
         Neuron fItem;
         NeuronData fItemData;
         Link fLink;
         #endregion


         
         #region Item

         /// <summary>
         /// Gets/sets the neuron that this link points to.
         /// </summary>
         public Neuron Item
         {
            get
            {
               return fItem;
            }
            set
            {
               if (fItem != value)
               {
                  fItem = value;
                  ItemData = BrainData.Current.NeuronInfo[value.ID];
               }
            }
         }

         #endregion

         
         #region ItemData

         /// <summary>
         /// Gets the extra data for the neuron that the link points to.
         /// </summary>
         public NeuronData  ItemData
         {
            get { return fItemData; }
            internal set { fItemData = value; }
         }

         #endregion


         #region IsVisible

         /// <summary>
         /// Gets/sets if this link should be displayed or not.
         /// </summary>
         public bool IsVisible
         {
            get
            {
               return fIsVisible;
            }
            set
            {
               fIsVisible = value;
            }
         }

         #endregion

         
         #region OriginalIsVisible

         /// <summary>
         /// Gets if the link is visible on the mindmap before any changes are applied.
         /// This is used to check if the value actually changed.
         /// </summary>
         public bool OriginalIsVisible
         {
            get { return fOriginalIsVisible; }
            internal set { fOriginalIsVisible = value; }
         }

         #endregion

         
         #region Link

         /// <summary>
         /// Gets the link that this objects provides a wrapper for.
         /// </summary>
         public Link Link
         {
            get { return fLink; }
            internal set { fLink = value; }
         }

         #endregion
      }

      #endregion

      #region Fields

      List<LinkItem> fIncomming = new List<LinkItem>();
      List<LinkItem> fOutgoing = new List<LinkItem>();
      MindMapNeuron fNeuron;

      #endregion

      public DlgSelectLinks(MindMapNeuron neuron)
      {
         fNeuron = neuron;
         using (LinksAccessor iLnksIn = neuron.Item.LinksIn)
         {
            foreach (Link i in iLnksIn.Items)
               CreateLinkItem(i, true);
         }
         using (LinksAccessor iLnksOut = neuron.Item.LinksOut)
         {
            foreach (Link i in iLnksOut.Items)
               CreateLinkItem(i, false);
         }
         Title = Title + neuron.NeuronInfo.DisplayTitle;

         InitializeComponent();
      }

      /// <summary>
      /// Creates a sinlge link item and adds it to the list.
      /// </summary>
      /// <param name="item">the link for which to create a wrapper.</param>
      /// <param name="list">The list to put the wrapper in.</param>
      private void CreateLinkItem(Link item, bool asIncomming)
      {
         LinkItem iNew;
         var iRes = (from iItem in fNeuron.Owner.Items
                     where iItem is MindMapLink && ((MindMapLink)iItem).Link == item
                     select iItem).FirstOrDefault();
         iNew = new LinkItem(iRes != null, item);
         if (asIncomming == true)
         {
            iNew.Item = item.From;
            fIncomming.Add(iNew);
         }
         else
         {
            iNew.Item = item.To;
            fOutgoing.Add(iNew);
         }
      }

      
      #region Incomming

      /// <summary>
      /// Gets the list of incomming links for the neuron in an appropriate editable format for the UI.
      /// </summary>
      public List<LinkItem> Incomming
      {
         get { return fIncomming; }
         internal set { fIncomming = value; }
      }

      #endregion

      
      #region Outgoing

      /// <summary>
      /// Gets/sets the list of outgoing links for the neuron in an appropriate editable format for the UI.
      /// </summary>
      public List<LinkItem> Outgoing
      {
         get
         {
            return fOutgoing;
         }
         set
         {
            fOutgoing = value;
         }
      }

      #endregion

      private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
      {
         //if the user selected ok, update all the links.
         if (DialogResult == true)
         {
            WindowMain.UndoStore.BeginUndoGroup(true);                                                        //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
            try
            {
               ProcessList(fIncomming);
               ProcessList(fOutgoing);
            }
            finally
            {
               WindowMain.UndoStore.EndUndoGroup();
            }
         }
      }

      /// <summary>
      /// Processes the list of links, checks if they are selected or not and adds/removes mindmap items accordingly.
      /// </summary>
      /// <param name="list">The list.</param>
      private void ProcessList(List<LinkItem> list)
      {
         double iOffset = 0.0;                                                                  //stores the offset to use for putting new neurons on the ui.
         foreach (LinkItem iLink in list)
         {
            if (iLink.OriginalIsVisible != iLink.IsVisible)
            {
               if (iLink.IsVisible == false)
               {
                  int iToRemove = -1;
                  for (int i = 0; i < fNeuron.Owner.Items.Count; i++)
                  {
                     MindMapLink iItem = fNeuron.Owner.Items[i] as MindMapLink;
                     if (iItem != null &&
                         ((list == fIncomming && iItem.To == fNeuron.ItemID && iItem.From == iLink.Item.ID) ||
                           (list == fOutgoing && iItem.From == fNeuron.ItemID && iItem.To == iLink.Item.ID))
                        )
                     {
                        iToRemove = i;
                        break;
                     }
                  }
                  if (iToRemove > -1)
                     fNeuron.Owner.Items.RemoveAt(iToRemove);
               }
               else
                  fNeuron.Owner.ShowLinkBetween(fNeuron, iLink.Link, list == fIncomming, ref iOffset);
            }
         }
      }


      private void OnClickOk(object sender, RoutedEventArgs e)
      {
         DialogResult = true;
      }


   }
}
