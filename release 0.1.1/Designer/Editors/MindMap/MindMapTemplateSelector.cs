﻿using System.Windows;
using System.Windows.Controls;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Tries to find the correct data template for the mind map.
   /// </summary>
   /// <remarks>
   /// if the item is a mindmap - link, it returns the template with key: "MindMaplink"
   ///                          - note, it returns the template with key: "MindMapNote"
   /// For neurons, it first searches for a template with as key, the id (as string), if this is not found,
   /// it checks if it is a cluster, if so, it returns "NeuronCluster"
   /// for text neurons, it returns: "TextNeuron" otherwise, "Neuron".
   /// </remarks>
   public class MindMapTemplateSelector : DataTemplateSelector
   {
      public override DataTemplate SelectTemplate(object item, DependencyObject container)
      {
         FrameworkElement iCont = container as FrameworkElement;
         if (iCont != null)
         {
            if (item is MindMapNeuron)
            {
               Neuron iNeuron = ((MindMapNeuron)item).Item;
               DataTemplate iFound = iCont.TryFindResource(iNeuron.ID.ToString()) as DataTemplate;
               if (iFound == null)
               {
                  if(iNeuron is NeuronCluster)
                     return iCont.TryFindResource("NeuronCluster") as DataTemplate;
                  else if(iNeuron is TextNeuron)
                     return iCont.TryFindResource("TextNeuron") as DataTemplate;
                  else
                     return iCont.TryFindResource("Neuron") as DataTemplate;
               }
               return iFound;
            }
            else if (item is MindMapNote)
               return iCont.TryFindResource("MindMapNoteTemplate") as DataTemplate;
            else if (item is MindMapLink)
               return iCont.TryFindResource("MindMapLinkTemplate") as DataTemplate;
         }
         return null;
      }
   }
}
