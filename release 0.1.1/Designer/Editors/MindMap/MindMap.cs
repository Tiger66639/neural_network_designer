﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using NeuralNetworkDesigne.Data;
using NeuralNetworkDesigne.UndoSystem;

namespace NeuralNetworkDesigne.HAB.Designer
{

   /// <summary>
   /// Contains all the information to draw a graph of part of the brain.
   /// </summary>
   public class MindMap : EditorBase, IWeakEventListener
   {


      #region Fields

      MindMapItemCollection fItems;
      double fHeight;
      double fHeightOverhead;
      double fWidth;
      double fWidthOverhead;
      double fZoom = 1.0;
      int fUndoStoreActionCount;

      const double fNewNeuronWidth = 40;                                                     //default width for a new neuron.
      const double fLetterLength = 4;                                                        //average length of letter that is used to calculate the width.
      const double fNewNeuronHeight = 25;                                                    //when new neurons are created for showing links or children, this is the default height.

      #endregion

      #region ctor

      public MindMap()
      {
         fItems = new MindMapItemCollection(this);
         LinkChangedEventManager.AddListener(Brain.Current, this);                           //event handlers for keeping sync with the brain.
         NeuronChangedEventManager.AddListener(Brain.Current, this);

         PreviewRedoPerformedEventManager.AddListener(WindowMain.UndoStore, this);           //event handlers for keeping an eye on the undostore.  Need to update the state of the mindmap (so that items are correctly added/removed by the itemcollection).
         PreviewUndoPerformedEventManager.AddListener(WindowMain.UndoStore, this);
         RedoPerformedEventManager.AddListener(WindowMain.UndoStore, this);
         UndoPerformedEventManager.AddListener(WindowMain.UndoStore, this);

      }

      ~MindMap()
      {
         LinkChangedEventManager.RemoveListener(Brain.Current, this);
         NeuronChangedEventManager.RemoveListener(Brain.Current, this);

         PreviewRedoPerformedEventManager.RemoveListener(WindowMain.UndoStore, this);
         PreviewUndoPerformedEventManager.RemoveListener(WindowMain.UndoStore, this);
         RedoPerformedEventManager.RemoveListener(WindowMain.UndoStore, this);
         UndoPerformedEventManager.RemoveListener(WindowMain.UndoStore, this);
      }

      #endregion


      #region Prop


      #region Icon
      /// <summary>
      /// Gets the resource path to the icon that should be used for this editor.  This is usually class specific.
      /// </summary>
      /// <value></value>
      public override string Icon
      {
         get { return "/Images/NewMindMap.png"; }
      }
      #endregion


      #region Items

      /// <summary>
      /// Gets the list of items visialble on this mind map.
      /// </summary>
      public MindMapItemCollection Items
      {
         get { return fItems; }
      }

      #endregion


      #region Height

      /// <summary>
      /// Gets the height of the work area.
      /// </summary>
      /// <remarks>
      /// This is important for scrolling. it is set by the <see cref="MindMapItems"/> whenever
      /// a property is changed.
      /// <para>
      /// We allow an internal setter cause the view must also be able to set this value.
      /// </para>
      /// </remarks>
      [XmlIgnore]
      public double Height
      {
         get
         {
            return fHeight;
         }
         internal set
         {
            fHeight = value;
            OnPropertyChanged("Height");
            OnPropertyChanged("ScrollHeight");
         }
      }

      #endregion

      #region HeightOverhead

      /// <summary>
      /// Gets/sets the extra amount of space that is added after the last item in the mindmap to allow proper editing behind the edge of the mindmap
      /// </summary>
      public double HeightOverhead
      {
         get
         {
            return fHeightOverhead;
         }
         set
         {
            fHeightOverhead = value;
            OnPropertyChanged("Height");
            OnPropertyChanged("ScrollHeight");
         }
      }

      #endregion

      #region Width

      /// <summary>
      /// Gets/sets the widht of the work area.
      /// </summary>
      /// <remarks>
      /// This is important for scrolling. it is set by the <see cref="MindMapItems"/> whenever
      /// a property is changed.
      /// <para>
      /// We allow an internal setter cause the view must also be able to set this value.
      /// </para>
      /// </remarks>
      [XmlIgnore]
      public double Width
      {
         get
         {
            return fWidth;
         }
         internal set
         {
            fWidth = value;
            OnPropertyChanged("Width");
            OnPropertyChanged("ScrollWidth");
         }
      }

      #endregion

      #region WidthOverhead

      /// <summary>
      /// Gets/sets the extra amount of space that is added after the last item in the mindmap to allow proper editing behind the edge of the mindmap
      /// </summary>
      [XmlIgnore]
      public double WidthOverhead
      {
         get
         {
            return fWidthOverhead;
         }
         set
         {
            fWidthOverhead = value;
            OnPropertyChanged("Width");
            OnPropertyChanged("ScrollWidth");
         }
      }

      #endregion

      #region ScrollWidth

      /// <summary>
      /// Gets the width of the work area, adjusted by the zoom value.
      /// </summary>
      /// <remarks>
      /// This is provided so the view doesn't have to use complex bindings for this value.
      /// </remarks>
      [XmlIgnore]
      public double ScrollWidth
      {
         get { return fWidth * fZoom; }
      }

      #endregion

      #region ScrollHeight

      /// <summary>
      /// Gets the width of the work area, adjusted by the zoom value.
      /// </summary>
      /// <remarks>
      /// This is provided so the view doesn't have to use complex bindings for this value.
      /// </remarks>
      [XmlIgnore]
      public double ScrollHeight
      {
         get { return fHeight * fZoom; }
      }

      #endregion


      #region Zoom

      /// <summary>
      /// Gets/sets the zoom factor that should be applied.
      /// </summary>
      public double Zoom
      {
         get
         {
            return fZoom;
         }
         set
         {
            if (fZoom != value)
            {
               fZoom = value;
               OnPropertyChanged("Zoom");
               OnPropertyChanged("ScrollHeight");
               OnPropertyChanged("ScrollWidth");
            }
         }
      }

      #endregion


      #region UndoStoreActionCount
      /// <summary>
      /// keeps track of how many nested undo levels have been encountered.  
      /// </summary>
      /// <remarks>
      /// For each preview event we increase by 1, for each normal (undo or redo) we decrease.
      /// This is required cause we need to know when data is being added from the undo system or
      /// by the user (needs to be treated differently).
      /// </remarks>
      /// <value>The undo store action count.</value>
      private int UndoStoreActionCount
      {
         get
         {
            return fUndoStoreActionCount;
         }
         set
         {
            if (value != fUndoStoreActionCount)
            {
               fUndoStoreActionCount = value;
               if (fUndoStoreActionCount == 0)
                  CurrentState = EditorState.Loaded;
               else
                  CurrentState = EditorState.Undoing;
            }
         }
      } 
      #endregion

      /// <summary>
      /// Gets a title that the description editor can use to display in the header.
      /// </summary>
      /// <value></value>
      public override string DescriptionTitle
      {
         get { return Name + " - Mind map"; }
      }

      #endregion

      #region IXmlSerializable Members

      /// <summary>
      /// Reads the fields/properties of the class.
      /// </summary>
      /// <param name="reader">The reader.</param>
      /// <remarks>
      /// This function is called for each element that is found, so this function should check which element it is
      /// and only read that element accordingly.
      /// </remarks>
      protected override bool ReadXmlInternal(XmlReader reader)
      {
         if (reader.Name == "Zoom")
         {
            fZoom = XmlStore.ReadElement<double>(reader, "Zoom");
            return true;
         }
         else if (reader.Name == "XmlObjectStore")
         {
            XmlSerializer valueSerializer = new XmlSerializer(typeof(XmlObjectStore));
            XmlObjectStore iNode = (XmlObjectStore)valueSerializer.Deserialize(reader);
            MindMapItem value = iNode.Data as MindMapItem;
            if (value != null)
               fItems.Add(value);
            reader.MoveToContent();
            return true;
         }
         else if (reader.Name == "MindMapNote")
         {
            XmlSerializer iNoteSer = new XmlSerializer(typeof(MindMapNote));
            MindMapNote iNode = (MindMapNote)iNoteSer.Deserialize(reader);
            if (iNode != null)
               fItems.Add(iNode);
            return true;
         }
         else if (reader.Name == "MindMapLink")
         {
            XmlSerializer iLinkSer = new XmlSerializer(typeof(MindMapLink));
            MindMapLink iNode = (MindMapLink)iLinkSer.Deserialize(reader);
            if (iNode != null)
               fItems.Add(iNode);
            return true;
         }
         else if (reader.Name == "MindMapNeuron")
         {
            XmlSerializer iNeuronSer = new XmlSerializer(typeof(MindMapNeuron));
            MindMapNeuron iNode = (MindMapNeuron)iNeuronSer.Deserialize(reader);
            if (iNode != null)
               fItems.Add(iNode);
            return true;
         }
         else if (reader.Name == "MindMapCluster")
         {
            XmlSerializer iClusterSer = new XmlSerializer(typeof(MindMapCluster));
            MindMapCluster iNode = (MindMapCluster)iClusterSer.Deserialize(reader);
            if (iNode != null)
               fItems.Add(iNode);
            return true;
         }
         else
            return base.ReadXmlInternal(reader);
      }

      /// <summary>
      /// Reads the XML.
      /// </summary>
      /// <param name="reader">The reader.</param>
      public override void ReadXml(XmlReader reader)
      {
         base.ReadXml(reader);
         var iClusters = from i in Items where i is MindMapCluster select (MindMapCluster)i;
         foreach (MindMapCluster i in iClusters)
            i.PrepareChildren();
      }

      /// <summary>
      /// Converts an object into its XML representation.
      /// </summary>
      /// <param name="writer">The <see cref="T:System.Xml.XmlWriter"/> stream to which the object is serialized.</param>
      public override void WriteXml(XmlWriter writer)
      {
         base.WriteXml(writer);
         XmlStore.WriteElement<double>(writer, "Zoom", Zoom);
         
         XmlSerializer valueSerializer = new XmlSerializer(typeof(XmlObjectStore));
         XmlSerializer iNoteSer = new XmlSerializer(typeof(MindMapNote));
         XmlSerializer iNeuronSer = new XmlSerializer(typeof(MindMapNeuron));
         XmlSerializer iClusterSer = new XmlSerializer(typeof(MindMapCluster));
         XmlSerializer iLinkSer = new XmlSerializer(typeof(MindMapLink));
         var iSorted = from i in Items orderby i.TypeIndex select i;             //we sort to make certain that all the links are at the back, which is required for loading.
         foreach (MindMapItem item in iSorted)
         {
            if (item is MindMapNote)
               iNoteSer.Serialize(writer, item);
            else if (item is MindMapLink)
               iLinkSer.Serialize(writer, item);
            else if (item is MindMapCluster)
               iClusterSer.Serialize(writer, item);
            else if (item is MindMapNeuron)
               iNeuronSer.Serialize(writer, item);
            else
            {
               XmlObjectStore iNode = new XmlObjectStore() { Data = item };
               valueSerializer.Serialize(writer, iNode);
            }
         }
      }

      #endregion

      #region Events handlers

      /// <summary>
      /// Need to update links that are depicted on this mindmap.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      void Current_LinkChanged(object sender, LinkChangedEventArgs e)
      {
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<LinkChangedEventArgs>(InternalLinkChanged), e);         
         
      }

      void InternalLinkChanged(LinkChangedEventArgs e)
      {
         if (e.Action == BrainAction.Removed)
         {
            MindMapLink iLink = (from i in fItems
                                 where i is MindMapLink && ((MindMapLink)i).Link == e.OriginalSource
                                 select (MindMapLink)i).FirstOrDefault();
            if (iLink != null)
               fItems.Remove(iLink);
         }
      }

      /// <summary>
      /// When a neuron is removed, we need to make certain that any mindmap items related to it are also removed.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void Current_NeuronChanged(object sender, NeuronChangedEventArgs e)
      {
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<NeuronChangedEventArgs>(InternalNeuronChanged), e);
      }

      /// <summary>
      /// thread local implementation for <see cref="MindMap.Current_NeuronChanged"/>.
      /// </summary>
      /// <param name="e">The <see cref="NeuralNetworkDesigne.HAB.NeuronChangedEventArgs"/> instance containing the event data.</param>
      void InternalNeuronChanged(NeuronChangedEventArgs e)
      {
         if (e.Action == BrainAction.Removed)
         {
            MindMapNeuron iNeuron = (from i in fItems
                                     where i is MindMapNeuron && ((MindMapNeuron)i).Item == e.OriginalSource
                                     select (MindMapNeuron)i).FirstOrDefault();
            if (iNeuron != null)
               fItems.Remove(iNeuron);
         }
         else if (e.Action == BrainAction.Changed && !(e is NeuronPropChangedEventArgs))
            UpdateItemsForNeuronChange(e);
      }

      /// <summary>
      /// Updates the items in response to a changed neuron.
      /// </summary>
      /// <remarks>
      /// When a neuron is changed, we need to create a new mindmap neuron cause it could have been or could become a cluster,
      /// which uses a different class.
      /// Because of this, we must also update all references to this mindmap neuron in the links and
      /// MindMapClusters.
      /// </remarks>
      /// <param name="e">The <see cref="NeuralNetworkDesigne.HAB.NeuronChangedEventArgs"/> instance containing the event data.</param>
      private void UpdateItemsForNeuronChange(NeuronChangedEventArgs e)
      {
         MindMapNeuron iNeuron = (from i in fItems
                                  where i is MindMapNeuron && ((MindMapNeuron)i).Item == e.OriginalSource
                                  select (MindMapNeuron)i).FirstOrDefault();
         if (iNeuron != null)
         {
            MindMapNeuron iNew = MindMapNeuron.CreateFor(e.NewValue);                           //we need to create a new one because a cluster uses a different class.
            iNew.CopyValues(iNeuron);
            List<MindMapLink> iLinks = (from i in fItems                                        //need to convert to list, cause we will be modifying the list in the loop.
                                        where i is MindMapLink && ((MindMapLink)i).ToMindMapItem == iNeuron
                                        select (MindMapLink)i).ToList();
            foreach (MindMapLink i in iLinks)                                                   //update 'To' part of all links
               i.ToMindMapItem = iNew;
            iLinks = (from i in fItems
                      where i is MindMapLink && ((MindMapLink)i).FromMindMapItem == iNeuron
                      select (MindMapLink)i).ToList();
            foreach (MindMapLink i in iLinks)                                                      //updat 'from' part of all links.
               i.FromMindMapItem = iNew;

            List<MindMapCluster> iClusters = (from i in fItems                                     //again, need to convert, cause we will change the list in the loop.
                                              where i is MindMapCluster                            //note: we don't filter on wether the cluster has the item as child, we simply let all the clusters handle this.  This is because the cluster needs to get the index of the child, which is the same CPU hit as checking for 'contains'.
                                              select (MindMapCluster)i).ToList();
            foreach (MindMapCluster i in iClusters)
               i.ReplaceChild(iNeuron, iNew);

            fItems[fItems.IndexOf(iNeuron)] = iNew;
            iNeuron.Shape = null;                                                                  //nicely release some resources, don't think this is required.
            iNeuron.Item = null;
         }
      }

      #endregion


      #region Helpers
      /// <summary>
      /// Shows the children of the specified neuron.
      /// </summary>
      /// <param name="mindMapCluster">The mind map cluster for which we want to see all the childrens.</param>
      public void ShowChildren(MindMapCluster cluster)
      {
         List<MindMapNeuron> iMMs = (from i in Items                                               //all the mindmap neurons
                                     where i is MindMapNeuron 
                                     select (MindMapNeuron)i).ToList();
         List<Neuron> iChildren;
         using (ChildrenAccessor iList = cluster.Cluster.Children)
            iChildren = iList.ConvertTo<Neuron>();                    //we need to have a list cause we need to have the count.
         List<Neuron> iAllNeurons = (from i in iMMs select i.Item).ToList();

         List<Neuron> iListToAdd = (from i in iChildren where iAllNeurons.Contains(i) == false select i).ToList();
         List<MindMapNeuron> iExistingMM = (from i in iMMs
                                            where iChildren.Contains(i.Item) == true 
                                            select i).ToList();
         ShowChildren(cluster, iListToAdd, iExistingMM);
      }

      internal void ShowChildren(MindMapCluster cluster, List<Neuron> newItems, List<MindMapNeuron> existing)
      {
         double iOffsetX = 16.0;                                                                    //stores the offset to use for putting new neurons on the ui.
         double iOffsetY = 56.0;

         int iTotal = newItems == null ? 0 : newItems.Count;
         iTotal += existing == null ? 0 : existing.Count;
         int iNrRows = (int)Math.Ceiling(Math.Sqrt((double)iTotal));                               //we lay the items out in a square.  To get the nr of rows, there are as many on X as on Y, so use square root.
         int iCols = iNrRows;                                                                     //so we can reassign the value after each row.
         if (newItems != null)
         {
            foreach (Neuron i in newItems)
            {
               MindMapNeuron iRes = CreateNewFor(i, cluster.X + iOffsetX, cluster.Y + iOffsetY);
               iOffsetX += (iRes.Width + 4);
               iCols--;
               if (iCols == 0)
               {
                  iCols = iNrRows;
                  iOffsetY += fNewNeuronHeight + 4;
                  iOffsetX = 20.0;
               }
            }
         }
         if (existing != null)
         {
            foreach (MindMapNeuron i in existing)
            {
               i.X = cluster.X + iOffsetX;
               i.Y = cluster.Y + iOffsetY;
               iOffsetX += (i.Width + 4);
               iCols--;
               if (iCols == 0)
               {
                  iCols = iNrRows;
                  iOffsetY += fNewNeuronHeight + 4;
                  iOffsetX = 20.0;
               }
            }
         }
         cluster.Width += 16.0;                                                                       //we increase the width a bit to create a border.
      }


      /// <summary>
      /// Shows all the clusters that own a neuron on the mindmap.
      /// </summary>
      /// <param name="item">The item for whom to show all the clusters that own it.</param>
      public void ShowOwners(MindMapNeuron item)
      {
         List<NeuronCluster> iExisting = (from i in Items                                                 //all the clusters on the mindmap
                                          where i is MindMapCluster
                                          select ((MindMapCluster)i).Cluster).ToList();
         List<NeuronCluster> iClusters;
         using (NeuronsAccessor iClusteredBy = item.Item.ClusteredBy)
         {
            iClusters = (from i in iClusteredBy.Items                                 //all the clusters to whom the item belongs.
                         select Brain.Current[i] as NeuronCluster).ToList();
            List<NeuronCluster> iToCreate = (from i in iClusters                                            //all the clusters that need to be created.
                                             where iExisting.Contains(i) == false
                                             select i).ToList();
            ShowClustersRound(item, iToCreate);
         }
      }

      /// <summary>
      /// Creates new <see cref="MindMapClusters"/> for all the clusters in the list and arranges them round the item.
      /// </summary>
      /// <param name="item">The item to arrange all the items in the list round.</param>
      /// <param name="list">The list.</param>
      private void ShowClustersRound(MindMapNeuron item, List<NeuronCluster> list)
      {
         double iOffsetX = list.Count * -5;                                                        //we do *10 so that we can go up and down a range with + 10
         double iOffsetY = list.Count * 5;
         foreach (NeuronCluster i in list)
         {
            MindMapNeuron iRes;
            if (i != null)                                                                         //just a sanity check, if we don't do this and the db is corrupted, we could get an error here.
               iRes = CreateNewFor(i, item.X + iOffsetX, item.Y + iOffsetY);
            //For corrupt db checking: but an else statement here
            iOffsetX += 10;
            iOffsetY -= 10;
         }
      }

      /// <summary>
      /// Shows all the links of the specified neuron.
      /// </summary>
      /// <param name="neuron">The neuron.</param>
      public void ShowLinksIn(MindMapNeuron neuron)
      {
         double iOffset = 0.0;                                                                  //stores the offset to use for putting new neurons on the ui.
         using (LinksAccessor iLinksIn = neuron.Item.LinksIn)
         {
            foreach (Link iLink in iLinksIn.Items)
               ShowLinkBetween(neuron, iLink, true, ref iOffset);
         }
      }

      /// <summary>
      /// Shows the outgoing links of the argument
      /// </summary>
      /// <param name="neuron">The neuron.</param>
      public void ShowLinksOut(MindMapNeuron neuron)
      {
         double iOffset = 0.0;                                                                  //stores the offset to use for putting new neurons on the ui.
         using (LinksAccessor iLinksOut = neuron.Item.LinksOut)
         {
            foreach (Link iLink in iLinksOut.Items)
               ShowLinkBetween(neuron, iLink, false, ref iOffset);
         }
      }

      public void ShowLinkBetween(MindMapNeuron from, Link link, bool asIncomming, ref double offset)
      {
         WindowMain.UndoStore.BeginUndoGroup(true);                                                                                 //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
         try
         {
            Neuron iTo;
            if (asIncomming == true)
               iTo = link.From;
            else
               iTo = link.To;
            //first check if the requested neuron is already visible, if not, creating it will automatically create the link.
            MindMapNeuron iFound = (from i in Items
                                    where i is MindMapNeuron && ((MindMapNeuron)i).ItemID == iTo.ID
                                    select (MindMapNeuron)i).FirstOrDefault();
            if (iFound == null)
            {
               MindMapNeuron iRes = CreateNewFor(iTo, from.X + offset, from.Y + (from.Height * 2));
               offset += (iRes.Width + 16);
            }
            else
            {
               MindMapLink iNew = new MindMapLink();
               if (asIncomming == true)
                  iNew.CreateLink(link, iFound, from);
               else
                  iNew.CreateLink(link, from, iFound);
               Items.Add(iNew);
            }
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      }

      /// <summary>
      /// Creates a new correct <see cref="MindMapNeuron"/>/<see cref="MindMapCluster"/> for the neuron to wrap.
      /// </summary>
      /// <param name="toWrap">Item to wrap.</param>
      /// <param name="relativeTo">MindMapItem to which we need to place this item relative to</param>
      /// <param name="offset">The offset.</param>
      /// <returns></returns>
      private MindMapNeuron CreateNewFor(Neuron toWrap, double x, double y)
      {
         MindMapNeuron iRes = MindMapNeuron.CreateFor(toWrap);
         iRes.Y = y;
         iRes.X = x;
         iRes.Width = fNewNeuronWidth + (fLetterLength * iRes.NeuronInfo.DisplayTitle.Length);
         iRes.Height = fNewNeuronHeight;
         Items.Add(iRes);                                                            //don't need to create the link object, this is done automatically when it is added to the list
         return iRes;
      }

      /// <summary>
      /// Shows all the existing incomming relations
      /// </summary>
      /// <param name="existing">The list of neurons that are existing on the mindmap.  This is passed as a param for
      /// optimazation, sometimes showExistingIn and Out is called after each other this is offcourse the same list for both, so
      /// it is best to reuse them.</param>
      /// <param name="checkExistence">True if we should check if any of the links are already visible for the neuron, or all
      /// should be created blindly without checking.</param>
      /// <param name="neuron">The neuron for whom to show the links.</param>
      internal void ShowExistingIn(MindMapNeuron neuron, IList<ulong> existing, bool checkExistence)
      {
         WindowMain.UndoStore.BeginUndoGroup(true);                                                                                 //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
         try
         {
            using (LinksAccessor iLinksAccess = neuron.Item.LinksIn)
            {
               var iLinksIn = from i in iLinksAccess.Items where existing.Contains(i.FromID) == true select i;
               foreach (Link iLink in iLinksIn)
               {
                  if (checkExistence == true)
                  {
                     var iFound = (from i in Items where i is MindMapLink && ((MindMapLink)i).Link == iLink select i).FirstOrDefault();
                     if (iFound != null)                                                                                          //the link already exists, don't try to create it.
                        break;
                  }
                  MindMapLink iNew = new MindMapLink();
                  MindMapNeuron iFrom = (from i in Items
                                         where i is MindMapNeuron && ((MindMapNeuron)i).ItemID == iLink.FromID
                                         select (MindMapNeuron)i).FirstOrDefault();
                  Debug.Assert(iFrom != null);
                  iNew.CreateLink(iLink, iFrom, neuron);
                  Items.Add(iNew);
               }
            }
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      }

      /// <summary>
      /// Shows all the existing outgoing relations
      /// </summary>
      /// <param name="existing">The list of neurons that are existing on the mindmap.  This is passed as a param for
      /// optimazation, sometimes showExistingIn and Out is called after each other this is offcourse the same list for both, so
      /// it is best to reuse them.</param>
      /// <param name="checkExistence">True if we should check if any of the links are already visible for the neuron, or all
      /// should be created blindly without checking.</param>
      /// <param name="neuron">The neuron for whom to show the links.</param>
      internal void ShowExistingOut(MindMapNeuron neuron, IList<ulong> existing, bool checkExistence)
      {
         WindowMain.UndoStore.BeginUndoGroup(true);                                                                                 //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
         try
         {
            using (LinksAccessor iLinksAccess = neuron.Item.LinksOut)
            {
               var iLinksOut = from i in iLinksAccess.Items where existing.Contains(i.ToID) select i;
               foreach (Link iLink in iLinksOut)
               {
                  if (checkExistence == true)
                  {
                     var iFound = (from i in Items where i is MindMapLink && ((MindMapLink)i).Link == iLink select i).FirstOrDefault();
                     if (iFound != null)                                                                                          //the link already exists, don't try to create it.
                        break;
                  }
                  MindMapLink iNew = new MindMapLink();
                  MindMapNeuron iTo = (from i in Items
                                       where i is MindMapNeuron && ((MindMapNeuron)i).ItemID == iLink.ToID
                                       select (MindMapNeuron)i).FirstOrDefault();
                  iNew.CreateLink(iLink, neuron, iTo);
                  Items.Add(iNew);
               }
            }
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      } 
      #endregion



      #region IWeakEventListener Members

      /// <summary>
      /// Receives events from the centralized event manager.
      /// </summary>
      /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
      /// <param name="sender">Object that originated the event.</param>
      /// <param name="e">Event data.</param>
      /// <returns>
      /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
      /// </returns>
      public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(LinkChangedEventManager))
         {
            Current_LinkChanged(sender, (LinkChangedEventArgs)e);
            return true;
         }
         else if (managerType == typeof(NeuronChangedEventManager))
         {
            Current_NeuronChanged(sender, (NeuronChangedEventArgs)e);
            return true;
         }
         else if (managerType == typeof(PreviewRedoPerformedEventManager) || managerType == typeof(PreviewUndoPerformedEventManager))
         {
            UndoStoreActionCount++;
            return true;
         }
         else if (managerType == typeof(RedoPerformedEventManager) || managerType == typeof(UndoPerformedEventManager))
         {
            UndoStoreActionCount--;
            return true;
         }
         else
            return false;
      }


      #endregion

      #region ZIndex manip
      /// <summary>
      /// Moves the specified mindmap items 1 up the ZIndex (visually).
      /// </summary>
      /// <remarks>
      /// warning: modifies the argument list.
      /// </remarks>
      /// <param name="toMove">Items to move. Warning: this list is empty when the function retursn.</param>
      public void MoveUp(List<MindMapItem> toMove)
      {
         while (toMove.Count > 0)
         {
            List<MindMapItem> iCurLevel = (from i in toMove
                                           where i.ZIndex == toMove[0].ZIndex
                                           select i).ToList();                                                    //get all the items at the same level as the first item and move them together by switching with all the items at the new level
            var iPrevOnNewPos = from i in Items
                                where i.ZIndex == toMove[0].ZIndex + 1
                                select i;                                                                         //these are all the items currently at the level of the items we are trying to move, so switch these levels.
            foreach (MindMapItem i in iPrevOnNewPos) i.ZIndex--;
            foreach (MindMapItem i in iCurLevel)
            {
               i.ZIndex++;
               toMove.Remove(i);
            }
         }
      }

      /// <summary>
      /// Moves the specified mindmap items 1 down the ZIndex (visually).
      /// </summary>
      /// <param name="iToMove">The items to move. Warning: this list is empty when the function retursn.</param>
      public void MoveDown(List<MindMapItem> toMove)
      {
         while (toMove.Count > 0)
         {
            List<MindMapItem> iCurLevel = (from i in toMove
                                           where i.ZIndex == toMove[0].ZIndex
                                           select i).ToList();                                                    //get all the items at the same level as the first item and move them together by switching with all the items at the new level
            var iPrevOnNewPos = from i in Items
                                where i.ZIndex == toMove[0].ZIndex - 1
                                select i;                                                                         //these are all the items currently at the level of the items we are trying to move, so switch these levels.
            foreach (MindMapItem i in iPrevOnNewPos) i.ZIndex++;
            foreach (MindMapItem i in iCurLevel)
            {
               i.ZIndex--;
               toMove.Remove(i);
            }
         }
      }

      /// <summary>
      /// Moves the specified mindmap items to the back of the ZIndex (visually).
      /// </summary>
      /// <param name="list">The list</param>
      public void MoveToBack(IList<MindMapItem> list)
      {
         foreach (MindMapItem iSelected in list)
         {
            foreach (MindMapItem iItem in Items)
            {
               if (list.Contains(iItem) == false)
               {
                  if (iItem.ZIndex < iSelected.ZIndex)
                     iItem.ZIndex++;
               }
            }
            iSelected.ZIndex = 0;
         }
      }

      /// <summary>
      /// Moves the specified mindmap items to the front of the ZIndex (visually).
      /// </summary>
      /// <param name="list">The list.</param>
      public void MoveToFront(IList<MindMapItem> list)
      {
         foreach (MindMapItem iSelected in list)
         {
            foreach (MindMapItem iItem in Items)
            {
               if (list.Contains(iItem) == false)
               {
                  if (iItem.ZIndex > iSelected.ZIndex)
                     iItem.ZIndex--;
               }
            }
            iSelected.ZIndex = Items.Count - 1;
         }
      }

      /// <summary>
      /// Moves the specified item down the ZIndex so that it visually behind the other item.
      /// </summary>
      /// <param name="toMove">To mind map item to move.</param>
      /// <param name="after">The mind map after that should be in front of the item that is being moved.</param>
      public void MoveAfter(MindMapItem toMove, MindMapItem after)
      {

         while (toMove.ZIndex >= after.ZIndex)
         {
            List<MindMapItem> iTemp = new List<MindMapItem>() { toMove };                             //we need to remake the list each time, cause the item gets removed in the MoveDown.
            MoveDown(iTemp);
         }
      } 
      #endregion




      #region Clipboard
      protected override void CopyToClipboard(DataObject data)
      {
         throw new NotImplementedException();
      }

      public override bool CanCopyToClipboard()
      {
         throw new NotImplementedException();
      }

      public override bool CanPasteSpecialFromClipboard()
      {
         throw new NotImplementedException();
      }

      public override void PasteSpecialFromClipboard()
      {
         throw new NotImplementedException();
      }



      public override bool CanPasteFromClipboard()
      {
         if (base.CanPasteFromClipboard() == true)
         {
         }
         throw new NotImplementedException();
      }

      public override void PasteFromClipboard()
      {
         throw new NotImplementedException();
      } 
      #endregion

      #region Delete
      public override void Delete()
      {
         throw new NotImplementedException();
      }

      public override bool CanDelete()
      {
         throw new NotImplementedException();
      }

      public override void DeleteSpecial()
      {
         throw new NotImplementedException();
      }

      public override bool CanDeleteSpecial()
      {
         throw new NotImplementedException();
      } 
      #endregion
   }

}
