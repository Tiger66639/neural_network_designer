﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Diagnostics;
using System.Windows;
using System.Xml.Serialization;
using System.Xml;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Defines the way neurons (or branches) should be deleted from an editor.
   /// </summary>
   public enum DeletionMethod
   {
      /// <summary>
      /// Don't do anything
      /// </summary>
      Nothing,
      /// <summary>
      /// Remove from list, don't delete from network.
      /// </summary>
      Remove,
      /// <summary>
      /// Remove from list, if no longer referenced as child or to-part of a link, delete it from the brain.
      /// </summary>
      DeleteIfNoRef,
      /// <summary>
      /// Delete from brain (even if there are still referenes).
      /// </summary>
      Delete,
   }

   /// <summary>
   /// A helper class for managing editor objects: frames, objects,... 
   /// </summary>
   internal class EditorsHelper
   {
      /// <summary>
      /// Makes the object, which consists out of a cluster, a text neuron and a neuron that can be used as a meaning.
      /// </summary>
      /// <returns></returns>
      static public MindMapCluster MakeObject()
      {
         string iObjectName = null;
         DlgStringQuestion iIn = new DlgStringQuestion();
         iIn.Owner = App.Current.MainWindow;
         iIn.Question = "Object name:";
         iIn.Answer = "New object";
         iIn.Title = "New object";
         if ((bool)iIn.ShowDialog() == true)
         {
            iObjectName = iIn.Answer;
            NeuronCluster iCluster = new NeuronCluster();
            iCluster.Meaning = (ulong)PredefinedNeurons.Object;
            WindowMain.AddItemToBrain(iCluster);                                                 //we use this function cause it takes care of the undo data.
            BrainData.Current.NeuronInfo[iCluster.ID].DisplayTitle = iObjectName;

            Neuron iNeuron = new Neuron();
            WindowMain.AddItemToBrain(iNeuron);                                                 //we use this function cause it takes care of the undo data.
            using (ChildrenAccessor iList = iCluster.ChildrenW)
               iList.Add(iNeuron);
            BrainData.Current.DefaultMeaningIds.Add(iNeuron.ID);
            BrainData.Current.NeuronInfo[iNeuron.ID].DisplayTitle = iObjectName;

            ulong iTextId;
            TextNeuron iText;
            string iTextName = iObjectName.ToLower();                                           //all dict items are lowercase.
            if (TextSin.Words.TryGetValue(iTextName, out iTextId) == true)                      //could be that this word already exists, if so, we reuse it, cause an item can only be once in the dict.
               iText = Brain.Current[iTextId] as TextNeuron;
            else
            {
               iText = new TextNeuron();
               iText.Text = iObjectName;
               WindowMain.AddItemToBrain(iText);                                                 //we use this function cause it takes care of the undo data.
               BrainData.Current.NeuronInfo[iText.ID].DisplayTitle = iTextName;
               TextSin.Words[iTextName] = iText.ID;
            }
            using (ChildrenAccessor iList = iCluster.ChildrenW)
               iList.Add(iText);

            return MindMapNeuron.CreateFor(iCluster) as MindMapCluster;
         }
         return null;
      }

      /// <summary>
      /// Asks the user for the name of a new frame and creates this frame.
      /// </summary>
      /// <returns>A neuron cluster that represents the frame.</returns>
      static public Frame MakeFrame()
      {
         string iObjectName = null;
         DlgStringQuestion iIn = new DlgStringQuestion();
         iIn.Owner = App.Current.MainWindow;
         iIn.Question = "Frame name:";
         iIn.Answer = "New frame";
         iIn.Title = "New frame";
         if ((bool)iIn.ShowDialog() == true)
         {
            iObjectName = iIn.Answer;
            NeuronCluster iCluster = new NeuronCluster();
            iCluster.Meaning = (ulong)PredefinedNeurons.Frame;
            WindowMain.AddItemToBrain(iCluster);                                                 //we use this function cause it takes care of the undo data.
            BrainData.Current.NeuronInfo[iCluster.ID].DisplayTitle = iObjectName;
            return new Frame(iCluster);
         }
         return null;
      }

      /// <summary>
      /// Asks the user for the name of a new object to be used as a frame element and creates it.
      /// </summary>
      /// <returns>null if the user canceled, otherwise a new object wrapper for a cluster.</returns>
      public static FrameElement MakeFrameElement()
      {
         string iObjectName = null;
         DlgStringQuestion iIn = new DlgStringQuestion();
         iIn.Owner = App.Current.MainWindow;
         iIn.Question = "Object name for element:";
         iIn.Answer = "New object";
         iIn.Title = "New object";
         if ((bool)iIn.ShowDialog() == true)
         {
            iObjectName = iIn.Answer;
            NeuronCluster iCluster = new NeuronCluster();
            iCluster.Meaning = (ulong)PredefinedNeurons.Object;
            WindowMain.AddItemToBrain(iCluster);                                                 //we use this function cause it takes care of the undo data.
            BrainData.Current.NeuronInfo[iCluster.ID].DisplayTitle = iObjectName;

            ulong iTextId;
            TextNeuron iText;
            string iTextName = iObjectName.ToLower();                                           //all dict items are lowercase.
            if (TextSin.Words.TryGetValue(iTextName, out iTextId) == true)                      //could be that this word already exists, if so, we reuse it, cause an item can only be once in the dict.
               iText = Brain.Current[iTextId] as TextNeuron;
            else
            {
               iText = new TextNeuron();
               iText.Text = iObjectName;
               WindowMain.AddItemToBrain(iText);                                                 //we use this function cause it takes care of the undo data.
               BrainData.Current.NeuronInfo[iText.ID].DisplayTitle = iTextName;
               TextSin.Words[iTextName] = iText.ID;
            }
            using (ChildrenAccessor iList = iCluster.ChildrenW)
               iList.Add(iText);

            return new FrameElement(iCluster);
         }
         return null;
      }

      /// <summary>
      /// Asks the user for the name of a new object to be used as a frame evoker and creates it.
      /// </summary>
      /// <returns>null if the user canceled, otherwise a new object wrapper for a cluster.</returns>
      public static FrameEvoker MakeFrameEvoker()
      {
         string iObjectName = null;
         DlgStringQuestion iIn = new DlgStringQuestion();
         iIn.Owner = App.Current.MainWindow;
         iIn.Question = "Object name for evoker:";
         iIn.Answer = "New object";
         iIn.Title = "New object";
         if ((bool)iIn.ShowDialog() == true)
         {
            iObjectName = iIn.Answer;
            NeuronCluster iCluster = new NeuronCluster();
            iCluster.Meaning = (ulong)PredefinedNeurons.Object;
            WindowMain.AddItemToBrain(iCluster);                                                 //we use this function cause it takes care of the undo data.
            BrainData.Current.NeuronInfo[iCluster.ID].DisplayTitle = iObjectName;

            ulong iTextId;
            TextNeuron iText;
            string iTextName = iObjectName.ToLower();                                           //all dict items are lowercase.
            if (TextSin.Words.TryGetValue(iTextName, out iTextId) == true)                      //could be that this word already exists, if so, we reuse it, cause an item can only be once in the dict.
               iText = Brain.Current[iTextId] as TextNeuron;
            else
            {
               iText = new TextNeuron();
               iText.Text = iObjectName;
               WindowMain.AddItemToBrain(iText);                                                 //we use this function cause it takes care of the undo data.
               BrainData.Current.NeuronInfo[iText.ID].DisplayTitle = iTextName;
               TextSin.Words[iTextName] = iText.ID;
            }
            using (ChildrenAccessor iList = iCluster.ChildrenW)
               iList.Add(iText);

            return new FrameEvoker(iCluster);
         }
         return null;
      }

      /// <summary>
      /// Asks the user for the name of a new object to be used as a frame sequence and creates it.
      /// </summary>
      /// <returns>null if the user canceled, otherwise a new object wrapper for a cluster.</returns>
      public static FrameSequence MakeFrameSequence()
      {
         string iObjectName = null;
         DlgStringQuestion iIn = new DlgStringQuestion();
         iIn.Owner = App.Current.MainWindow;
         iIn.Question = "sequence name:";
         iIn.Answer = "New sequence";
         iIn.Title = "New sequence";
         if ((bool)iIn.ShowDialog() == true)
         {
            iObjectName = iIn.Answer;
            NeuronCluster iCluster = new NeuronCluster();
            iCluster.Meaning = (ulong)PredefinedNeurons.FrameSequence;
            WindowMain.AddItemToBrain(iCluster);                                                 //we use this function cause it takes care of the undo data.
            BrainData.Current.NeuronInfo[iCluster.ID].DisplayTitle = iObjectName;
            return new FrameSequence(iCluster);
         }
         return null;
      }

      /// <summary>
      /// Asks the user for the name of a new flow and creates it if 'ok' is pressed.
      /// </summary>
      /// <returns>Null if the user canceled, otherwise a new flow.</returns>
      public static Flow MakeFlow()
      {
         string iObjectName = null;
         DlgStringQuestion iIn = new DlgStringQuestion();
         iIn.Owner = App.Current.MainWindow;
         iIn.Question = "Flow name:";
         iIn.Answer = "New flow";
         iIn.Title = "New flow";
         if ((bool)iIn.ShowDialog() == true)
         {
            iObjectName = iIn.Answer;
            NeuronCluster iCluster = new NeuronCluster();
            iCluster.Meaning = (ulong)PredefinedNeurons.Flow;
            WindowMain.AddItemToBrain(iCluster);                                                 //we use this function cause it takes care of the undo data.
            Flow iRes = new Flow();
            iRes.ItemID = iCluster.ID;
            iRes.NeuronInfo.DisplayTitle = iObjectName;
            iRes.Name = iObjectName;
            return iRes;
         }
         return null;
      }

      /// <summary>
      /// Creates a neuron cluster (meaning FlowItemConditional) and a new option (conditiona statement) for a flow that
      /// wraps the cluster.
      /// </summary>
      public static FlowItemConditional MakeFlowOption()
      {
         NeuronCluster iCluster = new NeuronCluster();
         iCluster.Meaning = (ulong)PredefinedNeurons.FlowItemConditional;
         WindowMain.AddItemToBrain(iCluster);                                                 //we use this function cause it takes care of the undo data.
         Link iLink = new Link(Brain.Current[(ulong)PredefinedNeurons.False], iCluster, (ulong)PredefinedNeurons.FlowItemIsLoop);
         FlowItemConditional iRes = new FlowItemConditional(iCluster);
         return iRes;
      }

      /// <summary>
      /// Creates a neuron cluster (meaning FlowItemConditional) and a new loop (conditiona statement) for a flow that
      /// wraps the cluster.
      /// </summary>
      public static FlowItemConditional MakeFlowLoop()
      {
         NeuronCluster iCluster = new NeuronCluster();
         iCluster.Meaning = (ulong)PredefinedNeurons.FlowItemConditional;
         WindowMain.AddItemToBrain(iCluster);                                                 //we use this function cause it takes care of the undo data.
         Link iLink = new Link(Brain.Current[(ulong)PredefinedNeurons.True], iCluster, (ulong)PredefinedNeurons.FlowItemIsLoop);
         FlowItemConditional iRes = new FlowItemConditional(iCluster);
         return iRes;
      }

      /// <summary>
      /// Creates a neuron cluster (meaning FlowItemConditionalPart) and a new conditional statement part for a flow that
      /// wraps the cluster.
      /// </summary>
      /// <remarks>
      /// A conditional statement part is the '|' in {|||} and [|||]
      /// </remarks>
      /// <returns></returns>
      internal static FlowItemConditionalPart MakeFlowCondPart()
      {
         NeuronCluster iCluster = new NeuronCluster();
         iCluster.Meaning = (ulong)PredefinedNeurons.FlowItemConditionalPart;
         WindowMain.AddItemToBrain(iCluster);                                                 //we use this function cause it takes care of the undo data.
         FlowItemConditionalPart iRes = new FlowItemConditionalPart(iCluster);
         return iRes;
      }

      internal static void InsertFlowItem(FlowItem toAdd, FlowItem relativeTo)
      {
         FlowItemBlock iOwner = relativeTo.Owner as FlowItemBlock;
         if (iOwner == null)
         {
            Flow iFlow = relativeTo.Owner as Flow;
            Debug.Assert(iFlow != null);
            iFlow.Items.Insert(iFlow.Items.IndexOf(relativeTo), toAdd);
         }
         else
         {
            if (!(toAdd is FlowItemConditionalPart))
            {
               if (iOwner is FlowItemConditionalPart)
                  iOwner.Items.Insert(iOwner.Items.IndexOf(relativeTo), toAdd);
               else
               {
                  FlowItemConditionalPart iNewPart = EditorsHelper.MakeFlowCondPart();
                  iOwner.Items.Insert(iOwner.Items.IndexOf(relativeTo), iNewPart);
                  iNewPart.Items.Add(toAdd);
               }
            }
            else
            {
               if (iOwner is FlowItemConditional)
                  iOwner.Items.Insert(iOwner.Items.IndexOf(relativeTo), toAdd);
               else
               {
                  FlowItemBlock iBlock = iOwner.Owner as FlowItemBlock;
                  if (iBlock == null)
                  {
                     Flow iFlow = relativeTo.Owner as Flow;
                     Debug.Assert(iFlow != null);
                     iFlow.Items.Insert(iFlow.Items.IndexOf(iOwner), toAdd);
                  }
                  else
                     iBlock.Items.Insert(iBlock.Items.IndexOf(iOwner), toAdd);
               }
            }
         }
         toAdd.IsSelected = true;                                                                           //we do this at the end, when it has been added to the list, this way, all the others are also updated correctly.




      }

      /// <summary>
      /// Adds a flow item relative to the second flow item.  Relative to means:
      /// - for static: add at end of owner of static.
      /// - for conditional part: add at end of the part
      /// - fo conditional: add at end of conditional, make certain that there is a new part also added if the added item is not a part.
      /// </summary>
      /// <param name="toAdd">To add.</param>
      /// <param name="relativeTo">The relative to.</param>
      /// <param name="flow">The flow that should own the item. This is required in case relativeTo is empty.</param>
      internal static void AddFlowItem(FlowItem toAdd, FlowItem relativeTo, Flow flow)
      {
         if (relativeTo is FlowItemStatic)
         {
            FlowItemBlock iOwner = relativeTo.Owner as FlowItemBlock;
            if (iOwner == null)
            {
               Debug.Assert(flow != null);
               flow.Items.Add(toAdd);
            }
            else
            {
               if (toAdd is FlowItemConditionalPart)                                      //we are adding a | when we are on a static, so we need to go up 1 more owner, so we add to the loop/option
                  iOwner = (FlowItemBlock)iOwner.Owner;
               iOwner.Items.Add(toAdd);
            }
         }
         else if (relativeTo is FlowItemConditional)
         {
            if (!(toAdd is FlowItemConditionalPart))
            {
               FlowItemConditionalPart iNewPart = EditorsHelper.MakeFlowCondPart();
               ((FlowItemConditional)relativeTo).Items.Add(iNewPart);
               iNewPart.Items.Add(toAdd);
            }
            else
               ((FlowItemConditional)relativeTo).Items.Add(toAdd);
         }
         else if (relativeTo is FlowItemConditionalPart)
         {
            if (toAdd is FlowItemConditionalPart)
            {
               FlowItemConditional iCond = (FlowItemConditional)relativeTo.Owner;
               iCond.Items.Add(toAdd);
            }
            else
               ((FlowItemConditionalPart)relativeTo).Items.Add(toAdd);
         }
         else if (relativeTo == null)
         {
            Debug.Assert(flow != null);
            flow.Items.Add(toAdd);
         }
         else
            throw new InvalidOperationException("Unkown flowItem, don't know how to add relative to it.");
         toAdd.IsSelected = true;                                                                           //we do this at the end, when it has been added to the list, this way, all the others are also updated correctly.
      }

      /// <summary>
      /// Creates a wrapper flow item for the specified neuron. It checks if the neuron is a cluster with as meaning 'FlowITemConditional'
      /// or 'FlowItemConditionalPart' and creates an appropriate item accordingly.
      /// </summary>
      /// <param name="neuron">The neuron.</param>
      /// <returns></returns>
      public static FlowItem CreateFlowItemFor(Neuron neuron)
      {
         NeuronCluster iCluster = neuron as NeuronCluster;
         if (iCluster != null)
         {
            if (iCluster.Meaning == (ulong)PredefinedNeurons.FlowItemConditional)
               return new FlowItemConditional(iCluster);
            else if (iCluster.Meaning == (ulong)PredefinedNeurons.FlowItemConditionalPart)
               return new FlowItemConditionalPart(iCluster);
         }
         return new FlowItemStatic(neuron);
      }


      /// <summary>
      /// Removes the specified neuron from the cluster. When the neuron no longer has any incomming links or belongs
      /// to a cluster, it is deleted.
      /// </summary>
      /// <param name="neuron">The neuron.</param>
      /// <param name="neuronCluster">The neuron cluster.</param>
      public static void RemoveOrDeleteFromCluster<T>(T neuron, ClusterCollection<T> cluster, DeletionMethod branchHandling)where T: EditorItem
      {
         cluster.Remove(neuron);
         DeleteWhenNotUsed(neuron.Item, branchHandling);
      }

      /// <summary>
      /// Deletes the neuron when it doesn't have any incomming links and is not clustered.
      /// </summary>
      /// <param name="neuron">The neuron.</param>
      public static void DeleteWhenNotUsed(Neuron neuron, DeletionMethod branchHandling)
      {
         if (neuron.CanBeDeleted == true)
         {
            bool iDelete;
            using (LinksAccessor iLinksIn = neuron.LinksIn)
            {
               using (NeuronsAccessor iClusteredBy = neuron.ClusteredBy)
                  iDelete = iLinksIn.Items.Count == 0 && iClusteredBy.Items.Count == 0;
            }
            if (iDelete == true)
            {
               ProcessBranch(neuron, branchHandling);
               WindowMain.DeleteItemFromBrain(neuron);
            }
         }
      }


      /// <summary>
      /// Processes the entire branch of the specified neuron and deletes them according to the specifications in 
      /// 'branchHandling'.
      /// </summary>
      /// <param name="neuron">The neuron.</param>
      /// <param name="branchHandling">The branch handling.</param>
      public static void ProcessBranch(Neuron neuron, DeletionMethod branchHandling)
      {
         switch (branchHandling)
         {
            case DeletionMethod.Nothing:
               break;
            case DeletionMethod.Remove:
               break;
            case DeletionMethod.DeleteIfNoRef:
               WindowMain.DeleteBranchChildren(neuron, false);
               break;
            case DeletionMethod.Delete:
               WindowMain.DeleteBranchChildren(neuron, true);
               break;
            default:
               throw new InvalidOperationException();
         }
      }

      /// <summary>
      /// Gets the type of code item that would be generated for the specified item.
      /// </summary>
      /// <param name="item">The item.</param>
      /// <returns></returns>
      internal static Type GetCodeItemTypeFor(Neuron item)
      {
         if (item is ResultStatement)
            return typeof(CodeItemResultStatement);
         else if (item is Statement)
            return typeof(CodeItemStatement);
         else if (item is ConditionalStatement)
            return typeof(CodeItemConditionalStatement);
         else if (item is ConditionalExpression)
            return typeof(CodeItemConditionalExpression);
         else if (item is Global)
            return typeof(CodeItemGlobal);
         else if (item is Variable)
            return typeof(CodeItemVariable);
         else if (item is SearchExpression)
            return typeof(CodeItemSearchExpression);
         else if (item is BoolExpression)
            return typeof(CodeItemBoolExpression);
         else if (item is Assignment)
            return typeof(CodeItemAssignment);
         else if (item is ExpressionsBlock)
            return typeof(CodeItemCodeBlock);
         else if (item is ByRefExpression)
            return typeof(CodeItemByRef);
         else
            return typeof(CodeItemStatic);
      }

      public static CodeItem CreateCodeItemFor(Neuron item)
      {
         if (item is ResultStatement)
            return new CodeItemResultStatement((ResultStatement)item);
         else if (item is Statement)
            return new CodeItemStatement((Statement)item);
         else if (item is ConditionalStatement)
            return new CodeItemConditionalStatement((ConditionalStatement)item);
         else if (item is ConditionalExpression)
            return new CodeItemConditionalExpression((ConditionalExpression)item);
         else if (item is Global)
            return new CodeItemGlobal((Global)item);
         else if (item is Variable)
            return new CodeItemVariable((Variable)item);
         else if (item is SearchExpression)
            return new CodeItemSearchExpression((SearchExpression)item);
         else if (item is BoolExpression)
            return new CodeItemBoolExpression((BoolExpression)item);
         else if (item is Assignment)
            return new CodeItemAssignment((Assignment)item);
         else if (item is ExpressionsBlock)
            return new CodeItemCodeBlock((ExpressionsBlock)item);
         else if (item is ByRefExpression)
            return new CodeItemByRef((ByRefExpression)item);
         else
            return new CodeItemStatic(item);

      }


      /// <summary>
      /// Determines whether the specified items can all be deleted (If there are any items).
      /// </summary>
      /// <param name="toDelete">List of neurons to delete.</param>
      /// <returns>
      /// 	<c>true</c> if they can all be deleted; otherwise, <c>false</c>.
      /// </returns>
      public static bool CanBeDeleted(IEnumerable<Neuron> toDelete)
      {
         int iCant = 0;                                                                   //keeps track of how many can be deleted.
         int iFound = 0;
         foreach (Neuron i in toDelete)
         {
            if (i.CanBeDeleted == false)
            {
               iCant++;
               iFound++;
            }
         }
         return iCant == 0 && iFound > 0;
      }

      /// <summary>
      /// Sets the first outgoing link the specified Neuron, to the new value. During
      /// this process, the proper undo data is generated.
      /// </summary>
      /// <param name="from">The neuron to change an outgoing link for.</param>
      /// <param name="meaning">The meaning.</param>
      /// <param name="value">The value.</param>
      /// <remarks>
      /// Note, we can't rely on OnPropertyChanging event handling of the undo system, to handle link changes, cause the
      /// editor item that generated the event can be replaced. Instead, we must use LinkUndoItem data.
      /// </remarks>
      public static void SetFirstOutgoingLinkTo(Neuron from, ulong meaning, EditorItem value)
      {
         Link iLink;
         LinkUndoItem iUndoData = null;
         using (LinksAccessor iLinksOut = from.LinksOut)
            iLink = (from i in iLinksOut.Items where i.MeaningID == meaning select i).FirstOrDefault();
         if (value != null)
         {
            if (iLink != null)
            {
               iUndoData = new LinkUndoItem() { Action = BrainAction.Changed, Item = iLink };
               iLink.To = value.Item;
            }
            else
            {
               iLink = new Link(value.Item, from, meaning);
               iUndoData = new LinkUndoItem() { Action = BrainAction.Created, Item = iLink };
            }
         }
         else if (iLink != null)
         {
            iUndoData = new LinkUndoItem() { Action = BrainAction.Removed, Item = iLink };
            iLink.Destroy();
         }
         if (iUndoData != null)
            WindowMain.UndoStore.AddCustomUndoItem(iUndoData);
      }


      /// <summary>
      /// Sets the first outgoing link the specified Neuron, to the new value. During
      /// this process, the proper undo data is generated.
      /// </summary>
      /// <param name="from">The neuron to change the outgoing link for.</param>
      /// <param name="meaning">The meaning.</param>
      /// <param name="value">The value as a bool, this is resolved to <see cref="PredefinedNeurons.True"/>
      /// or <see cref="PredefinedNeurons.True"/>.</param>
      /// <remarks>
      /// Note, we can't rely on OnPropertyChanging event handling of the undo system, to handle link changes, cause the
      /// editor item that generated the event can be replaced. Instead, we must use LinkUndoItem data.
      /// </remarks>
      public static void SetFirstOutgoingLinkTo(Neuron from, ulong meaning, bool value)
      {
         Link iLink;
         LinkUndoItem iUndoData = null;
         using (LinksAccessor iLinksOut = from.LinksOut)
            iLink = (from i in iLinksOut.Items where i.MeaningID == meaning select i).FirstOrDefault();
         Neuron iVal;
         if (value == true)
            iVal = Brain.Current[(ulong)PredefinedNeurons.True];
         else
            iVal = Brain.Current[(ulong)PredefinedNeurons.False];
         if (iLink != null)
         {
            iUndoData = new LinkUndoItem() { Action = BrainAction.Changed, Item = iLink };
            iLink.To = iVal;
         }
         else
         {
            iLink = new Link(iVal, from, meaning);
            iUndoData = new LinkUndoItem() { Action = BrainAction.Created, Item = iLink };
         }
         if (iUndoData != null)
            WindowMain.UndoStore.AddCustomUndoItem(iUndoData);
      }
   }
}
