﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using NeuralNetworkDesigne.DockingControl.Controls;
using System.Windows.Threading;
using System;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// Provides an overview of the project editor data.
   /// </summary>
   public partial class EditorsOverview : UserControl
   {
      TreeViewItem fSelected;

      public EditorsOverview()
      {
         InitializeComponent();
      }

      #region IsEditing

      /// <summary>
      /// IsEditing Dependency Property
      /// </summary>
      public static readonly DependencyProperty IsEditingProperty =
          DependencyProperty.Register("IsEditing", typeof(bool), typeof(EditorsOverview),
              new FrameworkPropertyMetadata((bool)false,
                  new PropertyChangedCallback(OnIsEditingChanged)));

      /// <summary>
      /// Gets or sets the IsEditing property.  This dependency property 
      /// indicates wether the currently selected item in the list is in edit mode or not.
      /// </summary>
      public bool IsEditing
      {
         get { return (bool)GetValue(IsEditingProperty); }
         set { SetValue(IsEditingProperty, value); }
      }

      /// <summary>
      /// Handles changes to the IsEditing property.
      /// </summary>
      private static void OnIsEditingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
      {
         ((EditorsOverview)d).OnIsEditingChanged(e);
      }

      /// <summary>
      /// Provides derived classes an opportunity to handle changes to the IsEditing property.
      /// </summary>
      protected virtual void OnIsEditingChanged(DependencyPropertyChangedEventArgs e)
      {
         if ((bool)e.NewValue == false)
         {
            if (fSelected != null)
               Keyboard.Focus(fSelected);                                               //need to focus to the container, and not the list so that nav keys keep working.
            else
                TrvItems.Focus();
         }
      }

      #endregion


      /// <summary>
      /// Handles the Selected event of the TrvItems_Item control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void TrvItems_Item_Selected(object sender, RoutedEventArgs e)
      {
         fSelected = e.OriginalSource as TreeViewItem;
         if (fSelected == TrvRoot)                                                     //the root object doesn't have a backing code object which handles the IsSelected, so we need to do it here.
            BrainData.Current.CurrentEditorsList = BrainData.Current.Editors;
      }


      /// <summary>
      /// Handles the MouseDoubleClick event of the Button control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
      private void Button_MouseDoubleClick(object sender, MouseButtonEventArgs e)
      {
         OpenEditor(((FrameworkElement)sender).DataContext as EditorBase);
      }

      /// <summary>
      /// Handles the Click event of the MnuOpen control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuOpen_Click(object sender, RoutedEventArgs e)
      {
         EditorBase iData = (EditorBase)((FrameworkElement)sender).DataContext;
         OpenEditor(iData);
      }

      /// <summary>
      /// Opens the mind map.
      /// </summary>
      /// <param name="mindMap">The mind map.</param>
      private void OpenEditor(EditorBase data)
      {
         Cursor iPrev = Mouse.OverrideCursor;
         Mouse.OverrideCursor = Cursors.Wait;

         try
         {
            if (data != null && !(data is EditorFolder))
            {
               int iIndex = BrainData.Current.OpenDocuments.IndexOf(data);
               WindowMain iMain = (WindowMain)App.Current.MainWindow;
               if (iIndex == -1)
               {
                  iIndex = BrainData.Current.OpenDocuments.Count;
                  BrainData.Current.OpenDocuments.Add(data);                                          //need to add directly, can't use windowMain.AddItemToOpenDocuments cause we need to make frame visible, even if editor is already added to open documents (always want to make it visible).
               }
               ToolFrame iFrame = iMain.TfData.ItemContainerGenerator.ContainerFromIndex(iIndex) as ToolFrame;
               if (iFrame != null)
                  iFrame.IsSelected = true;
            }
         }
         finally
         {
            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<Cursor>(ResetCursor), iPrev);
         }
      }


      /// <summary>
      /// Resets the cursor. Used to reset it async.
      /// </summary>
      /// <param name="prev">The prev.</param>
      void ResetCursor(Cursor prev)
      {
         Mouse.OverrideCursor = prev;
      }

      #region Readonly textboxes

      /// <summary>
      /// Handles the Click event of the EditTextBox control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void EditTextBox_Click(object sender, RoutedEventArgs e)
      {
         IsEditing = true;
      }


      /// <summary>
      /// Handles the LostFocus event of the TxtTitle control.
      /// </summary>
      /// <remarks>
      /// When looses focus, need to make certain that is editing is turned off.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void TxtTitle_LostKeybFocus(object sender, KeyboardFocusChangedEventArgs e)
      {
         IsEditing = false;
      }

      /// <summary>
      /// Handles the LostFocus event of the TxtTitle control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void TxtTitle_LostFocus(object sender, RoutedEventArgs e)
      {
         IsEditing = false;
      }



      /// <summary>
      /// Handles the PrvKeyDown event of the TxtTitle control.
      /// </summary>
      /// <remarks>
      /// when enter is pressed, need to stop editing.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
      private void TxtTitle_PrvKeyDown(object sender, KeyEventArgs e)
      {
         if (e.Key == Key.Enter || e.Key == Key.Return)
            IsEditing = false;
      } 

      #endregion

      /// <summary>
      /// Handles the Executed event of the Rename control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void Rename_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         IsEditing = true;
      }

      /// <summary>
      /// Handles the CanExecute event of the Rename control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void Rename_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         e.CanExecute = fSelected != null;
      }

      /// <summary>
      /// Handles the Executed event of the Delete control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         EditorBase iToDelete = fSelected.DataContext as EditorBase;
         Debug.Assert(iToDelete != null);
         MessageBoxResult iRes = MessageBox.Show(string.Format("Remove editor: {0} from the project?", iToDelete.Name), "Delete", MessageBoxButton.YesNo, MessageBoxImage.Question);
         if (iRes == MessageBoxResult.Yes)
         {
            EditorFolder iFolder = iToDelete.Owner as EditorFolder;
            if (iFolder != null)
               iFolder.Items.Remove(iToDelete);
            else
               BrainData.Current.Editors.Remove(iToDelete);
         }
      }


      

   }
}
