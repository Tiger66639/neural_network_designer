﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DnD;
using System.Windows.Controls;
using System.Diagnostics;
using System.Windows;

namespace NeuralNetworkDesigne.HAB.Designer
{
   public class EditorItemDropAdvisor : DropTargetBase
   {
      #region prop

      #region UsePreviewEvents
      /// <summary>
      /// Gets if the preview event versions should be used or not.
      /// </summary>
      /// <remarks>
      /// don't use preview events cause than the sub lists don't get used but only the main list cause this gets the events first,
      /// while we usually want to drop in a sublist.
      /// </remarks>
      public override bool UsePreviewEvents
      {
         get
         {
            return false;
         }
      }
      #endregion

      #region CodeList

      /// <summary>
      /// Gets the list containing all the code that the UI to which advisor is attached too, displays data for.
      /// </summary>
      public EditorCollection Items
      {
         get
         {
            ItemsControl iItemsControl = ItemsControl.ItemsControlFromItemContainer(TargetUI);
            Debug.Assert(iItemsControl != null);
            return iItemsControl.ItemsSource as EditorCollection;
         }
      }

      #endregion
      #endregion

      #region Overrides
      public override void OnDropCompleted(DragEventArgs arg, Point dropPoint)
      {
         EditorBase iItem = arg.Data.GetData(Properties.Resources.EDITORFORMAT) as EditorBase;
         if (iItem != null && (arg.Effects & DragDropEffects.Move) == DragDropEffects.Move)
            TryMoveItem(arg, dropPoint, iItem);
         else
            TryCreateNewCodeItem(arg, dropPoint);
      }


      public override bool IsValidDataObject(IDataObject obj)
      {
         return obj.GetDataPresent(Properties.Resources.EDITORFORMAT)
                || obj.GetDataPresent(Properties.Resources.NeuronIDFormat);
      }

      #endregion

      private void TryMoveItem(DragEventArgs arg, Point dropPoint, EditorBase iItem)
      {
         EditorBase iDroppedOn = ((FrameworkElement)TargetUI).DataContext as EditorBase;
         EditorCollection iList;
         if (iDroppedOn is EditorFolder)                                                           //if we drop on a folder, we want to put it in the folder, otherwise, we want to put it at the same level as the item we drop on.
            iList = ((EditorFolder)iDroppedOn).Items;
         else
            iList = Items;
         int iNewIndex = iList.IndexOf(iDroppedOn);
         int iOldIndex = iList.IndexOf(iItem);
         if (iOldIndex == -1)
         {
            iItem.RemoveFromOwner();
            if (iNewIndex > -1)
               iList.Insert(iNewIndex, iItem);
            else
               iList.Add(iItem);
         }
         else
            iList.Move(iOldIndex, iNewIndex);
         arg.Effects = DragDropEffects.None;                                                    //we need to prevent that the drag source removes the item because we already did that.  we need to do this because we can't assign an item to a new list if it hasn't already been removed from the previous (Owner prop prevents this).
      }

      private void TryCreateNewCodeItem(DragEventArgs obj, Point dropPoint)
      {
         ulong iId = (ulong)obj.Data.GetData(Properties.Resources.NeuronIDFormat);
         CodeEditor iNew = new CodeEditor(Brain.Current[iId]);
         EditorBase iDroppedOn = ((FrameworkElement)TargetUI).DataContext as EditorBase;
         Debug.Assert(iNew != null && iDroppedOn != null);
         EditorCollection iList = Items;
         Debug.Assert(iList != null);
         int iIndex =iList.IndexOf(iDroppedOn);
         if (iIndex > -1)
            iList.Insert(iIndex, iNew);
         else
            iList.Add(iNew);
      }
   }
}
