﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeuralNetworkDesigne.Data;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// An editor object that contains other editor objects, it functions as a folder in the Editors-overview.
   /// </summary>
   public class EditorFolder: EditorBase
   {
      #region fields
      EditorCollection fItems; 
      #endregion

      #region ctor
      /// <summary>
      /// Initializes a new instance of the <see cref="EditorFolder"/> class.
      /// </summary>
      public EditorFolder()
      {
         fItems = new EditorCollection(this);
      } 
      #endregion

      #region Prop

      #region Icon
      /// <summary>
      /// Gets the resource path to the icon that should be used for this editor.  This is usually class specific.
      /// </summary>
      /// <value></value>
      public override string Icon
      {
         get { return "/Images/NewFolder.png"; }
      } 
      #endregion


      #region IsSelected
      /// <summary>
      /// Gets/sets the wether the editor is currently selected in the overview list.
      /// </summary>
      /// <value></value>
      /// <remarks>
      /// This is provided so that a folder can make itself 'active' when selected.
      /// </remarks>
      public override bool IsSelected
      {
         get
         {
            return base.IsSelected;
         }
         set
         {
            base.IsSelected = value;
            if (value == true)
               BrainData.Current.CurrentEditorsList = Items;
         }
      } 
      #endregion
      
      #region Items

      /// <summary>
      /// Gets the list of editor objects that are stored in this folder.
      /// </summary>
      public EditorCollection Items
      {
         get { return fItems; }
      }

      #endregion 

      /// <summary>
      /// Gets a title that the description editor can use to display in the header.
      /// </summary>
      /// <value></value>
      public override string DescriptionTitle
      {
         get
         {
            return Name + " - Folder";
         }
      }

      #endregion

      #region Functions

      /// <summary>
      /// Removes the editor from the specified list.
      /// </summary>
      /// <param name="list">The list.</param>
      /// <remarks>
      /// This is a virtual function so that some editors can have multiple open documents (like a folder).
      /// </remarks>
      public override void RemoveEditorFrom(System.Collections.IList list)
      {
         foreach (EditorBase i in Items)
            i.RemoveEditorFrom(list);
      }

      /// <summary>
      /// Reads the fields/properties of the class.
      /// </summary>
      /// <param name="reader">The reader.</param>
      /// <returns>
      /// True if the item was properly read, otherwise false.
      /// </returns>
      /// <remarks>
      /// This function is called for each element that is found, so this function should check which element it is
      /// and only read that element accordingly.
      /// </remarks>
      protected override bool ReadXmlInternal(XmlReader reader)
      {
         if (reader.Name == "EditorCollection")
         {
            fItems.Clear();
            fItems.ReadXml(reader);
            return true;
         }
         else
            return base.ReadXmlInternal(reader);
      }

      /// <summary>
      /// Converts an object into its XML representation.
      /// </summary>
      /// <param name="writer">The <see cref="T:System.Xml.XmlWriter"/> stream to which the object is serialized.</param>
      public override void WriteXml(XmlWriter writer)
      {
         base.WriteXml(writer);
         XmlSerializer iCol = new XmlSerializer(typeof(EditorCollection));
         iCol.Serialize(writer, Items);
      }

      #endregion



      #region Clipboard
      protected override void CopyToClipboard(DataObject data)
      {
         throw new NotImplementedException();
      }

      public override bool CanCopyToClipboard()
      {
         throw new NotImplementedException();
      }

      public override bool CanPasteSpecialFromClipboard()
      {
         throw new NotImplementedException();
      }

      public override void PasteSpecialFromClipboard()
      {
         throw new NotImplementedException();
      }


      public override bool CanPasteFromClipboard()
      {
         if (base.CanPasteFromClipboard() == true)
         {
         }
         throw new NotImplementedException();
      }

      public override void PasteFromClipboard()
      {
         throw new NotImplementedException();
      } 
      #endregion

      public override void Delete()
      {
         throw new NotImplementedException();
      }

      public override bool CanDelete()
      {
         throw new NotImplementedException();
      }

      public override void DeleteSpecial()
      {
         throw new NotImplementedException();
      }

      public override bool CanDeleteSpecial()
      {
         throw new NotImplementedException();
      }
   }
}
