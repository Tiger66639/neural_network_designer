using System;
using System.Windows;
using System.Windows.Markup;

namespace DnD
{
    public abstract class DragSourceBase
    {
        #region Fields

        private string fSupportedFormat = "SampleFormat";
        private UIElement fSourceElt;

        #endregion Fields

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="DragSourceBase"/> class.
        /// </summary>
        public DragSourceBase()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DragSourceBase"/> class.
        /// </summary>
        /// <param name="supportedFormat">The default supported format.</param>
        public DragSourceBase(string supportedFormat)
        {
            fSupportedFormat = supportedFormat;
        }

        #endregion ctor

        /// <summary>
        /// Gets or sets the source UI that the dragsource is currently servising.
        /// </summary>
        /// <value>The source UI handling the events.</value>
        public virtual UIElement SourceUI
        {
            get
            {
                return fSourceElt;
            }
            set
            {
                fSourceElt = value;
            }
        }

        /// <summary>
        /// Gets if the preview event versions should be used or not.
        /// </summary>
        public virtual bool UsePreviewEvents
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the supported effects.
        /// </summary>
        /// <value>The supported effects.</value>
        public virtual DragDropEffects SupportedEffects
        {
            get { return DragDropEffects.Copy | DragDropEffects.Move; }
        }

        public virtual string SupportedFormat
        {
            get { return fSupportedFormat; }
            set { fSupportedFormat = value; }
        }

        /// <summary>
        /// Gets the data object containing all the data required to perform the drag operation.
        /// </summary>
        /// <remarks>
        /// To add multiple objects to the data object, use <see cref="Properties.Resources.MultiUIElementFormat"/> as
        /// data type for the visual object, with a list containing all the ui elements.  The drop target base will make
        /// it into a proper composite image.
        /// The first item in the list must be the item actually being dragged.
        /// </remarks>
        /// <param name="draggedElt">The dragged elt.</param>
        /// <returns></returns>
        public virtual DataObject GetDataObject(UIElement draggedElt)
        {
            string serializedElt = XamlWriter.Save(draggedElt);
            DataObject obj = new DataObject();
            obj.SetData(fSupportedFormat, serializedElt);
            return obj;
        }

        public abstract void FinishDrag(UIElement draggedElt, DragDropEffects finalEffects);

        public abstract bool IsDraggable(UIElement dragElt);

        /// <summary>
        /// If this drag source object supports delay loading of the data, descendents should reimplement this function.
        /// </summary>
        public virtual void DelayLoad(IDataObject data)
        {
        }

        /// <summary>
        /// If this drag source supports delay loading of the data, descendents should reimplement this function to let
        /// the system know the type of data the drag source will provide.
        /// </summary>
        public virtual Type GetDelayLoadResultType(IDataObject data)
        {
            return null;
        }
    }
}