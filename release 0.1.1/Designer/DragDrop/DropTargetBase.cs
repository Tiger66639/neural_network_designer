using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace DnD
{
    public abstract class DropTargetBase
    {
        private string fSupportedFormat = "SampleFormat";

        private UIElement fTargetUI;

        public virtual UIElement TargetUI
        {
            get
            {
                return fTargetUI;
            }
            set
            {
                fTargetUI = value;
            }
        }

        /// <summary>
        /// Gets if the preview event versions should be used or not.
        /// </summary>
        public virtual bool UsePreviewEvents
        {
            get
            {
                return true;
            }
        }

        public virtual string SupportedFormat
        {
            get { return fSupportedFormat; }
            set { fSupportedFormat = value; }
        }

        public virtual bool IsValidDataObject(IDataObject obj)
        {
            return (obj.GetDataPresent(fSupportedFormat));
        }

        /// <summary>
        /// Called when a drop is performed.
        /// </summary>
        /// <remarks>
        /// modified from original, changed IDataobject to DragEventArgs so that we have more
        /// info about the drop, like on who we are dropping.
        /// </remarks>
        /// <param name="obj"></param>
        /// <param name="dropPoint"></param>
        public abstract void OnDropCompleted(DragEventArgs obj, Point dropPoint);

        /// <summary>
        /// Gets the visual feedback.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns></returns>
        public virtual UIElement GetVisualFeedback(IDataObject obj)
        {
            UIElement iEl = ExtractElement(obj);
            if (iEl != null)
            {
                iEl.Opacity = 0.5;
                iEl.IsHitTestVisible = false;
                return iEl;
            }
            return null;
        }

        /// <summary>
        /// Extracts the element.
        /// </summary>
        /// <remarks>
        /// When there are multiple items on the list, we must construct a new element (canvas) containing all the items.
        /// </remarks>
        /// <param name="obj">The data object from which to extract the visual element.</param>
        /// <returns></returns>
        public virtual UIElement ExtractElement(IDataObject obj)
        {
            UIElement iRes = obj.GetData(Resources.UIElementFormat) as UIElement;
            if (iRes != null)
                return GetRectangleFor(iRes);
            else
            {
                List<UIElement> iList = obj.GetData(Resources.MultiUIElementFormat) as List<UIElement>;
                if (iList != null && iList.Count > 0)
                {
                    UIElement iFirst = iList[0];
                    var iMetrics = from i in iList                                                         //extract some info for each item in the list
                                   select new
                                   {
                                       Point = i.TranslatePoint(new Point(0, 0), iFirst),                 //this gets the top corner point relative to the first item in the list (which is the item being dragged).  This is used to calculate the total size of all the item.
                                       Width = i.GetValue(FrameworkElement.ActualWidthProperty),
                                       Height = i.GetValue(FrameworkElement.ActualHeightProperty)
                                   };
                    var iSum = new
                    {
                        MinX = iMetrics.Min(i => i.Point.X),                                                //the smallest value (to the left)
                        MaxX = iMetrics.Max(i => i.Point.X + (double)i.Width),                              //the biggest value (to the right)
                        MinY = iMetrics.Min(i => i.Point.Y),                                                //top
                        MaxY = iMetrics.Max(i => i.Point.Y + (double)i.Height)                               //the biggest value (to the bottom)
                    };

                    int iIndex = 0;
                    Canvas iCanvas = new Canvas();
                    iCanvas.Width = Math.Abs(iSum.MinX) + Math.Abs(iSum.MaxX);                                //we must assign a fixed widht/height to the canvas cause this is a top item.  MinX contains the left most pos, MaxX contains the biggest pos + size
                    iCanvas.Height = Math.Abs(iSum.MinY) + Math.Abs(iSum.MaxY);
                    TranslateTransform iTransform = new TranslateTransform(iSum.MinX, iSum.MinY);             //we also need to add a transform so that the canvas is positioned correctly over the items.
                    iCanvas.RenderTransform = iTransform;
                    foreach (var i in iMetrics)
                    {
                        Rectangle iRect = GetRectangleFor(iList[iIndex]);                                      //we create a rect for each UI item and put it in the canvas.
                        Canvas.SetTop(iRect, i.Point.Y - iSum.MinY);
                        Canvas.SetLeft(iRect, i.Point.X - iSum.MinX);
                        iCanvas.Children.Add(iRect);
                        iIndex++;
                    }
                    iRes = iCanvas;
                }
            }
            return iRes;
        }

        /// <summary>
        /// Creates a rectangle and assigns it a visual brush containing the arg to use for Fill.
        /// </summary>
        /// <param name="iRes">The UI to use as a brush o the rect.</param>
        /// <returns>A new rectangle that represents the item.</returns>
        private Rectangle GetRectangleFor(UIElement item)
        {
            Type iType = item.GetType();
            Rectangle iRect = new Rectangle();
            iRect.Width = (double)iType.GetProperty("ActualWidth").GetValue(item, null);
            iRect.Height = (double)iType.GetProperty("ActualHeight").GetValue(item, null);
            VisualBrush iBrush = new VisualBrush(item);
            iBrush.Stretch = Stretch.None;                                                                  //we need to turn of stretching, otherwise, some items can get disfigured because of bitmapeffects.
            iRect.Fill = iBrush;
            return iRect;
        }

        /// <summary>
        /// Called whenever an item is being moved over the drop target.  By default doesn't do anything.
        /// </summary>
        /// <remarks>
        /// Can be used to adjust the position (for snapping) or draw extra placement information.
        /// </remarks>
        /// <param name="position"></param>
        /// <param name="data">The data being dragged.</param>
        public virtual void OnDragOver(ref Point position, IDataObject data)
        {
        }

        /// <summary>
        /// Gets the effect that should be used for the drop operation.
        /// </summary>
        /// <remarks>
        /// By default, this function checks the control key, wen pressed, a copy is done, otherwise a move.
        /// </remarks>
        /// <param name="e">The drag event arguments.</param>
        /// <returns>The prefered effect to use.</returns>
        public virtual DragDropEffects GetEffect(DragEventArgs e)
        {
            return ((e.KeyStates & DragDropKeyStates.ControlKey) != 0) ? DragDropEffects.Copy : DragDropEffects.Move;
        }
    }
}