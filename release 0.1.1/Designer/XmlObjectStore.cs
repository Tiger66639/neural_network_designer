﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace NeuralNetworkDesigne.HAB.Designer
{
   /// <summary>
   /// A wrapper class for objects that need to be stored in xml with the exact type specified.
   /// </summary>
   public class XmlObjectStore : IXmlSerializable
   {
      /// <summary>
      /// The value.
      /// </summary>
      public object Data { get; set; }

      #region IXmlSerializable Members

      public XmlSchema GetSchema()
      {
         return null;
      }

      public void ReadXml(XmlReader reader)
      {
         bool wasEmpty = reader.IsEmptyElement;

         reader.Read();
         if (wasEmpty) return;

         reader.ReadStartElement("Type");
         string iFound = reader.ReadString();
         Type iType = Type.GetType(iFound);
         reader.ReadEndElement();

         XmlSerializer valueSerializer = new XmlSerializer(iType);
         Data = valueSerializer.Deserialize(reader);

         reader.ReadEndElement();
      }

      public void WriteXml(XmlWriter writer)
      {
         writer.WriteStartElement("Type");
         writer.WriteString(Data.GetType().ToString());
         writer.WriteEndElement();

         XmlSerializer valueSerializer = new XmlSerializer(Data.GetType());
         valueSerializer.Serialize(writer, Data);
      }

      #endregion
   }
}
