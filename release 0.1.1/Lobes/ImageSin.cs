﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace NeuralNetworkDesigne.HAB
{
    /// <summary>
    /// Sensory interface able to convert WPF BitmaSource objects into  neurons and vice versa.
    /// </summary>
    /// <remarks>
    /// <para>
    /// An image sin converts an image into neuron impulses by dividing each pixel into 4 colors:
    /// red, green, blue, gray value.  Each color becomes a neuron value, 4 colors in a neuron make
    /// a pixel.  These pixel neurons form the entrypoints that are saved in a cluster grouped per
    /// row.  So there is the image neuron (a cluster) containing lines (also cluster), each line contains
    /// pixels as children (neurons), each pixel has 4 references to other int neurons which have the actual value.
    /// </para>
    /// <para>
    /// A neuron is created per pixel.  This neuron contains 4 links to int neurons which contain the 3
    /// base colors + the gray scaled value of the 3 colors (just like the eye), so no transparancy, which
    /// is not wanted for a system that needs to mimic the eye.
    /// </para>
    /// <para>
    /// The neurons that represent the initial value of the image are not recreated each time a new image is
    /// provided. Instead, they are reused for each image. This is done because we would otherwise have to
    /// create far to many neurons + we would not be able to program each imput item seperatly + this technique
    /// is similar to our eyes (they don't create new neurons for each image, values are simply adjusted.
    ///
    /// This does have some implications:
    /// - an image can only be processed if the previous image has been handled.  This is done by sending the
    /// same input neuron back out. (so when the input neuron is not present, we need to wait untill it is back, done
    /// by output.
    /// - The size of the image that can be processed, is predifened. Whenever this changes, the entry points will change and
    /// so will the brain, cause old entry points might get deleted, new ones added.
    /// </para>
    /// <para>
    /// Because the entrypoint neurons are reused each time.  a special EntryPointsCreated function is called (if it assigned to
    /// this sin) after the entrypoints have been recreated.
    /// </para>
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.Red, typeof(Neuron))]                                         //create the default neuron we use as initial link types for pixels we receive as input.
    [NeuronID((ulong)PredefinedNeurons.Green, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Blue, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Gray, typeof(Neuron))]
    public class ImageSin : Sin
    {
        #region Fields

        private bool fIsProcessing = false;                                                                     //when true, the brain is processing

        #endregion Fields

        #region Events

        /// <summary>
        /// Occurs when the sin is ready proccesing a bitmapsource.
        /// </summary>
        /// <remarks>
        /// Monitor this event to check when a new bitmapsource can be send to this sin.
        /// </remarks>
        public event OutputEventHandler IsReady;

        #endregion Events

        #region IsProcessing

        /// <summary>
        /// Gets a value indicating whether this instance is processing.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is processing; otherwise, <c>false</c>.
        /// </value>
        public bool IsProcessing
        {
            get { return fIsProcessing; }
        }

        #endregion IsProcessing

        #region Width

        /// <summary>
        /// Gets/sets the width of the images to process.
        /// </summary>
        /// <remarks>
        /// When an image is processed that has a different with value as that of this imagesin, the image is resized.
        /// when there is no with relationship yet defined on this neuron, it is created
        /// </remarks>
        public int Width
        {
            get
            {
                IntNeuron iFound = FindFirstOut((ulong)PredefinedNeurons.Width) as IntNeuron;
                if (iFound == null)
                {
                    iFound = new IntNeuron() { Value = 0 };
                    Brain.Current.Add(iFound);
                    SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Width, iFound);
                }
                return iFound.Value;
            }
            set
            {
                IntNeuron iFound = FindFirstOut((ulong)PredefinedNeurons.Width) as IntNeuron;
                if (iFound == null)
                {
                    iFound = new IntNeuron() { Value = value };
                    Brain.Current.Add(iFound);
                    SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Width, iFound);
                }
                else
                    iFound.Value = value;
                ReBuildStartPoints(Height, value);
            }
        }

        #endregion Width

        #region Height

        /// <summary>
        /// Gets/sets the Height of the images to process.
        /// </summary>
        /// <remarks>
        /// When an image is processed that has a different Height value as that of this imagesin, the image is resized.
        /// when there is no Height relationship yet defined on this neuron, it is created
        /// </remarks>
        public int Height
        {
            get
            {
                IntNeuron iFound = FindFirstOut((ulong)PredefinedNeurons.Height) as IntNeuron;
                if (iFound == null)
                {
                    iFound = new IntNeuron() { Value = 0 };
                    Brain.Current.Add(iFound);
                    SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Height, iFound);
                }
                return iFound.Value;
            }
            set
            {
                IntNeuron iFound = FindFirstOut((ulong)PredefinedNeurons.Height) as IntNeuron;
                if (iFound == null)
                {
                    iFound = new IntNeuron() { Value = value };
                    Brain.Current.Add(iFound);
                    SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Height, iFound);
                }
                else
                    iFound.Value = value;
                ReBuildStartPoints(value, Width);
            }
        }

        #endregion Height

        #region Overrides

        /// <summary>
        /// Tries to translate the specified neuron to the output type of the Sin and send it to the outside world.
        /// </summary>
        /// <param name="toSend"></param>
        /// <remarks>
        /// <para>
        /// When this imageSin is being send back as output
        /// </para>
        /// </remarks>
        public override void Output(Neuron toSend)
        {
            if (toSend == this)
            {
                fIsProcessing = false;
                if (IsReady != null)
                    IsReady(this, new EventArgs());
            }
        }

        /// <summary>
        /// Called when the data needs to be saved.
        /// </summary>
        public override void Flush()
        {
            //don't need to do anything?
        }

        #endregion Overrides

        #region Process

        /// <summary>
        /// Tries to load an image from file and send this to the brain.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="proc">The processor to use for handling the file</param>
        public void Process(string filename, Processor proc)
        {
            if (File.Exists(filename) == true)
            {
                BitmapImage iImage = new BitmapImage(new Uri(filename));
                if (iImage != null)
                    Process(iImage, proc);
            }
            else
                throw new InvalidOperationException("File not found!");
        }

        /// <summary>
        /// Loads the specified source image into the brain after it is scaled to fit this sin.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="proc">The processor to use for handling the image.</param>
        /// <remarks>
        /// This function checks if the image needs to be resized and does this if needed.  It also makes
        /// certain that the image is of correct pixelformat which is important for loading the image.
        /// </remarks>
        public void Process(BitmapSource source, Processor proc)
        {
            if (source.PixelWidth != Width || source.PixelHeight != Height)                                 //need to resize
            {
                TransformedBitmap iScale = new TransformedBitmap();                           // Create the new BitmapSource that will be used to scale the size of the source.
                iScale.BeginInit();                                                           // BitmapSource objects like TransformedBitmap can only have their properties changed within a BeginInit/EndInit block.
                iScale.Source = source;
                iScale.Transform = new ScaleTransform(Width / source.PixelWidth, Height / source.PixelHeight);
                iScale.EndInit();
                source = iScale;
            }
            if (source.Format != PixelFormats.Bgra32 && source.Format != PixelFormats.Bgr24)                //need to change the pixel format.
            {
                FormatConvertedBitmap iConv = new FormatConvertedBitmap();
                iConv.BeginInit();
                iConv.Source = source;
                iConv.DestinationFormat = PixelFormats.Bgr24;
                iConv.EndInit();
            }
            InternalProcess(source, proc);
        }

        /// <summary>
        /// Internal load: translates the image into neurons.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="proc">The processor that will evaulate the image neurons.</param>
        /// <remarks>
        /// 	<para>
        /// Doesn't check the size or the pixelformat of the image.  This must already have been adjusted.
        /// </para>
        /// 	<para>
        /// The pixelformat expected is either Pbgra32 or Bgr24 (prefered, smaller data).
        /// </para>
        /// </remarks>
        private void InternalProcess(BitmapSource source, Processor proc)
        {
            if (fIsProcessing == true)
                throw new InvalidOperationException("Previous image has not yet been processed.");
            else
                fIsProcessing = true;

            NeuronCluster iEntry = FindFirstOut((ulong)PredefinedNeurons.EntryPoints) as NeuronCluster;
            if (iEntry == null)                                                                       //if for some reason, the entrypoints have not yet been created, do it now.  Should not be required though.
                ReBuildStartPoints(Height, Width);
            Debug.Assert(iEntry != null);

            int iWidth = source.PixelWidth;
            int iStride = iWidth * ((source.Format.BitsPerPixel + 7) / 8);
            byte[] iPixels = new byte[source.PixelHeight * iStride];                                //need a byte for each pixel in each collumn and row + for each color.
            source.CopyPixels(iPixels, iStride, 0);

            if (source.Format == PixelFormats.Bgr24)
                ProcessPixels(iEntry, iPixels, 3);
            else if (source.Format == PixelFormats.Bgra32)
                ProcessPixels(iEntry, iPixels, 4);
            else
                throw new InvalidOperationException("Image format not supported by ImageSin, only Bgra32 or Bgr24 are supported.");
            Process(this, proc, source.ToString());                     //an imagesin solves itself. This is because all the data is in a cluster as a relationship.
        }

        private void ProcessPixels(NeuronCluster entry, byte[] pixels, int byteCount)
        {
            using (ChildrenAccessor iList = entry.Children)
            {
                for (int i = 0; i < pixels.Length; i += byteCount)
                {
                    NeuronCluster iLine = Brain.Current[iList[i]] as NeuronCluster;
                    using (ChildrenAccessor iLineList = iLine.Children)
                        foreach (ulong iPixelId in iLineList)
                        {
                            Neuron iPixel = Brain.Current[iPixelId];
                            SetPixel(iPixel, pixels[i], pixels[i + 1], pixels[i + 2]);                 //first link must be blue, second link must be green, third red.
                        }
                }
            }
        }

        #endregion Process

        /// <summary>
        /// Sets the colors of a single pixel neuron.
        /// </summary>
        /// <param name="pixel">The pixel.</param>
        /// <param name="blue">The blue value.</param>
        /// <param name="green">The green value.</param>
        /// <param name="red">The red value.</param>
        private void SetPixel(Neuron pixel, byte blue, byte green, byte red)
        {
            using (LinksAccessor iLinksOut = pixel.LinksOut)
            {
                ((IntNeuron)iLinksOut.Items[0].To).Value = blue;
                ((IntNeuron)iLinksOut.Items[1].To).Value = green;
                ((IntNeuron)iLinksOut.Items[2].To).Value = red;
                ((IntNeuron)iLinksOut.Items[3].To).Value = blue + green + red;  //final link is gray.
            }
        }

        /// <summary>
        /// Builds the neurons that function as the starting points for images.
        /// </summary>
        /// <remarks>
        /// Rows are added/removed as needed.
        /// </remarks>
        private void ReBuildStartPoints(int height, int width)
        {
            NeuronCluster iEntry = FindFirstOut((ulong)PredefinedNeurons.EntryPoints) as NeuronCluster;
            if (iEntry == null)
            {
                iEntry = new NeuronCluster();
                Brain.Current.Add(iEntry);
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.EntryPoints, iEntry);
            }

            UpdateHeight(height, width, iEntry);
            UpdateWidth(width, iEntry);
            NeuronCluster iToCall = FindFirstOut((ulong)PredefinedNeurons.EntryPointsCreated) as NeuronCluster;
            if (iToCall != null)
            {
                Processor iProc = new Processor();
                iProc.Call(iToCall);
            }
        }

        /// <summary>
        /// Updates the widths of all the lines in the entrypoints cluster.
        /// </summary>
        /// <param name="width">The width.</param>
        /// <param name="entry">The list of lines.</param>
        private void UpdateWidth(int width, NeuronCluster lines)
        {
            using (ChildrenAccessor iList = lines.Children)
            {
                foreach (ulong i in iList)                                                                        //after the lines are updated, we can update all the lines who haven't yet have the proper width.  The first time we meet one that does, we can quit cause it means that the widh hasnt' changed, or we have reached the new lines.
                {
                    NeuronCluster iCluster = Brain.Current[i] as NeuronCluster;
                    Debug.Assert(iCluster != null);
                    using (ChildrenAccessor iClusterList = iCluster.ChildrenW)
                    {
                        if (iClusterList.Count == width)
                            break;
                        else if (iClusterList.Count > width)
                        {
                            while (iClusterList.Count > width)
                                RemoveLastPixel(iClusterList);
                        }
                        else
                        {
                            while (iClusterList.Count < width)
                                AddPixel(iCluster, iClusterList);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Updates the nr of lines in the entry point cluster..
        /// </summary>
        /// <param name="height">The height: nr of lines.</param>
        /// <param name="width">The width: nr of pixels per line</param>
        /// <param name="lines">the list of lines</param>
        private void UpdateHeight(int height, int width, NeuronCluster lines)
        {
            using (ChildrenAccessor iList = lines.ChildrenW)
            {
                if (iList.Count > height)                                                                            //first handle the lines.
                {
                    while (iList.Count > height)
                    {
                        NeuronCluster iLast = Brain.Current[iList[iList.Count - 1]] as NeuronCluster;        //this is the last line
                        Debug.Assert(iLast != null);
                        using (ChildrenAccessor iListOfLast = iLast.Children)
                        {
                            while (iListOfLast.Count > 0)
                                RemoveLastPixel(iListOfLast);
                        }
                        iList.RemoveAt(iList.Count - 1);
                        Brain.Current.Delete(iLast);
                    }
                }
                else
                {
                    while (iList.Count < height)
                    {
                        NeuronCluster iNew = new NeuronCluster();
                        Brain.Current.Add(iNew);
                        iList.Add(iNew);
                        using (ChildrenAccessor iListOfNew = iNew.ChildrenW)
                        {
                            for (int i = 0; i < width; i++)
                                AddPixel(iNew, iList);
                        }
                    }
                }
            }
        }

        private void RemoveLastPixel(ChildrenAccessor list)
        {
            Neuron iPixel = Brain.Current[list[list.Count - 1]];
            Neuron iColor = iPixel.FindFirstOut((ulong)PredefinedNeurons.Red);
            Brain.Current.Delete(iColor);                                                           //this will also remove the links.
            iColor = iPixel.FindFirstOut((ulong)PredefinedNeurons.Green);
            Brain.Current.Delete(iColor);
            iColor = iPixel.FindFirstOut((ulong)PredefinedNeurons.Blue);
            Brain.Current.Delete(iColor);
            iColor = iPixel.FindFirstOut((ulong)PredefinedNeurons.Gray);
            Brain.Current.Delete(iColor);
            list.Mode = AccessorMode.Write;                                                        //need to make the write mode as small as possible so that it doesn't block any other actions in this code.
            list.RemoveAt(list.Count - 1);
            list.Mode = AccessorMode.Read;                                                         //and set it back again, so other code items are also able to read it.
            Brain.Current.Delete(iPixel);
        }

        /// <summary>
        /// Adds an empty pixel to the cluster.
        /// </summary>
        /// <param name="cluster">The cluster to add a pixel to as a child.</param>
        /// <param name="list">The list to add the pixel to (usually the list of the cluster).  This is provided so that the accessor
        /// can be shared across calls.  Must be writable.</param>
        private void AddPixel(NeuronCluster cluster, ChildrenAccessor list)
        {
            Neuron iPixel = new Neuron();
            Brain.Current.Add(iPixel);
            list.Add(iPixel);
            IntNeuron iColor = new IntNeuron();
            iPixel.SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Blue, iColor);
            iColor = new IntNeuron();
            iPixel.SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Green, iColor);
            iColor = new IntNeuron();
            iPixel.SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Red, iColor);
            iColor = new IntNeuron();
            iPixel.SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Gray, iColor);
        }
    }
}