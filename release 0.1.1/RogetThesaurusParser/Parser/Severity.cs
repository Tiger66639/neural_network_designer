using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNetworkDesigne.RogetThesaurus.Parser
{
   // Summary:
   //     Specifies the severity of a parser error in a language service.
   public enum Severity
   {
      
      /// <summary>
      /// Indicates a comment returned by the parser.
      /// </summary>
      Hint = 0,
      
      /// <summary>
      /// Indicates a warning returned by the parser.
      /// </summary>
      Warning = 1,
      
      /// <summary>
      /// Indicates an error in parsing, however, parsing may have been able to continue.
      /// </summary>
      Error = 2,
      
      /// <summary>
      /// Indicates a fatal error in parsing that prevented any further parsing.
      /// </summary>
      Fatal = 3,
   }
}
