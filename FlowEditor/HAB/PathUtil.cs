﻿using System;
using System.Collections.Specialized;
using System.IO;

namespace JaStDev.HAB
{
    public class PathUtil
    {
        /// <summary>
        ///     Creates a relative path from one fileor folder to another.
        /// </summary>
        /// <param name="fromDirectory">
        ///     Contains the directory that defines the start of the relative path.
        /// </param>
        /// <param name="toPath">
        ///     Contains the path that defines the endpoint of the relative path.
        /// </param>
        /// <returns>
        ///     The relative path from the start directory to the end path.
        /// </returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static string RelativePathTo(string fromDirectory, string toPath)
        {
            if (fromDirectory == null)
                throw new ArgumentNullException("fromDirectory");
            if (toPath == null)
                throw new ArgumentNullException("toPath");
            var isRooted = Path.IsPathRooted(fromDirectory) && Path.IsPathRooted(toPath);
            if (isRooted)
            {
                var isDifferentRoot = string.Compare(Path.GetPathRoot(fromDirectory), Path.GetPathRoot(toPath), true) !=
                                      0;
                if (isDifferentRoot)
                    return toPath;
            }
            var relativePath = new StringCollection();
            var fromDirectories = fromDirectory.Split(Path.DirectorySeparatorChar);
            var toDirectories = toPath.Split(Path.DirectorySeparatorChar);
            var length = Math.Min(fromDirectories.Length, toDirectories.Length);
            var lastCommonRoot = -1;
            // find common root
            for (var x = 0; x < length; x++)
            {
                if (string.Compare(fromDirectories[x], toDirectories[x], true) != 0)
                    break;
                lastCommonRoot = x;
            }
            if (lastCommonRoot == -1)
                return toPath;
            // add relative folders in from path
            for (var x = lastCommonRoot + 1; x < fromDirectories.Length; x++)
                if (fromDirectories[x].Length > 0)
                    relativePath.Add("..");
            // add to folders to path
            for (var x = lastCommonRoot + 1; x < toDirectories.Length; x++)
                relativePath.Add(toDirectories[x]);

            // create relative path
            var relativeParts = new string[relativePath.Count];
            relativePath.CopyTo(relativeParts, 0);
            var newPath = string.Join(Path.DirectorySeparatorChar.ToString(), relativeParts);
            return newPath;
        }

        /// <summary>
        ///     Checks if the specified string ends with a path delimiter char and if not, makes certin it does.
        /// </summary>
        /// <param name="path">The string.</param>
        /// <returns></returns>
        public static string VerifyPathEnd(string path)
        {
            if (string.IsNullOrEmpty(path) == false && path[path.Length - 1] != Path.DirectorySeparatorChar)
                return path + Path.DirectorySeparatorChar;
            return path;
        }
    }
}