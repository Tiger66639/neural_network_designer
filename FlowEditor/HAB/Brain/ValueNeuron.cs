﻿namespace JaStDev.HAB.Brain
{
    /// <summary>
    /// A base class for all neurons that can contain a value, like a string, double or int.
    /// </summary>
    /// <remarks>
    /// This class is abstract cause it can't be created on it's own.
    /// </remarks>
    public abstract class ValueNeuron : Neuron
    {
    }
}