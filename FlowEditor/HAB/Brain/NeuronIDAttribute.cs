﻿using System;

namespace JaStDev.HAB.Brain
{
    /// <summary>
    /// An attribute that can be applied to classes to indicate that the <see cref="Neuron"/>
    /// should be automatically created for an empty brain, using the specified <see cref="Neuron.ID"/>
    /// value(s). You can optionally also define which type to use for creating the neuron, if no type is
    /// specified, the type to which the attribute is supplied is used.
    /// Multiple attributes for the same type are allowed.
    /// </summary>
    /// <remarks>
    /// This attribute is used to declare 'known' neurons such as <see cref="PopInstruction"/> or some
    /// <see cref="TextNeuron"/>s.
    /// </remarks>
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public class NeuronIDAttribute : Attribute
    {
        public NeuronIDAttribute(ulong id)
        {
            ID = id;
        }

        public NeuronIDAttribute(ulong id, Type type)
        {
            ID = id;
            Type = type;
        }

        /// <summary>
        /// Gets/sets the idea to use
        /// </summary>
        public ulong ID { get; set; }

        //There should come a check for the correct type here
        /// <summary>
        /// Gets/sets the type of neuron to create.
        /// </summary>
        public Type Type { get; set; }
    }
}