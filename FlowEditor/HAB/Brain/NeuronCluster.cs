﻿using JaStDev.HAB.Brain.Lists;
using JaStDev.HAB.Brain.Thread_syncing;
using JaStDev.HAB.Events;
using JaStDev.HAB.Storage;
using System.Collections.Generic;
using System.Xml;

namespace JaStDev.HAB.Brain
{
    /// <summary>
    /// A <see cref="Neuron"/> that represents a set of other neurons.
    /// </summary>
    /// <remarks>
    /// <para>
    /// For instance, all the neurons
    /// that have the same link meaning in a <see cref="Neuron.LinksTo"/> list, or a global neuron that
    /// has many specific instances, like the neuroncluster representing the name 'Jan', with specific
    /// children, me, and my current neighbour, Jan De Ceeser (or something).
    /// A NeuronCluster can also represent a sequence (of digits to form a number for instance) or a
    /// logical relation between other neurons (jan and tom).  A custer has a
    /// <see cref="NeuronCluster.Meaning"/> property that defines the meaning of the cluster.
    /// Another possible meaning can be 'function', in which case it should contain <see cref="Expression"/> neurons.
    /// </para>
    /// <para>
    /// All operations on the <see cref="NeuronCluster.Children"/> list should be locked.
    /// </para>
    /// </remarks>
    //[XmlRoot("NeuronCluster", Namespace = "http://tiger66639.clan.rip", IsNullable = false)]
    [NeuronID((ulong)PredefinedNeurons.NeuronCluster, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Object, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Frame, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.FrameEvokers, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.FrameSequences, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.FrameSequence, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.FrameElementId, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.FrameImportance, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Frame_Core, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Frame_peripheral, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Frame_extra_thematic, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.LemmaId, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Flow, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.FlowItemConditionalPart, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.FlowItemConditional, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.FlowItemIsLoop, typeof(Neuron))]
    public class NeuronCluster : Neuron
    {
        private ChildList fChildren;
        private ulong fMeaning = Neuron.EmptyId;

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="NeuronCluster"/> class.
        /// </summary>
        /// <remarks>
        /// This constructor is public for streaming, should be fixed later on
        /// </remarks>
        public NeuronCluster()
        {
            fChildren = new ChildList(this);
        }

        #endregion ctor

        #region prop

        #region Children

        /// <summary>
        /// Gets a thread safe list of child neuron id's in this cluster.
        /// </summary>
        /// </remarks>
        public ChildrenAccessor Children
        {
            get
            {
                return new ChildrenAccessor(fChildren, AccessorMode.Read, fChildren.Lock);
            }
        }

        /// <summary>
        /// Gets the children accessor in a  writable mode, which is faster for editing.
        /// </summary>
        /// <value>The children W.</value>
        public ChildrenAccessor ChildrenW
        {
            get
            {
                return new ChildrenAccessor(fChildren, AccessorMode.Write, fChildren.Lock);
            }
        }

        public ChildrenAccessor GetChildrenW()
        {
            return new ChildrenAccessor(fChildren, AccessorMode.Write, fChildren.Lock);
        }

        #endregion Children

        #region Meaning

        /// <summary>
        /// Gets/sets the meaning of the relationship between the different items in this cluster.
        /// </summary>
        /// <remarks>
        /// Examples of meanings are: or, and, number, sequence.
        /// <para>
        /// This also keeps track of the <see cref="Neuron.MeaningUsageCount"/> whenever the meaning of this
        /// cluster is changed.
        /// </para>
        /// </remarks>
        public ulong Meaning
        {
            get
            {
                return fMeaning;
            }
            set
            {
                if (fMeaning != value)
                {
                    if (fMeaning != Neuron.EmptyId)
                        Brain.Current[fMeaning].MeaningUsageCount--;
                    fMeaning = value;
                    if (fMeaning != Neuron.EmptyId)
                        Brain.Current[fMeaning].MeaningUsageCount++;
                    if (Brain.Current.HasNeuronChangedEvents)
                        Brain.Current.OnNeuronChanged(new NeuronPropChangedEventArgs("Meaning", this));
                }
            }
        }

        #endregion Meaning

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.NeuronCluster"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.NeuronCluster];
            }
        }

        #endregion TypeOfNeuron

        #endregion prop

        #region Functions

        /// <summary>
        /// Clears all the data from this instance.
        /// </summary>
        /// <remarks>
        /// This function is automically called when a neuron is deleted.
        /// This includes incomming and outgoing links, clustered by, children (if it is a clusterr), and any possible values.
        /// <para>
        /// Descendents can enhance this function and clean more data.
        /// </para>
        /// </remarks>
        public override void Clear()
        {
            base.Clear();
            Meaning = Neuron.EmptyId;                                               //we also reset the meaning, this clears the meaning counter.
            RemoveChildren();
        }

        /// <summary>
        /// Removes the 'ClusteredBy' reference from all the children of a cluster.
        /// </summary>
        /// <param name="toDelete">To delete.</param>
        private void RemoveChildren()
        {
            IList<Neuron> iToRemove;
            using (ChildrenAccessor iList = Children)
                iToRemove = iList.ConvertTo<Neuron>();
            foreach (Neuron i in iToRemove)                                            //can't put foreach in using loop, cause the RemoveClusteredBy does a write lock.
                i.RemoveClusteredBy(this);
            using (ChildrenAccessor iList = Children)
                iList.Clear();
        }

        /// <summary>
        /// A thread save way to add neurons to the list of children.
        /// </summary>
        /// <remarks>
        /// Items will be added at the end of the list.
        /// </remarks>
        /// <param name="toAdd"></param>
        public void AddChildren(Neuron[] toAdd)
        {
            lock (fChildren)
            {
                foreach (Neuron i in toAdd)
                    fChildren.Add(i);
            }
        }

        ///// <summary>
        ///// optimized functio for adding items directly from the stack of a processor to this cluster.
        ///// this is thread save.
        ///// </summary>
        ///// <remarks>
        ///// When done, the items are removed from the stack.
        ///// </remarks>
        ///// <param name="list">The stack from which to add items.</param>
        ///// <param name="count">The number of items to consume from the stack.</param>
        //internal void AddChidrenFromStack(Stack<Neuron> list, int count)
        //{
        //   lock (fChildren)
        //   {
        //      while (count > 0)
        //      {
        //         Neuron iTemp = list.Pop();
        //         fChildren.Add(iTemp.ID);
        //         count--;
        //      }
        //   }
        //}

        protected override void CopyTo(Neuron copyTo)
        {
            lock (fChildren)
            {
                base.CopyTo(copyTo);
                if (copyTo is NeuronCluster)
                    ((NeuronCluster)copyTo).fChildren.AddRange(fChildren);
            }
        }

        public override void ReadXml(XmlReader reader)
        {
            base.ReadXml(reader);
            bool iIsEmpty;
            if (reader.LocalName == "Meaning")                                                                    //the very original version forgot to save the meaning value, so we need to check if the element exists.
                fMeaning = XmlStore.ReadElement<ulong>(reader, "Meaning");
            iIsEmpty = reader.IsEmptyElement;
            reader.ReadStartElement("Children");
            if (iIsEmpty == false)
            {
                while (reader.NodeType != XmlNodeType.EndElement)
                {
                    ulong iChild = XmlStore.ReadElement<ulong>(reader, "ID");
                    fChildren.AddFromLoad(iChild);
                    reader.MoveToContent();
                }
                reader.ReadEndElement();
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            base.WriteXml(writer);
            XmlStore.WriteElement<ulong>(writer, "Meaning", Meaning);
            XmlStore.WriteIDList(writer, "Children", fChildren);
        }

        #endregion Functions

        /// <summary>
        /// Determines whether the specified list is this child list.
        /// </summary>
        /// <param name="list">The list.</param>
        internal bool IsChildList(BrainList list)
        {
            return fChildren == list;
        }
    }
}