﻿using JaStDev.HAB.Events;
using System.Xml;

namespace JaStDev.HAB.Brain
{
    /// <summary>
    ///     A <see cref="Neuron" /> which is used by the <see cref="TextSin" /> to represent it's context specific
    ///     data.  More specifically, it also stores the text that the neuron represents.
    /// </summary>
    /// <remarks>
    ///     This class returns the value for <see cref="TextNeuron.Text" /> for it's <see cref="TextNeuron.ToString" /> method.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.TextNeuron, typeof(Neuron))]
    public class TextNeuron : ValueNeuron
    {
        private string fText;

        #region Text

        /// <summary>
        ///     Gets/sets the text that the neuron respresents.
        /// </summary>
        public string Text
        {
            get { return fText; }
            set
            {
                if (fText != value)
                {
                    fText = value;
                    IsChanged = true;
                    if (Brain.Current.HasNeuronChangedEvents)
                        Brain.Current.OnNeuronChanged(new NeuronPropChangedEventArgs("Text", this));
                }
            }
        }

        #endregion Text

        #region TypeOfNeuron

        /// <summary>
        ///     Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.TextNeuron" />.</value>
        public override Neuron TypeOfNeuron
        {
            get { return Brain.Current[(ulong)PredefinedNeurons.TextNeuron]; }
        }

        #endregion TypeOfNeuron

        /// <summary>
        ///     Clears all the data from this instance.
        /// </summary>
        /// <remarks>
        ///     This function is automically called when a neuron is deleted.
        ///     This includes incomming and outgoing links, clustered by, children (if it is a clusterr), and any possible values.
        ///     <para>
        ///         Descendents can enhance this function and clean more data.
        ///     </para>
        /// </remarks>
        public override void Clear()
        {
            base.Clear();
            Text = null;
        }

        public override void ReadXml(XmlReader reader)
        {
            var iPrev = WhitespaceHandling.None;
            base.ReadXml(reader);
            if (reader is XmlTextReader)
            //we must make certain that we read all the white spaces correctly, we can only set this on an XmlTextreader.
            {
                iPrev = ((XmlTextReader)reader).WhitespaceHandling;
                ((XmlTextReader)reader).WhitespaceHandling = WhitespaceHandling.All;
            }
            reader.ReadStartElement("Text");
            fText = reader.ReadString();
            if (reader is XmlTextReader)
                ((XmlTextReader)reader).WhitespaceHandling = iPrev;
            reader.ReadEndElement();
        }

        /// <summary>
        ///     Gets the text representation of the class.
        /// </summary>
        /// <returns>Returns the <see cref="TextNeuron.Text" /> value.</returns>
        public override string ToString()
        {
            return Text;
        }

        /// <param name="writer">The xml writer to use</param>
        public override void WriteXml(XmlWriter writer)
        {
            base.WriteXml(writer);

            writer.WriteStartElement("Text");
            writer.WriteString(Text);
            writer.WriteEndElement();
        }

        /// <param name="right">The neuron to compare it with.</param>
        /// <param name="op">The operator to use.</param>
        /// <returns>True if the operator is correct.</returns>
        protected internal override bool CompareWith(Neuron right, Neuron op)
        {
            var iRight = right as TextNeuron;
            if (iRight != null)
            {
                var iVal = ((TextNeuron)right).Text;
                switch (op.ID)
                {
                    case (ulong)PredefinedNeurons.Equal:
                        return Text == iRight.Text;

                    case (ulong)PredefinedNeurons.Smaller:
                        return string.Compare(Text, iRight.Text) < 0;

                    case (ulong)PredefinedNeurons.SmallerOrEqual:
                        return string.Compare(Text, iRight.Text) <= 0;

                    case (ulong)PredefinedNeurons.Bigger:
                        return string.Compare(Text, iRight.Text) > 0;

                    case (ulong)PredefinedNeurons.BiggerOrEqual:
                        return string.Compare(Text, iRight.Text) >= 0;

                    case (ulong)PredefinedNeurons.Different:
                        return Text != iRight.Text;

                    default:
                        Log.Log.LogError("TextNeuron.CompareWith", string.Format("Invalid operator found: {0}.", op));
                        return false;
                }
            }
            return base.CompareWith(right, op);
        }

        /// <summary>
        ///     Copies all the data from this neuron to the argument.
        /// </summary>
        /// <param name="copyTo">The object to copy their data to.</param>
        /// <remarks>
        ///     By default, it only copies over all of the links (which includes the 'LinksOut' and 'LinksIn' lists.
        ///     <para>
        ///         Inheriters should reimplement this function and copy any extra information required for their specific type
        ///         of neuron.
        ///     </para>
        /// </remarks>
        protected override void CopyTo(Neuron copyTo)
        {
            base.CopyTo(copyTo);
            var iCopyTo = copyTo as TextNeuron;
            if (iCopyTo != null)
                iCopyTo.fText = fText;
        }

        #region ctor

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextNeuron" /> class.
        /// </summary>
        public TextNeuron()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextNeuron" /> class.
        /// </summary>
        /// <param name="text">The text to wrap.</param>
        public TextNeuron(string text)
        {
            Text = text;
        }

        #endregion ctor
    }
}