﻿namespace JaStDev.HAB.Brain
{
    /// <summary>
    /// Defines all the Id's of the neurons that are known by the system + 1 extra item to declare the start of the
    /// dynamic list.  This is used by the <see cref="Brain"/> to initialize the start of the dynamic neuron id count.
    /// </summary>
    public enum PredefinedNeurons : ulong
    {
        //instructions
        PopInstruction = 2, //0 and 1 are empty and temporary

        PushInstruction,
        AddLinkInstruction,
        AddInfoInstruction,
        AddChildInstruction,
        IndexOfChildInstruction,
        IndexOfInfoInstruction,
        IndexOfLinkInstruction,
        CountInstruction,
        CompleteSequenceInstruction,
        NewInstruction,
        OutputInstruction,
        CopyChildrenInstruction,
        CopyInfoInstruction,
        DeleteInstruction,
        DuplicateInstruction,
        MoveChildrenInstruction,
        MoveInfoInstruction,
        PeekInstruction,
        RemoveChildInstruction,
        RemoveLinkInstruction,
        RemoveInfoInstruction,
        InsertChildInstruction,
        InsertLinkInstruction,
        InsertInfoInstruction,
        MakeClusterInstruction,
        ChangeLinkTo,
        ChangeLinkFrom,
        ChangeLinkMeaning,

        //default neurons

        /// <summary>
        /// ID for the neuron used as a link meaning to indicate the neuron local entry points of a <see cref="Sin"/>.
        /// </summary>
        EntryPoints,

        /// <summary>
        /// ID for the neuron used a s link meaning to indicate the cluster that should be called (executed) whenever the sin local
        /// entrypoints have been recreated (to build further links for instance).
        /// </summary>
        EntryPointsCreated,

        /// <summary>
        /// This neuron is used by the <see cref="TextSin"/> to indicate the presence of a word in an input stream that was found in the dict.
        /// This is used as the meaning of a link when it adds words to a <see cref="KnoledgeNeuron"/>.  This is resolved from there on.
        /// </summary>
        ContainsWord,

        /// <summary>
        /// Identifies a neuron used as the meaning of a link pointing to a cluster containing letters (where the letter is expressed as
        /// an int neuron).
        /// </summary>
        LetterSequence,

        /// <summary>
        /// This neuron is used by the <see cref="TextSin"/> to indicate the presence of a double in an input stream This is used as the meaning
        /// of a link when it adds words to a <see cref="KnoledgeNeuron"/>.  This is resolved from there on.
        /// </summary>
        ContainsDouble,

        /// <summary>
        /// This neuron is used by the <see cref="TextSin"/> to indicate the presence of an int in an input stream. This is used as the meaning
        /// of a link when it adds words to a <see cref="KnoledgeNeuron"/>.  This is resolved from there on.
        /// </summary>
        ContainsInt,

        /// <summary>
        /// Id for the neuron used as link meaning to indicate the red color of a pixel in an image.
        /// </summary>
        Red,

        /// <summary>
        /// Id for the neuron used as link meaning to indicate the green color of a pixel in an image.
        /// </summary>
        Green,

        /// <summary>
        /// Id for the neuron used as link meaning to indicate the blue color of a pixel in an image.
        /// </summary>
        Blue,

        /// <summary>
        /// Id for the neuron used as link meaning to indicate the gray value of a pixel in an image.
        /// </summary>
        Gray,

        /// <summary>
        /// Id for the neuron used a a link meaning to indicate the width of something.  This is used by the <see cref="ImageSin"/>
        /// to indicate the size of the image.
        /// </summary>
        Width,

        /// <summary>
        /// Id for the neuron used a a link meaning to indicate the Height of something.  This is used by the <see cref="ImageSin"/>
        /// to indicate the size of the image.
        /// </summary>
        Height,

        /// <summary>
        /// Identifies the <see cref="ByRefExpression"/>
        /// </summary>
        ByRefExpression,

        /// <summary>
        /// Identifies the sensory interface that provides acces to the WordNet text database.
        /// </summary>
        WordNetSin,

        /// <summary>
        /// Used as the meaning for a link between a neuron and a cluster containing expressions that will be executed when the link is being solved.
        /// </summary>
        Rules,

        /// <summary>
        /// A neuron that identifies a <see cref="NeuronCluster"/> as an actions list used by a neuron as the list that contains all the expressions
        /// used to evaluate what to do after a neuron was processed by the processor.
        /// </summary>
        Actions,

        /// <summary>
        /// A neuron id that identifies a link to a neuron that is to be used as the condition of a <see cref="ConditionalExpression"/>
        /// or the cluster containing all the ConditionalExpressions of a <see cref="ConditionalGroup"/>.
        /// </summary>
        Condition,

        /// <summary>
        /// A neuron id for the neuron used as the <see cref="NeuronCluster.Meaning"/> of a cluster of expressions (a code list).
        /// </summary>
        Code,

        /// <summary>
        /// A neuron used as the meaninf for a link to a <see cref="NeuronCluster"/> used to hold sub expressions of a
        /// <see cref="ConditionalExpression"/>
        /// </summary>
        Statements,

        /// <summary>
        /// A neuron used as the meaning for a link to another neuron by a <see cref="BoolExpression"/> to identify it's operator.
        /// the link usually points to <see cref="PredefinedNeurons.Equals"/>, <see cref="PredefinedNeurons.Smaller"/>,
        /// <see cref="PredefinedNeurons.Bigger"/>, <see cref="PredefinedNeurons.Different"/>, <see cref="PredefinedNeurons.Or"/>,
        /// or <see cref="PredefinedNeurons.And"/>
        /// </summary>
        Operator,

        /// <summary>
        /// Identifies neuron used as meaning for link between a boolexpression and it's left part.
        /// </summary>
        LeftPart,

        /// <summary>
        /// Identifies neuron used as meaning for link between a boolexpression and it's right part.
        /// </summary>
        RightPart,

        /// <summary>
        /// Identifies a neuron that means the equal operator '==' used by <see cref="BoolExpression"/>
        /// </summary>
        Equal,

        /// <summary>
        /// Identifies a neuron that means the Less than operator  used by <see cref="BoolExpression"/>
        /// </summary>
        Smaller,

        /// <summary>
        /// Identifies a neuron that means the bigger than operator  used by <see cref="BoolExpression"/>
        /// </summary>
        Bigger,

        /// <summary>
        /// Identifies a neuron that means the Less than or equal operator  used by <see cref="BoolExpression"/>
        /// </summary>
        SmallerOrEqual,

        /// <summary>
        /// Identifies a neuron that means the bigger than or equal operator  used by <see cref="BoolExpression"/>
        /// </summary>
        BiggerOrEqual,

        /// <summary>
        /// Identifies a neuron that means the not equal operator  (!=) used by <see cref="BoolExpression"/>
        /// </summary>
        Different,

        /// <summary>
        /// Identifies a neuron that means the 'Contains' operator used by <see cref="BoolExpression"/>
        /// </summary>
        Contains,

        /// <summary>
        /// Identifies the neuron that means the boolean 'True' value, used by <see cref="BoolExpression"/>.
        /// </summary>
        True,

        /// <summary>
        /// Identifies the neuron that means the boolean 'False' value, used by <see cref="BoolExpression"/>.
        /// </summary>
        False,

        /// <summary>
        /// Identifies the neuron used to provide a meaning to the link used between a <see cref="Statement"/> and
        /// the <see cref="Instruction"/> it performs.
        /// </summary>
        /// <remarks>
        /// This is also used as the meaning of a link from a neuron sent to a sin as output, to a neuron that reflects
        /// the instruction that the sin should perform.
        /// </remarks>
        Instruction,

        /// <summary>
        /// Identifies the neuron used to provide a meaning to the link between a <see cref="Statement"/> and
        /// the <see cref="NeuronCluster"/> containing all the arguments for the statement.
        /// </summary>
        /// <remarks>
        /// This is also used as the meaning of a link from a neuron sent to a sin as output, to a neuron that represents an
        /// the argument for the instruction.
        /// Also used by <see cref="ReflectionSin"/> as the meaning for a link from an output neuron to a cluster containing
        /// all the arguments of function to call/generate.
        /// </remarks>
        Arguments,

        /// <summary>
        /// Identifies the neuron used to provide a meaning to the link used between a <see cref="ConditionalGroup"/> and
        /// the <see cref="Neuron"/> that determins how the conditional group should loop: not, Case, Looped, CaseLooped, ForEach,
        /// For, Until.
        /// </summary>
        LoopStyle,

        /// <summary>
        /// Identifies the neuron used as link meaning to identify the link to the VariableExpression that holds the current item
        /// in a loop (for or foreach loop).
        /// </summary>
        LoopItem,

        /// <summary>
        /// Identifies the neuron used as link meaning to identify the link to the VariableExpression that holds the item to check
        /// in a case statement.
        /// </summary>
        CaseItem,

        /// <summary>
        /// Identifies the neuron used to let a <see cref="ConditionalGroup"/> know he should not loop, but perform a normal if statement.
        /// </summary>
        Normal,

        /// <summary>
        /// Identifies the neuron used to let a <see cref="ConditionalGroup"/> know he should use a case statement.
        /// </summary>
        Case,

        /// <summary>
        /// Identifies the neuron used to let a <see cref="ConditionalGroup"/> know he should loop using a regular while xxx do yyy.
        /// </summary>
        Looped,

        /// <summary>
        /// Identifies the neuron used to let a <see cref="ConditionalGroup"/> know he should loop and evaulate the conditional expressions
        /// like a case, loop stops when no case matches.
        /// </summary>
        CaseLooped,

        /// <summary>
        /// Identifies the neuron used to let a <see cref="ConditionalGroup"/> know he should use a foreach style loop.
        /// </summary>
        ForEach,

        ///// <summary>
        ///// Identifies the neuron used to let a <see cref="ConditionalGroup"/> know he should use a for(x;y;z) style loop.
        ///// </summary>
        //For,

        /// <summary>
        /// Identifies the neuron used to let a <see cref="ConditionalGroup"/> know he should loop with the conditional check at the end.
        /// </summary>
        Until,

        /// <summary>
        /// Identifies the neuron used as the meaning for a link between a <see cref="Variable"/> and it's initial <see cref="Variable.Value"/>.
        /// </summary>
        Value,

        /// <summary>
        /// Identifies the neuron used as the meaning for a link between a <see cref="SearchExpression"/> and it's
        /// <see cref="SearchExpression.SearchFor"/>.
        /// </summary>
        SearchFor,

        /// <summary>
        /// Identifies the neuron used as the meaning for a link between a <see cref="SearchExpression"/> and it's
        /// <see cref="SearchExpression.ListToSearch"/>.
        /// Also between a <see cref="BoolExpression"/> and <see cref="BoolExpression.ListToSearch"/>
        /// </summary>
        ListToSearch,

        /// <summary>
        /// Identifies the neuron used as the meaning for a link between a <see cref="SearchExpression"/> and it's
        /// <see cref="SearchExpression.ToSearch"/>.
        /// </summary>
        ToSearch,

        /// <summary>
        /// used by the 'Contains' operator to indicate that the list of children should be checked.
        /// </summary>
        Children,

        /// <summary>
        /// Identifies the neuron used as the meaning from a <see cref="ReflectionSin"/> to a neuron indicating the state of the
        /// sin, which determins how output is processed. This can be <see cref="PredefinedNeurons.Cil"/> or <see cref="PredefinedNeurons.Call"/>
        /// </summary>
        ReflectionSinState,

        /// <summary>
        /// Identifies the neuron used in <see cref="SearchExpression.ListToSearch"/> to indicate that the <see cref="Neuron.LinksIn"/>
        /// list should be searched.
        /// </summary>
        In,

        /// <summary>
        /// Identifies the neuron used in <see cref="SearchExpression.ListToSearch"/> to indicate that the <see cref="Neuron.LinksOut"/>
        /// list should be searched.
        /// </summary>
        Out,

        /// <summary>
        /// Identifies the neuron used in <see cref="SearchExpression.SearchExpression"/> to indicate which info to search for (in the
        /// info list).
        /// </summary>
        InfoToSearchFor,

        /// <summary>
        /// Identifies the neuron that represents a verb (be it textual or in any other way).  Used by the thesaurus.
        /// </summary>
        Verb,

        /// <summary>
        /// Identifies the neuron that represents a noun (be it textual or in any other way).  Used by the thesaurus.
        /// </summary>
        Noun,

        /// <summary>
        /// Identifies the neuron that represents an adjective (be it textual or in any other way).  Used by the thesaurus.
        /// </summary>
        Adjective,

        /// <summary>
        /// Identifies the neuron that represents an adverb (be it textual or in any other way).  Used by the thesaurus.
        /// </summary>
        Adverb,

        /// <summary>
        /// Identifies the neuron that represents the <see cref="Neuron"/> type.  This is used by <see cref="NewInstruction"/> to
        /// find out which type of neuron it should create.
        /// </summary>
        Neuron,

        /// <summary>
        /// Identifies the neuron that represents the <see cref="NeuronCluster"/> type.  This is used by <see cref="NewInstruction"/> to
        /// find out which type of neuron it should create.
        /// </summary>
        NeuronCluster,

        /// <summary>
        /// Identifies the neuron that represents the <see cref="DoubleNeuron"/> type.  This is used by <see cref="NewInstruction"/> to
        /// find out which type of neuron it should create.
        /// </summary>
        DoubleNeuron,

        /// <summary>
        /// Identifies the neuron that represents the <see cref="IntNeuron"/> type.  This is used by <see cref="NewInstruction"/> to
        /// find out which type of neuron it should create.
        /// </summary>
        IntNeuron,

        /// <summary>
        /// Identifies the neuron that represents the <see cref="TextNeuron"/> type.  This is used by <see cref="NewInstruction"/> to
        /// find out which type of neuron it should create.
        /// </summary>
        TextNeuron,

        /// <summary>
        /// Identifies the neuron that represents the <see cref="BoolExpression"/> type.  This is used by <see cref="NewInstruction"/> to
        /// find out which type of neuron it should create.
        /// </summary>
        BoolExpression,

        /// <summary>
        /// Identifies the neuron that represents the <see cref="ConditionalExpression"/> type.  This is used by <see cref="NewInstruction"/> to
        /// find out which type of neuron it should create.
        /// </summary>
        ConditionalPart,

        /// <summary>
        /// Identifies the neuron that represents the <see cref="ConditionalStatement"/> type.  This is used by <see cref="NewInstruction"/> to
        /// find out which type of neuron it should create.
        /// </summary>
        ConditionalStatement,

        /// <summary>
        /// Identifies the neuron that represents the <see cref="Statement"/> type.  This is used by <see cref="NewInstruction"/> to
        /// find out which type of neuron it should create.
        /// </summary>
        Statement,

        /// <summary>
        /// Identifies the neuron that represents the <see cref="ResultStatement"/> type.  This is used by <see cref="NewInstruction"/> to
        /// find out which type of neuron it should create.
        /// </summary>
        ResultStatement,

        /// <summary>
        /// Identifies the neuron that represents the <see cref="SearchExpression"/> type.  This is used by <see cref="NewInstruction"/> to
        /// find out which type of neuron it should create.
        /// </summary>
        SearchExpression,

        /// <summary>
        /// Identifies the neuron that represents the <see cref="Variable"/> type.  This is used by <see cref="NewInstruction"/> to
        /// find out which type of neuron it should create.
        /// </summary>
        Variable,

        /// <summary>
        /// Identifies the neuron that represents the <see cref="Assignment"/> type.  This is used by <see cref="NewInstruction"/> to
        /// find out which type of neuron it should create.
        /// </summary>
        Assignment,

        /// <summary>
        /// Identifies the neuron that represents the 'empty' state.  This is a neuron that can be used as a return value for instructions
        /// to indicate no result. (<see cref="InInstruction"/> returns this value if it encountered an error).
        /// </summary>
        Empty,

        /// <summary>
        /// Identifies the neuron that represents the variable used to access the neuron found in the 'from' part of the link that is
        /// being executed in order to solve the neuron.  In other words, this is the neuron being solved.
        /// </summary>
        CurrentFrom,

        /// <summary>
        /// Identifies the neuron that represents the variable used to access the neuron found in the 'to' part of the link that is
        /// being executed in order to solve the neuron.
        /// </summary>
        CurrentTo,

        /// <summary>
        /// Identifies the neuron that represents the variable used to access the neuron found in the 'Meaning' part of the link that is
        /// being executed in order to solve the neuron.
        /// </summary>
        CurrentMeaning,

        /// <summary>
        /// Identifies the neuron that represents the variable used to access the neuron found in the 'Info' part of the link that is
        /// being executed in order to solve the neuron.
        /// </summary>
        CurrentInfo,

        /// <summary>
        /// Identifies the instruction <see cref="GetFirstOutInstruction"/>
        /// </summary>
        GetFirstOut,

        /// <summary>
        /// Identifies the instruction <see cref="SetFirstOutInstruction"/>
        /// </summary>
        SetFirstOut,

        /// <summary>
        /// Identifies the neuron that represents a preposition (be it textual or in any other way).  Usually used as a meaning.
        /// </summary>
        /// <remarks>
        /// A preposition links nouns, pronouns and phrases to other words in a sentence. The word or phrase that the preposition
        /// introduces is called the object of the preposition. A preposition usually indicates the temporal, spatial or logical
        /// relationship of its object to the rest of the sentence as in the following examples:
        /// The book is on the table.
        /// The book is beneath the table.
        /// The book is leaning against the table.
        /// The book is beside the table.
        /// </remarks>
        Preposition,

        /// <summary>
        /// Identifies the neuron that represents a Conjunction (be it textual or in any other way).  Usually used as a meaning.
        /// </summary>
        /// <remarks>
        /// You can use a conjunction to link words, phrases, and clauses, as in the following example:
        /// I ate the pizza and the pasta.
        /// Call the movers when you are ready.
        /// </remarks>
        Conjunction,

        /// <summary>
        /// Identifies the neuron that represents an Intersection (be it textual or in any other way).  Usually used as a meaning.
        /// </summary>
        /// <remarks>
        /// An interjection is a word added to a sentence to convey emotion. It is not grammatically related to any other part of the sentence.
        /// You usually follow an interjection with an exclamation mark. Interjections are uncommon in formal academic prose, except in
        /// direct quotations.The highlighted words in the following sentences are interjections:
        /// Ouch, that hurt!
        /// Oh no, I forgot that the exam was today.
        /// Hey! Put that down!
        /// </remarks>
        Intersection,

        /// <summary>
        /// Identies the neuron used as the meaning of a link to a letter of the alphabet expressed as an int.  This is used by the <see cref="TextSin"/>
        /// to process a string as a stream of letters.
        /// </summary>
        Letter,

        /// <summary>
        /// Identifies the neuron used as the meaning of a link from a <see cref="TimerSin"/> to a <see cref="DoubleNeuron"/> to indicate the time
        /// between 2 processing ticks.
        /// </summary>
        Interval,

        /// <summary>
        /// Idtentifies the neuron used as the meaning of a link from a neuron sent to a <see cref="TimerSin"/> as output, to either
        /// <see cref="PredefinedNeurons.True"/> or <see cref="PredefinedNeurons.False"/> to indicate that is should be acitve or not.
        /// </summary>
        IsActive,

        /// <summary>
        /// Identifies the neuron used as the meaning for a link between a sin and a cluster containing expressions that the sin will assign
        /// to all the data it sends for solving as input.
        /// </summary>
        ActionsForInput,

        /// <summary>
        /// Identifies the neuron that represents the <see cref="ExpressionsBlock"/> neuron type.  This is used by the <see cref="NewInstruction"/>.
        /// </summary>
        ExpressionsBlock,

        /// <summary>
        /// Identifies the variable that contains a reference to the current <see cref="Sin"/> which is the
        /// </summary>
        CurrentSin,

        /// <summary>
        /// Identifies the instruction <see cref="AwakeInstruction"/>
        /// </summary>
        AwakeInstruction,

        /// <summary>
        /// Identifies the instruction <see cref="SuspendInstruction"/>
        /// </summary>
        SuspendInstruction,

        /// <summary>
        /// Identifies the instruction <see cref="WarningInstruction"/>
        /// </summary>
        WarningInstruction,

        /// <summary>
        /// Identifies the instruction <see cref="ErrorInstruction"/>
        /// </summary>
        ErrorInstruction,

        /// <summary>
        /// Identifies a neuron that stands for the logical And operator '&&' used by <see cref="BoolExpression"/>
        /// </summary>
        And,

        /// <summary>
        /// Identifies a neuron that stands for the logical Or operator '||' used by <see cref="BoolExpression"/>
        /// </summary>
        Or,

        /// <summary>
        /// Identifies the neuron that is used by the 'Contains' operator to indicate that the list of owning clusters should be checked.
        /// </summary>
        Clusters,

        /// <summary>
        /// Identifies the instruction <see cref="ExecuteInstruction"/>
        /// </summary>
        ExecuteInstruction,

        /// <summary>
        /// Identifies the neuron used as the link between a <see cref="ByRefExpression"/> and it's argument.
        /// </summary>
        Argument,

        /// <summary>
        /// Identifies the instruction <see cref="GetChildrenInstruction"/>
        /// </summary>
        GetChildrenInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="TypeOfInstruction"/>
        /// </summary>
        TypeOfInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="GetClustersInstruction"/>
        /// </summary>
        GetClustersInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="GetClusterInstruction"/>
        /// </summary>
        GetFirClusterInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="GetChildrenOfTypeInstruction"/>
        /// </summary>
        GetChildrenOfTypeInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="GetClusterMeaningInstruction"/>
        /// </summary>
        GetClusterMeaningInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="SetClusterMeaningInstruction"/>
        /// </summary>
        SetClusterMeaningInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="ExitLinkInstruction"/>
        /// </summary>
        ExitLinkInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="SplitInstruction"/>
        /// </summary>
        SplitInstruction,

        /// <summary>
        /// Identifies the neuron used as the meaning of a cluster that contains the arguments for a statement or resultstatement.
        /// </summary>
        ArgumentsList,

        /// <summary>
        /// Identifies the neuron used as the meaning for a cluster that represents an object (cluster that represents a sinlge point
        /// of knoledge).
        /// </summary>
        Object,

        /// <summary>
        /// Identifies the 'Part of speech' neuron, used as the link meaning from an object to <see cref="PredefinedNeurons.Verb"/>,
        /// <see cref="PredefinedNeurons.Noun"/>, <see cref="PredefinedNeurons.Adverb"/>, <see cref="PredefinedNeurons.Adjvective"/>,...
        /// </summary>
        POS,

        /// <summary>
        /// Identifies the neuron used as a linkmeaning from an object to an <see cref="IntNeuron"/>, which lets the brain know that
        /// the info was loaded from wordnet + where exactly.
        /// </summary>
        SynSetID,

        /// <summary>
        /// Identifies the neuron used as the linkmeaning for cluster that contains <see cref="TextNeuron"/>s to build a compound
        /// word (a word that actually exists out of combining several words) ex: doppler effect, piano grande,...
        /// </summary>
        /// <remarks>
        /// I don't like the idea of compound words. I don't think it's grammatically correct. In the examples, 'doppler' is a property
        /// of effect, it's a specific effect, the doppler one. But, that's how wordnet stores many words, so this is how we load them
        /// in the brain.  It might change this structure at some point.
        /// </remarks>
        CompoundWord,

        /// <summary>
        /// Identifies the Instruction <see cref="UnionInstruction"/>
        /// </summary>
        UnionInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="IntersectInstruction"/>
        /// </summary>
        IntersectInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="InterleafInstruction"/>
        /// </summary>
        InterleafInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="DirectOutputInstruction"/>
        /// </summary>
        SolveInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="BlockedOutputInstruction"/>
        /// </summary>
        BlockedSolveInstruction,

        /// <summary>
        /// Identifies the NeuronCluster that contains all the relationships known by the <see cref="WordnetSin"/> neuron.
        /// </summary>
        WordNetRelationships,

        /// <summary>
        /// Identifies the Instruction <see cref="GetWeightInstruction"/>
        /// </summary>
        GetWeightInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="GetMaxWeightInstruction"/>
        /// </summary>
        GetMaxWeightInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="DecreaseWeightInstruction"/>
        /// </summary>
        DecreaseWeightInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="IncreaseWeightInstruction"/>
        /// </summary>
        IncreaseWeightInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="ExitNeuronInstruction"/>
        /// </summary>
        ExitNeuronInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="ExitSolveInstruction"/>
        /// </summary>
        ExitSolveInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="ClearChildrenInstruction"/>
        /// </summary>
        ClearChildrenInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="ClearInfoInstruction"/>
        /// </summary>
        ClearInfoInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="ClearLinksInInstruction"/>
        /// </summary>
        ClearLinksInInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="ClearLinksOutInstruction"/>
        /// </summary>
        ClearLinksOutInstruction,

        /// <summary>
        /// Identifies the neuron used by the <see cref="TextSin"/> to indicate that it should start a new 'textblock'.
        /// </summary>
        /// <remarks>
        /// See <see cref="PredefinedNeurons.EndBlock"/> for more info.
        /// </remarks>
        BeginTextBlock,

        /// <summary>
        /// Identifies the neuron used by the <see cref="TextSin"/> to indicate that it should start a new 'textblock'.
        /// </summary>
        /// <remarks>
        /// If the TextSin needs to form sentences or other blocks instead of simply sending word per word or lette per letter, you
        /// can use the 'BeginBlock' and 'EndBlock' neurons to indicate a block.  See more in <see cref="TextSin"/>.
        /// </remarks>
        EndTextBlock,

        /// <summary>
        /// Identifies the neuron used by the <see cref="ReflectionSin"/> as a possible state to indicate that it will try to generate
        /// IL code when output is sent to it.
        /// </summary>
        ReflectionSinRender,

        /// <summary>
        /// Identifies the neuron used by the <see cref="ReflectionSin"/> as a possible state to indicate that it will try to call
        /// a function when output is sent to it.
        /// </summary>
        ReflectionSinCall,

        /// <summary>
        /// Identifies the neuron used by the <see cref="ReflectionSin"/>  as the meaning for a link from an output neuron and
        /// the name of the function/field/property to call/render. When this is used to call a function, it must first have been loaded.  So
        /// that the sin knows about the function.
        /// </summary>
        NameOfMember,

        /// <summary>
        /// Identifies the neuron used by the <see cref="ReflectionSin"/> as the meaning for a link from an output neuron to
        /// a cluster containing the body of the assembly/type/function (the neurons linking to the opcodes and the arguments for the opcodes).
        /// </summary>
        BodyOfMember,

        /// <summary>
        /// Identifies the neuron used by the <see cref="ReflectionSin"/> as the meaning for a link from an output neuron to a textneuron
        /// that contains the fully qualified name of a type.
        /// </summary>
        TypeOfMember,

        /// <summary>
        /// Identifies the neuron used by the <see cref="ReflectionSin"/> as the meaning for a link from an output neuron to a member.
        /// </summary>
        MemberType,

        /// <summary>
        /// Identifies the neuron used as the meaning for a neuron cluster that contains frame elements, which identify the parts that
        /// need to be present in a frame.  A frame defines all the required parts of an information block.
        /// </summary>
        Frame,

        /// <summary>
        /// Identifies the neuron used as the meaning for a cluster (and as the meanig for the link between a frame and this cluster) that
        /// contains 'Object' clusters that directly trigger the frame.
        /// </summary>
        FrameEvokers,

        /// <summary>
        /// Identifies the neuron used as the meaning for a cluster (and as the meaning for the link between a frame and this cluster) that
        /// contains clusters that represent 'known sequences.  Each cluster in this list should have a meaning of 'FrameSequence'.
        /// </summary>
        FrameSequences,

        /// <summary>
        /// Identifies the neuron used as the meaning for a cluster that represents a single 'knonw' sequence for a Frame cluster.
        /// </summary>
        FrameSequence,

        /// <summary>
        /// Identifies the neuron used as the meaning for a link from an 'object' cluster to an int indicating the Id of the frame element
        /// that corresponds to the object.
        /// </summary>
        FrameElementId,

        /// <summary>
        /// Identifies the neuron used as the meaning for a link from an 'object' cluster to either 'Frame_Core', 'Frame_peripheral' or
        /// 'Frame_extra_thematic' to indicate the importance of a frame element within the set of elements for the frame.
        /// </summary>
        FrameImportance,

        /// <summary>
        /// Identifies the neuron used to indicate that a frame element is of core importance, or required.
        /// </summary>
        Frame_Core,

        /// <summary>
        /// Identifies the neuron used to indicate that a frame element is a peripheral item, so not required.
        /// </summary>
        Frame_peripheral,

        /// <summary>
        /// Identifies the neuron used to indicate that a frame element is a thematic item.
        /// </summary>
        Frame_extra_thematic,

        /// <summary>
        /// Identifies the neuron used as the meaning for a link from an 'object' cluster to an int indicating the 'lemma' id of the object
        /// as it is defined in framenet.  a lemma is similar as a synset in wordnet.
        /// </summary>
        LemmaId,

        /// <summary>
        /// Identifies the neuron that represents an article (the/a).
        /// </summary>
        Article,

        /// <summary>
        /// Identifies the neuron that represents a determiner (elke/deze/welke).
        /// </summary>
        Determiner,

        /// <summary>
        /// Identifies the neuron that represents a conjunction (dat/of). -> onderschikkend voegwoord.
        /// </summary>
        Complementizer,

        /// <summary>
        /// Identifies the Instruction <see cref="CiToSInstruction"/>
        /// </summary>
        CiToSInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="CcToSInstruction"/>
        /// </summary>
        CcToSInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="DToIInstruction"/>
        /// </summary>
        DToIInstruction,

        DToSInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="IToDInstruction"/>
        /// </summary>
        IToDInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="IToSInstruction"/>
        /// </summary>
        IToSInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="SToCcInstruction"/>
        /// </summary>
        SToCcInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="SToCiInstruction"/>
        /// </summary>
        SToCiInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="ResetWeightInstruction"/>
        /// </summary>
        ResetWeightInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="AddSplitResultInstruction"/>
        /// </summary>
        AddSplitResultInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="RemoveSplitResultInstruction"/>
        /// </summary>
        RemoveSplitResultInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="ClearSplitResultsInstruction"/>
        /// </summary>
        ClearSplitResultsInstruction,

        /// <summary>
        /// Identifies the Instruction <see cref="GetSplitResultsInstruction"/>
        /// </summary>
        GetSplitResultsInstruction,

        /// <summary>
        /// Identifies the neuron used as the meaning for a neuroncluster used as a flow.
        /// </summary>
        Flow,

        /// <summary>
        /// Identifies the neuron used as the meaning for a NeuronCluster used as a loop or option in a flow.
        /// </summary>
        FlowItemConditional,

        /// <summary>
        /// Identifies the neuron used as the meaning for a NeuronCluster used as a part of a loop or option in a flow.
        /// </summary>
        FlowItemConditionalPart,

        /// <summary>
        /// Identifies the neuron used as the meaning for a link from a FlowItemConditional to True or false, to indicate
        /// that it is looped or not.
        /// </summary>
        FlowItemIsLoop,

        /// <summary>
        /// this indicates the actual end of statically defined items by the system.  This is used by the explorer to find allow for a gap
        /// between static and dynamic items.
        /// </summary>
        EndOfStatic,

        /// <summary>
        /// This indicates the start of the dynamic list count (this is the start value that the brain can use to dynamically assign id to
        /// new neurons.
        /// </summary>
        Dynamic = 1000
    }
}