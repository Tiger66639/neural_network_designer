using JaStDev.HAB.Brain.Lists;
using JaStDev.HAB.Brain.Thread_syncing;
using JaStDev.HAB.Expressions;
using JaStDev.HAB.Storage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace JaStDev.HAB.Brain
{
    /// <summary>
    /// Base class for all objects that can be stored in a <see cref="Brain"/>.
    /// </summary>
    /// <remarks>
    /// It provides a list of all the neurons that this object links to + a list of all
    /// the objects that link to this one.  Both lists are non mutiple.  To add
    /// links, use the <see cref="Link"/> object and set it's <see cref="Link.To"/> and <see cref="Link.From"/>
    /// properties, or call the <see cref="Link.Destroy"/> function to remove the link
    /// <para>
    /// Descendents should re implement <see cref="Neuron.InternalDuplicateValues"/>
    /// </para>
    /// </remarks>

    #region Default neurons

    [NeuronID((ulong)PredefinedNeurons.Actions)]
    [NeuronID((ulong)PredefinedNeurons.Arguments)]
    [NeuronID((ulong)PredefinedNeurons.ArgumentsList)]
    [NeuronID((ulong)PredefinedNeurons.Case)]
    [NeuronID((ulong)PredefinedNeurons.Children)]
    [NeuronID((ulong)PredefinedNeurons.CaseLooped)]
    [NeuronID((ulong)PredefinedNeurons.Code)]
    [NeuronID((ulong)PredefinedNeurons.Condition)]
    [NeuronID((ulong)PredefinedNeurons.ForEach)]
    [NeuronID((ulong)PredefinedNeurons.In)]
    [NeuronID((ulong)PredefinedNeurons.InfoToSearchFor)]
    [NeuronID((ulong)PredefinedNeurons.Instruction)]
    [NeuronID((ulong)PredefinedNeurons.LeftPart)]
    [NeuronID((ulong)PredefinedNeurons.ListToSearch)]
    [NeuronID((ulong)PredefinedNeurons.Looped)]
    [NeuronID((ulong)PredefinedNeurons.LoopItem)]
    [NeuronID((ulong)PredefinedNeurons.CaseItem)]
    [NeuronID((ulong)PredefinedNeurons.LoopStyle)]
    [NeuronID((ulong)PredefinedNeurons.Normal)]
    [NeuronID((ulong)PredefinedNeurons.Operator)]
    [NeuronID((ulong)PredefinedNeurons.Out)]
    [NeuronID((ulong)PredefinedNeurons.RightPart)]
    [NeuronID((ulong)PredefinedNeurons.Rules)]
    [NeuronID((ulong)PredefinedNeurons.SearchFor)]
    [NeuronID((ulong)PredefinedNeurons.Statements)]
    [NeuronID((ulong)PredefinedNeurons.ToSearch)]
    [NeuronID((ulong)PredefinedNeurons.Until)]
    [NeuronID((ulong)PredefinedNeurons.Value)]
    [NeuronID((ulong)PredefinedNeurons.Height)]
    [NeuronID((ulong)PredefinedNeurons.Width)]
    [NeuronID((ulong)PredefinedNeurons.Empty)]
    [NeuronID((ulong)PredefinedNeurons.Neuron)]

    #endregion Default neurons

    public class Neuron : IXmlSerializable
    {
        #region Fields

        protected List<Link> fLinksIn = new List<Link>();                                               //protected so descendents can also access them + lock them if needed.
        protected List<Link> fLinksOut = new List<Link>();
        private ClusterList fClusterdBy;                                                                        //we need this for locking, external callers will also lock this field during looping.
        private WeakReference fLinksInLock;                                                                     //to provide thread safe access to LinksIn.  WeakReference so that we only keep it for as long as needed.
        private WeakReference fLinksOutLock;                                                                    //to provide thread safe access to LinksOut.  WeakReference so that we only keep it for as long as needed.
        private WeakReference fClusteredByLock;                                                                 //to provide thread safe access to ClusteredBy.  WeakReference so that we only keep it for as long as needed.
        private ulong fID;
        private bool fIsChanged = false;
        private uint fAccessCounter;
        private ulong fInfoUsageCount;
        private ulong fMeaningUsageCount;

        /// <summary>
        /// Identifies the Id that indicates it is not a registered neuron.
        /// </summary>
        public const ulong EmptyId = 0;

        public const ulong StartId = (ulong)PredefinedNeurons.PopInstruction;                            //this indicates the start of the neuron id count (which is the first neuron in the PredefinedNeurons List.

        /// <summary>
        /// Identifies the id that indicates it is a temporary neuron which will be registered
        /// the first time it is used in a link.
        /// </summary>
        /// <remarks>
        /// This is used for temporary neurons (like <see cref="IntNeurons"/> returned by <see cref="CountInstruction"/>).
        /// </remarks>
        public const ulong TempId = ulong.MaxValue;

        #endregion Fields

        #region ctor -dtor

        /// <summary>
        /// Default constructor
        /// </summary>
        public Neuron()
        {
            fClusterdBy = new ClusterList(this);
        }

        /// <summary>
        /// When this object gets removed, make certain it is streamed to the longterm memory (if possible) and
        /// remove the weak reference from the shortterm memory of the brain.
        /// </summary>
        ~Neuron()
        {
            //VERY SERIOUS BUG
            /*
             * unloading a neuron can still be done this way, but not saving to disk.  This currently works by accident.  A destructor
             * should normally not access other managed objects (this works cause Brain.Current is a root object).
             */
            if (Environment.HasShutdownStarted == false)                                              //we don't want to do this when the app closes, this is because in that case, the brain manages the close, if needed.
                Brain.Current.UnloadNeuron(this);
        }

        #endregion ctor -dtor

        #region prop

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.Neuron"/>.</value>
        public virtual Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Current[(ulong)PredefinedNeurons.Neuron];
            }
        }

        #endregion TypeOfNeuron

        #region IsChanged

        /// <summary>
        /// Gets/sets if this neuron was changed since it was last saved, or read.
        /// </summary>
        /// <remarks>
        /// When this property is true, the neurons are saved when unloaded.
        /// </remarks>
        [XmlIgnore]
        public bool IsChanged
        {
            get
            {
                return fIsChanged;
            }
            set
            {
                if (fIsChanged != value)
                {
                    fIsChanged = value;
                    if (ID != Neuron.EmptyId)
                    {
                        if (fIsChanged == true)
                            Brain.Current.NotifyChanged(this);
                        else
                            Brain.Current.NotifySaved(this);
                    }
                }
            }
        }

        #endregion IsChanged

        #region LinksIn

        /// <summary>
        /// Gets a thread safe accessor for the list of links that point to this neuron. (this neuron is in the <see cref="Link.To"/> field.
        /// </summary>
        public LinksAccessor LinksIn
        {
            get
            {
                ReaderWriterLockSlim iLock = LinksInLock;
                return new LinksAccessor(fLinksIn, AccessorMode.Read, iLock);
            }
        }

        #endregion LinksIn

        #region LinksInLock

        /// <summary>
        /// Gets the lock for the links In list.
        /// </summary>
        /// <value>The links in lock.</value>
        private ReaderWriterLockSlim LinksInLock
        {
            get
            {
                ReaderWriterLockSlim iLock;
                if (fLinksInLock == null)
                {
                    iLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
                    fLinksInLock = new WeakReference(iLock);
                }
                else if (fLinksInLock.IsAlive == false)
                {
                    iLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
                    fLinksInLock.Target = iLock;
                }
                else
                    iLock = (ReaderWriterLockSlim)fLinksInLock.Target;
                return iLock;
            }
        }

        #endregion LinksInLock

        #region LinksOut

        /// <summary>
        /// Gets a thread safe accessor for the list of links that originate from this neuron (this neuron is in the <see cref="Link.From"/> field.
        /// </summary>
        public LinksAccessor LinksOut
        {
            get
            {
                ReaderWriterLockSlim iLock = LinksOutLock;
                return new LinksAccessor(fLinksOut, AccessorMode.Read, iLock);
            }
        }

        #endregion LinksOut

        #region LinksOutLock

        /// <summary>
        /// Gets the lock for the links In list.
        /// </summary>
        /// <value>The links in lock.</value>
        private ReaderWriterLockSlim LinksOutLock
        {
            get
            {
                ReaderWriterLockSlim iLock;
                if (fLinksOutLock == null)
                {
                    iLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
                    fLinksOutLock = new WeakReference(iLock);
                }
                else if (fLinksOutLock.IsAlive == false)
                {
                    iLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
                    fLinksOutLock.Target = iLock;
                }
                else
                    iLock = (ReaderWriterLockSlim)fLinksOutLock.Target;
                return iLock;
            }
        }

        #endregion LinksOutLock

        #region Rules

        /// <summary>
        /// Gets the list of instructions that determin if this neuron is a valid value for the
        /// current state of the <see cref="Brain"/> to be used by the <see cref="Processor"/>.
        /// </summary>
        /// <remarks>
        /// When the processor needs to get a neuron object for a translation result, it uses this
        /// list of instructions to determin if the neuron is valid.
        /// </remarks>
        public ReadOnlyCollection<Expression> Rules
        {
            get
            {
                NeuronCluster iCluster = RulesCluster;
                if (iCluster != null)
                {
                    using (ChildrenAccessor iList = iCluster.Children)
                    {
                        List<Expression> iExps = iList.ConvertTo<Expression>();
                        if (iExps != null)
                            return new ReadOnlyCollection<Expression>(iExps);
                        else
                            Log.Log.LogError("Neuron.Rules", string.Format("Failed to convert rules list of '{0}' to an executable list.", this));
                    }
                }
                return null;
            }
        }

        #endregion Rules

        #region RulesCluster

        /// <summary>
        /// Gets/sets the cluster containing all the expressions used as rules.
        /// </summary>
        /// <remarks>
        /// <para>
        /// The NeuronCluster is stored in the <see cref="Neuron.LinksOut"/> list as a link with the
        /// meaning 'Rules'. If there is no relationship, with the meaning, null is returned.s
        /// </para>
        /// <para>
        /// see <see cref="Neuron.Rules"/> for more info.
        /// </para>
        /// </remarks>
        public NeuronCluster RulesCluster
        {
            get
            {
                return (NeuronCluster)FindFirstOut((ulong)PredefinedNeurons.Rules);
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Rules, value);
            }
        }

        #endregion RulesCluster

        #region Actions

        /// <summary>
        /// Gets the list of instructions that are executed after the <see cref="Processor"/> has
        /// solved this neuron.
        /// </summary>
        /// <remarks>
        /// <para>
        /// This can be used to perform actions at the end of handling every link. For instance, each link
        /// can try to extract argument values while the actions list
        /// </para>
        /// </remarks>
        public ReadOnlyCollection<Expression> Actions
        {
            get
            {
                NeuronCluster iCluster = ActionsCluster;
                if (iCluster != null)
                {
                    using (ChildrenAccessor iList = iCluster.Children)
                    {
                        List<Expression> iExps = iList.ConvertTo<Expression>();
                        if (iExps != null)
                            return new ReadOnlyCollection<Expression>(iExps);
                        else
                            Log.Log.LogError("Neuron.Actions", string.Format("Failed to convert actions list of '{0}' to an executable list.", this));
                    }
                }
                return null;
            }
        }

        #endregion Actions

        #region ActionsCluster

        /// <summary>
        /// Gets/sets the cluster containing all the expressions used as actions.
        /// </summary>
        /// <remarks>
        /// <para>
        /// The NeuronCluster is stored in the <see cref="Neuron.LinksOut"/> list as a link with the
        /// meaning 'Actions'. If there is no relationship, with the meaning, null is returned.
        /// </para>
        /// <para>
        /// see <see cref="Neuron.Actions"/> for more info.
        /// </para>
        /// </remarks>
        public NeuronCluster ActionsCluster
        {
            get
            {
                return (NeuronCluster)FindFirstOut((ulong)PredefinedNeurons.Actions);
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Actions, value);
            }
        }

        #endregion ActionsCluster

        #region ID

        /// <summary>
        /// Gets the name of the object
        /// </summary>
        public ulong ID
        {
            get { return fID; }
            internal set
            {
                fID = value;
                IsChanged = true;
            }
        }

        #endregion ID

        #region AccessCounter

        /// <summary>
        /// Gets how many times that this Neuron has been read by the <see cref="Brain"/> from file or internal storage.
        /// </summary>
        /// <remarks>
        /// <para>
        /// this is used to optimize the load of the brain so it can unload items correctly
        /// </para>
        /// <para>
        /// This value is persisted, so it is accumulative.  Probably should best split this up eventually in a session
        /// counter and accumulative one so optimizations for both can be made.
        /// </para>
        /// <para>
        /// Can be turned on/off through <see cref="Settins.TrackNeuronAccess"/>
        /// </para>
        /// </remarks>
        public uint AccessCounter
        {
            get { return fAccessCounter; }
            internal set
            {
                if (Settings.TrackNeuronAccess == true)
                {
                    fAccessCounter = value;
                    IsChanged = true;
                }
            }
        }

        #endregion AccessCounter

        #region InfoUsageCount

        /// <summary>
        /// Gets the number of times that this neuron is used in the <see cref="Link.Info"/> list.
        /// </summary>
        /// <remarks>
        /// This is used by the <see cref="Brain.Delete"/> function to determin if this neuron can be
        /// deleted or not (when this neuron is used as info, it can not yet be deleted).
        /// </remarks>
        public ulong InfoUsageCount
        {
            get
            {
                return fInfoUsageCount;
            }
            set
            {
                fInfoUsageCount = value;
                IsChanged = true;
            }
        }

        #endregion InfoUsageCount

        #region MeaningUsageCount

        /// <summary>
        /// Gets the number of times that this neuron is used as the value for <see cref="Link.Meaning"/>.
        /// </summary>
        /// <remarks>
        /// This is used by the <see cref="Brain.Delete"/> function to determin if this neuron can be
        /// deleted or not (when this neuron is used as Meaning, it can not yet be deleted).
        /// </remarks>
        public ulong MeaningUsageCount
        {
            get
            {
                return fMeaningUsageCount;
            }
            set
            {
                fMeaningUsageCount = value;
                IsChanged = true;
            }
        }

        #endregion MeaningUsageCount

        #region ClusteredBy

        /// <summary>
        /// Gets the thread manager that provides access to the list of NeuronCluster id's that this Neuron is a child of.
        /// </summary>
        /// <remarks>
        /// </remarks>
        public NeuronsAccessor ClusteredBy
        {
            get
            {
                ReaderWriterLockSlim iLock = ClusteredByLock;
                return new NeuronsAccessor(fClusterdBy, AccessorMode.Read, ClusteredByLock);
            }
        }

        #endregion ClusteredBy

        #region ClusteredByLock

        /// <summary>
        /// Gets the lock for the ClusteredBy list.
        /// </summary>
        /// <value>The ClusteredBy lock.</value>
        private ReaderWriterLockSlim ClusteredByLock
        {
            get
            {
                ReaderWriterLockSlim iLock;
                if (fClusteredByLock == null)
                {
                    iLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
                    fClusteredByLock = new WeakReference(iLock);
                }
                else if (fClusteredByLock.IsAlive == false)
                {
                    iLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
                    fClusteredByLock.Target = iLock;
                }
                else
                    iLock = (ReaderWriterLockSlim)fClusteredByLock.Target;
                return iLock;
            }
        }

        #endregion ClusteredByLock

        #region CanBeDeleted

        /// <summary>
        /// Gets a value indicating whether this instance can be deleted.
        /// </summary>
        /// <remarks>
        /// An item can't be deleted when:
        ///  - it is used as the value for 1 or more <see cref="Link.Meaning"/> props,
        ///  - it is used as the value for 1 or more <see cref="NeuronCluster.Meaning"/> props,
        ///  - it is used as a value in one or more <see cref="Link.Info"/> lists or,
        ///  - it is a statically created item (id less than <see cref="PredefinedNeurons.Dynamic"/>
        /// </remarks>
        /// <value>
        /// 	<c>true</c> if this instance can be deleted; otherwise, <c>false</c>.
        /// </value>
        public bool CanBeDeleted
        {
            get
            {
                return MeaningUsageCount == 0 && InfoUsageCount == 0 && ID >= (ulong)PredefinedNeurons.Dynamic;
            }
        }

        #endregion CanBeDeleted

        #region Weight

        /// <summary>
        /// Gets/sets the weight that is assigned to this neuron for the currently active processor.
        /// </summary>
        /// <remarks>
        /// <para>
        /// A neuron doesn't store this value itself.  This is stored by the processor in the <see cref="Processor.SplitValues"/>
        /// dict.  When this value is assigned, the neuron will automatically be added to the dict if it wasn't there already.
        /// </para>
        /// <para>
        /// The weight of a neuron is used to determin the order of the result
        /// list of a <see cref="SplitInstruction"/>.  The higher a weight, the
        /// closer to the top the result will be.
        /// </para>
        /// <para>
        /// When a weight is assigned, the neuron will remain in memory (will not be streamed to disk), because
        /// the processor will keep a ref to it.
        /// </para>
        /// </remarks>
        [XmlIgnore]
        public int Weight
        {
            get
            {
                Processor.Processor iCur = Processor.Processor.CurrentProcessor;
                if (iCur != null)
                {
                    int iVal;
                    if (iCur.SplitValues.TryGetValue(fID, out iVal))
                        return iVal;
                }
                return 0;
            }
            set
            {
                Processor.Processor iCur = Processor.Processor.CurrentProcessor;
                if (iCur != null)
                {
                    int iVal;
                    if (iCur.SplitValues.TryGetValue(fID, out iVal) == false)
                        iCur.SplitValues.Add(fID, value);
                    else
                        iCur.SplitValues[fID] = value;                               //don't call IsChanged: when the weight is changed, don't need to store this, is processor local.
                }
            }
        }

        #endregion Weight

        #endregion prop

        #region data management

        /// <summary>
        /// Thread save remove link.
        /// </summary>
        /// <remarks>
        /// Internal cause this operation is done through the link object.
        /// </remarks>
        internal void RemoveInboundLink(Link link)
        {
            IsChanged = true;
            using (LinksAccessor iLinksIn = new LinksAccessor(fLinksIn, AccessorMode.Write, LinksInLock))
                iLinksIn.Remove(link);
        }

        /// <summary>
        /// Adds the link to the inbound list.
        /// </summary>
        /// <remarks>
        /// Internal cause this operation is done through the link object.
        /// </remarks>
        /// <param name="link"></param>
        internal void AddInboundLink(Link link)
        {
            IsChanged = true;
            using (LinksAccessor iLinksIn = new LinksAccessor(fLinksIn, AccessorMode.Write, LinksInLock))
            {
                if (iLinksIn.Items.Contains(link) == true)
                    throw new BrainException("Link already stored as inbound link");
                iLinksIn.Add(link);
            }
        }

        /// <summary>
        /// Adds the link to the inbound list.
        /// </summary>
        /// <remarks>
        /// Internal cause this operation is done through the link object.
        /// </remarks>
        /// <param name="link"></param>
        internal void InsertInboundLink(Link link, int index)
        {
            IsChanged = true;
            using (LinksAccessor iLinksIn = new LinksAccessor(fLinksIn, AccessorMode.Write, LinksInLock))
            {
                if (iLinksIn.Items.Contains(link) == true)
                    throw new BrainException("Link already stored as inbound link");
                iLinksIn.Insert(index, link);
            }
        }

        /// <summary>
        /// Thread save remove link.
        /// </summary>
        /// <remarks>
        /// Internal cause this operation is done through the link object.
        /// </remarks>
        internal void RemoveOutgoingLink(Link link)
        {
            IsChanged = true;
            using (LinksAccessor iLinksOut = new LinksAccessor(fLinksOut, AccessorMode.Write, LinksOutLock))
                iLinksOut.Remove(link);
        }

        /// <summary>
        /// Thread save add link.
        /// </summary>
        /// <remarks>
        /// Internal cause this operation is done through the link object.
        /// </remarks>
        internal void AddOutgoingLink(Link link)
        {
            IsChanged = true;
            using (LinksAccessor iLinksOut = new LinksAccessor(fLinksOut, AccessorMode.Write, LinksOutLock))
            {
                if (iLinksOut.Items.Contains(link) == true)
                    throw new BrainException("Link already stored as outgoing link");
                iLinksOut.Add(link);
            }
        }

        /// <summary>
        /// Thread save insert link.
        /// </summary>
        /// <remarks>
        /// Internal cause this operation is done through the link object.
        /// </remarks>
        internal void InsertOutgoingLink(Link link, int index)
        {
            IsChanged = true;
            using (LinksAccessor iLinksOut = new LinksAccessor(fLinksOut, AccessorMode.Write, LinksOutLock))
            {
                if (iLinksOut.Items.Contains(link) == true)
                    throw new BrainException("Link already stored as outgoing link");
                iLinksOut.Insert(index, link);
            }
        }

        /// <summary>
        /// Searches the first link with the specified meaning in the
        /// <see cref="Neuron.LinksOut"/> list and changes it's <see cref="Link.To"/>
        /// reference to the new value. If this value is null, the link will be removed.
        /// </summary>
        /// <remarks>
        /// <para>
        /// this is thread safe.
        /// </para>
        /// Used by property setters.
        /// </remarks>
        /// <param name="meaning">The meaning to search for.</param>
        /// <param name="value">The value to assign to the 'To' property.</param>
        public void SetFirstOutgoingLinkTo(ulong meaning, Neuron value)
        {
            Link iLink;
            using (LinksAccessor iLinksOut = LinksOut)
                iLink = (from i in iLinksOut.Items where i.MeaningID == meaning select i).FirstOrDefault();
            if (value != null)
            {
                if (iLink != null)
                    iLink.To = value;
                else
                    iLink = new Link(value, this, meaning);
            }
            else if (iLink != null)
                iLink.Destroy();
        }

        internal void AddClusteredBy(NeuronCluster toAdd)
        {
            IsChanged = true;
            using (ClustersAccessor iClusteredBy = new ClustersAccessor(fClusterdBy, AccessorMode.Write, ClusteredByLock))
                iClusteredBy.AddDirect(toAdd);
        }

        internal void RemoveClusteredBy(NeuronCluster toRemove)
        {
            IsChanged = true;
            using (ClustersAccessor iClusteredBy = new ClustersAccessor(fClusterdBy, AccessorMode.Write, ClusteredByLock))
                iClusteredBy.RemoveDirect(toRemove);
        }

        #endregion data management

        #region functions

        /// <summary>
        /// Clears all the data from this instance.
        /// </summary>
        /// <remarks>
        /// This function is automically called when a neuron is deleted.
        /// This includes incomming and outgoing links, clustered by, children (if it is a clusterr), and any possible values.
        /// <para>
        /// Descendents can enhance this function and clean more data.
        /// </para>
        /// </remarks>
        public virtual void Clear()
        {
            RemoveLinks();
            RemoveClusteredBy();
        }

        /// <summary>
        /// Removes all the links that the neuron has.
        /// </summary>
        private void RemoveLinks()
        {
            Link[] iTemp;
            using (LinksAccessor iLinksIn = LinksIn)                                            //need to make it thread safe, if we don't, the list might get modified during the loop.
                iTemp = iLinksIn.Items.ToArray();                                                   //need to make a copy of the list, otherwise we get an error in the loop that we are trying to change the list within the loop due to the delete.
            foreach (Link i in iTemp)                                                           //need to clean out all references to the neuron being deleted.
                i.Destroy();
            using (LinksAccessor iLinksOut = LinksOut)                                            //need to make it thread safe, if we don't, the list might get modified during the loop.
                iTemp = iLinksOut.Items.ToArray();                                                         //need to make a copy of the list, otherwise we get an error in the loop that we are trying to change the list within the loop due to the delete.
            foreach (Link i in iTemp)
                i.Destroy();
        }

        /// <summary>
        /// Removes the neuron from all the clusters that own it.
        /// </summary>
        /// <param name="toDelete">To delete.</param>
        private void RemoveClusteredBy()
        {
            ulong[] iIdTemp;
            using (NeuronsAccessor iClusteredBy = ClusteredBy)
                iIdTemp = iClusteredBy.Items.ToArray();                                      //need to make a copy, otherwise we get an error that we are modifying the ClusteredBy list from whithin the loop.
            foreach (ulong i in iIdTemp)
            {
                using (ChildrenAccessor iArgsList = ((NeuronCluster)Brain.Current[i]).Children)
                    iArgsList.Remove(this);
            }
        }

        /// <summary>
        /// Changes the type of this neuron to the new specified type.  This action creates and destroys the object.
        /// </summary>
        /// <remarks>
        /// Values are copied over when possible.  Links are always copied over.
        /// The new object is registed with the brain.
        /// </remarks>
        /// <param name="type">The requested type.</param>
        /// <returns>The new object that represents the neuron of the new type.</returns>
        public Neuron ChangeTypeTo(Type type)
        {
            Neuron iNew = Activator.CreateInstance(type) as Neuron;
            CopyTo(iNew);
            Brain.Current.Replace(this, iNew);
            return iNew;
        }

        /// <summary>
        /// Creates an exact duplicate of this Neuron so the <see cref="Processor"/> can perform a split.
        /// </summary>
        /// <remarks>
        /// A new id is created for the neuron cause all neurons should have unique numbers.
        /// </remarks>
        /// <returns>An exact duplicate of the argument, but with a new id.</returns>
        public virtual Neuron Duplicate()
        {
            Neuron iRes = Activator.CreateInstance(GetType()) as Neuron;
            Brain.Current.Add(iRes);
            CopyTo(iRes);
            return iRes;
        }

        /// <summary>
        /// Copies all the data from this neuron to the argument.
        /// </summary>
        /// <remarks>
        /// By default, it only copies over all of the links (which includes the 'LinksOut' and 'LinksIn' lists.
        /// <para>
        /// Inheriters should reimplement this function and copy any extra information required for their specific type
        /// of neuron.
        /// </para>
        /// </remarks>
        /// <param name="copyTo">The object to copy their data to.</param>
        protected virtual void CopyTo(Neuron copyTo)
        {
            using (LinksAccessor iLinksIn = LinksIn)
            {
                foreach (Link i in iLinksIn.Items)
                {
                    Link iNew = new Link(copyTo, i.From, i.MeaningID);
                }
            }
            using (LinksAccessor iLinksOut = LinksOut)
            {
                foreach (Link i in iLinksOut.Items)
                {
                    Link iNew = new Link(i.To, copyTo, i.MeaningID);
                }
            }
            //don't need to copy actions and rules, they are copied automatically
            //by copying the links.
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return fID.ToString();
        }

        /// <summary>
        /// Compares this neuron with anohter neuron using the specified operator.
        /// </summary>
        /// <remarks>
        /// <para>
        /// The right part doesn't have to be solved in advance.  So it may be expressions which
        /// will be solved at the appropriate time, when needed.  This means that for some operators,
        /// the right part doesn't have to be solved (logical and/or).
        /// </para>
        /// <para>
        /// In this base implementation, the compare is performed on the id's, descendents
        /// can change this if they want (to compare numbers or string for instance).
        /// </para>
        /// </remarks>
        /// <param name="right">The neuron to compare it with.</param>
        /// <param name="op">The operator to use.</param>
        /// <returns>True if both id's are the same.</returns>
        protected internal virtual bool CompareWith(Neuron right, Neuron op)
        {
            switch (op.ID)
            {
                case (ulong)PredefinedNeurons.Equal: return ID == right.ID;
                case (ulong)PredefinedNeurons.Smaller: return ID < right.ID;
                case (ulong)PredefinedNeurons.SmallerOrEqual: return ID <= right.ID;
                case (ulong)PredefinedNeurons.Bigger: return ID > right.ID;
                case (ulong)PredefinedNeurons.BiggerOrEqual: return ID >= right.ID;
                case (ulong)PredefinedNeurons.Different: return ID != right.ID;
                case (ulong)PredefinedNeurons.And:
                    if (ID == (ulong)PredefinedNeurons.True)
                        return right.ID == (ulong)PredefinedNeurons.True;
                    else if (ID == (ulong)PredefinedNeurons.False)
                        return right.ID == (ulong)PredefinedNeurons.False;
                    else
                        return false;

                case (ulong)PredefinedNeurons.Or:
                    if (ID == (ulong)PredefinedNeurons.True)
                        return (right.ID == (ulong)PredefinedNeurons.True || right.ID == (ulong)PredefinedNeurons.False);
                    else if (ID == (ulong)PredefinedNeurons.False)
                        return right.ID == (ulong)PredefinedNeurons.True;
                    else
                        return false;

                default:
                    Log.Log.LogError("neuron.CompareWith", string.Format("Invalid operator found: {0}.", op));
                    return false;
            }
        }

        /// <summary>
        /// Checks if the specified item is an expression, if so, it solves it, otherwise, it creates a result list
        /// with the specified item.
        /// </summary>
        /// <remarks>
        /// This is static cause toProcess can be null.
        /// </remarks>
        /// <param name="toProcess">the item to check, null alowed.</param>
        /// <returns>null if invalid, otherwise an enumerator containing the result of the expression or the argument</returns>
        internal static IEnumerable<Neuron> SolveResultExp(Neuron toProcess, Processor.Processor processor)
        {
            IEnumerable<Neuron> iRes = null;

            if (toProcess != null)
            {
                ResultExpression iExp = toProcess as ResultExpression;
                if (iExp != null)
                    iRes = iExp.GetValue(processor);
                else
                    iRes = new List<Neuron>() { toProcess };
            }
            return iRes;
        }

        /// <summary>
        /// Checks if the specified item is an expression, if so, it solves it and checks it only has 1 result, otherwise, it
        /// returns the item itself.
        /// </summary>
        /// <remarks>
        /// This is static cause toProcess can be null.
        /// </remarks>
        /// <param name="toProcess">the item to check, null alowed.</param>
        /// <returns>null if invalid, otherwise an enumerator containing the result of the expression or the argument</returns>
        internal static Neuron SolveSingleResultExp(Neuron toProcess, Processor.Processor processor)
        {
            if (toProcess != null)
            {
                ResultExpression iExp = toProcess as ResultExpression;
                if (iExp != null)
                {
                    List<Neuron> iRes = iExp.GetValue(processor).ToList();
                    if (iRes.Count > 1)
                    {
                        Log.Log.LogError("Neuron.SolveSinlgeResult", string.Format("Expression '{0}' has multiple results, only 1 allowed.", iExp));
                        return null;
                    }
                    else
                        return iRes[0];
                }
            }
            return toProcess;
        }

        /// <summary>
        /// Checks if the specified id points to an empty or temp neuron or not.
        /// </summary>
        /// <param name="id">ulong to check.</param>
        /// <returns>true if id is <see cref="Neuron.EmptyId"/> or <see cref="Neuron.TempId"/>.</returns>
        public static bool IsEmpty(ulong id)
        {
            return id == Neuron.EmptyId || id == Neuron.TempId;
        }

        #endregion functions

        #region searching

        /// <summary>
        /// Searches and returns the first neuron linked through the <see cref="Neuron.LinksOut"/> list
        /// where the link has the specified meaning.
        /// </summary>
        /// <param name="meaning">The id of the meaning to look for</param>
        /// <returns>The neuron that was found.</returns>
        public Neuron FindFirstOut(ulong meaning)
        {
            using (LinksAccessor iLinksOut = LinksOut)
                return (from i in iLinksOut.Items
                        where i.MeaningID == meaning
                        select i.To).FirstOrDefault();
        }

        /// <summary>
        /// Searches and returns the first cluster found in the <see cref="Neuron.ClusteredBy"/> list
        /// where the meaning of the cluster is the specified value.  If no cluster can be found, null is returned.
        /// </summary>
        /// <param name="meaning">The id of the meaning to look for</param>
        /// <returns></returns>
        public NeuronCluster FindFirstClusteredBy(ulong meaning)
        {
            using (NeuronsAccessor iClusteredBy = ClusteredBy)
            {
                return (from i in iClusteredBy.Items
                        let cluster = ((NeuronCluster)Brain.Current[i])
                        where cluster.Meaning == meaning
                        select cluster).FirstOrDefault();
            }
        }

        #endregion searching

        #region IXmlSerializable Members

        public XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        /// Reads the class from xml file.
        /// </summary>
        /// <param name="reader"></param>
        public virtual void ReadXml(XmlReader reader)
        {
            bool wasEmpty = reader.IsEmptyElement;

            reader.Read();
            if (wasEmpty) return;

            fID = XmlStore.ReadElement<ulong>(reader, "ID");
            fInfoUsageCount = XmlStore.ReadElement<ulong>(reader, "InfoUsageCount");
            fMeaningUsageCount = XmlStore.ReadElement<ulong>(reader, "MeaningUsageCount");
            fAccessCounter = XmlStore.ReadElement<uint>(reader, "AccessCounter");
            int iWeight = 0;
            XmlStore.TryReadElement<int>(reader, "Weight", ref iWeight);                                          //weight has been removed from the neuron, is now stored by the processor so we can make it processor local.

            XmlStore.ReadList(reader, "LinksOut", new Action<XmlReader, bool>(ReadLink), true);
            XmlStore.ReadList(reader, "LinksIn", new Action<XmlReader, bool>(ReadLink), false);
            XmlStore.ReadIDList(reader, "Clusters", fClusterdBy.List);                                            //while reading, the neuron can't be changed by different threads so don't need thread safety.

            //fInternalClusters
        }

        private void ReadLink(XmlReader reader, bool asOut)
        {
            reader.ReadStartElement("Link");

            ulong iID = XmlStore.ReadElement<ulong>(reader, "ID");
            ulong iMeaningId = XmlStore.ReadElement<ulong>(reader, "Meaning");

            //create link
            Link iLink = null;
            if (asOut == true)
                iLink = BuildLinkOut(iID, iMeaningId);
            else
                iLink = BuildLinkIn(iID, iMeaningId);
            if (iLink != null)
                using (LinkInfoAccessor iList = iLink.InfoW)
                    XmlStore.ReadIDList(reader, "ExtraInfo", iList);                                                             //extra info of link.
            else
            {
                //the link is invalid somehow, but we still need to read the info data,
                //so create a temp list and do it like so.
                List<ulong> iTemp = new List<ulong>();
                XmlStore.ReadIDList(reader, "ExtraInfo", iTemp);
            }
            reader.ReadEndElement();
        }

        /// <summary>
        /// Builds the link using the specified values, checking if there is no duplicate already existing.
        /// </summary>
        /// <remarks>
        /// When there is a duplicate link, the new one is not created, we log a warning and indicate that the neuron is
        /// changed.
        /// </remarks>
        /// <param name="toID">To ID.</param>
        /// <param name="meaningID">The meaning ID.</param>
        private Link BuildLinkOut(ulong toID, ulong meaningID)
        {
            Neuron iFound;
            Link iLink = null;
            using (LinksAccessor iLinksOut = LinksOut)
            {
                var iExisting = (from i in iLinksOut.Items                                                               //need to make certain that there are no duplicate links declared in the xaml.  The system can't handle those, so filter them out.
                                 where i.ToID == toID && i.MeaningID == meaningID
                                 select i).FirstOrDefault();
                if (iExisting == null)
                {
                    if (Brain.Current.TryFindInCash(toID, out iFound) == true)
                    {
                        using (LinksAccessor iLinksIn = iFound.LinksIn)
                            iLink = (from i in iLinksIn.Items where i.FromID == fID && i.MeaningID == meaningID select i).FirstOrDefault();
                    }
                    if (iLink == null)
                        iLink = new Link(toID, fID, meaningID);
                    iLinksOut.Mode = AccessorMode.Write;                                                                  //need to change the mode so we can add an item.
                    iLinksOut.Add(iLink);
                }
                else
                {
                    Log.Log.LogWarning("Neuron.BuildLinkOut", string.Format("Duplicate link found from: {0}, to: {1}, Meaning: {2}, in: {0}", ID, toID, meaningID));
                    IsChanged = true;
                }
            }
            return iLink;
        }

        /// <summary>
        /// Builds the link using the specified values, checking if there is no duplicate already existing.
        /// </summary>
        /// <remarks>
        /// When there is a duplicate link, this is removed, we log a warning and indicate that the neuron is
        /// changed.  This way, we will automatically fix the neuron.
        /// </remarks>
        /// <param name="fromID">From ID.</param>
        /// <param name="meaningID">The meaning ID.</param>
        private Link BuildLinkIn(ulong fromID, ulong meaningID)
        {
            Neuron iFound;
            Link iLink = null;
            using (LinksAccessor iLinksIn = LinksIn)
            {
                var iExisting = (from i in iLinksIn.Items                                                    //need to make certain that there are no duplicate links declared in the xaml.  The system can't handle those, so filter them out.
                                 where i.FromID == fromID && i.MeaningID == meaningID
                                 select i).FirstOrDefault();
                if (iExisting == null)
                {
                    if (Brain.Current.TryFindInCash(fromID, out iFound) == true)
                    {
                        using (LinksAccessor iLinksOut = iFound.LinksOut)
                            iLink = (from i in iLinksOut.Items where i.ToID == fID && i.MeaningID == meaningID select i).FirstOrDefault();
                    }
                    if (iLink == null)
                        iLink = new Link(fID, fromID, meaningID);                                              //this constructor doesn't add items to the lists
                    iLinksIn.Mode = AccessorMode.Write;
                    iLinksIn.Add(iLink);
                }
                else
                {
                    Log.Log.LogWarning("Neuron.ReadLink", string.Format("Duplicate link found from: {0}, to: {1}, Meaning: {2}, in: {1}", fromID, ID, meaningID));
                    IsChanged = true;
                }
            }
            return iLink;
        }

        /// <summary>
        /// Writes the class to xml files
        /// </summary>
        /// <param name="writer">The xml writer to use</param>
        public virtual void WriteXml(XmlWriter writer)
        {
            using (LinksAccessor iLinksIn = LinksIn)                                            //we lock from the start so that we can't change from the beginning.
            {
                using (LinksAccessor iLinksOut = LinksOut)
                {
                    XmlStore.WriteElement<ulong>(writer, "ID", fID);
                    XmlStore.WriteElement<ulong>(writer, "InfoUsageCount", InfoUsageCount);
                    XmlStore.WriteElement<ulong>(writer, "MeaningUsageCount", MeaningUsageCount);
                    XmlStore.WriteElement<uint>(writer, "AccessCounter", AccessCounter);

                    writer.WriteStartElement("LinksOut");
                    foreach (Link i in iLinksOut.Items)
                        WriteLink(writer, i, true);
                    writer.WriteEndElement();

                    writer.WriteStartElement("LinksIn");
                    foreach (Link i in iLinksIn.Items)
                        WriteLink(writer, i, false);
                    writer.WriteEndElement();

                    XmlStore.WriteIDList(writer, "Clusters", fClusterdBy);
                }
            }
        }

        /// <summary>
        /// Writes the contents of a link to an xml stream.
        /// </summary>
        /// <param name="i"></param>
        /// <param name="asOut"></param>
        private void WriteLink(XmlWriter writer, Link link, bool asOut)
        {
            writer.WriteStartElement("Link");

            writer.WriteStartElement("ID");
            if (asOut == true)
                writer.WriteString(link.ToID.ToString());
            else
                writer.WriteString(link.FromID.ToString());
            writer.WriteEndElement();

            XmlStore.WriteElement<ulong>(writer, "Meaning", link.MeaningID);
            using (LinkInfoAccessor iList = link.Info)
                XmlStore.WriteIDList(writer, "ExtraInfo", iList);
            writer.WriteEndElement();
        }

        #endregion IXmlSerializable Members
    }
}