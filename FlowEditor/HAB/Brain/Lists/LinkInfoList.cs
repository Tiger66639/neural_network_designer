﻿namespace JaStDev.HAB.Brain.Lists
{
    /// <summary>
    /// A specific implementation of <see cref="NeuronList"/> that will increment/decrement the
    /// <see cref="Neuron.InfoUsageCounter"/> of the items being added/removed.
    /// </summary>
    public class LinkInfoList : BrainList
    {
        public LinkInfoList() : base()
        {
        }

        protected override void InternalInsert(int index, Neuron item)
        {
            base.InternalInsert(index, item);
            item.InfoUsageCount++;
        }

        protected override bool InternalRemove(Neuron item, int index)
        {
            if (base.InternalRemove(item, index) == true)
            {
                item.InfoUsageCount--;
                return true;
            }
            else
                return false;
        }
    }
}