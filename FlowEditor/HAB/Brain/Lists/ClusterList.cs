﻿using JaStDev.HAB.Brain.Thread_syncing;

namespace JaStDev.HAB.Brain.Lists
{
    /// <summary>
    ///     A List that contains all the ID's of the <see cref="NeuronCluster" />s that own a <see cref="Neuron" />.
    /// </summary>
    /// <remarks>
    ///     Provides extra functionality that makes certain that the <see cref="NeuronCluster.Children" /> lists are also
    ///     kept in sync.
    /// </remarks>
    public class ClusterList : OwnedBrainList
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ClusterList" /> class.
        /// </summary>
        /// <param name="owner">The owner.</param>
        internal ClusterList(Neuron owner) : base(owner)
        {
        }

        #region overrides

        protected internal override NeuronsAccessor GetAccessor()
        {
            return new ClustersAccessor(this, AccessorMode.Read, Lock);
        }

        /// <summary>
        ///     Performs the actual insertion.
        /// </summary>
        /// <remarks>
        ///     Makes certain that everything is regisetered + raises the appropriate events.
        /// </remarks>
        /// <param name="index">The index.</param>
        /// <param name="item">The item.</param>
        protected override void InternalInsert(int index, Neuron item)
        {
            var iItem = (NeuronCluster)item; //this will raise an exception if not of correct type.
            base.InternalInsert(index, item);
            using (var iList = iItem.Children)
                iList.InsertDirect(index, Owner);
        }

        /// <summary>
        ///     Removes the item at the specified index.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        protected override bool InternalRemove(Neuron item, int index)
        {
            var iItem = item as NeuronCluster;
            //dont' raise exception, while removing, we need maximum flexibility here.
            var iRes = base.InternalRemove(item, index);
            if (iItem != null)
                using (var iList = iItem.Children)
                    iList.RemoveDirect(item);
            return iRes;
        }

        /// <summary>
        ///     performs an insert and raises the event but doesn't change anything in the neuron being added.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="index">The index.</param>
        internal void InsertDirect(int index, Neuron item)
        {
            base.InternalInsert(index, item);
        }

        /// <summary>
        ///     performs a remove and raises the event but doesn't change anything in the neuron being added.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="index">The index.</param>
        internal void RemoveDirect(NeuronCluster toRemove)
        {
            var iIndex = IndexOf(toRemove.ID);
            if (iIndex > -1)
                base.InternalRemove(toRemove, iIndex);
        }

        #endregion overrides
    }
}