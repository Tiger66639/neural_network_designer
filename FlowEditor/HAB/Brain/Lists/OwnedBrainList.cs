﻿using System;

namespace JaStDev.HAB.Brain.Lists
{
    /// <summary>
    /// Base class for all lists used by the <see cref="Brain"/> that are owned by a neuron.
    /// </summary>
    /// <remarks>
    /// Makes certain that when an item is added to this list, the owner is registered with the brain (if it has a temp id).
    /// </remarks>
    public class OwnedBrainList : BrainList
    {
        private Neuron fOwner;

        /// <summary>
        /// Initializes a new instance of the <see cref="BrainList"/> class.
        /// </summary>
        /// <param name="owner">The owner.</param>
        internal OwnedBrainList(Neuron owner) : base()
        {
            if (owner == null)
                throw new ArgumentNullException();
            fOwner = owner;
        }

        #region Owner

        /// <summary>
        /// Gets the <see cref="Neuron"/> that owns this list.
        /// </summary>
        /// <remarks>
        /// This is used for internal checking to allow modifs or to buffer them.
        /// </remarks>
        public Neuron Owner
        {
            get { return fOwner; }
            //we allow an internal set, cause links use the 'from' neuron as the owner, since any changes reflect on the owner, so it must be editable. But offcourse, a link's from can change, so the owner must also be changeable.
            internal set { fOwner = value; }
        }

        #endregion Owner

        /// <summary>
        /// Performs the actual insertion.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="item">The item.</param>
        /// <remarks>
        /// Makes certain that everything is regisetered + raises the appropriate events.
        /// </remarks>
        /// <exception cref="BrainException">When there is no owner defined.</exception>
        protected override void InternalInsert(int index, Neuron item)
        {
            if (Owner != null)
            {
                if (Owner.ID == Neuron.TempId)                                                               //Can't add a child to a temp cluster, both need to be registered.
                    Brain.Current.Add(Owner);
                base.InternalInsert(index, item);
                Owner.IsChanged = true;
            }
            else
                throw new BrainException("Owner not defined.");
        }

        /// <summary>
        /// Removes the item at the specified index.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        protected override bool InternalRemove(Neuron item, int index)
        {
            bool iRes = false;
            if (Owner != null)
            {
                iRes = base.InternalRemove(item, index);
                if (iRes == true)
                    Owner.IsChanged = true;
            }
            return iRes;
        }
    }
}