﻿using JaStDev.HAB.Events;
using JaStDev.HAB.Sins;
using JaStDev.HAB.Storage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace JaStDev.HAB.Brain
{
    /// <summary>
    ///     Represents the entry point and central data store for a HAB system.
    /// </summary>
    /// <remarks>
    ///     <para>
    ///         This object provides easy access to brain objects: it provides an indexed property get (
    ///         <see cref="Brain.this" />)
    ///         for retrieving ( - converting an ulong to) a neuron object.
    ///     </para>
    ///     <para>
    ///         There should only be 1 Brain object per application.  You can't create objects of this type, instead
    ///         use the <see cref="Brain.Current" /> static property to access the default object.
    ///         This is because this object manages a large set of data objects, these objects must have easy access
    ///         to the owning object without loosing to much memory though back references.
    ///     </para>
    ///     <para>
    ///         All operations on this object are thread save, so the brain can be used from different threads (important so
    ///         that different Sins can work independently).
    ///     </para>
    ///     <para>
    ///         The objects stored in the short term memory of the brain, are stored with a <see cref="System.WeakReference" />
    ///         so that they can be garbage collected (and streamed to disk) when no longer used anywhere and mem room is
    ///         required.
    ///         When items are loaded and added to the iternal dictionary, they are kept alive delibertly so that
    ///         they remain in memory even if they are not used at the moment.  This is done for all newly registered items. At
    ///         regular
    ///         time intervals, a clean should be performed by releasing the objects
    ///     </para>
    /// </remarks>
    [XmlRoot("Brain", Namespace = "http://tiger66639.clan.rip/", IsNullable = false)]
    public class Brain : IXmlSerializable
    {
        #region ctor

        /// <summary>
        ///     Privates constructor so that no objects but the current can be used (singleton pattern).
        /// </summary>
        private Brain()
        {
        }

        #endregion ctor

        /// <summary>
        ///     Determines whether the specified ID is was used but has been deleted.
        /// </summary>
        /// <remarks>
        ///     Specifically checks the internal 'delete' and 'free' lists instead of the lists containing
        ///     the existing items (done by <see cref="Brain.IsExisting" />).
        /// </remarks>
        /// <param name="id">The id to check.</param>
        /// <returns>
        ///     <c>true</c> if the id is free (no neuron is using it); otherwise, <c>false</c>.
        /// </returns>
        public bool IsDeletedID(ulong id)
        {
            fFreeIdsLock.EnterReadLock();
            fNeuronsToDeleteLock.EnterReadLock();
            try
            {
                return fFreeIds.Contains(id) || fNeuronsToDelete.Contains(id);
            }
            finally
            {
                fFreeIdsLock.ExitReadLock();
                fNeuronsToDeleteLock.ExitReadLock();
            }
        }

        /// <summary>
        ///     Determines whether the specified ID exists or not.
        /// </summary>
        /// <remarks>
        ///     Specifically checks the lists containing all the existing items instead of the lists containing
        ///     the deleted items (done by <see cref="Brain.IsDeletedID" />).
        /// </remarks>
        /// <param name="iId">The id to check.</param>
        /// <returns>
        ///     <c>true</c> if the id is assigned to a neuron; Deleted, reserved or out of range values return <c>false</c>.
        /// </returns>
        public bool IsExistingID(ulong id)
        {
            var iRes = IsValidID(id);
            if (iRes == false)
                return false;
            fCashLock.EnterReadLock();
            try
            {
                return fNeuronCash.ContainsKey(id) || Storage.IsExistingID(id);
            }
            finally
            {
                fCashLock.ExitReadLock();
            }
        }

        /// <summary>
        ///     Determines whether the specified id is valid: within range of the existing items and not deleted: so a neuron
        ///     should
        ///     exist for the id.
        /// </summary>
        /// <param name="id">The id to verify.</param>
        /// <returns>
        ///     <c>true</c> if the specified id is valid; otherwise, <c>false</c>.
        /// </returns>
        public bool IsValidID(ulong id)
        {
            fFreeIdsLock.EnterReadLock();
            try
            {
                if (id > NextID)
                    return false;
                else if (id >= (ulong)PredefinedNeurons.EndOfStatic && id < (ulong)PredefinedNeurons.Dynamic)
                    return false;
                else if (fFreeIds.Contains(id))
                    return false;
                return true;
            }
            finally
            {
                fFreeIdsLock.ExitReadLock();
            }
        }

        /// <summary>
        ///     Loads all the neurons into the cash by indicating they have changed.
        /// </summary>
        public void TouchMem()
        {
            for (var i = Neuron.StartId; i < fNextID; i++)
            {
                Neuron iFound;
                if (TryFindNeuron(i, out iFound))
                    iFound.IsChanged = true;
            }
        }

        /// <summary>
        ///     Called when a neuron gets garbage collected.  Makes certain it is streamed
        ///     and removed from the short term memory.
        /// </summary>
        /// <param name="neuron"></param>
        internal void UnloadNeuron(Neuron neuron)
        {
            try
            {
                if (neuron.ID != Neuron.EmptyId && neuron.ID != Neuron.TempId && neuron.IsChanged)
                    //only try to save if it isn't a temp neuron.
                    Storage.SaveNeuron(neuron);
                RemoveFromCash(neuron.ID);
            }
            catch (Exception e)
            {
                Log.Log.LogError("Brain.UnloadNeuron", e.ToString());
            }
        }

        #region fields

        private static Brain fCurrent;

        private readonly Dictionary<ulong, object> fNeuronCash = new Dictionary<ulong, object>();
        //stores all neurons and weakReferences that are currently loaded in memory. This object is used as a lock for thread safe operations.

        private readonly ReaderWriterLockSlim fCashLock = new ReaderWriterLockSlim();
        //lock used to provide secure access to the neuroncash.

        private ulong fNextID = (ulong)PredefinedNeurons.Dynamic;
        private readonly List<Sin> fSins = new List<Sin>();
        private readonly Queue<ulong> fFreeIds = new Queue<ulong>();
        private readonly ReaderWriterLockSlim fFreeIdsLock = new ReaderWriterLockSlim();

        private readonly List<ulong> fNeuronsToDelete = new List<ulong>();
        //when Settings.AutoSaveNeurons == false, we can't remove neurons from the db backend untill a save instruction is issued.

        private readonly ReaderWriterLockSlim fNeuronsToDeleteLock = new ReaderWriterLockSlim();
        private bool fIsChanged;

        #endregion fields

        #region Events

        /// <summary>
        ///     Raised when a link between 2 neurons is changed.
        /// </summary>
        public event LinkChangedEventHandler LinkChanged;

        /// <summary>
        ///     Raised when something in the list of neurons has changed (add, remove or change).
        /// </summary>
        public event NeuronChangedEventHandler NeuronChanged;

        /// <summary>
        ///     Raised when a <see cref="NeuronList" /> was changed.
        /// </summary>
        public event NeuronListChangedEventHandler NeuronListChanged;

        /// <summary>
        ///     Occurs when the brain is cleared completely (all neurons are removed).
        /// </summary>
        public event EventHandler Cleared;

        /// <summary>
        ///     Occurs when a new data set has been loaded and the brain is ready for operation.
        /// </summary>
        public event EventHandler Loaded;

        /// <summary>
        ///     Occurs when the state of the database is changed from saved to changed or visa versa.
        /// </summary>
        /// <remarks>
        ///     Designers can use this to monitor any changes.
        /// </remarks>
        public event EventHandler Changed;

        #endregion Events

        #region prop

        #region this

        /// <summary>
        ///     gets the neuron object with the specified id (if available).
        /// </summary>
        /// <remarks>
        ///     This method is thread safe, it is locked at the beginning of the call and only unlocked at the end.
        ///     It is also sub processor safe.  That is, if the current thread's processor is a sub processor, it will first
        ///     try to get a local clone of the neuron.
        /// </remarks>
        /// <exception cref="IndexOutOfRangeException">There is no valid Neuron object using the specified id</exception>
        /// <param name="id">The identifier of the neuron.</param>
        /// <returns>The Neuron object with the specified id.</returns>
        [XmlIgnore]
        public Neuron this[ulong id]
        {
            get
            {
                Neuron iRes = null;
                try
                {
                    TryFindNeuron(id, out iRes);
                }
                catch (Exception e)
                {
                    throw new IndexOutOfRangeException(
                        string.Format("No neuron found with the specified id: {0}.", id), e);
                }
                if (iRes != null)
                {
                    if (iRes.AccessCounter + 1 > iRes.AccessCounter) //make certain we don't cause overflow with add.
                        iRes.AccessCounter++; //indicate it has been accessed so we can later optimize for it.
                    return iRes;
                }
                throw new IndexOutOfRangeException(string.Format("No neuron found with the specified id: {0}.", id));
            }
        }

        #endregion this

        #region Current

        /// <summary>
        ///     Gets a reference to the currently active Brain.
        /// </summary>
        public static Brain Current
        {
            get
            {
                if (fCurrent == null)
                    fCurrent = new Brain();
                return fCurrent;
            }
        }

        #endregion Current

        #region HasLinkChangedEvents

        /// <summary>
        ///     Gets if there are delegates registered for the <see cref="Brain.LinkChanged" /> event.
        ///     This can be used as a small optimization so you don't always have to create a
        ///     <see cref="LinkChangedEventArgs" /> object each time you have to call <see cref="Brain.OnLinkChanged" />.
        /// </summary>
        public bool HasLinkChangedEvents
        {
            get { return LinkChanged != null; }
        }

        #endregion HasLinkChangedEvents

        #region HasNeuronChangedEvents

        /// <summary>
        ///     Gets if there are delegates registered for the <see cref="Brain.NeuronChanged" /> event.
        ///     This can be used as a small optimization so you don't always have to create a
        ///     <see cref="NeuronChangedEventArgs" /> object each time you have to call <see cref="Brain.OnNeuronChanged" />.
        /// </summary>
        public bool HasNeuronChangedEvents
        {
            get { return NeuronChanged != null; }
        }

        #endregion HasNeuronChangedEvents

        #region HasNeuronChangedEvents

        /// <summary>
        ///     Gets if there are delegates registered for the <see cref="Brain.NeuronChanged" /> event.
        ///     This can be used as a small optimization so you don't always have to create a
        ///     <see cref="NeuronChangedEventArgs" /> object each time you have to call <see cref="Brain.OnNeuronChanged" />.
        /// </summary>
        public bool HasNeuronListChangedEvents
        {
            get { return NeuronListChanged != null; }
        }

        #endregion HasNeuronChangedEvents

        #region LastID

        /// <summary>
        ///     Gets the last used ID.
        /// </summary>
        /// <value>The last ID.</value>
        public ulong NextID
        {
            get { return fNextID; }
            private set
            {
                if (value < fNextID) //check for buffer overflow.
                    throw new BrainException(
                        "The Brain has reached it's full capacity, it is unable to store any more data.");
                fNextID = value;
            }
        }

        /// <summary>
        ///     internal helper function for savely decreasing the last id count (which checks for buffer overruns).
        /// </summary>
        private void DecLastID()
        {
            if (fNextID > (ulong)PredefinedNeurons.Dynamic)
                fNextID--;
        }

        #endregion LastID

        #region NeuronCount

        /// <summary>
        ///     Gets the total number of neurons that the brain currently has.
        /// </summary>
        /// <remarks>
        ///     This is primarely for display purposes.
        /// </remarks>
        [XmlIgnore]
        public ulong NeuronCount
        {
            get
            {
                fFreeIdsLock.EnterReadLock();
                try
                {
                    return fNextID - (ulong)fFreeIds.Count;
                }
                finally
                {
                    fFreeIdsLock.ExitReadLock();
                }
            }
        }

        #endregion NeuronCount

        #region Storage

        /// <summary>
        ///     Gets the long term storage container for Neuron objects.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         This is currently always a <see cref="LongtermMem" /> type.
        ///     </para>
        ///     <para>
        ///         The reasonf for using a storage is so that this could be replaced later on with other types of storage.
        ///     </para>
        /// </remarks>
        public ILongtermMem Storage { get; private set; } = new LongtermMem();

        #endregion Storage

        #region Sins

        /// <summary>
        ///     Gets the list of <see cref="Sin" />s that are used by the brain
        ///     for input/output (interaction).
        /// </summary>
        [XmlIgnore]
        public ReadOnlyCollection<Sin> Sins
        {
            get { return new ReadOnlyCollection<Sin>(fSins); }
        }

        #endregion Sins

        #region IsChanged

        /// <summary>
        ///     Gets wether the network has been changed since the last save.
        /// </summary>
        /// <remarks>
        ///     Also triggers the <see cref="Brain.Changed" /> event when this value changes.
        /// </remarks>
        public bool IsChanged
        {
            get { return fIsChanged; }
            internal set
            {
                if (fIsChanged != value)
                {
                    fIsChanged = value;
                    if (Changed != null)
                        Changed(this, EventArgs.Empty);
                }
            }
        }

        #endregion IsChanged

        #endregion prop

        #region functions

        /// <summary>
        ///     Tries to find the neuron with the specified id, if it can't find it, false is returned.
        /// </summary>
        /// <param name="id">The id of the neuron to find.</param>
        /// <param name="ffound">The neuron that was found or null if it wasn't found.</param>
        /// <returns>True if we found the item, otherwise false.</returns>
        public bool TryFindNeuron(ulong id, out Neuron found)
        {
            found = null;
            object iRef = null;

            fCashLock.EnterUpgradeableReadLock();
            try
            {
                if (IsValidID(id))
                {
                    if (Processor.Processor.CurrentProcessor != null &&
                        Processor.Processor.CurrentProcessor.TryFindNeuron(id, out found))
                        //if we are in a sub processor, check if there is a local clone for the neuron, if so return this.
                        return true;
                    if (fNeuronCash.TryGetValue(id, out iRef) == false ||
                        (iRef is WeakReference && ((WeakReference)iRef).IsAlive == false))
                    {
                        fCashLock.EnterWriteLock();
                        try
                        {
                            fNeuronCash.Remove(id);
                            //if there was still a key loaded, but it was a weak reference that is no longer alife, we need to remove the item before adding it again.
                        }
                        finally
                        {
                            fCashLock.ExitWriteLock();
                        }
                        if (Storage != null)
                        {
                            found = Storage.GetNeuron(id);
                            if (found != null)
                                AddToCash(found);
                        }
                    }
                    else if (iRef is Neuron)
                        found = (Neuron)iRef;
                    else if (iRef is WeakReference)
                        found = (Neuron)((WeakReference)iRef).Target;
                }
            }
            finally
            {
                fCashLock.ExitUpgradeableReadLock();
            }
            return found != null;
        }

        /// <summary>
        ///     Checks if the specified id is loaded in the cash, if so, it retuns the neuron that was found.
        /// </summary>
        /// <param name="id">The id to search for.</param>
        /// <param name="result">The neuron that was found or null.</param>
        /// <returns>True if there was a neuron in the cash with the specified id.</returns>
        public bool TryFindInCash(ulong id, out Neuron result)
        {
            fCashLock.EnterReadLock();
            try
            {
                result = null;
                object iRef = null;
                if (fNeuronCash.TryGetValue(id, out iRef) == false ||
                    (iRef is WeakReference && ((WeakReference)iRef).IsAlive == false))
                    return false;
                else if (iRef is Neuron)
                    result = (Neuron)iRef;
                else if (iRef is WeakReference)
                    result = (Neuron)((WeakReference)iRef).Target;
                return result != null;
            }
            finally
            {
                fCashLock.ExitReadLock();
            }
        }

        /// <summary>
        ///     Creates a new Current object and loads the values for the properties from the specified stream.
        /// </summary>
        /// <param name="source">The stream containing the source to load.</param>
        /// <param name="sins">The list of assemblies containing all the extra sins (required for loading external sins).</param>
        public static void Load(string path)
        {
            TextSin.ResetDict(); //whenever a new brain is loaded, we need to reset the dictionary.
            using (var iStr = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                var iSettings = new XmlReaderSettings();
                iSettings.IgnoreComments = true;
                iSettings.IgnoreWhitespace = true;
                using (var iReader = XmlReader.Create(iStr, iSettings))
                {
                    iReader.Read();
                    iReader.Read();
                    Current.ReadXml(iReader);
                }
                Current.CheckStoragePath(path);
                foreach (var i in fCurrent.Storage.GetEntryNeuronsFor(typeof(Brain)))
                {
                    if (i is Sin)
                        fCurrent.fSins.Add((Sin)i);
                    else
                        throw new InvalidOperationException(
                            string.Format("Sins expected as the entry points for the brain, found: {0}!", i.GetType()));
                }
                Current.IsChanged = false; //when just loaded, can't be changed.
                if (Current.Loaded != null)
                    Current.Loaded(Current, EventArgs.Empty);
            }
        }

        /// <summary>
        ///     Checks if the specified storage path is valid and will also make it absolute, if needed.
        /// </summary>
        /// <param name="path">The path.</param>
        private void CheckStoragePath(string path)
        {
            if (Path.IsPathRooted(Storage.DataPath) == false)
            {
                var iPath = new Uri(Storage.DataPath, UriKind.Relative);
                var iTemp = new Uri(new Uri(path), iPath);
                Storage.DataPath = iTemp.LocalPath;
            }
        }

        /// <summary>
        ///     Clears all the data from the db and closes all possibly running processors.
        /// </summary>
        /// <remarks>
        ///     Also raises the <see cref="Brain.Cleared" /> event.
        /// </remarks>
        public void Clear()
        {
            fFreeIdsLock.EnterWriteLock();
            fCashLock.EnterWriteLock();
            try
            {
                foreach (var i in fNeuronCash)
                //when we clear the data, we need to let each neuron know it is no longer associated with the brain, otherwise the neuron will try to save itself.
                {
                    if (i.Value is WeakReference)
                    {
                        var iVal = (WeakReference)i.Value;
                        if (iVal.IsAlive)
                            ((Neuron)iVal.Target).ID = Neuron.EmptyId;
                    }
                    else if (i.Value is Neuron)
                        ((Neuron)i.Value).ID = Neuron.EmptyId;
                    else
                        throw new InvalidDataException("Unknown type found in neuroncash, can't clear.");
                }
                fNeuronCash.Clear();
                fNextID = (ulong)PredefinedNeurons.Dynamic;
                fSins.Clear();
                Storage = new LongtermMem(); //we simply create a new mem manager, this resets, definitely.
                fFreeIds.Clear();
                if (Cleared != null)
                    Cleared(this, EventArgs.Empty);
            }
            finally
            {
                fCashLock.ExitWriteLock();
                fFreeIdsLock.ExitWriteLock();
            }
        }

        /// <summary>
        ///     Saves the property values of the Current object to the stream. It also flushes all the currently cashed content to
        ///     disk.
        /// </summary>
        /// <remarks>
        ///     This method is thread safe, it is locked at the beginning of the call and only unlocked at the end.
        /// </remarks>
        public void Save(string path)
        {
            fCashLock.EnterUpgradeableReadLock();
            try
            {
                RemoveDeletedNeurons();
                SaveState(path);
                Storage.SaveEntryNeuronsFor(from i in fSins select (Neuron)i, GetType());
            }
            finally
            {
                fCashLock.ExitUpgradeableReadLock();
            }
            Flush();
        }

        /// <summary>
        ///     Removes the neurons that were deleted, but for which this was not yet flushed to the db backend.
        /// </summary>
        private void RemoveDeletedNeurons()
        {
            fNeuronsToDeleteLock.EnterWriteLock();
            try
            {
                foreach (var i in fNeuronsToDelete)
                    Storage.RemoveNeuron(i);
                fNeuronsToDelete.Clear();
            }
            finally
            {
                fNeuronsToDeleteLock.ExitWriteLock();
            }
        }

        /// <summary>
        ///     Saves the Brain object to disk, but doesn't flush the cashed content to disk.
        /// </summary>
        /// <remarks>
        ///     While saving, the path to the storage path is made relative
        /// </remarks>
        /// <param name="aDest"></param>
        private void SaveState(string path)
        {
            using (var iStr = new FileStream(path, FileMode.Create, FileAccess.ReadWrite))
            {
                var iPath = Storage.DataPath;
                try
                {
                    Storage.DataPath = PathUtil.RelativePathTo(PathUtil.VerifyPathEnd(Path.GetDirectoryName(path)),
                        PathUtil.VerifyPathEnd(Storage.DataPath));
                    //need to make it relative for saving so that the project can be moved.
                    try
                    {
                        var iSer = new XmlSerializer(typeof(Brain));
                        using (TextWriter iWriter = new StreamWriter(iStr))
                            iSer.Serialize(iWriter, Current);
                        IsChanged = false;
                    }
                    catch (Exception e)
                    {
                        Log.Log.LogError("Brain.SaveState", e.Message);
                    }
                }
                finally
                {
                    Storage.DataPath = iPath; //need to restore the path so that it can be used again.
                }
            }
        }

        /// <summary>
        ///     Flushes the data in the cash to disk.
        /// </summary>
        /// <remarks>
        ///     The cash is stored in <see cref="Brain.fNeuronCash" />. Files are stored at <see cref="Brain.NeuronsPath" />
        ///     <para>
        ///         This method is thread safe, it is locked at the beginning of the call and only unlocked at the end.
        ///     </para>
        /// </remarks>
        public void Flush()
        {
            fCashLock.EnterUpgradeableReadLock();
            try
            {
                try
                {
                    foreach (var iSin in fSins) //use the general method of asking each Sin to store extra data.
                        iSin.Flush();
                    TextSin.SaveDict(); //and also some custom ways for storing Sin data.
                    var iToSave = (from i in fNeuronCash select i.Value).ToList();
                    //we will be changing the dictionary during the flush (possibly: when hard refs are made into weakreferences)
                    foreach (var i in iToSave)
                    {
                        var iToProcess = i as Neuron;
                        if (iToProcess == null)
                        {
                            var iRef = i as WeakReference;
                            Debug.Assert(iRef != null);
                            if (iRef.IsAlive)
                                iToProcess = (Neuron)iRef.Target;
                        }
                        if (iToProcess != null && iToProcess.ID != Neuron.TempId && iToProcess.IsChanged)
                            //we don't store temp items cause they are never used in a link or meaning.
                            Storage.SaveNeuron(iToProcess);
                    }
                }
                catch (Exception e)
                {
                    Log.Log.LogError("Brain.Flush", e.ToString());
                }
            }
            finally
            {
                fCashLock.ExitUpgradeableReadLock();
            }
        }

        /// <summary>
        ///     Makes the specified neuron a temp one that will be registered the first time it is used.
        /// </summary>
        /// <param name="toChange"></param>
        public void MakeTemp(Neuron toChange)
        {
            toChange.ID = Neuron.TempId;
        }

        /// <summary>
        ///     Creates a new set of initial neurons.
        /// </summary>
        /// <remarks>
        ///     Also raises the <see cref="Brain.Loaded" /> event since, when this operation is doen, a new brain has been loaded.
        /// </remarks>
        public static void New()
        {
            TextSin.ResetDict(); //whenever a new brain is loaded, we need to reset the dictionary.
            Current.LoadDefaultNeurons();
            if (Current.Loaded != null)
                Current.Loaded(Current, EventArgs.Empty);
            Current.IsChanged = false;
        }

        /// <summary>
        ///     Tries to create and store an object of each type in all the loaded assemblies that has
        ///     a <see cref="NeuronIDAttribute" />.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         This method is thread safe, it is locked at the beginning of the call and only unlocked at the end.
        ///     </para>
        /// </remarks>
        private void LoadDefaultNeurons()
        {
            fCashLock.EnterUpgradeableReadLock();
            try
            {
                try
                {
                    var iFailCount = 0; //keeps track of how many constructors failed.
                    var iAssemblies = AppDomain.CurrentDomain.GetAssemblies();
                    foreach (var iAsm in iAssemblies)
                    {
                        var iTypes = iAsm.GetTypes();
                        foreach (var iType in iTypes)
                        {
                            var iAttribs =
                                (NeuronIDAttribute[])iType.GetCustomAttributes(typeof(NeuronIDAttribute), false);
                            if (iAttribs.Length > 0)
                            {
                                foreach (var iAttrib in iAttribs)
                                {
                                    var iNeuronType = iAttrib.Type;
                                    if (iNeuronType == null)
                                        iNeuronType = iType;
                                    if (typeof(Neuron).IsAssignableFrom(iNeuronType) == false)
                                    {
                                        iFailCount++;
                                        Log.Log.LogInfo("Brain.LoadDefaultNeurons",
                                            string.Format(
                                                "Unable to create a default neuron for {0} because it is not derived from Neuron.",
                                                iType));
                                    }
                                    else
                                    {
                                        var iObj = Activator.CreateInstance(iNeuronType) as Neuron;
                                        if (iObj == null)
                                        {
                                            iFailCount++;
                                            Log.Log.LogInfo("Brain.LoadDefaultNeurons",
                                                string.Format("Failed to load default neuron: {0}.", iType));
                                        }
                                        else
                                        {
                                            iObj.IsChanged = true; //newly created items, must be stored in cash.
                                            iObj.ID = iAttrib.ID;
                                            AddToCash(iObj);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (iFailCount == 0)
                        Log.Log.LogInfo("Brain.LoadDefaultNeurons", "Succesfully loaded all default neurons.");
                    else
                        Log.Log.LogInfo("Brain.LoadDefaultNeurons",
                            string.Format("Found {0} errors while loading all default neurons.", iFailCount));
                }
                catch (Exception e)
                {
                    Log.Log.LogError("Brain.LoadDefaultNeurons",
                        string.Format("Failed to load the default neurons with message: {0}", e));
                }
            }
            finally
            {
                fCashLock.ExitUpgradeableReadLock();
            }
        }

        /// <summary>
        ///     Adds a new neuron object to the brain and assigns a valid <see cref="Neuron.ID" /> to the object.
        /// </summary>
        /// <exception cref="BrainException">The object already has an ID, so it can't be saved.</exception>
        /// <exception cref="BrainException">The brain can't store neurons created in sub processors!</exception>
        /// <exception cref="BrainException">The brain is full!</exception>
        /// <remarks>
        ///     <para>
        ///         When the neuron being added is a <see cref="Sin" /> is is also added to the internal list of Sins.
        ///     </para>
        ///     <para>
        ///         By default, the item is simply added to the working memory.  It is not persisted to the
        ///         <see cref="Brain.Storage" />.
        ///     </para>
        ///     <para>
        ///         This method is thread safe, it is locked at the moment when the internal cash is accessed and unlocked when
        ///         done.
        ///     </para>
        ///     <para>
        ///         When an add is requested from a sub processor, the request is denied.  This is because sub processors
        ///         are temporary and most of them will create invalid information that needs to be disgarded, so an
        ///         add is not allowed.  This is done by checking a static that has a unique value for each thread.
        ///     </para>
        /// </remarks>
        /// <param name="toAdd">The object to add.</param>
        public void Add(Neuron toAdd)
        {
            CheckStateForAdd();

            if (toAdd.ID == Neuron.EmptyId || toAdd.ID == Neuron.TempId)
            {
                fFreeIdsLock.EnterUpgradeableReadLock();
                try
                {
                    if (fFreeIds.Count > 0)
                    {
                        fFreeIdsLock.EnterWriteLock();
                        try
                        {
                            toAdd.ID = fFreeIds.Dequeue();
                        }
                        finally
                        {
                            fFreeIdsLock.ExitWriteLock();
                        }
                    }
                    else
                    {
                        toAdd.ID = NextID;
                        NextID++;
                    }
                }
                finally
                {
                    fFreeIdsLock.ExitUpgradeableReadLock();
                }
                RemoveFromNeuronsToDelete(toAdd.ID);
                AddToCash(toAdd);
                ProcessAfterAdd(toAdd);
            }
            else
                throw new BrainException("The object already has an ID assigned, can't add it to this brain!");
        }

        /// <summary>
        ///     Removes the id from the list of items that need to delete. This is to make certain that, when an id
        ///     is reused, it doesn't remain qeueud for deletion.
        /// </summary>
        /// <param name="id">The id.</param>
        private void RemoveFromNeuronsToDelete(ulong id)
        {
            fNeuronsToDeleteLock.EnterWriteLock();
            try
            {
                fNeuronsToDelete.Remove(id);
                //each time we create a new neuron, we make certain that the ID is no longer in the 'ToDelete' list. This makes certain that the list doesn't get to big.  We do this always (even if we are not in AutoSaveNeurons mode -> the state was determined at the time it was added, if it is in there and the id is reused, the file will be overwrittern anyway.
            }
            finally
            {
                fNeuronsToDeleteLock.ExitWriteLock();
            }
        }

        /// <summary>
        ///     Adds the neuron to the cash.
        /// </summary>
        /// <remarks>
        ///     Tries to add the neuron as a weakReference if possible (we have a datastore) so that it gets unloaded when
        ///     not used.
        /// </remarks>
        /// <param name="toAdd">To add.</param>
        private void AddToCash(Neuron toAdd)
        {
            fCashLock.EnterWriteLock();
            try
            {
                var iStorage = Storage as LongtermMem;
                switch (Settings.StorageMode)
                {
                    case NeuronStorageMode.AlwaysInMem:
                        fNeuronCash.Add(toAdd.ID, toAdd);
                        break;

                    case NeuronStorageMode.StreamWhenPossible:
                        if (toAdd.IsChanged == false && iStorage != null &&
                            string.IsNullOrEmpty(iStorage.DataPath) == false)
                            fNeuronCash.Add(toAdd.ID, new WeakReference(toAdd));
                        else
                            fNeuronCash.Add(toAdd.ID, toAdd);
                        break;

                    case NeuronStorageMode.AlwaysStream:
                        fNeuronCash.Add(toAdd.ID, new WeakReference(toAdd));
                        break;

                    default:
                        throw new InvalidOperationException();
                        break;
                }
            }
            finally
            {
                fCashLock.ExitWriteLock();
            }
        }

        /// <summary>
        ///     Notifies the brain that the specified neuron has changed.
        /// </summary>
        /// <remarks>
        ///     Important for storage mode (correct updates).
        /// </remarks>
        /// <param name="changed">The changed.</param>
        internal void NotifyChanged(Neuron changed)
        {
            IsChanged = true;
            if (Settings.StorageMode == NeuronStorageMode.StreamWhenPossible)
            {
                fCashLock.EnterUpgradeableReadLock();
                try
                {
                    object iFound;
                    if (fNeuronCash.TryGetValue(changed.ID, out iFound))
                    //when we add an item, we first set the ID, which triggeres the changed, but offcourse, the item isn't yet in the dictionary, so check for this situation.
                    {
                        if (iFound is WeakReference) //if it is a weakrefrefence, we replace it with a hard reference.
                        {
                            fCashLock.EnterWriteLock();
                            try
                            {
                                fNeuronCash[changed.ID] = changed;
                            }
                            finally
                            {
                                fCashLock.ExitWriteLock();
                            }
                        }
                    }
                }
                finally
                {
                    fCashLock.ExitUpgradeableReadLock();
                }
            }
        }

        /// <summary>
        ///     Notifies the brain that the specified neuron has been saved.
        /// </summary>
        /// <remarks>
        ///     Important for storage mode (correct updates): we need to make hardreference weak again, if required.
        /// </remarks>
        /// <param name="changed">The changed.</param>
        internal void NotifySaved(Neuron changed)
        {
            if (Settings.StorageMode == NeuronStorageMode.StreamWhenPossible)
            {
                fCashLock.EnterWriteLock();
                try
                {
                    var iFound = fNeuronCash[changed.ID];
                    if (iFound is Neuron) //if it is a weakrefrefence, we replace it with a hard reference.
                        fNeuronCash[changed.ID] = new WeakReference(changed);
                }
                finally
                {
                    fCashLock.ExitWriteLock();
                }
            }
        }

        /// <summary>
        ///     Adds a new neuron object to the brain and tries to use the specified value as <see cref="Neuron.ID" /> for the
        ///     object.
        ///     This is usefull if you want to restore a neuron.
        /// </summary>
        /// <exception cref="BrainException">The object already has an ID, so it can't be saved.</exception>
        /// <exception cref="BrainException">The brain can't store neurons created in sub processors!</exception>
        /// <exception cref="BrainException">The brain is full!</exception>
        /// <remarks>
        ///     <para>
        ///         If the id is not available, or not accesseable, a new one is created for the item, otherwise, the specified
        ///         id is used.
        ///     </para>
        ///     <para>
        ///         When the neuron being added is a <see cref="Sin" /> is is also added to the internal list of Sins.
        ///     </para>
        ///     <para>
        ///         By default, the item is simply added to the working memory.  It is not persisted to the
        ///         <see cref="Brain.Storage" />.
        ///     </para>
        ///     <para>
        ///         This method is thread safe, it is locked at the moment when the internal cash is accessed and unlocked when
        ///         done.
        ///     </para>
        ///     <para>
        ///         When an add is requested from a sub processor, the request is denied.  This is because sub processors
        ///         are temporary and most of them will create invalid information that needs to be disgarded, so an
        ///         add is not allowed.  This is done by checking a static that has a unique value for each thread.
        ///     </para>
        /// </remarks>
        /// <param name="toAdd">The object to add.</param>
        public void Add(Neuron toAdd, ulong id)
        {
            CheckStateForAdd();
            if (toAdd.ID == Neuron.EmptyId || toAdd.ID == Neuron.TempId)
            {
                fFreeIdsLock.EnterWriteLock();
                //we do a write lock from the start cause need to a write in to many different locations.
                try
                {
                    if (fFreeIds.Count > 0 && fFreeIds.Peek() == id)
                        //we can only handle this if the last item on the stack is the id, can't remove from inside the stack (bummer).
                        toAdd.ID = fFreeIds.Dequeue();
                    else if (NextID < id)
                    {
                        NextID++;
                        while (NextID < id - 1)
                            fFreeIds.Enqueue(NextID);
                        toAdd.ID = id;
                    }
                    else
                    {
                        toAdd.ID = NextID;
                        NextID++;
                    }
                    AddToCash(toAdd);
                }
                finally
                {
                    fFreeIdsLock.ExitWriteLock();
                }
                ProcessAfterAdd(toAdd);
            }
            else
                throw new BrainException("The object already has an ID assigned, can't add it to this brain!");
        }

        /// <summary>
        ///     Processes the item after it was added to the brain. This includes raising the event and checking if it is a sin.
        /// </summary>
        /// <param name="toAdd">Item that was added.</param>
        private void ProcessAfterAdd(Neuron toAdd)
        {
            if (toAdd is Sin)
                fSins.Add((Sin)toAdd);
            if (HasNeuronChangedEvents)
            {
                var iArgs = new NeuronChangedEventArgs
                {
                    Action = BrainAction.Created,
                    OriginalSource = toAdd,
                    OriginalSourceID = toAdd.ID
                };
                OnNeuronChanged(iArgs);
            }
        }

        /// <summary>
        ///     Checks the state of the brain for an add.
        /// </summary>
        private void CheckStateForAdd()
        {
            //if (Processor.CurrentProcessor != null && Processor.CurrentProcessor.IsSub == true)
            //   throw new BrainException("The brain can't store neurons created in sub processors!");
            if (NextID == ulong.MaxValue)
                throw new BrainException("The brain is full!");
        }

        /// <summary>
        ///     Replaces the object representation of a neuron with a new one.
        /// </summary>
        /// <remarks>
        ///     This function is used for instance, when the type of a neuron is changed to replace
        ///     a physical object with the new representation of the item.
        ///     <para>
        ///         Thread safe.
        ///     </para>
        /// </remarks>
        /// <param name="old">The old neuron</param>
        /// <param name="fresh">The new neuron.</param>
        internal void Replace(Neuron old, Neuron fresh)
        {
            fCashLock.EnterWriteLock();
            try
            {
                if (fresh.ID != old.ID)
                    fresh.ID = old.ID;
                fresh.IsChanged = true; //always needs to be saved again.
                if (fNeuronCash.ContainsKey(old.ID)) //if it's cashed, update the cashe.
                {
                    if (fNeuronCash[old.ID] is WeakReference)
                        fNeuronCash[old.ID] = new WeakReference(fresh);
                    else
                        fNeuronCash[old.ID] = fresh;
                }
                if (HasNeuronChangedEvents)
                {
                    var iArgs = new NeuronChangedEventArgs
                    {
                        Action = BrainAction.Changed,
                        OriginalSource = old,
                        NewValue = fresh,
                        OriginalSourceID = old.ID
                    };
                    OnNeuronChanged(iArgs);
                }
                old.IsChanged = false;
                //we use this to prevent the old item from being streamed to disk when it is no longer valid, we can't
                old.ID = Neuron.EmptyId;
            }
            finally
            {
                fCashLock.ExitWriteLock();
            }
        }

        /// <summary>
        ///     Permanently removes an item from the brain when possible.
        /// </summary>
        /// <exception cref="BrainException">The brain can't remove neurons from sub processors!</exception>
        /// <exception cref="BrainException">The neuron is not stored in the brain!!</exception>
        /// <exception cref="BrainException">The id recycle list is full!</exception>
        /// <remarks>
        ///     <para>
        ///         The id is recycled.  Because of this recyclying, only int.MaxValue number of items can be deleted at the
        ///         same time (which is much less than the brain can store in total).
        ///     </para>
        ///     <para>
        ///         When a neuron is removed, all the links to and from that neuron are also destroyed. The neuron is also
        ///         removed from any clusters that contain it. All the code associated with the neuron is also removed from
        ///         the brain.
        ///     </para>
        ///     <para>
        ///         When a remove is requested from a sub processor, the request is denied.  This is because sub processors
        ///         are temporary and most of them will create invalid information that needs to be disgarded, so a
        ///         remove is not allowed.  This is done by checking a static that has a unique value for each thread.
        ///     </para>
        /// </remarks>
        /// <param name="toDelete">Item to delete</param>
        /// <returns>
        ///     True when the item was successfully deleted.  False when the item is blocked and can't be deleted.
        ///     An item can be blocked because:
        ///     - it is used as the value for 1 or more <see cref="Link.Meaning" /> props,
        ///     - it is used as the value for 1 or more <see cref="NeuronCluster.Meaning" /> props,
        ///     - it is used as a value in one or more <see cref="Link.Info" /> lists or,
        ///     - it is a statically created item (id less than <see cref="PredefinedNeurons.Dynamic" />
        /// </returns>
        public bool Delete(Neuron toDelete)
        {
            if (toDelete.ID == Neuron.EmptyId || toDelete.ID == Neuron.TempId)
                throw new BrainException("The neuron is not stored in the brain!");
            if (toDelete.CanBeDeleted == false)
                return false;
            if (toDelete.ID == NextID) //if we are trying to remove the last item, simply decrease the counter
                DecLastID();
            else
                AddFreeId(toDelete.ID);
            foreach (var i in fSins)
                //also need to let each sin know that a possible entry point has been remove.  This is required for the textsin in case a textneuron is removed.
                i.RemoveEntryPoint(toDelete);
            toDelete.Clear();
            RemoveFromCash(toDelete.ID);
            RemoveFromStorage(toDelete);
            if (toDelete is Sin)
                fSins.Remove((Sin)toDelete);
            if (HasNeuronChangedEvents)
            {
                var iArgs = new NeuronChangedEventArgs
                {
                    Action = BrainAction.Removed,
                    OriginalSource = toDelete,
                    OriginalSourceID = toDelete.ID
                };
                OnNeuronChanged(iArgs);
            }
            toDelete.ID = Neuron.EmptyId;
            //we need to reset the id so that other parts of the system now it no longer belongs to a brain.  We do this after all the events so that the event handlers can still see the correct id.
            return true;
        }

        /// <summary>
        ///     Removes the neuron from the backend storage.
        /// </summary>
        /// <param name="toDelete">Item To remove.</param>
        private void RemoveFromStorage(Neuron toDelete)
        {
            switch (Settings.StorageMode)
            {
                case NeuronStorageMode.AlwaysInMem:
                    Storage.RemoveNeuron(toDelete);
                    break;

                case NeuronStorageMode.StreamWhenPossible:
                    if (toDelete.IsChanged == false)
                        Storage.RemoveNeuron(toDelete);
                    else
                        AddToNeuronsToDelete(toDelete.ID);
                    break;

                case NeuronStorageMode.AlwaysStream:
                    AddToNeuronsToDelete(toDelete.ID);
                    break;
            }
        }

        /// <summary>
        ///     Adds id to the list of neurons to delete.
        /// </summary>
        /// <param name="id">The id.</param>
        private void AddToNeuronsToDelete(ulong id)
        {
            fNeuronsToDeleteLock.EnterWriteLock();
            try
            {
                fNeuronsToDelete.Add(id);
            }
            finally
            {
                fNeuronsToDeleteLock.ExitWriteLock();
            }
        }

        /// <summary>
        ///     Removes the id from the cash.
        /// </summary>
        /// <param name="id">The id to remove.</param>
        private void RemoveFromCash(ulong id)
        {
            fCashLock.EnterWriteLock();
            try
            {
                fNeuronCash.Remove(id);
            }
            finally
            {
                fCashLock.ExitWriteLock();
            }
        }

        /// <summary>
        ///     Adds the id to the list of free ids.
        /// </summary>
        /// <param name="id">The id.</param>
        private void AddFreeId(ulong id)
        {
            fFreeIdsLock.EnterWriteLock();
            try
            {
                if (fFreeIds.Count == int.MaxValue)
                    throw new BrainException("The id recycle list is full!");
                fFreeIds.Enqueue(id);
            }
            finally
            {
                fFreeIdsLock.ExitWriteLock();
            }
        }

        /// <summary>
        ///     Deletes the neuron with the specified id.
        /// </summary>
        /// <param name="id">The id of the neuron to remove.</param>
        public void Delete(ulong id)
        {
            Delete(this[id]);
        }

        /// <summary>
        ///     Raises the <see cref="Brain.LinkChanged" /> event.
        /// </summary>
        /// <remarks>
        ///     Also see <see cref="Brain.HasLinkChangedEvents" />.
        /// </remarks>
        /// <param name="sender">The link that changed.</param>
        /// <param name="e">The event arguments.</param>
        internal void OnLinkChanged(LinkChangedEventArgs e)
        {
            if (LinkChanged != null)
                LinkChanged(this, e);
        }

        /// <summary>
        ///     Raises the <see cref="Brain.NeuronChanged" /> event.
        /// </summary>
        /// <remarks>
        ///     Also see <see cref="Brain.HasNeuronChangedEvents" />.
        ///     Only events for registered or temp neurons are raised.
        /// </remarks>
        /// <param name="sender">The Neuron that changed.</param>
        /// <param name="e">The event arguments.</param>
        internal void OnNeuronChanged(NeuronChangedEventArgs e)
        {
            if (NeuronChanged != null && e.OriginalSource.ID > Neuron.EmptyId)
                NeuronChanged(this, e);
        }

        /// <summary>
        ///     Raises the <see cref="Brain.NeuronChanged" /> event.
        /// </summary>
        /// <remarks>
        ///     Also see <see cref="Brain.HasNeuronChangedEvents" />.
        ///     Only events for registered or temp neurons are raised.
        /// </remarks>
        /// <param name="sender">The Neuron who owns the list that's changed.</param>
        /// <param name="e">The event arguments (the actual list that changed can be found here + more data about what changed..</param>
        internal void OnNeuronListChanged(NeuronListChangedEventArgs e)
        {
            if (NeuronListChanged != null)
                NeuronListChanged(this, e);
        }

        #endregion functions

        #region IXmlSerializable Members

        /// <summary>
        ///     This method is reserved and should not be used. When implementing the IXmlSerializable interface, you should return
        ///     null (Nothing in Visual Basic) from this method, and instead, if specifying a custom schema is required, apply the
        ///     <see cref="T:System.Xml.Serialization.XmlSchemaProviderAttribute" /> to the class.
        /// </summary>
        /// <returns>
        ///     An <see cref="T:System.Xml.Schema.XmlSchema" /> that describes the XML representation of the object that is
        ///     produced by the <see cref="M:System.Xml.Serialization.IXmlSerializable.WriteXml(System.Xml.XmlWriter)" /> method
        ///     and consumed by the <see cref="M:System.Xml.Serialization.IXmlSerializable.ReadXml(System.Xml.XmlReader)" />
        ///     method.
        /// </returns>
        public XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        ///     Generates an object from its XML representation.
        /// </summary>
        /// <param name="reader">The <see cref="T:System.Xml.XmlReader" /> stream from which the object is deserialized.</param>
        public void ReadXml(XmlReader reader)
        {
            var wasEmpty = reader.IsEmptyElement;

            reader.Read();
            if (wasEmpty) return;

            reader.ReadStartElement("Count");
            var iVal = reader.ReadString();
            var iConverted = ulong.Parse(iVal);
            fNextID = iConverted;
            reader.ReadEndElement();

            var valueSerializer = new XmlSerializer(typeof(LongtermMem));
            Storage = (LongtermMem)valueSerializer.Deserialize(reader);
            reader.MoveToContent();

            wasEmpty = reader.IsEmptyElement;
            reader.ReadStartElement("FreeIds");
            if (wasEmpty == false)
            {
                while (reader.NodeType != XmlNodeType.EndElement)
                {
                    reader.ReadStartElement("ID");
                    iVal = reader.ReadString();
                    iConverted = ulong.Parse(iVal);
                    fFreeIds.Enqueue(iConverted); //don't need thread safe access while loading the network.
                    reader.ReadEndElement();
                    reader.MoveToContent();
                }
                reader.ReadEndElement();
            }
        }

        /// <summary>
        ///     Converts an object into its XML representation.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Xml.XmlWriter" /> stream to which the object is serialized.</param>
        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Count");
            writer.WriteString(fNextID.ToString());
            writer.WriteEndElement();

            var valueSerializer = new XmlSerializer(typeof(LongtermMem));
            valueSerializer.Serialize(writer, (LongtermMem)Storage);

            writer.WriteStartElement("FreeIds");
            fFreeIdsLock.EnterReadLock();
            //a project safe can occur while the network is running, so we need to make certain it doesn't change while saving.
            try
            {
                foreach (var i in fFreeIds)
                {
                    writer.WriteStartElement("ID");
                    writer.WriteString(i.ToString());
                    writer.WriteEndElement();
                }
            }
            finally
            {
                fFreeIdsLock.ExitReadLock();
            }
            writer.WriteEndElement();
        }

        #endregion IXmlSerializable Members
    }

    #region Exceptions

    [Serializable]
    public class BrainException : Exception
    {
        //
        // For guidelines regarding the creation of new exception types, see
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
        // and
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
        //

        public BrainException()
        {
        }

        public BrainException(string message) : base(message)
        {
        }

        public BrainException(string message, Exception inner) : base(message, inner)
        {
        }

        protected BrainException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }

    #endregion Exceptions
}