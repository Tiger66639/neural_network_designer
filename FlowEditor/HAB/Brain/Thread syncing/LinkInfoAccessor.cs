﻿using JaStDev.HAB.Brain.Lists;
using System.Threading;

namespace JaStDev.HAB.Brain.Thread_syncing
{
    /// <summary>
    /// An <see cref="Accessor"/> specicific for <see cref="Link.LinkInfo"/> data.
    /// </summary>
    public class LinkInfoAccessor : NeuronsRWAccessor
    {
        public LinkInfoAccessor(LinkInfoList list, AccessorMode mode, ReaderWriterLockSlim rwLock)
           : base(list, mode, rwLock)
        {
        }
    }
}