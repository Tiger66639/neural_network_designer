﻿using JaStDev.HAB.Brain.Lists;
using System.Threading;

namespace JaStDev.HAB.Brain.Thread_syncing
{
    /// <summary>
    /// provides specialized access to a <see cref="ClusterList"/>.
    /// </summary>
    public class ClustersAccessor : NeuronsAccessor
    {
        public ClustersAccessor(ClusterList list, AccessorMode mode, ReaderWriterLockSlim rwLock)
           : base(list, mode, rwLock)
        {
        }

        /// <summary>
        /// Adds the item directly without raising events in a thread safe way.
        /// </summary>
        /// <param name="toAdd">To add.</param>
        internal void AddDirect(NeuronCluster toAdd)
        {
            if (Mode == AccessorMode.Write)
            {
                ClusterList iList = (ClusterList)List;
                iList.InsertDirect(iList.Count, toAdd);
            }
        }

        internal void RemoveDirect(NeuronCluster toRemove)
        {
            if (Mode == AccessorMode.Write)
            {
                ClusterList iList = (ClusterList)List;
                iList.RemoveDirect(toRemove);
            }
        }
    }
}