﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JaStDev.HAB.Expressions
{
    /// <summary>
    ///     This expression is always a part of a <see cref="ConditionalStatement" />.  It represents a single branch
    ///     of a conditional statement.
    /// </summary>
    /// <remarks>
    ///     Execution of the statements of a conditional expression doesn't perform the evaluation to check if the statements
    ///     need to be
    ///     executed, they are always executed by calling this function.  To evaluate, call
    ///     <see cref="ConditionalExpression.EvaluateCondition" />. This is seperate cause we have now way of knowing
    ///     how and when it should be evaluated.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.ConditionalPart, typeof(Neuron))]
    public class ConditionalExpression : ExpressionsBlock
    {
        /// <summary>
        ///     The condition to evaluate.  If this returns true, the <see cref="Expression.Statements" /> are executed.
        /// </summary>
        /// <remarks>
        ///     This is a regular neuron and not a specific <see cref="ConditonalExpression" /> cause the condition can be
        ///     anything.
        ///     If the loop is a <see cref="PredefinedNeurons.WhileDo" /> or <see cref="PredefinedNeurons.DoWhile" />, it should
        ///     be a conditionalExpression, howeever, if it is cased, this can be a neuron, or another
        ///     <see cref="ResultExpression" />.
        /// </remarks>
        public ResultExpression Condition
        {
            get { return FindFirstOut((ulong)PredefinedNeurons.Condition) as ResultExpression; }
            set { SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Condition, value); }
        }

        #region TypeOfNeuron

        /// <summary>
        ///     Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ConditionalPart" />.</value>
        public override Neuron TypeOfNeuron
        {
            get { return Brain.Brain.Current[(ulong)PredefinedNeurons.ConditionalPart]; }
        }

        #endregion TypeOfNeuron

        /// <summary>
        ///     Returns the evalulation of the condition.
        /// </summary>
        /// <remarks>
        ///     If there is a <b>compareTo</b> value specified, a case statement is presumed, and all the
        ///     neurons in the compareTo must also be in Condition (after solving a possible expression) and no more.
        ///     If compareTo is null, a boolean expression is presumed and it's result is returned or an error logged
        ///     and false returned.
        ///     When the condition is empty, true is always presumed.
        /// </remarks>
        /// <param name="compareTo">The posssible list of neurons that must be found through the condition.</param>
        /// <returns></returns>
        internal bool EvaluateCondition(IEnumerable<Neuron> compareTo, Processor.Processor processor)
        {
            if (compareTo == null)
            {
                var iCond = Condition;
                if (iCond != null)
                {
                    var iRes = iCond.GetValue(processor);
                    foreach (var i in iRes)
                    {
                        if (i != null && i.ID == (ulong)PredefinedNeurons.False)
                            return false;
                    }
                }
                return true;
            }
            else
            {
                var iCond = SolveResultExp(Condition, processor);
                return compareTo.SequenceEqual(iCond);
            }
        }

        public override string ToString()
        {
            var iStr = new StringBuilder("(");
            var iCond = Condition;
            if (iCond != null)
                iStr.Append(iCond);
            iStr.AppendLine(")");

            return iStr.ToString();
        }

        #region ctor

        public ConditionalExpression()
        {
        }

        /// <summary>
        ///     Creates a new ConditionalExpression, with the proper links already made.
        /// </summary>
        /// <param name="condition">The condition that determins if the statements are executed or not.</param>
        /// <param name="statements">The list of statements that are executed if the condition is evaluated to true.</param>
        public ConditionalExpression(Variable condition, NeuronCluster statements)
        {
            var iNew = new Link(condition, this, (ulong)PredefinedNeurons.Condition);
            iNew = new Link(statements, this, (ulong)PredefinedNeurons.Statements);
        }

        #endregion ctor
    }
}