﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;

namespace JaStDev.HAB.Expressions
{
    /// <summary>
    ///     An expression that takes 1 <see cref="ByRefExpression.Argument" /> and simply returns it.  This is usefull
    ///     to get the reference to code instead of solving it.
    /// </summary>
    [NeuronID((ulong)PredefinedNeurons.ByRefExpression, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Argument, typeof(Neuron))]
    public class ByRefExpression : SimpleResultExpression
    {
        #region Argument

        /// <summary>
        ///     Gets/sets the argument to the expression.
        /// </summary>
        /// <remarks>
        ///     This neuron is simly returned when the expression is executed, even if the argument is another expression,
        ///     it is not solved.
        /// </remarks>
        public Neuron Argument
        {
            get { return FindFirstOut((ulong)PredefinedNeurons.Argument); }
            set { SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Argument, value); }
        }

        #endregion Argument

        #region TypeOfNeuron

        /// <summary>
        ///     Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ByRefExpression" />.</value>
        public override Neuron TypeOfNeuron
        {
            get { return Brain.Brain.Current[(ulong)PredefinedNeurons.ByRefExpression]; }
        }

        #endregion TypeOfNeuron

        /// <summary>
        ///     Calcules the result of this expression and returns this as a list.
        /// </summary>
        /// <param name="processor"></param>
        /// <returns>
        ///     A list containing all the valid results for the expression.
        /// </returns>
        public override IEnumerable<Neuron> GetValue(Processor.Processor processor)
        {
            var iArg = Argument;
            if (iArg == null)
                iArg = Brain.Brain.Current[(ulong)PredefinedNeurons.Empty];
            return new List<Neuron> { iArg };
        }

        /// <summary>
        ///     Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </returns>
        public override string ToString()
        {
            var iArg = Argument;
            if (iArg != null)
                return "^(" + iArg + ")";
            return "^()";
        }
    }
}