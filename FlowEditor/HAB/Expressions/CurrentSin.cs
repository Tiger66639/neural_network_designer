﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;

namespace JaStDev.HAB.Expressions
{
    /// <summary>
    /// This system variable returns the <see cref="Sin"/> that triggered the current processor or for which the processor is currently
    /// preparing an output.
    /// </summary>
    [NeuronID((ulong)PredefinedNeurons.CurrentSin)]
    public class CurrentSin : SystemVariable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CurrentSin"/> class.
        /// </summary>
        public CurrentSin()
        {
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="proc">The proc.</param>
        /// <returns></returns>
        public override IEnumerable<Neuron> GetValue(Processor.Processor proc)
        {
            List<Neuron> iRes = new List<Neuron>(1);
            iRes.Add(proc.CurrentSin);
            return iRes;
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return "CurrentSin";
        }
    }
}