﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace JaStDev.HAB.Expressions
{
    /// <summary>
    /// A Variable result expression always returns a specific neuron (list). It's initial value is specified in <see cref="Variable.Value"/>.
    /// </summary>
    /// <remarks>
    /// <para>
    /// This type of expression can be used to reference somethng and who's reference can change during execution.
    /// </para>
    /// <para>
    /// You can also use a variable expression to encapsulate other expressions if you want to work on the expression object itself
    /// instead of executing it.  For instance, if you want to do a search in the actions lists of a result expression, it will be
    /// executed and it's result will be searched.  To prevent this, incapsulate it in this type of expression.
    /// </para>
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.Variable, typeof(Neuron))]
    public class Variable : SimpleResultExpression
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.Variable"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.Variable];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Gets/sets item that is encapsulated by this expression.
        /// </summary>
        public Neuron Value
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.Value);
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Value, value);
            }
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="proc">The proc.</param>
        /// <returns></returns>
        public override IEnumerable<Neuron> GetValue(Processor.Processor proc)
        {
            return InternalGetvalue(proc);
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return "var(" + ID.ToString() + ")";
        }

        /// <summary>
        /// Stores the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="proc">The proc.</param>
        protected internal virtual void StoreValue(IEnumerable<Neuron> value, Processor.Processor proc)
        {
            Dictionary<ulong, List<Neuron>> iDict = proc.VariableValues.Peek();
            Debug.Assert(iDict != null);
            iDict[this.ID] = value.ToList();
        }

        private List<Neuron> InternalGetvalue(Processor.Processor proc)
        {
            return proc.GetValueFor(this);
        }

        #region ctor

        /// <summary>
        /// A constructor that initializes the object with a reference to the specified <see cref="Neuron"/>.
        /// </summary>
        /// <param name="value"></param>
        public Variable(Neuron value)
        {
            Link iNew = new Link(value, this, (ulong)PredefinedNeurons.Value);
        }

        /// <summary>
        /// default constructor.
        /// </summary>
        public Variable()
        {
        }

        #endregion ctor
    }
}