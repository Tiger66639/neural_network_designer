﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace JaStDev.HAB.Expressions
{
    /// <summary>
    /// A conditional statement (to compare with if-then-else + do-while + while-do + repeat + for + foreach.
    /// </summary>
    /// <remarks>
    /// Uses the same conditional technique as gene.
    /// <para>
    /// Doesn't do a for loop, this can easely be done with a while loop.
    /// </para>
    /// <para>
    /// - For each loop:
    /// 1 condition expected which should return the list of items to walk through, it's statements are executed.
    /// The item that is currently processed, should be defined with the <see cref="ConditionalGroup.LoopItem"/>
    /// expression.
    /// </para>
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.ConditionalStatement, typeof(Neuron))]
    public class ConditionalStatement : Expression
    {
        #region ctor

        /// <summary>
        /// Default constructor
        /// </summary>
        public ConditionalStatement()
        {
        }

        /// <summary>
        /// Creates a new ConditionalExpression, with the proper links already made.
        /// </summary>
        /// <param name="conditions">The list of conditional expressions to evaluate.</param>
        /// <param name="loopstyle">The type of looping that should be used with this group.</param>
        public ConditionalStatement(NeuronCluster conditions, Neuron loopstyle)
        {
            Link iNew = new Link(conditions, this, (ulong)PredefinedNeurons.Condition);
            iNew = new Link(loopstyle, this, (ulong)PredefinedNeurons.LoopStyle);
        }

        #endregion ctor

        #region Conditions

        /// <summary>
        /// Gets the list of conditions that are contained in this group, as conditional expressions.
        /// </summary>
        /// <remarks>
        /// To edit this list, use the <see cref="Neuron.LinksTo"/> list.
        /// </remarks>
        public ReadOnlyCollection<ConditionalExpression> Conditions
        {
            get
            {
                NeuronCluster iCluster = ConditionsCluster;
                if (iCluster != null)
                {
                    using (ChildrenAccessor iList = iCluster.Children)
                    {
                        List<ConditionalExpression> iExps = iList.ConvertTo<ConditionalExpression>();
                        if (iExps != null)
                            return new ReadOnlyCollection<ConditionalExpression>(iExps);
                        else
                            Log.Log.LogError("ConditionalGroup.Conditions", string.Format("Failed to convert Conditions list of '{0}' to an executable list.", this));
                    }
                }
                return null;
            }
        }

        #endregion Conditions

        #region ConditionsCluster

        /// <summary>
        /// Gets the <see cref="NeuronCluster"/> used to store all the sub statements of this conditional expression.
        /// </summary>
        public NeuronCluster ConditionsCluster
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.Condition) as NeuronCluster;
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.Condition, value);
            }
        }

        #endregion ConditionsCluster

        #region LoopStyle

        /// <summary>
        /// Gets/sets the type of looping applied to this conditional groups.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Possible values are: Not, Case, Looped, CaseLooped, ForEach, For, Until
        /// </para>
        /// <para>
        /// This can be a result expression that needs to be solved.  Only a single value
        /// result is allowed.
        /// </para>
        /// </remarks>
        public Neuron LoopStyle
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.LoopStyle);
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.LoopStyle, value);
            }
        }

        #endregion LoopStyle

        #region LoopItem

        /// <summary>
        /// Gets/sets the <see cref="Variable"/> used to store the current value of a for or
        /// foreach loop. Only has to be set if this is a foreach loop.
        /// </summary>
        public Variable LoopItem
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.LoopItem) as Variable;
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.LoopItem, value);
            }
        }

        #endregion LoopItem

        #region CaseItem

        /// <summary>
        /// Gets/sets the <see cref="Variable"/> used to define the item or list of
        /// neurons to compare against in a case statemnt.  This value is only required
        /// if it is a case or looped case.
        /// </summary>
        public Variable CaseItem
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.CaseItem) as Variable;
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.CaseItem, value);
            }
        }

        #endregion CaseItem

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ConditionalStatement"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.ConditionalStatement];
            }
        }

        #endregion TypeOfNeuron

        #region Functions

        /// <summary>
        /// Only 1 loop style is allowed.
        /// </summary>
        /// <param name="handler"></param>
        protected internal override void Execute(Processor.Processor handler)
        {
            Neuron iLoopStyle = Neuron.SolveSingleResultExp(LoopStyle, handler);
            if (iLoopStyle != null)
            {
                switch (iLoopStyle.ID)
                {
                    case (ulong)PredefinedNeurons.Normal:
                        PerformIf(handler);
                        break;

                    case (ulong)PredefinedNeurons.Case:
                        PerformCase(handler);
                        break;

                    case (ulong)PredefinedNeurons.Looped:
                        while (PerformIf(handler) == true) ;
                        break;

                    case (ulong)PredefinedNeurons.CaseLooped:
                        while (PerformCase(handler) == true) ;
                        break;

                    case (ulong)PredefinedNeurons.Until:
                        PerformUntill(handler);
                        break;
                    //case (ulong)PredefinedNeurons.For:
                    //   PerformFor();
                    //   break;
                    case (ulong)PredefinedNeurons.ForEach:
                        PerformForEach(handler);
                        break;

                    default:
                        Log.Log.LogError("ConditionalGroup.Execute", string.Format("Unknown loop style: '{0}'.", LoopStyle));
                        break;
                }
            }
            else
                Log.Log.LogError("ConditionalGroup.Execute", string.Format("'{0}' is not/does not produce a valid loop style.", LoopStyle));
        }

        /// <summary>
        /// Checks the conditions if there are any empty allowed + if so, that they are at the back.
        /// </summary>
        /// <param name="nrAlowedEmpty">The nr of alowed empty conditions (usually 1 or 0). This is counted from the back.</param>
        private void CheckConditions(IList<ConditionalExpression> list, int nrAlowedEmpty)
        {
            for (int i = list.Count - 1; i >= 0; i--)
            {
                if (list[i].Condition == null && nrAlowedEmpty <= 0)
                    Log.Log.LogError("ConditionalStatement.CheckConditions", string.Format("Invalid condition at index: {0}!", list.Count - i - 1));
                nrAlowedEmpty--;
            }
        }

        /// <summary>
        /// uses the result of the condition of each <see cref="ConditionalExpression"/> as the 'for' part
        /// </summary>
        /// <remarks>
        /// Only 1 condition statement allowed.
        /// </remarks>
        private void PerformForEach(Processor.Processor proc)
        {
            ReadOnlyCollection<ConditionalExpression> iCond = Conditions;
            Variable iLoopItem = LoopItem;
            if (iLoopItem != null)
            {
                if (iCond != null && iCond.Count != 0)
                {
                    //if there are more than 1, it doesn't stop executing it, so only a warning is needed, there is simply to much and invalid info.
                    if (iCond.Count != 1)
                        Log.Log.LogWarning("ConditionalGroup.PerformForEach", "To many condition expressions defined in group, only first is evaluated in a for - each loop!");
                    CheckConditions(iCond, 0);
                    IEnumerable<Neuron> iItems = Neuron.SolveResultExp(iCond[0].Condition, proc);
                    foreach (Neuron i in iItems)
                    {
                        Dictionary<ulong, List<Neuron>> iDict = proc.VariableValues.Peek();
                        Debug.Assert(iDict != null);
                        iDict[iLoopItem.ID] = new List<Neuron>() { i };
                        proc.EvaluateBranch(iCond[0].StatementsCluster);
                    }
                }
                else
                    Log.Log.LogError("ConditionalGroup.PerformForEach", "Can't perform for - each: no conditions defined!");
            }
            else
                Log.Log.LogError("ConditionalGroup.PerformForEach", "Can't perform for - each: no conditions defined!");
        }

        ///// <summary>
        /////
        ///// </summary>
        //private void PerformFor()
        //{
        //   throw new NotImplementedException();
        //}

        /// <summary>
        /// Do the statements, check the condition after the perform.
        /// </summary>
        /// <remarks>
        /// only 1 conditional expressio allowed.
        /// </remarks>
        /// <returns></returns>
        private void PerformUntill(Processor.Processor proc)
        {
            ReadOnlyCollection<ConditionalExpression> iCond = Conditions;
            if (iCond != null && iCond.Count != 0)
            {
                //if there are more than 1, it doesn't stop executing it, so only a warning is needed, there is simply to much and invalid info.
                if (iCond.Count != 1)
                    Log.Log.LogWarning("ConditionalGroup.PerformUntill", "To many condition expressions defined in group, only first is evaluated and performed in an untill loop!");
                CheckConditions(iCond, 0);
                do
                {
                    proc.EvaluateBranch(iCond[0].StatementsCluster);
                } while (iCond[0].EvaluateCondition(null, proc) == true);
            }
            else
                Log.Log.LogError("ConditionalGroup.PerformForEach", "Can't perform for - each: no conditions defined!");
        }

        /// <summary>
        /// Evaluats the conditional expressions like in a case statement.
        /// </summary>
        /// <returns>True if there was a valid case conditional expression found, otherwise false.</returns>
        private bool PerformCase(Processor.Processor proc)
        {
            ReadOnlyCollection<ConditionalExpression> iCond = Conditions;
            CheckConditions(iCond, 1);
            Variable iCaseItem = CaseItem;
            if (iCaseItem != null)
            {
                IEnumerable<Neuron> iCaseValues = Neuron.SolveResultExp(iCaseItem, proc);
                if (iCaseValues != null)
                {
                    if (iCond != null && iCond.Count != 0)
                    {
                        foreach (ConditionalExpression i in iCond)
                        {
                            if (i.EvaluateCondition(iCaseValues, proc) == true)
                            {
                                proc.EvaluateBranch(i.StatementsCluster);
                                return true;
                            }
                        }
                    }
                    else
                        Log.Log.LogError("ConditionalGroup.PerformCase", "Can't perform case: no conditions defined!");
                }
                else
                    Log.Log.LogError("ConditionalGroup.PerformCase", "Can't perform case: CaseItem is null after execution!");
            }
            else
                Log.Log.LogError("ConditionalGroup.PerformCase", "Can't perform case: no case item defined!");
            return false;
        }

        private bool PerformIf(Processor.Processor proc)
        {
            ReadOnlyCollection<ConditionalExpression> iCond = Conditions;
            CheckConditions(iCond, 1);
            if (iCond != null && iCond.Count != 0)
            {
                foreach (ConditionalExpression i in iCond)
                {
                    if (i.EvaluateCondition(null, proc) == true)
                    {
                        NeuronCluster iCluster = i.StatementsCluster;
                        if (iCluster != null)                                                            //could be that there simply are no statements defined, this is legal, but not for the EvaluateBranch function, so we filter on this here.
                            proc.EvaluateBranch(iCluster);
                        return true;
                    }
                }
            }
            else
                Log.Log.LogError("ConditionalGroup.PerformIf", "Can't perform if statement: no conditions defined!");
            return false;
        }

        public override string ToString()
        {
            return "Conditional: " + ID.ToString();
        }

        #endregion Functions
    }
}