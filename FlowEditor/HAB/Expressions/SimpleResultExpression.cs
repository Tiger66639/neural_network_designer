﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;

namespace JaStDev.HAB.Expressions
{
    /// <summary>
    /// A base class for Expressions that, when executed, simply put there result on the stack.
    /// </summary>
    public abstract class SimpleResultExpression : ResultExpression
    {
        /// <summary>
        /// Executes this expression
        /// </summary>
        /// <param name="handler">The processor to execute the statements on.</param>
        protected internal override void Execute(Processor.Processor handler)
        {
            IEnumerable<Neuron> iRes = GetValue(handler);
            if (iRes != null)
            {
                foreach (Neuron i in iRes)
                    handler.Push(i);
            }
        }
    }
}