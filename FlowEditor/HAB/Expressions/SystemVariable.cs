﻿using JaStDev.HAB.Brain;
using System;
using System.Collections.Generic;

namespace JaStDev.HAB.Expressions
{
    /// <summary>
    /// A system variable is a special kind of variable that is prefilled by the brain, which can't be
    /// changed and describes something about the system.
    /// </summary>
    public class SystemVariable : Variable
    {
        /// <remarks>
        /// A system variable doesn't allow setting.
        /// </remarks>
        /// <param name="value">The value.</param>
        /// <param name="proc">The proc.</param>
        protected internal override void StoreValue(IEnumerable<Neuron> value, Processor.Processor proc)
        {
            throw new InvalidOperationException();
        }
    }
}