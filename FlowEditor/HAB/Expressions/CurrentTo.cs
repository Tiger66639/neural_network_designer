﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;

namespace JaStDev.HAB.Expressions
{
    /// <summary>
    ///     A variable that returns the neuron in the to part of the link being executed.
    /// </summary>
    [NeuronID((ulong)PredefinedNeurons.CurrentTo)]
    public class CurrentTo : SystemVariable
    {
        /// <summary>
        ///     Gets the value.
        /// </summary>
        /// <param name="proc">The proc.</param>
        /// <returns></returns>
        public override IEnumerable<Neuron> GetValue(Processor.Processor proc)
        {
            var iRes = new List<Neuron>();
            using (var iLinksOut = proc.NeuronToSolve.LinksOut)
                iRes.Add(iLinksOut.Items[proc.CurrentLink].To);
            return iRes;
        }

        /// <summary>
        ///     Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </returns>
        public override string ToString()
        {
            return "CurrentTo";
        }
    }
}