﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions.IndexOf
{
    /// <summary>
    /// Returns the index position of a neuron in an info list of a link.
    /// </summary>
    /// <remarks>
    /// arg:
    /// 1: from part of link
    /// 2: to part of link
    /// 3: meaning part of link
    /// 4: neuron to search for.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.IndexOfInfoInstruction)]
    public class IndexOfInfoInstruction : SingleResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.IndexOfInfoInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.IndexOfInfoInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 4; }
        }

        /// <summary>
        /// Gets the actual value.
        /// </summary>
        /// <param name="processor">The processor to use.</param>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The result of the instruction.</returns>
        protected override Neuron InternalGetValue(HAB.Processor.Processor processor, IList<Neuron> list)
        {
            IntNeuron iNeuron = null;
            if (list != null && list.Count >= 4)
            {
                Link iLink = GetLink(list);
                if (iLink != null)
                {
                    if (list[3] != null)
                    {
                        iNeuron = new IntNeuron();
                        Brain.Brain.Current.MakeTemp(iNeuron);
                        using (LinkInfoAccessor iList = iLink.Info)
                            iNeuron.Value = iList.IndexOf(list[3].ID);
                    }
                    else
                        Log.Log.LogError("IndexOfInfoInstruction.InternalGetValue", string.Format("neuron to search is null"));
                }
            }
            else
                Log.Log.LogError("IndexOfInfoInstruction.InternalGetValue", "No arguments specified");
            if (iNeuron == null)
                iNeuron = new IntNeuron() { Value = -1 };                                              //we always return a value
            Brain.Brain.Current.MakeTemp(iNeuron);                                                          //we always return temp values.
            return iNeuron;
        }

        /// <summary>
        /// Gets the link including proper checking and error handling.
        /// </summary>
        /// <param name="from">From part of link</param>
        /// <param name="to">To part of link</param>
        /// <param name="meaning">The meaning of the link</param>
        /// <param name="name">The text to use for error reporting.</param>
        /// <returns></returns>
        private Link GetLink(IList<Neuron> list)
        {
            if (list[0] == null)
            {
                Log.Log.LogError("IndexOfInfoInstruction.GetLink", string.Format("From part of is null"));
                return null;
            }
            if (list[1] == null)
            {
                Log.Log.LogError("IndexOfInfoInstruction.GetLink", string.Format("To part is null"));
                return null;
            }
            if (list[2] == null)
            {
                Log.Log.LogError("IndexOfInfoInstruction.GetLink", string.Format("Meaning part is null"));
                return null;
            }
            Link iRes = Link.Find(list[0], list[1], list[2]);
            if (iRes == null)
                Log.Log.LogError("IndexOfInfoInstruction.GetLink", string.Format("Failed to find link from={0}, to={1}, meanin={2}.", list[0], list[1], list[2]));
            return iRes;
        }
    }
}