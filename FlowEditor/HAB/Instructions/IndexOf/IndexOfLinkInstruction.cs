﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions.IndexOf
{
    /// <summary>
    /// Returns the index position of a link in one of the sides of the link
    /// </summary>
    /// <remarks>
    /// arg:
    /// 1: from part of link
    /// 2: to part of link
    /// 3: meaning part of link
    /// 4: either from or to part, to indicate at which side the index should be returned.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.IndexOfLinkInstruction)]
    public class IndexOfLinkInstruction : SingleResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.IndexOfLinkInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.IndexOfLinkInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 4; }
        }

        /// <summary>
        /// Gets the actual value.
        /// </summary>
        /// <param name="processor">The processor to use.</param>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The result of the instruction.</returns>
        protected override Neuron InternalGetValue(HAB.Processor.Processor processor, IList<Neuron> list)
        {
            IntNeuron iNeuron = new IntNeuron() { Value = -1 };                                       //we always return a value
            Brain.Brain.Current.MakeTemp(iNeuron);                                                          //we always return temp values.
            if (list != null && list.Count >= 4)
                iNeuron.Value = GetIndexOfLink(list);
            else
                Log.Log.LogError("IndexOfLinkInstruction.InternalGetValue", "No arguments specified");
            return iNeuron;
        }

        private int GetIndexOfLink(IList<Neuron> list)
        {
            if (list[0] == null)
            {
                Log.Log.LogError("IndexOfLinkInstruction.GetLink", string.Format("From part of is null"));
                return -1;
            }
            if (list[1] == null)
            {
                Log.Log.LogError("IndexOfLinkInstruction.GetLink", string.Format("To part is null"));
                return -1;
            }
            if (list[2] == null)
            {
                Log.Log.LogError("IndexOfLinkInstruction.GetLink", string.Format("Meaning part is null"));
                return -1;
            }
            if (list[3] == null)
            {
                Log.Log.LogError("IndexOfLinkInstruction.GetLink", string.Format("Meaning part is null"));
                return -1;
            }
            return Link.FindIndex(list[0], list[1], list[2], list[3]);
        }
    }
}