﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions.IndexOf
{
    /// <summary>
    /// Returns the index number of the second argument into the list of children of the first argument,which must be a
    /// <see cref="NeuronCluster"/>
    /// </summary>
    /// <remarks>
    /// arguments:
    /// - a neuron cluster
    /// - a neuron to search for.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.IndexOfChildInstruction)]
    public class IndexOfChildInstruction : SingleResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.IndexOfChildInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.IndexOfChildInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 2; }
        }

        /// <summary>
        /// Gets the actual value.
        /// </summary>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The count of the list.</returns>
        protected override Neuron InternalGetValue(HAB.Processor.Processor processor, IList<Neuron> list)
        {
            IntNeuron iNeuron = null;
            if (list != null && list.Count >= 2)
            {
                NeuronCluster iCluster = list[0] as NeuronCluster;
                Neuron iToSearch = list[1];
                if (iCluster != null)
                    if (iToSearch != null)
                    {
                        using (ChildrenAccessor iList = iCluster.Children)
                            iNeuron = new IntNeuron() { Value = iList.IndexOf(iToSearch.ID) };
                    }
                    else
                        Log.Log.LogError("IndexOfInstruction.InternalGetValue", "Invalid second argument, Neuron expected, found null.");
                else
                    Log.Log.LogError("IndexOfInstruction.InternalGetValue", "Invalid first argument, NeuronCluster expected, found null.");
            }
            else
                Log.Log.LogError("IndexOfInstruction.InternalGetValue", "Invalid nr of arguments specified");
            if (iNeuron == null)
                iNeuron = new IntNeuron() { Value = -1 };                                              //we always return a value
            Brain.Brain.Current.MakeTemp(iNeuron);                                                          //we always return temp values.
            return iNeuron;
        }
    }
}