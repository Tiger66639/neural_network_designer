﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions.Neurons
{
    /// <summary>
    /// Returns a duplicate copy (but with unique id) of the item passed in as param.
    /// </summary>
    /// <remarks>
    /// Only 1 argument allowed, the item that needs to be copied.
    /// returns a duplicate of the item.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.DuplicateInstruction)]
    public class DuplicateInstruction : SingleResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.DuplicateInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.DuplicateInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Gets the actual value.
        /// </summary>
        /// <param name="processor">The processor to use.</param>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The result of the instruction: a duplicate of the item or <see cref="PredefinedNeurons.Empty"/>
        /// if there is no valid result.</returns>
        protected override Neuron InternalGetValue(HAB.Processor.Processor processor, IList<Neuron> list)
        {
            if (list.Count >= 1 && list[0] != null)
            {
                Neuron iOld = list[0];
                Neuron iRes = iOld.Duplicate();
                return list[0].Duplicate();
            }
            else
                Log.Log.LogError("DuplicateInstruction.InternalGetValue", "Invalid nr of arguments specified");
            return Brain.Brain.Current[(ulong)PredefinedNeurons.Empty];
        }

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 1; }
        }
    }
}