﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;
using System.Diagnostics;

namespace JaStDev.HAB.Instructions.Add
{
    /// <summary>
    ///     Creates a link between 3 neurons.
    /// </summary>
    /// <remarks>
    ///     Arg 1 = from neuron
    ///     Arg 2 = to neuron
    ///     Arg 3 = meaning neuron
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.AddLinkInstruction)]
    public class AddLinkInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        ///     Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.AddLinkInstruction" />.</value>
        public override Neuron TypeOfNeuron
        {
            get { return Brain.Brain.Current[(ulong)PredefinedNeurons.AddLinkInstruction]; }
        }

        #endregion TypeOfNeuron

        /// <summary>
        ///     Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        ///     A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 3; }
        }

        /// <summary>
        ///     Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">
        ///     The arguments that the instruction requires to be properly executed. These are also
        ///     <see cref="Neuron" />s.
        /// </param>
        /// <remarks>
        ///     Instructions should never work directly on the data other than for searching.  Instead, they should go
        ///     through the methods of the <see cref="Processor" /> that is passed along as an argument.  This is important
        ///     cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            Debug.Assert(processor != null);
            Debug.Assert(args != null);

            if (args.Count >= 3)
            {
                if (args[0] == null)
                {
                    Log.Log.LogError("AddLinkInstruction.Execute", "From part is null (first arg).");
                    return;
                }
                if (args[1] == null)
                {
                    Log.Log.LogError("AddLinkInstruction.Execute", "To part is null (second arg).");
                    return;
                }
                if (args[2] == null)
                {
                    Log.Log.LogError("AddLinkInstruction.Execute", "meaning is null (third arg).");
                    return;
                }
                var iNew = new Link(args[1], args[0], args[2]);
            }
            else
                Log.Log.LogError("AddLinkInstruction.Execute", "Invalid nr of arguments specified");
        }
    }
}