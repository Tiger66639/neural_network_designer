﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions.Add
{
    /// <summary>
    /// Searches for the link between the first 2 neurons in the arg list and adds
    /// all the following arguments to the info section of the link, if found.
    /// </summary>
    /// <remarks>
    /// <para>
    /// This is a special implementation of the <see cref="AddInstruction"/>.  It takes
    /// less arguments.
    /// </para>
    /// arguments:
    /// 1: from part of link
    /// 2: to part of link
    /// 3: meaning part of link.
    /// 4: info to set.
    /// If the link is not found, nothing is done.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.AddInfoInstruction)]
    public class AddInfoInstruction : Instruction
    {
        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 4; }
        }

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.AddInfoInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.AddInfoInstruction];
            }
        }

        #endregion TypeOfNeuron

        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            if (args.Count >= 4)
            {
                if (args[0] == null)
                {
                    Log.Log.LogError("AddInfoInstruction.Execute", "From part is null (first arg).");
                    return;
                }
                if (args[1] == null)
                {
                    Log.Log.LogError("AddInfoInstruction.Execute", "To part is null (second arg).");
                    return;
                }
                if (args[2] == null)
                {
                    Log.Log.LogError("AddInfoInstruction.Execute", "Meaning pat is null (third arg).");
                    return;
                }
                if (args[3] == null)
                {
                    Log.Log.LogError("AddInfoInstruction.Execute", "Item to add is null  (fourth arg).");
                    return;
                }
                Link iLink = Link.Find(args[1], args[0], args[2]);
                if (iLink != null)
                    using (LinkInfoAccessor iList = iLink.Info)
                        iList.Add(args[3].ID);
                else
                    Log.Log.LogError("AddInfoInstruction.Execute", string.Format("Could not find link from={0}, to={1}, meaning={2}.  Failed to set info={3}", args[0], args[1], args[2], args[3]));
            }
            else
                Log.Log.LogError("AddInfoInstruction.Execute", "Invalid nr of arguments specified");
        }
    }
}