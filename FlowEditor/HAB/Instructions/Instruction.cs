﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions
{
    /// <summary>
    ///     A base class for types that represent a <see cref="Neuron" /> which the <see cref="Processor" /> interprets as
    ///     basic instruction blocks that can be executed and which don't return a result.
    /// </summary>
    /// <remarks>
    ///     Instructions can work directly on the data like searching, creating neurons or link.  There aren't any
    ///     methods provided by the <see cref="Processor" /> to do this.  This is no problem even not for a sub processor,
    ///     fo which changes might need to be discarded. All the brain data can buffer the changes in the sub processors
    ///     so they are only applied when the sub processor becomes main again.
    ///     <para>
    ///         Retrieving neurons from the <see cref="Brain" /> is also save, even in sub processors.  This is because
    ///         the brain checks the current processor.
    ///     </para>
    /// </remarks>
    public abstract class Instruction : Neuron
    {
        /// <summary>
        ///     Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <remarks>
        ///     A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public abstract int ArgCount { get; }

        /// <summary>
        ///     Performs the tasks on the specified processor.
        /// </summary>
        /// <remarks>
        ///     Instructions should never work directly on the data other than for searching.  Instead, they should go
        ///     through the methods of the <see cref="Processor" /> that is passed along as an argument.  This is important
        ///     cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">
        ///     The arguments that the instruction requires to be properly executed. These are also
        ///     <see cref="Neuron" />s.
        /// </param>
        public abstract void Execute(HAB.Processor.Processor processor, IList<Neuron> args);

        /// <summary>
        ///     Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </returns>
        public override string ToString()
        {
            var iRes = GetType().Name;
            return iRes.Replace("Instruction", "");
        }
    }
}