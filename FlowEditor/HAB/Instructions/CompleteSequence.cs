﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions
{
    /// <summary>
    /// Gets a sequence of numbers and completes them.
    /// </summary>
    /// <remarks>
    /// 1: first number, can be double or int neuron, determins which type the 2 other numbers must be.
    /// 2: second number, is made as same type as 1
    /// 3: third number, is made as same type as 1
    /// 4: nr of numbers that need to be generated.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.CompleteSequenceInstruction)]
    public class CompleteSequence : MultiResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.CompleteSequenceInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.CompleteSequenceInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 4; }
        }

        /// <summary>
        /// performs the task and returns it's result.
        /// </summary>
        /// <param name="processor"></param>
        /// <param name="list">The list of arguments</param>
        /// <returns>The list of results</returns>
        /// <remarks>
        /// Note: when When a result instruction is executed, ( so <see cref="ResultInstruction.Execute"/> is
        /// called instead of <see cref="ResultInstruction.GetValues"/>, the result value(s) are pushed on the
        /// execution stack.
        /// </remarks>
        public override IEnumerable<Neuron> GetValues(HAB.Processor.Processor processor, IList<Neuron> list)
        {
            List<Neuron> iRes = null;
            if (list != null && list.Count >= 4)
            {
                int iGenCount;
                if (list[3] is IntNeuron)
                    iGenCount = ((IntNeuron)list[3]).Value;
                else
                {
                    Log.Log.LogError("CompleteSequence.GetValues", "IntNeuron expected to indicate the nr of values to generate(fourth arg)");
                    return null;
                }
                if (list[0] is IntNeuron)
                    iRes = CompleteInts(list, iGenCount);
                else if (list[0] is DoubleNeuron)
                    iRes = CompleteDoubles(list, iGenCount);
                else
                    Log.Log.LogError("CompleteSequence.GetValues", string.Format("Invalid first argument, IntNeuron or DoubleNeuron expected, found {0}.", list[0]));
            }
            else
                Log.Log.LogError("CompleteSequence.GetValues", "No arguments specified");
            if (iRes == null)
                iRes = new List<Neuron>();
            return iRes;
        }

        private List<Neuron> CompleteDoubles(IList<Neuron> list, int genCount)
        {
            double[] iBase = new double[3];

            for (int i = 0; i < 3; i++)
            {
                if (list[i] is IntNeuron)
                    iBase[i] = (double)((IntNeuron)list[i]).Value;
                else if (list[i] is DoubleNeuron)
                    iBase[i] = ((DoubleNeuron)list[i]).Value;
                else
                {
                    Log.Log.LogError("CompleteSequence.CompleteInts", "No arguments specified");
                    return null;
                }
            }
            /*
             * here's an example calculation:
             * x        |   p(x) = 2x2 − 3x + 2 |	diff1(x) = ( p(x-1) - p(x) ) 	|  diff2(x) = ( diff1(x-1) - diff1(x) )
             * 0.00     | 	         2.00
             * 0.10     | 	1.72                 |	0.28
             * 0.20 	   |  1.48 	               |  0.24 	                        |  0.04
             * 0.30 	   |  1.28 	               |  0.20 	                        |  0.04
             * 0.40 	   |  1.12 	               |  0.16 	                        |  0.04
             */
            double[] iDiffs = new double[2];
            iDiffs[0] = iBase[0] - iBase[1];
            iDiffs[1] = iBase[1] - iBase[2];
            double iDifDif = iDiffs[0] - iDiffs[1];
            List<Neuron> iRes = new List<Neuron>();
            double iDiffCum = iDiffs[1];
            double iBaseCum = iBase[2];
            while (genCount > 0)
            {
                iDiffCum -= iDifDif;
                iBaseCum -= iDiffCum;
                DoubleNeuron iNew = new DoubleNeuron() { Value = iBaseCum };
                iRes.Add(iNew);
                genCount--;
            }
            return iRes;
        }

        /// <summary>
        /// Completes the sequence using int.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <param name="genCount">The nr of items to generate.</param>
        /// <returns></returns>
        private List<Neuron> CompleteInts(IList<Neuron> list, int genCount)
        {
            int[] iBase = new int[3];

            for (int i = 0; i < 3; i++)
            {
                if (list[i] is IntNeuron)
                    iBase[i] = ((IntNeuron)list[i]).Value;
                else if (list[i] is DoubleNeuron)
                    iBase[i] = (int)((DoubleNeuron)list[i]).Value;
                else
                {
                    Log.Log.LogError("CompleteSequence.CompleteInts", "No arguments specified");
                    return null;
                }
            }
            /*
             * here's an example calculation:
             * x        |   p(x) = 2x2 − 3x + 2 |	diff1(x) = ( p(x-1) - p(x) ) 	|  diff2(x) = ( diff1(x-1) - diff1(x) )
             * 0.00     | 	         2.00
             * 0.10     | 	1.72                 |	0.28
             * 0.20 	   |  1.48 	               |  0.24 	                        |  0.04
             * 0.30 	   |  1.28 	               |  0.20 	                        |  0.04
             * 0.40 	   |  1.12 	               |  0.16 	                        |  0.04
             */
            int[] iDiffs = new int[2];
            iDiffs[0] = iBase[0] - iBase[1];
            iDiffs[1] = iBase[1] - iBase[2];
            int iDifDif = iDiffs[0] - iDiffs[1];
            List<Neuron> iRes = new List<Neuron>();
            int iDiffCum = iDiffs[1];
            int iBaseCum = iBase[2];
            while (genCount > 0)
            {
                iDiffCum -= iDifDif;
                iBaseCum -= iDiffCum;
                IntNeuron iNew = new IntNeuron() { Value = iBaseCum };
                iRes.Add(iNew);
                genCount--;
            }
            return iRes;
        }
    }
}