﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using System.Collections.Generic;
using System.Linq;

namespace JaStDev.HAB.Instructions.Clear
{
    /// <summary>
    /// Removes all the incomming links from a neuron.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// 1. The neuron for wich to remove all the incomming links.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.ClearLinksInInstruction)]
    public class ClearLinksInInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ClearLinksInInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.ClearLinksInInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 1; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            if (args != null && args.Count >= 1)
            {
                Neuron iNeuron = args[0];
                if (iNeuron != null)
                {
                    Link[] iTemp;
                    using (LinksAccessor iLinksIn = LinksIn)                                            //need to make it thread safe, if we don't, the list might get modified during the loop.
                        iTemp = iLinksIn.Items.ToArray();                                                   //need to make a copy of the list, otherwise we get an error in the loop that we are trying to change the list within the loop due to the delete.
                    foreach (Link i in iTemp)
                        i.Destroy();
                }
                else
                    Log.Log.LogError("ClearLinksInInstruction.Execute", string.Format("Can't clear Links in, Invalid Neuron specified: {0}.", args[0]));
            }
            else
                Log.Log.LogError("ClearLinksInInstruction.Execute", "Invalid nr of arguments specified");
        }
    }
}