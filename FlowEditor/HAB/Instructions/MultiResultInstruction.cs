﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions
{
    /// <summary>
    /// A result instruction that is able to return multiple results.
    /// </summary>
    public abstract class MultiResultInstruction : ResultInstruction
    {
        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            IEnumerable<Neuron> iRes = GetValues(processor, args);
            foreach (Neuron i in iRes)
                processor.Push(i);
        }
    }
}