﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions.Convert
{
    /// <summary>
    /// Converts an int neuron into a double.
    /// It is possible to convert multiple items at once.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// - an list of int neurons.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.IToDInstruction)]
    public class IToDInstruction : MultiResultInstruction
    {
        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 1; }
        }

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.IToDInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.IToDInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// performs the task and returns it's result.
        /// </summary>
        /// <param name="processor"></param>
        /// <param name="list">The list of arguments</param>
        /// <returns>The list of results</returns>
        /// <remarks>
        /// Note: when When a result instruction is executed, ( so <see cref="ResultInstruction.Execute"/> is
        /// called instead of <see cref="ResultInstruction.GetValues"/>, the result value(s) are pushed on the
        /// execution stack.
        /// </remarks>
        public override IEnumerable<Neuron> GetValues(HAB.Processor.Processor processor, IList<Neuron> list)
        {
            if (list != null && list.Count >= 1)
            {
                List<Neuron> iList = new List<Neuron>();
                foreach (Neuron iNeuron in list)
                {
                    IntNeuron iPar = iNeuron as IntNeuron;
                    if (iPar != null)
                    {
                        DoubleNeuron iRes = new DoubleNeuron();
                        iRes.Value = (double)iPar.Value;
                        Brain.Brain.Current.Add(iRes);
                        iList.Add(iRes);
                    }
                    else
                        Log.Log.LogError("IToDInstruction.GetValues", "Argument must be int neurons!");
                }
                return iList;
            }
            else
                Log.Log.LogError("IToDInstruction.GetValues", "Invalid nr of arguments specified!");
            return new List<Neuron>();                                                 //when we get here, there was something wrong.  Still need to return something, so return empty.
        }
    }
}