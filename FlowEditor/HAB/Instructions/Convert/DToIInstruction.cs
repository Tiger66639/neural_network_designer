﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions.Convert
{
    /// <summary>
    /// Converts a double neuron into an int neuron.
    /// It is possible to convert multiple items at once.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// - a list of double neurons.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.DToIInstruction)]
    public class DToIInstruction : MultiResultInstruction
    {
        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 1; }
        }

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.DToIInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.DToIInstruction];
            }
        }

        #endregion TypeOfNeuron

        public override IEnumerable<Neuron> GetValues(HAB.Processor.Processor processor, IList<Neuron> list)
        {
            if (list != null && list.Count >= 1)
            {
                List<Neuron> iList = new List<Neuron>();
                foreach (Neuron iNeuron in list)
                {
                    DoubleNeuron iPar = iNeuron as DoubleNeuron;
                    if (iPar != null)
                    {
                        IntNeuron iRes = new IntNeuron();
                        iRes.Value = (int)iPar.Value;
                        Brain.Brain.Current.Add(iRes);
                        iList.Add(iRes);
                    }
                    else
                        Log.Log.LogError("DToIInstruction.GetValues", "Argument must be double neurons!");
                }
                return iList;
            }
            else
                Log.Log.LogError("DToIInstruction.GetValues", "Invalid nr of arguments specified!");
            return new List<Neuron>();                                                 //when we get here, there was something wrong.  Still need to return something, so return empty.
        }
    }
}