﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using System.Collections.Generic;
using System.Text;

namespace JaStDev.HAB.Instructions.Convert
{
    /// <summary>
    /// Converts a cluster containing string neurons (where each string neuron represents a character), into a single string neuron.
    /// It is possible to convert multiple items at once.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// - a list of clusters containing only string neurons neurons, usually with 1 char.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.CcToSInstruction)]
    public class CcToSInstruction : MultiResultInstruction
    {
        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 1; }
        }

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.CcToSInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.CcToSInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// performs the task and returns it's result.
        /// </summary>
        /// <param name="processor"></param>
        /// <param name="list">The list of arguments</param>
        /// <returns>The list of results</returns>
        /// <remarks>
        /// Note: when When a result instruction is executed, ( so <see cref="ResultInstruction.Execute"/> is
        /// called instead of <see cref="ResultInstruction.GetValues"/>, the result value(s) are pushed on the
        /// execution stack.
        /// </remarks>
        public override IEnumerable<Neuron> GetValues(HAB.Processor.Processor processor, IList<Neuron> list)
        {
            if (list != null && list.Count >= 1)
            {
                List<Neuron> iList = new List<Neuron>();
                foreach (Neuron iNeuron in list)
                {
                    NeuronCluster iCluster = iNeuron as NeuronCluster;
                    if (iCluster != null)
                    {
                        StringBuilder iBuilder = new StringBuilder();
                        using (ChildrenAccessor iClusterList = iCluster.Children)
                        {
                            foreach (ulong i in iClusterList)
                            {
                                TextNeuron iInt = Brain.Brain.Current[i] as TextNeuron;
                                if (iInt != null)
                                    iBuilder.Append(iInt.Text);
                                else
                                {
                                    Neuron iReal = Brain.Brain.Current[i];
                                    Log.Log.LogError("CcToSInstruction.GetValues", string.Format("The cluster should only contain int neurons, found a {0} at pos '{1}': {2}", iReal.GetType().ToString(), iClusterList.IndexOf(i), iReal));
                                    return new List<Neuron>();                                         //we have an invalid state, so exit.
                                }
                            }
                        }
                        TextNeuron iRes = new TextNeuron(iBuilder.ToString());
                        Brain.Brain.Current.Add(iRes);
                        iList.Add(iRes);
                    }
                    else
                        Log.Log.LogError("CcToSInstruction.GetValues", "Argument must be clusters!");
                }
                return iList;
            }
            else
                Log.Log.LogError("CcToSInstruction.GetValues", "Invalid nr of arguments specified!");
            return new List<Neuron>();                                                 //when we get here, there was something wrong.  Still need to return something, so return empty.
        }
    }
}