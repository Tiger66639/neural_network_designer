﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions
{
    /// <summary>
    /// Returns the number of items in a list.
    /// </summary>
    /// <remarks>
    /// arguments:
    /// - The list of arguments is counted.
    /// The neuron that is returned is only registered as a temp neuron, so it is not permanent, but if it is somehow
    /// stored permanetly, it's id is updated.  This allows for temporary values during code execution.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.CountInstruction)]
    public class CountInstruction : SingleResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.CountInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.CountInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return -1; }
        }

        /// <summary>
        /// Gets the actual value.
        /// </summary>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The count of the list.</returns>
        protected override Neuron InternalGetValue(HAB.Processor.Processor processor, IList<Neuron> list)
        {
            IntNeuron iNeuron;
            if (list != null)
                iNeuron = new IntNeuron() { Value = list.Count };
            else
                iNeuron = new IntNeuron() { Value = 0 };
            Brain.Brain.Current.MakeTemp(iNeuron);                                                          //we always return temp values: this is important, otherwise we can't use temporary values in the code.
            return iNeuron;
        }
    }
}