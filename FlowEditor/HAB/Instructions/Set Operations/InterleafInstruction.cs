﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions.Set_Operations
{
    /// <summary>
    /// Interleafs the content of arg 1 (cluster) with the content of arg2 (cluster)
    /// </summary>
    /// <remarks>
    /// If cluster1 = aaaa
    /// and cluster2 = b
    /// than result = abababa
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.InterleafInstruction)]
    public class InterleafInstruction : MultiResultInstruction
    {
        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 2; }
        }

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.InterleafInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.InterleafInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// performs the task and returns it's result.
        /// </summary>
        /// <param name="processor"></param>
        /// <param name="list">The list of arguments</param>
        /// <returns>The list of results</returns>
        /// <remarks>
        /// Note: when When a result instruction is executed, ( so <see cref="ResultInstruction.Execute"/> is
        /// called instead of <see cref="ResultInstruction.GetValues"/>, the result value(s) are pushed on the
        /// execution stack.
        /// </remarks>
        public override IEnumerable<Neuron> GetValues(HAB.Processor.Processor processor, IList<Neuron> list)
        {
            if (list != null && list.Count >= 2)
            {
                NeuronCluster iFirst = list[0] as NeuronCluster;
                if (iFirst != null)
                {
                    NeuronCluster iSecond = list[1] as NeuronCluster;
                    if (iSecond != null)
                    {
                        List<Neuron> iInterleaf;
                        using (ChildrenAccessor iList = iSecond.Children)
                            iInterleaf = iList.ConvertTo<Neuron>();
                        List<Neuron> iRes = new List<Neuron>();
                        using (ChildrenAccessor iList = iFirst.Children)
                        {
                            for (int i = 0; i < iList.Count - 1; i++)
                            {
                                iRes.Add(Brain.Brain.Current[iList[i]]);
                                iRes.AddRange(iInterleaf);
                            }
                            iRes.Add(Brain.Brain.Current[iList[iList.Count - 1]]);
                        }
                        return iRes;
                    }
                    else
                        Log.Log.LogError("Interleaf.GetValues", "Second argument should be a NeuronCluster!");
                }
                else
                    Log.Log.LogError("Interleaf.GetValues", "First argument should be a NeuronCluster!");
            }
            else
                Log.Log.LogError("Interleaf.GetValues", "No arguments specified");
            return new List<Neuron>();                                                                            //when we get here, simply return an empty result.
        }
    }
}