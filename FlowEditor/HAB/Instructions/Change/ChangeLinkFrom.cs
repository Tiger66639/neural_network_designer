﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;
using System.Diagnostics;

namespace JaStDev.HAB.Instructions.Change
{
    /// <summary>
    /// Changes the from part of a link.
    /// </summary>
    /// <remarks>
    /// 1: From part
    /// 2: to part
    /// 3: meaning part
    /// 4: new From
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.ChangeLinkFrom)]
    public class ChangeLinkFrom : Instruction
    {
        public override int ArgCount
        {
            get { return 4; }
        }

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ChangeLinkFrom"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.ChangeLinkFrom];
            }
        }

        #endregion TypeOfNeuron

        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            Debug.Assert(processor != null);
            Debug.Assert(args != null);

            if (args.Count >= 4)
            {
                if (args[0] == null)
                {
                    Log.Log.LogError("ChangeLinkFrom.Execute", "From part is null (first arg).");
                    return;
                }
                if (args[1] == null)
                {
                    Log.Log.LogError("ChangeLinkFrom.Execute", "To part is null (second arg).");
                    return;
                }
                if (args[2] == null)
                {
                    Log.Log.LogError("ChangeLinkFrom.Execute", "meaning is null (third arg).");
                    return;
                }
                if (args[4] == null)
                {
                    Log.Log.LogError("ChangeLinkFrom.Execute", "New from is null (fourth arg).");
                    return;
                }
                Link iNew = Link.Find(args[1], args[0], args[2]);
                if (iNew != null)
                    iNew.From = args[4];
                else
                    Log.Log.LogError("ChangeLinkFrom.Execute", string.Format("Link not found: to={0}, from={1}, meaning={2}.", args[0], args[1], args[2]));
            }
            else
                Log.Log.LogError("ChangeLinkFrom.Execute", "Invalid nr of arguments specified");
        }
    }
}