﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;
using System.Diagnostics;

namespace JaStDev.HAB.Instructions.Change
{
    /// <summary>
    /// Changes a link so that it points to a new 'To' neuron.
    /// </summary>
    /// <remarks>
    /// 1: From part
    /// 2: to part
    /// 3: meaning part
    /// 4: new to
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.ChangeLinkTo)]
    public class ChangeLinkTo : Instruction
    {
        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 4; }
        }

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ChangeLinkTo"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.ChangeLinkTo];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            Debug.Assert(processor != null);
            Debug.Assert(args != null);

            if (args.Count >= 4)
            {
                if (args[0] == null)
                {
                    Log.Log.LogError("ChangeLinkTo.Execute", "From part is null (first arg).");
                    return;
                }
                if (args[1] == null)
                {
                    Log.Log.LogError("ChangeLinkTo.Execute", "To part is null (second arg).");
                    return;
                }
                if (args[2] == null)
                {
                    Log.Log.LogError("ChangeLinkTo.Execute", "meaning is null (third arg).");
                    return;
                }
                if (args[4] == null)
                {
                    Log.Log.LogError("ChangeLinkTo.Execute", "New to is null (fourth arg).");
                    return;
                }
                Link iNew = Link.Find(args[1], args[0], args[2]);
                if (iNew != null)
                    iNew.To = args[4];
                else
                    Log.Log.LogError("ChangeLinkTo.Execute", string.Format("Link not found: to={0}, from={1}, meaning={2}.", args[0], args[1], args[2]));
            }
            else
                Log.Log.LogError("ChangeLinkTo.Execute", "Invalid nr of arguments specified");
        }
    }
}