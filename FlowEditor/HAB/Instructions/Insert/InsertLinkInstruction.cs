﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;
using System.Diagnostics;

namespace JaStDev.HAB.Instructions.Insert
{
    /// <summary>
    /// Creates a new link between from and to and performs an insert when the link is added to from and/or to.
    /// </summary>
    /// <remarks>
    /// Args:
    /// 1: from part
    /// 2: from index (can be invalid, indicates an add)
    /// 3: to part
    /// 4: to index (can be invalid, indicates an add)
    /// 5: meaning part
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.InsertLinkInstruction)]
    public class InsertLinkInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.InsertLinkInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.InsertLinkInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 5; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            Debug.Assert(processor != null);
            Debug.Assert(args != null);

            if (args.Count >= 5)
            {
                int iFrom;
                int iTo;
                if (CheckArgs(args, out iFrom, out iTo) == true)
                {
                    Link.CreateArgs iArgs = new Link.CreateArgs() { To = args[2], From = args[0], Meaning = args[4], FromIndex = iFrom, ToIndex = iTo };
                    Link iNew = new Link(iArgs);
                }
            }
            else
                Log.Log.LogError("InsertLinkInstruction.Execute", "Invalid nr of arguments specified");
        }

        private bool CheckArgs(IList<Neuron> args, out int from, out int to)
        {
            from = -1;
            to = -1;
            if (args[0] == null)
            {
                Log.Log.LogError("InsertLinkInstruction.Execute", "From part is null (first arg).");
                return false;
            }
            IntNeuron iTemp = args[1] as IntNeuron;
            if (iTemp != null)
                from = iTemp.Value;
            if (args[2] == null)
            {
                Log.Log.LogError("InsertLinkInstruction.Execute", "To part is null (third arg).");
                return false;
            }
            iTemp = args[3] as IntNeuron;
            if (iTemp != null)
                to = iTemp.Value;
            if (args[4] == null)
            {
                Log.Log.LogError("InsertLinkInstruction.Execute", "meaning is null (fith arg).");
                return false;
            }
            return true;
        }
    }
}