﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;
using System.Text;

namespace JaStDev.HAB.Instructions
{
    /// <summary>
    /// An instruction that adds a warning to the log. The text to log is declared as a list of neurons which are transformed into text.
    /// </summary>
    /// <remarks>
    /// Arguments: unlimited list of neurons which are transformed into text  (with ToString), concatenated and added to the log as warning.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.WarningInstruction)]
    public class WarningInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.WarningInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.WarningInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return -1; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            StringBuilder iStr = new StringBuilder();
            foreach (Neuron i in args)
                iStr.Append(i.ToString());
            Log.Log.LogWarning("WarningInstruction.Execute", iStr.ToString());
        }
    }
}