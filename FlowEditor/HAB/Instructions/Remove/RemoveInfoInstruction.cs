﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions.Remove
{
    /// <summary>
    /// Removes an info item from a link.
    /// </summary>
    /// <remarks>
    /// Args:
    /// 1: from part
    /// 2: to part
    /// 3: meaning part
    /// 4: neuron to remove from list.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.RemoveInfoInstruction)]
    public class RemoveInfoInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.RemoveInfoInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.RemoveInfoInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 4; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            if (args.Count >= 4)
            {
                if (args[0] == null)
                {
                    Log.Log.LogError("RemoveInfoInstruction.Execute", "From part is null (first arg).");
                    return;
                }
                if (args[1] == null)
                {
                    Log.Log.LogError("RemoveInfoInstruction.Execute", "To part is null (second arg).");
                    return;
                }
                if (args[2] == null)
                {
                    Log.Log.LogError("RemoveInfoInstruction.Execute", "Meaning pat is null (third arg).");
                    return;
                }
                if (args[3] == null)
                {
                    Log.Log.LogError("RemoveInfoInstruction.Execute", "Item to remove is null  (fourth arg).");
                    return;
                }
                Link iLink = Link.Find(args[1], args[0], args[2]);
                if (iLink != null)
                    using (LinkInfoAccessor iList = iLink.Info)
                        iList.Remove(args[3].ID);
                else
                    Log.Log.LogError("RemoveInfoInstruction.Execute", string.Format("Could not find link from={0}, to={1}.  Failed to set info={2}", args[0], args[1], args[2]));
            }
            else
                Log.Log.LogError("RemoveInfoInstruction.Execute", "Invalid nr of arguments specified");
        }
    }
}