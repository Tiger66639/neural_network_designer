﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions.Remove
{
    /// <summary>
    /// Removes a neuron from a cluster.
    /// </summary>
    /// <remarks>
    /// Args:
    /// 1: The neuron cluster
    /// 2: the item to remove from the cluster.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.RemoveChildInstruction)]
    public class RemoveChildInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.RemoveChildInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.RemoveChildInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 2; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            if (args != null && args.Count >= 2)
            {
                if (args[1] == null)
                {
                    Log.Log.LogError("RemoveChildInstruction.Execute", "item to remove is null (second arg).");
                    return;
                }
                NeuronCluster iCluster = args[0] as NeuronCluster;
                if (iCluster != null)
                {
                    using (ChildrenAccessor iList = iCluster.ChildrenW)
                        iList.Remove(args[1]);
                }
                else
                {
                    Log.Log.LogError("RemoveChildInstruction.Execute", string.Format("Can't remove, Invalid cluster specified: {}.", args[0]));
                    return;
                }
            }
            else
                Log.Log.LogError("RemoveChildInstruction.Execute", "Invalid nr of arguments specified");
        }
    }
}