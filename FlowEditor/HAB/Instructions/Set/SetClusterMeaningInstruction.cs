﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions.Set
{
    /// <summary>
    /// Changes the meaning of a <see cref="NeuronCluster"/> to the new value.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// - 1: The NeuronCluster who's meaning needs to be changed.
    /// - 2: The new meaning of the cluster.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.SetClusterMeaningInstruction)]
    public class SetClusterMeaningInstruction : Instruction
    {
        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 2; }
        }

        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ConditionalPart"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.SetClusterMeaningInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            if (args != null && args.Count >= 2)
            {
                if (args[1] == null)
                {
                    Log.Log.LogError("SetClusterMeaningInstruction.Execute", "Meaning argument can't be null (second arg).");
                    return;
                }
                NeuronCluster iCluster = args[0] as NeuronCluster;
                if (iCluster != null)
                    iCluster.Meaning = args[1].ID;
                else
                {
                    Log.Log.LogError("SetClusterMeaningInstruction.Execute", string.Format("Can't change meaning, Invalid cluster specified: {0}.", args[0]));
                    return;
                }
            }
            else
                Log.Log.LogError("SetClusterMeaningInstruction.Execute", "Invalid nr of arguments specified");
        }
    }
}