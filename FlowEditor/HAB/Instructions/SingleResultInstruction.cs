﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions
{
    /// <summary>
    ///     A resultInstruction that only has 1 result.
    /// </summary>
    public abstract class SingleResultInstruction : ResultInstruction
    {
        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            var iRes = InternalGetValue(processor, args);
            processor.Push(iRes);
        }

        /// <summary>
        ///     performs the task and returns it's result.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         Note: when When a result instruction is executed, ( so <see cref="ResultInstruction.Execute" /> is
        ///         called instead of <see cref="ResultInstruction.GetValues" />, the result value(s) are pushed on the
        ///         execution stack.
        ///     </para>
        ///     This method has a default implementation that uses <see cref="ResultInstruction.InternalGetValue" />
        ///     to get the actual result.
        /// </remarks>
        /// <param name="iArgs"></param>
        /// <returns></returns>
        public override IEnumerable<Neuron> GetValues(HAB.Processor.Processor processor, IList<Neuron> iArgs)
        {
            var iRes = new List<Neuron>();
            var iNeuron = InternalGetValue(processor, iArgs);
            iRes.Add(iNeuron);
            return iRes;
        }

        /// <summary>
        ///     Gets the actual value.
        /// </summary>
        /// <param name="processor">The processor to use.</param>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The result of the instruction.</returns>
        protected abstract Neuron InternalGetValue(HAB.Processor.Processor processor, IList<Neuron> list);
    }
}