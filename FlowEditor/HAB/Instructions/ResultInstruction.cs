﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions
{
    /// <summary>
    ///
    /// </summary>
    public abstract class ResultInstruction : Instruction
    {
        /// <summary>
        /// performs the task and returns it's result.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Note: when When a result instruction is executed, ( so <see cref="ResultInstruction.Execute"/> is
        /// called instead of <see cref="ResultInstruction.GetValues"/>, the result value(s) are pushed on the
        /// execution stack.
        /// </para>
        /// </remarks>
        /// <param name="list">The list of arguments</param>
        /// <returns>The list of results</returns>
        public abstract IEnumerable<Neuron> GetValues(HAB.Processor.Processor processor, IList<Neuron> list);
    }
}