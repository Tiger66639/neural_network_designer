﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions
{
    /// <summary>
    /// Copies the contents of a list from one neuron to another neuron.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// 1: the neuron cluster to copy to
    /// 2: the index at which to start copying
    /// 3: the neuron cluster to copy from
    /// 4: start index copy from when not an int -> 0 is used (so Predefined.Empty can be used)
    /// 5: end index copy from when not an int -> Childrend.count is used (so Predefined.Empty can be used)
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.CopyChildrenInstruction)]
    public class CopyChildrenInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.CopyChildrenInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.CopyChildrenInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 5; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            if (args != null && args.Count >= 5)
            {
                int iStartIndex;
                int iEndIndex;
                int iInsertAt = -1;                                                                    //stores where the insert should start at the target.

                NeuronCluster iTo = args[0] as NeuronCluster;
                if (iTo != null)
                {
                    IntNeuron iTempInt = args[1] as IntNeuron;
                    if (iTempInt != null)
                        iInsertAt = iTempInt.Value;
                    NeuronCluster iFrom = args[2] as NeuronCluster;
                    if (iFrom != null)
                    {
                        using (ChildrenAccessor iFromList = iFrom.Children)
                        {
                            if (GetRange(args, out iStartIndex, out iEndIndex, iFromList.Count) == true)
                            {
                                if (iFromList.Count <= iEndIndex)
                                    CopyChildren(iFromList, iStartIndex, iEndIndex, iTo, iInsertAt);
                                else
                                    Log.Log.LogError("CopyChildrenInstruction.Execute", string.Format("end pos '{0}' is bigger than list.", iEndIndex));
                            }
                        }
                    }
                    else
                        Log.Log.LogError("CopyChildrenInstruction.CheckFirstArgsForClusters", "Invalid 'Copy from' argument (second arg)");
                }
                else
                    Log.Log.LogError("CopyChildrenInstruction.CheckFirstArgsForClusters", "Invalid 'Copy to' argument (first arg)");
            }
            else
                Log.Log.LogError("CopyChildrenInstruction.Execute", "Invalid nr of arguments specified");
        }

        /// <summary>
        /// Performs the actual copy operation.
        /// </summary>
        /// <remarks>
        /// virtual so that descendents can do other things, like deleting the items.
        /// </remarks>
        /// <param name="from">From.</param>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        /// <param name="to">To.</param>
        /// <param name="insertAt">The insert at.</param>
        protected virtual void CopyChildren(ChildrenAccessor from, int start, int end, NeuronCluster to, int insertAt)
        {
            using (ChildrenAccessor iTo = to.Children)
            {
                if (insertAt == -1)
                {
                    while (start < end)
                    {
                        iTo.Add(from[start]);
                        start++;
                    }
                }
                else
                {
                    while (start < end)
                    {
                        iTo.Insert(insertAt++, from[start]);
                        start++;
                    }
                }
            }
        }

        /// <summary>
        /// Checks the args and returns the start and end index to use.
        /// </summary>
        /// <param name="args">The args passed along with execute.</param>
        /// <param name="start">The start index of the list at which copying should start.</param>
        /// <param name="end">The end index before wich copying should stop (so if this is 5, up untill 4 will be copied).</param>
        /// <returns>True if everything is ok.</returns>s
        private bool GetRange(IList<Neuron> args, out int start, out int end, int max)
        {
            start = 0;
            end = max;
            IntNeuron iTemp = args[3] as IntNeuron;
            if (iTemp != null)
                start = iTemp.Value;
            iTemp = args[4] as IntNeuron;
            if (iTemp != null)
                end = iTemp.Value;
            if (end < start)
                Log.Log.LogWarning("CopyChildrenInstruction.CheckArgs", string.Format("end pos '{0}' is smaller than start '{1}'.", end, start));
            return true;
        }
    }
}