﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;

namespace JaStDev.HAB.Instructions
{
    /// <summary>
    /// Same as <see cref="CopyChildrenInstruction"/> but removes the children in the from list after the copy.
    /// </summary>
    [NeuronID((ulong)PredefinedNeurons.MoveChildrenInstruction)]
    public class MoveInstruction : CopyChildrenInstruction
    {
        /// <summary>
        /// Performs the actual copy operation.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        /// <param name="to">To.</param>
        /// <param name="insertAt">The insert at.</param>
        /// <remarks>
        /// Removes the child after each copy.
        /// </remarks>
        protected override void CopyChildren(ChildrenAccessor from, int start, int end, NeuronCluster to, int insertAt)
        {
            using (ChildrenAccessor iTo = to.ChildrenW)
            {
                if (insertAt == -1)
                {
                    while (start < end)
                    {
                        iTo.Add(from[start]);
                        from.Mode = AccessorMode.Write;
                        from.RemoveAt(start);
                        from.Mode = AccessorMode.Read;
                        end++;
                    }
                }
                else
                {
                    while (start < end)
                    {
                        iTo.Insert(insertAt++, from[start]);
                        from.Mode = AccessorMode.Write;
                        from.RemoveAt(start);
                        from.Mode = AccessorMode.Read;
                        end++;
                    }
                }
            }
        }
    }
}