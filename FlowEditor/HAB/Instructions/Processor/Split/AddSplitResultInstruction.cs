﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Processor;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions.Processor.Split
{
    /// <summary>
    /// Adds a neuron to the <see cref="Processor.SplitValues"/> list.
    /// </summary>
    /// <remarks>
    /// When the neuron is already in the list, it is not stored 2 times, so each neuron is always stored uniquely.
    /// Arguments: a list of neurons.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.AddSplitResultInstruction)]
    public class AddSplitResultInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.AddSplitResultInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.AddSplitResultInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return -1; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            if (args != null)
            {
                SplitResultsDict iDict = processor.SplitValues;
                foreach (Neuron i in args)
                {
                    if (iDict.ContainsKey(i.ID) == false)                                            //only add if not in dict.
                        iDict.Add(i.ID, 0);
                }
            }
            else
                Log.Log.LogError("AddSplitResultInstruction.Execute", "No arguments specified");
        }
    }
}