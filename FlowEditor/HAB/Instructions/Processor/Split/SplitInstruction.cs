﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Expressions;
using JaStDev.HAB.Processor;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions.Processor.Split
{
    /// <summary>
    /// Splits up the processor into identical sub processors which continue processing from the same point.
    /// For each argument after the third one, a new sub processor is created.
    /// </summary>
    /// <remarks>
    /// <para>
    /// A processor that is already split can't be split again and will log an error.
    /// </para>
    /// Arguments:
    /// 1: the NeuronCluster that should be called (executed) when the split is ready.
    /// 2: the NeuronCluster that will contain all the results.
    /// 3: the var that will contain the result of the split (must be declared as byref, or as the content of another var).
    /// 4: a list of neurons on which to perform the split (for each item, a new sub processor will be created
    ///    and in that processor, only
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.SplitInstruction)]
    public class SplitInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ConditionalPart"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.SplitInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return -1; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            if (args.Count >= 4)
            {
                NeuronCluster iCode = args[0] as NeuronCluster;
                if (iCode != null)
                {
                    NeuronCluster iList = args[1] as NeuronCluster;
                    if (iList != null)
                    {
                        Variable iVar = args[2] as Variable;
                        if (iVar != null)
                        {
                            SplitManager.SplitArgs iSplitArgs = new SplitManager.SplitArgs();
                            iSplitArgs.Callback = iCode;
                            iSplitArgs.ResultList = iList;
                            iSplitArgs.Variable = iVar;
                            iSplitArgs.Processor = processor;
                            for (int i = 3; i < args.Count; i++)
                                iSplitArgs.ToSplit.Add(args[i]);
                            SplitManager.Default.Split(iSplitArgs);
                        }
                        else
                            Log.Log.LogError("SplitInstruction.Execute", "Invalid third argument, Variable expected, found null.");
                    }
                    else
                        Log.Log.LogError("SplitInstruction.Execute", "Invalid second argument, NeuronCluster expected, found null.");
                }
                else
                    Log.Log.LogError("SplitInstruction.Execute", "Invalid first argument, NeuronCluster expected, found null.");
            }
            else
                Log.Log.LogError("SplitInstruction.Execute", "Invalid nr of arguments specified");
        }
    }
}