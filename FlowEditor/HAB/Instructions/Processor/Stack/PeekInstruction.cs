﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions.Processor.Stack
{
    /// <summary>
    ///     Returns the top of the Neuron execution stack without removing it.  If the stack is empty,
    ///     <see cref="PredefinedNeurons.Emtpy" /> is used.
    /// </summary>
    [NeuronID((ulong)PredefinedNeurons.PeekInstruction)]
    public class PeekInstruction : SingleResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        ///     Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.PeekInstruction" />.</value>
        public override Neuron TypeOfNeuron
        {
            get { return Brain.Brain.Current[(ulong)PredefinedNeurons.PeekInstruction]; }
        }

        #endregion TypeOfNeuron

        /// <summary>
        ///     Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        ///     A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 0; }
        }

        /// <summary>
        ///     Gets the actual value.
        /// </summary>
        /// <param name="processor">The processor to use.</param>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The result of the instruction.</returns>
        protected override Neuron InternalGetValue(HAB.Processor.Processor processor, IList<Neuron> list)
        {
            return processor.Peek();
        }
    }
}