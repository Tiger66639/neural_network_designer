﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;
using System.Diagnostics;

namespace JaStDev.HAB.Instructions.Processor.Stack
{
    /// <summary>
    ///     This instruction removes the current <see cref="Neuron" /> from the <see cref="Processor" />.
    /// </summary>
    [NeuronID((ulong)PredefinedNeurons.PopInstruction)]
    //[XmlRoot("PopInstruction", Namespace = "http://tiger66639.clan.rip", IsNullable = false)]
    public class PopInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        ///     Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.PopInstruction" />.</value>
        public override Neuron TypeOfNeuron
        {
            get { return Brain.Brain.Current[(ulong)PredefinedNeurons.PopInstruction]; }
        }

        #endregion TypeOfNeuron

        public override int ArgCount
        {
            get { return 0; }
        }

        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            Debug.Assert(processor != null);
            processor.Pop();
        }
    }
}