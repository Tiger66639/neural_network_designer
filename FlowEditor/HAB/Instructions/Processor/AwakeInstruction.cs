﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions.Processor
{
    /// <summary>
    /// Lets a suspended processor (through <see cref="SuspendInstruction"/>) continue processing and removes the
    /// neuron from the cluster. This is an atomic instruction.
    /// </summary>
    /// <remarks>
    /// This instruction behaves very similar to the <see cref="RemoveChildInstruction"/> in that it takes the same arguments:
    /// a cluster and a neuron + it also tries to remove the neuron from the cluster. This is done to provide an atomic
    /// remove operation with the wakeup instruction. This way, we can be certain that, once the processor has been woken up,
    /// the item is removed from the cluster.
    /// <para>
    /// Arguments:
    /// - the cluster fromt where the neuron needs to be removed.
    /// - the neuron that represents the <see cref="Processor"/>.  This was previously returned by the <see cref="SuspendInstruction"/>.
    /// </para>
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.AwakeInstruction)]
    public class AwakeInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.AwakeInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.AwakeInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 2; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            if (args != null && args.Count >= 2)
            {
                Neuron iIndicator = args[1];
                if (iIndicator == null)
                {
                    Log.Log.LogError("AwakeInstruction.Execute", "item to remove is null (second arg).");
                    return;
                }
                NeuronCluster iCluster = args[0] as NeuronCluster;
                if (iCluster != null)
                {
                    using (ChildrenAccessor iList = iCluster.ChildrenW)
                        iList.Remove(iIndicator);

                    SuspendInstruction.Resume(iIndicator);
                }
                else
                {
                    Log.Log.LogError("AwakeInstruction.Execute", string.Format("Can't remove, Invalid cluster specified: {}.", args[0]));
                    return;
                }
            }
            else
                Log.Log.LogError("AwakeInstruction.Execute", "No arguments specified");
        }
    }
}