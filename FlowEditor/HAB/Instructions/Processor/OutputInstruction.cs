﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Sins;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions.Processor
{
    /// <summary>
    /// Sends the specified arguments to a <see cref="Sin"/> for output.  This is done directly (so without solving first)
    /// in a synchronous manner.
    /// </summary>
    /// <remarks>
    /// At least 2 parameters are required:
    /// - first the Sin to use for output.
    /// - followed by the first neuron to send as output
    /// - and any others may come after that.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.OutputInstruction)]
    public class OutputInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.DirectOutputInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.OutputInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return -1; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            if (args != null && args.Count >= 2)
            {
                Sin iSin = args[0] as Sin;
                if (iSin != null)
                {
                    for (int i = 1; i < args.Count; i++)
                        iSin.Output(args[i]);
                }
                else
                    Log.Log.LogError("OutInstruction.Execute", "Invalid first argument, NeuronCluster expected, found null.");
            }
            else
                Log.Log.LogError("OutInstruction.Execute", "No arguments specified");
        }
    }
}