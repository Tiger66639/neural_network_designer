﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Processor;
using System.Collections.Generic;
using System.Diagnostics;

namespace JaStDev.HAB.Instructions.Processor.Weight
{
    /// <summary>
    /// Resets the weigth of all the neurons passed as argument.
    /// </summary>
    [NeuronID((ulong)PredefinedNeurons.ResetWeightInstruction)]
    public class ResetWeightInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.ResetWeightInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.ResetWeightInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return -1; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            if (args != null)
            {
                SplitResultsDict iDict = processor.SplitValues;
                Debug.Assert(iDict != null);
                foreach (Neuron i in args)
                    iDict.Remove(i.ID);
            }
            else
                Log.Log.LogError("GetMaxWeightInstruction.Execute", "No arguments specified");
        }
    }
}