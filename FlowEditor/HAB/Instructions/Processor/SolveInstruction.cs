﻿using JaStDev.HAB.Brain;
using System;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions.Processor
{
    /// <summary>
    /// This instruction tries to solve a neuron on a new processor.
    /// </summary>
    /// <remarks>
    /// The new processor will be running for the same sin as the calling processor.
    /// 1 arg:
    /// - The neuron that needs to be solved.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.SolveInstruction)]
    public class SolveInstruction : Instruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.OutputInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.SolveInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Any length is possible but at least 2 are required.
        /// </summary>
        public override int ArgCount
        {
            get { return 1; }
        }

        /// <summary>
        /// Performs the tasks on the specified processor.
        /// </summary>
        /// <param name="processor">The processor on which the tasks need to be performed.</param>
        /// <param name="args">The arguments that the instruction requires to be properly executed. These are also
        /// <see cref="Neuron"/>s.</param>
        /// <remarks>
        /// Instructions should never work directly on the data other than for searching.  Instead, they should go
        /// through the methods of the <see cref="Processor"/> that is passed along as an argument.  This is important
        /// cause when the instruction is executed for a sub processor, the changes might need to be discarded.
        /// </remarks>
        public override void Execute(HAB.Processor.Processor processor, IList<Neuron> args)
        {
            if (args != null && args.Count >= 1)
            {
                HAB.Processor.Processor iNew = Activator.CreateInstance(processor.GetType()) as HAB.Processor.Processor;              //the current processor can be a debugger, in which case, we want the same type.                                                          //switch to the output sin, otherwise the message goes to the input sin.
                iNew.CurrentSin = processor.CurrentSin;                                                   //we keep the same sin.
                iNew.Push(args[0]);
                iNew.Solve();
            }
            else
                Log.Log.LogError("OutInstruction.Execute", "No arguments specified");
        }
    }
}