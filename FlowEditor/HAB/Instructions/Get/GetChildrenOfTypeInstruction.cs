﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using System.Collections.Generic;

namespace JaStDev.HAB.Instructions.Get
{
    /// <summary>
    /// Returns all the children of the specified type for the cluster.
    /// </summary>
    /// <remarks>
    /// Arguments:
    /// 1: The cluster for which to return the children
    /// 2: the neuron that idenfities the type of children that should be returned.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.GetChildrenOfTypeInstruction)]
    public class GetChildrenOfTypeInstruction : MultiResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.IndexOfChildInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.GetChildrenOfTypeInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// performs the task and returns it's result.
        /// </summary>
        /// <param name="processor"></param>
        /// <param name="list">The list of arguments</param>
        /// <returns>The list of results</returns>
        /// <remarks>
        /// Note: when When a result instruction is executed, ( so <see cref="ResultInstruction.Execute"/> is
        /// called instead of <see cref="ResultInstruction.GetValues"/>, the result value(s) are pushed on the
        /// execution stack.
        /// </remarks>
        public override IEnumerable<Neuron> GetValues(HAB.Processor.Processor processor, IList<Neuron> list)
        {
            List<Neuron> iRes = new List<Neuron>();
            if (list != null && list.Count >= 2)
            {
                NeuronCluster iCluster = list[0] as NeuronCluster;
                Neuron iType = list[1];
                if (iCluster != null)
                {
                    if (iType != null)
                    {
                        using (ChildrenAccessor iList = iCluster.Children)
                        {
                            foreach (Neuron i in iList.ConvertTo<Neuron>())
                            {
                                if (i.TypeOfNeuron == iType)
                                    iRes.Add(i);
                            }
                        }
                    }
                    else
                        Log.Log.LogError("GetChildrenOfTypeInstruction.InternalGetValue", "Invalid second argument, Neuron expected, found null.");
                }
                else
                    Log.Log.LogError("GetChildrenOfTypeInstruction.InternalGetValue", "Invalid first argument, NeuronCluster expected, found ." + list[0].ToString());
            }
            else
                Log.Log.LogError("GetChildrenOfTypeInstruction.InternalGetValue", "Invalid nr of arguments specified");
            return iRes;
        }

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 2; }
        }
    }
}