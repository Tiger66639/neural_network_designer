﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using System.Collections.Generic;
using System.Linq;

namespace JaStDev.HAB.Instructions.Get
{
    /// <summary>
    /// Returns the cluster that owns the specified neuron and has the specified meaning.
    /// </summary>
    /// <remarks>
    /// <para>
    /// This is a shortcut instruction that combines a few statements (getClusters, loop, check meaning).
    /// </para>
    /// Arguments:
    /// 1: The neuron for which to find a cluster that it belongs to.
    /// 2: the meaning that should be assigned to the cluster that needs to be returned.
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.GetFirClusterInstruction)]
    public class GetFirstClusterInstruction : SingleResultInstruction
    {
        #region TypeOfNeuron

        /// <summary>
        /// Gets the type of this neuron expressed as a Neuron.
        /// </summary>
        /// <value><see cref="PredefinedNeurons.GetChildrenInstruction"/>.</value>
        public override Neuron TypeOfNeuron
        {
            get
            {
                return Brain.Brain.Current[(ulong)PredefinedNeurons.GetFirClusterInstruction];
            }
        }

        #endregion TypeOfNeuron

        /// <summary>
        /// Gets the actual value.
        /// </summary>
        /// <param name="processor">The processor to use.</param>
        /// <param name="list">the list to get the nr of items from.</param>
        /// <returns>The result of the instruction.</returns>
        protected override Neuron InternalGetValue(HAB.Processor.Processor processor, IList<Neuron> list)
        {
            if (list != null && list.Count >= 2)
            {
                Neuron iChild = list[0];
                Neuron iMeaning = list[1];
                if (iChild != null)
                    if (iMeaning != null)
                    {
                        using (NeuronsAccessor iClusteredBy = iChild.ClusteredBy)
                        {
                            var iClusters = from i in iClusteredBy.Items select Brain.Brain.Current[i] as NeuronCluster;
                            foreach (NeuronCluster i in iClusters)
                            {
                                if (i.Meaning == iMeaning.ID)
                                    return i;
                            }
                        }
                    }
                    else
                        Log.Log.LogError("GetFirstClusterInstruction.InternalGetValue", "Invalid second argument, Neuron expected, found null.");
                else
                    Log.Log.LogError("GetFirstClusterInstruction.InternalGetValue", "Invalid first argument, Neuron expected, found null.");
            }
            else
                Log.Log.LogError("GetFirstClusterInstruction.InternalGetValue", "Invalid nr of arguments specified");
            return Brain.Brain.Current[(ulong)PredefinedNeurons.Empty];                                                          //if not found, return empty.
        }

        /// <summary>
        /// Returns the number of arguments that are required by this instruction.
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// A value of -1 indicates that a list of neurons is allowed, without any specific number of values.
        /// </remarks>
        public override int ArgCount
        {
            get { return 2; }
        }
    }
}