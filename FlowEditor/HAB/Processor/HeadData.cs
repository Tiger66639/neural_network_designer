﻿using JaStDev.HAB.Brain;
using System.Collections.Generic;

namespace JaStDev.HAB.Processor
{
    /// <summary>
    /// Contains all the data specific for a split of a processor.
    /// </summary>
    internal class HeadData
    {
        /// <summary>
        /// Keeps track of how many sub processors are still active.
        /// </summary>
        public int StillActive { get; set; }

        /// <summary>
        /// Keeps a record of all the sub processors.
        /// </summary>
        public List<SplitData> SubProcessors { get; set; }

        /// <summary>
        /// Gets or sets the neuroncluster with the callback code that needs to be called when the split is done.
        /// </summary>
        /// <value>The callback.</value>
        public NeuronCluster Callback { get; set; }

        /// <summary>
        /// Gets or sets the Neuroncluster that will store all the different possible results.
        /// </summary>
        /// <value>The result list.</value>
        public NeuronCluster ResultList { get; set; }

        /// <summary>
        /// Gets or sets the SplitData object that is owned by the processor that requested the split.
        /// </summary>
        /// <value>The requestor.</value>
        public SplitData Requestor { get; set; }

        /// <summary>
        /// Gets or sets <see cref="HeadData"/> object that was previously active before a new split was done.
        /// When this headData is ready the specified object gets signalled as one more channel ready.
        /// </summary>
        /// <value>Previous.</value>
        public HeadData Previous { get; set; }
    }
}