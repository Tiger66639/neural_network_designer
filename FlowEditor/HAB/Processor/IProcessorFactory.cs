﻿namespace JaStDev.HAB.Processor
{
    public interface IProcessorFactory
    {
        Processor GetProcessor();
    }
}