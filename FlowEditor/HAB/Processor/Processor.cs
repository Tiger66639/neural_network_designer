﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using JaStDev.HAB.Expressions;
using JaStDev.HAB.Sins;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace JaStDev.HAB.Processor
{
    /// <summary>
    ///     Used by the <see cref="JaStDev.HAB.Brain" /> object to perform translations from 1 type of <see cref="Neuron" /> to
    ///     another using
    ///     <see cref="Neuron.Rules" /> and <see cref="Neuron.Actions" />.
    /// </summary>
    /// <remarks>
    ///     <para>
    ///         A processor tries to solve all the neurons on it's stack for as long as there remains a neuron on it.  If a
    ///         solve run is complete
    ///         for a specific neuron, and there is something still on the stack, it continues to solve that neuron, and so on.
    ///     </para>
    ///     <para>
    ///         The property <see cref="Processor.SplitData" /> determins if it is running as the child of another processor
    ///         due to a <see cref="Processor.Split" />
    ///         operation. Sub processors are used to solve conflicts between multiple possibilities by trying out different
    ///         scenarios at the same
    ///         time.
    ///     </para>
    ///     <para>
    ///         After the split has been resolved, the last processor continues processing.
    ///     </para>
    ///     <para>
    ///         Descendents should reimplement CreateProcessors.
    ///     </para>
    ///     <para>
    ///         When a processor is finished, the <see cref="Processor.Finished" /> event is raised.
    ///     </para>
    /// </remarks>
    public class Processor
    {
        #region internal types

        /// <summary>
        ///     Defines the different states that a processor can have. This is primarely used for split, continue, break and exit
        ///     requests.
        /// </summary>
        public enum State
        {
            Normal,
            Exit,
            Continue,

            /// <summary>
            ///     when it was not possible to convert the neuron to someting else.
            /// </summary>
            NotUnderstood
        }

        #endregion internal types

        #region Events

        /// <summary>
        ///     Raised when the processor is finished processing.
        /// </summary>
        public event EventHandler Finished;

        #endregion Events

        #region fields

        /// <summary>
        ///     Stores all the temporary data to be filled in.
        /// </summary>
        private readonly Stack<Neuron> fStack = new Stack<Neuron>(Settings.InitProcessorStackSize);

        private IList<Expression> fToProcess;
        //ref to the list currently being executed.  Allows us to return the 'Expression' currently being executed (not just the id).

        private AutoResetEvent fThreadBlocker;

        /// <summary>
        ///     This static has a unique value for each thread.  It contains which <see cref="Processor" /> is currently
        ///     running in the current thread.  This is used by the <see cref="JaStDev.HAB.Brain.Add" /> to check for valid
        ///     operations.
        /// </summary>
        [ThreadStatic]
        public static Processor CurrentProcessor; //must be a field, can't use ThreadStatic otherwise.

        #endregion fields

        #region prop

        #region NeuronToSolve

        /// <summary>
        ///     Gets the Neuron currently being solved.
        /// </summary>
        /// <remarks>
        ///     This can be usefull to do queries from.  It is considered to be an always valid variable (
        ///     or better a constant) from within <see cref="Instruction" />s and search criteria, so from
        ///     within neural programs.
        /// </remarks>
        public virtual Neuron NeuronToSolve { get; internal set; }

        #endregion NeuronToSolve

        #region VariableValues

        /// <summary>
        ///     Gets the stack of dictionaries containing all the current values for the variables.
        /// </summary>
        public Stack<Dictionary<ulong, List<Neuron>>> VariableValues { get; } =
            new Stack<Dictionary<ulong, List<Neuron>>>();

        #endregion VariableValues

        #region CurrentState

        /// <summary>
        ///     Gets the current state of the brain.
        /// </summary>
        /// <value>The state of the current.</value>
        internal State CurrentState { get; private set; } = State.Normal;

        #endregion CurrentState

        #region SplitData

        /// <summary>
        ///     Gets or sets the split data.
        /// </summary>
        /// <remarks>
        ///     contains data for sub procesors, also indicates that this is a sub processor.
        ///     Must be internal so that it can't be seen by reflector.
        /// </remarks>
        /// <value>The split data.</value>
        internal SplitData SplitData { get; set; }

        #endregion SplitData

        #region SplitValues

        /// <summary>
        ///     Gets the dictionary containing all the split result values for this processor. This is a thread safe accessible
        ///     dictionary.
        /// </summary>
        /// <remarks>
        ///     Because this is a dictionary, it is not possible to store the same neuron 2 times.  This guarantees that the list
        ///     always contains unique items.
        /// </remarks>
        /// <value>The split values.</value>
        protected internal SplitResultsDict SplitValues { get; } = new SplitResultsDict();

        #endregion SplitValues

        #region CurrentExpID

        /// <summary>
        ///     Gets/sets the index of the <see cref="Instruction" /> that will be executed next.
        /// </summary>
        /// <remarks>
        ///     This points to the next instruction for the split.
        ///     Changing this value will cause a jump in the execution position.
        /// </remarks>
        public int NextExpID { get; set; }

        #endregion CurrentExpID

        #region CurrentExpression

        /// <summary>
        ///     Gets the current expression that is being (or just was) executed.
        /// </summary>
        /// <value>The current expression.</value>
        public Expression CurrentExpression
        {
            get
            {
                var iId = NextExpID - 1;
                //we always point to the next step already when a step is executing (this is done for the split).
                if (fToProcess != null && iId >= 0 && iId < fToProcess.Count)
                    return fToProcess[iId];
                return null;
            }
        }

        #endregion CurrentExpression

        #region CurrentLink

        /// <summary>
        ///     Gets/sets the index of the link that is currently being processed in a <see cref="Processor.Solve" />  operation.
        /// </summary>
        /// <remarks>
        ///     Changing this value will cause a jump in the execution position.
        /// </remarks>
        public virtual int CurrentLink { get; set; }

        #endregion CurrentLink

        #region CurrentSin

        /// <summary>
        ///     Gets/sets the Sin that triggered this processor / for which it is generating data.
        /// </summary>
        /// <remarks>
        ///     This is stored in the processor cause a processor can only have 1 current sin and is
        ///     local for the process.
        ///     It is provided so that the <see cref="CurrentSin" /> system variable can easely find this
        ///     value.
        ///     <para>
        ///         This var is automatically filled in once a processor starts.  It can be changed by the
        ///         <see cref="OutputInstruction" /> when it turns the processor into an output generator.
        ///     </para>
        /// </remarks>
        public Sin CurrentSin { get; internal set; }

        #endregion CurrentSin

        #region CurrentMeaning

        /// <summary>
        ///     Gets/sets the meaning neuron that is currently being executed in order to solve the
        ///     <see cref="Processor.NeuronToSolve" />.
        /// </summary>
        public Neuron CurrentMeaning { get; internal set; }

        #endregion CurrentMeaning

        #region CurrentExpressionSource

        /// <summary>
        ///     Gets the Neuron that contains the code currently being executed.  Which type of code that is being executed (the
        ///     list), can
        ///     be found throught <see cref="Processor.CurrentExecListType" />
        /// </summary>
        /// <remarks>
        ///     This is primarely provided for debuggers and designers.
        /// </remarks>
        public Neuron CurrentExecSource { get; internal set; }

        #endregion CurrentExpressionSource

        #region CurrentExecListType

        /// <summary>
        ///     Gets the type of list that is currently being executed, which is attached to
        ///     <see cref="Processor.CurrentExpressionSource" />.
        /// </summary>
        /// <remarks>
        ///     This is primarely provided for debuggers and designers.
        /// </remarks>
        public ExecListType CurrentExecListType { get; internal set; }

        #endregion CurrentExecListType

        #region Data stack

        /// <summary>
        ///     the data stack.
        /// </summary>
        protected Stack<Neuron> Stack
        {
            get { return fStack; }
        }

        #endregion Data stack

        /// <summary>
        ///     Gets the number of neurons currently on the stack.
        /// </summary>
        /// <value>The nr of items on the stack.</value>
        public int Count
        {
            get { return fStack.Count; }
        }

        #region ThreadNeuron

        /// <summary>
        ///     Gets the Neuron that represents the thread in which this processor is running.
        /// </summary>
        public Neuron ThreadNeuron { get; set; }

        #endregion ThreadNeuron

        #region ThreadBlocker

        /// <summary>
        ///     Gets the object to block, restart the processor.
        /// </summary>
        public AutoResetEvent ThreadBlocker
        {
            get
            {
                if (fThreadBlocker == null)
                    fThreadBlocker = new AutoResetEvent(false);
                return fThreadBlocker;
            }
        }

        #endregion ThreadBlocker

        #endregion prop

        #region Functions

        #region Solve/call/branch

        /// <summary>
        ///     Tries to solve all the <see cref="Neuron" />s currently on the stack untill the stack is empty, untill there is
        ///     only 1 neuron
        ///     left that can't be solved anymore or untill <see cref="Process.Exit" /> is called.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         Inheriters should reinplement <see cref="Processor.InternalSolve" /> since this is called by all solving
        ///         routines (this and
        ///         <see cref="Processor.SolveBlocked" />).
        ///     </para>
        ///     this method simply starts an internal thread.
        /// </remarks>
        public void Solve()
        {
            Action iFunc = InternalSolve;
            iFunc.BeginInvoke(null, null);
        }

        public void SolveBlocked()
        {
            Action iFunc = InternalSolve;
            var iAsyncCall = iFunc.BeginInvoke(null, null);
            iAsyncCall.AsyncWaitHandle.WaitOne(); //we wait untill the sub thread is finnished.
        }

        /// <summary>
        ///     pops last item from stack to solve, solves it and continues untill there is nothing more on the stack.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         Actual solving is done by <see cref="Processor.SolveStack" />.  This one mostly takes care of the current
        ///         processor.
        ///     </para>
        /// </remarks>
        private void InternalSolve()
        {
            try
            {
                CurrentProcessor = this; //we need to indicate that this thread is run for this processor
                try
                {
                    SolveStack();
                    OnFinished();
                }
                finally
                {
                    CurrentProcessor = null;
                }
            }
            catch (Exception e) //we need a catch all handler since this is the entry point of the thread.
            {
                Log.Log.LogError("Processor.InternalSolve", string.Format("Internal error: {0}.", e));
            }
        }

        /// <summary>
        ///     Called when the processor is finished with solving the stack. Raises the <see cref="Processor.Finished" /> event.
        /// </summary>
        protected virtual void OnFinished()
        {
            if (Finished != null)
                Finished(this, EventArgs.Empty);
        }

        /// <summary>
        ///     Solves (or tries to solve) all the neurons on the stack.
        /// </summary>
        /// <remarks>
        ///     Actual solving is done by another method.  This one mostly handles state changes in the procesor like for exit
        ///     requests.
        ///     The state of the processor is checked before any code is called.
        /// </remarks>
        private void SolveStack()
        {
            Neuron iToSolve;
            var iLoop = true;
            while (iLoop)
            {
                if (Stack.Count > 0 && CurrentState != State.Exit)
                //we check the exit state before we call any code, cause this function can be called from 'SolveStackFrom', in which case we must be prepared for an exit state.
                {
                    iToSolve = Stack.Pop();
                    CurrentLink = 0; //solveNeuron doesn't do this cause it allows arbitrary start pos.
                    SolveNeuron(iToSolve);
                }
                else
                {
                    var iLastOfSplits = false;
                    //when true, this thread was the last to resolve in a split set.  This is used to determin if we need to continue processing or not.
                    if (SplitData != null)
                        //when we get here, let it know the split is ready, so we can check if we need to continue after resolving it.
                        iLastOfSplits = SplitManager.Default.FinishSplit(SplitData);
                    if (Stack.Count != 0 && iLastOfSplits)
                        CurrentState = State.Normal;
                    //always reset the state, cause the loop properly exited through the Exit statement.  After the split has been resolved, the stack is empty again, so if there is something on it after resolving the split, the callback put it on there.
                    else
                        iLoop = false; //we really want to quit.
                }
            }
        }

        /// <summary>
        ///     Tries to solve the specified neuron.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         Solving is done by walking down every 'To' link in the neuron and pushing this to reference on the stack after
        ///         which it calls
        ///         the <see cref="Link.Meaning" />'s <see cref="Neuron.Actions" /> list.
        ///     </para>
        ///     <para>
        ///         this is an internal function, cause the output instruction needs to be able to let a neuron be solved
        ///         sequential (<see cref="Processor.Solve" /> is async and solves the intire stack.
        ///     </para>
        /// </remarks>
        /// <param name="toSolve"></param>
        protected internal virtual void SolveNeuron(Neuron toSolve)
        {
            VariableValues.Push(new Dictionary<ulong, List<Neuron>>());
            //a new function needs a new dictionary for the variables, so that variable values are local to the function.
            try
            {
                SolveNeuronWithoutVarChange(toSolve);
            }
            finally
            {
                VariableValues.Pop(); //remove the dictionary cause the function is ready.
            }
        }

        /// <summary>
        ///     Solves the neuron without changing the variable dictionary stack.
        /// </summary>
        /// <remarks>
        ///     When solving, we need to check if the item needs to be sent to the output sin.  This is the
        ///     case if we are in output mode + the current neuron to solve has not outgoing links that it can solve itself
        ///     <para>
        ///         also sets <see cref="Processor.NeuronToSolve" />
        ///     </para>
        /// </remarks>
        /// <param name="toSolve">To solve.</param>
        private void SolveNeuronWithoutVarChange(Neuron toSolve)
        {
            IList<Expression> iActions = null;
            //keeps track+stores the expressions of the 'Actions' link for the neuron to solve.  This is handled seperatly.
            NeuronToSolve = toSolve;
            //don't init current link cause this way we can start from an arbitrary position like with SolveNeuronFrom
            //also: can't put a 'using' for the accessor to the links round the whole loop, cause then we can't change the currenly changing neuron,
            //wich should be possible.
            int iCount;
            using (var iLinksToSolve = toSolve.LinksOut)
                iCount = iLinksToSolve.Items.Count;
            while (CurrentLink < iCount) //using this type of loop allows the processor to easely perform code jumps.
            {
                using (var iLinksToSolve = toSolve.LinksOut)
                    CurrentMeaning = iLinksToSolve.Items[CurrentLink].Meaning;
                //can't advance currrentlink here yet cause some instructions might still want to use it.
                if (CurrentMeaning != null && CurrentMeaning.ID != (ulong)PredefinedNeurons.Actions)
                {
                    NextExpID = 0;
                    //we reset the current expression, cause Process doesn't do this so it can easely be called using the middle of the execution list as starting point.
                    IList<Expression> iExpList = CurrentMeaning.Rules;
                    if (iExpList != null)
                        Process(CurrentMeaning, iExpList, ExecListType.Rules);
                    //this will continue to process where the parent left off cause the split copied over the current statement position
                    if (CurrentState == State.Exit)
                        break;
                    if (NeuronToSolve.ID == Neuron.EmptyId)
                    //if CurrentFrom got deleted, we can't process any further and need to stop.  This is a safety precaution.
                    {
                        Log.Log.LogWarning("Processor.SolveNeuron",
                            "CurrentFrom has been deleted, moving on to next neuron.");
                        break;
                    }
                }
                else if (CurrentMeaning == null)
                    Log.Log.LogError("Processor.SolveNeuron",
                        "Internal error: link has an invalid meaning cause it can't be found in the brain.");
                else if (iActions == null)
                //this is simply a quicker way to find the 'action's ref.  We have to walk through all the links anyway, so we will pass the 'actions' link
                {
                    NeuronCluster iCluster;
                    using (var iLinksToSolve = toSolve.LinksOut)
                        iCluster = iLinksToSolve.Items[CurrentLink].To as NeuronCluster;
                    Debug.Assert(iCluster != null);
                    using (var iList = iCluster.Children)
                        iActions = iList.ConvertTo<Expression>();
                }
                else
                    Log.Log.LogWarning("Processor.SolveNeuronWithoutVarChange",
                        "Found more than 1 link declared as the 'actions' cluster!");
                //it's a double 'actions' list, which should not be declared, not illegal, but lets get the user informed.
                CurrentLink++; //if we don't do this, we never get out of the loop.
                using (var iLinksToSolve = toSolve.LinksOut)
                    iCount = iLinksToSolve.Items.Count;
            }
            CurrentMeaning = null;
            PerformActions(toSolve, iActions);
        }

        /// <summary>
        ///     If the neuron has any actions defined, these are executed here.
        /// </summary>
        /// <param name="toSolve">neuron to solve.</param>
        /// <param name="actions">
        ///     The actions that should be executed.  These are passed along cause
        ///     this is faster for the calling function.
        /// </param>
        private void PerformActions(Neuron toSolve, IList<Expression> actions)
        {
            if (actions != null)
            //quick check, if there are no items at this point, we don't try to execute.  This is not thread save, can't block cause ConvertTo does the actual blocking.
            {
                NextExpID = 0;
                Process(toSolve, actions, ExecListType.Actions);
            }
        }

        /// <summary>
        ///     Solves the stack from the specified neuron. Used for starting a subprocessor.
        /// </summary>
        /// <param name="from">From.</param>
        internal void SolveStackFrom(Neuron from)
        {
            Thread.CurrentThread.Name = "SubProc for: " + from.ID;
            //this is for easy debugging, so we recognise the thread in VS.
            CurrentProcessor = this; //we need to indicate that this thread is run for this processor
            try
            {
                try
                {
                    SolveNeuronFrom(from);
                    SolveStack();
                }
                catch (Exception e) //if there is an exception, we still need to check and update the end of the split.
                {
                    if (SplitData != null) //when we get here, let it know we are ready.
                        SplitManager.Default.FinishSplit(SplitData);
                    throw e;
                }
                OnFinished();
            }
            finally
            {
                CurrentProcessor = null;
                VariableValues.Pop();
                //remove the dictionary cause the function is ready and it was put on there due to the split.
            }
        }

        /// <summary>
        ///     Tries to solve the specified neuron starting from the specified link and statement.
        /// </summary>
        /// <param name="toSolve">The neuron that needs solving.</param>
        /// <param name="fromLink">The index of the link that we need to continue processing.</param>
        /// <param name="fromStatement"></param>
        protected virtual void SolveNeuronFrom(Neuron toSolve)
        {
            if (toSolve != null && toSolve.ID != Neuron.EmptyId)
            {
                using (var iLinksOut = toSolve.LinksOut)
                {
                    if (CurrentLink < iLinksOut.Items.Count)
                    {
                        NeuronToSolve = toSolve;
                        CurrentMeaning = iLinksOut.Items[CurrentLink].Meaning;
                        //we need to execute the part of the currentLink that wasn't processed by the parent here, cause SolveNeuron resets currentExpression, which we need to keep as it is cause it indicates where the parent stopped and where we should continue.
                        if (CurrentMeaning != null)
                        {
                            IList<Expression> iExecList = CurrentMeaning.Rules;
                            if (iExecList != null)
                                Process(CurrentMeaning, iExecList, ExecListType.Rules);
                            //this will continue to process where the parent left off cause the split copied over the current statement position
                            if (CurrentState == State.Exit)
                                return;
                            CurrentLink++; //we need to advance caus current link has been processed, need to do next.
                            SolveNeuronWithoutVarChange(toSolve);
                            //we hook back into the regular procedure, don't need var dict handling, this is done here cause a split makes a duplicate of the original's dict.
                        }
                        else
                            Log.Log.LogError("Processor.SolveNeuronFrom",
                                "Internal error: link has an invalid meaning cause it can't be found in the brain.");
                    }
                    else
                        Log.Log.LogError("Processor.SolveNeuronFrom",
                            "Internal error: failed to split neuron cause the current link execution position is invalid.");
                }
            }
            else
                Log.Log.LogWarning("Processor.SolveNeuronFrom",
                    "Neuron to solve has been deleted, moving on to next neuron.");
        }

        /// <summary>
        ///     Tries to process the list of neurons.
        /// </summary>
        /// <remarks>
        ///     Don't init the pos, this way it can be used to start from an arbitrary pos like with SolveNeuronFrom.
        /// </remarks>
        /// <param name="toExec">
        ///     The neuron who's code needs executing.  This is not used in this class but is provided
        ///     for inheritters, so they can do something with it.
        /// </param>
        protected virtual void Process(Neuron toExec, IList<Expression> toProcess, ExecListType listType)
        {
            if (toExec != null && toExec.ID != Neuron.EmptyId)
            {
                var iLength = toProcess.Count;
                var iPrev = fToProcess;
                //we need to store prev val cause this function can be called recursivelly (when a block or branch is executed).
                var iPrevToExec = CurrentExecSource;
                var iPrevExecType = CurrentExecListType;

                try
                {
                    fToProcess = toProcess;
                    CurrentExecListType = listType;
                    CurrentExecSource = toExec;
                    while (NextExpID >= 0 && NextExpID < iLength)
                    //using this type of loop allows the processor to easely perform code jumps.
                    {
                        var iExp = toProcess[NextExpID];
                        Process(iExp);
                        //we don't call iExp.Execute directly, this way, a debugger or something else can do something just before and/or after a statement is performed.
                    }
                }
                finally
                {
                    fToProcess = iPrev;
                    //this used to be in a finally block, but can't do this, cause, when there is an exception, that would reset this list, which disables correct logging of the expression that caused the exception.
                    CurrentExecListType = iPrevExecType;
                    CurrentExecSource = iPrevToExec;
                }
            }
            else
                Log.Log.LogWarning("Processor.Process", "Neuron to process has been deleted, moving on to next neuron.");
        }

        /// <summary>
        ///     Tries to process a single expression.
        /// </summary>
        /// <param name="toProcess"></param>
        protected virtual void Process(Expression toProcess)
        {
            NextExpID++;
            //we change the current position here, this is safest before 'Execute' is called on instruction, cause if we do a split instruction, this val gets copied and this way, it already points to the correct next instrcution to execute.
            toProcess.Execute(this);
        }

        /// <summary>
        ///     Converts the neuron cluster's children to expressions and tries to execute them
        /// </summary>
        public virtual void Call()
        {
            if (fStack.Count > 0)
                //not really thread save cause items can change between this call and the convertion, this is no blocking issue though cause it would still work, just could miss an execution is a small nr of cases.
                Call(fStack.Pop() as NeuronCluster);
            else
                Log.Log.LogError("Processor.Call", "Internal error: no more items on stack.");
        }

        /// <summary>
        ///     Converts the neuron cluster's children to expressions and tries to execute them
        /// </summary>
        /// <remarks>
        ///     Since this function can also be called from the outside to initiate a run, it must
        ///     also assign the current processor, and reset it after it's done.
        /// </remarks>
        public virtual void Call(NeuronCluster cluster)
        {
            var iPrev = CurrentProcessor;
            CurrentProcessor = this; //we need to indicate that this thread is run for this processor
            try
            {
                if (cluster != null)
                {
                    IList<Expression> iToExecute;
                    using (var iList = cluster.Children)
                        iToExecute = iList.ConvertTo<Expression>();
                    if (iToExecute == null)
                    {
                        Log.Log.LogError("Processor.Call",
                            string.Format(
                                "Failed to transform children of cluster {0} to a list of expressions, call aborted.",
                                cluster));
                        return;
                    }
                    var iPrevPos = NextExpID;
                    VariableValues.Push(new Dictionary<ulong, List<Neuron>>());
                    //a new function needs a new dictionary for the variables, so that variable values are local to the function.
                    try
                    {
                        NextExpID = 0;
                        Process(cluster, iToExecute, ExecListType.Children);
                    }
                    finally
                    {
                        NextExpID = iPrevPos;
                        VariableValues.Pop();
                    }
                }
                else
                    Log.Log.LogError("Processor.Call", "Failed to perform call: NeuronCluster ref is null.");
            }
            finally
            {
                CurrentProcessor = iPrev;
            }
        }

        /// <summary>
        ///     Converts the neuron cluster's children to expressions and tries to execute them while preserving the current
        ///     set of variable values, which is ideal for branch evaluation or expression blocks.
        /// </summary>
        internal void EvaluateBranch(NeuronCluster cluster)
        {
            if (cluster != null)
            {
                IList<Expression> iToExecute;
                using (var iList = cluster.Children)
                    iToExecute = iList.ConvertTo<Expression>();
                if (iToExecute == null)
                {
                    Log.Log.LogError("Processor.EvaluateBranch",
                        string.Format(
                            "Failed to transform children of cluster {0} to a list of expressions, evaluate branch aborted.",
                            cluster));
                    return;
                }
                var iPrevPos = NextExpID;
                try
                {
                    NextExpID = 0;
                    Process(cluster, iToExecute, ExecListType.Children);
                }
                finally
                {
                    NextExpID = iPrevPos;
                }
            }
            else
                Log.Log.LogError("Processor.EvaluateBranch",
                    "Failed to evaluate branch: no NeuronCluster specified to evaluate.");
        }

        #endregion Solve/call/branch

        /// <summary>
        ///     Gets the current 'processor local' value for the specified value.
        /// </summary>
        /// <remarks>
        ///     This function is not thread safe.  Make certain that you call it from within the processor thread when the proc is
        ///     running.
        /// </remarks>
        /// <param name="variable">The variable for which to return the current value.</param>
        /// <returns></returns>
        public List<Neuron> GetValueFor(Variable variable)
        {
            List<Neuron> iRes = null;
            if (VariableValues.Count > 0 && variable != null)
            {
                var iDict = VariableValues.Peek();
                Debug.Assert(iDict != null);
                if (iDict.TryGetValue(variable.ID, out iRes) == false)
                {
                    var iValue = variable.Value;
                    if (iValue != null)
                    {
                        iRes = Neuron.SolveResultExp(iValue, this).ToList();
                        iDict.Add(variable.ID, iRes);
                    }
                }
            }
            if (iRes == null)
            {
                iRes = new List<Neuron>();
                iRes.Add(Brain.Brain.Current[(ulong)PredefinedNeurons.Empty]);
            }
            return iRes;
        }

        #region Stack Operations

        /// <summary>
        ///     Pushes the specified neuron on it's internal stack.
        /// </summary>
        /// <param name="toPush">The neuron to add.</param>
        public virtual void Push(Neuron toPush)
        {
            fStack.Push(toPush);
        }

        /// <summary>
        ///     Removes the last node from the stack.
        /// </summary>
        /// <remarks>
        ///     You can't do this directly on a stack list cause other types of processors might want
        ///     to do some extra stuf, like a warn a debugger.
        /// </remarks>
        public virtual Neuron Pop()
        {
            if (fStack.Count > 0)
                return fStack.Pop();
            Log.Log.LogError("Processor.Pop", "Internal error: no more items on stack.");
            return null;
        }

        /// <summary>
        ///     Returns the neuron currently at the top of the Neuron execution stack (neurons that still need to be solved).
        /// </summary>
        /// <returns>The current top of the stack or <see cref="PredefinedNeurons.Empty" /> if there is nothing on the stack.</returns>
        public virtual Neuron Peek()
        {
            if (fStack.Count > 0)
                return fStack.Peek();
            return Brain.Brain.Current[(ulong)PredefinedNeurons.Empty];
        }

        #endregion Stack Operations

        #region Control flow

        /// <summary>
        ///     Stops the entire <see cref="Processor.Solve" /> process so that a result node can be returned.
        /// </summary>
        public virtual void Exit()
        {
            using (var iLinksOut = NeuronToSolve.LinksOut)
                CurrentLink = iLinksOut.Items.Count;
            //we assign the exact value, cause if we assign int.maxvalue, we can have an overflow, cause this var is increased at the end of each link solve.
            NextExpID = int.MaxValue;
            //this will put the pointer of the current expression after the last item, garanteed cause this is the max nr of values.
            CurrentState = State.Exit;
        }

        /// <summary>
        ///     Stops the processing of the current link during a <see cref="Processor.Solve" /> and continues with the next one.
        /// </summary>
        public virtual void Continue()
        {
            CurrentState = State.Continue;
        }

        /// <summary>
        ///     goes to the end of the code for solving the current link so that the function stops and continues with the next
        ///     link.
        /// </summary>
        internal void ExitLink()
        {
            NextExpID = int.MaxValue;
            //this will put the pointer of the current expression after the last item, garanteed cause this is the max nr of values.
        }

        /// <summary>
        ///     Stops solving the current neuron.
        /// </summary>
        internal void ExitNeuron()
        {
            using (var iLinksOut = NeuronToSolve.LinksOut)
                CurrentLink = iLinksOut.Items.Count;
            //we assign the exact value, cause if we assign int.maxvalue, we can have an overflow, cause this var is increased at the end of each link solve.
            NextExpID = int.MaxValue;
            //this will put the pointer of the current expression after the last item, garanteed cause this is the max nr of values.
        }

        #endregion Control flow

        #region Sub processor

        /// <summary>
        ///     Returns an array of the specified size, filled with processors.
        /// </summary>
        /// <param name="count">The nr of processors to create.</param>
        /// <returns>An array of newly created processors (not initialized).</returns>
        /// <remarks>
        ///     Descendents should reimplement this so they can give another type (for debugging).
        /// </remarks>
        public virtual Processor[] CreateProcessors(int count)
        {
            var iRes = new Processor[count];
            for (var i = 0; i < count; i++)
                iRes[i] = new Processor();
            return iRes;
        }

        /// <summary>
        ///     Clones this processor to the specified processors, thereby letting the subprocessors know they are clones.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="iRes">The i res.</param>
        internal void CloneProcessors(Processor[] iRes)
        {
            var iNrOfClones = Count;
            var iTemp = new List<Neuron>(fStack);
            //we need a temp list cause all copy functions of stack, do a copy last in, first out, so similar as pop order.
            foreach (var i in iRes)
            {
                //we don't need to copy over execution data cause this
                //is known through the current expression that is link that
                //is evaluated.
                i.SplitData = new SplitData(); //we need to create the split data in order to indicate it is a clone.
                i.SplitData.Processor = i;
                i.NextExpID = NextExpID;
                i.CurrentLink = CurrentLink;
                i.CurrentSin = CurrentSin;
                foreach (var iNeuron in iTemp)
                {
                    var iClone = iNeuron.Duplicate();
                    i.SplitData.Clones.Add(iNeuron.ID, iClone); //we need to store all the copies as well.
                    i.Push(iNeuron);
                }
                var iTempDicts = new List<Dictionary<ulong, List<Neuron>>>(VariableValues);
                //need to make a copy of the stack so we preserve the order of the stack (all 'copy' functions of the stack change the order).
                for (var counter = iTempDicts.Count - 1; counter >= 0; counter--)
                {
                    var iNew = new Dictionary<ulong, List<Neuron>>(iTempDicts[counter]);
                    i.VariableValues.Push(iNew);
                }
                i.SplitValues.InitFrom(SplitValues);
            }
        }

        /// <summary>
        ///     Copies the content of the SplitValues to the specified list. Warning: list should already be locked for
        ///     writing, this function doesn't do this.
        /// </summary>
        /// <remarks>
        ///     Called when a subprocessor is ready and it's result must be copied over.
        /// </remarks>
        /// <param name="list">The neuron list.</param>
        internal void CopyResultsTo(ChildrenAccessor list)
        {
            foreach (var i in SplitValues)
                list.Add(Brain.Brain.Current[i.Key]);
        }

        /// <summary>
        ///     Copies the content of this processor to the specified processor. Warning: list should already be locked for
        ///     writing, this function doesn't do this.
        /// </summary>
        /// <param name="to">Processor to copy these split values to.</param>
        internal void CopyResultsTo(Processor to)
        {
            var iTo = to.SplitValues;
            int iItem;
            foreach (var i in SplitValues)
            {
                if (iTo.TryGetValue(i.Key, out iItem))
                    iTo[i.Key] = Math.Max(iItem, i.Value);
                else
                    iTo.Add(i.Key, i.Value);
            }
        }

        /// <summary>
        ///     Checks if we are a sub processor and if so, if we creted a clone for the
        ///     specified neuron, if so, we return the local copy.
        /// </summary>
        /// <param name="id">The id of the neuron we which to check for clone</param>
        /// <param name="res">The local copy (clone) of the neuron.</param>
        /// <returns></returns>
        internal bool TryFindNeuron(ulong id, out Neuron res)
        {
            res = null;
            if (SplitData != null)
                return SplitData.Clones.TryGetValue(id, out res);
            return false;
        }

        #endregion Sub processor

        #endregion Functions
    }

    #region Enumbs

    /// <summary>
    ///     Used to let inheriters know which list is being executed.
    /// </summary>
    /// <remarks>
    ///     Since we always know at compile time which list we are trying to call,
    ///     we can easely pass this info along without to much overhead to the
    ///     designer for instance.
    /// </remarks>
    public enum ExecListType
    {
        Rules,
        Actions,
        Children
    }

    /// <summary>
    ///     Determins the processing direction that the processor is currently handling.
    /// </summary>
    /// <remarks>
    ///     The process direction always starts with input. This indicates that input data is being processed.
    ///     At a certain point, the brain can execute an <see cref="OutputIntruction" />.  This changes the
    ///     direction of the process to generate output data.
    /// </remarks>
    public enum ProcessDirection
    {
        Input,
        Output
    }

    #endregion Enumbs
}