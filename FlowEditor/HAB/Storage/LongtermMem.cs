﻿using JaStDev.HAB.Brain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace JaStDev.HAB.Storage
{
    /// <summary>
    ///     Implements the <see cref="ILongtermMem" /> interface.  It saves the neurons to xml files in the path specified by
    ///     <see cref="LongtermMem.NeuronsPath" />
    /// </summary>
    [XmlRoot("Memory", Namespace = "http://tiger66639.clan.rip/", IsNullable = false)]
    public class LongtermMem : ILongtermMem
    {
        #region DataPath

        /// <summary>
        ///     Gets/sets the path to the location where all the neurons are saved as files.
        /// </summary>
        /// <remarks>
        ///     the filename is the ID, with extention 'xml'.
        /// </remarks>
        public string DataPath { get; set; }

        #endregion DataPath

        /// <summary>
        ///     Tries to load the neuron with the specified ID
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         If the id can't be found null is returned.
        ///     </para>
        ///     the filename is the ID, with extention 'xml'. the path is defined in <see cref="LongtermMem.NeuronsPath" />
        ///     <para>
        ///         errors are written to the log.
        ///     </para>
        /// </remarks>
        /// <param name="id">The id of the neuron to load.</param>
        /// <returns>The object that was loaded or null if not found.</returns>
        public Neuron GetNeuron(ulong id)
        {
            if (string.IsNullOrEmpty(DataPath) == false)
            {
                var iPath = Path.Combine(DataPath, id + ".xml");
                if (File.Exists(iPath))
                {
                    var iSer = new XmlSerializer(typeof(XmlStore));
                    using (var iReader = new StreamReader(iPath))
                    {
                        var iTemp = iSer.Deserialize(iReader) as XmlStore;
                        if (iTemp != null)
                            return iTemp.Data;
                        return null;
                    }
                }
                if (Settings.LogNeuronNotFoundInLongTermMem)
                    Log.Log.LogWarning("LongtermMemory.GetNeuron", "File not found: " + iPath);
                return null;
            }
            Log.Log.LogError("LongtermMemory.GetNeuron", "Can't search for parked neurons, NeuronsPath not yet defined!");
            return null;
        }

        /// <summary>
        ///     Tries to save the neuron to the long time memory.
        /// </summary>
        /// <remarks>
        ///     This implementation of <see cref="ILongtermMem" /> stores the neuron to an xml file.
        /// </remarks>
        /// <param name="toSave">The object to save.</param>
        public void SaveNeuron(Neuron toSave)
        {
            if (string.IsNullOrEmpty(DataPath) || Directory.Exists(DataPath) == false)
                throw new BrainException(
                    "Can't save the Neuron because there is no valid NeuronPath defined.  Please provide the proper setup info.");
            if (toSave.ID == Neuron.EmptyId)
                throw new BrainException(
                    "Can't save the Neuron because it has no valid ID.  Has it already been added to the Brain?");
            var iSer = new XmlSerializer(typeof(XmlStore));
            var iPath = Path.Combine(DataPath, toSave.ID + ".xml");

            using (TextWriter iWriter = new StreamWriter(iPath))
            {
                var iTemp = new XmlStore { Data = toSave };
                iSer.Serialize(iWriter, iTemp);
            }
            toSave.IsChanged = false; //it's been saved, so let the neuron know.
        }

        /// <summary>
        ///     Removes the specified <see cref="Neuron" /> from the long term memory.
        /// </summary>
        /// <param name="toRemove">The object to remove</param>
        public void RemoveNeuron(Neuron toRemove)
        {
            if (toRemove != null)
                RemoveNeuron(toRemove.ID);
        }

        /// <summary>
        ///     Removes the specified <see cref="Neuron" /> from the long term memory.
        /// </summary>
        /// <param name="toRemove">The id of the neuron to remove.</param>
        public void RemoveNeuron(ulong toRemove)
        {
            if (string.IsNullOrEmpty(DataPath) == false)
            {
                var iPath = Path.Combine(DataPath, toRemove + ".xml");
                File.Delete(iPath);
                //don't need to check if the file exists, cause the function doesn't throw an exception when it doesn't.
            }
        }

        /// <summary>
        ///     Retrieves a list of neurons that form the entry points
        /// </summary>
        /// <param name="type">The type for which to retrieve the data.</param>
        /// <returns></returns>
        public IList<Neuron> GetEntryNeuronsFor(Type type)
        {
            if (DataPath != null)
            {
                var iRes = new List<Neuron>();
                var iPath = Path.Combine(DataPath, "EntryPointsFor" + type + ".dat");
                if (File.Exists(iPath))
                {
                    using (var iStr = new FileStream(iPath, FileMode.Open, FileAccess.Read))
                    {
                        using (var iReader = new BinaryReader(iStr))
                        {
                            while (iStr.Position != iStr.Length)
                            {
                                var iId = iReader.ReadUInt64();
                                try
                                {
                                    Neuron iFound;
                                    if (Brain.Brain.Current.TryFindNeuron(iId, out iFound))
                                        iRes.Add(iFound);
                                    else
                                        Log.Log.LogWarning("LongtermMem.GetEntryNeuronsFor",
                                            string.Format("{0} not found in brain, skipped as entry point.", iId));
                                }
                                catch (Exception e)
                                {
                                    Log.Log.LogWarning("LongtermMem.GetEntryNeuronsFor",
                                        string.Format(
                                            "Failed to find entry point for Sin type: '{0}' with error: {1}.", type, e));
                                }
                            }
                        }
                    }
                }
                return iRes;
            }
            Log.Log.LogError("LongtermMem.GetEntryNeuronsFor",
                string.Format("No NeuronsPath defined: failed to find entry point for Sin type {0}.", type));
            return null;
        }

        /// <summary>
        ///     Stores the list of neurons as the default entry points for the specified type.
        /// </summary>
        /// <param name="list">
        ///     The list of neurons that need to be stored as entry points.  Note that these Neurons are already
        ///     stored as regular neurons in the brain, so you should only store the Id of the neurons.
        /// </param>
        /// <param name="type">The Sin type for which to perform the store.</param>
        public void SaveEntryNeuronsFor(IEnumerable<Neuron> list, Type type)
        {
            var iPath = Path.Combine(DataPath, "EntryPointsFor" + type + ".dat");
            using (var iStr = new FileStream(iPath, FileMode.Create, FileAccess.Write))
            {
                using (var iWriter = new BinaryWriter(iStr))
                {
                    foreach (var i in list)
                        iWriter.Write(i.ID);
                }
            }
        }

        /// <summary>
        ///     Stores the list of neurons as the default entry points for the specified type.
        /// </summary>
        /// <param name="list">
        ///     The list of neurons that need to be stored as entry points.  Note that these Neurons are already
        ///     stored as regular neurons in the brain, so you should only store the Id of the neurons.
        /// </param>
        /// <param name="type">The Sin type for which to perform the store.</param>
        public void SaveEntryNeuronsFor(IEnumerable<ulong> list, Type type)
        {
            var iPath = Path.Combine(DataPath, "EntryPointsFor" + type + ".dat");
            using (var iStr = new FileStream(iPath, FileMode.Create, FileAccess.Write))
            {
                using (var iWriter = new BinaryWriter(iStr))
                {
                    foreach (var i in list)
                        iWriter.Write(i);
                }
            }
        }

        /// <summary>
        ///     Gets extra data associated with the specified type and id from the store.
        /// </summary>
        /// <typeparam name="T">The type of the property value that should be read.</typeparam>
        /// <param name="type">The type for which to get the property value.</param>
        /// <param name="id">The id of the property value that should be retrieved.</param>
        /// <returns></returns>
        /// <remarks>
        ///     <para>
        ///         This is primarely used by sins to retrieve extra information from the brain that they require for proper
        ///         processing.
        ///     </para>
        ///     <para>
        ///         The data should be stored in xml format.
        ///     </para>
        /// </remarks>
        public T GetProperty<T>(Type type, string id) where T : class
        {
            if (DataPath != null)
            {
                var iPath = Path.Combine(DataPath, type + "_" + id + ".xml");
                if (File.Exists(iPath))
                {
                    using (var iStr = new FileStream(iPath, FileMode.Open, FileAccess.Read))
                    {
                        var valueSerializer = new XmlSerializer(typeof(T));
                        return (T)valueSerializer.Deserialize(iStr);
                    }
                }
                return null;
            }
            Log.Log.LogError("Memory.GetProperty",
                string.Format("No NeuronsPath defined: failed to find property values for Sin type {0} - {1}.", type, id));
            return null;
        }

        /// <summary>
        ///     Saves extra data associated with the specified type and id from the store.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="id">The id of the property value that should be saved.</param>
        /// <param name="value">The value that needs to be saved.</param>
        /// <remarks>
        ///     See <see cref="ILongtermMem.GetProperty" /> for more info.
        /// </remarks>
        public void SaveProperty(Type type, string id, object value)
        {
            var iPath = Path.Combine(DataPath, type + "_" + id + ".xml");
            using (var iStr = new FileStream(iPath, FileMode.Create, FileAccess.Write))
            {
                var valueSerializer = new XmlSerializer(value.GetType());
                valueSerializer.Serialize(iStr, value);
            }
        }

        /// <summary>
        ///     Copies all the data to the specified location.  This operation doesn't effect the value of
        ///     <see cref="ILongtermMem.DataPath" />.
        /// </summary>
        /// <param name="loc">The location to copy the data to.</param>
        public void CopyTo(string loc)
        {
            foreach (var i in Directory.GetFiles(DataPath))
                //we copy all the files, don't filter on any file type so we make certain we have everything. (SVN stores in subdir which isn't copied over) -> subdirs of SVN gave problems while deleting the sandbox.
                File.Copy(i, Path.Combine(loc, Path.GetFileName(i)));
        }

        /// <summary>
        ///     Determines whether the specified id exists.
        /// </summary>
        /// <param name="id">The id to look for.</param>
        /// <returns>
        ///     <c>true</c> if the id exists in the db; otherwise, <c>false</c>.
        /// </returns>
        public bool IsExistingID(ulong id)
        {
            if (string.IsNullOrEmpty(DataPath) == false)
            {
                var iPath = Path.Combine(DataPath, id + ".xml");
                return File.Exists(iPath);
            }
            return false;
        }
    }
}