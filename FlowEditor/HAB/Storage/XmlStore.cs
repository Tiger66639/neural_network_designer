﻿using JaStDev.HAB.Brain;
using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace JaStDev.HAB.Storage
{
    /// <summary>
    ///     A wrapper class for <see cref="Neuron" />s. This class allows us to load neurons from disk using xml without
    ///     knowing
    ///     the exact root type (crapp work around).
    /// </summary>
    /// <remarks>
    ///     .net doesn't handle inheritence very well while streaming from xml. You can use the xmlElement attribute on 'Data',
    ///     but that would mean we have to know every possible neuron type in advance, which we don't.  To solve this, we first
    ///     write the type of the class, than the neuron itself.
    /// </remarks>
    public class XmlStore : IXmlSerializable
    {
        /// <summary>
        ///     The value.
        /// </summary>
        public Neuron Data { get; set; }

        /// <summary>
        ///     Reads an xml block from the specified reader.  The block should be for the specified element name.
        ///     The result (content of the element), is returned
        /// </summary>
        /// <remarks>
        ///     this function can only be used for simple types
        /// </remarks>
        /// <typeparam name="T">The type of value that should be read.</typeparam>
        /// <param name="reader">The xmlreader to read the data from.</param>
        /// <param name="name">The name of the element to read.</param>
        /// <returns>The content of the element that was found, cast to the specified type.</returns>
        public static T ReadElement<T>(XmlReader reader, string name) where T : IConvertible
        {
            if (reader.IsEmptyElement == false)
            {
                reader.ReadStartElement(name);
                var iVal = reader.ReadString();
                var iRes = (T)Convert.ChangeType(iVal, typeof(T));
                reader.ReadEndElement();
                return iRes;
            }
            reader.ReadStartElement(name);
            return default(T);
        }

        /// <summary>
        ///     Reads a list of ulong id's where the list is surrounded by the specified element name and each
        ///     item in the list is called 'ID'.
        /// </summary>
        /// <param name="reader">The xml reader that should be used.</param>
        /// <param name="name">The name of the element that surrounds the list.</param>
        /// <param name="list">The list that stores the result ulongs.</param>
        public static void ReadIDList(XmlReader reader, string name, IList<ulong> list)
        {
            //extra info of link.
            var iIsEmpty = reader.IsEmptyElement;
            reader.ReadStartElement(name);
            if (iIsEmpty == false)
            {
                while (reader.NodeType != XmlNodeType.EndElement)
                {
                    reader.ReadStartElement("ID");
                    var iVal = reader.ReadString();
                    reader.ReadEndElement();
                    var iID = ulong.Parse(iVal);
                    list.Add(iID);
                    reader.MoveToContent();
                }
                reader.ReadEndElement();
            }
        }

        /// <summary>
        ///     Reads a list of sub elements surroundeed by the specified element name.  Each sub element is read
        ///     using the callback, that has an extra bool arg.
        /// </summary>
        /// <param name="reader">The xmlreader to use.</param>
        /// <param name="name">The name of the element that surrounds the list.</param>
        /// <param name="action">The action to call each time a sub element is found.</param>
        /// <param name="extraArg">An extra argument, passed to the action</param>
        public static void ReadList(XmlReader reader, string name, Action<XmlReader, bool> action, bool extraArg)
        {
            var iIsEmpty = reader.IsEmptyElement;
            reader.ReadStartElement(name);
            if (iIsEmpty == false)
            {
                while (reader.NodeType != XmlNodeType.EndElement)
                {
                    action(reader, extraArg);
                    reader.MoveToContent();
                }
                reader.ReadEndElement();
            }
        }

        /// <summary>
        ///     Tries to read an xml block from the specified reader.  The block should be for the specified element name.
        ///     The result (content of the element), is returned.  If the block is not found, nothing is done.
        /// </summary>
        /// <typeparam name="T">The type of value that should be read.</typeparam>
        /// <param name="reader">The xmlreader to read the data from.</param>
        /// <param name="name">The name of the element to read.</param>
        /// <param name="val">The value that was read.  This is a return value.</param>
        /// <returns>
        ///     true when there was an element of the specified name, otherwise false.
        /// </returns>
        /// <remarks>
        ///     this function can only be used for simple types
        /// </remarks>
        public static bool TryReadElement<T>(XmlReader reader, string name, ref T val) where T : IConvertible
        {
            if (reader.Name == name)
            {
                if (reader.IsEmptyElement == false)
                {
                    reader.ReadStartElement(name);
                    var iVal = reader.ReadString();
                    var iRes = (T)Convert.ChangeType(iVal, typeof(T));
                    reader.ReadEndElement();
                    val = iRes;
                }
                else
                {
                    reader.ReadStartElement(name);
                    val = default(T);
                }
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Writes the specified value to the xml writer using the specified element to surround it.
        /// </summary>
        /// <typeparam name="T">The type of the value that needs to be saved.</typeparam>
        /// <param name="writer">The xml writer to save the value to.</param>
        /// <param name="name">The name of the xml element that surrounds the value.</param>
        /// <param name="value">The value to save.</param>
        public static void WriteElement<T>(XmlWriter writer, string name, T value) where T : IConvertible
        {
            writer.WriteStartElement(name);
            if (value != null && value.Equals(default(T)) == false) //important for strings: if null: exception.
                writer.WriteString(value.ToString());
            writer.WriteEndElement();
        }

        internal static void WriteIDList(XmlWriter writer, string name, IList<ulong> list)
        {
            writer.WriteStartElement(name);
            if (list != null)
            {
                foreach (var i in list)
                {
                    writer.WriteStartElement("ID");
                    writer.WriteString(i.ToString());
                    writer.WriteEndElement();
                }
            }
            writer.WriteEndElement();
        }

        #region IXmlSerializable Members

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            var wasEmpty = reader.IsEmptyElement;

            reader.Read();
            if (wasEmpty) return;

            reader.ReadStartElement("Type");
            var iFound = reader.ReadString();
            var iType = Type.GetType(iFound, false);
            if (iType == null)
            {
                foreach (var i in Settings.SinAssemblies)
                {
                    iType = i.GetType(iFound, false);
                    if (iType != null)
                        break;
                }
            }
            reader.ReadEndElement();

            var valueSerializer = new XmlSerializer(iType);
            Data = (Neuron)valueSerializer.Deserialize(reader);

            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Type");
            writer.WriteString(Data.GetType().ToString());
            writer.WriteEndElement();

            var valueSerializer = new XmlSerializer(Data.GetType());
            valueSerializer.Serialize(writer, Data);
        }

        #endregion IXmlSerializable Members
    }
}