﻿using JaStDev.HAB.Brain;
using System;
using System.Collections.Generic;

namespace JaStDev.HAB.Storage
{
    /// <summary>
    /// Provides an interface for objects that provide long term storage of <see cref="Neuron"/> objects (sreamed to file or to database,...).
    /// </summary>
    public interface ILongtermMem
    {
        #region Prop

        /// <summary>
        /// Gets or sets the path where the data should be saved to.
        /// </summary>
        /// <value>The directory as a string.</value>
        string DataPath { get; set; }

        #endregion Prop

        /// <summary>
        /// Copies all the data to the specified location.  This operation doesn't effect the value of <see cref="ILongtermMem.DataPath"/>.
        /// </summary>
        /// <param name="loc">The location to copy the data to.</param>
        void CopyTo(string loc);

        /// <summary>
        /// Retrieves a list of neurons that form the entry points for a <see cref="Sin"/>.
        /// </summary>
        /// <remarks>
        /// A sin can only have 1 set of entrypoints.  If more control over data saving is required,
        /// use <see cref="ILongtermMem.GetProperty"/>
        /// </remarks>
        /// <param name="type">The Sin type for which to retrieve the data</param>
        /// <returns></returns>
        IList<Neuron> GetEntryNeuronsFor(Type type);

        /// <summary>
        /// Tries to load the neuron with the specified ID
        /// </summary>
        /// <remarks>
        /// <para>
        /// If the id can't be found null should be returned.
        /// </para>
        /// </remarks>
        /// <param name="id">The id of the neuron to load.</param>
        /// <returns>The object that was loaded or null if not found.</returns>
        Neuron GetNeuron(ulong id);

        /// <summary>
        /// Gets extra data associated with the specified type and id from the store.
        /// </summary>
        /// <remarks>
        /// <para>
        /// This is primarely used by sins to retrieve extra information from the brain that they require for proper processing.
        /// </para>
        /// <para>
        /// The data should be stored in xml format.
        /// </para>
        /// </remarks>
        /// <typeparam name="T">The type of the property value that should be read.</typeparam>
        /// <param name="type">The type for which to get the property value.</param>
        /// <param name="id">The id of the property value that should be retrieved.</param>
        /// <returns></returns>
        T GetProperty<T>(Type type, string id) where T : class;

        /// <summary>
        /// Determines whether the specified id exists.
        /// </summary>
        /// <param name="id">The id to look for.</param>
        /// <returns>
        /// 	<c>true</c> if the id exists in the db; otherwise, <c>false</c>.
        /// </returns>
        bool IsExistingID(ulong id);

        /// <summary>
        /// Removes the specified <see cref="Neuron"/> from the long term memory.
        /// </summary>
        /// <param name="toRemove">The object to remove</param>
        void RemoveNeuron(Neuron toRemove);

        /// <summary>
        /// Removes the specified <see cref="Neuron"/> from the long term memory.
        /// </summary>
        /// <param name="toRemove">The id of the neuron to remove.</param>
        void RemoveNeuron(ulong toRemove);

        /// <summary>
        /// Stores the list of neurons as the default entry points for the specified type.
        /// </summary>
        /// <param name="list">The list of neuron id's that need to be stored as entry points. </param>
        /// <param name="type">The Sin type for which to perform the store.</param>
        void SaveEntryNeuronsFor(IEnumerable<ulong> list, Type type);

        /// <summary>
        /// Stores the list of neurons as the default entry points for the specified type but uses a list of neurons instead of ids.
        /// </summary>
        /// <param name="list">The list of neurons that need to be stored as entry points.  Note that these Neurons are already
        /// stored as regular neurons in the brain, so you should only store the Id of the neurons.</param>
        /// <param name="type">The Sin type for which to perform the store.</param>
        void SaveEntryNeuronsFor(IEnumerable<Neuron> list, Type type);

        /// <summary>
        /// Tries to save the neuron to the long time memory.
        /// </summary>
        /// <param name="toSave">The object to save.</param>
        void SaveNeuron(Neuron toSave);

        /// <summary>
        /// Saves extra data associated with the specified type and id from the store.
        /// </summary>
        /// <remarks>
        /// See <see cref="ILongtermMem.GetProperty"/> for more info.
        /// </remarks>
        /// <param name="type">The type.</param>
        /// <param name="id">The id of the property value that should be saved.</param>
        /// <param name="value">The value that needs to be saved.</param>
        void SaveProperty(Type type, string id, object value);
    }
}