﻿using System.Collections.Generic;
using System.Reflection;

namespace JaStDev.HAB
{
    /// <summary>
    /// Defines the different ways that a neural network can stream it's cashed neurons to disk.
    /// </summary>
    public enum NeuronStorageMode
    {
        /// <summary>
        /// In this mode, all neurons will always be kept in memory once they are loaded. They are not
        /// all automatically loaded at startup, only on an 'as needed' basis.
        /// </summary>
        AlwaysInMem,

        /// <summary>
        /// In this mode, neurons will only remain in memory when they have been modified (and need to be saved).
        /// This is usefull for editors.
        /// </summary>
        StreamWhenPossible,

        /// <summary>
        /// In this mode, they will always be streamed when necessary.  When a neuron was changed, it is saved first.
        /// This mode is the normal run mode.
        /// </summary>
        AlwaysStream
    }

    /// <summary>
    /// Stores all the settings that are currently applicable for the Brain.
    /// </summary>
    /// <remarks>
    /// <para>
    /// This class is provided so an application has a single entry point to
    /// this info (which is ok since there can only be a single brain in an
    /// application), but doesn't force a specific storage mechanisme (such
    /// as seperate xml files or application settings.
    /// </para>
    /// <para>
    /// Changing a setting doesn't update all the objects, the new value will
    /// only be used for new objects.
    /// Automatic update is not supported since these properties should be set
    /// initially at startup and normally don't change.  This can be refactored later on.
    /// </para>
    /// </remarks>
    public class Settings
    {
        /// <summary>
        /// The initial size of the stack used by processors.
        /// </summary>
        public static int InitProcessorStackSize = 40;

        /// <summary>
        /// determins the amount of info is written to the log. 0, means only the critical
        /// items.
        /// </summary>
        public static int LogLevel = 0;

        /// <summary>
        /// Gets or sets a value indicating whether to log if a neuron was not found in long the term mem.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [log neuron not found in long term mem]; otherwise, <c>false</c>.
        /// </value>
        static public bool LogNeuronNotFoundInLongTermMem { get; set; }

        /// <summary>
        /// Gets or sets the list containing assemblies with externally defined sensory interfaces.
        /// </summary>
        /// <value>The list of assemblies containing external sins.</value>
        /// <remarks>
        /// This property is used to find types that correspond to type names.  By using an external
        /// list, we don't create a hard dependency on assemblies that are not always loaded.  For instance,
        /// there could be a wpf implementation and a forms implementation of the Image sin, or a
        /// device might already provide fft data from an audio stream, which also requires a different audio sin.
        /// </remarks>
        static public List<Assembly> SinAssemblies { get; set; }

        /// <summary>
        /// Gets or sets the storage mode used by the <see cref="Brain"/>.
        /// </summary>
        /// <value>The storage mode.</value>
        static public NeuronStorageMode StorageMode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to track neuron access (how many times a neuron is acessed).
        /// </summary>
        /// <remarks>
        /// <para>
        /// When this value is true, the <see cref="Brain"/> will keep track how many times a neuron is retrieved from
        /// the longterm memory.  This info can be used to optimize neuron load.
        /// </para>
        /// <para>
        /// While editing a neural network with an editor however, it is desirable to turn this off since this will change
        /// the state of the project, while the user actually only viewed the value of a neuron.
        /// </para>
        /// </remarks>
        /// <value><c>true</c> if tracking should be turned on; otherwise, <c>false</c>.</value>
        static public bool TrackNeuronAccess { get; set; }
    }
}