﻿using JaStDev.HAB.Processor;

namespace JaStDev.HAB.Sins
{
    /// <summary>
    ///     A base class for all <see cref="Sin" />s that need to create new processors somehow.
    /// </summary>
    public abstract class ProcessorDependentSin : Sin, IProcessorFactory
    {
        /// <summary>
        ///     Creates <see cref="Processor" /> objects. Required to provide debugging fascilities.
        /// </summary>
        private IProcessorFactory fFactory;

        #region Factory

        /// <summary>
        ///     Gets/sets the factory that should be used for creating processors. Use this to determin
        ///     which type of debug processors or other type of processors will be used when a time tick is passed.
        /// </summary>
        /// <remarks>
        ///     The default value, is this object itself.  This uses normall processors.
        /// </remarks>
        public IProcessorFactory Factory
        {
            get
            {
                if (fFactory == null)
                    fFactory = this;
                return fFactory;
            }
            set { fFactory = value; }
        }

        #endregion Factory

        #region IProcessorFactory Members

        /// <summary>
        ///     Gets the processor.
        /// </summary>
        /// <returns></returns>
        public Processor.Processor GetProcessor()
        {
            return new Processor.Processor();
        }

        #endregion IProcessorFactory Members
    }
}