﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Lists;
using JaStDev.HAB.Brain.Thread_syncing;
using JaStDev.HAB.Events;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;

namespace JaStDev.HAB.Sins
{
    #region inner types

    /// <summary>
    /// Defines the way that that the text channel will process input text.
    /// </summary>
    public enum TextSinProcessMode
    {
        /// <summary>
        /// Items are added as TextNeurons, found in the dictionary.  When a word is not found, it is
        /// automatically added to the internal dict.
        /// </summary>
        DictionaryWords,

        /// <summary>
        /// Items are added as a cluster of letters (so never a lookup).  This is analysed further using
        /// more of a visual analysis (size of word, split in 3 parts,...)
        /// </summary>
        ClusterWords,

        /// <summary>
        /// Every letter is sent seperatly.  This algorithm mimics the audio system best.
        /// </summary>
        LetterStream,
    }

    #endregion inner types

    /// <summary>
    /// A Sin that is able to process string data as input/output.
    /// </summary>
    /// <remarks>
    /// <para>
    /// For input: text will be transformed into TextNeurons containing (words/ints/doubles) , letter sequence clusters or individual letters,
    /// depending on the procesing method that is chosen.
    /// </para>
    /// <para>
    /// For output (from the brain to an event): TextNeurons or 'object clusters' that contain a single textneuron or the
    /// <see cref="PredefinedNeurons.BeginTextBlock"/> and <see cref="PredefinedNeurons.EndTextBlock"/> if you need to group words together into
    /// blocks. By default, the TextSin will raise an event for each neuron that it needs to output.  However, when it receives a 'BeginTextBlock',
    /// it will accumulate each string (it doesn't insert spaces or anything, simply add each string to the end of the last result) untill it receives
    /// the EndTextBlock or again the BeginTxtBlock neuron.
    /// </para>
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.ContainsWord, typeof(Neuron))]                                         //create the default neuron we use as initial link types for words we receive as input.
    [NeuronID((ulong)PredefinedNeurons.ContainsDouble, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.ContainsInt, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.LetterSequence, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Letter, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Verb, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Noun, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Adjective, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Adverb, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Preposition, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Conjunction, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Intersection, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Article, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Determiner, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.Complementizer, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.POS, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.BeginTextBlock, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.EndTextBlock, typeof(Neuron))]
    public class TextSin : Sin
    {
        #region Events

        /// <summary>
        /// Raised when the TextSin has got some text it wants to output.
        /// </summary>
        public event OutputEventHandler<string> TextOut;

        #endregion Events

        #region Words

        /// <summary>
        /// Gets the dictionary containing all the known words with their corresponding Neuron id.
        /// </summary>
        /// <remarks>
        /// this is loaded from
        /// </remarks>
        public static IDictionary<string, ulong> Words
        {
            get
            {
                if (fWords == null && Brain.Brain.Current != null)
                {
                    fWords = new DictThreadSafe<string, ulong>();
                    if (Brain.Brain.Current.Storage.DataPath != null)                                               //only try to load if there was a datapath.  No datapath is possible when there is no project loaded yet.
                    {
                        IList<Neuron> iEntryPoints = Brain.Brain.Current.Storage.GetEntryNeuronsFor(typeof(TextSin));
                        if (iEntryPoints != null)
                        {
                            foreach (Neuron i in iEntryPoints)
                            {
                                TextNeuron iText = i as TextNeuron;
                                if (iText != null)
                                    fWords.Add(iText.Text, iText.ID);
                                else
                                    Log.Log.LogError("TextSin.Words", string.Format("Found invalid neuron in entry points: {0} is not a TextNeuron.", i.ToString()));
                            }
                        }
                        else
                            Log.Log.LogError("TextSin.Words", "Can't load dictionary, No Neuron storage path!");
                    }
                }
                else if (Brain.Brain.Current == null)
                    Log.Log.LogError("TextSin.Words", "Can't load dictionray, Brain not yet loaded!");
                return fWords;
            }
        }

        #endregion Words

        /// <summary>
        /// Resets the dictionary content to null.
        /// </summary>
        public static void ResetDict()
        {
            fWords = null;
        }

        /// <summary>
        /// saves the dictionary (if there is any to the neuron entries store.
        /// </summary>
        /// <remarks>
        /// We don't use the flush method for doing this, cuase it is global, should be handled globally.
        /// </remarks>
        public static void SaveDict()
        {
            if (fWords != null && Brain.Brain.Current != null)
                Brain.Brain.Current.Storage.SaveEntryNeuronsFor(fWords.Values, typeof(TextSin));
        }

        /// <summary>
        /// Need to provide an implemntation, does nothing, check <see cref="TextSin.SaveDict"/> for more info.
        /// </summary>
        public override void Flush()
        {
        }

        #region Fields

        private static DictThreadSafe<string, ulong> fWords;

        /// <summary>
        /// used to build up a text from multiple neurons that are inside a BeginTextBlock - EndTExtBlock or BeginTextBLock - BeginTextBlock.
        /// When null, there is no block.
        /// </summary>
        private StringBuilder fToSend;

        /// <summary>
        /// Contains the list of neurons that, together, create the <see cref="TextSin.fToSend"/> value.  When null, there is no text block building.
        /// </summary>
        private List<Neuron> fNeuronsToSend;

        #endregion Fields

        #region Functions

        /// <summary>
        /// Tries to parse the specified string into neurons which can be processed by the processor.
        /// </summary>
        /// <remarks>
        /// Initial implementation always uses the <see cref="ProcessMode.DictionaryWords"/> technique.
        /// </remarks>
        /// <param name="value">The string text to parse.</param>
        /// <param name="processor">The processor to initiate the process with. This should be provided so that visualizers
        /// can hook into it by providing custom processors (like the debuger).</param>
        /// <returns>the neuron that was used as the input event.</returns>
        public Neuron[] Process(string value, Processor.Processor processor)
        {
            return ProcessWithDict(value, processor);
        }

        /// <summary>
        /// Processes the specified value.
        /// </summary>
        /// <param name="value">The string text to parse.</param>
        /// <param name="processor">The processor to initiate the process with. This should be provided so that visualizers
        /// can hook into it by providing custom processors (like the debuger).</param>
        /// <param name="mode">The mode that should be used to process the value</param>
        /// <returns>
        /// the neurons that were used as the input event.
        /// </returns>
        public Neuron[] Process(string value, Processor.Processor processor, TextSinProcessMode mode)
        {
            switch (mode)
            {
                case TextSinProcessMode.DictionaryWords:
                    return ProcessWithDict(value, processor);

                case TextSinProcessMode.ClusterWords:
                    return ProcessWithClusters(value, processor);

                case TextSinProcessMode.LetterStream:
                    return ProcessAsLetterStream(value, processor);

                default:
                    throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Tries to parse the specified string into neurons which can be processed by the processor.
        /// </summary>
        /// <remarks>
        /// Initial implementation uses a very simple string split algorithm combined with a dictionary lookup
        /// to get the text nodes. Numbers and doubles are treated seperatly, in a more convenient fashion.
        /// If a word is not found in the dictionary, a new TextNeuron is made, the processor will try to
        /// figure out more about the word (or if it can't, ask more info about, completely depends on the rules and actions
        /// that are defined.
        /// </remarks>
        /// <param name="value">The string text to parse.</param>
        /// <param name="processor">The processor to initiate the process with. This should be provided so that visualizers
        /// can hook into it by providing custom processors (like the debuger).</param>
        /// <returns>the neurons that were used as the input event.</returns>
        protected virtual Neuron[] ProcessWithDict(string value, Processor.Processor processor)
        {
            if (processor == null)
                throw new ArgumentNullException("processor");
            string[] iWords = value.Split(null);
            IDictionary<string, ulong> iDict = Words;
            if (iWords != null)
            {
                Neuron iToProcess = new Neuron();                                   //this is the neuron we will eventually send to the brain to indicate that the input processing event happened.  This is also the store of the words, so the processor can solve it.
                Brain.Brain.Current.Add(iToProcess);
                foreach (string iWord in iWords)
                {
                    Neuron iNeuron = null;
                    PredefinedNeurons iRelationship = PredefinedNeurons.Empty;
                    iNeuron = TryGetNumber(iWord, ref iRelationship);
                    if (iNeuron == null)
                    {
                        iNeuron = GetWord(iWord);
                        iRelationship = PredefinedNeurons.ContainsWord;
                    }
                    Debug.Assert(iNeuron != null && iRelationship != PredefinedNeurons.Empty);
                    Link iLink = new Link(iNeuron, iToProcess, (ulong)iRelationship);                //that's all that is required for the link, it is automatically added to all correct lists.
                }
                Process(iToProcess, processor, value);
                return new Neuron[] { iToProcess };
            }
            else
                Log.Log.LogError("TextSin.ProcessWithDict", string.Format("Failed to generate impuls for text: '{0}' because the split of the string into words failed.", value));
            return null;
        }

        /// <summary>
        /// Tries to parse the specified string into neurons which can be processed by the processor.
        /// </summary>
        /// <remarks>
        /// This implementation will process each word as a cluster of letters.
        /// </remarks>
        /// <param name="value">The string text to parse.</param>
        /// <param name="processor">The processor to initiate the process with. This should be provided so that visualizers
        /// can hook into it by providing custom processors (like the debuger).</param>
        /// <returns>the neurons that were used as the input event.</returns>
        protected virtual Neuron[] ProcessWithClusters(string value, Processor.Processor processor)
        {
            if (processor == null)
                throw new ArgumentNullException("processor");
            string[] iWords = value.Split(null);
            IDictionary<string, ulong> iDict = Words;
            if (iWords != null)
            {
                KnoledgeNeuron iToProcess = new KnoledgeNeuron();                                   //this is the neuron we will eventually send to the brain to indicate that the input processing event happened.  This is also the store of the words, so the processor can solve it.
                Brain.Brain.Current.Add(iToProcess);
                foreach (string iWord in iWords)
                {
                    NeuronCluster iNeuron = new NeuronCluster();
                    using (ChildrenAccessor iList = iNeuron.ChildrenW)
                    {
                        foreach (char iLetter in iWord)
                        {
                            Neuron iToAdd = GetWord(new string(iLetter, 1));
                            iList.Add(iToAdd.ID);
                        }
                    }
                    Link iLink = new Link(iNeuron, iToProcess, (ulong)PredefinedNeurons.LetterSequence);                //that's all that is required for the link, it is automatically added to all correct lists.
                }
                Process(iToProcess, processor, value);
                return new Neuron[] { iToProcess };
            }
            else
                Log.Log.LogError("TextSin.ProcessWithDict", string.Format("Failed to generate impuls for text: '{0}' because the split of the string into words failed.", value));
            return null;
        }

        /// <summary>
        /// Tries to parse the specified string into neurons which can be processed by the processor.
        /// </summary>
        /// <remarks>
        /// This implementation will process each letter as a seperate event instance.
        /// </remarks>
        /// <param name="value">The string text to parse.</param>
        /// <param name="processor">The processor to initiate the process with. This should be provided so that visualizers
        /// can hook into it by providing custom processors (like the debuger).</param>
        /// <returns>the neurons that were used as the input event.</returns>
        protected virtual Neuron[] ProcessAsLetterStream(string value, Processor.Processor processor)
        {
            if (processor == null)
                throw new ArgumentNullException("processor");
            List<Neuron> iRes = new List<Neuron>();
            Neuron iFrom = new Neuron();                                                        //indicates the event of a new instance of the specified letter.
            Brain.Brain.Current.Add(iFrom);
            iRes.Add(iFrom);
            foreach (char i in value)
            {
                TextNeuron iTo = new TextNeuron(new string(i, 1));                              //the letter to use: always a new one cause we can't create mutliple links between the same neurons with the same meaning.
                Brain.Brain.Current.Add(iTo);
                Link iLink = new Link(iTo, iFrom, (ulong)PredefinedNeurons.Letter);
            }
            Process(iFrom, processor, value);
            return iRes.ToArray();
        }

        /// <summary>
        /// Converts the specified string into a word neuron.  If the word doesn't yet exist, it is created.
        /// </summary>
        /// <returns></returns>
        private Neuron GetWord(string iWord)
        {
            TextNeuron iNeuron;
            ulong iFound;
            IDictionary<string, ulong> iWords = Words;                                        //we do this cause the prop getter 'Words' does extra code, creates the object if needed,..
            if (iWords.TryGetValue(iWord, out iFound) == true)
                iNeuron = Brain.Brain.Current[iFound] as TextNeuron;                                //this should be the case because of how we loaded the dict.
            else
            {
                iNeuron = new TextNeuron() { Text = iWord };
                Brain.Brain.Current.Add(iNeuron);
                iWords.Add(iWord, iNeuron.ID);
            }
            return iNeuron;
        }

        /// <summary>
        /// Checks if the argument can be parsed as a double or int, if so, it creates a neuron for the value and
        /// returns it, otherwise, it returns null.
        /// </summary>
        /// <param name="word">The text that needs to be checked.</param>
        /// <returns>A neuron that contains the double value found in the string, if it was possible to convert it, otherwise null.</returns>
        private Neuron TryGetNumber(string word, ref PredefinedNeurons relationship)
        {
            int iIntRes;
            if (int.TryParse(word, NumberStyles.Any, null, out iIntRes) == true)
            {
                IntNeuron iNeuron = new IntNeuron() { Value = iIntRes };
                Brain.Brain.Current.Add(iNeuron);
                relationship = PredefinedNeurons.ContainsInt;
                return iNeuron;
            }

            double iNr;
            if (double.TryParse(word, NumberStyles.Any, null, out iNr) == true)
            {
                DoubleNeuron iNeuron = new DoubleNeuron() { Value = iNr };
                Brain.Brain.Current.Add(iNeuron);
                relationship = PredefinedNeurons.ContainsDouble;
                return iNeuron;
            }
            return null;
        }

        /// <summary>
        /// Tries to translate the specified neuron to the output type of the Sin and send it to the outside world.
        /// </summary>
        /// <remarks>
        /// <para>
        /// will only perform an output if there are objects monitoring the <see cref="TextSin.TextOut"/> event.
        /// </para>
        /// <para>
        /// This method is called by the <see cref="Brain"/> itself during/after processing of input.
        /// </para>
        /// <para>
        /// Allowed output values are:
        /// - Any value neuron, like TextNeuron (-> text is sent), DoubleNeuron or IntNeuron (-> value is sent).
        /// - An object cluster, in that case, the 'TextNeuron' is extracted
        /// </para>
        /// </remarks>
        public override void Output(Neuron toSend)
        {
            if (toSend.ID == (ulong)PredefinedNeurons.BeginTextBlock)                                       //need to start a new block, check if we need to close a previous one first.
            {
                if (fToSend != null)
                    OnTextOut(fToSend.ToString(), fNeuronsToSend);
                fToSend = new StringBuilder();
                fNeuronsToSend = new List<Neuron>();
            }
            else if (toSend.ID == (ulong)PredefinedNeurons.EndTextBlock)                                    //need to close the current block: send the event + close the block.
            {
                if (fToSend != null)
                    OnTextOut(fToSend.ToString(), fNeuronsToSend);
                fToSend = null;
                fNeuronsToSend = null;
            }
            else
                OutputNeuron(toSend);
        }

        /// <summary>
        /// Outputs a single neuron.
        /// </summary>
        /// <param name="toSend">To send.</param>
        private void OutputNeuron(Neuron toSend)
        {
            string iRes = null;
            NeuronCluster iCluster = toSend as NeuronCluster;
            if (iCluster != null && iCluster.Meaning == (ulong)PredefinedNeurons.Object)
            {
                TextNeuron iFound;
                using (ChildrenAccessor iList = iCluster.Children)
                    iFound = (from i in iList.ConvertTo<Neuron>()
                              where i is TextNeuron
                              select (TextNeuron)i).FirstOrDefault();
                if (iFound != null)
                    iRes = iFound.Text;
            }
            if (iRes == null)
                iRes = toSend.ToString();                                                                             //this will transform any text, double or int and catch any other.
            if (fToSend != null)
            {
                fToSend.Append(iRes);
                fNeuronsToSend.Add(toSend);
            }
            else
            {
                List<Neuron> iList = new List<Neuron>() { toSend };
                OnTextOut(iRes, iList);
            }
        }

        /// <summary>
        /// Called when there is new text output.
        /// </summary>
        /// <param name="value">The value as a string.</param>
        /// <param name="data">The list of neurons that make up the string.</param>
        protected virtual void OnTextOut(string value, IList<Neuron> data)
        {
            if (TextOut != null)
                TextOut(this, new OutputEventArgs<string>() { Value = value, Data = data });
        }

        /// <summary>
        /// Removes the entry point from the sin.
        /// </summary>
        /// <param name="toRemove">To remove.</param>
        /// <remarks>
        /// By default, a <see cref="Sin"/> doesn't need to have entrypoints, so this function doesn't do anything.  Some
        /// sins use dictionaries.  They need to be kept in sync when items are removed.  That's why we have this function.
        /// </remarks>
        protected internal override void RemoveEntryPoint(Neuron toRemove)
        {
            TextNeuron iToRemove = toRemove as TextNeuron;
            if (iToRemove != null && iToRemove.Text != null)
                Words.Remove(iToRemove.Text);
        }

        #endregion Functions
    }
}