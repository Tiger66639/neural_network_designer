﻿using JaStDev.HAB.Brain;
using System;
using System.Collections.Generic;

namespace JaStDev.HAB.Events
{
    /// <summary>
    ///     delegate used for output events without arguments.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void OutputEventHandler(object sender, EventArgs e);

    /// <summary>
    ///     Generic delegate used for output events.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void OutputEventHandler<T>(object sender, OutputEventArgs<T> e);

    /// <summary>
    ///     A default implementation for an EventArgs class used to raise output events by <see cref="Sin" />s.
    /// </summary>
    /// <remarks>
    ///     Sins don't have to use this class (or events for output), this is just a shortcut. It does offer an
    ///     extra property Data, which is a reference to the <see cref="Neuron" /> that triggered the output event.
    /// </remarks>
    /// <typeparam name="T">The type of output data.</typeparam>
    public class OutputEventArgs<T> : EventArgs
    {
        public T Value { get; set; }
        public IList<Neuron> Data { get; set; }
    }
}