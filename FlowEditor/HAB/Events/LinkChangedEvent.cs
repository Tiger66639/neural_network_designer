﻿using JaStDev.HAB.Brain;

namespace JaStDev.HAB.Events
{
    /// <summary>
    ///     Delelate used for events that signals changes in a link.
    /// </summary>
    /// <param name="sender">The object that raised the event.</param>
    /// <param name="e">The arguments for the event.</param>
    public delegate void LinkChangedEventHandler(object sender, LinkChangedEventArgs e);

    /// <summary>
    ///     Identifies the different modifications that can be performed on a link or Neuron (or
    ///     possibly other objects stored by the <see cref="Brain" />).
    /// </summary>
    /// <remarks>
    ///     This enum is used by the <see cref="LinkChangedEventArgs" /> to indicate the
    ///     type of change that occured.
    /// </remarks>
    public enum BrainAction
    {
        /// <summary>
        ///     A new neuron/link was created.
        /// </summary>
        Created,

        /// <summary>
        ///     The object representation of a neuron was replaced by a new one / a link was changed.
        /// </summary>
        /// <remarks>
        ///     This can happen when a neuron's type was changed. for instance, when a neuron is promoted to a cluster.
        /// </remarks>
        Changed,

        /// <summary>
        ///     A neuron/link was removed.
        /// </summary>
        Removed
    }

    /// <summary>
    ///     Event arguments that contain information of how a link was changed and the old/new values.
    /// </summary>
    public class LinkChangedEventArgs : BrainEventArgs
    {
        /// <summary>
        ///     Identifiies the link that was changed.
        /// </summary>
        public Link OriginalSource { get; set; }

        /// <summary>
        ///     Identifies what exactly happened with the link: created, changed or removed.
        /// </summary>
        public BrainAction Action { get; set; }

        /// <summary>
        ///     Identifies the old value for <see cref="Link.From" />, when <see cref="LinkChangedEventArgs.Action" />
        ///     is <see cref="LinkAction.Changed" /> or <see cref="LinkAction.Removed" />.
        /// </summary>
        public ulong OldFrom { get; set; }

        /// <summary>
        ///     Identifies the old value for <see cref="Link.To" />, when <see cref="LinkChangedEventArgs.Action" />
        ///     is <see cref="LinkAction.Changed" /> or <see cref="LinkAction.Removed" />.
        /// </summary>
        public ulong OldTo { get; set; }

        /// <summary>
        ///     Identifies the new value for <see cref="Link.From" />, when <see cref="LinkChangedEventArgs.Action" />
        ///     is <see cref="LinkAction.Changed" /> or <see cref="LinkAction.Created" />.
        /// </summary>
        public ulong NewFrom { get; set; }

        /// <summary>
        ///     Identifies the new value for <see cref="Link.To" />, when <see cref="LinkChangedEventArgs.Action" />
        ///     is <see cref="LinkAction.Changed" /> or <see cref="LinkAction.Created" />.
        /// </summary>
        public ulong NewTo { get; set; }
    }
}