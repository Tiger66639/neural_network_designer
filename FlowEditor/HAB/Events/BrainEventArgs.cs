﻿using System;

namespace JaStDev.HAB.Events
{
    /// <summary>
    /// Base class for all event arguments for events comming from the brain.
    /// </summary>
    public class BrainEventArgs : EventArgs
    {
        #region Fields

        private Processor.Processor fProcessor;

        #endregion Fields

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="BrainEventArgs"/> class.
        /// </summary>
        public BrainEventArgs()
        {
            Processor = HAB.Processor.Processor.CurrentProcessor;
        }

        #endregion ctor

        #region Processor

        /// <summary>
        /// Gets the processor in which the event was triggered.
        /// </summary>
        /// <remarks>
        /// This allows you to check on this value reliably accros threads. (important cause the Current processor is specific to a thread).
        /// </remarks>
        public Processor.Processor Processor
        {
            get { return fProcessor; }
            private set { fProcessor = value; }
        }

        #endregion Processor
    }
}