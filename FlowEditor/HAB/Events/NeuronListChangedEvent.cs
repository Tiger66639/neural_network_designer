﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Lists;
using JaStDev.HAB.Brain.Thread_syncing;
using System;

namespace JaStDev.HAB.Events
{
    /// <summary>
    /// Defines the different types of changes that can be applied to a <see cref="NeuronList"/>
    /// </summary>
    public enum NeuronListChangeAction
    {
        Insert,
        Remove
    }

    /// <summary>
    /// A delegate for events that signals changes in <see cref="Neuron"/>s.
    /// </summary>
    /// <param name="sender">The Neuron who owns the list that's changed.</param>
    /// <param name="e">The event arguments (the actual list that changed can be found here + more data about what changed..</param>
    public delegate void NeuronListChangedEventHandler(object sender, NeuronListChangedEventArgs e);

    /// <summary>
    /// Event arguments used for events that signal changes in <see cref="Neuron"/> objects.
    /// </summary>
    public class NeuronListChangedEventArgs : BrainEventArgs
    {
        private BrainList fList;

        public NeuronListChangedEventArgs() : base()
        {
        }

        /// <summary>
        /// Identifies which neuron was added/removed to/from the list.
        /// </summary>
        public Neuron Item { get; set; }

        /// <summary>
        /// Gets/sets the index at which the change occured.
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Gets/sets the type of change that occured on the list.
        /// </summary>
        public NeuronListChangeAction Action { get; set; }

        /// <summary>
        /// Gets a thread safe accessor for the list that changed.
        /// </summary>
        public NeuronsAccessor List
        {
            get
            {
                return fList.GetAccessor();
            }
        }

        /// <summary>
        /// Gets the type of the list.
        /// </summary>
        /// <remarks>
        /// This is used by the designer to check the type of the list, for redistribution of the event.
        /// </remarks>
        /// <value>The type of the list.</value>
        public Type ListType
        {
            get { return fList.GetType(); }
        }

        /// <summary>
        /// Gets or sets the internal (actual) list.
        /// </summary>
        /// <value>The internal list.</value>
        internal BrainList InternalList
        {
            get { return fList; }
            set { fList = value; }
        }

        /// <summary>
        /// Determines whether this event is raised because an item was changed in the specified <see cref="NeuronCluster"/>.
        /// </summary>
        /// <param name="cluster">The cluster to check.</param>
        /// <returns>
        /// 	<c>true</c> if the event was raised for the list of the cluster; otherwise, <c>false</c>.
        /// </returns>
        public bool IsListFrom(NeuronCluster cluster)
        {
            return cluster.IsChildList(InternalList);
        }
    }
}