﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using System;
using System.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace JaStDev.HAB.Framenet
{
    /// <summary>
    /// Contains all the data for a single frame in framenet.
    /// </summary>
    public class Frame : FrameBase
    {
        #region Fields

        private ObservedCollection<FrameElement> fElements;
        private ObservedCollection<LexUnit> fLexUnits;
        private string fDescription;
        private bool fIsSelected;

        #endregion Fields

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="Frame"/> class.
        /// </summary>
        public Frame()
        {
            fElements = new ObservedCollection<FrameElement>(this);
            fLexUnits = new ObservedCollection<LexUnit>(this);
        }

        #endregion ctor

        #region Description (Definition)

        /// <summary>
        /// Gets/sets the description of the frame.
        /// </summary>
        [XmlElement(Form = XmlSchemaForm.Unqualified, ElementName = "definition")]
        public string Description
        {
            get
            {
                return fDescription;
            }
            set
            {
                OnPropertyChanging("Description", fDescription, value);
                fDescription = value;
                OnPropertyChanged("Description");
            }
        }

        #endregion Description (Definition)

        #region Elements

        /// <summary>
        /// Gets the list of Frame elements
        /// </summary>
        [XmlArray(Form = XmlSchemaForm.Unqualified, ElementName = "fes")]
        [XmlArrayItem("fe", typeof(FrameElement), Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public ObservedCollection<FrameElement> Elements
        {
            get { return fElements; }
        }

        #endregion Elements

        #region LexUnits

        /// <summary>
        /// Gets the list of lexical units in this frame.
        /// </summary>
        [XmlArray(Form = XmlSchemaForm.Unqualified, ElementName = "lexunits")]
        [XmlArrayItem("lexunit", typeof(LexUnit), Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public ObservedCollection<LexUnit> LexUnits
        {
            get { return fLexUnits; }
        }

        #endregion LexUnits

        #region IsSelected

        /// <summary>
        /// Gets/sets the wether the current item is selected or not.
        /// </summary>
        [XmlIgnore]
        public bool IsSelected
        {
            get
            {
                return fIsSelected;
            }
            set
            {
                if (fIsSelected != value)
                {
                    fIsSelected = value;
                    OnPropertyChanged("IsSelected");
                    FrameNet iOwner = Owner as FrameNet;
                    if (iOwner != null)
                    {
                    }
                }
            }
        }

        #endregion IsSelected

        /// <summary>
        /// Imports this instance into the brain.
        /// </summary>
        public NeuronCluster Import(out NeuronCluster lexUnits)
        {
            NeuronCluster iRes = BrainHelper.CreateFrame(out lexUnits);
            foreach (FrameElement i in Elements)
            {
                string[] iSplit = i.Name.Split('.');                                                     //only need the first part (don't need .pos)
                NeuronCluster iNew;
                if (i.WordNetID != 0)
                    iNew = WordNetSin.Default.GetObject(iSplit[0], i.WordNetID);
                else
                {
                    string iPos = iSplit.Length > 1 ? iSplit[1] : null;
                    iNew = GetObject(iSplit[0], i.ID, (ulong)PredefinedNeurons.FrameElementId, iPos);
                }
                AssignCoreType(iNew, i.CoreType);
                using (ChildrenAccessor iList = iRes.ChildrenW)
                    iList.Add(iNew);
            }
            foreach (LexUnit i in LexUnits)
            {
                string[] iSplit = i.Name.Split('.');                                                     //only need the first part (don't need .pos)
                NeuronCluster iNew;
                if (i.Lexemes.Count == 1)
                {
                    if (i.WordNetID != 0)
                        iNew = WordNetSin.Default.GetObject(iSplit[0], i.WordNetID);
                    else if (i.Lexemes[0].WordNetID != 0)
                        iNew = WordNetSin.Default.GetObject(iSplit[0], i.Lexemes[0].WordNetID);
                    else
                        iNew = GetObject(iSplit[0], i.LemmaID, (ulong)PredefinedNeurons.LemmaId, i.POS);
                    using (ChildrenAccessor iList = lexUnits.ChildrenW)
                        iList.Add(iNew);
                }
                else
                {
                    foreach (Lexeme iLexeme in i.Lexemes)
                    {
                        if (iLexeme.WordNetID != 0)
                            iNew = WordNetSin.Default.GetObject(iSplit[0], iLexeme.WordNetID);
                        else
                            iNew = GetObject(iSplit[0], iLexeme.ID, (ulong)PredefinedNeurons.LemmaId, iLexeme.POS);
                        using (ChildrenAccessor iList = lexUnits.ChildrenW)
                            iList.Add(iNew);
                    }
                }
            }
            return iRes;
        }

        /// <summary>
        /// Creates a link with meaning 'FrameImportance' from the item to the neuron representing the specified value.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="value">The value.</param>
        private void AssignCoreType(NeuronCluster item, string value)
        {
            value = value.ToLower();
            ulong iTo = 0;
            if (value == "core")
                iTo = (ulong)PredefinedNeurons.Frame_Core;
            else if (value == "peripheral")
                iTo = (ulong)PredefinedNeurons.Frame_peripheral;
            else if (value == "extra-thematic")
                iTo = (ulong)PredefinedNeurons.Frame_extra_thematic;
            else
                throw new ArgumentException("Invalid core type specified.", "value");
            Link iLink = new Link(Brain.Brain.Current[iTo], item, (ulong)PredefinedNeurons.FrameImportance);
        }

        /// <summary>
        /// Gets the object cluster for the specified text and with the specified frame element id.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="feId">The id value to use as attached value for the cluster.</param>
        /// <param name="pos">The part of speech that should be assigned to the object, if it is a newly created one.</param>
        /// <returns></returns>
        private NeuronCluster GetObject(string text, int feId, ulong meaningID, string pos)
        {
            GetObjectArgs iArgs = new GetObjectArgs();
            iArgs.Text = text;
            iArgs.AttachedInt = feId;
            iArgs.MeaningID = (ulong)PredefinedNeurons.FrameElementId;
            NeuronCluster iRes = BrainHelper.GetObject(iArgs);
            if (iArgs.IsNew == true && pos != null)                                                //still need to assign  the pos if there was any.
            {
                pos = pos.ToLower();
                ulong iTo = 0;
                if (pos == "v" || pos == "verb")
                    iTo = (ulong)PredefinedNeurons.Verb;
                else if (pos == "n" || pos == "noun")
                    iTo = (ulong)PredefinedNeurons.Noun;
                else if (pos == "adv" || pos == "adverb")
                    iTo = (ulong)PredefinedNeurons.Adverb;
                else if (pos == "a" || pos == "adjective")
                    iTo = (ulong)PredefinedNeurons.Adjective;
                else if (pos == "prep" || pos == "preposition")
                    iTo = (ulong)PredefinedNeurons.Preposition;
                else if (pos == "con" || pos == "conjunction")
                    iTo = (ulong)PredefinedNeurons.Conjunction;
                else if (pos == "inter" || pos == "Intersection")
                    iTo = (ulong)PredefinedNeurons.Intersection;
                else
                    throw new ArgumentException(string.Format("Unkown part of speech found (pos): {0}.", pos));
                Link iNew = new Link(Brain.Brain.Current[iTo], iRes, (ulong)PredefinedNeurons.POS);
            }
            return iRes;
        }
    }
}