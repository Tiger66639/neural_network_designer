﻿using System.Xml.Serialization;

namespace JaStDev.HAB.Framenet
{
    [XmlType(TypeName = "semType", Namespace = "")]
    public class SemType : ObservableObject
    {
        #region fields

        private int fID;
        private string fName;

        #endregion fields

        #region ID

        /// <summary>
        /// Gets/sets the id of the semtype
        /// </summary>
        public int ID
        {
            get
            {
                return fID;
            }
            set
            {
                OnPropertyChanging("ID", fID, value);
                fID = value;
                OnPropertyChanged("ID");
            }
        }

        #endregion ID

        #region Name

        /// <summary>
        /// Gets/sets the name of the semtype
        /// </summary>
        public string Name
        {
            get
            {
                return fName;
            }
            set
            {
                OnPropertyChanging("Name", fName, value);
                fName = value;
                OnPropertyChanged("Name");
            }
        }

        #endregion Name
    }
}