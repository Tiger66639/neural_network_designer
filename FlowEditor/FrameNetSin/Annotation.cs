﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace JaStDev.HAB.Framenet
{
    /// <summary>
    /// The annotation info of a lexical unit.
    /// </summary>
    public class Annotation : ObservableObject
    {
        private int fAnnotated;
        private int fTotal;

        #region Annotated

        /// <summary>
        /// Gets/sets the nr of annotated items.
        /// </summary>
        [XmlElement("annotated", Form = XmlSchemaForm.Unqualified)]
        public int Annotated
        {
            get
            {
                return fAnnotated;
            }
            set
            {
                OnPropertyChanging("Annotated", fAnnotated, value);
                fAnnotated = value;
                OnPropertyChanged("Annotated");
            }
        }

        #endregion Annotated

        #region Total

        /// <summary>
        /// Gets/sets the Total nr of annotated items.
        /// </summary>
        [XmlElement("total", Form = XmlSchemaForm.Unqualified)]
        public int Total
        {
            get
            {
                return fTotal;
            }
            set
            {
                OnPropertyChanging("Total", fTotal, value);
                fTotal = value;
                OnPropertyChanged("Total");
            }
        }

        #endregion Total
    }
}