﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace JaStDev.HAB.Framenet
{
    [XmlRoot(ElementName = "lexeme", Namespace = "", IsNullable = false)]
    public class Lexeme : FrameCore
    {
        #region Fields

        private int fID;
        private string fPOS;
        private bool fIsHeadWord;
        private bool fBreakBefore;
        private string fValue;
        private int fWordNetID;

        #endregion Fields

        #region ID

        /// <summary>
        /// Gets/sets the id of the frame.
        /// </summary>
        [XmlAttribute()]
        public int ID
        {
            get
            {
                return fID;
            }
            set
            {
                OnPropertyChanging("ID", fID, value);
                fID = value;
                OnPropertyChanged("ID");
            }
        }

        #endregion ID

        #region POS

        /// <summary>
        /// Gets/sets the part of speech of this lexical unit.
        /// </summary>
        [XmlAttribute("pos")]
        public string POS
        {
            get
            {
                return fPOS;
            }
            set
            {
                OnPropertyChanging("POS", fPOS, value);
                fPOS = value;
                OnPropertyChanged("POS");
            }
        }

        #endregion POS

        #region BreakBefore

        /// <summary>
        /// Gets/sets the wether the break is before or after the item
        /// </summary>
        [XmlAttribute("breakBefore")]
        public bool BreakBefore
        {
            get
            {
                return fBreakBefore;
            }
            set
            {
                OnPropertyChanging("BreakBefore", fBreakBefore, value);
                fBreakBefore = value;
                OnPropertyChanged("BreakBefore");
            }
        }

        #endregion BreakBefore

        #region IsHeadWord

        /// <summary>
        /// Gets/sets the wether this is a headword or not
        /// </summary>
        [XmlAttribute("headword")]
        public bool IsHeadWord
        {
            get
            {
                return fIsHeadWord;
            }
            set
            {
                OnPropertyChanging("IsHeadWord", fIsHeadWord, value);
                fIsHeadWord = value;
                OnPropertyChanged("IsHeadWord");
            }
        }

        #endregion IsHeadWord

        #region Value

        /// <summary>
        /// Gets/sets the string value of the lexeme.
        /// </summary>
        [XmlText()]
        public string Value
        {
            get
            {
                return fValue;
            }
            set
            {
                OnPropertyChanging("Value", fValue, value);
                fValue = value;
                OnPropertyChanged("Value");
            }
        }

        #endregion Value

        #region WordNetID

        /// <summary>
        /// Gets/sets the id of the 'synset' that corresponds to the lemma. (both refer to a single meaning of a word (but where the same meaning can be expressed by different words). ex: bunnet - trunk
        /// </summary>
        [XmlAttribute("synsetId")]
        public int WordNetID
        {
            get
            {
                if (fWordNetID == 0)
                {
                    FrameNet iRoot = Root;
                    fWordNetID = Root.GetWordNetIDFor(ID);
                }
                return fWordNetID;
            }
            set
            {
                OnPropertyChanging("WordNetID", fWordNetID, value);
                fWordNetID = value;
                OnPropertyChanged("WordNetID");
                FrameNet iRoot = Root;
                if (iRoot != null && iRoot.WordNetMapLU != null)
                {
                    iRoot.WordNetMapLU[ID] = value;
                    iRoot.WordNetMapLUChanged = true;
                }
            }
        }

        #endregion WordNetID

        /// <summary>
        /// Gets the list of wordNet ID values that can be related to this lexical unit (because they have the same textual representation).
        /// </summary>
        /// <value>The word net ID values.</value>
        [XmlIgnore]
        public IList<FrameNet.Synset> WordNetIDValues
        {
            get
            {
                List<FrameNet.Synset> iRes = new List<FrameNet.Synset>();
                foreach (wordnetDataSet.WordInfoRow i in WordNetSin.Default.GetWordInfoFor(Value, POS))
                {
                    FrameNet.Synset iNew = new FrameNet.Synset() { ID = i.synsetid, Description = i.definition };
                    iRes.Add(iNew);
                }
                return iRes;
            }
        }
    }
}