﻿using System.Xml.Schema;
using System.Xml.Serialization;

namespace JaStDev.HAB.Framenet
{
    /// <summary>
    /// Base class for frame data elements that have an ID, name, description, date and SemTypes
    /// </summary>
    public class FrameBase : FrameCore
    {
        #region Fields

        private ObservedCollection<SemType> fSemTypes;
        private int fID;
        private string fName;
        private string fDescription;
        private string fDate;

        #endregion Fields

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="FrameBase"/> class.
        /// </summary>
        public FrameBase()
        {
            fSemTypes = new ObservedCollection<SemType>(this);
        }

        #endregion ctor

        #region Description (Definition)

        /// <summary>
        /// Gets/sets the description of the item.
        /// </summary>
        [XmlElement(Form = XmlSchemaForm.Unqualified, ElementName = "definition")]
        public string Description
        {
            get
            {
                return fDescription;
            }
            set
            {
                OnPropertyChanging("Description", fDescription, value);
                fDescription = value;
                OnPropertyChanged("Description");
            }
        }

        #endregion Description (Definition)

        #region SemTypes

        /// <summary>
        /// Gets the list of semantic types associated with this frame.
        /// </summary>
        [XmlElement("semTypes")]
        public ObservedCollection<SemType> SemTypes
        {
            get { return fSemTypes; }
            set { fSemTypes = value; }
        }

        #endregion SemTypes

        #region ID

        /// <summary>
        /// Gets/sets the id of the frame.
        /// </summary>
        [XmlAttribute()]
        public int ID
        {
            get
            {
                return fID;
            }
            set
            {
                OnPropertyChanging("ID", fID, value);
                fID = value;
                OnPropertyChanged("ID");
            }
        }

        #endregion ID

        #region Name

        /// <summary>
        /// Gets/sets the name of the frame
        /// </summary>
        [XmlAttributeAttribute("name")]
        public string Name
        {
            get
            {
                return fName;
            }
            set
            {
                OnPropertyChanging("Name", fName, value);
                fName = value;
                OnPropertyChanged("Name");
            }
        }

        #endregion Name

        #region Date

        /// <summary>
        /// Gets/sets the date that the frame was created
        /// </summary>
        [XmlAttributeAttribute("cDate")]
        public string Date
        {
            get
            {
                return fDate;
            }
            set
            {
                OnPropertyChanging("Date", fDate, value);
                fDate = value;
                OnPropertyChanged("Date");
            }
        }

        #endregion Date
    }
}