﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Threading;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems;
using JaStDev.HAB.Designer.Editors.MindMap;
using JaStDev.HAB.Designer.WPF.BrainEventManagers;
using JaStDev.HAB.Events;
using JaStDev.HAB.Processor;

namespace JaStDev.HAB.Designer.Undo_data
{
//To Finish: 
   /// <summary>
   /// This class is responsible for keeping track of changes to the brain and syncing this with the undo data system. This is
   /// a singleton class.
   /// </summary>
   /// <remarks>
   /// <para>
   /// When the <see cref="Brain"/> has <see cref="Processor"/>s running, it produces a lot of events with all the changes to the
   /// brain.  In response to those changes, some <see cref="MindMap"/>s, <see cref="CodeEditor"/>s or others can produce undo
   /// data because, for instance a neuron was removed that they also depicted. These changes are normally also recorded by the undo
   /// system cause they go the normal channels. However, since the actual brain changes are not recorded since they aren't triggered
   /// by the user, so this undo data becomes invalid.
   /// </para>
   /// <para>
   /// At the same time it is also intresting to be able to undo everything a processor (user input sent to the brain) changed.  This can
   /// be done since all the changes to the brainare sent as events.
   /// </para>
   /// <para>
   /// So that's what this class does: it allows undoing of a 'thought' and it makes certain that user actions are properly recorded, even 
   /// if a processor is running (and also producing undo data through this class)
   /// </para>
   /// </remarks>
   class BrainUndoMonitor : IWeakEventListener
   {

      static BrainUndoMonitor fCurrent;
      Dictionary<Processor.Processor, List<UndoItem>> fUndoData = new Dictionary<Processor.Processor, List<UndoItem>>();              //stores all the undo data generated per processor.

      #region ctor

      /// <summary>
      /// Creates and initializes the singelton.
      /// </summary>
      /// <remarks>
      /// This function must be called before the singleton can be used.  This is to force a specific point
      /// in time for it's creation, which is important to make certain that the events are registered as soon as possible.
      /// </remarks>
      public static void Instantiate()
      {
         fCurrent = new BrainUndoMonitor();
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="BrainUndoMonitor"/> class.
      /// </summary>
      private BrainUndoMonitor()
      {
         LinkChangedEventManager.AddListener(Brain.Brain.Current, this);
         NeuronChangedEventManager.AddListener(Brain.Brain.Current, this);
         NeuronListChangedEventManager.AddListener(Brain.Brain.Current, this);
         WindowMain.UndoStore.PreviewUndoStoreChanged += new UndoStoreHandler(UndoStore_PreviewUndoStoreChanged);
      }

      /// <summary>
      /// Handles the PreviewUndoStoreChanged event of the UndoStore control.
      /// </summary>
      /// <remarks>
      /// We cancel the operation if the undo item is generated from a thread that has a processor assigned (from the execution 
      /// engine).  We do this cause they might cause some problems otherwise.  undoing a run will probably be done by recording
      /// these items some other way?
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="JaStDev.UndoSystem.UndoStoreEventArgs"/> instance containing the event data.</param>
      void UndoStore_PreviewUndoStoreChanged(object sender, UndoStoreEventArgs e)
      {
         e.IsCanceled = Processor.Processor.CurrentProcessor != null;
      }


      #endregion

      /// <summary>
      /// Gets the current instance.
      /// </summary>
      /// <value>The current.</value>
      public static BrainUndoMonitor Current
      {
         get
         {
            if (fCurrent == null)
               throw new InvalidOperationException("BrainUndoMonitor is a singleton class that needs to be instantiated before it can be accessed.");
            return fCurrent;
         }
      }

      /// <summary>
      /// Gets a value indicating whether this instance is instantiated or not.
      /// </summary>
      /// <remarks>
      /// This is usefull to determin since you can't access Current untill it is valid.
      /// </remarks>
      /// <value>
      /// 	<c>true</c> if this instance is instantiated; otherwise, <c>false</c>.
      /// </value>
      public static bool IsInstantiated
      {
         get
         {
            return fCurrent != null;
         }
      }

      #region IWeakEventListener Members

      /// <summary>
      /// Receives events from the centralized event manager.
      /// </summary>
      /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
      /// <param name="sender">Object that originated the event.</param>
      /// <param name="e">Event data.</param>
      /// <returns>
      /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
      /// </returns>
      public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(LinkChangedEventManager))
         {
            LinkChanged(sender, (LinkChangedEventArgs)e);
            return true;
         }
         else if (managerType == typeof(NeuronChangedEventManager))
         {
            NeuronChanged(sender, (NeuronChangedEventArgs)e);
            return true;
         }
         else if (managerType == typeof(NeuronListChangedEventManager))
         {
            NeuronListChanged(sender, (NeuronListChangedEventArgs)e);
            return true;
         }
         else
            return false;
      }

      private void NeuronListChanged(object sender, NeuronListChangedEventArgs e)
      {
         if (e.Processor != null)                                                                     //if there is no current processor, it has been triggered by the designer, which is handled elsewhere.  
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<object, NeuronListChangedEventArgs>(InternalNeuronListChanged), sender, new object[] { e });
      }

      private void InternalNeuronListChanged(object sender, NeuronListChangedEventArgs e)
      {
      }

      private void LinkChanged(object sender, LinkChangedEventArgs linkChangedEventArgs)
      {
         //throw new NotImplementedException();
      }

      private void NeuronChanged(object sender, NeuronChangedEventArgs neuronChangedEventArgs)
      {
         //throw new NotImplementedException();
      }


      #endregion
   }
}
