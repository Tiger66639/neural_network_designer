﻿using System;
using System.Collections.Generic;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using JaStDev.HAB.Events;

namespace JaStDev.HAB.Designer.Undo_data
{
   /// <summary>
   /// An undo data item that is used to handle changes with neurons themselves, like delete or create.
   /// </summary>
   /// <remarks>
   /// Sometimes, these events are handled because the underlying collection containers will also update
   /// the <see cref="Brain"/>.  Often, the designer provides direct commands to create or destroy
   /// neurons, these commands also need to be undoable.
   /// </remarks>
   public class NeuronUndoItem: BrainUndoItem
   {
      #region Inner types

      /// <summary>
      /// Stores info about a link so that it can be recreated easily.
      /// </summary>
      class LinkInfo
      {
         /// <summary>
         /// Gets/sets the meaning that was assigned to the link. 
         /// </summary>
         /// <remarks>
         /// This is a neuron, not an id because this is undo data.  If the other neuron would also be destroyed, it's id could 
         /// get lost.  This is not a problem for other parts of the system, but the undo data needs to correctly relink to the
         /// same neuron (in case they also got deleted and restored), not id.
         /// </remarks>
         public Neuron Meaning { get; set; }
         /// <summary>
         /// Gets/sets the other side of the link.
         /// </summary>
         public Neuron To { get; set; }
         /// <summary>
         /// Gets/sets the list of info items that were assigned to the link.
         /// </summary>
         public List<Neuron> Info { get; set; }
      }

      /// <summary>
      /// Stores info about the clusters in which an item that was removed belonged.
      /// </summary>
      class ClusterInfo
      {
         /// <summary>
         /// Gets or sets the cluster in which the item belonged.
         /// </summary>
         /// <value>The cluster.</value>
         public NeuronCluster Cluster { get; set; }

         /// <summary>
         /// Gets or sets the index at which the item was located in the child list.
         /// </summary>
         /// <value>The index.</value>
         public int Index { get; set; }
      }

      #endregion

      #region Fields

      List<LinkInfo> fLinksIn = new List<LinkInfo>(); 
      List<LinkInfo> fLinksOut = new List<LinkInfo>();
      List<ClusterInfo> fClusteredBy = new List<ClusterInfo>();                                       //keeps track of all the clusters that this belonged to, so we can recreate.
      List<Neuron> fChildren;                                                                         //if the neuron is a cluster, we also keep track of all the children it had, so we can reassign them.
      Neuron fNeuron;

      #endregion

      #region Prop
    
      
      #region Neuron

      /// <summary>
      /// Gets or sets the neuron that is involved with the change.
      /// </summary>
      /// <remarks>
      /// we keep a reference to this object so we can use the same physical object again without having to create a new one.
      /// </remarks>
      /// <value>The neuron.</value>
      public Neuron Neuron
      {
         get
         {
            return fNeuron;
         }
         set
         {
            fNeuron = value;
            if (value != null)
            {
               using (LinksAccessor iLinksIn = value.LinksIn)
               {
                  foreach (Link i in iLinksIn.Items)
                  {
                     LinkInfo iNew = new LinkInfo() { Meaning = i.Meaning, To = i.From };
                     if (iNew.Info != null)
                     {
                        using (NeuronsAccessor iList = i.Info)
                           iNew.Info = iList.ConvertTo<Neuron>();
                     }
                     fLinksIn.Add(iNew);
                  }
               }
               using (LinksAccessor iLinksOut = value.LinksOut)
               {
                  foreach (Link i in iLinksOut.Items)
                  {
                     LinkInfo iNew = new LinkInfo() { Meaning = i.Meaning, To = i.To };
                     if (iNew.Info != null)
                     {
                        using (NeuronsAccessor iList = i.Info)
                           iNew.Info = iList.ConvertTo<Neuron>();
                     }
                     fLinksOut.Add(iNew);
                  }
               }
               using (NeuronsAccessor iClusteredByAcc = value.ClusteredBy)
               {
                  foreach (ulong i in iClusteredByAcc.Items)                                                        //keep track of the clusters that own this neuron as a child so we can add again later on.
                  {
                     NeuronCluster iCluster = Brain.Brain.Current[i] as NeuronCluster;
                     if (iCluster != null)
                     {
                        ClusterInfo iInfo;
                        using (ChildrenAccessor iList = iCluster.Children)
                           iInfo = new ClusterInfo() { Cluster = iCluster, Index = iList.IndexOf(value.ID) };
                        fClusteredBy.Add(iInfo);
                     }
                  }
               }
               if (fNeuron is NeuronCluster)
               {
                  fChildren = new List<Neuron>();
                  using (ChildrenAccessor iList = ((NeuronCluster)fNeuron).Children)
                     foreach (ulong i in iList)
                        fChildren.Add(Brain.Brain.Current[i]);
               }
            }
            else
            {
               fLinksIn.Clear();
               fLinksOut.Clear();
               fChildren = null;
            }
         }
      }

      #endregion


      /// <summary>
      /// Gets or sets the ID of the neuron that was changed.
      /// </summary>
      /// <remarks>
      /// we need this cause a delete for instance, will erase the id in the neuron.
      /// </remarks>
      /// <value>The ID.</value>
      public ulong ID { get; set; }



      #endregion

      #region Overrides
      /// <summary>
      /// Performs all the actions stored in the undo item, thereby undoing the action.
      /// </summary>
      /// <param name="caller">The undo managaer that is calling this method.</param>
      public override void Execute(UndoStore caller)
      {
         switch (Action)
         {
            case BrainAction.Created:                                                  //we need to remove it from the brain.
               NeuronUndoItem iUndoData = new NeuronUndoItem() { Neuron = Neuron, ID = Neuron.ID, Action = BrainAction.Removed };
               WindowMain.UndoStore.AddCustomUndoItem(iUndoData);
               Brain.Brain.Current.Delete(Neuron);
               break;
            case BrainAction.Removed:                                                  //we need to recreate it, preferebly using the same object with the same ID.  Normally, this is not a problem, but it can be (when the brain was running and it already consumed the ID).  In that case we use a new ID, which is still ok, cause most lists will create new viewer objects anyway (like mindmap and code editor).
               RestoreNeuron();                                                        //important to call restore last, otherwise the recreation of the links will mess up the order of some things.
               iUndoData = new NeuronUndoItem() { Neuron = Neuron, ID = Neuron.ID, Action = BrainAction.Created };
               WindowMain.UndoStore.AddCustomUndoItem(iUndoData);
               break;
            default:
               throw new InvalidOperationException(string.Format("Unsuported BrainAction type: {0}.", Action));
         }
      }

      /// <summary>
      /// Restores the neuron's internal data like links and cluster relationships.
      /// </summary>
      private void RestoreNeuron()
      {
         Brain.Brain.Current.Add(Neuron, ID);
         foreach (ClusterInfo i in fClusteredBy)
         {
            if (i.Cluster.ID != Neuron.EmptyId)
               using (ChildrenAccessor iList = i.Cluster.ChildrenW)
                  iList.Insert(i.Index, Neuron);
         }
         foreach (LinkInfo i in fLinksIn)
         {
            if (i.To.ID != Neuron.EmptyId)
            {
               Link iLink = new Link(Neuron, i.To, i.Meaning);
               if (i.Info != null)
                  using (LinkInfoAccessor iList = iLink.InfoW)
                     foreach (Neuron ii in i.Info)
                        iList.Add(ii);
            }
         }
         foreach (LinkInfo i in fLinksOut)
         {
            if (i.To.ID != Neuron.EmptyId)
            {
               Link iLink = new Link(i.To, Neuron, i.Meaning);
               if (i.Info != null)
                  using (LinkInfoAccessor iList = iLink.InfoW)
                     foreach (Neuron ii in i.Info)
                        iList.Add(ii);
            }
         }
         if (fChildren != null)
         {
            NeuronCluster iCluster = (NeuronCluster)Neuron;
            using (ChildrenAccessor iList = iCluster.ChildrenW)
               foreach (Neuron i in fChildren)
                  iList.Add(i);
         }
      }

      /// <summary>
      /// Checks if this UndoItem has the same target(s) as the undo item to compare.
      /// </summary>
      /// <param name="toCompare">An undo item to compare this undo item with.</param>
      /// <returns>
      /// True, if they have the same target, otherwise false.
      /// </returns>
      /// <remarks>
      /// This is used by the undo system to check for duplicate entries which don't need to be stored if they are within
      /// the <see cref="P:JaStDev.UndoSystem.UndoStore.MaxSecBetweenUndo"/> limit.
      /// </remarks>
      public override bool HasSameTarget(UndoItem toCompare)
      {
         NeuronUndoItem iToCompare = toCompare as NeuronUndoItem;
         if (iToCompare != null)
            return iToCompare.ID == ID && iToCompare.Action == Action;                             //we also compare action cause if they are different, we actually have a different object (if add followed delete and delete would be considered the same as the add, it would not be stored, which we don't want.
         else
            return false;
      }

      /// <summary>
      /// Checks if this undo item contains data from the specified source object.
      /// </summary>
      /// <param name="source">The object to check for.</param>
      /// <returns>
      /// True if this object contains undo data for the specified object.
      /// </returns>
      /// <remarks>
      /// This function is used by the undo system to clean up undo data from items that are no longer available.  This can happen for
      /// undo data generated from user interface controls which are no longer valid (loaded).
      /// </remarks>
      public override bool StoresDataFrom(object source)
      {
         return Neuron == source;
      } 
      #endregion
   }
}
