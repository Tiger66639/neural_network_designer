﻿using System;
using System.Collections.Generic;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using JaStDev.HAB.Events;

namespace JaStDev.HAB.Designer.Undo_data
{
   /// <summary>
   /// An undo data item that is used to handle creation and destruction of links (changes are handled differently).
   /// </summary>
   public class LinkUndoItem: BrainUndoItem
   {

      
      Link fItem;
      #region Item

      /// <summary>
      /// Gets or sets the actual link.
      /// </summary>
      /// <remarks>
      /// When the link is created, we can store a reference to the exact link.  When the link was destroyed,
      /// this property has become invalid.
      /// </remarks>
      /// <value>The item.</value>
      public Link Item
      {
         get
         {
            return fItem;
         }
         set
         {
            fItem = value;
            if (value != null)                                                   //we always store the internals of the link in case it was a delete, in that case, the link object looses it's info.
            {
               From = value.From;
               To = value.To;
               Meaning = value.Meaning;
               using (NeuronsAccessor iList = value.Info)
                  Info = iList.ConvertTo<Neuron>();
            }
         }
      }

      #endregion

      /// <summary>
      /// Gets or sets the From part of the link
      /// </summary>
      /// <value>From.</value>
      Neuron From { get; set; }


      /// <summary>
      /// Gets or sets the To part of the link.
      /// </summary>
      /// <value>To.</value>
      Neuron To { get; set; }

      /// <summary>
      /// Gets or sets the meaning part of the link.
      /// </summary>
      /// <value>The meaning.</value>
      Neuron Meaning { get; set; }

      /// <summary>
      /// Gets or sets the info part of the link.
      /// </summary>
      /// <value>The info.</value>
      List<Neuron> Info { get; set; }


      #region overrides
      /// <summary>
      /// Performs all the actions stored in the undo item, thereby undoing the action.
      /// </summary>
      /// <param name="caller">The undo managaer that is calling this method.</param>
      public override void Execute(UndoStore caller)
      {
         switch (Action)
         {
            case BrainAction.Created:
               LinkUndoItem iUndoData = new LinkUndoItem() { Action = BrainAction.Removed, Item = Item };
               WindowMain.UndoStore.AddCustomUndoItem(iUndoData);
               Item.Destroy();
               break;
            case BrainAction.Removed:
               if (To.ID != Neuron.EmptyId && From.ID != Neuron.EmptyId && Meaning.ID != Neuron.EmptyId)
               {
                  Item.Recreate(From, To, Meaning);                                                      //we re-use the same link object, this will allow the UI object to remain the same (works better with the undo system). 
                  if (Info != null)
                  {
                     using (LinkInfoAccessor iList = Item.InfoW)
                        foreach (Neuron i in Info)
                           if (i.ID != Neuron.EmptyId)
                              iList.Add(i);
                  }
                  iUndoData = new LinkUndoItem() { Action = BrainAction.Created, Item = Item };
                  WindowMain.UndoStore.AddCustomUndoItem(iUndoData);
               }
               break;
            default:
               throw new InvalidOperationException(string.Format("Unsuported BrainAction type: {0}.", Action));
         }
      }

      /// <summary>
      /// Checks if this UndoItem has the same target(s) as the undo item to compare.
      /// </summary>
      /// <param name="toCompare">An undo item to compare this undo item with.</param>
      /// <returns>
      /// True, if they have the same target, otherwise false.
      /// </returns>
      /// <remarks>
      /// This is used by the undo system to check for duplicate entries which don't need to be stored if they are within
      /// the <see cref="UndoStore.MaxSecBetweenUndo"/> limit.
      /// </remarks>
      public override bool HasSameTarget(UndoItem toCompare)
      {
         LinkUndoItem iLink = toCompare as LinkUndoItem;
         if (iLink != null)
            return iLink.From == From && iLink.To == To && iLink.Meaning == Meaning;
         else
            return false;
      }

      /// <summary>
      /// Checks if this undo item contains data from the specified source object.
      /// </summary>
      /// <param name="source">The object to check for.</param>
      /// <returns>
      /// True if this object contains undo data for the specified object.
      /// </returns>
      public override bool StoresDataFrom(object source)
      {
         return source == Meaning || source == To || source == From;
      } 
      #endregion
   }
}
