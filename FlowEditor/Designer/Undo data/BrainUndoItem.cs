﻿using JaStDev.HAB.Events;

namespace JaStDev.HAB.Designer.Undo_data
{
   /// <summary>
   /// A base class for custom undo items for the brain.
   /// </summary>
   public abstract class BrainUndoItem : UndoItem
   {
      /// <summary>
      /// Gets or sets the action that was undertaken: a delete or create.
      /// </summary>
      /// <remarks>
      /// When undoing the action, the reverse of this should be done.
      /// </remarks>
      /// <value>The action.</value>
      public BrainAction Action { get; set; }
   }
}
