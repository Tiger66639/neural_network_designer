﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace JaStDev.HAB.Designer
{
    /// <summary>
    ///     A class containing various utility functions.
    /// </summary>
    internal class Helper
    {
        #region Text

        /// <summary>
        ///     copies the richtext data from one flow to the other.
        /// </summary>
        /// <param name="to"></param>
        /// <param name="from"></param>
        public static void CopyRichTextTo(FlowDocument to, FlowDocument from)
        {
            TextRange range;
            using (var stream = new MemoryStream())
            {
                range = new TextRange(from.ContentStart, from.ContentEnd);
                range.Save(stream, DataFormats.Xaml);
                range = new TextRange(to.ContentStart, to.ContentEnd);
                range.Load(stream, DataFormats.Xaml);
            }
        }

        /// <summary>
        ///     Creates a FlowDocument with all the default values set.
        /// </summary>
        /// <returns></returns>
        public static FlowDocument CreateDefaultFlowDoc()
        {
            var iRes = new FlowDocument();
            var iPar = new Paragraph();
            var iRun = new Run();
            iRun.FontSize = 12.0;
            iRun.FontFamily = new FontFamily("Segoe UI");
            iPar.Inlines.Add(iRun);
            iRes.Blocks.Add(iPar);
            return iRes;
        }

        #endregion

        #region Math

        /// <summary>
        ///     Calculates the angle between 2 points (if you were to draw a horizontal line through b and vertical one through a).
        /// </summary>
        /// <remarks>
        ///     See 'corner calculation explanation.design' (in images dir of project) for more info. Note, this function presumes
        ///     0 degrees to be vertical: right, horizontal = center.
        /// </remarks>
        /// <param name="a">The first point, becomes the center point.</param>
        /// <param name="b">The second point</param>
        /// <returns>the angle in degrees between the 2 points.</returns>
        public static double GetAnlge(Point a, Point b)
        {
            //we need to keep track of the quadrant that b is in compared to a cause the default function only
            //returns the value for the first one.
            if (b.X >= a.X && b.Y <= a.Y)
                return Math.Atan(Math.Abs(b.Y - a.Y)/Math.Abs(b.X - a.X))*(180/Math.PI);
            if (b.X < a.X && b.Y <= a.Y)
                return Math.Atan(Math.Abs(b.X - a.X)/Math.Abs(b.Y - a.Y))*(180/Math.PI) + 90;
            if (b.X <= a.X && b.Y > a.Y)
                return Math.Atan(Math.Abs(b.Y - a.Y)/Math.Abs(b.X - a.X))*(180/Math.PI) + 180;
            return Math.Atan(Math.Abs(b.X - a.X)/Math.Abs(b.Y - a.Y))*(180/Math.PI) + 270;
        }

        /// <summary>
        ///     Calculates the size of 1 side in a triangle if only the size of 1 side is known + the angle between that
        ///     side and an adjacent one (so on the opposite side of the side you want to calculate.
        /// </summary>
        public static double GetLengthOfOpposite(double angle, double adjacent)
        {
            return Math.Tan(angle/(180/Math.PI))*adjacent; //need to convert the angle to radial first.
        }

        /// <summary>
        ///     Calculates the size of an side in a triangle when the length of the opposite side is known and
        ///     the corner between the side you want to know and an adjacent side.
        /// </summary>
        public static double GetLengthOfAdjacent(double angle, double opposite)
        {
            return opposite/Math.Tan(angle/(180/Math.PI));
        }

        #endregion
    }
}