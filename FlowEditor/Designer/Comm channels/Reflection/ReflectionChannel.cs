﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using JaStDev.HAB.Designer.Tools.Debugger;

namespace JaStDev.HAB.Designer.Comm_channels.Reflection
{
   /// <summary>
   /// Contains all the data for managing a <see cref="ReflectionSin"/>
   /// </summary>
   public class ReflectionChannel : CommChannel
   {
      #region fields

      ObservedCollection<AssemblyData> fAssemblies;
      ObservedCollection<DebugNeuron> fOpCodes;

      #endregion

      #region ctor
      /// <summary>
      /// Initializes a new instance of the <see cref="ReflectionChannel"/> class.
      /// </summary>
      public ReflectionChannel()
      {
         fAssemblies = new ObservedCollection<AssemblyData>(this);
         fOpCodes = new ObservedCollection<DebugNeuron>(this);
      }

      #endregion

      #region prop

      #region Assemblies

      /// <summary>
      /// Gets the list of available assemblies
      /// </summary>
      [XmlIgnore]
      public ObservedCollection<AssemblyData> Assemblies
      {
         get { return fAssemblies; }
      }

      #endregion

      #region AssemblyNames

      /// <summary>
      /// Gets/sets the list of assemblies that is loaded.
      /// </summary>
      /// <remarks>
      /// This is primarely used for xml streaming.
      /// </remarks>
      public List<string> AssemblyNames
      {
         get
         {
            return (from i in Assemblies select i.Name).ToList();
         }
         set
         {
            Assemblies.Clear();
            foreach (string i in value)
               Assemblies.Add(new AssemblyData(i));
         }
      }

      #endregion

      #region OpCodes

      /// <summary>
      /// Gets the list of neurons used as op codes in the form of debug neurons.
      /// </summary>
      [XmlIgnore]
      public ObservedCollection<DebugNeuron> OpCodes
      {
         get { return fOpCodes; }
      }

      #endregion

      #region OpCodeIds

      /// <summary>
      /// Gets/sets the list of neuron id's used as opcodes by the current channel
      /// </summary>
      /// <remarks>
      /// Primarely for streaming.
      /// </remarks>
      public List<ulong> OpCodeIds
      {
         get
         {
            return (from i in OpCodes select i.Item.ID).ToList();
         }
         set
         {
            OpCodes.Clear();
            if (value != null)
            {
               foreach (ulong i in value)
               {
                  Neuron iFound;
                  if (Brain.Brain.Current.TryFindNeuron(i, out iFound) == true)
                     OpCodes.Add(new DebugNeuron(iFound));
                  else
                     Log.Log.LogError("ReflectionChannel.OpCodeId's", string.Format("removed opcode from list: id '{0}' not found in brain.", i));
               }
            }
         }
      }

      #endregion

      #endregion

      #region Functions

      

      #endregion

      /// <summary>
      /// Loads all the op codes.
      /// </summary>
      internal void LoadOpCodes()
      {
         ReflectionSin iSin = (ReflectionSin)Sin;
         OpCodes.Clear();
         foreach (NeuronCluster i in iSin.CreateOpcodes())
         {
            TextNeuron iText;
            using (ChildrenAccessor iList = i.Children)
               iText = (from ii in iList.ConvertTo<Neuron>() where ii is TextNeuron select (TextNeuron)ii).FirstOrDefault(); //we get the text neuron so we can build a display tag.
            DebugNeuron iWrap = new DebugNeuron(i);
            OpCodes.Add(iWrap);
            iWrap.NeuronInfo.DisplayTitle = iText.Text;
         }
      }

      /// <summary>
      /// Unloads all the op codes.
      /// </summary>
      internal void UnloadOpCodes()
      {
         Log.Log.LogError("ReflectionChannel.UnloadOpCodes", "There's still a bug here: the opcodes are created as objects, but deleted as neurons: the clusters and textneurons still remain in the brain!!");
         foreach (DebugNeuron i in OpCodes)
            BrainHelper.DeleteObject((NeuronCluster)i.Item);
         OpCodes.Clear();
      }
   }
}
