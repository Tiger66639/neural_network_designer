﻿using System;
using System.Reflection;

namespace JaStDev.HAB.Designer.Comm_channels.Reflection
{
   /// <summary>
   /// Manages all the data of a single type.
   /// </summary>
   public class TypeData : ReflectionData
   {
      #region Fields
      ObservedCollection<FunctionData> fFunctions;
      Type fType;
      #endregion

      #region ctor

      public TypeData(Type type)
      {
         fFunctions = new ObservedCollection<FunctionData>(this);
         Type = type;
      }

      #endregion

      #region Properties
      #region Children

      /// <summary>
      /// Gets the list of all the types found in the namespace
      /// </summary>
      public ObservedCollection<FunctionData> Children
      {
         get { return fFunctions; }
      }

      #endregion

      
      #region Type

      /// <summary>
      /// Gets the type that this object wraps.
      /// </summary>
      public Type Type
      {
         get { return fType; }
         internal set 
         { 
            fType = value;
            if (value != null)
            {
               Name = value.Name;
               foreach (MethodInfo i in value.GetMethods(BindingFlags.Static | BindingFlags.Public))
               {
                  bool iAllValue = true;
                  foreach (ParameterInfo iPar in i.GetParameters())
                  {
                     if (iPar.ParameterType.IsValueType == false && iPar.ParameterType.Name != "String")
                     {
                        iAllValue = false;
                        break;
                     }
                  }
                  if (iAllValue == true)
                  {
                     FunctionData iFunc = new FunctionData(i);
                     Children.Add(iFunc);
                  }
               }
            }
            else
               Name = null;
            OnPropertyChanged("Type");
            OnLoadedChanged();
         }
      }

      #endregion

      #region IsLoaded
      /// <summary>
      /// Gets or sets the function(s)/children are loaded.
      /// </summary>
      /// <value>
      /// 	<c>true</c>: all the children are loaded - the function is loaded
      /// <c>false</c>: none of the children are loaded - the function is not loaded.
      /// <c>null</c>: some loaded - invalid.
      /// </value>
      /// <remarks>
      /// Setting to null is not processed.
      /// </remarks>
      public override bool? IsLoaded
      {
         get
         {
            if (Children.Count > 0)
            {
               bool? iRes = Children[0].IsLoaded;
               for (int i = 1; i < Children.Count; i++)
               {
                  if (Children[i].IsLoaded != iRes)
                     return null;
               }
               return iRes;
            }
            else
               return null;
         }
         set
         {
            if (value.HasValue == true)
            {
               foreach (FunctionData i in Children)
                  i.IsLoaded = value;
            }
            OnLoadedChanged();
         }
      }
      #endregion 
      #endregion
   }
}
