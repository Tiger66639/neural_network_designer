﻿namespace JaStDev.HAB.Designer.Comm_channels.Reflection
{
   /// <summary>
   /// Contains all the types of a single namespace in an assembly formatted to be displayed in a <see cref="ReflectionChannelView"/>.
   /// </summary>
   public class NamespaceData : ReflectionData
   {
      ObservedCollection<TypeData> fChildren;

      #region ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="NamespaceData"/> class.
      /// </summary>
      public NamespaceData()
      {
         fChildren = new ObservedCollection<TypeData>(this);
      }

      #endregion

      #region Children

      /// <summary>
      /// Gets the list of all the types found in the namespace
      /// </summary>
      public ObservedCollection<TypeData> Children
      {
         get { return fChildren; }
      }

      #endregion


      /// <summary>
      /// Gets or sets the function(s)/children are loaded.
      /// </summary>
      /// <value>
      /// 	<c>true</c>: all the children are loaded - the function is loaded
      /// <c>false</c>: none of the children are loaded - the function is not loaded.
      /// <c>null</c>: some loaded - invalid.
      /// </value>
      /// <remarks>
      /// Setting to null is not processed.
      /// </remarks>
      public override bool? IsLoaded
      {
         get
         {
            if (Children.Count > 0)
            {
               bool? iRes = Children[0].IsLoaded;
               for (int i = 1; i < Children.Count; i++)
               {
                  if (Children[i].IsLoaded != iRes)
                     return null;
               }
               return iRes;
            }
            else
               return null;
         }
         set
         {
            if (value.HasValue == true)
            {
               foreach (TypeData i in Children)
                  i.IsLoaded = value;
               OnLoadedChanged();
            }
         }
      }
   }
}
