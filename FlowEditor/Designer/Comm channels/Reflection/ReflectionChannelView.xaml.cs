﻿using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;

namespace JaStDev.HAB.Designer.Comm_channels.Reflection
{
   /// <summary>
   /// Interaction logic for ReflectionChannelView.xaml
   /// </summary>
   public partial class ReflectionChannelView : UserControl
   {
      public ReflectionChannelView()
      {
         InitializeComponent();
      }

      /// <summary>
      /// Handles the Click event of the BtnLoadCach control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void BtnLoadCach_Click(object sender, RoutedEventArgs e)
      {
         Dialogs.DlgAssemblyFromGac iDlg = new Dialogs.DlgAssemblyFromGac();
         bool? iRes = iDlg.ShowDialog();
         if (iRes.HasValue == true && iRes.Value == true)
            LoadAssembly(iDlg.SelectedItem);
      }

      /// <summary>
      /// Handles the Click event of the BtnLoadFile control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void BtnLoadFile_Click(object sender, RoutedEventArgs e)
      {
         OpenFileDialog iDlg = new OpenFileDialog();
         iDlg.Filter = "Executables(*.exe;*.dll)|*.dll;*.exe|Libraries|*.dll|Applications|*.exe|All(*.*)|*.*";
         iDlg.Multiselect = true;
         bool? iRes = iDlg.ShowDialog(Window.GetWindow(this));
         if (iRes.HasValue == true && iRes.Value == true)
         {
            foreach (string iFile in iDlg.FileNames)
               LoadAssembly(iFile);
         }
      }

      /// <summary>
      /// Loads the assembly in the brain.
      /// </summary>
      /// <param name="asm">The assembly to load.</param>
      /// <param name="file">The file from where it came.</param>
      private void LoadAssembly(string file)
      {
         ReflectionChannel iChannel = (ReflectionChannel)DataContext;
         Assembly iAsm = Assembly.LoadFile(file);

         AssemblyData iData = new AssemblyData();
         iData.Assembly = iAsm;
         iData.Name = iAsm.GetName().Name + " (" + file + ")";
         if (iData.Children.Count > 0)
            iChannel.Assemblies.Add(iData);
         else
            MessageBox.Show("No public types with static public methods found in assembly!");
      }

      /// <summary>
      /// Handles the Checked event of the BtnToggleOpcodes control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void BtnToggleOpcodes_Checked(object sender, RoutedEventArgs e)
      {
         ReflectionChannel iChannel = (ReflectionChannel)DataContext;
         iChannel.LoadOpCodes();
      }

      /// <summary>
      /// Handles the Unchecked event of the BtnToggleOpcodes control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void BtnToggleOpcodes_Unchecked(object sender, RoutedEventArgs e)
      {
         ReflectionChannel iChannel = (ReflectionChannel)DataContext;
         iChannel.UnloadOpCodes();
      }
   }
}
