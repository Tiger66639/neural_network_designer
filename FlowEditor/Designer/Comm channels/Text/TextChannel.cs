﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Speech.Synthesis;
using System.Windows.Threading;
using System.Xml.Serialization;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Tools.Debugger;
using JaStDev.HAB.Events;
using JaStDev.HAB.Sins;

namespace JaStDev.HAB.Designer.Comm_channels.Text
{
   /// <summary>
   /// A wrapper class for the <see cref="TextSin"/>. Provides some extra props for visualisation.
   /// </summary>
   public class TextChannel: CommChannel
   {

      #region fields
      ObservableCollection<TextChanellTrackingData> fDialogData = new ObservableCollection<TextChanellTrackingData>();
      double fInputSplitterValue;
      double fDialogSplitterValue;
      double fNeuronSplitterValue;
      bool fViewIn;
      bool fViewOut;
      SpeechSynthesizer fSpeaker;                                                                        //when set, we need to produce audio out.
      //bool fAudioOn;
      ObservableCollection<TextChanellTrackingData> fOutputData = new ObservableCollection<TextChanellTrackingData>();
      ObservableCollection<TextChanellTrackingData> fInputData = new ObservableCollection<TextChanellTrackingData>();
      //DebugNeuron fInputNeuron;
      TextSinProcessMode? fProcessMode;
      #endregion

      #region ctor
      /// <summary>
      /// Initializes a new instance of the <see cref="TextChannel"/> class.
      /// </summary>
      public TextChannel()
      {
      } 
      #endregion

      #region props
      #region InputSplitterValue

      /// <summary>
      /// Gets/sets the size that should be used for the splitter that controls the input dialog.
      /// </summary>
      public double InputSplitterValue
      {
         get
         {
            return fInputSplitterValue;
         }
         set
         {
            fInputSplitterValue = value;
            OnPropertyChanged("InputSplitterValue");
         }
      }

      #endregion

      #region DialogSplitterValue

      /// <summary>
      /// Gets/sets the size that should be used for the splitter that controls the conversation dialog.
      /// </summary>
      public double DialogSplitterValue
      {
         get
         {
            return fDialogSplitterValue;
         }
         set
         {
            fDialogSplitterValue = value;
            OnPropertyChanged("DialogSplitterValue");
         }
      }

      #endregion

      #region NeuronSplitterValue

      /// <summary>
      /// Gets/sets the size that should be used for the splitter that seperates the 2 neuron displays.
      /// </summary>
      public double NeuronSplitterValue
      {
         get
         {
            return fNeuronSplitterValue;
         }
         set
         {
            fNeuronSplitterValue = value;
            OnPropertyChanged("NeuronSplitterValue");
         }
      }

      #endregion

      #region AudioOn

      /// <summary>
      /// Gets/sets wether oudio output is activated or not.
      /// </summary>
      public bool AudioOn
      {
         get
         {
            return fSpeaker != null;
         }
         set
         {
            if (AudioOn != value)
            {
               if (value == true)
                  fSpeaker = new SpeechSynthesizer();
               else
                  fSpeaker = null;
               OnPropertyChanged("AudioOn");
            }
         }
      }

      #endregion

      #region ViewIn

      /// <summary>
      /// Gets/sets if the input node view should be visible.
      /// </summary>
      public bool ViewIn
      {
         get
         {
            return fViewIn;
         }
         set
         {
            fViewIn = value;
            OnPropertyChanged("ViewIn");
         }
      }

      #endregion 
      
      #region ViewOut

      /// <summary>
      /// Gets/sets if the output node view should be visible.
      /// </summary>
      public bool ViewOut
      {
         get
         {
            return fViewOut;
         }
         set
         {
            fViewOut = value;
            OnPropertyChanged("ViewOut");
         }
      }

      #endregion

      #region OutputData

      /// <summary>
      /// Gets the list of tracking data rendered for output events (text received from the brain).
      /// </summary>
      [XmlIgnore]
      public ObservableCollection<TextChanellTrackingData> OutputData
      {
         get { return fOutputData; }
      }

      #endregion
      
      #region InputData

      /// <summary>
      /// Gets the list of tracking data rendered for input events (text sent to the brain).
      /// </summary>
      [XmlIgnore]
      public ObservableCollection<TextChanellTrackingData> InputData
      {
         get { return fInputData; }
      }

      #endregion


      
      #region DialogData

      /// <summary>
      /// Gets the data containing all the dialog statements.
      /// </summary>
      [XmlIgnore]
      public ObservableCollection<TextChanellTrackingData> DialogData
      {
         get { return fDialogData; }
      }

      #endregion

      #region ProcessMode

      /// <summary>
      /// Gets/sets the the processing mode that should be used.
      /// </summary>
      public TextSinProcessMode? ProcessMode
      {
         get
         {
            return fProcessMode;
         }
         set
         {
            OnPropertyChanging("ProcessMode", fProcessMode, value);
            fProcessMode = value;
            OnPropertyChanged("ProcessMode");
         }
      }

      #endregion

      #endregion

      #region functions

      /// <summary>
      /// Sets the Sensory interface that this object is a wrapper of.
      /// </summary>
      /// <param name="sin">The sin.</param>
      protected override void SetSin(Sin sin)
      {
         TextSin iSin = sin as TextSin;
         if(iSin != null)
            iSin.TextOut += new OutputEventHandler<string>(Sin_TextOut);
         base.SetSin(sin);
      }

      /// <summary>
      /// Handles the TextOut event of the Sin control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="OutputEventArgs{T}.String&gt;"/> instance containing the event data.</param>
      void Sin_TextOut(object sender, OutputEventArgs<string> e)
      {
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<OutputEventArgs<string>>(HandleIncommingData), e);
      }

      void HandleIncommingData(OutputEventArgs<string> e)
      {
         TextChanellTrackingData iData = new TextChanellTrackingData(e.Value, e.Data.ToArray(), "PC");
         OutputData.Add(iData);
         DialogData.Add(iData);
         if (fSpeaker != null)
            fSpeaker.SpeakAsync(e.Value);
      }

      #endregion

      /// <summary>
      /// Sends the text to sin.
      /// </summary>
      /// <param name="value">The value.</param>
      internal void SendTextToSin(string value)
      {
         if (string.IsNullOrEmpty(value) == false)
         {
            TextSin iSin = (TextSin)Sin;
            if (iSin != null)
            {
               Neuron[] iRes;
               if (ProcessMode.HasValue == false)
                  iRes = iSin.Process(value, ProcessorManager.Current.GetProcessor());
               else
                  iRes = iSin.Process(value, ProcessorManager.Current.GetProcessor(), ProcessMode.Value);
               if (iRes != null)
               {
                  TextChanellTrackingData iData = new TextChanellTrackingData(value, iRes, "You");
                  InputData.Add(iData);
                  DialogData.Add(iData);
               }
               else
                  Log.Log.LogError("TextChannel.SendText", "Sin failed to return a neuron for the output text!");
            }
         }
      }
   }
}
