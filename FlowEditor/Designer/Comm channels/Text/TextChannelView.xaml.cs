﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using JaStDev.HAB.Sins;

namespace JaStDev.HAB.Designer.Comm_channels.Text
{
   /// <summary>
   /// This class displays the info of a <see cref="TextSin"/> and provides interaction with it.
   /// </summary>
   public partial class TextChannelView : UserControl
   {
      #region ctor-dtor
      public TextChannelView()
      {
         InitializeComponent();
      }

      #endregion

      #region Prop



      /// <summary>
      /// Gets the <see cref="TextSin"/> that we provide a view for.
      /// </summary>
      /// <remarks>
      /// This is retrieved when the <see cref="TextChannel.Channel"/> property is assigned.
      /// </remarks>
      public TextSin Sin { get; private set; }

      #endregion

      #region functions


      private void BtnSend_Click(object sender, RoutedEventArgs e)
      {
         SendTextToSin();
      }

      private void TxtSend_PrvKeyDown(object sender, KeyEventArgs e)
      {
         if (e.Key == Key.Return)
            SendTextToSin();
      }

      /// <summary>
      /// Tries to send the text in the send text box to the Sin (if there is anything to send).
      /// </summary>
      private void SendTextToSin()
      {
         TextChannel iChannel = (TextChannel)DataContext;
         iChannel.SendTextToSin(TxtToSend.Text);
      }

      

      #endregion

   }
}
