﻿
using JaStDev.HAB.Designer.Comm_channels.Audio;

namespace JaStDev.HAB.Designer.Comm_channels.Resources
{
   /// <summary>
   /// A <see cref="ResourceReference"/> for the <see cref="AudioChannel"/>.
   /// </summary>
   public class AudioReference: ResourceReference
   {
   }
}
