﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;

namespace JaStDev.HAB.Designer.Comm_channels.Resources
{
   /// <summary>
   /// A control that is able to manage a list of <see cref="ResourceReference"/> objects. It is able to
   /// remove and add items to the list.
   /// </summary>
   public partial class CtrlResourceManager : UserControl
   {
      public CtrlResourceManager()
      {
         InitializeComponent();
      }

      #region Items

      /// <summary>
      /// Items Dependency Property
      /// </summary>
      public static readonly DependencyProperty ItemsProperty =
          DependencyProperty.Register("Items", typeof(IList), typeof(CtrlResourceManager),
              new FrameworkPropertyMetadata(null));

      /// <summary>
      /// Gets or sets the Items property.  This dependency property 
      /// indicates the resource values to display.
      /// </summary>
      /// <remarks>
      /// For best results, the list assigned should implement <see cref="ICollectionChanged"/>.
      /// </remarks> 
      public IList Items
      {
         get { return (IList)GetValue(ItemsProperty); }
         set { SetValue(ItemsProperty, value); }
      }

      #endregion

      #region ResourceType

      /// <summary>
      /// ResourceType Dependency Property
      /// </summary>
      public static readonly DependencyProperty ResourceTypeProperty =
          DependencyProperty.Register("ResourceType", typeof(Type), typeof(CtrlResourceManager),
              new FrameworkPropertyMetadata((Type)null));

      /// <summary>
      /// Gets or sets the ResourceType property.  This dependency property 
      /// indicates the type of resource that should be created.  This should be a descendent of 
      /// <see cref="ResourceReference"/>.
      /// </summary>
      public Type ResourceType
      {
         get { return (Type)GetValue(ResourceTypeProperty); }
         set { SetValue(ResourceTypeProperty, value); }
      }

      #endregion

      #region HasSelection

      /// <summary>
      /// HasSelection Dependency Property
      /// </summary>
      public static readonly DependencyProperty HasSelectionProperty =
          DependencyProperty.Register("HasSelection", typeof(bool), typeof(CtrlResourceManager),
              new FrameworkPropertyMetadata((bool)false));

      /// <summary>
      /// Gets or sets the HasSelection property.  This dependency property 
      /// indicates wether there are items selected or not.
      /// </summary>
      public bool HasSelection
      {
         get { return (bool)GetValue(HasSelectionProperty); }
         set { SetValue(HasSelectionProperty, value); }
      }

      #endregion

      #region DialogFilter

      /// <summary>
      /// DialogFilter Dependency Property
      /// </summary>
      public static readonly DependencyProperty DialogFilterProperty =
          DependencyProperty.Register("DialogFilter", typeof(string), typeof(CtrlResourceManager),
              new FrameworkPropertyMetadata((string)null));

      /// <summary>
      /// Gets or sets the DialogFilter property.  This dependency property 
      /// indicates the filter string to use by the OpenFileDialog for adding new resources.
      /// </summary>
      public string DialogFilter
      {
         get { return (string)GetValue(DialogFilterProperty); }
         set { SetValue(DialogFilterProperty, value); }
      }

      #endregion



      /// <summary>
      /// Handles the Click event of the Button control.
      /// </summary>
      /// <remarks>
      /// Adds one or more new resource items to the list.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void AddBtn_Click(object sender, RoutedEventArgs e)
      {
         OpenFileDialog iDlg = new OpenFileDialog();
         iDlg.Multiselect = true;
         iDlg.Filter = DialogFilter;
         bool? iRes = iDlg.ShowDialog(Window.GetWindow(this));
         if (iRes.HasValue == true && iRes.Value == true)
         {
            IList iItems = Items;
            Debug.Assert(iItems != null);
            foreach (string i in iDlg.FileNames)
            {
               ResourceReference iNew = (ResourceReference)Activator.CreateInstance(ResourceType);
               iNew.FileName = i;
               iItems.Add(iNew);
            }
         }
      }

      /// <summary>
      /// Handles the SelectionChanged event of the LstItems control.
      /// </summary>
      /// <remarks>
      /// Update if there is a selection or not.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
      private void LstItems_SelectionChanged(object sender, SelectionChangedEventArgs e)
      {
         HasSelection = LstItems.SelectedItems.Count > 0;
      }




   }
}
