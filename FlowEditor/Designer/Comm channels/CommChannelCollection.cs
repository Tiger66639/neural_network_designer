﻿using System;
using System.Windows;
using System.Windows.Threading;
using JaStDev.HAB.Designer.WPF.BrainEventManagers;
using JaStDev.HAB.Events;
using JaStDev.HAB.Sins;

namespace JaStDev.HAB.Designer.Comm_channels
{
    /// <summary>
    /// A collection of communication channels.
    /// </summary>
    /// <remarks>
    /// Monitors the brain for changes in the neurons, so that objects may be removed when they no longer are sins.
    /// </remarks>
    public class CommChannelCollection : ObservedCollection<CommChannel>, IWeakEventListener
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommChannelCollection"/> class.
        /// </summary>
        public CommChannelCollection()
        {
            NeuronChangedEventManager.AddListener(Brain.Brain.Current, this);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommChannelCollection"/> class.
        /// </summary>
        /// <param name="owner">The owner.</param>
        public CommChannelCollection(BrainData.BrainData owner) : base(owner)
        {
            NeuronChangedEventManager.AddListener(Brain.Brain.Current, this);
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="CommChannelCollection"/> is reclaimed by garbage collection.
        /// </summary>
        ~CommChannelCollection()
        {
            NeuronChangedEventManager.RemoveListener(Brain.Brain.Current, this);
        }

        #region IWeakEventListener Members

        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            if (managerType == typeof(NeuronChangedEventManager))
            {
                App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<NeuronChangedEventArgs>(NeuronChanged), (NeuronChangedEventArgs)e);
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Handles the neuronchanged event handler.
        /// </summary>
        /// <remarks>
        /// Checks if a neuron changed, if so, and it is contained in our list, than we must update the item in the list
        /// </remarks>
        /// <param name="sender">The sender.</param>
        /// <param name="neuronChangedEventArgs">The <see cref="NeuronChangedEventArgs"/> instance containing the event data.</param>
        private void NeuronChanged(NeuronChangedEventArgs e)
        {
            if (e.Action == BrainAction.Removed || (e.Action == BrainAction.Changed && !(e is NeuronPropChangedEventArgs) && !(e.NewValue is Sin)))                //only need to remove the item if the intire neuron was changed (not a prop) and the new neuron is not a sin.
            {
                CommChannel iFound = (from i in this where i.NeuronID == e.OriginalSource.ID select i).FirstOrDefault();
                if (iFound != null)
                    this.Remove(iFound);
            }
        }

        #endregion IWeakEventListener Members
    }
}