﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace JaStDev.HAB.Designer.Comm_channels.Wordnet
{
   /// <summary>
   /// Interaction logic for WordNetChannelView.xaml
   /// </summary>
   public partial class WordNetChannelView : UserControl
   {
      public WordNetChannelView()
      {
         InitializeComponent();
      }

      /// <summary>
      /// Handles the Click event of the BtnSend control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void BtnLoadAll_Click(object sender, RoutedEventArgs e)
      {
         MessageBoxResult iMRes = MessageBox.Show(string.Format("Load all available WordNet data into the network for '{0}'?", TxtInput.Text), "Import from WordNet", MessageBoxButton.YesNo, MessageBoxImage.Question);
         if (iMRes == MessageBoxResult.Yes)
         {
            //we call the load in anohter thread so that the UI stays responsive.
            Action<string> iFunc = new Action<string>(WordNetSin.Default.Load);
            iFunc.BeginInvoke(TxtInput.Text, null, null);
         }
      }

      private void TxtInput_KeyDown(object sender, KeyEventArgs e)
      {
         if (e.Key == Key.Enter)
            BtnSearch_Click(sender, null);
      }

      /// <summary>
      /// Handles the Click event of the BtnSearch control.
      /// </summary>
      /// <remarks>
      /// Displays all the available data frm wordnet for the current word in the view for the selected relationship.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void BtnSearch_Click(object sender, RoutedEventArgs e)
      {
         WordNetChannel iChannel = (WordNetChannel)DataContext;
         iChannel.LoadChildrenFor(TxtInput.Text);
      }

   }
}
