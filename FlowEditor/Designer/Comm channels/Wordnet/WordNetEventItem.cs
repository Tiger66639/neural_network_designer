﻿using System;

namespace JaStDev.HAB.Designer.Comm_channels.Wordnet
{
   /// <summary>
   /// Stores the information that was gathered when an event came in from the <see cref="WordNetSin"/>.
   /// </summary>
   public class WordNetEventItem
   {
      public WordNetEventItem()
      {
         Time = DateTime.Now;
      }

      /// <summary>
      /// Gets or sets the text that should be displayed to disgribe the item.
      /// </summary>
      /// <value>The text.</value>
      public string Text { get; set; }

      /// <summary>
      /// Gets or sets the event args that were associated with the item.
      /// </summary>
      /// <value>The event args.</value>
      public object EventArgs { get; set; }

      /// <summary>
      /// Gets or sets the time at which the event was registered.
      /// </summary>
      /// <value>The time.</value>
      public DateTime Time { get; set; }
   }
}
