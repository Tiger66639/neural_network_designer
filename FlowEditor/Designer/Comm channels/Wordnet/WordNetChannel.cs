﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using System.Xml.Serialization;
using JaStDev.HAB.Designer.BrainData;
using JaStDev.HAB.Designer.WPF.BrainEventManagers;
using JaStDev.HAB.Designer.WPF.WordNetEventManagers;
using JaStDev.HAB.Events;
using JaStDev.HAB.wordnetDataSetTableAdapters;

namespace JaStDev.HAB.Designer.Comm_channels.Wordnet
{
   /// <summary>
   /// Stores all the data generated from wordnet + registers it with the designer so that they are correctly labeled.
   /// </summary>
   public class WordNetChannel : CommChannel, IWeakEventListener, IWordNetItemOwner
   {
      #region Fields

      ObservableCollection<WordNetEventItem> fEventItems = new ObservableCollection<WordNetEventItem>();
      ObservedCollection<WordNetItem> fChildren;
      int fSelectedRelationship;
      bool fIsRecursiveRelationship;
      string fSearchText;
      bool fIncludeCompoundWords;
      /// <summary>
      /// The id assigned to the 'synonyms' relationship. this is not defined in wordnet 'as such'.
      /// </summary>
      public const int SYNONYMSID = -2; 

      #endregion

      #region ctor-dtor

      /// <summary>
      /// Initializes a new instance of the <see cref="WordNetData"/> class.
      /// </summary>
      public WordNetChannel()
      {
         fChildren = new ObservedCollection<WordNetItem>(this);
         CompoundObjectCreatedEventManager.AddListener(WordNetSin.Default, this);
         ObjectCreatedEventManager.AddListener(WordNetSin.Default, this);
         RelationshipCreatedEventManager.AddListener(WordNetSin.Default, this);
         RelationshipTypeCreatedEventManager.AddListener(WordNetSin.Default, this);
         WordNetStartedEventManager.AddListener(WordNetSin.Default, this);
         WordNetFinishedEventManager.AddListener(WordNetSin.Default, this);
         NeuronChangedEventManager.AddListener(Brain.Brain.Current, this);

         WordNetSin.Default.IncludeDescription = true;
         WordNetSin.Default.IncludeCompoundWords = IncludeCompoundWords;
      }


      /// <summary>
      /// Releases unmanaged resources and performs other cleanup operations before the
      /// <see cref="WordNetData"/> is reclaimed by garbage collection.
      /// </summary>
      ~WordNetChannel()
      {
         CompoundObjectCreatedEventManager.RemoveListener(WordNetSin.Default, this);
         ObjectCreatedEventManager.RemoveListener(WordNetSin.Default, this);
         RelationshipCreatedEventManager.RemoveListener(WordNetSin.Default, this);
         RelationshipTypeCreatedEventManager.RemoveListener(WordNetSin.Default, this);
         WordNetStartedEventManager.RemoveListener(WordNetSin.Default, this);
         WordNetFinishedEventManager.RemoveListener(WordNetSin.Default, this);
         NeuronChangedEventManager.RemoveListener(Brain.Brain.Current, this);
      }

      #endregion

      #region prop

      
      #region EventItems

      /// <summary>
      /// Gets the list of data that was stored for each event that occured in the WordNetSin.
      /// </summary>
      [XmlIgnore]
      public ObservableCollection<WordNetEventItem> EventItems
      {
         get { return fEventItems; }
      }

      #endregion

      #region IWordNetItemOwner Members

      [XmlIgnore]
      public ObservableCollection<WordNetItem> Children
      {
         get { return fChildren; }
      }

      #endregion


      #region SelectedRelationship

      /// <summary>
      /// Gets/sets the ID of the currently selected relationhip, which determins the children that are displayed.
      /// </summary>
      /// <remarks>
      /// This is not the index, but the actual value.
      /// </remarks>
      public int SelectedRelationship
      {
         get
         {
            return fSelectedRelationship;
         }
         set
         {
            if (value != fSelectedRelationship)
            {
               fSelectedRelationship = value;
               OnPropertyChanged("SelectedRelationship");
               RelationshipsTableAdapter iRelDB = new RelationshipsTableAdapter();
               IsRecursiveRelationship = iRelDB.IsRecursive(value) == "Y" ? true : false;
               if (fSearchText != null)
                  LoadChildrenFor(fSearchText);
            }
         }
      }

      #endregion

      #region IsRecursiveRelationship

      /// <summary>
      /// Gets wether the currently selected relationship is recusrive
      /// </summary>
      [XmlIgnore]
      public bool IsRecursiveRelationship
      {
         get { return fIsRecursiveRelationship; }
         internal set { fIsRecursiveRelationship = value; }
      }

      #endregion

      
      #region Relationships

      /// <summary>
      /// Gets the list of available relationships.  This is the entire list of relationships as defined by wordnet, augmented
      /// with the list of synonyms (same words in the synset).
      /// </summary>
      [XmlIgnore]
      public List<WordNetRelationship> Relationships
      {
         get
         {
            RelationshipsTableAdapter iRelAdapater = new RelationshipsTableAdapter();
            wordnetDataSet.RelationshipsDataTable iRelData = iRelAdapater.GetData();
            List<WordNetRelationship> iRes = (from i in iRelData
                                              select new WordNetRelationship()
                                              {
                                                 ID = i.linkid,
                                                 Name = i.name,
                                                 Recurses = i.recurses == "Y" ? true : false
                                              }).ToList();
            iRes.Add(new WordNetRelationship()
            {
               ID = SYNONYMSID,
               Name = "Synonyms",
               Recurses = false
            });
            return iRes;
         }
      }
      #endregion

      #region IncludeCompoundWords

      /// <summary>
      /// Gets/sets wether to include compound words in the import and search, or not.
      /// </summary>
      public bool IncludeCompoundWords
      {
         get
         {
            return fIncludeCompoundWords;
         }
         set
         {
            if (fIncludeCompoundWords != value)
            {
               fIncludeCompoundWords = value;
               OnPropertyChanged("IncludeCompoundWords");
               WordNetSin.Default.IncludeCompoundWords = value;
               LoadChildrenFor(fSearchText);                                              //redo the search so that the items are removed/included in the display as well.
            }
         }
      }

      #endregion

      #endregion

      #region IWeakEventListener Members

      /// <summary>
      /// Receives events from the centralized event manager.
      /// </summary>
      /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
      /// <param name="sender">Object that originated the event.</param>
      /// <param name="e">Event data.</param>
      /// <returns>
      /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
      /// </returns>
      public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(CompoundObjectCreatedEventManager))
         {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<object, NewObjectEventArgs>(OnCompoundObjectCreated), 
                                               sender, new object[] { e });
            return true;
         }
         else if (managerType == typeof(ObjectCreatedEventManager))
         {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<object, NewObjectEventArgs>(OnObjectCreated),
                                               sender, new object[] { e });
            return true;
         }
         else if (managerType == typeof(RelationshipCreatedEventManager))
         {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<object, NewRelationshipEventArgs>(OnRelationshipCreated),
                                               sender, new object[] { e });
            return true;
         }
         else if (managerType == typeof(RelationshipTypeCreatedEventManager))
         {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<object, NewRelationshipTypeEventArgs>(OnRelationshipTypeCreated),
                                               sender, new object[] { e });
            return true;
         }
         else if (managerType == typeof(WordNetStartedEventManager))
         {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(OnSearchStarted));
            return true;
         }
         else if (managerType == typeof(WordNetFinishedEventManager))
         {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(OnSearchFinished));
            return true;
         }
         else if (managerType == typeof(NeuronChangedEventManager))
         {
            if (((NeuronChangedEventArgs)e).Action == BrainAction.Removed)                //only need to handle if there is a remove, all others not rquired.
               App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<NeuronChangedEventArgs>(OnNeuronRemoved), e);
            return true;
         }
         else
            return false;
      }

      /// <summary>
      /// Called when a neuron is changed: added/removed/replaced.  Need to ask each child to check the change and update what needs 
      /// updating.
      /// </summary>
      /// <param name="sender">The sender.</param>
      /// <param name="e">The <see cref="NeuronChangedEventArgs"/> instance containing the event data.</param>
      void OnNeuronRemoved(NeuronChangedEventArgs e)
      {
         foreach (WordNetItem i in Children)
            i.UpdateForDelete(e.OriginalSourceID);
      }

      /// <summary>
      /// Called when a search has started.
      /// </summary>
      void OnSearchStarted()
      {
         BrainData.BrainData.Current.LearnCount++;
      }

      /// <summary>
      /// Called when a search is finished.
      /// </summary>
      void OnSearchFinished()
      {
         BrainData.BrainData.Current.LearnCount--;
      }

      private void LogItem(WordNetEventItem iEvent)
      {
         EventItems.Add(iEvent);
         while (EventItems.Count > 2000)
            EventItems.RemoveAt(0);
      }

      /// <summary>
      /// Called when a new relationship type was created.
      /// </summary>
      /// <param name="sender">The sender.</param>
      /// <param name="e">The <see cref="JaStDev.HAB.NewRelationshipTypeEventArgs"/> instance containing the event data.</param>
      private void OnRelationshipTypeCreated(object sender, NewRelationshipTypeEventArgs e)
      {
         ulong iId = e.Neuron.ID;
         BrainData.BrainData.Current.DefaultMeaningIds.Add(iId);
         BrainData.BrainData.Current.NeuronInfo[iId].DisplayTitle = e.Relationship;
         WordNetEventItem iEvent = new WordNetEventItem() { EventArgs = e };
         iEvent.Text = "Relationship type created: " + e.Relationship;
         LogItem(iEvent);
      }

      /// <summary>
      /// Called when a new relationship created.
      /// </summary>
      /// <param name="sender">The sender.</param>
      /// <param name="e">The <see cref="JaStDev.HAB.NewRelationshipEventArgs"/> instance containing the event data.</param>
      private void OnRelationshipCreated(object sender, NewRelationshipEventArgs e)
      {
         WordNetEventItem iEvent = new WordNetEventItem() { EventArgs = e };
         StringBuilder iStr = new StringBuilder("Relationship created from: '");
         iStr.Append(BrainData.BrainData.Current.NeuronInfo[e.Neuron.ID]);
         iStr.Append(" ' to: '");
         iStr.Append(BrainData.BrainData.Current.NeuronInfo[e.Related.ID]);
         iStr.Append("' meaning: '");
         iStr.Append(BrainData.BrainData.Current.NeuronInfo[e.Related.Meaning]);
         iStr.Append("'");
         iEvent.Text = iStr.ToString();
         LogItem(iEvent);
      }

      /// <summary>
      /// Called when [object created]. Also alks each child if it represents the newly created object.
      /// </summary>
      /// <param name="sender">The sender.</param>
      /// <param name="e">The <see cref="JaStDev.HAB.NewObjectEventArgs"/> instance containing the event data.</param>
      private void OnObjectCreated(object sender, NewObjectEventArgs e)
      {
         NeuronData iInfo = BrainData.BrainData.Current.NeuronInfo[e.Neuron.ID];
         iInfo.DisplayTitle = e.Value;
         iInfo.StoreDescription(e.Description);                                        //set the description
         WordNetEventItem iEvent = new WordNetEventItem() { EventArgs = e };
         iEvent.Text = "object created: " + e.Value;
         LogItem(iEvent);
         foreach (WordNetItem i in Children)
            i.UpdateForNewObject(e.SynSetId, e.Value);
      }

      /// <summary>
      /// Called when a new compound object was created.
      /// </summary>
      /// <param name="sender">The sender.</param>
      /// <param name="e">The <see cref="JaStDev.HAB.NewObjectEventArgs"/> instance containing the event data.</param>
      private void OnCompoundObjectCreated(object sender, NewObjectEventArgs e)
      {
         BrainData.BrainData.Current.NeuronInfo[e.Neuron.ID].DisplayTitle = e.Value;
         WordNetEventItem iEvent = new WordNetEventItem() { EventArgs = e };
         iEvent.Text = "Compound object created: " + e.Value;
         LogItem(iEvent);
      }

      #endregion


      /// <summary>
      /// Loads all the children of the specified word for the current relationship.
      /// </summary>
      /// <param name="text">The text.</param>
      public void LoadChildrenFor(string text)
      {
         Children.Clear();
         fSearchText = text;                                                                          //store the last searched item, so we can do the search again if the selected relationship is changed.
         foreach (wordnetDataSet.WordInfoRow iRow in WordNetSin.Default.GetWordInfoFor(text))
         {
            WordNetItem iNew = new WordNetItem();
            iNew.Text = iRow.Word;
            iNew.POS = iRow.pos;
            iNew.SynsetID = iRow.synsetid;
            iNew.Description = iRow.definition;
            fChildren.Add(iNew);
            if (SelectedRelationship == SYNONYMSID)
               iNew.LoadSynonymsFor(iRow.synsetid);
            else
               iNew.LoadRelatedWordsFor(text, iRow.synsetid, SelectedRelationship);
         }
         if (IncludeCompoundWords == true)
         {
            CompundWordsTableAdapter iCompoundAdapter = new CompundWordsTableAdapter();
            wordnetDataSet.CompundWordsDataTable iData = iCompoundAdapter.GetData(text);
            foreach (wordnetDataSet.CompundWordsRow i in iData)
            {
               WordNetItem iNew = new WordNetItem();
               iNew.Text = i.Word;
               iNew.POS = i.pos;
               iNew.SynsetID = i.synsetid;
               iNew.Description = i.definition;
               fChildren.Add(iNew);
               if (SelectedRelationship == SYNONYMSID)
                  iNew.LoadSynonymsFor(i.synsetid);
               else
                  iNew.LoadRelatedWordsFor(text, i.synsetid, SelectedRelationship);
            }
         }
      }
   }
}
