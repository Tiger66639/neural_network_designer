﻿using System.Collections.ObjectModel;

namespace JaStDev.HAB.Designer.Comm_channels.Wordnet
{
   public interface IWordNetItemOwner
   {
      /// <summary>
      /// Gets the list of children for this item.
      /// </summary>
      ObservableCollection<WordNetItem> Children { get; }

   }
}
