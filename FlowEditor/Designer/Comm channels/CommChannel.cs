﻿using System.Xml.Serialization;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.BrainData;
using JaStDev.HAB.Designer.Comm_channels.Audio;
using JaStDev.HAB.Designer.Comm_channels.Images;
using JaStDev.HAB.Designer.Comm_channels.Resources;
using JaStDev.HAB.Designer.Comm_channels.Text;
using JaStDev.HAB.Designer.Comm_channels.Wordnet;
using JaStDev.HAB.Sins;

namespace JaStDev.HAB.Designer.Comm_channels
{
    /// <summary>
    /// Incapsulates the functionality for a communication channel (<see cref="Sin"/>).
    /// </summary>
    [XmlInclude(typeof(AudioChannel))]
    [XmlInclude(typeof(ImageChannel))]
    [XmlInclude(typeof(TextChannel))]
    [XmlInclude(typeof(WordNetChannel))]
    public abstract class CommChannel : OwnedObject, INeuronInfo, INeuronWrapper
    {
        #region Fields

        private bool fIsVisible;

        //string fName;
        private Sin fSin;

        private NeuronData fItemData;

        private SerializableDictionary<string, object> fDictionary = new SerializableDictionary<string, object>();
        private ObservedCollection<ResourceReference> fResources;

        #endregion Fields

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="CommChannel"/> class.
        /// </summary>
        public CommChannel()
        {
            fResources = new ObservedCollection<ResourceReference>(this);
        }

        #endregion ctor

        #region prop

        #region IsVisible

        /// <summary>
        /// Gets/sets the if this comm channel is curently visible or not.
        /// </summary>
        public bool IsVisible
        {
            get
            {
                return fIsVisible;
            }
            set
            {
                if (fIsVisible != value)
                {
                    OnPropertyChanging("IsVisible", fIsVisible, value);
                    fIsVisible = value;
                    UpdateOpenDocuments();
                    OnPropertyChanged("IsVisible");
                }
            }
        }

        private void UpdateOpenDocuments()
        {
            if (BrainData.BrainData.Current != null && BrainData.BrainData.Current.OpenDocuments != null)                                  //this prop gets set after the braindata has been loaded (so current does not yet exist), so in this case, we can't set the open documents yet. This list is craeted by the Onloaded routine.
            {
                if (IsVisible == true)
                    BrainData.BrainData.Current.OpenDocuments.Add(this);                                                           //this actually shows the item
                else
                    BrainData.BrainData.Current.OpenDocuments.Remove(this);
            }
        }

        #endregion IsVisible

        #region NeuronID

        /// <summary>
        /// Gets/sets the ID of the <see cref="CommChannel.Sin"/>
        /// </summary>
        /// <remarks>
        /// This prop is primarely for streaming.
        /// </remarks>
        public ulong NeuronID
        {
            get
            {
                if (fSin != null)
                    return fSin.ID;
                else
                    return Neuron.EmptyId;
            }
            set
            {
                if (value != Neuron.EmptyId)
                {
                    if (Brain.Brain.Current.NeuronCount > value)
                    {
                        SetSin(Brain.Brain.Current[value] as Sin);
                        if (fSin == null)
                            Log.Log.LogError("Communication channel", string.Format("Failed to load neuron with id {0} because it is not a Sin. You probably have a corrupt comm channels file.", value));
                    }
                    else
                        Log.Log.LogError("Communication channel", string.Format("Failed to load neuron with id {0} because it is out of range of the brain's content. You probably have a corrupt comm channels file.", value));
                }
                else
                    SetSin(null);
            }
        }

        /// <summary>
        /// Sets the Sensory interface that this object is a wrapper of.
        /// </summary>
        /// <param name="sin">The sin.</param>
        protected virtual void SetSin(Sin sin)
        {
            fSin = sin;
            if (sin != null && BrainData.BrainData.Current != null && BrainData.BrainData.Current.NeuronInfo != null)              //neuroninfo can be null when a project is loaded.  SetSin gets called after the entire project is loaded.
                fItemData = BrainData.BrainData.Current.NeuronInfo[fSin.ID];
            else
                fItemData = null;
        }

        #endregion NeuronID

        #region Sin

        /// <summary>
        /// Gets the sin that this object incapsulates.
        /// </summary>
        /// <value>The sin.</value>
        public Sin Sin
        {
            get { return fSin; }
        }

        #endregion Sin

        #region Dictionary

        /// <summary>
        /// Gets a dictionary that can be used to store extra info used by visualizers of a communication channel.
        /// For instance, <see cref="TextChannelView"/> uses this to store sub screen sizes.
        /// </summary>
        public SerializableDictionary<string, object> Dictionary
        {
            get { return fDictionary; }
        }

        #endregion Dictionary

        #region Resources

        /// <summary>
        /// Gets the list of resources that are defined for this sin.
        /// </summary>
        public ObservedCollection<ResourceReference> Resources
        {
            get { return fResources; }
        }

        #endregion Resources

        #endregion prop

        #region INeuronWrapper Members

        /// <summary>
        /// Gets the item.
        /// </summary>
        /// <value>The item.</value>
        public Neuron Item
        {
            get { return Sin; }
        }

        #endregion INeuronWrapper Members

        #region INeuronInfo Members

        /// <summary>
        /// Gets the extra info for the specified neuron.  Can be null.
        /// </summary>
        /// <value></value>
        public NeuronData NeuronInfo
        {
            get
            {
                if (fItemData == null && fSin != null)                                     //when a project gets loaded, we can't yet load the NeuronData cause it is not yet accessible, so do this in a delayed fashion.
                    fItemData = BrainData.BrainData.Current.NeuronInfo[fSin.ID];
                return fItemData;
            }
        }

        #endregion INeuronInfo Members
    }
}