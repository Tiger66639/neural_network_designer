﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace JaStDev.HAB.Designer.Editors.Frames
{
   /// <summary>
   /// Interaction logic for FrameEditorView.xaml
   /// </summary>
   public partial class FrameEditorView : UserControl
   {

      #region Commands

      /// <summary>
      /// Command to add a frame element to a frame.
      /// </summary>
      public static RoutedCommand AddFrameElementCmd = new RoutedCommand();

      /// <summary>
      /// Command to add a frame evoker to a frame.
      /// </summary>
      public static RoutedCommand AddFrameEvokerCmd = new RoutedCommand();

      /// <summary>
      /// Command to add a sequence to a frame.
      /// </summary>
      public static RoutedCommand AddFrameSequenceCmd = new RoutedCommand();

      /// <summary>
      /// Command to move a sequence up in the list
      /// </summary>
      public static RoutedCommand MoveElementUpCmd = new RoutedCommand();

      /// <summary>
      /// Command to move a sequence down in the list
      /// </summary>
      public static RoutedCommand MoveElementDownCmd = new RoutedCommand();

      /// <summary>
      /// Command to add an element to a sequence.
      /// </summary>
      public static RoutedCommand AddElementToSequenceCmd = new RoutedCommand();

      /// <summary>
      /// Command to remove an element from a sequence.
      /// </summary>
      public static RoutedCommand RemoveElementFromSequenceCmd = new RoutedCommand();

      #endregion

      #region ctor
      /// <summary>
      /// Initializes a new instance of the <see cref="FrameEditorView"/> class.
      /// </summary>
      public FrameEditorView()
      {
         InitializeComponent();
      } 
      #endregion

      #region Event handler

      /// <summary>
      /// Handles the Click event of the BtnAddFrame control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void BtnAddFrame_Click(object sender, RoutedEventArgs e)
      {
         try
         {
            WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
            try
            {
               Frame iObject = EditorsHelper.MakeFrame();
               if (iObject != null)
                  AddNewFrame(iObject);
            }
            finally
            {
               Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(WindowMain.UndoStore.EndUndoGroup));          //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled.
            }
         }
         catch (Exception ex)
         {
            string iMsg = ex.ToString();
            MessageBox.Show(iMsg);
            Log.Log.LogError("New Frame", iMsg);
         }

      }


      #endregion

      #region Functions
      /// <summary>
      /// Adds the new frame to the frame editor object and makes it the selected one.
      /// </summary>
      /// <param name="toAdd">To add.</param>
      private void AddNewFrame(Frame toAdd)
      {
         FrameEditor iEditor = (FrameEditor)DataContext;
         iEditor.Frames.Add(toAdd);
         iEditor.SelectedFrame = toAdd;
      }

      /// <summary>
      /// Adds the new frame element to the currently selected frame and makes it the selected one + visual.
      /// </summary>
      /// <param name="iObject">The i object.</param>
      private void AddNewFrameElement(FrameElement toAdd)
      {
         FrameEditor iEditor = (FrameEditor)DataContext;
         Frame iSelected = iEditor.SelectedFrame;
         Debug.Assert(iSelected != null);
         iSelected.Elements.Add(toAdd);
         TabContent.SelectedIndex = 0;                                                                //need to make certain that the correct page is visible
         DataElements.SelectedItem = toAdd;
      }

      /// <summary>
      /// Adds the new frame evoker to the currently selected frame and makes it the selected one + visual.
      /// </summary>
      /// <param name="toAdd">To add.</param>
      private void AddNewFrameEvoker(FrameEvoker toAdd)
      {
         FrameEditor iEditor = (FrameEditor)DataContext;
         Frame iSelected = iEditor.SelectedFrame;
         Debug.Assert(iSelected != null);
         iSelected.Evokers.Add(toAdd);
         TabContent.SelectedIndex = 1;                                                                //need to make certain that the correct page is visible
      }


      private void AddNewFrameSequence(FrameSequence toAdd)
      {
         FrameEditor iEditor = (FrameEditor)DataContext;
         Frame iSelected = iEditor.SelectedFrame;
         Debug.Assert(iSelected != null);
         iSelected.Sequences.Add(toAdd);
         TabContent.SelectedIndex = 2;                                                                //need to make certain that the correct page is visible
      }

      #endregion


      #region Add element/evoker/sequence
      /// <summary>
      /// Handles the CanExecute event of the AddToSelected control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void AddToSelected_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         FrameEditor iEditor = (FrameEditor)DataContext;
         e.CanExecute = iEditor != null && iEditor.SelectedFrame != null;
      }

      /// <summary>
      /// Handles the Executed event of the AddFrameSequence control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void AddFrameSequence_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         try
         {
            WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
            try
            {
               FrameSequence iObject = EditorsHelper.MakeFrameSequence();
               if (iObject != null)
                  AddNewFrameSequence(iObject);
            }
            finally
            {
               Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(WindowMain.UndoStore.EndUndoGroup));          //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled.
            }
         }
         catch (Exception ex)
         {
            string iMsg = ex.ToString();
            MessageBox.Show(iMsg);
            Log.Log.LogError("New Frame sequence", iMsg);
         }
      }



      /// <summary>
      /// Handles the Executed event of the AddFrameEvoker control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void AddFrameEvoker_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         try
         {
            WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
            try
            {
               FrameEvoker iObject = EditorsHelper.MakeFrameEvoker();
               if (iObject != null)
                  AddNewFrameEvoker(iObject);
            }
            finally
            {
               Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(WindowMain.UndoStore.EndUndoGroup));          //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled.
            }
         }
         catch (Exception ex)
         {
            string iMsg = ex.ToString();
            MessageBox.Show(iMsg);
            Log.Log.LogError("New Frame evoker", iMsg);
         }
      }

      /// <summary>
      /// Handles the Executed event of the AddFrameElement control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void AddFrameElement_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         try
         {
            WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
            try
            {
               FrameElement iObject = EditorsHelper.MakeFrameElement();
               if (iObject != null)
                  AddNewFrameElement(iObject);
            }
            finally
            {
               Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(WindowMain.UndoStore.EndUndoGroup));          //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled.
            }
         }
         catch (Exception ex)
         {
            string iMsg = ex.ToString();
            MessageBox.Show(iMsg);
            Log.Log.LogError("New Frame element", iMsg);
         }
      } 
      #endregion

      #region MoveElementUp
      private void MoveElementUp_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         if (LstSequenceContent != null)
         {
            FrameElementCollection iCollection = (FrameElementCollection)LstSequenceContent.ItemsSource;
            if (LstSequenceContent.SelectedItems.Count > 0)
            {
               int iIndex = iCollection.IndexOf((FrameElement)LstSequenceContent.SelectedItems[0]);
               e.CanExecute = iIndex > 0;                                                             //if the first item is at the first pos, we can no longer move up.
            }
            else
               e.CanExecute = false;
         }
         else
            e.CanExecute = false;
      }

      private void MoveElementUp_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FrameElementCollection iCollection = (FrameElementCollection)LstSequenceContent.ItemsSource;
         List<FrameElement> iSelected = (from FrameElement i in LstSequenceContent.SelectedItems select i).ToList();
         foreach (FrameElement i in iSelected)
         {
            int iIndex = iCollection.IndexOf(i);
            iCollection.Move(iIndex, iIndex - 1);
         }
      } 
      #endregion

      #region MoveElementDown
      /// <summary>
      /// Handles the CanExecute event of the MoveElementDown control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void MoveElementDown_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         if (LstSequenceContent != null)
         {
            FrameElementCollection iCollection = (FrameElementCollection)LstSequenceContent.ItemsSource;
            IList iSelected = LstSequenceContent.SelectedItems;
            if (iSelected.Count > 0)
            {
               int iIndex = iCollection.IndexOf((FrameElement)iSelected[iSelected.Count - 1]);
               e.CanExecute = iIndex < iCollection.Count - 1;                                                             //if the last selected item is at the last pos, we can no longer move up.
            }
            else
               e.CanExecute = false;
         }
         else
            e.CanExecute = false;
      }

      /// <summary>
      /// Handles the Executed event of the MoveElementDown control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void MoveElementDown_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FrameElementCollection iCollection = (FrameElementCollection)LstSequenceContent.ItemsSource;
         List<FrameElement> iSelected = (from FrameElement i in LstSequenceContent.SelectedItems select i).Reverse().ToList();      //we need to reverse the list cause the last item needs to be moved first.
         foreach (FrameElement i in iSelected)
         {
            int iIndex = iCollection.IndexOf(i);
            iCollection.Move(iIndex, iIndex + 1);
         }
      } 
      #endregion


      #region AddElementToSequence
      /// <summary>
      /// Handles the CanExecute event of the AddElementToSequence control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void AddElementToSequence_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         e.CanExecute = LstNotUsed != null && LstNotUsed.SelectedItems.Count > 0;
      }

      /// <summary>
      /// Handles the Executed event of the AddElementToSequence control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void AddElementToSequence_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FrameElementCollection iCollection = (FrameElementCollection)LstSequenceContent.ItemsSource;
         List<FrameElement> iList = (from FrameElement i in LstNotUsed.SelectedItems select i).ToList();       //need to make a copy cause we are going to modify this list.
         foreach (FrameElement i in iList)
            iCollection.Add(i);

      } 
      #endregion

      #region RemoveElementFromSequence
      /// <summary>
      /// Handles the CanExecute event of the RemoveElementFromSequence control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void RemoveElementFromSequence_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         e.CanExecute = LstSequenceContent != null && LstSequenceContent.SelectedItems.Count > 0;
      }

      /// <summary>
      /// Handles the Executed event of the RemoveElementFromSequence control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void RemoveElementFromSequence_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FrameElementCollection iCollection = (FrameElementCollection)LstSequenceContent.ItemsSource;
         List<FrameElement> iList = (from FrameElement i in LstSequenceContent.SelectedItems select i).ToList();       //need to make a copy cause we are going to modify this list.
         foreach (FrameElement i in iList)
            iCollection.Remove(i);
      } 
      #endregion


   }
}
