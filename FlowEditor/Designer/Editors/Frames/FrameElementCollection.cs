﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.BrainData;
using JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems;

namespace JaStDev.HAB.Designer.Editors.Frames
{
   public class FrameElementCollection : ClusterCollection<FrameElement>
   {
      #region ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="CodeItemCollection"/> class.
      /// </summary>
      /// <param name="owner">The <see cref="CodeEditor"/> that contains this code list.</param>
      /// <param name="childList">The NeuronCluster that contains all the code items.</param>
      public FrameElementCollection(INeuronWrapper owner, NeuronCluster childList): base(owner, childList)
      {

      }

      /// <summary>
      /// Initializes a new instance of the <see cref="CodeItemCollection"/> class.
      /// </summary>
      /// <param name="owner">The owner.</param>
      /// <param name="linkMeaning">The link meaning.</param>
      public FrameElementCollection(INeuronWrapper owner, ulong linkMeaning)
         : base(owner, linkMeaning, linkMeaning)
      {

      }

      #endregion


      #region Functions
		/// <summary>
      /// Called when a new wrapper object needs to be created for a neuron.
      /// </summary>
      /// <param name="toWrap">To wrap.</param>
      /// <returns></returns>
      /// <remarks>
      /// CodeEditors do: return CodeEditor.CreateCodeItemFor(toWrap)
      /// </remarks>
      public override FrameElement GetWrapperFor(Neuron toWrap)
      {
         return new FrameElement(toWrap);
      } 
	#endregion

      /// <summary>
      /// Returns the meaning that should be assigned to the cluster when it is newly created.
      /// </summary>
      /// <param name="linkMeaning">The meaning of the link between the wrapped cluster and the owner of this collection.</param>
      /// <returns></returns>
      protected override ulong GetListMeaning(ulong linkMeaning)
      {
         return (ulong)PredefinedNeurons.Frame;
      }
   }
}
