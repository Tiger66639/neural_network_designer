﻿using System;
using System.Windows;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.BrainData;

namespace JaStDev.HAB.Designer.Editors.Frames
{
   public class FrameItemBase : OwnedObject<INeuronWrapper>, INeuronWrapper, INeuronInfo, IWeakEventListener
   {
      #region fields
      Neuron fToWrap; 
      #endregion

      #region ctor
      /// <summary>
      /// Initializes a new instance of the <see cref="FrameElement"/> class.
      /// </summary>
      /// <param name="toWrap">To wrap.</param>
      public FrameItemBase(Neuron toWrap)
      {
         fToWrap = toWrap;
      } 
      #endregion

      #region Item (INeuronWrapper Members)

      /// <summary>
      /// Gets the item.
      /// </summary>
      /// <value>The item.</value>
      public Neuron Item
      {
         get { return fToWrap; }
      }

      #endregion

      #region NeuronInfo (INeuronInfo Members)

      /// <summary>
      /// Gets the extra info for the specified neuron.  Can be null.
      /// </summary>
      /// <value></value>
      public NeuronData NeuronInfo
      {
         get 
         {
            if (fToWrap != null)
               return BrainData.BrainData.Current.NeuronInfo[fToWrap.ID];
            else
               return null;
         }
      }

      #endregion

      #region IWeakEventListener Members

      /// <summary>
      /// Receives events from the centralized event manager.
      /// </summary>
      /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
      /// <param name="sender">Object that originated the event.</param>
      /// <param name="e">Event data.</param>
      /// <returns>
      /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
      /// </returns>
      public virtual bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         //note: we don't need to check for delete or change on item, this is done by the list manager.
         return false;
      }

      #endregion

   }
}
