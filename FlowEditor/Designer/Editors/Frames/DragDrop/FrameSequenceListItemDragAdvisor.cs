﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using JaStDev.HAB.Designer.DragDrop;

namespace JaStDev.HAB.Designer.Editors.Frames.DragDrop
{
   public class FrameSequenceListItemDragAdvisor : DragSourceBase
   {
      public FrameSequenceListItemDragAdvisor()
      {
         SupportedFormat = Properties.Resources.NeuronIDFormat;                                                        //this is not really used, simply informtive: this is our main data type.
      }

      #region UsePreviewEvents
      /// <summary>
      /// Gets if the preview event versions should be used or not.
      /// </summary>
      /// <remarks>
      /// don't use preview events cause than the sub drop points don't get used but only the main list cause this gets the events first,
      /// while we usually want to drop in a sub drop point.
      /// </remarks>
      public override bool UsePreviewEvents
      {
         get
         {
            return false;
         }
      }
      #endregion

      

      #region Item

      /// <summary>
      /// Gets the item that is the datacontext of the target of this drop advisor.
      /// </summary>
      public FrameSequence Item
      {
         get
         {
            return ((FrameworkElement)SourceUI).DataContext as FrameSequence;
         }
      }

      #endregion

      #region Items

      /// <summary>
      /// Gets the list containing all the code that the UI to which advisor is attached too, displays data for.
      /// </summary>
      public FrameOrderCollection Items
      {
         get
         {
            ListBoxItem iListBoxItem = TreeHelper.FindInTree<ListBoxItem>(SourceUI);
            ItemsControl iItemsControl = ItemsControl.ItemsControlFromItemContainer(iListBoxItem);
            Debug.Assert(iItemsControl != null);
            return iItemsControl.ItemsSource as FrameOrderCollection;
         }
      }

      #endregion

      public override void FinishDrag(UIElement draggedElt, DragDropEffects finalEffects)
      {
         if ((finalEffects & DragDropEffects.Move) == DragDropEffects.Move)
         {
            FrameSequence iItem = Item;
            FrameOrderCollection iList = Items;
            Debug.Assert(iList != null);
            iList.Remove(Item);
         }
      }

      /// <summary>
      /// We can drag when there is a content in the presenter.
      /// </summary>
      public override bool IsDraggable(UIElement dragElt)
      {
         return Item != null && Items != null;
      }

      /// <summary>
      /// we override cause we put the image to use + an ulong if it is a neuron, or a ref to the mind map item.
      /// If the item is a link, we also store which side of the link it was, so we can adjust it again (+ update it).
      /// </summary>
      public override DataObject GetDataObject(UIElement draggedElt)
      {
         FrameworkElement iDragged = (FrameworkElement)draggedElt;
         DataObject iObj = new DataObject();

         FrameSequence iContent = Item;

         iObj.SetData(Properties.Resources.UIElementFormat, iDragged);
         iObj.SetData(Properties.Resources.FrameSequenceFormat, iContent);
         iObj.SetData(Properties.Resources.NeuronIDFormat, iContent.Item.ID);

         return iObj;

      }
   }
}
