﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using JaStDev.HAB.Designer.DragDrop;

namespace JaStDev.HAB.Designer.Editors.Frames.DragDrop
{
   /// <summary>
   /// Drop advisor for <see cref="FrameEvokerCollection"/> objects.  
   /// </summary>
   /// <remarks>
   /// This object should be attached to an ItemsControl that is bound to a code list with
   /// it's <see cref="ItemsControl.ItemsSource"/> property.
   /// <para>
   /// Provides only add functionality for the list, no inserts.  This has to be done with <see cref="FrameEvokerListItemDropAdvisor"/>
   /// </para>
   /// </remarks>
   public class FrameEvokerListDropAdvisor : DropTargetBase
   {
      #region prop
      #region Target

      public ItemsControl Target
      {
         get
         {
            return (ItemsControl)TargetUI;
         }
      }

      #endregion

      #region UsePreviewEvents
      /// <summary>
      /// Gets if the preview event versions should be used or not.
      /// </summary>
      /// <remarks>
      /// don't use preview events cause than the sub lists don't get used but only the main list cause this gets the events first,
      /// while we usually want to drop in a sublist.
      /// </remarks>
      public override bool UsePreviewEvents
      {
         get
         {
            return false;
         }
      }
      #endregion

      #region Items

      /// <summary>
      /// Gets the list containing all the code that the UI to which advisor is attached too, displays data for.
      /// </summary>
      public FrameEvokerCollection Items
      {
         get
         {
            return Target.ItemsSource as FrameEvokerCollection;
         }
      }

      #endregion
      #endregion

      #region Overrides
      public override void OnDropCompleted(DragEventArgs arg, Point dropPoint)
      {
         FrameEvoker iItem = arg.Data.GetData(Properties.Resources.FrameEvokerFormat) as FrameEvoker;
         if (iItem != null && Items.Contains(iItem) == true)
            TryMoveItem(arg, dropPoint, iItem);
         else
            TryCreateNewCodeItem(arg, dropPoint);                                                                             //we are not moving around an item, so add new code item.
         //Control iDropTarget = (Control)TargetUI;
      }


      public override bool IsValidDataObject(IDataObject obj)
      {
         return obj.GetDataPresent(Properties.Resources.FrameEvokerFormat)
                || obj.GetDataPresent(Properties.Resources.NeuronIDFormat);
      }

      //public override DragDropEffects GetEffect(DragEventArgs e)
      //{
      //   DragDropEffects iRes = base.GetEffect(e);
      //   if (iRes == DragDropEffects.Move)
      //   {
      //      CodeItem iItem = e.Data.GetData(Properties.Resources.CodeItemFormat) as CodeItem;
      //      if (iItem != null && CodeList.Contains(iItem) == true)
      //         return DragDropEffects.Copy;                                                                 //when we move on the same list, the drag source doesn't have to do anything.
      //   }
      //   return iRes;
      //}

      #endregion

      /// <summary>
      /// Tries the move item.
      /// </summary>
      /// <param name="arg">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
      /// <param name="dropPoint">The drop point.</param>
      /// <param name="iItem">The i item.</param>
      private void TryMoveItem(DragEventArgs arg, Point dropPoint, FrameEvoker iItem)
      {
         FrameEvokerCollection iList = Items;
         int iIndex = iList.IndexOf(iItem);
         if (iIndex == -1)                                    //we need to create a new item if the item being moved wasn't in this list or when a copy was requested because shift was pressed.
         {
            FrameEvoker iNew = new FrameEvoker(iItem.Item);                                                         //we always create a new item, even with a move, cause we don't know how the item was being dragged (with shift pressed or not), this is the savest and easiest way.
            iList.Add(iNew);
         }
         else
         {
            iList.Move(iIndex, iList.Count - 1);
            arg.Effects = DragDropEffects.None;
         }
      }

      private void TryCreateNewCodeItem(DragEventArgs obj, Point dropPoint)
      {
         ulong iId = (ulong)obj.Data.GetData(Properties.Resources.NeuronIDFormat);

         FrameEvoker iNew = new FrameEvoker(Brain.Brain.Current[iId]);
         FrameEvokerCollection iList = Items;
         Debug.Assert(iList != null);
         iList.AddWithoutEvents(iNew);
      }
   }
}
