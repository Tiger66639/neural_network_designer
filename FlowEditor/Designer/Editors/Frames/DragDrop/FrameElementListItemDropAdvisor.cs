﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using JaStDev.HAB.Designer.DragDrop;

namespace JaStDev.HAB.Designer.Editors.Frames.DragDrop
{
   /// <summary>
   /// Drop advisor for the child items of <see cref="FrameElementCollection"/> objects.  
   /// </summary>
   public class FrameElementListItemDropAdvisor : DropTargetBase
   {
      #region prop

      #region UsePreviewEvents
      /// <summary>
      /// Gets if the preview event versions should be used or not.
      /// </summary>
      /// <remarks>
      /// don't use preview events cause than the sub lists don't get used but only the main list cause this gets the events first,
      /// while we usually want to drop in a sublist.
      /// </remarks>
      public override bool UsePreviewEvents
      {
         get
         {
            return false;
         }
      }
      #endregion

      #region Items

      /// <summary>
      /// Gets the list containing all the code that the UI to which advisor is attached too, displays data for.
      /// </summary>
      public FrameElementCollection Items
      {
         get
         {
            ItemsControl iItemsControl = ItemsControl.ItemsControlFromItemContainer(TargetUI);
            Debug.Assert(iItemsControl != null);
            return iItemsControl.ItemsSource as FrameElementCollection;
         }
      }

      #endregion
      #endregion

      #region Overrides
      public override void OnDropCompleted(DragEventArgs arg, Point dropPoint)
      {
         FrameElement iItem = arg.Data.GetData(Properties.Resources.FrameElementFormat) as FrameElement;
         if (iItem != null && Items.Contains(iItem) == true)
            TryMoveItem(arg, dropPoint, iItem);
         else
            TryCreateNewCodeItem(arg, dropPoint);                                                  //we are not moving around an item, so add new code item.
      }


      public override bool IsValidDataObject(IDataObject obj)
      {
         return obj.GetDataPresent(Properties.Resources.FrameElementFormat)
                || obj.GetDataPresent(Properties.Resources.NeuronIDFormat);
      }

      #endregion

      private void TryMoveItem(DragEventArgs arg, Point dropPoint, FrameElement iItem)
      {
         FrameElement iDroppedOn = ((FrameworkElement)TargetUI).DataContext as FrameElement;
         FrameElementCollection iList = Items;
         int iNewIndex = iList.IndexOf(iDroppedOn);
         int iOldIndex = iList.IndexOf(iItem);
         if (iOldIndex == -1)
         {
            FrameElement iNew = new FrameElement(iItem.Item);                                                         //we always create a new item, even with a move, cause we don't know how the item was being dragged (with shift pressed or not), this is the savest and easiest way.
            iList.Insert(iNewIndex, iNew);
         }
         else
         {
            iList.Move(iOldIndex, iNewIndex);
            arg.Effects = DragDropEffects.None;
         }
      }

      private void TryCreateNewCodeItem(DragEventArgs obj, Point dropPoint)
      {
         ulong iId = (ulong)obj.Data.GetData(Properties.Resources.NeuronIDFormat);
         FrameElement iNew = new FrameElement(Brain.Brain.Current[iId]);
         FrameElement iDroppedOn = ((FrameworkElement)TargetUI).DataContext as FrameElement;
         Debug.Assert(iNew != null && iDroppedOn != null);
         FrameElementCollection iList = Items;
         Debug.Assert(iList != null);
         iList.InsertWithoutEvents(iList.IndexOf(iDroppedOn), iNew);
      }
   }
}
