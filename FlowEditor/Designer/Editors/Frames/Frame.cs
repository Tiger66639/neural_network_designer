﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.BrainData;
using JaStDev.HAB.Designer.WPF.BrainEventManagers;
using JaStDev.HAB.Events;

namespace JaStDev.HAB.Designer.Editors.Frames
{
    /// <summary>
    ///     A wrapper for a <see cref="NeuronCluster" /> that represents a Frame.
    /// </summary>
    /// <remarks>
    ///     A frame contains information about a single sentence: what are it's possible elements, the allowed order of
    ///     elements + triggers.
    /// </remarks>
    public class Frame : OwnedObject<FrameEditor>, INeuronWrapper, INeuronInfo, IWeakEventListener
    {
        #region ctor

        /// <summary>
        ///     Initializes a new instance of the <see cref="Frame" /> class.
        /// </summary>
        public Frame(NeuronCluster toWrap)
        {
            Debug.Assert(toWrap != null);
            Elements = new FrameElementCollection(this, toWrap);

            var iFound = toWrap.FindFirstOut((ulong) PredefinedNeurons.FrameSequences) as NeuronCluster;
            if (iFound != null)
                Sequences = new FrameOrderCollection(this, iFound);
            else
                Sequences = new FrameOrderCollection(this, (ulong) PredefinedNeurons.FrameSequences);

            iFound = toWrap.FindFirstOut((ulong) PredefinedNeurons.FrameEvokers) as NeuronCluster;
            if (iFound != null)
                Evokers = new FrameEvokerCollection(this, iFound);
            else
                Evokers = new FrameEvokerCollection(this, (ulong) PredefinedNeurons.FrameEvokers);
            LinkChangedEventManager.AddListener(Brain.Brain.Current, this);
            NeuronChangedEventManager.AddListener(Brain.Brain.Current, this);
        }

        ~Frame()
        {
            LinkChangedEventManager.RemoveListener(Brain.Brain.Current, this);
            NeuronChangedEventManager.RemoveListener(Brain.Brain.Current, this);
        }

        #endregion

        #region prop

        #region Elements

        /// <summary>
        ///     Gets the list of frame elements declared in the frame.
        /// </summary>
        public FrameElementCollection Elements { get; private set; }

        #endregion

        #region Sequences

        /// <summary>
        ///     Gets the list of FrameOrder objects which represent known frame element sequences.
        /// </summary>
        public FrameOrderCollection Sequences { get; private set; }

        #endregion

        #region Evokers

        /// <summary>
        ///     Gets the list of evokers for this frame.
        /// </summary>
        public FrameEvokerCollection Evokers { get; private set; }

        #endregion

        #region Item (INeuronWrapper Members)

        /// <summary>
        ///     Gets the item.
        /// </summary>
        /// <value>The item.</value>
        public Neuron Item
        {
            get { return Elements.Cluster; }
        }

        #endregion

        #region NeuronInfo(INeuronInfo Members)

        /// <summary>
        ///     Gets the extra info for the specified neuron.  Can be null.
        /// </summary>
        /// <value></value>
        public NeuronData NeuronInfo
        {
            get { return BrainData.BrainData.Current.NeuronInfo[Elements.Cluster.ID]; }
        }

        #endregion

        #endregion

        #region Functions

        /// <summary>
        ///     Receives events from the centralized event manager.
        /// </summary>
        /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager" /> calling this method.</param>
        /// <param name="sender">Object that originated the event.</param>
        /// <param name="e">Event data.</param>
        /// <returns>
        ///     true if the listener handled the event. It is considered an error by the
        ///     <see cref="T:System.Windows.WeakEventManager" /> handling in WPF to register a listener for an event that the
        ///     listener does not handle. Regardless, the method should return false if it receives an event that it does not
        ///     recognize or handle.
        /// </returns>
        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            if (managerType == typeof(LinkChangedEventManager))
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                    new Action<LinkChangedEventArgs>(LinkChanged), e);
                return true;
            }
            if (managerType == typeof(NeuronChangedEventManager))
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                    new Action<NeuronChangedEventArgs>(NeuronChanged), e);
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Called when a link has been changed, checks if this is the pos link on our wrapped object, if so, raise the
        ///     property changed
        ///     so that everybody knows about the change.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="LinkChangedEventArgs" /> instance containing the event data.</param>
        private void LinkChanged(LinkChangedEventArgs e)
        {
            if (e.OriginalSource.FromID == Item.ID && e.OriginalSource.MeaningID == (ulong) PredefinedNeurons.POS)
                OnPropertyChanged("POS");
        }

        private void NeuronChanged(NeuronChangedEventArgs e)
        {
            if (e.OriginalSource == Item) //this item is deleted
            {
                if (e.Action == BrainAction.Removed) //note: don't change the owner's list, it does this automatically.
                    Elements = null;
                else if (e.Action == BrainAction.Changed)
                {
                    Elements = new FrameElementCollection(this, (NeuronCluster) e.NewValue);
                    OnPropertyChanged("Elements");
                }
            }
            else if (e.OriginalSource == Evokers.Cluster)
            {
                if (e.Action == BrainAction.Removed)
                    Evokers = null;
                else if (e.Action == BrainAction.Changed)
                    Evokers = new FrameEvokerCollection(this, (NeuronCluster) e.NewValue);
                OnPropertyChanged("Evokers");
            }
            else if (e.OriginalSource == Sequences.Cluster)
            {
                if (e.Action == BrainAction.Removed)
                    Sequences = null;
                else if (e.Action == BrainAction.Changed)
                    Sequences = new FrameOrderCollection(this, (NeuronCluster) e.NewValue);
                OnPropertyChanged("Evokers");
            }
        }

        #endregion
    }
}