﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.BrainData;
using JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems;

namespace JaStDev.HAB.Designer.Editors.Frames
{

   /// <summary>
   /// Contains a collection of FrameEvokers found in a single cluster.
   /// </summary>
   public class FrameEvokerCollection: ClusterCollection<FrameEvoker>
   {

      #region ctor
      /// <summary>
      /// Initializes a new instance of the <see cref="FrameOrderCollection"/> class.
      /// </summary>
      /// <param name="owner">The <see cref="CodeEditor"/> that contains this code list.</param>
      /// <param name="childList">The NeuronCluster that contains all the code items.</param>
      public FrameEvokerCollection(INeuronWrapper owner, NeuronCluster childList)
         : base(owner, childList)
      {

      }


      /// <summary>
      /// Initializes a new instance of the <see cref="FrameOrderCollection"/> class.
      /// </summary>
      /// <param name="owner">The owner.</param>
      /// <param name="linkMeaning">The link meaning.</param>
      public FrameEvokerCollection(INeuronWrapper owner, ulong linkMeaning)
         : base(owner, linkMeaning, linkMeaning)
      {

      } 
      #endregion

      public override FrameEvoker GetWrapperFor(Neuron toWrap)
      {
         return new FrameEvoker(toWrap);
      }

      protected override ulong GetListMeaning(ulong linkMeaning)
      {
         return (ulong)PredefinedNeurons.FrameEvokers;
      }
   }
}
