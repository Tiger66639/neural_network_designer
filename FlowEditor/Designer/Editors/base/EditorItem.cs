﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using JaStDev.HAB.Designer.BrainData;
using JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems;
using JaStDev.HAB.Designer.WPF.BrainEventManagers;
using JaStDev.HAB.Events;

namespace JaStDev.HAB.Designer.Editors.@base
{
   /// <summary>
   /// Base class for items used by editors. They can be nested.
   /// </summary>
   public class EditorItem : OwnedObject<IOwnedObject>, IWeakEventListener, INeuronWrapper, INeuronInfo               //we inherit from OwnedObject<IOwnedObject> cause this is the generic version that provides functions for searching the owner tree using generics. 
   {

      #region fields

      Neuron fItem;
      bool fIsSelected;
      bool fIsMultiUsed = false;
      NeuronData fNeuronInfo;
      #endregion

      #region ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="CodeItem"/> class.
      /// </summary>
      /// <param name="toWrap">The item to wrap.</param>
      public EditorItem(Neuron toWrap)
      {
         if (toWrap == null)
            throw new ArgumentNullException();
         Item = toWrap;
         InternalCreate();
      }

      /// <summary>
      /// For static code item
      /// </summary>
      public EditorItem()
      {
         InternalCreate();
      }

      private void InternalCreate()
      {
         InternalChange = false;
         NeuronListChangedEventManager.AddListener(Brain.Brain.Current, this);
         LinkChangedEventManager.AddListener(Brain.Brain.Current, this);
      }


      ~EditorItem()
      {
         LinkChangedEventManager.RemoveListener(Brain.Brain.Current, this);
         NeuronListChangedEventManager.RemoveListener(Brain.Brain.Current, this);
      } 

      #endregion

      #region prop

      /// <summary>
      /// a switch that determins if the class is doing an internal change or not. 
      /// This is used by the LinkChanged event manager to see if there needs to be
      /// an update in response to a linkchange or not.
      /// </summary>
      protected bool InternalChange { get; set; }


      #region IsSelected

      /// <summary>
      /// Gets/sets if this item is selected or not.
      /// </summary>
      /// <remarks>
      /// When changed, stores the value and raises the event but also updates the root list.
      /// </remarks>
      public bool IsSelected
      {
         get
         {
            return fIsSelected;
         }
         set
         {
            if (value != fIsSelected)
               UpdateRoot(value);                                    //we don't need to call setSelected again, this is done by the root when it adds the item to the list.
         }
      }

      /// <summary>
      /// Updates the root object's <see cref="IEditorSelection.SelectedItems"/> list so that everything is up to date.
      /// </summary>
      /// <param name="value">if set to <c>true</c> [value].</param>
      private void UpdateRoot(bool value)
      {
         IEditorSelection iRoot = Root;
         if (iRoot != null)
         {
            if (value == false)
               Root.SelectedItems.Remove(this);
            else
            {
               if (Keyboard.IsKeyDown(Key.LeftShift) == false && Keyboard.IsKeyDown(Key.RightShift) == false)
                  Root.SelectedItems.Clear();
               Root.SelectedItems.Add(this);
            }
         }
      }

      /// <summary>
      /// Stores the selected value and raises the event.
      /// </summary>
      /// <param name="value"></param>
      internal void SetSelected(bool value)
      {
         fIsSelected = value;
         OnPropertyChanged("IsSelected");
      }

      #endregion

      #region Item

      /// <summary>
      /// Gets the expression (or neuron in case of a static) item that is wrapped by this one.
      /// </summary>
      [XmlIgnore]
      public Neuron Item
      {
         get { return fItem; }
         protected set
         {
            if (fItem != value)
            {
               fItem = value;
               if (value != null)
                  OnItemChanged(value);
               OnPropertyChanged("Item");
            }
         }
      }

      /// <summary>
      /// Called when the <see cref="CodeItem.Item"/> has changed.
      /// </summary>
      /// <param name="value">The value.</param>
      protected virtual void OnItemChanged(Neuron value)
      {         
         using (NeuronsAccessor iClusteredBy = value.ClusteredBy)
            IsMultiUsed = iClusteredBy.Items.Count > 1;                                                         //if an expression belongs to multiple groups, we presume it is in multiple code sets.
      }

      #endregion

      #region Root
      /// <summary>
      /// Gets the root page, the <see cref="CodeEditorPage"/> object that contains this code.
      /// </summary>
      /// <remarks>
      /// This is used to find out if 2 code Items belong to the same function.
      /// </remarks>
      public IEditorSelection Root
      {
         get
         {
            return FindFirstOwner<IEditorSelection>();
         }
      }
      #endregion

      #region IsMultiUsed
      /// <summary>
      /// Queries the underlying <see cref="Expressions.Expression"/> to see if it used in more locations than 1.
      /// </summary>
      /// <remarks>
      /// It is possible for an expression (block) to be used by more than 1 function in more than 1 place.
      /// This property returns if this is the case or not.
      /// </remarks>
      [XmlIgnore]
      public bool IsMultiUsed
      {
         get
         {
            return fIsMultiUsed;
         }
         private set
         {
            fIsMultiUsed = value;
            OnPropertyChanged("IsMultiUsed");
         }
      }
      #endregion



      #region INeuronWrapper Members

      /// <summary>
      /// Gets the item.
      /// </summary>
      /// <value>The item.</value>
      Neuron INeuronWrapper.Item
      {
         get { return Item; }
      }

      #endregion

      #endregion

      #region IWeakEventListener Members

      /// <summary>
      /// Receives events from the centralized event manager.
      /// </summary>
      /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
      /// <param name="sender">Object that originated the event.</param>
      /// <param name="e">Event data.</param>
      /// <returns>
      /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
      /// </returns>
      public virtual bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(LinkChangedEventManager))
         {
            LinkChanged(sender, (LinkChangedEventArgs)e);
            return true;
         }
         else if (managerType == typeof(NeuronListChangedEventManager))
         {
            NeuronListChanged(sender, (NeuronListChangedEventArgs)e);
            return true;
         }
         else
            return false;
      }



      /// <summary>
      /// Handles the NeuronListChanged event of the Current control.
      /// </summary>
      /// <remarks>
      /// Whenever this item is added or removed to a list, check if it belongs to multiple clusters.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="NeuronListChangedEventArgs"/> instance containing the event data.</param>
      private void NeuronListChanged(object sender, NeuronListChangedEventArgs e)
      {
         if (e.Item == Item)
         {
            using (NeuronsAccessor iClusteredBy = Item.ClusteredBy)
            {
               switch (e.Action)
               {
                  case NeuronListChangeAction.Insert:
                     IsMultiUsed = iClusteredBy.Items.Count > 1;
                     break;
                  case NeuronListChangeAction.Remove:
                     IsMultiUsed = iClusteredBy.Items.Count > 2;                                            //we need to check for >2 cause the event of a remove is raised before it is actually removed.
                     break;
                  default:
                     break;
               }
            }
         }
      }

      /// <summary>
      /// Need to check if the relationship between the statement and the argument cluster doesn't change.
      /// In that case, we need to update the list.
      /// also raise the correct propertyChanged updates.
      /// </summary>
      void LinkChanged(object sender, LinkChangedEventArgs e)
      {
         if (InternalChange == false && e.OriginalSource.FromID == Item.ID)
            InternalLinkChanged(e.OriginalSource, e);
      }

      /// <summary>
      /// descendents that need to update links that changed can do this through this function.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      protected virtual void InternalLinkChanged(Link sender, LinkChangedEventArgs e)
      {

      }

      #endregion


      /// <summary>
      /// Removes the current code item from the code list, but not the actual neuron that represents the code
      /// item, this stays in the brain, it is simply no longer used in this code list.
      /// </summary>
      public virtual void RemoveChildFromCode(EditorItem child)
      {
         MessageBox.Show("Unable to remove the child!");
      }

      #region INeuronInfo Members

      public NeuronData NeuronInfo
      {
         get
         {
            if (fNeuronInfo == null)                                                    //we store a local copy so that, if it is a weak ref in the BrainData, the object remains valid for as long as this item exists (otherwise, we can't edit the displaytitle because the object is garbage collected).
               fNeuronInfo = BrainData.BrainData.Current.NeuronInfo[Item.ID];
            return fNeuronInfo;
         }
      }

      #endregion
   }
}
