﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace JaStDev.HAB.Designer.Editors.@base
{
   public class CtrlEditorItem: UserControl
   {
      public CtrlEditorItem()
      {
         MouseDown += new MouseButtonEventHandler(CtrlStatement_PreviewMouseDown);                                      //don't use preview version, cause than, if the user keeps shift pressed, all items underneath the top one will also be selected.
      }


      void CtrlStatement_PreviewMouseDown(object sender, MouseButtonEventArgs e)
      {
         FrameworkElement iOrSource = e.OriginalSource as FrameworkElement;
         if (iOrSource != null && iOrSource.DataContext == DataContext)                                                 //whe check on similar data context between this item and the original source to see if the click was done on this user control, and not one on top of this one.
            IsSelected = true;
      }

      #region IsSelected

      /// <summary>
      /// IsSelected Dependency Property
      /// </summary>
      public static readonly DependencyProperty IsSelectedProperty =
          DependencyProperty.Register("IsSelected", typeof(bool), typeof(CtrlEditorItem), new FrameworkPropertyMetadata(false,
                                 FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

      /// <summary>
      /// Gets or sets the IsSelected property.  This dependency property 
      /// indicates wether this code item is selected or not.
      /// </summary>
      public bool IsSelected
      {
         get { return (bool)GetValue(IsSelectedProperty); }
         set { SetValue(IsSelectedProperty, value); }
      }



      #endregion

   }
}
