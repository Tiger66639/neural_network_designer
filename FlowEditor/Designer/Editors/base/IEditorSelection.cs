﻿using System.Collections;

namespace JaStDev.HAB.Designer.Editors.@base
{

   /// <summary>
   /// A simple interface to change the selected values.
   /// </summary>
   public interface IEditorSelection
   {
      /// <summary>
      /// Gets the list of selected items.
      /// </summary>
      /// <value>The selected items.</value>
      IList SelectedItems { get; }

      /// <summary>
      /// Gets/sets the currently selected item. If there are multiple selections, the first is returned.
      /// </summary>
      object SelectedItem { get; }
   }
}
