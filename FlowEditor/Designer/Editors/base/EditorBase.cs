﻿using System.Collections;
using System.IO;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using JaStDev.HAB.Designer.BrainData;
using JaStDev.HAB.Designer.Editors.Overview;
using JaStDev.HAB.Designer.Tools.Description;
using JaStDev.HAB.Storage;

namespace JaStDev.HAB.Designer.Editors.@base
{

   /// <summary>
   /// Keeps track of the current state of the mindmap, is used to check if the object is being loaded or has already been so.
   /// </summary>
   public enum EditorState
   {
      Loading,
      Loaded,
      Undoing
   }

   /// <summary>
   /// Base class for all editor objects.
   /// </summary>
   abstract public class EditorBase : NamedObject, IDescriptionable, IXmlSerializable
   {
      #region Fields

      EditorState fCurrentState = EditorState.Loaded;                                                    //for when first created.
      string fDescription;
      bool fIsSelected;

      #endregion

      #region Prop

      #region Icon

      /// <summary>
      /// Gets the resource path to the icon that should be used for this editor.  This is usually class specific.
      /// </summary>
      public abstract string Icon
      {
         get;
      }

      #endregion

      #region IsSelected

      /// <summary>
      /// Gets/sets the wether the editor is currently selected in the overview list.
      /// When an editor is selected, it's owning list is automatically assigned to
      /// become the <see cref="BrainData.CurrentEditorsList"/>
      /// </summary>
      /// <remarks>
      /// This is provided so that a folder can make itself 'active' when selected.
      /// </remarks>
      public virtual bool IsSelected
      {
         get
         {
            return fIsSelected;
         }
         set
         {
            if (value != fIsSelected)
            {
               if (value == true)
               {
                  EditorFolder iOwner = Owner as EditorFolder;
                  if (iOwner != null)
                     BrainData.BrainData.Current.CurrentEditorsList = iOwner.Items;
                  else
                     BrainData.BrainData.Current.CurrentEditorsList = BrainData.BrainData.Current.Editors;
               }
               fIsSelected = value;
               OnPropertyChanged("IsSelected");
            }
         }
      }

      #endregion

      #region CurrentState

      /// <summary>
      /// Gets/sets the current state of the object.  This is used by MindMapItemCollection.
      /// </summary>
      internal EditorState CurrentState
      {
         get
         {
            if (fCurrentState == EditorState.Loaded && 
               (WindowMain.UndoStore.CurrentState == UndoState.Redoing)
               || WindowMain.UndoStore.CurrentState == UndoState.Undoing)
               return EditorState.Undoing;
            else
               return fCurrentState;
         }
         set
         {
            fCurrentState = value;
         }
      }

      #endregion

      #region IDescriptionable Members

      #region Description title
      /// <summary>
      /// Gets a title that the description editor can use to display in the header.
      /// </summary>
      /// <value></value>
      public virtual string DescriptionTitle
      {
         get { return Name + " - Editor"; }
      }
      #endregion

      #region Description

      /// <summary>
      /// Gets/sets the description of the graph.
      /// </summary>
      /// <remarks>
      /// Could be that this prop needs to be streamed manually with xmlWriter.WriteRaws.
      /// </remarks>
      public FlowDocument Description
      {
         get
         {
            if (fDescription != null)
            {
               StringReader stringReader = new StringReader(fDescription);
               XmlReader xmlReader = XmlReader.Create(stringReader);
               return XamlReader.Load(xmlReader) as FlowDocument;
            }
            else
               return Helper.CreateDefaultFlowDoc();
         }
         set
         {
            string iVal = XamlWriter.Save(value);
            if (fDescription != iVal)
            {
               fDescription = iVal;
               OnPropertyChanged("Description");
            }
         }
      }

      #endregion

      #endregion 

      #endregion

      #region IXmlSerializable Members

      /// <summary>
      /// This method is reserved and should not be used. When implementing the IXmlSerializable interface, you should return null (Nothing in Visual Basic) from this method, and instead, if specifying a custom schema is required, apply the <see cref="T:System.Xml.Serialization.XmlSchemaProviderAttribute"/> to the class.
      /// </summary>
      /// <returns>
      /// An <see cref="T:System.Xml.Schema.XmlSchema"/> that describes the XML representation of the object that is produced by the <see cref="M:System.Xml.Serialization.IXmlSerializable.WriteXml(System.Xml.XmlWriter)"/> method and consumed by the <see cref="M:System.Xml.Serialization.IXmlSerializable.ReadXml(System.Xml.XmlReader)"/> method.
      /// </returns>
      public XmlSchema GetSchema()
      {
         return null;
      }

      public virtual void ReadXml(XmlReader reader)
      {
         CurrentState = EditorState.Loading;
         try
         {
            string iName = reader.Name;
            bool wasEmpty = reader.IsEmptyElement;

            reader.Read();
            if (wasEmpty) return;

            while (reader.Name != iName)
            {
               if (ReadXmlInternal(reader) == false)                                      //if for some reason, we failed to read the item, log an error, and advance to the next item so that we don't get in a loop.
               {
                  Log.Log.LogError("EditorBase.ReadXml", string.Format("Failed to read xml element {0} in stream.", reader.Name));
                  reader.Skip();
               }
            }
            reader.ReadEndElement();
         }
         finally
         {
            CurrentState = EditorState.Loaded;
         }
      }

      /// <summary>
      /// Reads the fields/properties of the class.
      /// </summary>
      /// <param name="reader">The reader.</param>
      /// <returns>True if the item was properly read, otherwise false.</returns>
      /// <remarks>
      /// This function is called for each element that is found, so this function should check which element it is
      /// and only read that element accordingly.
      /// </remarks>
      protected virtual bool ReadXmlInternal(XmlReader reader)
      {
         if (reader.Name == "FlowDocument")
         {
            if (reader.IsEmptyElement == false)
               fDescription = reader.ReadOuterXml();
            else
               reader.ReadStartElement("FlowDocument");
            return true;
         }
         else if (reader.Name == "Name")
         {
            Name = XmlStore.ReadElement<string>(reader, "Name");
            return true;
         }
         return false;
      }

      /// <summary>
      /// Converts an object into its XML representation.
      /// </summary>
      /// <param name="writer">The <see cref="T:System.Xml.XmlWriter"/> stream to which the object is serialized.</param>
      public virtual void WriteXml(XmlWriter writer)
      {
         XmlStore.WriteElement<string>(writer, "Name", Name);
         if (fDescription != null)
            writer.WriteRaw(fDescription);
         else
         {
            writer.WriteStartElement("FlowDocument");
            writer.WriteEndElement();
         }
      }

      #endregion


      /// <summary>
      /// Removes the editor from the specified list.
      /// </summary>
      /// <remarks>
      /// This is a virtual function so that some editors can have multiple open documents (like a folder).
      /// </remarks>
      /// <param name="list">The list.</param>
      public virtual void RemoveEditorFrom(IList list)
      {
         list.Remove(this);
      }



      internal void RemoveFromOwner()
      {
         EditorFolder iFolder = Owner as EditorFolder;
         if (iFolder != null)
            iFolder.Items.Remove(this);
         else
            BrainData.BrainData.Current.Editors.Remove(this);
      }
   }
}
