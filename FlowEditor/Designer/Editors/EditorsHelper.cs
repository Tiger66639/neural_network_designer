﻿using System;
using System.Diagnostics;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using JaStDev.HAB.Designer.Dialogs;
using JaStDev.HAB.Designer.Editors.Flow.FlowItems;
using JaStDev.HAB.Designer.Editors.Frames;
using JaStDev.HAB.Designer.Editors.MindMap;
using JaStDev.HAB.Sins;
using Frame = JaStDev.HAB.Designer.Editors.Frames.Frame;

namespace JaStDev.HAB.Designer.Editors
{
   /// <summary>
   /// A helper class for managing editor objects: frames, objects,... 
   /// </summary>
   internal class EditorsHelper
   {
      /// <summary>
      /// Makes the object, which consists out of a cluster, a text neuron and a neuron that can be used as a meaning.
      /// </summary>
      /// <returns></returns>
      static public MindMapCluster MakeObject()
      {
         string iObjectName = null;
         DlgStringQuestion iIn = new DlgStringQuestion();
         iIn.Owner = App.Current.MainWindow;
         iIn.Question = "Object name:";
         iIn.Answer = "New object";
         iIn.Title = "New object";
         if ((bool)iIn.ShowDialog() == true)
         {
            iObjectName = iIn.Answer;
            NeuronCluster iCluster = new NeuronCluster();
            iCluster.Meaning = (ulong)PredefinedNeurons.Object;
            WindowMain.AddItemToBrain(iCluster);                                                 //we use this function cause it takes care of the undo data.
            BrainData.BrainData.Current.NeuronInfo[iCluster.ID].DisplayTitle = iObjectName;

            Neuron iNeuron = new Neuron();
            WindowMain.AddItemToBrain(iNeuron);                                                 //we use this function cause it takes care of the undo data.
            using (ChildrenAccessor iList = iCluster.ChildrenW)
               iList.Add(iNeuron);
            BrainData.BrainData.Current.DefaultMeaningIds.Add(iNeuron.ID);
            BrainData.BrainData.Current.NeuronInfo[iNeuron.ID].DisplayTitle = iObjectName;

            ulong iTextId;
            TextNeuron iText;
            string iTextName = iObjectName.ToLower();                                           //all dict items are lowercase.
            if (TextSin.Words.TryGetValue(iTextName, out iTextId) == true)                      //could be that this word already exists, if so, we reuse it, cause an item can only be once in the dict.
               iText = Brain.Brain.Current[iTextId] as TextNeuron;
            else
            {
               iText = new TextNeuron();
               iText.Text = iObjectName;
               WindowMain.AddItemToBrain(iText);                                                 //we use this function cause it takes care of the undo data.
               BrainData.BrainData.Current.NeuronInfo[iText.ID].DisplayTitle = iTextName;
               TextSin.Words[iTextName] = iText.ID;
            }
            using (ChildrenAccessor iList = iCluster.ChildrenW)
               iList.Add(iText);

            return MindMapNeuron.CreateFor(iCluster) as MindMapCluster;
         }
         return null;
      }

      /// <summary>
      /// Asks the user for the name of a new frame and creates this frame.
      /// </summary>
      /// <returns>A neuron cluster that represents the frame.</returns>
      static public Frame MakeFrame()
      {
         string iObjectName = null;
         DlgStringQuestion iIn = new DlgStringQuestion();
         iIn.Owner = App.Current.MainWindow;
         iIn.Question = "Frame name:";
         iIn.Answer = "New frame";
         iIn.Title = "New frame";
         if ((bool)iIn.ShowDialog() == true)
         {
            iObjectName = iIn.Answer;
            NeuronCluster iCluster = new NeuronCluster();
            iCluster.Meaning = (ulong)PredefinedNeurons.Frame;
            WindowMain.AddItemToBrain(iCluster);                                                 //we use this function cause it takes care of the undo data.
            BrainData.BrainData.Current.NeuronInfo[iCluster.ID].DisplayTitle = iObjectName;
            return new Frame(iCluster);
         }
         return null;
      }

      /// <summary>
      /// Asks the user for the name of a new object to be used as a frame element and creates it.
      /// </summary>
      /// <returns>null if the user canceled, otherwise a new object wrapper for a cluster.</returns>
      public static FrameElement MakeFrameElement()
      {
         string iObjectName = null;
         DlgStringQuestion iIn = new DlgStringQuestion();
         iIn.Owner = App.Current.MainWindow;
         iIn.Question = "Object name for element:";
         iIn.Answer = "New object";
         iIn.Title = "New object";
         if ((bool)iIn.ShowDialog() == true)
         {
            iObjectName = iIn.Answer;
            NeuronCluster iCluster = new NeuronCluster();
            iCluster.Meaning = (ulong)PredefinedNeurons.Object;
            WindowMain.AddItemToBrain(iCluster);                                                 //we use this function cause it takes care of the undo data.
            BrainData.BrainData.Current.NeuronInfo[iCluster.ID].DisplayTitle = iObjectName;

            ulong iTextId;
            TextNeuron iText;
            string iTextName = iObjectName.ToLower();                                           //all dict items are lowercase.
            if (TextSin.Words.TryGetValue(iTextName, out iTextId) == true)                      //could be that this word already exists, if so, we reuse it, cause an item can only be once in the dict.
               iText = Brain.Brain.Current[iTextId] as TextNeuron;
            else
            {
               iText = new TextNeuron();
               iText.Text = iObjectName;
               WindowMain.AddItemToBrain(iText);                                                 //we use this function cause it takes care of the undo data.
               BrainData.BrainData.Current.NeuronInfo[iText.ID].DisplayTitle = iTextName;
               TextSin.Words[iTextName] = iText.ID;
            }
            using (ChildrenAccessor iList = iCluster.ChildrenW)
               iList.Add(iText);

            return new FrameElement(iCluster);
         }
         return null;
      }

      /// <summary>
      /// Asks the user for the name of a new object to be used as a frame evoker and creates it.
      /// </summary>
      /// <returns>null if the user canceled, otherwise a new object wrapper for a cluster.</returns>
      public static FrameEvoker MakeFrameEvoker()
      {
         string iObjectName = null;
         DlgStringQuestion iIn = new DlgStringQuestion();
         iIn.Owner = App.Current.MainWindow;
         iIn.Question = "Object name for evoker:";
         iIn.Answer = "New object";
         iIn.Title = "New object";
         if ((bool)iIn.ShowDialog() == true)
         {
            iObjectName = iIn.Answer;
            NeuronCluster iCluster = new NeuronCluster();
            iCluster.Meaning = (ulong)PredefinedNeurons.Object;
            WindowMain.AddItemToBrain(iCluster);                                                 //we use this function cause it takes care of the undo data.
            BrainData.BrainData.Current.NeuronInfo[iCluster.ID].DisplayTitle = iObjectName;

            ulong iTextId;
            TextNeuron iText;
            string iTextName = iObjectName.ToLower();                                           //all dict items are lowercase.
            if (TextSin.Words.TryGetValue(iTextName, out iTextId) == true)                      //could be that this word already exists, if so, we reuse it, cause an item can only be once in the dict.
               iText = Brain.Brain.Current[iTextId] as TextNeuron;
            else
            {
               iText = new TextNeuron();
               iText.Text = iObjectName;
               WindowMain.AddItemToBrain(iText);                                                 //we use this function cause it takes care of the undo data.
               BrainData.BrainData.Current.NeuronInfo[iText.ID].DisplayTitle = iTextName;
               TextSin.Words[iTextName] = iText.ID;
            }
            using (ChildrenAccessor iList = iCluster.ChildrenW)
               iList.Add(iText);

            return new FrameEvoker(iCluster);
         }
         return null;
      }

      /// <summary>
      /// Asks the user for the name of a new object to be used as a frame sequence and creates it.
      /// </summary>
      /// <returns>null if the user canceled, otherwise a new object wrapper for a cluster.</returns>
      public static FrameSequence MakeFrameSequence()
      {
         string iObjectName = null;
         DlgStringQuestion iIn = new DlgStringQuestion();
         iIn.Owner = App.Current.MainWindow;
         iIn.Question = "sequence name:";
         iIn.Answer = "New sequence";
         iIn.Title = "New sequence";
         if ((bool)iIn.ShowDialog() == true)
         {
            iObjectName = iIn.Answer;
            NeuronCluster iCluster = new NeuronCluster();
            iCluster.Meaning = (ulong)PredefinedNeurons.FrameSequence;
            WindowMain.AddItemToBrain(iCluster);                                                 //we use this function cause it takes care of the undo data.
            BrainData.BrainData.Current.NeuronInfo[iCluster.ID].DisplayTitle = iObjectName;
            return new FrameSequence(iCluster);
         }
         return null;
      }

      /// <summary>
      /// Asks the user for the name of a new flow and creates it if 'ok' is pressed.
      /// </summary>
      /// <returns>Null if the user canceled, otherwise a new flow.</returns>
      public static Flow.FlowItems.Flow MakeFlow()
      {
         string iObjectName = null;
         DlgStringQuestion iIn = new DlgStringQuestion();
         iIn.Owner = App.Current.MainWindow;
         iIn.Question = "Flow name:";
         iIn.Answer = "New flow";
         iIn.Title = "New flow";
         if ((bool)iIn.ShowDialog() == true)
         {
            iObjectName = iIn.Answer;
            NeuronCluster iCluster = new NeuronCluster();
            iCluster.Meaning = (ulong)PredefinedNeurons.Flow;
            WindowMain.AddItemToBrain(iCluster);                                                 //we use this function cause it takes care of the undo data.
            Flow.FlowItems.Flow iRes = new Flow.FlowItems.Flow();
            iRes.ItemID = iCluster.ID;
            iRes.NeuronInfo.DisplayTitle = iObjectName;
            iRes.Name = iObjectName;
            return iRes;
         }
         return null;
      }

      /// <summary>
      /// Creates a neuron cluster (meaning FlowItemConditional) and a new option (conditiona statement) for a flow that
      /// wraps the cluster.
      /// </summary>
      public static FlowItemConditional MakeFlowOption()
      {
         NeuronCluster iCluster = new NeuronCluster();
         iCluster.Meaning = (ulong)PredefinedNeurons.FlowItemConditional;
         WindowMain.AddItemToBrain(iCluster);                                                 //we use this function cause it takes care of the undo data.
         Link iLink = new Link(Brain.Brain.Current[(ulong)PredefinedNeurons.False], iCluster, (ulong)PredefinedNeurons.FlowItemIsLoop);
         FlowItemConditional iRes = new FlowItemConditional(iCluster);
         return iRes;
      }

      /// <summary>
      /// Creates a neuron cluster (meaning FlowItemConditional) and a new loop (conditiona statement) for a flow that
      /// wraps the cluster.
      /// </summary>
      public static FlowItemConditional MakeFlowLoop()
      {
         NeuronCluster iCluster = new NeuronCluster();
         iCluster.Meaning = (ulong)PredefinedNeurons.FlowItemConditional;
         WindowMain.AddItemToBrain(iCluster);                                                 //we use this function cause it takes care of the undo data.
         Link iLink = new Link(Brain.Brain.Current[(ulong)PredefinedNeurons.True], iCluster, (ulong)PredefinedNeurons.FlowItemIsLoop);
         FlowItemConditional iRes = new FlowItemConditional(iCluster);
         return iRes;
      }

      /// <summary>
      /// Creates a neuron cluster (meaning FlowItemConditionalPart) and a new conditional statement part for a flow that
      /// wraps the cluster.
      /// </summary>
      /// <remarks>
      /// A conditional statement part is the '|' in {|||} and [|||]
      /// </remarks>
      /// <returns></returns>
      internal static FlowItemConditionalPart MakeFlowCondPart()
      {
         NeuronCluster iCluster = new NeuronCluster();
         iCluster.Meaning = (ulong)PredefinedNeurons.FlowItemConditionalPart;
         WindowMain.AddItemToBrain(iCluster);                                                 //we use this function cause it takes care of the undo data.
         FlowItemConditionalPart iRes = new FlowItemConditionalPart(iCluster);
         return iRes;
      }

      internal static void InsertFlowItem(FlowItem toAdd, FlowItem relativeTo)
      {
         FlowItemBlock iOwner = relativeTo.Owner as FlowItemBlock;
         if (iOwner == null)
         {
            Flow.FlowItems.Flow iFlow = relativeTo.Owner as Flow.FlowItems.Flow;
            Debug.Assert(iFlow != null);
            iFlow.Items.Insert(iFlow.Items.IndexOf(relativeTo), toAdd);
         }
         else
         {
            if (!(toAdd is FlowItemConditionalPart))
            {
               if (iOwner is FlowItemConditionalPart)
                  iOwner.Items.Insert(iOwner.Items.IndexOf(relativeTo), toAdd);
               else
               {
                  FlowItemConditionalPart iNewPart = EditorsHelper.MakeFlowCondPart();
                  iOwner.Items.Insert(iOwner.Items.IndexOf(relativeTo), iNewPart);
                  iNewPart.Items.Add(toAdd);
               }
            }
            else
            {
               if (iOwner is FlowItemConditional)
                  iOwner.Items.Insert(iOwner.Items.IndexOf(relativeTo), toAdd);
               else
               {
                  FlowItemBlock iBlock = iOwner.Owner as FlowItemBlock;
                  if (iBlock == null)
                  {
                     Flow.FlowItems.Flow iFlow = relativeTo.Owner as Flow.FlowItems.Flow;
                     Debug.Assert(iFlow != null);
                     iFlow.Items.Insert(iFlow.Items.IndexOf(iOwner), toAdd);
                  }
                  else
                     iBlock.Items.Insert(iBlock.Items.IndexOf(iOwner), toAdd);
               }
            }
         }
         toAdd.IsSelected = true;                                                                           //we do this at the end, when it has been added to the list, this way, all the others are also updated correctly.




      }

      /// <summary>
      /// Adds a flow item relative to the second flow item.  Relative to means:
      /// - for static: add at end of owner of static.
      /// - for conditional part: add at end of the part
      /// - fo conditional: add at end of conditional, make certain that there is a new part also added if the added item is not a part.
      /// </summary>
      /// <param name="toAdd">To add.</param>
      /// <param name="relativeTo">The relative to.</param>
      /// <param name="flow">The flow that should own the item. This is required in case relativeTo is empty.</param>
      internal static void AddFlowItem(FlowItem toAdd, FlowItem relativeTo, Flow.FlowItems.Flow flow)
      {
         if (relativeTo is FlowItemStatic)
         {
            FlowItemBlock iOwner = relativeTo.Owner as FlowItemBlock;
            if (iOwner == null)
            {
               Debug.Assert(flow != null);
               flow.Items.Add(toAdd);
            }
            else
            {
               if (toAdd is FlowItemConditionalPart)                                      //we are adding a | when we are on a static, so we need to go up 1 more owner, so we add to the loop/option
                  iOwner = (FlowItemBlock)iOwner.Owner;
               iOwner.Items.Add(toAdd);
            }
         }
         else if (relativeTo is FlowItemConditional)
         {
            if (!(toAdd is FlowItemConditionalPart))
            {
               FlowItemConditionalPart iNewPart = EditorsHelper.MakeFlowCondPart();
               ((FlowItemConditional)relativeTo).Items.Add(iNewPart);
               iNewPart.Items.Add(toAdd);
            }
            else
               ((FlowItemConditional)relativeTo).Items.Add(toAdd);
         }
         else if (relativeTo is FlowItemConditionalPart)
            ((FlowItemConditionalPart)relativeTo).Items.Add(toAdd);
         else if (relativeTo == null)
         {
            Debug.Assert(flow != null);
            flow.Items.Add(toAdd);
         }
         else
            throw new InvalidOperationException("Unkown flowItem, don't know how to add relative to it.");
         toAdd.IsSelected = true;                                                                           //we do this at the end, when it has been added to the list, this way, all the others are also updated correctly.
      }

      /// <summary>
      /// Creates a wrapper flow item for the specified neuron. It checks if the neuron is a cluster with as meaning 'FlowITemConditional'
      /// or 'FlowItemConditionalPart' and creates an appropriate item accordingly.
      /// </summary>
      /// <param name="neuron">The neuron.</param>
      /// <returns></returns>
      public static FlowItem CreateFlowItemFor(Neuron neuron)
      {
         NeuronCluster iCluster = neuron as NeuronCluster;
         if (iCluster != null)
         {
            if (iCluster.Meaning == (ulong)PredefinedNeurons.FlowItemConditional)
               return new FlowItemConditional(iCluster);
            else if (iCluster.Meaning == (ulong)PredefinedNeurons.FlowItemConditionalPart)
               return new FlowItemConditionalPart(iCluster);
         }
         return new FlowItemStatic(neuron);
      }
   }
}
