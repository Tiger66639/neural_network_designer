﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Editors.@base;

namespace JaStDev.HAB.Designer.Editors.Flow.FlowItems
{
   /// <summary>
   /// The base class for flow items like statics, and conditional items.
   /// </summary>
   public class FlowItem: EditorItem
   {
      public FlowItem(Neuron towrap): base(towrap)
      {

      }
   }
}
