﻿using JaStDev.HAB.Brain;

namespace JaStDev.HAB.Designer.Editors.Flow.FlowItems
{
   /// <summary>
   /// A single list of items within a loop or option.
   /// </summary>
   public class FlowItemConditionalPart : FlowItemBlock
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="FlowItemConditionalPart"/> class.
      /// </summary>
      /// <param name="toWrap">To wrap.</param>
      public FlowItemConditionalPart(NeuronCluster toWrap): base(toWrap)
      {

      }
   }
}
