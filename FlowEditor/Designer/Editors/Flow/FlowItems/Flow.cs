﻿using System.Collections;
using System.Windows;
using System.Xml.Serialization;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Editors.@base;
using JaStDev.HAB.Designer.Editors.Frames;

namespace JaStDev.HAB.Designer.Editors.Flow.FlowItems
{
    /// <summary>
    ///     Provides a way to visually express interoperability between <see cref="Frame" />s over a period
    ///     of time.  Ex: conversation flow.
    /// </summary>
    public class Flow : NeuronEditor, IEditorSelection
    {
        #region IEditorSelection Members

        /// <summary>
        ///     Gets/sets the currently selected item. If there are multiple selections, the first is returned.
        /// </summary>
        /// <value></value>
        [XmlIgnore]
        object IEditorSelection.SelectedItem
        {
            get { return SelectedItem; }
        }

        #endregion

        /// <summary>
        ///     Registers the item that was read from xml.
        /// </summary>
        /// <remarks>
        ///     This must be called when the editor is read from xml.  In that situation, the
        ///     brainData isn't always loaded properly yet.  At this point, this can be resolved.
        ///     It is called by the brainData.
        /// </remarks>
        public override void RegisterItem()
        {
            base.RegisterItem();
            LoadItems();
        }

        /// <summary>
        ///     Builds a <see cref="FlowItem" /> for each neuron in the wrapped cluster.
        /// </summary>
        private void LoadItems()
        {
            Items = new FlowItemCollection(this, (NeuronCluster) Item);
        }

        #region fields

        private FlowItemCollection fItems;
        private readonly EditorItemSelectionList<FlowItem> fSelectedItems = new EditorItemSelectionList<FlowItem>();
        private bool fIsSelected;
        private UIElement fPopupTarget;
        private bool fPopupDoesInsert;
        private bool fPopupIsOpen;

        #endregion

        #region Prop

        #region Icon

        /// <summary>
        ///     Gets the resource path to the icon that should be used for this editor.  This is usually class specific.
        /// </summary>
        /// <value></value>
        public override string Icon
        {
            get { return "/Images/Flow/flow.png"; }
        }

        #endregion

        #region  IsSelected

        /// <summary>
        ///     Gets/sets if the flow is currently selected or not.
        /// </summary>
        /// <remarks>
        ///     Used to select the item visually, must be bound to from xaml.
        ///     We override, cause we don't want to update the editors list.
        /// </remarks>
        [XmlIgnore]
        public override bool IsSelected
        {
            get { return fIsSelected; }
            set
            {
                fIsSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        #endregion

        public override string NamePrefix
        {
            get { return "Flow"; }
        }

        #region Items

        /// <summary>
        ///     Gets the list of flow items defined in this flow.
        /// </summary>
        public FlowItemCollection Items
        {
            get { return fItems; }
            internal set
            {
                fItems = value;
                OnPropertyChanged("Items");
            }
        }

        #endregion

        #region ItemID

        /// <summary>
        ///     Gets or sets the ID of the item that this object provides an editor for.
        /// </summary>
        /// <value>The ID of the item.</value>
        /// <remarks>
        ///     When read from xml, use <see cref="NeuronEditor.RegisterItem" /> to properly attach the item.
        /// </remarks>
        public override ulong ItemID
        {
            get { return base.ItemID; }
            set
            {
                base.ItemID = value;
                if (Item != null)
                    LoadItems();
            }
        }

        #endregion

        #region PopupDoesInsert

        /// <summary>
        ///     Gets/sets the wether the popup does an insert or an add.  This prop is provided so that the editor can easely link
        ///     this
        ///     data with the visualizer for this object.
        /// </summary>
        public bool PopupDoesInsert
        {
            get { return fPopupDoesInsert; }
            set
            {
                fPopupDoesInsert = value;
                OnPropertyChanged("PopupDoesInsert");
            }
        }

        #endregion

        #region PopupTarget

        /// <summary>
        ///     Gets/sets the the target of the pupup. This prop is provided so that the editor can easely link this
        ///     data with the visualizer for this object.
        /// </summary>
        public UIElement PopupTarget
        {
            get { return fPopupTarget; }
            set
            {
                fPopupTarget = value;
                OnPropertyChanged("PopupTarget");
            }
        }

        #endregion

        #region PopupIsOpen

        /// <summary>
        ///     Gets/sets wether the popup should be opened or closed. This prop is provided so that the editor can easely link
        ///     this
        ///     data with the visualizer for this object.
        /// </summary>
        public bool PopupIsOpen
        {
            get { return fPopupIsOpen; }
            set
            {
                fPopupIsOpen = value;
                OnPropertyChanged("PopupIsOpen");
            }
        }

        #endregion

        #endregion

        #region IEditorSelection Members

        /// <summary>
        ///     Gets the list of selected items.
        /// </summary>
        /// <value>The selected items.</value>
        [XmlIgnore]
        public IList SelectedItems
        {
            get { return fSelectedItems; }
        }

        /// <summary>
        ///     Gets/sets the currently selected item. If there are multiple selections, the first is returned.
        /// </summary>
        /// <value></value>
        [XmlIgnore]
        public FlowItem SelectedItem
        {
            get
            {
                if (fSelectedItems.Count > 0)
                    return fSelectedItems[0];
                return null;
            }
        }

        #endregion
    }
}