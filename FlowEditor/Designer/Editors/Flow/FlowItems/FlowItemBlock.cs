﻿using System.Windows.Controls;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Editors.@base;
using JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems;

namespace JaStDev.HAB.Designer.Editors.Flow.FlowItems
{
   /// <summary>
   /// Base class for flow items that contain other flow items.
   /// </summary>
   /// <typeparam name="T">The type of children allowed.</typeparam>
   public class FlowItemBlock: FlowItem 
   {
      #region fields

      FlowItemCollection fItems;
      Orientation fOrientation;

      #endregion

      public FlowItemBlock(NeuronCluster toWrap): base(toWrap)
      {
         Items = new FlowItemCollection(this, toWrap);
      }


      #region Items

      /// <summary>
      /// Gets the list of flow items defined in this flow.
      /// </summary>
      public FlowItemCollection Items
      {
         get { return fItems; }
         internal set 
         { 
            fItems = value;
            OnPropertyChanged("Items");
         }
      }

      #endregion

      
      #region Orientation

      /// <summary>
      /// Gets/sets the orientation of the wrappanel, which determins how the items are displayed: horizontally-wrapped or vertically stacked.
      /// </summary>
      public Orientation Orientation
      {
         get
         {
            return fOrientation;
         }
         set
         {
            OnPropertyChanging("Orientation", fOrientation, value);
            fOrientation = value;
            OnPropertyChanged("Orientation");
         }
      }

      #endregion

      /// <summary>
      /// Called when the <see cref="CodeItem.Item"/> has changed.
      /// </summary>
      /// <param name="value">The value.</param>
      protected override void OnItemChanged(Neuron value)
      {
         base.OnItemChanged(value);
         Items = new FlowItemCollection(this, (NeuronCluster)value);
      }

      /// <summary>
      /// Removes the current code item from the code list, but not the actual neuron that represents the code
      /// item, this stays in the brain, it is simply no longer used in this code list.
      /// </summary>
      /// <param name="child"></param>
      public override void RemoveChildFromCode(EditorItem child)
      {
         FlowItem iChild = (FlowItem)child;
         if (Items.Remove(iChild) == false)
            base.RemoveChildFromCode(child);
      }
   }
}
