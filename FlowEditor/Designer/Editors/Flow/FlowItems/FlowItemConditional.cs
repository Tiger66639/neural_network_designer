﻿using System.Collections.Specialized;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Events;

namespace JaStDev.HAB.Designer.Editors.Flow.FlowItems
{
   /// <summary>
   /// A loop or option.
   /// </summary>
   public class FlowItemConditional : FlowItemBlock
   {
      #region fields
      bool fIsLooped;
      string fBackSign;
      string fFrontSign;
      /// <summary>
      /// Default width of a single line.
      /// </summary>
      const double LignWidth = 4.3;
      double fSignWidth = LignWidth;
      #endregion

      #region ctor
      /// <summary>
      /// Initializes a new instance of the <see cref="FlowItemConditional"/> class.
      /// </summary>
      /// <param name="toWrap">To wrap.</param>
      public FlowItemConditional(NeuronCluster toWrap)
         : base(toWrap)
      {
         Items.CollectionChanged += new NotifyCollectionChangedEventHandler(Items_CollectionChanged);
         SignWidth = LignWidth + (Items.Count * LignWidth);                                                    //when initially created, set the signwidh, based on the nr of items loaded.  if we don't do this, the init is incorrect for loaded items.
      }

      #endregion

      #region prop
      #region IsLooped

      /// <summary>
      /// Gets/sets wether the conditional flow item is looped or not.
      /// </summary>
      public bool IsLooped
      {
         get
         {
            return fIsLooped;
         }
         set
         {
            if (value != fIsLooped)
            {
               OnPropertyChanging("IsLooped", fIsLooped, value);
               InternalSetIsLooped(value);
               InternalChange = true;
               try
               {
                  if (value == true)
                     Item.SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.FlowItemIsLoop, Brain.Brain.Current[(ulong)PredefinedNeurons.True]);
                  else
                     Item.SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.FlowItemIsLoop, Brain.Brain.Current[(ulong)PredefinedNeurons.False]);
               }
               finally
               {
                  InternalChange = false;
               }
            }
         }
      }


      #endregion 

      #region FrontSign

      /// <summary>
      /// Gets the string to put in front of the conditional, to indicate it is a loop '{' or option '['
      /// </summary>
      public string FrontSign
      {
         get { return fFrontSign; }
         internal set 
         { 
            fFrontSign = value;
            OnPropertyChanged("FrontSign");
         }
      }

      #endregion

      #region BackSign

      /// <summary>
      /// Gets the string to put in the back of the conditional, to indicate it is a loop '}' or option ']'
      /// </summary>
      public string BackSign
      {
         get
         {
            return fBackSign;
         }
         set
         {
            fBackSign = value;
            OnPropertyChanged("BackSign");
         }
      }

      #endregion

      
      #region SignWidth

      /// <summary>
      /// Gets/sets the height that should be applied to the front and back sign.  This is calculated based on the nr of 
      /// items in the collection * 8.
      /// </summary>
      public double SignWidth
      {
         get
         {
            return fSignWidth;
         }
         set
         {
            fSignWidth = value;
            OnPropertyChanged("SignWidth");
         }
      }

      #endregion

      #endregion


      #region Functions

      void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         SignWidth = LignWidth + (Items.Count * LignWidth);
      } 

      /// <summary>
      /// Stores the actual value for IsLooped and raises the PropertyChanged.
      /// </summary>
      /// <param name="value">if set to <c>true</c> [value].</param>
      private void InternalSetIsLooped(bool value)
      {
         fIsLooped = value;
         OnPropertyChanged("IsLooped");
         if (value == true)
         {
            FrontSign = "{";
            BackSign = "}";
         }
         else
         {
            FrontSign = "[";
            BackSign = "]";
         }
      }

      /// <summary>
      /// descendents that need to update links that changed can do this through this function.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      protected override void InternalLinkChanged(Link sender, LinkChangedEventArgs e)
      {
         base.InternalLinkChanged(sender, e);
         if (sender.MeaningID == (ulong)PredefinedNeurons.FlowItemIsLoop)
            if (e.Action == BrainAction.Removed)
               InternalSetIsLooped(false);
            else
               InternalSetIsLooped(e.NewTo == (ulong)PredefinedNeurons.True);
      }

      protected override void OnItemChanged(Neuron value)
      {
         base.OnItemChanged(value);
         Neuron iFound = Item.FindFirstOut((ulong)PredefinedNeurons.FlowItemIsLoop);
         InternalSetIsLooped(iFound != null && iFound.ID == (ulong)PredefinedNeurons.True);
      }

      #endregion
   }
}
