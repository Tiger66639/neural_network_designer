﻿using System;
using System.Windows;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Serialization;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.BrainData;
using JaStDev.HAB.Designer.Editors.@base;
using JaStDev.HAB.Designer.WPF.BrainEventManagers;
using JaStDev.HAB.Events;

namespace JaStDev.HAB.Designer.Editors.Flow.FlowItems
{
    public class FlowEditor : EditorBase, IWeakEventListener
    {
        #region ctor

        /// <summary>
        ///     Initializes a new instance of the <see cref="FlowEditor" /> class.
        /// </summary>
        public FlowEditor()
        {
            Flows = new ObservedCollection<Flow>(this);
        }

        #endregion

        /// <summary>
        ///     Creates the correct <see cref="FlowItem" /> for the specified neuron.
        /// </summary>
        /// <param name="toWrap">To wrap.</param>
        /// <returns></returns>
        public static FlowItem CreateFlowItemFor(Neuron toWrap)
        {
            var iToWrap = toWrap as NeuronCluster;
            if (iToWrap != null)
            {
                if (iToWrap.Meaning == (ulong) PredefinedNeurons.FlowItemConditional)
                    return new FlowItemConditional(iToWrap);
                if (iToWrap.Meaning == (ulong) PredefinedNeurons.FlowItemConditionalPart)
                    return new FlowItemConditionalPart(iToWrap);
                return new FlowItemStatic(toWrap);
            }
            return new FlowItemStatic(toWrap);
        }

        /// <summary>
        ///     need to reasign the list and the selected item so that they are change, this will force a complete redraw.
        /// </summary>
        internal void ReDrawFlows()
        {
            var iSelected = SelectedFlow;
            ObservedCollection<Flow> iList = Flows;
            Flows = null;
            OnPropertyChanged("Flows");
            SelectedFlow = null;
            Flows = iList;
            OnPropertyChanged("Flows");
            SelectedFlow = iSelected;
        }

        #region Fields

        private Flow fSelectedFlow;
        private bool fDisplayAsList;

        #endregion

        #region Prop

        #region Icon

        /// <summary>
        ///     Gets the resource path to the icon that should be used for this editor.  This is usually class specific.
        /// </summary>
        /// <value></value>
        public override string Icon
        {
            get { return "/Images/Flow/flow.png"; }
        }

        #endregion

        #region DescriptionTitle

        /// <summary>
        ///     Gets a title that the description editor can use to display in the header.
        /// </summary>
        /// <value></value>
        public override string DescriptionTitle
        {
            get { return Name + " - Flow Editor"; }
        }

        #endregion

        #region Flows

        /// <summary>
        ///     Gets the list of frames
        /// </summary>
        public ObservedCollection<Flow> Flows { get; private set; }

        #endregion

        #region SelectedFlow

        /// <summary>
        ///     Gets/sets the currently selected flow.
        /// </summary>
        /// <remarks>
        ///     This is provided so that we can change the active flow easely from code.
        /// </remarks>
        public Flow SelectedFlow
        {
            get { return fSelectedFlow; }
            set
            {
                fSelectedFlow = value;
                OnPropertyChanged("SelectedFlow");
            }
        }

        #endregion

        #region DisplayAsList

        /// <summary>
        ///     Gets/sets wether the flows are displayed in a list or in a single detail view.
        /// </summary>
        public bool DisplayAsList
        {
            get { return fDisplayAsList; }
            set
            {
                if (value != fDisplayAsList)
                {
                    fDisplayAsList = value;

                    OnPropertyChanged("DisplayAsList");
                }
            }
        }

        #endregion

        //#region ListVisibility

        ///// <summary>
        ///// Gets the visibility value for the flow list.  This is automatically set when <see cref="FlowEditor.DisplayAsList"/> is changed.
        ///// </summary>
        //public Visibility ListVisibility
        //{
        //   get
        //   {
        //      return fListVisibility;
        //   }
        //   internal set
        //   {
        //      fListVisibility = value;
        //      OnPropertyChanged("ListVisibility");
        //   }
        //}

        //#endregion


        //#region ListVisibility

        ///// <summary>
        ///// Gets the visibility value for the single item flow.  This is automatically set when <see cref="FlowEditor.DisplayAsList"/> is changed.
        ///// </summary>
        //public Visibility ItemVisibility
        //{
        //   get
        //   {
        //      return fItemVisibility;
        //   }
        //   internal set
        //   {
        //      fItemVisibility = value;
        //      OnPropertyChanged("ItemVisibility");
        //   }
        //}

        //#endregion

        #endregion

        #region Functions

        /// <summary>
        ///     Reads the fields/properties of the class.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns>
        ///     True if the item was properly read, otherwise false.
        /// </returns>
        /// <remarks>
        ///     This function is called for each element that is found, so this function should check which element it is
        ///     and only read that element accordingly.
        /// </remarks>
        protected override bool ReadXmlInternal(XmlReader reader)
        {
            if (reader.Name == "Flow")
            {
                var iNeuronSer = new XmlSerializer(typeof(Flow));
                var iNode = (Flow) iNeuronSer.Deserialize(reader);
                Flows.Add(iNode);
                return true;
            }

            return base.ReadXmlInternal(reader);
        }

        /// <summary>
        ///     Converts an object into its XML representation.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Xml.XmlWriter" /> stream to which the object is serialized.</param>
        public override void WriteXml(XmlWriter writer)
        {
            base.WriteXml(writer);
            var iFlowSer = new XmlSerializer(typeof(Flow));
            foreach (Flow i in Flows)
                iFlowSer.Serialize(writer, i);
        }

        #endregion

        #region IWeakEventListener

        /// <summary>
        ///     Receives events from the centralized event manager.
        /// </summary>
        /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager" /> calling this method.</param>
        /// <param name="sender">Object that originated the event.</param>
        /// <param name="e">Event data.</param>
        /// <returns>
        ///     true if the listener handled the event. It is considered an error by the
        ///     <see cref="T:System.Windows.WeakEventManager" /> handling in WPF to register a listener for an event that the
        ///     listener does not handle. Regardless, the method should return false if it receives an event that it does not
        ///     recognize or handle.
        /// </returns>
        public virtual bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            if (managerType == typeof(NeuronChangedEventManager))
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                    new Action<NeuronChangedEventArgs>(Item_NeuronChanged), e);
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Called when the wrapped item is changed. When removed, makes certain that the <see cref="BrainData.CodeEditors" />
        ///     list is updated.
        /// </summary>
        /// <param name="e">The <see cref="NeuronChangedEventArgs" /> instance containing the event data.</param>
        protected virtual void Item_NeuronChanged(NeuronChangedEventArgs e)
        {
            if (e.Action == BrainAction.Removed)
            {
                var iFound = (from i in Flows where i.Item == e.OriginalSource select i).FirstOrDefault();
                if (iFound != null)
                    Flows.Remove(iFound);
            }
        }

        #endregion
    }
}