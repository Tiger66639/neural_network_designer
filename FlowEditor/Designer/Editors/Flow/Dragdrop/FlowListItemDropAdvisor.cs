﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using JaStDev.HAB.Designer.DragDrop;
using JaStDev.HAB.Designer.Editors.Flow.FlowItems;

namespace JaStDev.HAB.Designer.Editors.Flow.Dragdrop
{
   public class FlowListItemDropAdvisor : DropTargetBase
   {
      #region prop

      #region UsePreviewEvents
      /// <summary>
      /// Gets if the preview event versions should be used or not.
      /// </summary>
      /// <remarks>
      /// don't use preview events cause than the sub lists don't get used but only the main list cause this gets the events first,
      /// while we usually want to drop in a sublist.
      /// </remarks>
      public override bool UsePreviewEvents
      {
         get
         {
            return false;
         }
      }
      #endregion

      #region CodeList

      /// <summary>
      /// Gets the list containing all the code that the UI to which advisor is attached too, displays data for.
      /// </summary>
      public FlowItemCollection CodeList
      {
         get
         {
            ItemsControl iItemsControl = ItemsControl.ItemsControlFromItemContainer(TargetUI);
            Debug.Assert(iItemsControl != null);
            return iItemsControl.ItemsSource as FlowItemCollection;
         }
      }

      #endregion
      #endregion

      #region Overrides
      public override void OnDropCompleted(DragEventArgs arg, Point dropPoint)
      {
         FlowItem iItem = arg.Data.GetData(Properties.Resources.FLOWITEMFORMAT) as FlowItem;
         if (iItem != null && CodeList.Contains(iItem) == true)
            TryMoveItem(arg, dropPoint, iItem);
         else
            TryCreateNewCodeItem(arg, dropPoint);                                                  //we are not moving around an item, so add new code item.
      }


      public override bool IsValidDataObject(IDataObject obj)
      {
         return obj.GetDataPresent(Properties.Resources.FLOWITEMFORMAT)
                || obj.GetDataPresent(Properties.Resources.NeuronIDFormat);
      }

      #endregion

      private void TryMoveItem(DragEventArgs arg, Point dropPoint, FlowItem iItem)
      {
         FlowItem iDroppedOn = ((FrameworkElement)TargetUI).DataContext as FlowItem;
         FlowItemCollection iList = CodeList;
         int iNewIndex = iList.IndexOf(iDroppedOn);
         int iOldIndex = iList.IndexOf(iItem);
         if (iOldIndex == -1)
         {
            FlowItem iNew = FlowEditor.CreateFlowItemFor(iItem.Item);                                                         //we always create a new item, even with a move, cause we don't know how the item was being dragged (with shift pressed or not), this is the savest and easiest way.
            iList.Insert(iNewIndex, iNew);
         }
         else
         {
            iList.Move(iOldIndex, iNewIndex);
            arg.Effects = DragDropEffects.None;
         }
      }

      private void TryCreateNewCodeItem(DragEventArgs obj, Point dropPoint)
      {
         ulong iId = (ulong)obj.Data.GetData(Properties.Resources.NeuronIDFormat);
         FlowItem iNew = FlowEditor.CreateFlowItemFor(Brain.Brain.Current[iId]);
         FlowItem iDroppedOn = ((FrameworkElement)TargetUI).DataContext as FlowItem;
         Debug.Assert(iNew != null && iDroppedOn != null);
         FlowItemCollection iCodeList = CodeList;
         Debug.Assert(iCodeList != null);
         iCodeList.InsertWithoutEvents(iCodeList.IndexOf(iDroppedOn), iNew);
      }
   }
}
