﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Dialogs;
using JaStDev.HAB.Designer.Editors.Flow.FlowItems;

namespace JaStDev.HAB.Designer.Editors.Flow.Views
{
   /// <summary>
   /// Interaction logic for FlowEditorView.xaml
   /// </summary>
   public partial class FlowEditorView : UserControl
   {
      public FlowEditorView()
      {
         InitializeComponent();
      }

      #region IsEditing

      /// <summary>
      /// IsEditing Dependency Property
      /// </summary>
      public static readonly DependencyProperty IsEditingProperty =
          DependencyProperty.Register("IsEditing", typeof(bool), typeof(FlowEditorView),
              new FrameworkPropertyMetadata((bool)false,
                  new PropertyChangedCallback(OnIsEditingChanged)));

      /// <summary>
      /// Gets or sets the IsEditing property.  This dependency property 
      /// indicates wether the currently selected item in the list is in edit mode or not.
      /// </summary>
      public bool IsEditing
      {
         get { return (bool)GetValue(IsEditingProperty); }
         set { SetValue(IsEditingProperty, value); }
      }

      /// <summary>
      /// Handles changes to the IsEditing property.
      /// </summary>
      private static void OnIsEditingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
      {
         ((FlowEditorView)d).OnIsEditingChanged(e);
      }

      /// <summary>
      /// Provides derived classes an opportunity to handle changes to the IsEditing property.
      /// </summary>
      protected virtual void OnIsEditingChanged(DependencyPropertyChangedEventArgs e)
      {
         //if ((bool)e.NewValue == false)
         //{
         //   if (fSelected != null)
         //      Keyboard.Focus(fSelected);                                               //need to focus to the container, and not the list so that nav keys keep working.
         //   else
         //      LstItems.Focus();
         //}
      }

      #endregion


      #region Readonly textboxes

      /// <summary>
      /// Handles the Click event of the EditTextBox control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void EditTextBox_Click(object sender, RoutedEventArgs e)
      {
         IsEditing = true;
      }


      /// <summary>
      /// Handles the LostFocus event of the TxtTitle control.
      /// </summary>
      /// <remarks>
      /// When looses focus, need to make certain that is editing is turned off.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void TxtTitle_LostKeybFocus(object sender, KeyboardFocusChangedEventArgs e)
      {
         IsEditing = false;
      }

      /// <summary>
      /// Handles the LostFocus event of the TxtTitle control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void TxtTitle_LostFocus(object sender, RoutedEventArgs e)
      {
         IsEditing = false;
      }



      /// <summary>
      /// Handles the PrvKeyDown event of the TxtTitle control.
      /// </summary>
      /// <remarks>
      /// when enter is pressed, need to stop editing.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.KeyEventArgs"/> instance containing the event data.</param>
      private void TxtTitle_PrvKeyDown(object sender, KeyEventArgs e)
      {
         if (e.Key == Key.Enter || e.Key == Key.Return)
            IsEditing = false;
      }

      #endregion

      #region Commands
      #region Rename
      /// <summary>
      /// Handles the Executed event of the Rename control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void Rename_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         IsEditing = true;
      }

      /// <summary>
      /// Handles the CanExecute event of the Rename control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void Rename_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         e.CanExecute = LstItems != null && LstItems.SelectedItem != null;
      } 
      #endregion

      #region Delete
      /// <summary>
      /// Handles the Executed event of the Delete control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowItems.Flow iToDelete = ((FrameworkElement)e.OriginalSource).DataContext as FlowItems.Flow;
         Debug.Assert(iToDelete != null);
         MessageBoxResult iRes = MessageBox.Show(string.Format("Delete flow: {0} from the project and the network?", iToDelete.Name), "Delete", MessageBoxButton.YesNo, MessageBoxImage.Question);
         if (iRes == MessageBoxResult.Yes)
            WindowMain.DeleteItemFromBrain(iToDelete.Item);                         //this triggers the delete in the flow editors that it is used.

      }

      /// <summary>
      /// Handles the Executed event of the Remove control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void Remove_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowEditor iEditor = (FlowEditor)DataContext;
         FlowItems.Flow iToDelete = ((FrameworkElement)e.OriginalSource).DataContext as FlowItems.Flow;
         Debug.Assert(iToDelete != null);
         MessageBoxResult iRes = MessageBox.Show(string.Format("Remove flow: {0} from the editor (but not from the network)?", iToDelete.Name), "Remove", MessageBoxButton.YesNo, MessageBoxImage.Question);
         if (iRes == MessageBoxResult.Yes)
            iEditor.Flows.Remove(iToDelete);
      }

      #endregion

      #region CanExecute
      /// <summary>
      /// Handles the CanExecute event of the InsertFlowItem Command.
      /// </summary>
      /// <remarks>
      /// We can add a flow item when the editor has a flow assigned.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void InsertFlowItem_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         if (LstItems != null)
         {
            FlowItems.Flow iFlow = LstItems.SelectedItem as FlowItems.Flow;
            if (iFlow != null)
            {
               e.CanExecute = iFlow.SelectedItem != null;
               return;
            }
         }
         e.CanExecute = false;
      }

      private void AddFlowItem_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         FlowEditor iEditor = (FlowEditor)DataContext;
         e.CanExecute = iEditor != null && iEditor.SelectedFlow != null;
      } 
      #endregion

      #region Option
      /// <summary>
      /// Handles the Executed event of the InsertOption Command.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void InsertOption_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)LstItems.SelectedItem;
         FlowItemConditional iNew = EditorsHelper.MakeFlowOption();
         EditorsHelper.InsertFlowItem(iNew, (FlowItem)iFlow.SelectedItem);
      }

      private void AddOption_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)LstItems.SelectedItem;
         FlowItemConditional iNew = EditorsHelper.MakeFlowOption();
         EditorsHelper.AddFlowItem(iNew, (FlowItem)iFlow.SelectedItem, iFlow);
      } 
      #endregion



      #region Loop
      /// <summary>
      /// Handles the Executed event of the InsertLoop Command.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void InsertLoop_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)LstItems.SelectedItem;
         FlowItemConditional iNew = EditorsHelper.MakeFlowLoop();
         EditorsHelper.InsertFlowItem(iNew, (FlowItem)iFlow.SelectedItem);
      }

      private void AddLoop_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)LstItems.SelectedItem;
         FlowItemConditional iNew = EditorsHelper.MakeFlowLoop();
         EditorsHelper.AddFlowItem(iNew, (FlowItem)iFlow.SelectedItem, iFlow);
      } 
      #endregion

      #region static
      /// <summary>
      /// Handles the Executed event of the InsertStatic Command.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void InsertStatic_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)LstItems.SelectedItem;
         Debug.Assert(iFlow != null);
         iFlow.PopupDoesInsert = true;
         iFlow.PopupTarget = (UIElement)e.OriginalSource;
         iFlow.PopupIsOpen = true;
      }

      private void AddStatic_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)LstItems.SelectedItem;
         Debug.Assert(iFlow != null);
         iFlow.PopupDoesInsert = false;
         iFlow.PopupTarget = (UIElement)e.OriginalSource;
         iFlow.PopupIsOpen = true;
      } 
      #endregion

      #region New object
      private void AddNewObject_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         DlgStringQuestion iIn = new DlgStringQuestion();
         iIn.Owner = App.Current.MainWindow;
         iIn.Question = "Object name:";
         iIn.Answer = "New object";
         iIn.Title = "New object";
         if ((bool)iIn.ShowDialog() == true)
         {
            FlowItems.Flow iFlow = (FlowItems.Flow)LstItems.SelectedItem;
            Neuron iMeaning;
            NeuronCluster iCluster = BrainHelper.CreateObject(iIn.Answer, out iMeaning);
            FlowItemStatic iStatic = new FlowItemStatic(iCluster);
            iStatic.NeuronInfo.DisplayTitle = iIn.Answer;
            EditorsHelper.AddFlowItem(iStatic, (FlowItem)iFlow.SelectedItem, iFlow);
         }
      }

      private void InsertNewObject_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         DlgStringQuestion iIn = new DlgStringQuestion();
         iIn.Owner = App.Current.MainWindow;
         iIn.Question = "Object name:";
         iIn.Answer = "New object";
         iIn.Title = "New object";
         if ((bool)iIn.ShowDialog() == true)
         {
            FlowItems.Flow iFlow = (FlowItems.Flow)LstItems.SelectedItem;
            Neuron iMeaning;
            NeuronCluster iCluster = BrainHelper.CreateObject(iIn.Answer, out iMeaning);
            FlowItemStatic iStatic = new FlowItemStatic(iCluster);
            iStatic.NeuronInfo.DisplayTitle = iIn.Answer;
            EditorsHelper.InsertFlowItem(iStatic, (FlowItem)iFlow.SelectedItem);
         }
      } 
      #endregion

      #region Conditional part
      /// <summary>
      /// Handles the CanExecute event of the InsertCondPart control.
      /// </summary>
      /// <remarks>
      /// Almost similar as for the other inserts, except when a listbox item is focused, in that case we need to check if the
      /// flow item is contained in a Flow-item-Conditional.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void InsertCondPart_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         if (LstItems != null)
         {
            FlowItems.Flow iFlow = LstItems.SelectedItem as FlowItems.Flow;
            if (iFlow != null && iFlow.SelectedItem != null)
            {
               e.CanExecute = iFlow.SelectedItem is FlowItemBlock | iFlow.SelectedItem.Owner is FlowItemBlock;
               return;
            }
         }
         e.CanExecute = false;
      }

      private void InsertCondPart_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)LstItems.SelectedItem;
         FlowItem iSelected = iFlow.SelectedItem;
         iFlow.SelectedItems.Clear();                                                        //we do this manually for the | cause this is inserted/added using 'shift + \' -> this means that the shift is presssed & SetIsSelected interprets this as if we want to add to the selection, which we don't want.
         FlowItemConditionalPart iNew = EditorsHelper.MakeFlowCondPart();
         EditorsHelper.InsertFlowItem(iNew, iSelected);
      }

      private void AddCondPart_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)LstItems.SelectedItem;
         FlowItem iSelected = iFlow.SelectedItem;
         iFlow.SelectedItems.Clear();                                                        //we do this manually for the | cause this is inserted/added using 'shift + \' -> this means that the shift is presssed & SetIsSelected interprets this as if we want to add to the selection, which we don't want.
         FlowItemConditionalPart iNew = EditorsHelper.MakeFlowCondPart();
         EditorsHelper.AddFlowItem(iNew, iSelected, iFlow);
      }

      #endregion 

      #region Change option/loop
      private void ChangeOptionToLoop_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowItems.Flow iFlow = LstItems.SelectedItem as FlowItems.Flow;
         if (iFlow != null && iFlow.SelectedItem != null)
         {
            FlowItemConditional iCond;
            if (iFlow.SelectedItem is FlowItemConditional)
               iCond = (FlowItemConditional)iFlow.SelectedItem;
            else
               iCond = iFlow.SelectedItem.FindFirstOwner<FlowItemConditional>();
            if (iCond != null)
               iCond.IsLooped = true;
         }
      }

      private void ChangeLoopToOption_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowItems.Flow iFlow = LstItems.SelectedItem as FlowItems.Flow;
         if (iFlow != null && iFlow.SelectedItem != null)
         {
            FlowItemConditional iCond;
            if (iFlow.SelectedItem is FlowItemConditional)
               iCond = (FlowItemConditional)iFlow.SelectedItem;
            else
               iCond = iFlow.SelectedItem.FindFirstOwner<FlowItemConditional>();
            if (iCond != null)
               iCond.IsLooped = false;
         }
      } 
      #endregion

      
      /// <summary>
      /// Handles the Executed event of the AddFlow control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void AddFlow_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowEditor iEditor = (FlowEditor)DataContext;
         FlowItems.Flow iFlow = EditorsHelper.MakeFlow();
         if (iFlow != null)
         {
            iEditor.Flows.Add(iFlow);
            iFlow.IsSelected = true;
//            CtrlEditor.LstItems.Focus();
         }
      }

      #region NextPage
      private void NextPage_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         FlowEditor iEditor = (FlowEditor)DataContext;
         Debug.Assert(iEditor != null);
         if (iEditor.SelectedFlow != null)
         {
            int iIndex = iEditor.Flows.IndexOf(iEditor.SelectedFlow);
            e.CanExecute = iIndex < iEditor.Flows.Count - 1;
         }
         else
            e.CanExecute = false;
      }

      private void NextPage_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowEditor iEditor = (FlowEditor)DataContext;
         Debug.Assert(iEditor != null);
         int iIndex = iEditor.Flows.IndexOf(iEditor.SelectedFlow);
         iEditor.SelectedFlow = iEditor.Flows[iIndex + 1];
      } 
      #endregion

      #region PrevPage
      private void PrevPage_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         FlowEditor iEditor = (FlowEditor)DataContext;
         Debug.Assert(iEditor != null);
         if (iEditor.SelectedFlow != null)
         {
            int iIndex = iEditor.Flows.IndexOf(iEditor.SelectedFlow);
            e.CanExecute = iIndex > 0;
         }
         else
            e.CanExecute = false;
      }

      private void PrevPage_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowEditor iEditor = (FlowEditor)DataContext;
         Debug.Assert(iEditor != null);
         int iIndex = iEditor.Flows.IndexOf(iEditor.SelectedFlow);
         iEditor.SelectedFlow = iEditor.Flows[iIndex - 1];
      } 
      #endregion
      

      
      

      
      #endregion



      /// <summary>
      /// Handles the SelectionChanged event of the CmbStaticItemDisplayMode control.
      /// </summary>
      /// <remarks>
      /// We invalidate the intire editor so that all the items will be redrawn with the new correct value.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Controls.SelectionChangedEventArgs"/> instance containing the event data.</param>
      private void CmbStaticItemDisplayMode_SelectionChanged(object sender, SelectionChangedEventArgs e)
      {
         FlowEditor iEditor = (FlowEditor)DataContext;
         if (iEditor != null)
            iEditor.ReDrawFlows();
 
      }

      

      
      

      

      

   }
}
