﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using JaStDev.HAB.Designer.Editors.Flow.FlowItems;

namespace JaStDev.HAB.Designer.Editors.Flow.Views
{
   /// <summary>
   /// Interaction logic for FlowItemStaticView.xaml
   /// </summary>
   public partial class FlowItemStaticView : ContentControl
   {
      public FlowItemStaticView()
      {
         InitializeComponent();
      }

      /// <summary>
      /// Handles the MouseDoubleClick event of the ContentControl control.
      /// </summary>
      /// <remarks>
      /// Navigate to the link, if possible.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
      private void ContentControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
      {
         FlowItemStatic iStatic = (FlowItemStatic)DataContext;
         if (iStatic != null && iStatic.IsLink == true)
         {
            FlowEditor iEditor = iStatic.FindFirstOwner<FlowEditor>();
            Debug.Assert(iEditor != null);
            FlowItems.Flow iFound = (from i in iEditor.Flows where i.Item == iStatic.Item select i).FirstOrDefault();
            if (iFound == null)
            {
              MessageBoxResult iRes = MessageBox.Show(string.Format("The flow '{0}' is not yet included in this editor, do so now?", iStatic.NeuronInfo.DisplayTitle), "Go to flow", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.OK);
              if (iRes == MessageBoxResult.OK)
              {
                 iFound = new FlowItems.Flow();
                 iFound.ItemID = iStatic.Item.ID;
                 iEditor.Flows.Add(iFound);
              }
            }
            if (iFound != null)
               iEditor.SelectedFlow = iFound;
         }
      }
   }
}
