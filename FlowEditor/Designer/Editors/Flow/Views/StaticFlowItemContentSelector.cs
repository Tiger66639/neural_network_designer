﻿using System.Windows;
using System.Windows.Controls;

namespace JaStDev.HAB.Designer.Editors.Flow.Views
{
    /// <summary>
    ///     Provides custom logic for the <see cref="FlowItemStaticView" /> to select the appropriate template
    /// </summary>
    public class StaticFlowItemContentSelector : DataTemplateSelector
    {
        /// <summary>
        ///     When overridden in a derived class, returns a <see cref="T:System.Windows.DataTemplate" /> based on custom logic.
        /// </summary>
        /// <param name="item">The data object for which to select the template.</param>
        /// <param name="container">The data-bound object.</param>
        /// <returns>
        ///     Returns a <see cref="T:System.Windows.DataTemplate" /> or null. The default value is null.
        /// </returns>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var iCont = container as FrameworkElement;
            if (Properties.Settings.Default.FlowItemDisplayMode == 0)
                return iCont.TryFindResource("FlowItemStaticNormalView") as DataTemplate;
            if (Properties.Settings.Default.FlowItemDisplayMode == 1)
                return iCont.TryFindResource("FlowItemStaticLeftIcon") as DataTemplate;
            if (Properties.Settings.Default.FlowItemDisplayMode == 2)
                return iCont.TryFindResource("FlowItemStaticUnderIcon") as DataTemplate;
            return base.SelectTemplate(item, container);
        }
    }
}