﻿using System.Windows.Controls;

namespace JaStDev.HAB.Designer.Editors.Flow.Views
{
   /// <summary>
   /// Interaction logic for FlowItemConditionalView.xaml
   /// </summary>
   public partial class FlowItemConditionalView : UserControl
   {
      public FlowItemConditionalView()
      {
         InitializeComponent();
         BndDelete.CommandTarget = LstItems;
      }
   }
}
