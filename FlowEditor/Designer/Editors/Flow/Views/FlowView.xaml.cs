﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.BrainData;
using JaStDev.HAB.Designer.Editors.Flow.FlowItems;

namespace JaStDev.HAB.Designer.Editors.Flow.Views
{
   /// <summary>
   /// Interaction logic for FlowView.xaml
   /// </summary>
   public partial class FlowView : UserControl
   {
      /// <summary>
      /// used to go up and down through items, stores the minimum width of all items, so that we have an offset from the border.
      /// </summary>
      const double StaticMinWidth = 20;                           

      public FlowView()
      {
         InitializeComponent();
      }



      #region PopupIsOpen

      /// <summary>
      /// PopupIsOpen Dependency Property
      /// </summary>
      public static readonly DependencyProperty PopupIsOpenProperty =
          DependencyProperty.Register("PopupIsOpen", typeof(bool), typeof(FlowView),
              new FrameworkPropertyMetadata((bool)false,
                  new PropertyChangedCallback(OnPopupIsOpenChanged)));

      /// <summary>
      /// Gets or sets the PopupIsOpen property.  This dependency property 
      /// indicates wether the popup is currently open or closed.
      /// </summary>
      public bool PopupIsOpen
      {
         get { return (bool)GetValue(PopupIsOpenProperty); }
         set { SetValue(PopupIsOpenProperty, value); }
      }

      /// <summary>
      /// Handles changes to the PopupIsOpen property.
      /// </summary>
      private static void OnPopupIsOpenChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
      {
         ((FlowView)d).OnPopupIsOpenChanged(e);
      }

      /// <summary>
      /// Provides derived classes an opportunity to handle changes to the PopupIsOpen property.
      /// </summary>
      protected virtual void OnPopupIsOpenChanged(DependencyPropertyChangedEventArgs e)
      {
         if ((bool)e.NewValue == true)
         {
            FlowItems.Flow iFlow = (FlowItems.Flow)DataContext;
            Point iPoint;
            FrameworkElement iTarget = (FrameworkElement)iFlow.PopupTarget;
            if (iTarget != null && iTarget != LstItems)
            {
               if (iFlow.PopupDoesInsert == true)
                  iPoint = iTarget.TranslatePoint(new Point(0, iTarget.ActualHeight), LstItems);
               else
                  iPoint = iTarget.TranslatePoint(new Point(iTarget.ActualWidth, iTarget.ActualHeight), LstItems);
            }
            else
            {
               IList iSource = LstItems.ItemsSource as IList;
               Debug.Assert(iSource != null);
               FrameworkElement iLast;
               if (iSource.Count > 0)
               {
                  if (iFlow.PopupDoesInsert == true)
                  {
                     iLast = (FrameworkElement)LstItems.ItemContainerGenerator.ContainerFromItem(iFlow.SelectedItem);
                     iPoint = iLast.TranslatePoint(new Point(0, iLast.ActualHeight), LstItems);
                  }
                  else
                  {
                     iLast = (FrameworkElement)LstItems.ItemContainerGenerator.ContainerFromIndex(iSource.Count - 1);
                     iPoint = iLast.TranslatePoint(new Point(iLast.ActualWidth, iLast.ActualHeight), LstItems);
                  }
               }
               else
                  iPoint = new Point(0, 0);
            }
            PopupSelectItem.HorizontalOffset = iPoint.X;
            PopupSelectItem.VerticalOffset = iPoint.Y;
         }
      }

      #endregion




      private void Delete_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)DataContext;
         e.CanExecute = iFlow != null && iFlow.SelectedItems.Count > 0;
      }

      /// <summary>
      /// Handles the Executed event of the Delete control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)DataContext;
         Debug.Assert(iFlow != null);
         List<Neuron> iToDelete = (from FlowItem i in iFlow.SelectedItems select i.Item).ToList();
         foreach (Neuron i in iToDelete)
            WindowMain.DeleteItemFromBrain(i);
      }

      /// <summary>
      /// Handles the Executed event of the Remove control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void Remove_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)DataContext;
         Debug.Assert(iFlow != null);
         var iToDelete = (from FlowItem i in iFlow.SelectedItems
                          select new { Item = i, Owner = i.Owner as FlowItemBlock }).ToList();        //we create a new object so we are certain we don't loose the owner somehow.
         foreach (var i in iToDelete)
         {
            if (i.Owner != null)
               i.Owner.Items.Remove(i.Item);
            else
               iFlow.Items.Remove(i.Item);
         }
      }

      #region Popup
      /// <summary>
      /// Handles the Opened event of the PopupSelectItem control.
      /// </summary>
      /// <remarks>
      /// Need to assign the items source.  We do this when it opens so that it refreshes, cause the list doesn't provide
      /// for any updates + it can get large, don't want to be bound all the time.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
      private void PopupSelectItem_Opened(object sender, EventArgs e)
      {
         LstDictItems.ItemsSource = from i in BrainData.BrainData.Current.NeuronInfo.Items.Values orderby i.DisplayTitle ascending select i;
         LstDictItems.SelectedIndex = 0;
         LstDictItems.Focus();
         JaStDev.ControlFramework.Input.FocusManager.SetIsSelectedItemFocused(LstDictItems, true);                   //we use this style to set the first item as focused.  This simply works.
      }

      /// <summary>
      /// Handles the Closed event of the PopupSelectItem control.
      /// </summary>
      /// <remarks>
      /// Only generates a new item if 'true' was assigned to the tag of the popup. This is to prevent creating items if the popup was closed
      /// without explicetly selecting an item.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
      private void PopupSelectItem_Closed(object sender, EventArgs e)
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)DataContext;
         if (PopupSelectItem.Tag != null && (bool)PopupSelectItem.Tag == true)                        //we only add an item if the value 'true' was assigned to the tag.
         {
            FlowItem iPrevSelected = iFlow.SelectedItem;
            NeuronData iData = LstDictItems.SelectedItem as NeuronData;
            if (iData != null)
               CreateFlowItemFromPopup(iData.Neuron);
         }
         LstDictItems.ItemsSource = null;
         PopupSelectItem.Tag = null;
         if (iFlow.PopupTarget != null)
            iFlow.PopupTarget.Focus();                                                                   //move back to the item that requested the operation, if we don't do this, it is difficult to continue editing with the keyboard.
         else
            LstItems.Focus();
      }

      /// <summary>
      /// Creates a new flow item when the popup was closed..
      /// </summary>
      /// <param name="neuron">The neuron.</param>
      private void CreateFlowItemFromPopup(Neuron neuron)
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)DataContext;
         FlowItem iNew = EditorsHelper.CreateFlowItemFor(neuron);
         if (iFlow.PopupDoesInsert == true)
            EditorsHelper.InsertFlowItem(iNew, (FlowItem)iFlow.SelectedItem);
         else
            EditorsHelper.AddFlowItem(iNew, (FlowItem)iFlow.SelectedItem, iFlow);
      }

      /// <summary>
      /// Handles the MouseDoubleClick event of the LstDictItem control.
      /// </summary>
      /// <remarks>
      /// need to close the popup when a double click is done.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
      private void LstDictItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
      {
         PopupSelectItem.Tag = true;                                                            //we use the tag to pass along that the item should be generated.
         PopupIsOpen = false;
      }

      private void LstDictItems_KeyDown(object sender, KeyEventArgs e)
      {
         if (e.Key == Key.Return || e.Key == Key.Enter)
         {
            PopupSelectItem.Tag = true;                                                            //we use the tag to pass along that the item should be generated.
            PopupIsOpen = false;
         }
         else if (e.Key == Key.Escape)
         {
            PopupSelectItem.Tag = null;                                                            //we use the tag to pass along that nothing be generated.
            PopupIsOpen = false;
         }
      }

      #endregion


      #region Keyboard navigation

      private void LstItems_KeyDown(object sender, KeyEventArgs e)
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)DataContext;
         if (iFlow != null && (Keyboard.Modifiers & ModifierKeys.Alt) == ModifierKeys.None)        //don't do any navigation when the alt is pressed.  This is used to navigate through the different flows.
         {
            FlowItem iSelected = iFlow.SelectedItem as FlowItem;
            if (iSelected != null)
            {
               if (e.Key == Key.Right)
               {
                  MoveRightOf(iSelected);
                  e.Handled = true;
               }
               else if (e.Key == Key.Left)
               {
                  MoveLeftOf(iSelected);
                  e.Handled = true;
               }
               else if (e.Key == Key.Down)
               {
                  MoveDownByLine();
                  e.Handled = true;
               }
               else if (e.Key == Key.Up)
               {
                  MoveUpByLine();
                  e.Handled = true;
               }
               else if (e.Key == Key.Home)
               {
                  GotoHome();
                  e.Handled = true;
               }
               else if (e.Key == Key.End)
               {
                  GotoEnd();
                  e.Handled = true;
               }
            }
         }
      }

      /// <summary>
      /// Goes to the end, which is the listbox containing the flow, so it clears the selectionlist.
      /// </summary>
      /// <remarks>
      /// Goes to the owning list's last item if this isn't selected, otherwise the last item at the level of the owning list.
      /// if there is no owning sub list, it goes to the last item of the flow and then clears the selection.
      /// </remarks>
      private void GotoEnd()
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)DataContext;
         if (iFlow != null && LstItems != null)
         {
            if (iFlow.SelectedItem != null)
            {
               FlowItemBlock iOwner = iFlow.SelectedItem.Owner as FlowItemBlock;
               if (iOwner != null)
                  GotoEndOf(iOwner, iFlow.SelectedItem);
               else
               {
                  if (iFlow.SelectedItem == iFlow.Items[iFlow.Items.Count - 1])
                     iFlow.SelectedItems.Clear();                                               //go to end of the intire list.
                  else
                     iFlow.Items[iFlow.Items.Count - 1].IsSelected = true;
               }
            }
         }
      }

      private void GotoEndOf(FlowItemBlock block, FlowItem selected)
      {
         if (block.Items[block.Items.Count - 1] != selected)
            block.Items[block.Items.Count - 1].IsSelected = true;
         else
         {
            FlowItemBlock iOwner = block.Owner as FlowItemBlock;
            if (iOwner != null)
               GotoEndOf(iOwner, block);
            else
            {
               FlowItems.Flow iFlow = (FlowItems.Flow)DataContext;
               if (block == iFlow.Items[iFlow.Items.Count - 1])
                  iFlow.SelectedItems.Clear();                                               //go to end of the intire list.
               else
                  iFlow.Items[iFlow.Items.Count - 1].IsSelected = true;
            }
         }
      }

      /// <summary>
      /// Makes the first item selected.
      /// </summary>
      private void GotoHome()
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)DataContext;
         if (iFlow != null && LstItems != null)
         {
            if (iFlow.SelectedItem != null)
            {
               FlowItemBlock iOwner = iFlow.SelectedItem.Owner as FlowItemBlock;
               if (iOwner != null)
                  GotoStartOf(iOwner, iFlow.SelectedItem);
               else
               {
                  if (iFlow.SelectedItem == iFlow.Items[0])
                     iFlow.SelectedItems.Clear();                                               //go to end of the intire list.
                  else
                     iFlow.Items[0].IsSelected = true;
               }
            }
         }
      }

      private void GotoStartOf(FlowItemBlock block, FlowItem selected)
      {
         if (block.Items[0] != selected)
            block.Items[0].IsSelected = true;
         else
         {
            FlowItemBlock iOwner = block.Owner as FlowItemBlock;
            if (iOwner != null)
               GotoStartOf(iOwner, block);
            else
            {
               FlowItems.Flow iFlow = (FlowItems.Flow)DataContext;
               if (block == iFlow.Items[0])
                  iFlow.SelectedItems.Clear();                                               //go to end of the intire list.
               else
                  iFlow.Items[0].IsSelected = true;
            }
         }
      }

      /// <summary>
      /// Moves the cursor to the left of the the speciefied item.  This is done by going as deep into FlowItemBLocks as possible.
      /// </summary>
      /// <param name="item">The item to move left of.</param>
      private void MoveLeftOf(FlowItem item)
      {
         FlowItem iLeft = null;
         if (item is FlowItemConditional)
         {
            FlowItemConditional iCond = (FlowItemConditional)item;
            if (iCond.Items.Count > 0)
            {
               FlowItemConditionalPart iPart = iCond.Items[iCond.Items.Count - 1] as FlowItemConditionalPart;
               if (iPart != null && iPart.Items.Count > 0)
                  iLeft = iPart.Items[iPart.Items.Count - 1];
               else
                  iLeft = iCond.Items[iCond.Items.Count - 1];
            }
            else
               iLeft = FindLeftOf(item);
         }
         else if (item is FlowItemConditionalPart && ((FlowItemConditionalPart)item).Items.Count > 0)
         {
            FlowItem iPrev = FindLeftOf(item) as FlowItem;
            if (iPrev is FlowItemConditionalPart)
               iLeft = ((FlowItemConditionalPart)iPrev).Items[((FlowItemConditionalPart)iPrev).Items.Count - 1];
            else
               iLeft = iPrev;
         }
         else
            iLeft = FindLeftOf(item);
         if (iLeft != null)
            iLeft.IsSelected = true;
      }

      /// <summary>
      /// Finds the flow item to the left of the specified item, at the same level or up, so without going down into flowItemBlocks.
      /// </summary>
      /// <param name="item">The item.</param>
      /// <returns>The first item to the left, not into the tree.</returns>
      private FlowItem FindLeftOf(FlowItem item)
      {
         if (item.Owner is FlowItemBlock)
         {
            FlowItemBlock iBlock = (FlowItemBlock)item.Owner;
            int iIndex = iBlock.Items.IndexOf(item);
            if (iIndex == 0)
            {
               FlowItemConditionalPart iOwner = item.Owner as FlowItemConditionalPart;
               if (iOwner != null)
                  return iOwner;
               else
                  return FindLeftOf(iBlock);
            }
            else if (iIndex > 0)
               return iBlock.Items[iIndex - 1];
            else
               throw new InvalidOperationException("Flow item not found in owner.");
         }
         else if (item.Owner is FlowItems.Flow)
         {
            FlowItems.Flow iFlow = (FlowItems.Flow)item.Owner;
            int iIndex = iFlow.Items.IndexOf(item);
            if (iIndex > 0)
               return iFlow.Items[iIndex - 1];
            else if (iIndex < 0)
               throw new InvalidOperationException("Flow item not found in owner.");
         }
         else
            throw new InvalidOperationException("Unkown owner of flow item.");
         return null;
      }

      /// <summary>
      /// Moves the cursor to the right of the the speciefied item.  This is done by going as deep into FlowItemBLocks as possible.
      /// </summary>
      /// <param name="item">The item to move left of.</param>
      private void MoveRightOf(FlowItem item)
      {
         FlowItem iRight = null;
         if (item is FlowItemConditional)
         {
            FlowItemConditional iCond = (FlowItemConditional)item;
            if (iCond.Items.Count > 0)
            {
               FlowItemConditionalPart iPart = iCond.Items[0] as FlowItemConditionalPart;
               if (iPart != null && iPart.Items.Count > 0)
                  iRight = iPart.Items[0];
               else
                  iRight = iCond.Items[0];
            }
            else
               iRight = FindRightOf(item);
         }
         else if (item is FlowItemConditionalPart && ((FlowItemConditionalPart)item).Items.Count > 0)
            iRight = ((FlowItemConditionalPart)item).Items[0];
         else
            iRight = FindRightOf(item);

         if (iRight != null)
            iRight.IsSelected = true;
      }

      /// <summary>
      /// Finds the flow item to the right of the specified item, at the same level or up, so without going down into flowItemBlocks.
      /// </summary>
      /// <param name="item">The item.</param>
      /// <returns>The first item to the right, not into the tree.</returns>
      private FlowItem FindRightOf(FlowItem item)
      {
         if (item.Owner is FlowItemBlock)
         {
            FlowItemBlock iBlock = (FlowItemBlock)item.Owner;
            int iIndex = iBlock.Items.IndexOf(item);
            if (iIndex == iBlock.Items.Count - 1)
               return FindRightOf(iBlock);
            else if (iIndex >= 0)
               return iBlock.Items[iIndex + 1];
            else
               throw new InvalidOperationException("Flow item not found in owner.");
         }
         else if (item.Owner is FlowItems.Flow)
         {
            FlowItems.Flow iFlow = (FlowItems.Flow)item.Owner;
            int iIndex = iFlow.Items.IndexOf(item);
            if (iIndex < iFlow.Items.Count - 1)
               return iFlow.Items[iIndex + 1];
            else if (iIndex < 0)
               throw new InvalidOperationException("Flow item not found in owner.");
         }
         else
            throw new InvalidOperationException("Unkown owner of flow item.");
         return null;
      }

      private void MoveDownByLine()
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)DataContext;
         if (iFlow.SelectedItem != null && iFlow.SelectedItem.Owner is FlowItemBlock)
         {

            if (iFlow.SelectedItem is FlowItemConditionalPart && iFlow.SelectedItem.Owner is FlowItemConditional)
               MoveDownFromPart((FlowItemConditional)iFlow.SelectedItem.Owner, (FlowItemConditionalPart)iFlow.SelectedItem);
            else
               MoveDownFromItem(iFlow.SelectedItem);
         }
      }

      private void MoveDownFromPart(FlowItemConditional owner, FlowItemConditionalPart cur)
      {
         while (owner != null)                                                                     //we do a loop so that we also handle the end of a list correctly.
         {
            int iIndex = owner.Items.IndexOf(cur);
            if (iIndex < owner.Items.Count - 1)
            {
               cur = owner.Items[iIndex + 1] as FlowItemConditionalPart;
               cur.IsSelected = true;
               owner = null;                                                                      //need to exit the loop
            }
            else
            {
               cur = owner.Owner as FlowItemConditionalPart;
               if (cur != null)
                  owner = cur.Owner as FlowItemConditional;
               else
                  owner = null;
            }
         }
      }

      private void MoveDownFromItem(FlowItem flowItem)
      {
         FlowItemConditionalPart iCur = flowItem.Owner as FlowItemConditionalPart;
         FlowItemConditional iOwner = iCur.Owner as FlowItemConditional;
         while (iOwner != null)                                                                     //we do a loop so that we also handle the end of a list correctly.
         {
            int iIndex = iOwner.Items.IndexOf(iCur);
            if (iIndex < iOwner.Items.Count - 1)
            {
               iCur = iOwner.Items[iIndex + 1] as FlowItemConditionalPart;
               if (iCur != null)                                                                   //this could be if there is an error in the compound type structure.
               {
                  if (iCur.Items.Count > 0)
                     iCur.Items[0].IsSelected = true;
                  else
                     iCur.IsSelected = true;
               }
               iOwner = null;                                                                      //need to exit the loop
            }
            else
            {
               iCur = iOwner.Owner as FlowItemConditionalPart;
               if (iCur != null)
                  iOwner = iCur.Owner as FlowItemConditional;
               else
                  iOwner = null;
            }
         }
      }

      private void MoveUpByLine()
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)DataContext;
         if (iFlow.SelectedItem != null && iFlow.SelectedItem.Owner is FlowItemBlock)
         {
            
            if (iFlow.SelectedItem is FlowItemConditionalPart && iFlow.SelectedItem.Owner is FlowItemConditional)
               MoveUpFromPart((FlowItemConditional)iFlow.SelectedItem.Owner, (FlowItemConditionalPart)iFlow.SelectedItem);
            else
               MoveUpFromItem(iFlow.SelectedItem);
         }
      }

      private void MoveUpFromItem(FlowItem item)
      {
         FlowItemConditionalPart iCur = item.Owner as FlowItemConditionalPart;
         FlowItemConditional iOwner = iCur.Owner as FlowItemConditional;
         while (iOwner != null)                                                                     //we do a loop so that we also handle the end of a list correctly.
         {
            int iIndex = iOwner.Items.IndexOf(iCur);
            if (iIndex > 0)
            {
               iCur = iOwner.Items[iIndex - 1] as FlowItemConditionalPart;
               if (iCur != null)                                                                   //this could be if there is an error in the compound type structure.
               {
                  if (iCur.Items.Count > 0)
                     iCur.Items[0].IsSelected = true;
                  else
                     iCur.IsSelected = true;
               }
               iOwner = null;                                                                      //need to exit the loop
            }
            else
            {
               iCur = iOwner.Owner as FlowItemConditionalPart;
               if (iCur != null)
                  iOwner = iCur.Owner as FlowItemConditional;
               else
                  iOwner = null;
            }
         }
      }

      private void MoveUpFromPart(FlowItemConditional owner, FlowItemConditionalPart cur)
      {
         while (owner != null)                                                                     //we do a loop so that we also handle the end of a list correctly.
         {
            int iIndex = owner.Items.IndexOf(cur);
            if (iIndex > 0)
            {
               cur = owner.Items[iIndex - 1] as FlowItemConditionalPart;
               cur.IsSelected = true;
               owner = null;                                                                      //need to exit the loop
            }
            else
            {
               cur = owner.Owner as FlowItemConditionalPart;
               if (cur != null)
                  owner = cur.Owner as FlowItemConditional;
               else
                  owner = null;
            }
         }
      }

      #endregion


      /// <summary>
      /// Handles the MouseDown event of the LstItems control.
      /// </summary>
      /// <remarks>
      /// When the listbox itself receives focus by getting clicked on, we reset the selected item to null.  If we don't do that, there is no
      /// way to select the flow itself to add items to.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
      private void LstItems_MouseDown(object sender, MouseButtonEventArgs e)
      {
         FlowItems.Flow iFlow = (FlowItems.Flow)DataContext;
         FrameworkElement iSource = e.OriginalSource as FrameworkElement;
         if (iFlow != null &&  iSource != null && iSource.DataContext == iFlow)                                    //we need to make certain we only respond to a click on the actual listbox, not on one of it's children.
            iFlow.SelectedItems.Clear();
      }

      

      
      

   }
}
