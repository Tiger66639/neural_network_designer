﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems;
using JaStDev.HAB.Designer.Tools.Debugger;

namespace JaStDev.HAB.Designer.Editors.CodeEditor
{
   /// <summary>
   /// Interaction logic for CodeEditor.xaml
   /// </summary>
   public partial class CodeEditorView : UserControl
   {
      public CodeEditorView()
      {
         InitializeComponent();
      }

     

      private void RunNeuron_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         Processor.Processor iProc = ProcessorManager.Current.GetProcessor();
         CodeEditorPage iPage = TabPages.SelectedItem as CodeEditorPage;
         Action<NeuronCluster> iCallAsync = new Action<NeuronCluster>(iProc.Call);
         iCallAsync.BeginInvoke(iPage.Items.Cluster, null, null);                                     //we have to call async, otherwise the ui can get stuck because the debugger is doing a thread sync by waiting.

      }

      private void RunNeuron_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         e.CanExecute = true;
      }
   }
}
