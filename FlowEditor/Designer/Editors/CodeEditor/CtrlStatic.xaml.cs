﻿
using JaStDev.HAB.Designer.Editors.@base;

namespace JaStDev.HAB.Designer.Editors.CodeEditor
{
   /// <summary>
   /// Interaction logic for CtrlStatic.xaml
   /// </summary>
   public partial class CtrlStatic : CtrlEditorItem
   {
      public CtrlStatic()
      {
         InitializeComponent();
      }
   }
}
