﻿
using JaStDev.HAB.Designer.Editors.@base;

namespace JaStDev.HAB.Designer.Editors.CodeEditor
{
   /// <summary>
   /// Interaction logic for CtrlByRefExpression.xaml
   /// </summary>
   public partial class CtrlByRef : CtrlEditorItem
   {
      public CtrlByRef()
      {
         InitializeComponent();
      }
   }
}
