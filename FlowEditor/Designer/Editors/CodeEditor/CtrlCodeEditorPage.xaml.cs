﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems;
using JaStDev.HAB.Designer.Tools.Toolbox;
using JaStDev.HAB.Expressions;

namespace JaStDev.HAB.Designer.Editors.CodeEditor
{
   /// <summary>
   /// Interaction logic for CtrlCodeEditorPage.xaml
   /// </summary>
   public partial class CtrlCodeEditorPage : UserControl
   {
      #region Fields
      List<NeuronToolBoxItem> fRegisteredToolboxItems;
      CodeEditorPage fPage;                            
      #endregion                                                //required because datacontext is lost when unloaded.

      #region ctor
      public CtrlCodeEditorPage()
      {
         InitializeComponent();
         DataContextChanged += new DependencyPropertyChangedEventHandler(CtrlCodeEditorPage_DataContextChanged);
      } 
      #endregion

      #region Event handlers
      void CtrlCodeEditorPage_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
      {
         RemoveToolBoxItems();
         CreateToolBoxItems();
      }

      /// <summary>
      /// When this control gets loaded, and there are no toolbox items created yet for the list of variables on this
      /// page, do so now.
      /// </summary>
      private void UserControl_Loaded(object sender, RoutedEventArgs e)
      {
         CreateToolBoxItems();
      }

      /// <summary>
      /// If the list of toolbox items is not yet removed from the braindata, do so now
      /// </summary>
      private void UserControl_Unloaded(object sender, RoutedEventArgs e)
      {
         RemoveToolBoxItems();
      }

      void RegisteredVariables_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         CodeEditorPage iPage = (CodeEditorPage)DataContext;
         switch (e.Action)
         {
            case NotifyCollectionChangedAction.Add:
               foreach (Variable i in e.NewItems)
                  AddVariable(i, iPage);
               break;
            case NotifyCollectionChangedAction.Remove:
               foreach (Variable i in e.OldItems)
                  RemoveVariable(i, iPage);
               break;
            case NotifyCollectionChangedAction.Replace:
               break;
            case NotifyCollectionChangedAction.Reset:
               foreach (ToolBoxItem i in fRegisteredToolboxItems)
                  BrainData.BrainData.Current.ToolBoxItems.Remove(i);
               fRegisteredToolboxItems.Clear();
               break;
            default:
               break;
         }
      }

      private void ListBox_MouseDown(object sender, MouseButtonEventArgs e)
      {
         if (Keyboard.IsKeyDown(Key.LeftShift) == false && Keyboard.IsKeyDown(Key.RightShift) == false)
         {
            ItemsControl iSender = (ItemsControl)sender;
            CodeEditorPage iPage = (CodeEditorPage)iSender.DataContext;
            iPage.SelectedItems.Clear();
         }
      }

      #endregion

      #region Commands

      #region Order

      private void MoveUp_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         CodeEditorPage iPage = (CodeEditorPage)DataContext;
         e.CanExecute = iPage.CanMoveUpFor(iPage.Items);
      }

      private void MoveDown_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         CodeEditorPage iPage = (CodeEditorPage)DataContext;
         e.CanExecute = iPage.CanMoveDownFor(iPage.Items);
      }


      private void MoveUp_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         CodeEditorPage iPage = (CodeEditorPage)DataContext;
         iPage.MoveUpFor(iPage.Items);
      }

      private void MoveDown_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         CodeEditorPage iPage = (CodeEditorPage)DataContext;
         iPage.MoveDownFor(iPage.Items);
      }

      private void MoveToEnd_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         CodeEditorPage iPage = (CodeEditorPage)DataContext;
         iPage.MoveToEndFor(iPage.Items);
      }

      private void MoveToHome_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         CodeEditorPage iPage = (CodeEditorPage)DataContext;
         iPage.MoveToStartFor(iPage.Items);
      }

      #endregion

      #region Delete neuron
      /// <summary>
      /// removes the selected code item(s) and also deletes the related neuron(s).
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void DeleteCmd_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         MessageBoxResult iRes = MessageBox.Show("Permanently remove the selected neurons from the brain?", "Delete neuron", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
         if (iRes == MessageBoxResult.OK)
         {
            List<CodeItem> iToRemove = fPage.SelectedItems.ToList();                                        //we make a copy just in case we don't brake any loop code.
            iToRemove.Reverse();                                                                            //we reverse the list so that we remove the last code item first.  This allows for better consistency in editing.
            WindowMain.UndoStore.BeginUndoGroup(false);                                                          //we group all the data together so a single undo command cand restore the previous state.
            try
            {
               foreach (CodeItem i in iToRemove)
               {
                  if (i.Item.CanBeDeleted == true)
                  {
                     WindowMain.DeleteBranchFromBrain(i.Item, false);
                     fPage.SelectedItems.Remove(i);                                                            //we only remove from the selectionlist those that are really removed.
                  }
                  else
                     MessageBox.Show(string.Format("Neuron {0} can't be deleted because it is used as a meaning or info.", i), "Delete", MessageBoxButton.OK, MessageBoxImage.Warning);
               }
            }
            finally
            {
               WindowMain.UndoStore.EndUndoGroup();
            }
         }
      }

      /// <summary>
      /// Handles the CanExecute event of the DeleteCmd command.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void DeleteCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         if (fPage != null)
         {
            int iCanBeCount = 0;                                                                   //keeps track of how many can be deleted.
            foreach (CodeItem i in fPage.SelectedItems)
            {
               if (i.Item.CanBeDeleted == true)
                  iCanBeCount++;
            }
            e.CanExecute = iCanBeCount > 0;
         }
         else
            e.CanExecute = false;
      }

      #endregion

      #region Remove
      /// <summary>
      /// Handles the CanExecute event of the RemoveCmd control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void RemoveCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         e.CanExecute = fPage != null && fPage.SelectedItems.Count > 0;
      }

      /// <summary>
      /// Handles the Executed event of the RemoveCmd control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void RemoveCmd_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         MessageBoxResult iRes = MessageBox.Show("Remove the selected neurons from the designer? (Neurons are not deleted, just removed from the list.)", "Remove neuron", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
         if (iRes == MessageBoxResult.OK)
         {
            List<CodeItem> iToRemove = fPage.SelectedItems.ToList();                                        //we make a copy just in case we don't brake any loop code.
            WindowMain.UndoStore.BeginUndoGroup(false);                                                          //we group all the data together so a single undo command cand restore the previous state.
            try
            {
               foreach (CodeItem i in iToRemove)
               {
                  if (i.Owner == fPage)
                  {
                     if (fPage.Items.Remove(i) == false)
                        MessageBox.Show("Failed to remove the item from the main code list!");
                  }
                  else if (i.Owner is CodeItem)
                     ((CodeItem)i.Owner).RemoveChildFromCode(i);
                  else
                     MessageBox.Show("Unkown owner type, can't Remove!");
               }
            }
            finally
            {
               WindowMain.UndoStore.EndUndoGroup();
            }
         }
      }
      #endregion

      #region ViewCode
      /// <summary>
      /// Handles the Executed event of the RemoveCmd control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void ViewCodeCmd_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         CodeItem iSelected = fPage.SelectedItem;
         if (iSelected != null)
            ((WindowMain)App.Current.MainWindow).ViewCodeForNeuron(iSelected.Item);
      } 
      #endregion

      #region Sync
      /// <summary>
      /// Handles the Executed event of the SyncCmd control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void SyncCmd_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         CodeItem iSelected = fPage.SelectedItem;
         if (iSelected != null)
            ((WindowMain)App.Current.MainWindow).SyncExplorerToNeuron(iSelected.Item);
      }
      #endregion



      #endregion

      #region functions

      private void CreateToolBoxItems()
      {
         fPage = DataContext as CodeEditorPage;
         if (fRegisteredToolboxItems == null && fPage != null)
         {
            fRegisteredToolboxItems = new List<NeuronToolBoxItem>();
            foreach (Variable i in fPage.RegisteredVariables)
               AddVariable(i, fPage);
            fPage.RegisteredVariables.CollectionChanged += new NotifyCollectionChangedEventHandler(RegisteredVariables_CollectionChanged);
         }
      }

      /// <summary>
      /// Adds a variable to the list of toolbox items of the brain so they can appear on any toolbox controls.
      /// </summary>
      /// <param name="variable"></param>
      /// <param name="page"></param>
      private void AddVariable(Variable variable, CodeEditorPage page)
      {
         NeuronToolBoxItem iNew = new NeuronToolBoxItem();
         iNew.Item = variable;
         iNew.Category = string.Format("Variables for {0} - {1}", BrainData.BrainData.Current.NeuronInfo[page.Item.ID].DisplayTitle, page.Title);
         BrainData.BrainData.Current.ToolBoxItems.Add(iNew);
         fRegisteredToolboxItems.Add(iNew);
      }

      

      private void RemoveToolBoxItems()
      {
         if (fRegisteredToolboxItems != null && fPage != null)
         {
            fPage.RegisteredVariables.CollectionChanged -= new NotifyCollectionChangedEventHandler(RegisteredVariables_CollectionChanged);
            foreach (NeuronToolBoxItem i in fRegisteredToolboxItems)
               BrainData.BrainData.Current.ToolBoxItems.Remove(i);
            fRegisteredToolboxItems = null;
         }
      }

      

      private void RemoveVariable(Variable i, CodeEditorPage iPage)
      {
         NeuronToolBoxItem iToolboxItem = (from iReg in fRegisteredToolboxItems
                                           where iReg.Item == i
                                           select iReg).FirstOrDefault();
         if (iToolboxItem != null)
         {
            BrainData.BrainData.Current.ToolBoxItems.Remove(iToolboxItem);
            fRegisteredToolboxItems.Remove(iToolboxItem);
         }
      }

      #endregion

   }
}
