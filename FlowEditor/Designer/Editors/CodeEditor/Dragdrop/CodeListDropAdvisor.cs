﻿using JaStDev.HAB.Designer.DragDrop;
using JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems;
using JaStDev.HAB.Designer.Properties;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

namespace JaStDev.HAB.Designer.Editors.CodeEditor.Dragdrop
{
    /// <summary>
    ///     Drop advisor for <see cref="CodeItemCollection" /> objects.
    /// </summary>
    /// <remarks>
    ///     This object should be attached to an ItemsControl that is bound to a code list with
    ///     it's <see cref="ItemsControl.ItemsSource" /> property.
    ///     <para>
    ///         Provides only add functionality for the list, no inserts.  This has to be done with
    ///         <see cref="CodeListItemDropAdvisor" />
    ///     </para>
    /// </remarks>
    public class CodeListDropAdvisor : DropTargetBase
    {
        /// <summary>
        ///     Tries the move item.
        /// </summary>
        /// <param name="arg">The <see cref="System.Windows.DragEventArgs" /> instance containing the event data.</param>
        /// <param name="dropPoint">The drop point.</param>
        /// <param name="iItem">The i item.</param>
        private void TryMoveItem(DragEventArgs arg, Point dropPoint, CodeItem iItem)
        {
            var iList = CodeList;
            int iIndex = iList.IndexOf(iItem);
            if (iIndex == -1)
            //we need to create a new item if the item being moved wasn't in this list or when a copy was requested because shift was pressed.
            {
                var iNew = CodeItems.CodeEditor.CreateCodeItemFor(iItem.Item);
                //we always create a new item, even with a move, cause we don't know how the item was being dragged (with shift pressed or not), this is the savest and easiest way.
                iList.Add(iNew);
            }
            else
            {
                iList.Move(iIndex, iList.Count - 1);
                arg.Effects = DragDropEffects.None;
            }
        }

        private void TryCreateNewCodeItem(DragEventArgs obj, Point dropPoint)
        {
            var iId = (ulong)obj.Data.GetData(Resources.NeuronIDFormat);

            var iNew = CodeItems.CodeEditor.CreateCodeItemFor(Brain.Brain.Current[iId]);
            Debug.Assert(iNew != null);
            var iCodeList = CodeList;
            Debug.Assert(iCodeList != null);
            iCodeList.AddWithoutEvents(iNew);
        }

        #region prop

        #region Target

        public ItemsControl Target
        {
            get { return (ItemsControl)TargetUI; }
        }

        #endregion Target

        #region UsePreviewEvents

        /// <summary>
        ///     Gets if the preview event versions should be used or not.
        /// </summary>
        /// <remarks>
        ///     don't use preview events cause than the sub lists don't get used but only the main list cause this gets the events
        ///     first,
        ///     while we usually want to drop in a sublist.
        /// </remarks>
        public override bool UsePreviewEvents
        {
            get { return false; }
        }

        #endregion UsePreviewEvents

        #region CodeList

        /// <summary>
        ///     Gets the list containing all the code that the UI to which advisor is attached too, displays data for.
        /// </summary>
        public CodeItemCollection CodeList
        {
            get { return Target.ItemsSource as CodeItemCollection; }
        }

        #endregion CodeList

        #endregion prop

        #region Overrides

        public override void OnDropCompleted(DragEventArgs arg, Point dropPoint)
        {
            var iItem = arg.Data.GetData(Resources.CodeItemFormat) as CodeItem;
            if (iItem != null && CodeList.Contains(iItem) == true)
                TryMoveItem(arg, dropPoint, iItem);
            else
                TryCreateNewCodeItem(arg, dropPoint); //we are not moving around an item, so add new code item.
            //Control iDropTarget = (Control)TargetUI;
        }

        public override bool IsValidDataObject(IDataObject obj)
        {
            return obj.GetDataPresent(Resources.CodeItemFormat)
                   || obj.GetDataPresent(Resources.NeuronIDFormat);
        }

        //public override DragDropEffects GetEffect(DragEventArgs e)
        //{
        //   DragDropEffects iRes = base.GetEffect(e);
        //   if (iRes == DragDropEffects.Move)
        //   {
        //      CodeItem iItem = e.Data.GetData(Properties.Resources.CodeItemFormat) as CodeItem;
        //      if (iItem != null && CodeList.Contains(iItem) == true)
        //         return DragDropEffects.Copy;                                                                 //when we move on the same list, the drag source doesn't have to do anything.
        //   }
        //   return iRes;
        //}

        #endregion Overrides
    }
}