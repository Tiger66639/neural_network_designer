﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.DragDrop;
using JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems;
using JaStDev.HAB.Designer.Properties;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

namespace JaStDev.HAB.Designer.Editors.CodeEditor.Dragdrop
{
    public class CodeItemDropAdvisor : DropTargetBase
    {
        #region UsePreviewEvents

        /// <summary>
        ///     Gets if the preview event versions should be used or not.
        /// </summary>
        /// <remarks>
        ///     don't use preview events cause than the sub drop points don't get used but only the main list cause this gets the
        ///     events first,
        ///     while we usually want to drop in a sub drop point.
        /// </remarks>
        public override bool UsePreviewEvents
        {
            get { return false; }
        }

        #endregion UsePreviewEvents

        /// <summary>
        ///     When true, drops will be done by the actual neuron, they won't be wrapped inside a CodeItem.
        /// </summary>
        /// <remarks>
        ///     This is required cause some props map directly to the neuron (see
        ///     <see cref="CodeitemConditionalStatement.CaseItem" />).
        ///     -> no longer used for CaseItem, still suppported.
        /// </remarks>
        public bool IsRaw { get; set; }

        /// <summary>
        ///     Gets/sets the type of code editor object that can be accepted.
        /// </summary>
        /// <remarks>
        ///     This property is used to check for valid drop point when a code item is being dragged around
        ///     By default this is empty, indicating all types are allowed.  When set, only objects from this type (or descendents)
        ///     are allowed).
        /// </remarks>
        public Type AllowedType { get; set; }

        /// <summary>
        ///     Gets/sets the type of neuron object that can be accepted.
        /// </summary>
        /// <remarks>
        ///     This property is used to check for valid drop point when a neuron is being dragged that has no corresponding code
        ///     item.
        ///     By default this is empty, indicating all types are allowed.  When set, only objects from this type (or descendents)
        ///     are allowed).
        /// </remarks>
        public Type AllowedNeuronType { get; set; }

        #region Item

        /// <summary>
        ///     Gets the code item that is the datacontext of the target of this drop advisor.
        /// </summary>
        /// <remarks>
        ///     Only valid when IsRaw = false
        /// </remarks>
        public CodeItem Item
        {
            get { return ((FrameworkElement)TargetUI).DataContext as CodeItem; }
        }

        #endregion Item

        #region Neuron

        /// <summary>
        ///     Gets the neuron that is the datacontext of the target of this drop advisor.
        /// </summary>
        /// <remarks>
        ///     Only valid when IsRaw = true
        /// </remarks>
        public Neuron Neuron
        {
            get { return ((FrameworkElement)TargetUI).DataContext as Neuron; }
        }

        #endregion Neuron

        #region Presenter

        /// <summary>
        ///     Gets the content presenter that should be used to display the data on.
        /// </summary>
        public ContentPresenter Presenter
        {
            get { return ((FrameworkElement)TargetUI).Tag as ContentPresenter; }
        }

        #endregion Presenter

        #region Overrides

        public override void OnDropCompleted(DragEventArgs arg, Point dropPoint)
        {
            var iItem = arg.Data.GetData(Resources.CodeItemFormat) as CodeItem;
            if (iItem == null || IsRaw)
                //we are not moving around an item, so add new code item. Also, when raw, we can't work with code items, instead we work on the neuron directly.
                TryCreateNewCodeItem(arg, dropPoint);
            else
                TryMoveItem(arg, dropPoint, iItem);
        }

        /// <summary>
        ///     moves the code item.  This is actually a fake move, there is always a new object created.
        ///     This is done so we can easely create duplicate visual items that point to the same code item, which
        ///     is automatically done when the dragsource doesn't remove the item.
        /// </summary>
        private void TryMoveItem(DragEventArgs arg, Point dropPoint, CodeItem iItem)
        {
            var iNew = CodeItems.CodeEditor.CreateCodeItemFor(iItem.Item);
            Debug.Assert(iNew != null);
            Presenter.Content = iNew;
        }

        private void TryCreateNewCodeItem(DragEventArgs obj, Point dropPoint)
        {
            var iId = (ulong)obj.Data.GetData(Resources.NeuronIDFormat);
            var iPresenter = Presenter;

            if (IsRaw == false)
            {
                var iNew = CodeItems.CodeEditor.CreateCodeItemFor(Brain.Brain.Current[iId]);
                Debug.Assert(iNew != null);
                iPresenter.Content = iNew;
            }
            else
                iPresenter.Content = Brain.Brain.Current[iId];
        }

        public override bool IsValidDataObject(IDataObject obj)
        {
            var iRes = false;
            Type iResultType = null;
            if (obj.GetDataPresent(Resources.DelayLoadResultType))
            {
                var iSource = obj.GetData(Resources.DelayLoadResultType) as DragSourceBase;
                Debug.Assert(iSource != null);
                iResultType = iSource.GetDelayLoadResultType(obj);
            }

            if (IsRaw == false)
            {
                if (obj.GetDataPresent(Resources.CodeItemFormat))
                {
                    var iItem = obj.GetData(Resources.CodeItemFormat) as CodeItem;
                    if (AllowedType != null)
                    {
                        if (iResultType == null)
                            iResultType = iItem.GetType();
                        iRes = Presenter.Content != iItem && AllowedType.IsAssignableFrom(iResultType);
                    }
                    else
                        iRes = Presenter.Content != iItem;
                }
                else if (obj.GetDataPresent(Resources.NeuronIDFormat))
                {
                    if (AllowedNeuronType != null)
                    {
                        if (iResultType == null)
                        {
                            var iNeuron = (ulong)obj.GetData(Resources.NeuronIDFormat);
                            iResultType = Brain.Brain.Current[iNeuron].GetType();
                        }
                        iRes = AllowedNeuronType.IsAssignableFrom(iResultType);
                    }
                    else
                        iRes = true;
                }
            }
            else
            {
                iRes = obj.GetDataPresent(Resources.NeuronIDFormat);
                if (iRes && AllowedType != null)
                {
                    if (iResultType == null)
                    {
                        var iNeuron = (ulong)obj.GetData(Resources.NeuronIDFormat);
                        iResultType = Brain.Brain.Current[iNeuron].GetType();
                    }
                    iRes = AllowedNeuronType.IsAssignableFrom(iResultType);
                }
            }
            return iRes;
        }

        /// <summary>
        ///     Gets the effect that should be used for the drop operation.
        /// </summary>
        /// <remarks>
        ///     we always want a move when the item comes from the same code editor, otherwise we request a copy
        /// </remarks>
        /// <param name="e">The drag event arguments.</param>
        /// <returns>The prefered effect to use.</returns>
        public override DragDropEffects GetEffect(DragEventArgs e)
        {
            var iItem = e.Data.GetData(Resources.CodeItemFormat) as CodeItem;
            if (iItem != null)
            {
                var iThisRoot = Item.Root as CodeEditorPage;
                Debug.Assert(iThisRoot != null);
                if (iItem.Root != iThisRoot)
                    return DragDropEffects.Copy;
                return DragDropEffects.Move;
            }
            return (e.KeyStates & DragDropKeyStates.ControlKey) != 0 ? DragDropEffects.Copy : DragDropEffects.Move;
            //we are dragging an item around on the design surface, so move it, only create new copies when control is pressed.
        }

        #endregion Overrides
    }
}