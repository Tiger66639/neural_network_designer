﻿using JaStDev.HAB.Designer.DragDrop;
using JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems;
using JaStDev.HAB.Designer.Properties;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

namespace JaStDev.HAB.Designer.Editors.CodeEditor.Dragdrop
{
    public class CodeListItemDragAdvisor : DragSourceBase
    {
        public CodeListItemDragAdvisor()
        {
            SupportedFormat = Resources.NeuronIDFormat;
            //this is not really used, simply informtive: this is our main data type.
        }

        #region UsePreviewEvents

        /// <summary>
        ///     Gets if the preview event versions should be used or not.
        /// </summary>
        /// <remarks>
        ///     don't use preview events cause than the sub drop points don't get used but only the main list cause this gets the
        ///     events first,
        ///     while we usually want to drop in a sub drop point.
        /// </remarks>
        public override bool UsePreviewEvents
        {
            get { return false; }
        }

        #endregion UsePreviewEvents

        #region Item

        /// <summary>
        ///     Gets the code item that is the datacontext of the target of this drop advisor.
        /// </summary>
        public CodeItem Item
        {
            get { return ((FrameworkElement)SourceUI).DataContext as CodeItem; }
        }

        #endregion Item

        #region CodeList

        /// <summary>
        ///     Gets the list containing all the code that the UI to which advisor is attached too, displays data for.
        /// </summary>
        public CodeItemCollection CodeList
        {
            get
            {
                ListBoxItem iListBoxItem = TreeHelper.FindInTree<ListBoxItem>(SourceUI);
                var iItemsControl = ItemsControl.ItemsControlFromItemContainer(iListBoxItem);
                Debug.Assert(iItemsControl != null);
                return iItemsControl.ItemsSource as CodeItemCollection;
            }
        }

        #endregion CodeList

        public override void FinishDrag(UIElement draggedElt, DragDropEffects finalEffects)
        {
            if ((finalEffects & DragDropEffects.Move) == DragDropEffects.Move)
            {
                var iItem = Item;
                var iList = CodeList;
                Debug.Assert(iList != null);
                iList.Remove(Item);
            }
        }

        /// <summary>
        ///     We can drag when there is a content in the presenter.
        /// </summary>
        public override bool IsDraggable(UIElement dragElt)
        {
            return Item != null && CodeList != null;
        }

        /// <summary>
        ///     we override cause we put the image to use + an ulong if it is a neuron, or a ref to the mind map item.
        ///     If the item is a link, we also store which side of the link it was, so we can adjust it again (+ update it).
        /// </summary>
        public override DataObject GetDataObject(UIElement draggedElt)
        {
            var iDragged = (FrameworkElement)draggedElt;
            var iObj = new DataObject();

            var iContent = Item;

            iObj.SetData(Resources.UIElementFormat, iDragged);
            iObj.SetData(Resources.CodeItemFormat, iContent);
            iObj.SetData(Resources.NeuronIDFormat, iContent.Item.ID);

            return iObj;
        }
    }
}