﻿using System.Windows.Controls;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Editors.@base;
using JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems;

namespace JaStDev.HAB.Designer.Editors.CodeEditor
{
   /// <summary>
   /// Interaction logic for CtrlBoolExpression.xaml
   /// </summary>
   public partial class CtrlBoolExpression : CtrlEditorItem
   {
      public CtrlBoolExpression()
      {
         InitializeComponent();
      }

      private void Operator_SelectionChanged(object sender, SelectionChangedEventArgs e)
      {
         if (e.AddedItems.Count > 0)
         {
            Neuron iOp = e.AddedItems[0] as Neuron;
            OperatorPart.Content = new CodeItemStatic(iOp);
         }
         else if (e.RemovedItems.Count > 0)
            OperatorPart.Content = null;
      }

      private void ClusterLists_SelectionChanged(object sender, SelectionChangedEventArgs e)
      {
         if (e.AddedItems.Count > 0)
         {
            Neuron iOp = e.AddedItems[0] as Neuron;
            CPListToSearch.Content = new CodeItemStatic(iOp);
         }
         else if (e.RemovedItems.Count > 0)
            CPListToSearch.Content = null;
      }
      
   }
}
