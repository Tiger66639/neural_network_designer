﻿
using JaStDev.HAB.Designer.Editors.@base;

namespace JaStDev.HAB.Designer.Editors.CodeEditor
{
   /// <summary>
   /// Interaction logic for CtrlAssignment.xaml
   /// </summary>
   public partial class CtrlAssignment : CtrlEditorItem
   {
      public CtrlAssignment()
      {
         InitializeComponent();
      }
   }
}
