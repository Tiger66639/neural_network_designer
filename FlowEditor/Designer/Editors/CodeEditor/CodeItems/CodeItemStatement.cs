﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Editors.@base;
using JaStDev.HAB.Events;
using JaStDev.HAB.Expressions;
using JaStDev.HAB.Instructions;

namespace JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems
{
   /// <summary>
   /// Provides a wrapper for <see cref="Statement"/> objects so that they work properly with the undo system 
   /// and the ui.
   /// </summary>
   public class CodeItemStatement : CodeItem
   {

      CodeItemCollection fArguments;

      #region ctor-dtor
      public CodeItemStatement(Statement toWrap)
         : base(toWrap)
      {
         
      }
 
      #endregion

      #region Prop
      #region Instruction

      /// <summary>
      /// Gets/sets the instruction to use.
      /// </summary>
      /// <remarks>
      /// Wrapper for <see cref="Statement.Instruction"/> to provide undo info and ui updating.
      /// </remarks>
      public Instruction Instruction
      {
         get
         {
            return ((Statement)Item).Instruction;
         }
         set
         {
            OnPropertyChanging("Instruction", ((Statement)Item).Instruction, value);
            ((Statement)Item).Instruction = value;
            //PropertyChanged event is raised by the Link_Changed event handler, making certain that all updates are correctly done.
         }
      }

      #endregion

      #region Arguments

      /// <summary>
      /// Gets the list of arguments for this statement.
      /// </summary>
      public CodeItemCollection Arguments
      {
         get { return fArguments; }
         internal set
         {
            if (fArguments != value)
            {
               fArguments = value;
               OnPropertyChanged("Arguments");
            }
         }
      }

      #endregion 
      #endregion

      #region Functions

      /// <summary>
      /// Called when the <see cref="CodeItem.Item"/> has changed.
      /// </summary>
      /// <param name="value">The value.</param>
      protected override void OnItemChanged(Neuron value)
      {
         base.OnItemChanged(value);
         fArguments = null;
         LoadArguments();
      }

      /// <summary>
      /// descendents that need to update links that changed can do this through this function.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      protected override void InternalLinkChanged(Link sender, LinkChangedEventArgs e)
      {
         base.InternalLinkChanged(sender, e);
         if (sender.MeaningID == (ulong)PredefinedNeurons.Instruction)
            OnPropertyChanged("Instruction");
         else if (sender.MeaningID == (ulong)PredefinedNeurons.Arguments)
            LoadArguments();
      }

      private void LoadArguments()
      {
         NeuronCluster iNew = ((Statement)Item).ArgumentsCluster;
         if (fArguments == null || iNew != fArguments.Cluster)
         {
            if (iNew != null)
               Arguments = new CodeItemCollection(this, iNew);
            else
               Arguments = new CodeItemCollection(this, (ulong)PredefinedNeurons.Arguments);
         }
      }

      /// <summary>
      /// Removes the current code item from the code list, but not the actual neuron that represents the code
      /// item, this stays in the brain, it is simply no longer used in this code list.
      /// </summary>
      /// <param name="child"></param>
      public override void RemoveChildFromCode(EditorItem child)
      {
         CodeItem iChild = (CodeItem)child;
         if (Arguments.Remove(iChild) == false)
            base.RemoveChildFromCode(child);
      }

      #endregion


     
   }
}
