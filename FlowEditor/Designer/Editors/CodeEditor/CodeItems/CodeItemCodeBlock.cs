﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Editors.@base;
using JaStDev.HAB.Events;
using JaStDev.HAB.Expressions;

namespace JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems
{
   public class CodeItemCodeBlock : ExpandableCodeItem
   {
      #region fields
      CodeItemCollection fChildren;
      #endregion


      /// <summary>
      /// Initializes a new instance of the <see cref="CodeItemCodeBlock"/> class.
      /// </summary>
      /// <param name="toWrap">To wrap.</param>
      public CodeItemCodeBlock(ExpressionsBlock toWrap)
         : base(toWrap)
      {
         IsExpanded = false;
      }

      #region Children

      /// <summary>
      /// Gets the list of statements for this conditional expression.
      /// </summary>
      public CodeItemCollection Children
      {
         get { return fChildren; }
         private set
         {
            if (fChildren != value)
            {
               fChildren = value;
               OnPropertyChanged("Children");
            }
         }
      }

      #endregion

      #region Functions

      /// <summary>
      /// Called when the <see cref="CodeItem.Item"/> has changed.
      /// </summary>
      /// <param name="value">The value.</param>
      protected override void OnItemChanged(Neuron value)
      {
         base.OnItemChanged(value);
         fChildren = null;                                                                         //item changed, so children always need to be reloaded.
         LoadChildren();
      }

      /// <summary>
      /// descendents that need to update links that changed can do this through this function.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      protected override void InternalLinkChanged(Link sender, LinkChangedEventArgs e)
      {
         base.InternalLinkChanged(sender, e);
         if (sender.MeaningID == (ulong)PredefinedNeurons.Statements)
            LoadChildren();
      }

      /// <summary>
      /// Loads the children.
      /// </summary>
      private void LoadChildren()
      {
         NeuronCluster iStatements = ((ExpressionsBlock)Item).StatementsCluster;
         if (fChildren == null || iStatements != fChildren.Cluster)
         {
            if (iStatements != null)
               Children = new CodeItemCollection(this, iStatements);
            else
               Children = new CodeItemCollection(this, (ulong)PredefinedNeurons.Statements);
         }
      }

      /// <summary>
      /// Removes the current code item from the code list, but not the actual neuron that represents the code
      /// item, this stays in the brain, it is simply no longer used in this code list.
      /// </summary>
      /// <param name="child"></param>
      public override void RemoveChildFromCode(EditorItem child)
      {
         CodeItem iChild = (CodeItem)child;
         if (Children.Remove(iChild) == false)
            base.RemoveChildFromCode(iChild);
      } 
      #endregion
   }
}
