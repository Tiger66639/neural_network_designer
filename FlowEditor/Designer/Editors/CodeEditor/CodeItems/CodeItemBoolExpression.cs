﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Editors.@base;
using JaStDev.HAB.Events;
using JaStDev.HAB.Expressions;

namespace JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems
{
   public class CodeItemBoolExpression : CodeItemResult
   {

      #region fields
      CodeItemResult fLeftPart;
      CodeItemResult fRightPart;
      CodeItemResult fListToSearch;
      CodeItemResult fOperator;
      Visibility fListToSearchVisibility = Visibility.Collapsed;
      Orientation fOrientation = Orientation.Horizontal;
      #endregion

      #region ctor - dtor
      /// <summary>
      /// Initializes a new instance of the <see cref="CodeItemBoolExpression"/> class.
      /// </summary>
      /// <param name="toWrap">To wrap.</param>
      public CodeItemBoolExpression(BoolExpression toWrap)
         : base(toWrap)
      {
         
      }


      #endregion

      #region prop

      #region Orientation

      /// <summary>
      /// Gets/sets the orientation to use for displaying the bool expression.
      /// </summary>
      /// <remarks>
      /// This is usefull for longer expressions so we can show the right part on a new line.
      /// </remarks>
      public Orientation Orientation
      {
         get
         {
            return fOrientation;
         }
         set
         {
            fOrientation = value;
            OnPropertyChanged("Orientation");
         }
      }

      #endregion

      #region NotHasLeftPart

      /// <summary>
      /// Gets the if the loopitem is present or not.
      /// </summary>
      public bool NotHasLeftPart
      {
         get { return ((BoolExpression)Item).LeftPart == null; }
      }

      #endregion

      #region LeftPart

      /// <summary>
      /// Gets/sets the left part of the boolean expression.
      /// </summary>
      public CodeItemResult LeftPart
      {
         get
         {
            return fLeftPart;
         }
         set
         {
            if (fLeftPart != value)
            {
               OnPropertyChanging("LeftPart", fLeftPart, value);
               InternalSetLeftPart(value);
               InternalChange = true;
               try
               {
                  if (value != null)
                     ((BoolExpression)Item).LeftPart = value.Item;
                  else
                     ((BoolExpression)Item).LeftPart = null;
               }
               finally
               {
                  InternalChange = false;
               }
            }
         }
      }

      /// <summary>
      /// only stores the new value, warns the undo system + the UI
      /// </summary>
      /// <param name="value">The new value.</param>
      private void InternalSetLeftPart(CodeItemResult value)
      {
         if (fLeftPart != null)
            UnRegisterChild(fLeftPart);
         fLeftPart = value;
         if (fLeftPart != null)
            RegisterChild(fLeftPart);
         OnPropertyChanged("LeftPart");
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(OnPropertyChanged), "NotHasLeftPart");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event.
         UpdateOrientation();
      }

      #endregion

      #region NotHasRightPart

      /// <summary>
      /// Gets the if the loopitem is present or not.
      /// </summary>
      public bool NotHasRightPart
      {
         get { return ((BoolExpression)Item).RightPart == null; }
      }

      #endregion
      
      #region RightPart

      /// <summary>
      /// Gets/sets the right part of the bool expression
      /// </summary>
      public CodeItemResult RightPart
      {
         get
         {
            return fRightPart;
         }
         set
         {
            if (fRightPart != value)
            {
               OnPropertyChanging("RightPart", fRightPart, value);
               InternalSetRightPart(value);
               InternalChange = true;
               try
               {
                  if (value != null)
                     ((BoolExpression)Item).RightPart = value.Item;
                  else
                     ((BoolExpression)Item).RightPart = null;
               }
               finally
               {
                  InternalChange = false;
               }
            }
         }
      }

      private void InternalSetRightPart(CodeItemResult value)
      {
         if (fRightPart != null)
            UnRegisterChild(fRightPart);
         fRightPart = value;
         if (fRightPart != null)
            RegisterChild(fRightPart);
         OnPropertyChanged("RightPart");
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(OnPropertyChanged), "NotHasRightPart");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event.
         UpdateOrientation();
      }

      

      #endregion

      #region ListToSearch

      /// <summary>
      /// Gets/sets the identifier for the list to search in a 'contains' statement (the to, from, ...)
      /// </summary>
      public CodeItemResult ListToSearch
      {
         get
         {
            return fListToSearch;
         }
         set
         {
            if (fListToSearch != value)
            {
               OnPropertyChanging("ListToSearch", fListToSearch, value);
               InternalSetListToSearch(value);
               InternalChange = true;
               try
               {
                  if (value != null)
                     ((BoolExpression)Item).ListToSearch = value.Item;
                  else
                     ((BoolExpression)Item).ListToSearch = null;
               }
               finally
               {
                  InternalChange = false;
               }
            }
         }
      }

      private void InternalSetListToSearch(CodeItemResult value)
      {
         if (fListToSearch != null)
            UnRegisterChild(fListToSearch);
         fListToSearch = value;
         if (fListToSearch != null)
            RegisterChild(fListToSearch);
         OnPropertyChanged("ListToSearch");
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(OnPropertyChanged), "NotHasListToSearch");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event.
      }

      #endregion

      #region NotHasListToSearch

      /// <summary>
      /// Gets the if the operator is present or not.
      /// </summary>
      public bool NotHasListToSearch
      {
         get { return ((BoolExpression)Item).ListToSearch == null; }
      }

      #endregion


      #region ListToSearchVisibility

      /// <summary>
      /// Gets the if the case item should be displayed.
      /// </summary>
      public Visibility ListToSearchVisibility
      {
         get { return fListToSearchVisibility; }
         internal set
         {
            fListToSearchVisibility = value;
            OnPropertyChanged("ListToSearchVisibility");
         }
      }

      #endregion
      
      #region Operator

      /// <summary>
      /// Gets/sets the operator to use for the boolean expression
      /// </summary>
      public CodeItemResult Operator
      {
         get
         {
            return fOperator;
         }
         set
         {
            if (fOperator != value)
            {
               OnPropertyChanging("Operator", fOperator, value);
               InternalSetOperator(value);
               InternalChange = true;
               try
               {
                  if (value != null)
                     ((BoolExpression)Item).Operator = value.Item;
                  else
                     ((BoolExpression)Item).Operator = null;
               }
               finally
               {
                  InternalChange = false;
               }
            }
         }
      }

      private void InternalSetOperator(CodeItemResult value)
      {
         if (fOperator != null)
            UnRegisterChild(fOperator);
         fOperator = value;
         if (fOperator != null)
            RegisterChild(fOperator);
         OnPropertyChanged("Operator");
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(OnPropertyChanged), "NotHasOperator");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event.
         if (value is CodeItemStatic)
         {
            if (((CodeItemStatic)value).Item.ID == (ulong)PredefinedNeurons.Contains)
               ListToSearchVisibility = Visibility.Visible;
            else
               ListToSearchVisibility = Visibility.Collapsed;
         }
         else
            ListToSearchVisibility = Visibility.Visible;                                     //variable operators can always end up as a contains, so always allow list to search.
      }

      #endregion

      #region NotHasOperator

      /// <summary>
      /// Gets the if the operator is present or not.
      /// </summary>
      public bool NotHasOperator
      {
         get { return ((BoolExpression)Item).Operator == null; }
      }

      #endregion

      #endregion

      #region Functions

      /// <summary>
      /// Automatically updates the orientation based on the type of code items found in left and right.
      /// </summary>
      private void UpdateOrientation()
      {
         if (LeftPart is CodeItemBoolExpression || RightPart is CodeItemBoolExpression)
            Orientation = Orientation.Vertical;
         else
            Orientation = Orientation.Horizontal;
      }

      /// <summary>
      /// Called when the <see cref="CodeItem.Item"/> has changed.
      /// </summary>
      /// <param name="value">The value.</param>
      protected override void OnItemChanged(Neuron value)
      {
         base.OnItemChanged(value);
         BoolExpression iToWrap = value as BoolExpression;
         if (iToWrap != null)
         {
            Neuron iFound = iToWrap.LeftPart;
            if (iFound != null)
               InternalSetLeftPart((CodeItemResult)CodeEditor.CreateCodeItemFor(iFound));
            iFound = iToWrap.RightPart;
            if (iFound != null)
               InternalSetRightPart((CodeItemResult)CodeEditor.CreateCodeItemFor(iFound));
            iFound = iToWrap.ListToSearch;
            if (iFound != null)
               InternalSetListToSearch((CodeItemResult)CodeEditor.CreateCodeItemFor(iFound));
            iFound = iToWrap.Operator;
            if (iFound != null)
               InternalSetOperator((CodeItemResult)CodeEditor.CreateCodeItemFor(iFound));
         }
      }

      /// <summary>
      /// descendents that need to update links that changed can do this through this function.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      protected override void InternalLinkChanged(Link sender, LinkChangedEventArgs e)
      {
         base.InternalLinkChanged(sender, e);
         if (sender.MeaningID == (ulong)PredefinedNeurons.LeftPart)
            if (e.Action == BrainAction.Removed)
               InternalSetLeftPart(null);
            else
               InternalSetLeftPart((CodeItemResult)CodeEditor.CreateCodeItemFor(Brain.Brain.Current[e.NewTo]));
         else if (sender.MeaningID == (ulong)PredefinedNeurons.RightPart)
            if (e.Action == BrainAction.Removed)
               InternalSetRightPart(null);
            else
               InternalSetRightPart((CodeItemResult)CodeEditor.CreateCodeItemFor(Brain.Brain.Current[e.NewTo]));
         else if (sender.MeaningID == (ulong)PredefinedNeurons.ListToSearch)
            if (e.Action == BrainAction.Removed)
               InternalSetListToSearch(null);
            else
               InternalSetListToSearch((CodeItemResult)CodeEditor.CreateCodeItemFor(Brain.Brain.Current[e.NewTo]));
         else if (sender.MeaningID == (ulong)PredefinedNeurons.Operator)
            if (e.Action == BrainAction.Removed)
               InternalSetOperator(null);
            else
               InternalSetOperator((CodeItemResult)CodeEditor.CreateCodeItemFor(Brain.Brain.Current[e.NewTo]));
      }

      public override void RemoveChildFromCode(EditorItem child)
      {
         if (LeftPart == child)
            LeftPart = null;
         else if (RightPart == child)
            RightPart = null;
         else if (ListToSearch == child)
            ListToSearch = null;
         else if (Operator == child)
            Operator = null;
         else
            base.RemoveChildFromCode(child);
      }

      #endregion

   }
}
