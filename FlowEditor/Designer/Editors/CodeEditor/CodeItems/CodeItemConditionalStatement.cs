﻿using System;
using System.Windows;
using System.Windows.Threading;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Editors.@base;
using JaStDev.HAB.Events;
using JaStDev.HAB.Expressions;

namespace JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems
{
   public class CodeItemConditionalStatement : ExpandableCodeItem
   {

      #region fields

      CodeItemCollection fChildren;
      CodeItemResult fLoopStyle;
      CodeItemVariable fCaseItem;
      CodeItemVariable fLoopItem;
      Visibility fLoopLineVisibility = Visibility.Collapsed;
      Visibility fCaseItemVisibility = Visibility.Collapsed;
      Visibility fLoopItemVisibility = Visibility.Collapsed;

      #endregion

      #region ctor-dtor
      /// <summary>
      /// Initializes a new instance of the <see cref="CodeItemConditionalStatement"/> class.
      /// </summary>
      /// <param name="towrap">The towrap.</param>
      public CodeItemConditionalStatement(ConditionalStatement towrap)
         : base(towrap)
      {
         
      }

      #endregion


      #region Prop
      #region LoopStyle

      /// <summary>
      /// Gets/sets the style of looping that should be used.
      /// </summary>
      public CodeItemResult LoopStyle
      {
         get
         {
            return fLoopStyle;
         }
         set
         {
            OnPropertyChanging("LoopStyle", fLoopStyle, value);
            InternalSetLoopStyle(value);
            InternalChange = true;
            try
            {
               if (value != null)
                  ((ConditionalStatement)Item).LoopStyle = value.Item;
               else
                  ((ConditionalStatement)Item).LoopStyle = null;
            }
            finally
            {
               InternalChange = false;
            }
         }
      }

      private void InternalSetLoopStyle(CodeItemResult value)
      {
         if (fLoopStyle != null)
            UnRegisterChild(fLoopStyle);
         fLoopStyle = value;
         if (fLoopStyle != null)
         {
            RegisterChild(fLoopStyle);
            if (fLoopStyle.Item.ID == (ulong)PredefinedNeurons.Looped
               || fLoopStyle.Item.ID == (ulong)PredefinedNeurons.CaseLooped
               || fLoopStyle.Item.ID == (ulong)PredefinedNeurons.ForEach
               || fLoopStyle is CodeItemVariable)
               LoopLineVisibility = Visibility.Visible;
            else
               LoopLineVisibility = Visibility.Collapsed;
            if (fLoopStyle.Item.ID == (ulong)PredefinedNeurons.Case
               || fLoopStyle.Item.ID == (ulong)PredefinedNeurons.CaseLooped
               || fLoopStyle is CodeItemVariable)
               CaseItemVisibility = Visibility.Visible;
            else
               CaseItemVisibility = Visibility.Collapsed;
            if (fLoopStyle.Item.ID == (ulong)PredefinedNeurons.ForEach
               || fLoopStyle is CodeItemVariable)
               LoopItemVisibility = Visibility.Visible;
            else
               LoopItemVisibility = Visibility.Collapsed;
         }
         else
            LoopLineVisibility = Visibility.Collapsed;
         OnPropertyChanged("LoopStyle");
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(OnPropertyChanged), "NotHasLoopStyle");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event.
      }

      #endregion

      #region NotHasLoopItem

      /// <summary>
      /// Gets the if the loopitem is present or not.
      /// </summary>
      public bool NotHasLoopStyle
      {
         get { return ((ConditionalStatement)Item).LoopStyle == null; }
      }

      #endregion

      #region LoopLineVisibility

      /// <summary>
      /// Gets the if the loop lines should be displayed or not.
      /// </summary>
      public Visibility LoopLineVisibility
      {
         get { return fLoopLineVisibility; }
         internal set
         {
            fLoopLineVisibility = value;
            OnPropertyChanged("LoopLineVisibility");
         }
      }

      #endregion


      #region CaseItemVisibility

      /// <summary>
      /// Gets the if the case item should be displayed.
      /// </summary>
      public Visibility CaseItemVisibility
      {
         get { return fCaseItemVisibility; }
         internal set
         {
            fCaseItemVisibility = value;
            OnPropertyChanged("CaseItemVisibility");
         }
      }

      #endregion


      #region LoopItemVisibility

      /// <summary>
      /// Gets if the loop item should be displayed.
      /// </summary>
      public Visibility LoopItemVisibility
      {
         get { return fLoopItemVisibility; }
         internal set
         {
            fLoopItemVisibility = value;
            OnPropertyChanged("LoopItemVisibility");
         }
      }

      #endregion

      #region CaseItem

      /// <summary>
      /// Gets/sets the info to search for.
      /// </summary>
      /// <remarks>
      /// Should only be visible if we are searching an info list.
      /// </remarks>
      public CodeItemVariable CaseItem
      {
         get
         {
            return fCaseItem;
         }
         set
         {
            if (fCaseItem != value)
            {
               OnPropertyChanging("CaseItem", fCaseItem, value);
               InternalChange = true;
               try
               {
                  if (value != null)
                     ((ConditionalStatement)Item).CaseItem = (Variable)value.Item;
                  else
                     ((ConditionalStatement)Item).CaseItem = null;
               }
               finally
               {
                  InternalChange = false;
               }
               InternalSetCaseItem(value);
            }
         }
      }

      private void InternalSetCaseItem(CodeItemVariable value)
      {
         if (fCaseItem != null)
            UnRegisterChild(fCaseItem);
         fCaseItem = value;
         if (fCaseItem != null)
            RegisterChild(fCaseItem);
         OnPropertyChanged("CaseItem");
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(OnPropertyChanged), "NotHasCaseItem");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event.
      }

      #endregion

      #region NotHasCaseItem

      /// <summary>
      /// Gets the if the loopitem is present or not.
      /// </summary>
      public bool NotHasCaseItem
      {
         get { return ((ConditionalStatement)Item).CaseItem == null; }
      }

      #endregion

      #region LoopItem

      /// <summary>
      /// Gets/sets the info to search for.
      /// </summary>
      /// <remarks>
      /// Should only be visible if we are searching an info list.
      /// </remarks>
      public CodeItemVariable LoopItem
      {
         get
         {
            return fLoopItem;
         }
         set
         {
            if (fLoopItem != value)
            {
               OnPropertyChanging("LoopItem", fLoopItem, value);
               InternalChange = true;
               try
               {
                  if (value != null)
                     ((ConditionalStatement)Item).LoopItem = (Variable)value.Item;
                  else
                     ((ConditionalStatement)Item).LoopItem = null;
               }
               finally
               {
                  InternalChange = false;
               }
               InternalSetLoopItem(value);
            }
         }
      }

      private void InternalSetLoopItem(CodeItemVariable value)
      {
         if (fLoopItem != null)
            UnRegisterChild(fLoopItem);
         fLoopItem = value;
         if (fLoopItem != null)
            RegisterChild(fLoopItem);
         OnPropertyChanged("LoopItem");
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(OnPropertyChanged), "NotHasLoopItem");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event
      }

      #endregion

      #region NotHasLoopItem

      /// <summary>
      /// Gets the if the loopitem is present or not.
      /// </summary>
      public bool NotHasLoopItem
      {
         get { return ((ConditionalStatement)Item).LoopItem == null; }
      }

      #endregion

      #region Children

      /// <summary>
      /// Gets the list of child code items.
      /// </summary>
      /// <remarks>
      /// If <see cref="CodeItem.HasChildren"/> is false, this reference is invalid.  The list
      /// is only filled if <see cref="ChildrenLoaded"/> is true.
      /// </remarks>
      public CodeItemCollection Children
      {
         get { return fChildren; }
         private set
         {
            if (fChildren != value)
            {
               fChildren = value;
               OnPropertyChanged("Children");
            }
         }
      }

      #endregion 
      #endregion

      #region Functions
      /// <summary>
      /// Called when the <see cref="CodeItem.Item"/> has changed.
      /// </summary>
      /// <param name="value">The value.</param>
      protected override void OnItemChanged(Neuron value)
      {
         base.OnItemChanged(value);
         ConditionalStatement iTowrap = value as ConditionalStatement;
         if (iTowrap != null)
         {
            Neuron iFound = iTowrap.LoopStyle;
            if (iFound != null)
               InternalSetLoopStyle((CodeItemResult)CodeEditor.CreateCodeItemFor(iFound));
            iFound = iTowrap.CaseItem;
            if (iFound != null)
               InternalSetCaseItem((CodeItemVariable)CodeEditor.CreateCodeItemFor(iFound));
            iFound = iTowrap.LoopItem;
            if (iFound != null)
               InternalSetLoopItem((CodeItemVariable)CodeEditor.CreateCodeItemFor(iFound));
         }
         fChildren = null;
         LoadChildren();
      }

      /// <summary>
      /// descendents that need to update links that changed can do this through this function.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      protected override void InternalLinkChanged(Link sender, LinkChangedEventArgs e)
      {
         base.InternalLinkChanged(sender, e);
         if (sender.MeaningID == (ulong)PredefinedNeurons.LoopStyle)
            if (e.Action == BrainAction.Removed)
               InternalSetLoopStyle(null);
            else
               InternalSetLoopStyle((CodeItemResult)CodeEditor.CreateCodeItemFor(Brain.Brain.Current[e.NewTo]));
         else if (sender.MeaningID == (ulong)PredefinedNeurons.CaseItem)
            if (e.Action == BrainAction.Removed)
               InternalSetCaseItem(null);
            else
               InternalSetCaseItem((CodeItemVariable)CodeEditor.CreateCodeItemFor(Brain.Brain.Current[e.NewTo]));
         else if (sender.MeaningID == (ulong)PredefinedNeurons.LoopItem)
            if (e.Action == BrainAction.Removed)
               InternalSetLoopItem(null);
            else
               InternalSetLoopItem((CodeItemVariable)CodeEditor.CreateCodeItemFor(Brain.Brain.Current[e.NewTo]));
         else if (sender.MeaningID == (ulong)PredefinedNeurons.Condition)
            LoadChildren();
      }



      private void LoadChildren()
      {
         NeuronCluster iConditions = ((ConditionalStatement)Item).ConditionsCluster;
         if (fChildren == null || iConditions != fChildren.Cluster)
         {
            if (iConditions != null)
               Children = new CodeItemCollection(this, iConditions);
            else
               Children = new CodeItemCollection(this, (ulong)PredefinedNeurons.Condition);
         }
      }

      /// <summary>
      /// Removes the current code item from the code list, but not the actual neuron that represents the code
      /// item, this stays in the brain, it is simply no longer used in this code list.
      /// </summary>
      /// <param name="child"></param>
      public override void RemoveChildFromCode(EditorItem child)
      {
         CodeItem iChild = (CodeItem)child;
         if (LoopStyle == iChild)
            LoopStyle = null;
         else if (CaseItem == iChild)
            CaseItem = null;
         else if (LoopItem == iChild)
            LoopItem = null;
         else if (Children.Remove(iChild) == false)
            base.RemoveChildFromCode(iChild);
      } 
      #endregion

   }
}

