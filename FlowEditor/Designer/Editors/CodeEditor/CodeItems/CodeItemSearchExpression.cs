﻿using System;
using System.Windows.Threading;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Editors.@base;
using JaStDev.HAB.Events;
using JaStDev.HAB.Expressions;

namespace JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems
{
   public class CodeItemSearchExpression : CodeItemResult
   {
      #region Fields

      CodeItemResult fInfoToSearchFor;
      CodeItemResult fListToSearch;
      CodeItemResult fSearchFor;
      CodeItemResult fToSearch;

      #endregion


      #region ctor-dtor
      /// <summary>
      /// Initializes a new instance of the <see cref="CodeItemSearchExpression"/> class.
      /// </summary>
      /// <param name="toWrap">To wrap.</param>
      public CodeItemSearchExpression(SearchExpression toWrap)
         : base(toWrap)
      {
         
      }


      #endregion


      #region Prop
      #region InfoToSearchFor

      /// <summary>
      /// Gets/sets the info to search for.
      /// </summary>
      /// <remarks>
      /// Should only be visible if we are searching an info list.
      /// </remarks>
      public CodeItemResult InfoToSearchFor
      {
         get
         {
            return fInfoToSearchFor;
         }
         set
         {
            if (fInfoToSearchFor != value)
            {
               OnPropertyChanging("InfoToSearchFor", fInfoToSearchFor, value);
               InternalSetInfoToSearchFor(value);
               InternalChange = true;
               try
               {
                  if (value != null)
                     ((SearchExpression)Item).InfoToSearchFor = value.Item;
                  else
                     ((SearchExpression)Item).InfoToSearchFor = null;
               }
               finally
               {
                  InternalChange = false;
               }
            }
         }
      }

      private void InternalSetInfoToSearchFor(CodeItemResult value)
      {
         if (fInfoToSearchFor != null)
            UnRegisterChild(fInfoToSearchFor);
         fInfoToSearchFor = value;
         if (fInfoToSearchFor != null)
            RegisterChild(fInfoToSearchFor);
         OnPropertyChanged("InfoToSearchFor");
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(OnPropertyChanged), "NotHasInfoToSearchFor");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event
      }

      #endregion

      #region NotHasInfoToSearchFor

      /// <summary>
      /// Gets if there is an InfoToSearchFor item.
      /// </summary>
      public bool NotHasInfoToSearchFor
      {
         get
         {
            return fInfoToSearchFor == null;
         }
      }

      #endregion

      #region ListToSearch

      /// <summary>
      /// Gets/sets the identifier for the list to search (to, from, info,...)
      /// </summary>
      public CodeItemResult ListToSearch
      {
         get
         {
            return fListToSearch;
         }
         set
         {
            OnPropertyChanging("ListToSearch", fListToSearch, value);
            InternalSetListToSearch(value);
            InternalChange = true;
            try
            {
               if (value != null)
                  ((SearchExpression)Item).ListToSearch = value.Item;
               else
                  ((SearchExpression)Item).ListToSearch = null;
            }
            finally
            {
               InternalChange = false;
            }

         }
      }

      private void InternalSetListToSearch(CodeItemResult value)
      {
         if (fListToSearch != null)
            UnRegisterChild(fListToSearch);
         fListToSearch = value;
         if (fListToSearch != null)
            RegisterChild(fListToSearch);
         OnPropertyChanged("ListToSearch");
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(OnPropertyChanged), "NotHasListToSearch");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event
      }

      #endregion

      #region NotHasListToSearch

      /// <summary>
      /// Gets if there is an ListToSearch item.
      /// </summary>
      public bool NotHasListToSearch
      {
         get
         {
            return fListToSearch == null;
         }
      }

      #endregion

      #region SearchFor

      /// <summary>
      /// Gets/sets the item(s) to search for.
      /// </summary>
      public CodeItemResult SearchFor
      {
         get
         {
            return fSearchFor;
         }
         set
         {
            OnPropertyChanging("SearchFor", fSearchFor, value);
            InternalSetSearchFor(value);
            InternalChange = true;
            try
            {
               if (value != null)
                  ((SearchExpression)Item).SearchFor = value.Item;
               else
                  ((SearchExpression)Item).SearchFor = null;
            }
            finally
            {
               InternalChange = false;
            }
         }
      }

      private void InternalSetSearchFor(CodeItemResult value)
      {
         if (fSearchFor != null)
            UnRegisterChild(fSearchFor);
         fSearchFor = value;
         if (fSearchFor != null)
            RegisterChild(fSearchFor);
         OnPropertyChanged("SearchFor");
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(OnPropertyChanged), "NotHasSearchFor");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event
      }

      #endregion

      #region NotHasSearchFor

      /// <summary>
      /// Gets if there is an SearchFor item.
      /// </summary>
      public bool NotHasSearchFor
      {
         get
         {
            return fSearchFor == null;
         }
      }

      #endregion

      #region ToSearch

      /// <summary>
      /// Gets/sets the object who's list should be searched.
      /// </summary>
      public CodeItemResult ToSearch
      {
         get
         {
            return fToSearch;
         }
         set
         {
            OnPropertyChanging("ToSearch", fToSearch, value);
            InternalSetToSearch(value);
            InternalChange = true;
            try
            {
               if (value != null)
                  ((SearchExpression)Item).ToSearch = value.Item;
               else
                  ((SearchExpression)Item).ToSearch = null;
            }
            finally
            {
               InternalChange = false;
            }
         }
      }

      private void InternalSetToSearch(CodeItemResult value)
      {
         if (fToSearch != null)
            UnRegisterChild(fToSearch);
         fToSearch = value;
         if (fToSearch != null)
            RegisterChild(fToSearch);
         OnPropertyChanged("ToSearch");
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(OnPropertyChanged), "NotHasToSearch");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event
      }

      #endregion

      #region HasToSearch

      /// <summary>
      /// Gets if there is an ToSearch item.
      /// </summary>
      public bool NotHasToSearch
      {
         get
         {
            return fToSearch == null;
         }
      }

      #endregion 
      #endregion

      #region Functions

      /// <summary>
      /// Called when the <see cref="CodeItem.Item"/> has changed.
      /// </summary>
      /// <param name="value">The value.</param>
      protected override void OnItemChanged(Neuron value)
      {
         base.OnItemChanged(value);
         SearchExpression iToWrap = value as SearchExpression;
         if (iToWrap != null)
         {
            Neuron iFound = iToWrap.InfoToSearchFor;
            if (iFound != null)
               InternalSetInfoToSearchFor((CodeItemResult)CodeEditor.CreateCodeItemFor(iFound));
            iFound = iToWrap.ListToSearch;
            if (iFound != null)
               InternalSetListToSearch((CodeItemResult)CodeEditor.CreateCodeItemFor(iFound));
            iFound = iToWrap.SearchFor;
            if (iFound != null)
               InternalSetSearchFor((CodeItemResult)CodeEditor.CreateCodeItemFor(iFound));
            iFound = iToWrap.ToSearch;
            if (iFound != null)
               InternalSetToSearch((CodeItemResult)CodeEditor.CreateCodeItemFor(iFound));
         }
      }

      /// <summary>
      /// descendents that need to update links that changed can do this through this function.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      protected override void InternalLinkChanged(Link sender, LinkChangedEventArgs e)
      {
         base.InternalLinkChanged(sender, e);
         if (sender.MeaningID == (ulong)PredefinedNeurons.InfoToSearchFor)
            if (e.Action == BrainAction.Removed)
               InternalSetInfoToSearchFor(null);
            else
               InternalSetInfoToSearchFor((CodeItemResult)CodeEditor.CreateCodeItemFor(Brain.Brain.Current[e.NewTo] as Expression));
         else if (sender.MeaningID == (ulong)PredefinedNeurons.ListToSearch)
            if (e.Action == BrainAction.Removed)
               InternalSetListToSearch(null);
            else
               InternalSetListToSearch((CodeItemResult)CodeEditor.CreateCodeItemFor(Brain.Brain.Current[e.NewTo] as Expression));
         else if (sender.MeaningID == (ulong)PredefinedNeurons.SearchFor)
            if (e.Action == BrainAction.Removed)
               InternalSetSearchFor(null);
            else
               InternalSetSearchFor((CodeItemResult)CodeEditor.CreateCodeItemFor(Brain.Brain.Current[e.NewTo] as Expression));
         else if (sender.MeaningID == (ulong)PredefinedNeurons.ToSearch)
            if (e.Action == BrainAction.Removed)
               InternalSetToSearch(null);
            else
               InternalSetToSearch((CodeItemResult)CodeEditor.CreateCodeItemFor(Brain.Brain.Current[e.NewTo] as Expression));
      }

      /// <summary>
      /// Removes the current code item from the code list, but not the actual neuron that represents the code
      /// item, this stays in the brain, it is simply no longer used in this code list.
      /// </summary>
      /// <param name="child"></param>
      public override void RemoveChildFromCode(EditorItem child)
      {
         if (InfoToSearchFor == child)
            InfoToSearchFor = null;
         else if (ListToSearch == child)
            ListToSearch = null;
         else if (SearchFor == child)
            SearchFor = null;
         else if (ToSearch == child)
            ToSearch = null;
         else
            base.RemoveChildFromCode(child);
      }

      #endregion

     
   }
}
