﻿
using JaStDev.HAB.Brain;

namespace JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems
{
   public class CodeItemStatic: CodeItemResult
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="CodeItemStatic"/> class.
      /// </summary>
      /// <param name="toWrap">The item to wrap.</param>
      public CodeItemStatic(Neuron toWrap)
      {
         Item = toWrap;
      }
   }
}
