﻿using System;
using System.Windows.Threading;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Editors.@base;
using JaStDev.HAB.Events;
using JaStDev.HAB.Expressions;

namespace JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems
{
   /// <summary>
   /// A wrapper for the <see cref="Variable"/> neuron.
   /// </summary>
   public class CodeItemVariable : CodeItemResult
   {

      #region Fields

      CodeItemResult fValue;

      #endregion

      #region ctor-dtor
      public CodeItemVariable(Variable toWrap)
         : base(toWrap)
      {
      }

      /// <summary>
      /// Gets or sets the owner.
      /// </summary>
      /// <value>The owner.</value>
      //public override JaStDev.Data.IOwnedObject Owner
      //{
      //   get
      //   {
      //      return base.Owner;
      //   }
      //   set
      //   {
      //      CodeEditorPage iRoot = Root;
      //      if (iRoot != null)
      //         iRoot.RegisteredVariables.Remove((Variable)Item);
      //      base.Owner = value;
      //      if (value != null)
      //         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, new Action(TryRegisterVar));              //we call this async, cause when still loading from xml, there is no root item yet, but there is already a parent.  When the application is idle, the xml should be loaded
               
      //   }
      //}

      /// <summary>
      /// Tries to register this variable with the root CodeEditorPage, if there is any.
      /// </summary>
      private void TryRegisterVar()
      {
         CodeEditorPage iRoot = Root as CodeEditorPage;
         if (iRoot != null && iRoot.RegisteredVariables.Contains((Variable)Item) == false && !(Item is SystemVariable))               //we don't want a system var cause this is already placed on the toolbox.
            iRoot.RegisteredVariables.Add((Variable)Item);
      }

      /// <summary>
      /// Releases unmanaged resources and performs other cleanup operations before the
      /// <see cref="CodeItemVariable"/> is reclaimed by garbage collection.
      /// </summary>
      ~CodeItemVariable()
      {
         if (Environment.HasShutdownStarted == false)                                     //don't try to remove the item if we are trying to quit the app, this causes a problem.
         {
            CodeEditorPage iRoot = Root as CodeEditorPage;
            if (iRoot != null)
               iRoot.RegisteredVariables.Remove((Variable)Item);
         }
      } 
      #endregion

      #region Value

      /// <summary>
      /// Gets/sets the object who's list should be searched.
      /// </summary>
      public CodeItemResult Value
      {
         get
         {
            return fValue;
         }
         set
         {
            OnPropertyChanging("Value", fValue, value);
            InternalSetValue(value);
            InternalChange = true;
            try
            {
               if (value != null)
                  ((Variable)Item).Value = value.Item;
               else
                  ((Variable)Item).Value = null;
            }
            finally
            {
               InternalChange = false;
            }
         }
      }

      private void InternalSetValue(CodeItemResult value)
      {
         if (fValue != null)
            UnRegisterChild(fValue);
         fValue = value;
         if (fValue != null)
            RegisterChild(fValue);
         OnPropertyChanged("Value");
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(OnPropertyChanged), "NotHasValue");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event
      }

      #endregion

      #region NotHasValue

      /// <summary>
      /// Gets if there is an ToSearch item.
      /// </summary>
      public bool NotHasValue
      {
         get
         {
            return fValue == null;
         }
      }

      #endregion 


      /// <summary>
      /// descendents that need to update links that changed can do this through this function.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      protected override void InternalLinkChanged(Link sender, LinkChangedEventArgs e)
      {
         base.InternalLinkChanged(sender, e);
         if (sender.MeaningID == (ulong)PredefinedNeurons.Value)
            OnPropertyChanged("Value");
      }

      /// <summary>
      /// Called when the <see cref="CodeItem.Item"/> has changed.
      /// </summary>
      /// <param name="value">The value.</param>
      protected override void OnItemChanged(Neuron value)
      {
         base.OnItemChanged(value);
         Variable iToWrap = value as Variable;
         if (iToWrap != null)
         {
            Neuron iFound = iToWrap.Value;
            if (iFound != null)
               InternalSetValue((CodeItemResult)CodeEditor.CreateCodeItemFor(iFound));
         }
      }

      /// <summary>
      /// Removes the current code item from the code list, but not the actual neuron that represents the code
      /// item, this stays in the brain, it is simply no longer used in this code list.
      /// </summary>
      /// <param name="child"></param>
      public override void RemoveChildFromCode(EditorItem child)
      {
         if (Value == child)
            Value = null;
         else
            base.RemoveChildFromCode(child);
      }
   }
}
