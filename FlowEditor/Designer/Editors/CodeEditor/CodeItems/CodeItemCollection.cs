﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.BrainData;

namespace JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems
{
   /// <summary>
   /// Contains a list of <see cref="CodeItem"/> objects that are the children of a neuron cluster.
   /// </summary>
   /// <remarks>
   /// This is a separete class since it has to make certain that all changes to this list are also 
   /// reflected in the underlying list of the <see cref="NeuronCluster"/>.
   /// <para>
   /// doesn't raise events when the list is changed cause this confuses the undo system.  Instead, drag
   /// handlers and functions generate their own undo data.
   /// </para>
   /// </remarks>
   public class CodeItemCollection : ClusterCollection<CodeItem>
   {

      #region ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="CodeItemCollection"/> class.
      /// </summary>
      /// <param name="owner">The <see cref="CodeEditor"/> that contains this code list.</param>
      /// <param name="childList">The NeuronCluster that contains all the code items.</param>
      public CodeItemCollection(INeuronWrapper owner, NeuronCluster childList): base(owner, childList)
      {

      }

      /// <summary>
      /// Initializes a new instance of the <see cref="CodeItemCollection"/> class.
      /// </summary>
      /// <param name="owner">The owner.</param>
      /// <param name="linkMeaning">The link meaning.</param>
      public CodeItemCollection(INeuronWrapper owner, ulong linkMeaning): base(owner, linkMeaning)
      {

      }

      #endregion

      #region Functions
      /// <summary>
      /// Called when a new wrapper object needs to be created for a neuron.
      /// </summary>
      /// <param name="toWrap">To wrap.</param>
      /// <returns></returns>
      /// <remarks>
      /// CodeEditors do: return CodeEditor.CreateCodeItemFor(toWrap)
      /// </remarks>
      public override CodeItem GetWrapperFor(Neuron toWrap)
      {
         return CodeEditor.CreateCodeItemFor(toWrap);
      }

      /// <summary>
      /// Returns the meaning that should be assigned to the cluster when it is newly created.
      /// </summary>
      /// <param name="linkMeaning">The meaning of the link between the wrapped cluster and the owner of this collection.</param>
      /// <returns></returns>
      protected override ulong GetListMeaning(ulong linkMeaning)
      {
         if (linkMeaning == (ulong)PredefinedNeurons.Arguments)                                    //a code list created for arguments, has a different meaning cause it can't really be executed (it can have statics) but is part of the code.
            return (ulong)PredefinedNeurons.ArgumentsList;
         else
            return (ulong)PredefinedNeurons.Code;
      }

      #endregion
   }
}
