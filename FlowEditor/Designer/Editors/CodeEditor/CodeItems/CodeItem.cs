﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Editors.@base;
using Expression = JaStDev.HAB.Expressions.Expression;

namespace JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems
{
   /// <summary>
   /// Represents a code item in a <see cref="CodeList"/>.
   /// </summary>
   public class CodeItem : EditorItem
   {

      #region fields
      bool fIsBreakPoint;
      #endregion

      #region ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="CodeItem"/> class.
      /// </summary>
      /// <param name="toWrap">The item to wrap.</param>
      public CodeItem(Neuron toWrap): base(toWrap)
      {
      }

      /// <summary>
      /// For static code item
      /// </summary>
      public CodeItem():base()
      {
      }

      #endregion

      #region prop

      #region IsBreakPoint

      /// <summary>
      /// Gets/sets if the processor should pause on this line.
      /// </summary>
      public bool IsBreakPoint
      {
         get
         {
            return fIsBreakPoint;
         }
         set
         {
            Expression iItem = Item as Expression;
            if (iItem != null)                                                                           //we could wrap a static neuron.
            {
               OnPropertyChanging("IsBreakPoint", fIsBreakPoint, value);
               fIsBreakPoint = value;
               if (value == true)
                  BrainData.BrainData.Current.BreakPoints.Add(iItem);
               else
                  BrainData.BrainData.Current.BreakPoints.Remove(iItem);
               OnPropertyChanged("IsBreakPoint");
            }
         }
      }

      #endregion 

      #endregion

      /// <summary>
      /// Called when the <see cref="CodeItem.Item"/> has changed.
      /// </summary>
      /// <param name="value">The value.</param>
      protected override void OnItemChanged(Neuron value)
      {
         if (value is Expression)
         {
            if (BrainData.BrainData.Current.BreakPoints.Contains((Expression)value) == true)
               IsBreakPoint = true;
         }
         base.OnItemChanged(value);
      }

   }
}
