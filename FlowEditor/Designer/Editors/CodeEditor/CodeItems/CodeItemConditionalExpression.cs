﻿using System;
using System.Windows.Threading;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Editors.@base;
using JaStDev.HAB.Events;
using JaStDev.HAB.Expressions;

namespace JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems
{
   public class CodeItemConditionalExpression : CodeItemCodeBlock
   {
      #region internal types

      class InternalLinkChangedEventArgs
      {
         public LinkChangedEventArgs EventArgs { get; set; }
      }

      #endregion

      #region fields
      CodeItemResult fCondition;
      #endregion

      #region ctor - dtor
      /// <summary>
      /// Initializes a new instance of the <see cref="CodeItemConditionalExpression"/> class.
      /// </summary>
      /// <param name="toWrap">To wrap.</param>
      public CodeItemConditionalExpression(ConditionalExpression toWrap)
         : base(toWrap)
      {
      }

      #endregion





      #region Prop
      #region Condition

      /// <summary>
      /// Gets/sets the Condition that needs to be evaluated to true for executing all the statements.
      /// </summary>
      /// <remarks>
      /// This is usually a <see cref="CodeItemBooleanExpression"/> but could also be a variable or static.
      /// </remarks>
      public CodeItemResult Condition
      {
         get
         {
            return fCondition;
         }
         set
         {
            if (fCondition != value)
            {
               OnPropertyChanging("Condition", fCondition, value);
               InternalSetCondition(value);
               InternalChange = true;
               try
               {
                  if (value != null)
                     ((ConditionalExpression)Item).Condition = (ResultExpression)value.Item;
                  else
                     ((ConditionalExpression)Item).Condition = null;
               }
               finally
               {
                  InternalChange = false;
               }
            }
         }
      }

      private void InternalSetCondition(CodeItemResult value)
      {
         if (fCondition != null)
            UnRegisterChild(fCondition);
         fCondition = value;
         if (fCondition != null)
            RegisterChild(fCondition);
         OnPropertyChanged("Condition");
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(OnPropertyChanged), "NotHasCondition");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event.
      }

      #endregion

      #region NotHasCondition

      /// <summary>
      /// Gets the if there is a condition or not (true when not).
      /// </summary>
      public bool NotHasCondition
      {
         get { return fCondition == null; }
      }

      #endregion 
      #endregion


      #region Functions

      /// <summary>
      /// Called when the <see cref="CodeItem.Item"/> has changed.
      /// </summary>
      /// <param name="value">The value.</param>
      protected override void OnItemChanged(Neuron value)
      {
         base.OnItemChanged(value);
         ConditionalExpression iToWrap = value as ConditionalExpression;
         if (iToWrap != null)
         {
            ResultExpression iFound = iToWrap.Condition;
            if (iFound != null)
               InternalSetCondition((CodeItemResult)CodeEditor.CreateCodeItemFor(iFound));
         }
      }

      /// <summary>
      /// descendents that need to update links that changed can do this through this function.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      protected override void InternalLinkChanged(Link sender, LinkChangedEventArgs e)
      {
         base.InternalLinkChanged(sender, e);
         if (sender.MeaningID == (ulong)PredefinedNeurons.Condition)
            if (e.Action == BrainAction.Removed)
               InternalSetCondition(null);
            else
               InternalSetCondition((CodeItemResult)CodeEditor.CreateCodeItemFor(Brain.Brain.Current[e.NewTo] as Expression));
         else
            base.InternalLinkChanged(sender, e);
      }

      /// <summary>
      /// Removes the current code item from the code list, but not the actual neuron that represents the code
      /// item, this stays in the brain, it is simply no longer used in this code list.
      /// </summary>
      /// <param name="child"></param>
      public override void RemoveChildFromCode(EditorItem child)
      {
         if (Condition == child)
            Condition = null;
         else
            base.RemoveChildFromCode(child);
      } 
      #endregion


   }
}
