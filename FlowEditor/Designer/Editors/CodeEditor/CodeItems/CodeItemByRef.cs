﻿using System;
using System.Windows.Threading;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Editors.@base;
using JaStDev.HAB.Events;
using JaStDev.HAB.Expressions;

namespace JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems
{
   public class CodeItemByRef: CodeItemResult
   {
      #region Fields

      CodeItem fArgument;

      #endregion

      /// <summary>
      /// Initializes a new instance of the <see cref="CodeItemByRef"/> class.
      /// </summary>
      /// <param name="toWrap">To wrap.</param>
      public CodeItemByRef(ByRefExpression toWrap): base(toWrap)
      {

      }

      #region Argument

      /// <summary>
      /// Gets/sets the object who's list should be searched.
      /// </summary>
      public CodeItem Argument
      {
         get
         {
            return fArgument;
         }
         set
         {
            OnPropertyChanging("Argument", fArgument, value);
            InternalSetArgument(value);
            InternalChange = true;
            try
            {
               if (value != null)
                  ((ByRefExpression)Item).Argument = value.Item;
               else
                  ((ByRefExpression)Item).Argument = null;
            }
            finally
            {
               InternalChange = false;
            }
         }
      }

      private void InternalSetArgument(CodeItem value)
      {
         if (fArgument != null)
            UnRegisterChild(fArgument);
         fArgument = value;
         if (fArgument != null)
            RegisterChild(fArgument);
         OnPropertyChanged("Argument");
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(OnPropertyChanged), "NotHasArgument");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event
      }

      #endregion

      #region NotHasArgument

      /// <summary>
      /// Gets if there is an ToSearch item.
      /// </summary>
      public bool NotHasArgument
      {
         get
         {
            return fArgument == null;
         }
      }

      #endregion


      /// <summary>
      /// descendents that need to update links that changed can do this through this function.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      protected override void InternalLinkChanged(Link sender, LinkChangedEventArgs e)
      {
         base.InternalLinkChanged(sender, e);
         if (sender.MeaningID == (ulong)PredefinedNeurons.Argument)
         {
            if (e.Action == BrainAction.Removed)
               InternalSetArgument(null);
            else
               InternalSetArgument(CodeEditor.CreateCodeItemFor(Brain.Brain.Current[e.NewTo]));
         }
      }

      /// <summary>
      /// Called when the <see cref="CodeItem.Item"/> has changed.
      /// </summary>
      /// <param name="value">The value.</param>
      protected override void OnItemChanged(Neuron value)
      {
         base.OnItemChanged(value);
         ByRefExpression iToWrap = value as ByRefExpression;
         if (iToWrap != null)
         {
            Neuron iFound = iToWrap.Argument;
            if (iFound != null)
               InternalSetArgument(CodeEditor.CreateCodeItemFor(iFound));
         }
      }

      /// <summary>
      /// Removes the current code item from the code list, but not the actual neuron that represents the code
      /// item, this stays in the brain, it is simply no longer used in this code list.
      /// </summary>
      /// <param name="child"></param>
      public override void RemoveChildFromCode(EditorItem child)
      {
         if (Argument == child)
            Argument = null;
         else
            base.RemoveChildFromCode(child);
      }
   }
}
