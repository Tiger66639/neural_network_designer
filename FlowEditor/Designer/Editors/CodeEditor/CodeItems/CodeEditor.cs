﻿using System;
using System.Xml.Serialization;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Editors.@base;
using JaStDev.HAB.Events;
using JaStDev.HAB.Expressions;
using JaStDev.HAB.Processor;
using JaStDev.HAB.Sins;

namespace JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems
{
   /// <summary>
   /// Encapsulates the 2 NeuronLists from a <see cref="Neuron"/> containing executable items for editing in a <see cref="CodeEditor"/>.
   /// If the item being encapsulated is a <see cref="NeuronCluster"/>, the children of the cluster are also depicted in a code view, 
   /// if possible.
   /// </summary>
   public class CodeEditor : NeuronEditor
   {
      ObservedCollection<CodeEditorPage> fEntryPoints;                               //rules, actions + possibly children goes in here
      int fSelectedIndex;
      bool fShowInProject;

      #region ctor -dtor
      /// <summary>
      /// Constructor with the item to wrap.
      /// </summary>
      /// <param name="toWrap"></param>
      public CodeEditor(Neuron toWrap): base()
      {
         if (toWrap == null)
            throw new ArgumentNullException();
         fEntryPoints = new ObservedCollection<CodeEditorPage>(this);
         ItemID = toWrap.ID;

         if (NeuronInfo != null)                                                               //also build the name correctly, based on the display title of the item to wrap.
               Name = NamePrefix + ": " + NeuronInfo.DisplayTitle;
            else
            Name = NamePrefix + ":";
      }

      /// <summary>
      /// Default empty constructor, for streaming.
      /// </summary>
      public CodeEditor():base()
      {
         fEntryPoints = new ObservedCollection<CodeEditorPage>(this);
      }


      
      #endregion

      #region Prop

      #region Icon
      public override string Icon
      {
         get { return "/Images/ViewCode_Enabled.png"; }
      } 
      #endregion
  

      #region EntryPoints

      /// <summary>
      /// Gets all the entry points defined by the neuron that this object wraps.
      /// </summary>
      [XmlIgnore]
      public ObservedCollection<CodeEditorPage> EntryPoints
      {
         get { return fEntryPoints; }
      }

      #endregion


      /// <summary>
      /// Gets the text that should be added to the front of the name to display as default value.
      /// </summary>
      /// <value>The name prefix.</value>
      public override string NamePrefix
      {
         get { return "Code"; }
      }

      #region ItemID

      /// <summary>
      /// Gets/sets the Id of the <see cref="Neuron"/> we are encapsulating.
      /// </summary>
      /// <remarks>
      /// This is for streaming.
      /// </remarks>
      public override ulong ItemID
      {
         get
         {
            return base.ItemID;
         }
         set
         {
            if (value != ItemID)
            {
               base.ItemID = value;
               LoadEntryPoints();
            }
         }
      }

      #endregion 

      #region ShowInProject

      /// <summary>
      /// Gets/sets wether this code editor is displayed as a shortcut in the project tree.
      /// </summary>
      public bool ShowInProject
      {
         get
         {
            return fShowInProject;
         }
         set
         {
            if (value != fShowInProject)
            {
               fShowInProject = value;
               if (value == true)
                  BrainData.BrainData.Current.CurrentEditorsList.Add(this);
               else
                  BrainData.BrainData.Current.Editors.RemoveRecursive(this);
               OnPropertyChanged("ShowInProject");
            }
         }
      }

      #endregion
      
      #region SelectedIndex

      /// <summary>
      /// Gets/sets the index of the page that is currently selected.
      /// </summary>
      /// <remarks>
      /// We store this so that this is remembered when the user switches screen.
      /// </remarks>
      public int SelectedIndex
      {
         get
         {
            return fSelectedIndex;
         }
         set
         {
            if (value != fSelectedIndex && value != -1)                                            //we don't want to loose the index value, so we skip -1, if we didn't do that, this prop would have no result.
            {
               fSelectedIndex = value;
               OnPropertyChanged("SelectedIndex");
            }
         }
      }

      #endregion

      #region SelectedListType
		/// <summary>
      /// Gets or sets the type of the selected list. This allows you to change the currently selected tab,
      /// based on debug info.
      /// </summary>
      /// <value>The type of the selected list.</value>
      public ExecListType SelectedListType
      {
         get
         {
            if (EntryPoints[SelectedIndex].Items.MeaningID == (ulong)PredefinedNeurons.Rules)
               return ExecListType.Rules;
            else if (EntryPoints[SelectedIndex].Items.MeaningID == (ulong)PredefinedNeurons.Actions)
               return ExecListType.Actions;
            else
               return ExecListType.Children;
         }
         set
         {
            ulong iToSearch;
            switch (value)
            {
               case ExecListType.Rules:
                  iToSearch = (ulong)PredefinedNeurons.Rules;
                  break;
               case ExecListType.Actions:
                  iToSearch = (ulong)PredefinedNeurons.Actions;
                  break;
               case ExecListType.Children:
                  iToSearch = (ulong)PredefinedNeurons.Statements;
                  break;
               default:
                  iToSearch = Neuron.EmptyId;
                  break;
            }
            if (iToSearch == Neuron.EmptyId)
            {
               for (int i = 0; i < EntryPoints.Count; i++)
               {
                  if (EntryPoints[i].Items.MeaningID == iToSearch)
                  {
                     SelectedIndex = i;
                     break;
                  }
               }
            }
         }
      } 
	#endregion

      #endregion

      #region functions

      /// <summary>
      /// Registers the item that was read from xml.
      /// </summary>
      /// <remarks>
      /// This must be called when the editor is read from xml.  In that situation, the
      /// brainData isn't always loaded properly yet.  At this point, this can be resolved.
      /// It is called by the brainData.
      /// </remarks>
      public override void RegisterItem()
      {
         base.RegisterItem();
         LoadEntryPoints();
      }

      /// <summary>
      /// Loads the CodeEditorPages for the current item.
      /// </summary>
      private void LoadEntryPoints()
      {
         fEntryPoints.Clear();
         if (Item != null)
         {
            NeuronCluster iCluster = Item.RulesCluster;
            if (iCluster == null)
               fEntryPoints.Add(new CodeEditorPage("Rules", Item, (ulong)PredefinedNeurons.Rules));
            else
               fEntryPoints.Add(new CodeEditorPage("Rules", Item, iCluster));

            iCluster = Item.ActionsCluster;
            if (iCluster == null)
               fEntryPoints.Add(new CodeEditorPage("Actions", Item, (ulong)PredefinedNeurons.Actions));
            else
               fEntryPoints.Add(new CodeEditorPage("Actions", Item, iCluster));

            if (Item is NeuronCluster)
               fEntryPoints.Add(new CodeEditorPage("Children", Item, (NeuronCluster)Item));
            else if (Item is ExpressionsBlock)
            {
               iCluster = ((ExpressionsBlock)Item).StatementsCluster;
               if (iCluster != null)
                  fEntryPoints.Add(new CodeEditorPage("Statements", Item, iCluster));
               else
                  fEntryPoints.Add(new CodeEditorPage("Statements", Item, (ulong)PredefinedNeurons.Statements));
            }
            else if (Item is TimerSin)
            {
               iCluster = ((TimerSin)Item).StatementsCluster;
               if (iCluster != null)
                  fEntryPoints.Add(new CodeEditorPage("Statements", Item, iCluster));
               else
                  fEntryPoints.Add(new CodeEditorPage("Statements", Item, (ulong)PredefinedNeurons.Statements));
            }
            if (Item is Sin)
            {
               iCluster = ((Sin)Item).ActionsForInput;
               if (iCluster != null)
                  fEntryPoints.Add(new CodeEditorPage("Input actions", Item, iCluster));
               else
                  fEntryPoints.Add(new CodeEditorPage("Input actions", Item, (ulong)PredefinedNeurons.ActionsForInput));
            }
         }
      }

      public static CodeItem CreateCodeItemFor(Neuron item)
      {
         if (item is ResultStatement)
            return new CodeItemResultStatement((ResultStatement)item);
         else if (item is Statement)
            return new CodeItemStatement((Statement)item);
         else if (item is ConditionalStatement)
            return new CodeItemConditionalStatement((ConditionalStatement)item);
         else if (item is ConditionalExpression)
            return new CodeItemConditionalExpression((ConditionalExpression)item);
         else if (item is Variable)
            return new CodeItemVariable((Variable)item);
         else if (item is SearchExpression)
            return new CodeItemSearchExpression((SearchExpression)item);
         else if (item is BoolExpression)
            return new CodeItemBoolExpression((BoolExpression)item);
         else if (item is Assignment)
            return new CodeItemAssignment((Assignment)item);
         else if (item is ExpressionsBlock)
            return new CodeItemCodeBlock((ExpressionsBlock)item);
         else if (item is ByRefExpression)
            return new CodeItemByRef((ByRefExpression)item);
         else
            return new CodeItemStatic(item);
         
      }

      /// <summary>
      /// Asks each page on this code editor to remove all their registered variables (toolbox items).
      /// </summary>
      public void ParkRegisteredVariables()
      {
         foreach (CodeEditorPage i in EntryPoints)
            i.ParkVariables();
      }

      public void UnParkRegisteredVariables()
      {
         foreach (CodeEditorPage i in EntryPoints)
            i.UnParkVariables();
      }

      #endregion



      #region IWeakEventListener Members



      protected override void Item_NeuronChanged(NeuronChangedEventArgs e)
      {
         base.Item_NeuronChanged(e);
         if (e.Action == BrainAction.Removed)
            BrainData.BrainData.Current.CodeEditors.Remove(this);
      }

      #endregion

   }
}
