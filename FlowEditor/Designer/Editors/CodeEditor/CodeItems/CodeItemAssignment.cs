﻿using System;
using System.Windows.Threading;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Editors.@base;
using JaStDev.HAB.Events;
using JaStDev.HAB.Expressions;

namespace JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems
{
   public class CodeItemAssignment: CodeItem
   {
      #region fields
      CodeItemResult fLeftPart;
      CodeItemResult fRightPart;
      #endregion


      #region ctor - dtor

      /// <summary>
      /// Initializes a new instance of the <see cref="CodeItemAssignment"/> class.
      /// </summary>
      /// <param name="toWrap">To wrap.</param>
      public CodeItemAssignment(Assignment toWrap): base(toWrap)
      {
         
      }

      #endregion

      #region prop

      #region NotHasLeftPart

      /// <summary>
      /// Gets the if the loopitem is present or not.
      /// </summary>
      public bool NotHasLeftPart
      {
         get { return ((Assignment)Item).LeftPart == null; }
      }

      #endregion

      #region LeftPart

      /// <summary>
      /// Gets/sets the left part of the boolean expression.
      /// </summary>
      public CodeItemResult LeftPart
      {
         get
         {
            return fLeftPart;
         }
         set
         {
            if (fLeftPart != value)
            {
               OnPropertyChanging("LeftPart", fLeftPart, value);
               InternalSetLeftPart(value);
               InternalChange = true;
               try
               {
                  if (value != null)
                     ((Assignment)Item).LeftPart = value.Item;
                  else
                     ((Assignment)Item).LeftPart = null;
               }
               finally
               {
                  InternalChange = false;
               }
            }
         }
      }

      /// <summary>
      /// only stores the new value, warns the undo system + the UI
      /// </summary>
      /// <param name="value">The new value.</param>
      private void InternalSetLeftPart(CodeItemResult value)
      {
         if (fLeftPart != null)
            UnRegisterChild(fLeftPart);
         fLeftPart = value;
         if (fLeftPart != null)
            RegisterChild(fLeftPart);
         OnPropertyChanged("LeftPart");
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(OnPropertyChanged), "NotHasLeftPart");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event.
      }

      #endregion


      #region NotHasRightPart

      /// <summary>
      /// Gets the if the loopitem is present or not.
      /// </summary>
      public bool NotHasRightPart
      {
         get { return ((Assignment)Item).RightPart == null; }
      }

      #endregion

      #region RightPart

      /// <summary>
      /// Gets/sets the right part of the bool expression
      /// </summary>
      public CodeItemResult RightPart
      {
         get
         {
            return fRightPart;
         }
         set
         {
            if (fRightPart != value)
            {
               OnPropertyChanging("RightPart", fRightPart, value);                        //don't put this in InternalSetRight cause than it would also be triggered when the change comes from within the system, which we don't want cause that generates to many undo data (creates duplicate items).
               InternalSetRightPart(value);
               InternalChange = true;
               try
               {
                  if (value != null)
                     ((Assignment)Item).RightPart = value.Item;
                  else
                     ((Assignment)Item).RightPart = null;
               }
               finally
               {
                  InternalChange = false;
               }
            }
         }
      }

      private void InternalSetRightPart(CodeItemResult value)
      {
         if (fRightPart != null)
            UnRegisterChild(fRightPart);
         fRightPart = value;
         if (fRightPart != null)
            RegisterChild(fRightPart);
         OnPropertyChanged("RightPart");
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<string>(OnPropertyChanged), "NotHasRightPart");  //we call async cause when called by the prop setter, the value has not yet been assigned to the neuron, which would cause an invalid value for this event.
      }



      #endregion

      
      #endregion


      #region Functions

      /// <summary>
      /// Called when the <see cref="CodeItem.Item"/> has changed.
      /// </summary>
      /// <param name="value">The value.</param>
      protected override void OnItemChanged(Neuron value)
      {
         base.OnItemChanged(value);
         Assignment iToWrap = value as Assignment;
         if (iToWrap != null)
         {
            Neuron iFound = iToWrap.LeftPart;
            if (iFound != null)
               InternalSetLeftPart((CodeItemResult)CodeEditor.CreateCodeItemFor(iFound));
            iFound = iToWrap.RightPart;
            if (iFound != null)
               InternalSetRightPart((CodeItemResult)CodeEditor.CreateCodeItemFor(iFound));
            
         }
      }

      /// <summary>
      /// descendents that need to update links that changed can do this through this function.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      protected override void InternalLinkChanged(Link sender, LinkChangedEventArgs e)
      {
         base.InternalLinkChanged(sender, e);
         if (sender.MeaningID == (ulong)PredefinedNeurons.LeftPart)
            if (e.Action == BrainAction.Removed)
               InternalSetLeftPart(null);
            else
               InternalSetLeftPart((CodeItemResult)CodeEditor.CreateCodeItemFor(Brain.Brain.Current[e.NewTo]));
         else if (sender.MeaningID == (ulong)PredefinedNeurons.RightPart)
            if (e.Action == BrainAction.Removed)
               InternalSetRightPart(null);
            else
               InternalSetRightPart((CodeItemResult)CodeEditor.CreateCodeItemFor(Brain.Brain.Current[e.NewTo]));
      }

      public override void RemoveChildFromCode(EditorItem child)
      {
         if (LeftPart == child)
            LeftPart = null;
         else if (RightPart == child)
            RightPart = null;
         else
            base.RemoveChildFromCode(child);
      }

      #endregion
   }
}
