﻿
using JaStDev.HAB.Designer.Editors.@base;

namespace JaStDev.HAB.Designer.Editors.CodeEditor
{
   /// <summary>
   /// Interaction logic for CtrlVariable.xaml
   /// </summary>
   public partial class CtrlVariable : CtrlEditorItem
   {
      public CtrlVariable()
      {
         InitializeComponent();
      }
   }
}
