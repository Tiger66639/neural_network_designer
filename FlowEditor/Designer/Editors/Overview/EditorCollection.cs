﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using JaStDev.HAB.Designer.Editors.@base;
using JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems;
using JaStDev.HAB.Designer.Editors.Flow.FlowItems;
using JaStDev.HAB.Designer.Editors.Frames;

namespace JaStDev.HAB.Designer.Editors.Overview
{
   /// <summary>
   /// A collection for <see cref="EditorBase"/> objects able to stream properly.
   /// </summary>
   /// <remarks>
   /// this is required cause EditorBase implements <see cref="System.Xml.Serialization.IXmlSerializable"/> which causes
   /// the serialization of the items to fail: always use base type as name of xml element instead of actual one.
   /// </remarks>
   public class EditorCollection : ObservedCollection<EditorBase>, IXmlSerializable
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="EditorCollection"/> class.
      /// </summary>
      public EditorCollection()
      {

      }

      /// <summary>
      /// Initializes a new instance of the <see cref="EditorCollection"/> class.
      /// </summary>
      /// <param name="owner">The owner.</param>
      public EditorCollection(object owner): base(owner)
      {

      }

      /// <summary>
      /// Removes the item recursivelly, that is, when the item is not in this list, all child editor folders
      /// are searched as well.
      /// </summary>
      /// <param name="toRemove">To remove.</param>
      public bool RemoveRecursive(EditorBase toRemove)
      {
         if (Remove(toRemove) == false)
         {
            var iFolders = from i in this where i is EditorFolder select (EditorFolder)i;
            foreach (EditorFolder i in iFolders)
            {
               if (i.Items.RemoveRecursive(toRemove) == true)
                  return true;
            }
            return false;
         }
         else
            return true;
      }

      /// <summary>
      /// Gets all the <see cref="NeuronEditor"/>s.
      /// </summary>
      /// <remarks>
      /// Use this to quickly access all the editors that derive from <see cref="NeuronEditor"/>.
      /// </remarks>
      /// <returns>All the neuron editors.</returns>
      public IEnumerable<NeuronEditor> AllNeuronEditors()
      {
         foreach (EditorBase i in this)
         {
            if (i is EditorFolder)
               ((EditorFolder)i).Items.AllNeuronEditors();
            else if (i is NeuronEditor)
               yield return (NeuronEditor)i;
            else if (i is FlowEditor)
            {
               foreach (Flow.FlowItems.Flow ii in ((FlowEditor)i).Flows)
                  yield return ii;
            }
         }
      }

      /// <summary>
      /// Gets all the <see cref="CodeEditor"/>s.
      /// </summary>
      /// <returns>All the code editors.</returns>
      public IEnumerable<CodeEditor.CodeItems.CodeEditor> AllCodeEditors()
      {
         foreach (EditorBase i in this)
         {
            if (i is EditorFolder)
               ((EditorFolder)i).Items.AllCodeEditors();
            else if (i is CodeEditor.CodeItems.CodeEditor)
               yield return (CodeEditor.CodeItems.CodeEditor)i;

         }
      }

      #region IXmlSerializable Members

      /// <summary>
      /// This method is reserved and should not be used. When implementing the IXmlSerializable interface, you should return null (Nothing in Visual Basic) from this method, and instead, if specifying a custom schema is required, apply the <see cref="T:System.Xml.Serialization.XmlSchemaProviderAttribute"/> to the class.
      /// </summary>
      /// <returns>
      /// An <see cref="T:System.Xml.Schema.XmlSchema"/> that describes the XML representation of the object that is produced by the <see cref="M:System.Xml.Serialization.IXmlSerializable.WriteXml(System.Xml.XmlWriter)"/> method and consumed by the <see cref="M:System.Xml.Serialization.IXmlSerializable.ReadXml(System.Xml.XmlReader)"/> method.
      /// </returns>
      public XmlSchema GetSchema()
      {
         return null;
      }

      public void ReadXml(XmlReader reader)
      {
         string iName = reader.Name;
         bool wasEmpty = reader.IsEmptyElement;

         reader.Read();
         if (wasEmpty) return;

         while (reader.Name != iName)
         {
            if (ReadXmlInternal(reader) == false)                                      //if for some reason, we failed to read the item, log an error, and advance to the next item so that we don't get in a loop.
            {
               Log.Log.LogError("EditorCollection.ReadXml", string.Format("Failed to read xml element {0} in stream.", reader.Name));
               reader.Skip();
            }
         }
         reader.ReadEndElement();
      }

      private bool ReadXmlInternal(XmlReader reader)
      {
         if (reader.Name == "MindMap")
         {
            XmlSerializer iNoteSer = new XmlSerializer(typeof(MindMap.MindMap));
            MindMap.MindMap iNode = (MindMap.MindMap)iNoteSer.Deserialize(reader);
            if (iNode != null)
               Add(iNode);
            return true;
         }
         else if (reader.Name == "FrameEditor")
         {
            XmlSerializer iLinkSer = new XmlSerializer(typeof(FrameEditor));
            FrameEditor iNode = (FrameEditor)iLinkSer.Deserialize(reader);
            if (iNode != null)
               Add(iNode);
            return true;
         }
         else if (reader.Name == "Flow")
         {
            XmlSerializer iNeuronSer = new XmlSerializer(typeof(Flow.FlowItems.Flow));
            Flow.FlowItems.Flow iNode = (Flow.FlowItems.Flow)iNeuronSer.Deserialize(reader);
            if (iNode != null)
               Add(iNode);
            return true;
         }
         else if (reader.Name == "CodeEditor")
         {
            XmlSerializer iLinkSer = new XmlSerializer(typeof(CodeEditor.CodeItems.CodeEditor));
            CodeEditor.CodeItems.CodeEditor iNode = (CodeEditor.CodeItems.CodeEditor)iLinkSer.Deserialize(reader);
            if (iNode != null)
               Add(iNode);
            return true;
         }
         else if (reader.Name == "FlowEditor")
         {
            XmlSerializer iLinkSer = new XmlSerializer(typeof(FlowEditor));
            FlowEditor iNode = (FlowEditor)iLinkSer.Deserialize(reader);
            if (iNode != null)
               Add(iNode);
            return true;
         }
         return false;
      }

      /// <summary>
      /// Converts an object into its XML representation.
      /// </summary>
      /// <param name="writer">The <see cref="T:System.Xml.XmlWriter"/> stream to which the object is serialized.</param>
      public void WriteXml(XmlWriter writer)
      {
         XmlSerializer iMindMapSer = new XmlSerializer(typeof(MindMap.MindMap));
         XmlSerializer iFrameSer = new XmlSerializer(typeof(FrameEditor));
         XmlSerializer iFlowSer = new XmlSerializer(typeof(Flow.FlowItems.Flow));
         XmlSerializer iFlowEdSer = new XmlSerializer(typeof(FlowEditor));
         XmlSerializer iCodeSer = new XmlSerializer(typeof(CodeEditor.CodeItems.CodeEditor));
         foreach (EditorBase item in this)
         {
            if (item is MindMap.MindMap)
               iMindMapSer.Serialize(writer, item);
            else if (item is FrameEditor)
               iFrameSer.Serialize(writer, item);
            else if (item is Flow.FlowItems.Flow)
               iFlowSer.Serialize(writer, item);
            else if (item is CodeEditor.CodeItems.CodeEditor)
               iCodeSer.Serialize(writer, item);
            else if (item is FlowEditor)
               iFlowEdSer.Serialize(writer, item);
            else
               throw new InvalidOperationException();
         }
      }

      #endregion
   }
}
