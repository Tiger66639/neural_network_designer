﻿using System;
using System.Xml;
using System.Xml.Serialization;
using JaStDev.HAB.Designer.Editors.@base;
using JaStDev.HAB.Designer.Editors.Frames;

namespace JaStDev.HAB.Designer.Editors.Overview
{
   /// <summary>
   /// An editor object that contains other editor objects, it functions as a folder.
   /// </summary>
   public class EditorFolder: EditorBase
   {
      #region fields
      EditorCollection fItems; 
      #endregion

      #region ctor
      /// <summary>
      /// Initializes a new instance of the <see cref="EditorFolder"/> class.
      /// </summary>
      public EditorFolder()
      {
         fItems = new EditorCollection(this);
      } 
      #endregion

      #region Prop

      #region Icon
      /// <summary>
      /// Gets the resource path to the icon that should be used for this editor.  This is usually class specific.
      /// </summary>
      /// <value></value>
      public override string Icon
      {
         get { return "/Images/NewFolder.png"; }
      } 
      #endregion


      #region IsSelected
      /// <summary>
      /// Gets/sets the wether the editor is currently selected in the overview list.
      /// </summary>
      /// <value></value>
      /// <remarks>
      /// This is provided so that a folder can make itself 'active' when selected.
      /// </remarks>
      public override bool IsSelected
      {
         get
         {
            return base.IsSelected;
         }
         set
         {
            base.IsSelected = value;
            if (value == true)
               BrainData.BrainData.Current.CurrentEditorsList = Items;
         }
      } 
      #endregion
      
      #region Items

      /// <summary>
      /// Gets the list of editor objects that are stored in this folder.
      /// </summary>
      public EditorCollection Items
      {
         get { return fItems; }
      }

      #endregion 

      /// <summary>
      /// Gets a title that the description editor can use to display in the header.
      /// </summary>
      /// <value></value>
      public override string DescriptionTitle
      {
         get
         {
            return Name + " - Folder";
         }
      }

      #endregion

      #region Functions

      /// <summary>
      /// Removes the editor from the specified list.
      /// </summary>
      /// <param name="list">The list.</param>
      /// <remarks>
      /// This is a virtual function so that some editors can have multiple open documents (like a folder).
      /// </remarks>
      public override void RemoveEditorFrom(System.Collections.IList list)
      {
         foreach (EditorBase i in Items)
            i.RemoveEditorFrom(list);
      }

      /// <summary>
      /// Reads the fields/properties of the class.
      /// </summary>
      /// <param name="reader">The reader.</param>
      /// <returns>
      /// True if the item was properly read, otherwise false.
      /// </returns>
      /// <remarks>
      /// This function is called for each element that is found, so this function should check which element it is
      /// and only read that element accordingly.
      /// </remarks>
      protected override bool ReadXmlInternal(XmlReader reader)
      {
         if (reader.Name == "MindMap")
         {
            XmlSerializer iNoteSer = new XmlSerializer(typeof(MindMap.MindMap));
            MindMap.MindMap iNode = (MindMap.MindMap)iNoteSer.Deserialize(reader);
            if (iNode != null)
               fItems.Add(iNode);
            return true;
         }
         else if (reader.Name == "FrameEditor")
         {
            XmlSerializer iLinkSer = new XmlSerializer(typeof(FrameEditor));
            FrameEditor iNode = (FrameEditor)iLinkSer.Deserialize(reader);
            if (iNode != null)
               fItems.Add(iNode);
            return true;
         }
         else if (reader.Name == "Flow")
         {
            XmlSerializer iNeuronSer = new XmlSerializer(typeof(Flow.FlowItems.Flow));
            Flow.FlowItems.Flow iNode = (Flow.FlowItems.Flow)iNeuronSer.Deserialize(reader);
            if (iNode != null)
               fItems.Add(iNode);
            return true;
         }
         else if (reader.Name == "EditorFolder")
         {
            XmlSerializer iClusterSer = new XmlSerializer(typeof(EditorFolder));
            EditorFolder iNode = (EditorFolder)iClusterSer.Deserialize(reader);
            if (iNode != null)
               fItems.Add(iNode);
            return true;
         }
         else
            return base.ReadXmlInternal(reader);
      }

      /// <summary>
      /// Converts an object into its XML representation.
      /// </summary>
      /// <param name="writer">The <see cref="T:System.Xml.XmlWriter"/> stream to which the object is serialized.</param>
      public override void WriteXml(XmlWriter writer)
      {
         base.WriteXml(writer);

         XmlSerializer iMindMapSer = new XmlSerializer(typeof(MindMap.MindMap));
         XmlSerializer iFrameEditorSer = new XmlSerializer(typeof(FrameEditor));
         XmlSerializer iFlowSer = new XmlSerializer(typeof(Flow.FlowItems.Flow));
         XmlSerializer iMapSer = new XmlSerializer(typeof(EditorFolder));
         foreach (EditorBase item in Items)
         {
            if (item is MindMap.MindMap)
               iMindMapSer.Serialize(writer, item);
            else if (item is FrameEditor)
               iFrameEditorSer.Serialize(writer, item);
            else if (item is Flow.FlowItems.Flow)
               iFlowSer.Serialize(writer, item);
            else if (item is EditorFolder)
               iFrameEditorSer.Serialize(writer, item);
            else
               throw new InvalidOperationException("Unkown editor type: don't know how to store.");
         }
      }

      #endregion

      
   }
}
