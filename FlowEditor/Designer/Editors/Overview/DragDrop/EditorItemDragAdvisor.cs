﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using JaStDev.HAB.Designer.DragDrop;
using JaStDev.HAB.Designer.Editors.@base;

namespace JaStDev.HAB.Designer.Editors.Overview.DragDrop
{
   public class EditorItemDragAdvisor : DragSourceBase
   {
      public EditorItemDragAdvisor()
      {
         SupportedFormat = Properties.Resources.EDITORFORMAT;                                                        //this is not really used, simply informtive: this is our main data type.
      }

      #region UsePreviewEvents
      /// <summary>
      /// Gets if the preview event versions should be used or not.
      /// </summary>
      /// <remarks>
      /// don't use preview events cause than the sub drop points don't get used but only the main list cause this gets the events first,
      /// while we usually want to drop in a sub drop point.
      /// </remarks>
      public override bool UsePreviewEvents
      {
         get
         {
            return false;
         }
      }
      #endregion

      

      #region Item

      /// <summary>
      /// Gets the code item that is the datacontext of the target of this drop advisor.
      /// </summary>
      public EditorBase Item
      {
         get
         {
            return ((FrameworkElement)SourceUI).DataContext as EditorBase;
         }
      }

      #endregion

      #region CodeList

      /// <summary>
      /// Gets the list containing all the code that the UI to which advisor is attached too, displays data for.
      /// </summary>
      public EditorCollection Collection
      {
         get
         {
            TreeViewItem iItem = TreeHelper.FindInTree<TreeViewItem>(SourceUI);
            ItemsControl iItemsControl = ItemsControl.ItemsControlFromItemContainer(iItem);
            Debug.Assert(iItemsControl != null);
            return iItemsControl.ItemsSource as EditorCollection;
         }
      }

      #endregion

      public override void FinishDrag(UIElement draggedElt, DragDropEffects finalEffects)
      {
         if ((finalEffects & DragDropEffects.Move) == DragDropEffects.Move)
         {
            EditorBase iItem = Item;
            EditorCollection iList = Collection;
            Debug.Assert(iList != null);
            iList.Remove(Item);
         }
      }

      /// <summary>
      /// We can drag when there is a content in the presenter.
      /// </summary>
      public override bool IsDraggable(UIElement dragElt)
      {
         return Item != null && Collection != null;
      }

      /// <summary>
      /// we override cause we put the image to use + an ulong if it is a neuron, or a ref to the mind map item.
      /// If the item is a link, we also store which side of the link it was, so we can adjust it again (+ update it).
      /// </summary>
      public override DataObject GetDataObject(UIElement draggedElt)
      {
         FrameworkElement iDragged = (FrameworkElement)draggedElt;
         DataObject iObj = new DataObject();

         EditorBase iContent = Item;

         iObj.SetData(Properties.Resources.UIElementFormat, iDragged);
         iObj.SetData(Properties.Resources.EDITORFORMAT, iContent);

         return iObj;

      }
   }
}
