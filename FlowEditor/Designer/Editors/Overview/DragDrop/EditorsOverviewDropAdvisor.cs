﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using JaStDev.HAB.Designer.DragDrop;
using JaStDev.HAB.Designer.Editors.@base;

namespace JaStDev.HAB.Designer.Editors.Overview.DragDrop
{
   public class EditorsOverviewDropAdvisor : DropTargetBase
   {
      #region prop
      #region Target

      public ItemsControl Target
      {
         get
         {
            return (ItemsControl)TargetUI;
         }
      }

      #endregion

      #region UsePreviewEvents
      /// <summary>
      /// Gets if the preview event versions should be used or not.
      /// </summary>
      /// <remarks>
      /// don't use preview events cause than the sub lists don't get used but only the main list cause this gets the events first,
      /// while we usually want to drop in a sublist.
      /// </remarks>
      public override bool UsePreviewEvents
      {
         get
         {
            return false;
         }
      }
      #endregion

      #region CodeList

      /// <summary>
      /// Gets the list containing all the code that the UI to which advisor is attached too, displays data for.
      /// </summary>
      public EditorCollection Items
      {
         get
         {
            return Target.ItemsSource as EditorCollection;
         }
      }

      #endregion
      #endregion

      #region Overrides
      public override void OnDropCompleted(DragEventArgs arg, Point dropPoint)
      {
         EditorBase iItem = arg.Data.GetData(Properties.Resources.EDITORFORMAT) as EditorBase;
         if (iItem != null && Items.Contains(iItem) == true)
            TryMoveItem(arg, dropPoint, iItem);
         else
            TryCreateNewItem(arg, dropPoint);                                                                             //we are not moving around an item, so add new code item.
      }


      public override bool IsValidDataObject(IDataObject obj)
      {
         return obj.GetDataPresent(Properties.Resources.EDITORFORMAT)
                || obj.GetDataPresent(Properties.Resources.NeuronIDFormat);
      }


      #endregion

      /// <summary>
      /// Tries the move item.
      /// </summary>
      /// <param name="arg">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
      /// <param name="dropPoint">The drop point.</param>
      /// <param name="iItem">The i item.</param>
      private void TryMoveItem(DragEventArgs arg, Point dropPoint, EditorBase iItem)
      {
         EditorCollection iList = Items;
         int iIndex = iList.IndexOf(iItem);
         if (iIndex == -1)                                    //we need to create a new item if the item being moved wasn't in this list or when a copy was requested because shift was pressed.
         {
            iItem.RemoveFromOwner();
            if (iIndex > -1)
               iList.Insert(iIndex, iItem);
            else
               iList.Add(iItem);
         }
         else
            iList.Move(iIndex, iList.Count - 1);
         arg.Effects = DragDropEffects.None;
      }

      private void TryCreateNewItem(DragEventArgs obj, Point dropPoint)
      {
         ulong iId = (ulong)obj.Data.GetData(Properties.Resources.NeuronIDFormat);

         CodeEditor.CodeItems.CodeEditor iNew = new CodeEditor.CodeItems.CodeEditor(Brain.Brain.Current[iId]);
         Debug.Assert(iNew != null);
         EditorCollection iList = Items;
         Debug.Assert(iList != null);
         iList.Add(iNew);
      }
   }
}
