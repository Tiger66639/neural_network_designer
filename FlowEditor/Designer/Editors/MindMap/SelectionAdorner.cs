﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Shapes;

namespace JaStDev.HAB.Designer.Editors.MindMap
{
   /// <summary>
   /// Used to select a set of neurons on the mind map.
   /// </summary>
   public class SelectionAdorner: Adorner
   {
      #region fields
      Rectangle fRect;
      Point fStartPoint;

      #endregion


      public SelectionAdorner(Point startPoint, UIElement adornedElt)
         : base(adornedElt)
      {
         fStartPoint = startPoint;
         fEndPoint = startPoint;
         fRect = new Rectangle();
         fRect.Fill = new SolidColorBrush(Colors.AliceBlue);
         fRect.Opacity = 0.4;
         fRect.Stroke = new SolidColorBrush(Colors.SteelBlue);
      }


      Point fEndPoint;
      #region EndPoint

      /// <summary>
      /// Gets/sets the end point of the selection, can be smaller than startpoint for inverse selection. 
      /// </summary>
      public Point EndPoint
      {
         get
         {
            return fEndPoint;
         }
         set
         {
            fEndPoint = value;
            AdornerLayer layer = this.Parent as AdornerLayer;
            if (layer != null)
               layer.Update(AdornedElement);
         }
      }

      #endregion

      #region StartPoint

      /// <summary>
      /// Gets the startpoint
      /// </summary>
      public Point StartPoint
      {
         get { return fStartPoint; }
      }

      #endregion
       

      protected override Size MeasureOverride(Size constraint)
      {
         Size iSize =new Size();
         if (fStartPoint.X < fEndPoint.X)
            iSize.Width = fEndPoint.X - fStartPoint.X;
         else
            iSize.Width = fStartPoint.X - fEndPoint.X;

         if (fStartPoint.Y < fEndPoint.Y)
            iSize.Height = fEndPoint.Y - fStartPoint.Y;
         else
            iSize.Height = fStartPoint.Y - fEndPoint.Y;

         fRect.Measure(iSize);
         return fRect.DesiredSize;
      }

      protected override Size ArrangeOverride(Size finalSize)
      {

         Rect iRect = GetSelectArea();
         fRect.Arrange(iRect);
         return iRect.Size;
      }

      protected override Visual GetVisualChild(int index)
      {
         return fRect;
      }

      protected override int VisualChildrenCount
      {
         get
         {
            return 1;
         }
      }


      internal Rect GetSelectArea()
      {
         Size iSize = new Size();
         Point iStart = new Point();
         if (fStartPoint.X < fEndPoint.X)
         {
            iStart.X = fStartPoint.X;
            iSize.Width = fEndPoint.X - fStartPoint.X;
         }
         else
         {
            iStart.X = fEndPoint.X;
            iSize.Width = fStartPoint.X - fEndPoint.X;
         }

         if (fStartPoint.Y < fEndPoint.Y)
         {
            iStart.Y = fStartPoint.Y;
            iSize.Height = fEndPoint.Y - fStartPoint.Y;
         }
         else
         {
            iStart.Y = fEndPoint.Y;
            iSize.Height = fStartPoint.Y - fEndPoint.Y;
         }
         return new Rect(iStart, iSize);
      }
   }
}
