﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace JaStDev.HAB.Designer.Editors.MindMap
{
   /// <summary>
   /// Interaction logic for MindMapNoteView.xaml
   /// </summary>
   public partial class MindMapNoteView : UserControl
   {
      bool fInternalChange;

      public MindMapNoteView()
      {
         InitializeComponent();
      }

      /// <summary>
      /// Used to assign the flowdocument in the data context to the richtextbox editor, it doesn't allow binding.
      /// </summary>
      private void NoteEditorLoaded(object sender, RoutedEventArgs e)
      {
         RichTextBox iSender = (RichTextBox)sender;
         iSender.Document = ((MindMapNote)DataContext).Description;
      }

      /// <summary>
      /// Handles the LostFocus event of the NoteEditor control.
      /// </summary>
      /// <remarks>
      /// need to save the value back to the data object.  That is because it always returns a new item and keeps no
      /// ref to the flowdocs it returns.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void NoteEditor_LostFocus(object sender, RoutedEventArgs e)
      {
         fInternalChange = true;                                                       //need to prevent that MindMapNote_PropertyChanged gets called when we change the value, this causes a loop, which we don't want.
         try
         {
            RichTextBox iSender = (RichTextBox)sender;
            ((MindMapNote)DataContext).Description = iSender.Document;
         }
         finally
         {
            fInternalChange = false;
         }
      }

      private void UserControl_Loaded(object sender, RoutedEventArgs e)
      {
         MindMapNote iDataContext = DataContext as MindMapNote;
         iDataContext.PropertyChanged += new PropertyChangedEventHandler(MindMapNote_PropertyChanged);
      }

      private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
      {
         MindMapNote iDataContext = DataContext as MindMapNote;
         if (e.OldValue != null)
            ((MindMapNote)e.OldValue).PropertyChanged -= new PropertyChangedEventHandler(MindMapNote_PropertyChanged);
         if (e.NewValue != null)
            ((MindMapNote)e.NewValue).PropertyChanged += new PropertyChangedEventHandler(MindMapNote_PropertyChanged);
      }

      /// <summary>
      /// Handles the PropertyChanged event of the MindMap control.
      /// </summary>
      /// <remarks>
      /// If the description is changed, we need to manually assign it back to the editor.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
      void MindMapNote_PropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         if (fInternalChange == false)
            RtfEditor.Document = ((MindMapNote)DataContext).Description;
      }
   }
}
