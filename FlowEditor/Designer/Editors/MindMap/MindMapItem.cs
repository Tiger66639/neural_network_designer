﻿using System;
using System.IO;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Xml;
using System.Xml.Serialization;
using JaStDev.HAB.Designer.Tools.Description;

namespace JaStDev.HAB.Designer.Editors.MindMap
{
   /// <summary>
   /// An item that can be displayed on a <see cref="MindMap"/>.
   /// </summary>
   /// <remarks>
   /// Ineritors should reimplement the 'Duplicate' function.
   /// Could be that this class needs to be streamed manually with xmlwriter.WriteRaw for flowDocument.
   /// <para>
   /// This is an owned object, with <see cref="MindMap"/> as the owner, so that it can update certain
   /// properties of the owner in response to property changes here (like the width, height, or link line points).
   /// </para>
   /// </remarks>
   [XmlInclude(typeof(MindMapLink))]
   [XmlInclude(typeof(MindMapNeuron))]
   [XmlInclude(typeof(MindMapNote))]
   [XmlInclude(typeof(MindMapCluster))]
   public class MindMapItem: OwnedObject<MindMap>, IDescriptionable
   {
      #region Fields

      string fDescription;
      int fZIndex = 0;
      Adorner fAdorner;

      #endregion


      #region Prop

      
      #region Adorner

      /// <summary>
      /// Gets/sets the Adorner used by the mindmap item.
      /// </summary>
      /// <remarks>
      /// This property is added because we need a secure location to store the adorner for each item in such
      /// a way we can always get to it, even if the listbox item that represents the mindmap item is already
      /// removed.
      /// </remarks>
      [XmlIgnore]
      public Adorner Adorner
      {
         get
         {
            return fAdorner;
         }
         set
         {
            fAdorner = value;
         }
      }

      #endregion

      #region ZIndex

      /// <summary>
      /// Gets/sets the Z-index of this item.
      /// </summary>
      /// <remarks>
      /// <para>
      /// This value determins the order in which they are drawn on the canvas.  The
      /// bigger this value, the more likely the item will appear at the top.
      /// </para>
      /// <para>
      /// By default, this is 0.
      /// </para>
      /// </remarks>
      public int ZIndex
      {
         get
         {
            return fZIndex;
         }
         set
         {
            OnPropertyChanging("ZIndex", fZIndex, value);
            fZIndex = value;
            OnPropertyChanged("ZIndex");
         }
      }

      #endregion

      #region Description

      /// <summary>
      /// Gets the description for this object.
      /// </summary>
      /// <remarks>
      /// <para>
      /// Always returns a disconnected item. When the return value is modified, it is not saved back in the mind map
      /// item, so you need to reassign it.  This is crap, I know, it's because flowDocuments take up to much mem, so
      /// need to save as string.
      /// </para>
      /// <para>
      /// this is virtual so that the <see cref="MindMapNeuron"/> can override this.
      /// </para>
      /// </remarks>
      [XmlIgnore]
      public virtual FlowDocument Description
      {
         get
         {
            if (fDescription != null)
            {
               StringReader stringReader = new StringReader(fDescription);
               XmlReader xmlReader = XmlReader.Create(stringReader);
               return XamlReader.Load(xmlReader) as FlowDocument;
            }
            else
               return Helper.CreateDefaultFlowDoc();
         }
         set
         {
            string iVal = XamlWriter.Save(value);
            if (fDescription != iVal)
            {
               fDescription = iVal;
               OnPropertyChanged("Description");
            }
         }
      }

      #endregion

      #region IDescriptionable Members

      public virtual string DescriptionTitle
      {
         get { return "Mindmap item"; }
      }

      #endregion

      /// <summary>
      /// this is used to determin a custom ordening for saving the items in the list.  This is used to make certain that
      /// link objects are at the back of the list (so that they can load properly).
      /// </summary>
      /// <value>The index of the type.</value>
      internal protected virtual int TypeIndex
      {
         get
         {
            return 1;
         }
      }

      #endregion

      #region functions

      /// <summary>
      /// creates a deep copy of this object.
      /// </summary>
      /// <remarks>
      /// Currently primarely used for dragging.
      /// </remarks>
      /// <returns>A deep copy of this object.</returns>
      public virtual MindMapItem Duplicate()
      {
         MindMapItem iRes = Activator.CreateInstance(GetType()) as MindMapItem;
         iRes.fDescription = fDescription;
         return iRes;
      }

      #endregion      
   }
}
