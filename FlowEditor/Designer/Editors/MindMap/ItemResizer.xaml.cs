﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Media;

namespace JaStDev.HAB.Designer.Editors.MindMap
{

   /// <summary>
   /// Interaction logic for CanvasResizer.xaml
   /// </summary>
   public partial class ItemResizer : UserControl
   {

      /// <summary>
      /// Initializes a new instance of the <see cref="ItemResizer"/> class.
      /// </summary>
      public ItemResizer()
      {
         InitializeComponent();
      }

      

      #region ToControl

      /// <summary>
      /// ToControl Dependency Property
      /// </summary>
      public static readonly DependencyProperty ToControlProperty =
          DependencyProperty.Register("ToControl", typeof(PositionedMindMapItem), typeof(ItemResizer),
              new FrameworkPropertyMetadata((PositionedMindMapItem)null));

      /// <summary>
      /// Gets or sets the ToControl property.  This dependency property 
      /// indicates the object that will be resized by this resizer object.
      /// </summary>
      public PositionedMindMapItem ToControl
      {
         get { return (PositionedMindMapItem)GetValue(ToControlProperty); }
         set { SetValue(ToControlProperty, value); }
      }

      #endregion



      /// <summary>
      /// LTs the dragged.
      /// </summary>
      /// <param name="sender">The sender.</param>
      /// <param name="e">The <see cref="System.Windows.Controls.Primitives.DragDeltaEventArgs"/> instance containing the event data.</param>
      private void LTDragged(object sender, DragDeltaEventArgs e)
      {
         PositionedMindMapItem iToControl = ToControl;
         if (iToControl != null)
         {
            double iPrevVal = iToControl.X;                                                     //when we set iToControl.X, it might cut the value to what is allowed, if this happens, we need to compensate for this when we adjust the width.
            iToControl.X += e.HorizontalChange;
            iToControl.Width -= (iToControl.X - iPrevVal);

            iPrevVal = iToControl.Y;
            iToControl.Y += e.VerticalChange;
            iToControl.Height -= (iToControl.Y - iPrevVal);
         }
      }

      private void TDragged(object sender, DragDeltaEventArgs e)
      {
         PositionedMindMapItem iToControl = ToControl;
         if (iToControl != null)
         {
            double iPrevVal = iToControl.Y;
            iToControl.Y += e.VerticalChange;
            iToControl.Height -= (iToControl.Y - iPrevVal);
         }
      }

      private void RTDragged(object sender, DragDeltaEventArgs e)
      {
         PositionedMindMapItem iToControl = ToControl;
         if (iToControl != null)
         {
            double iPrevVal = iToControl.Y;
            iToControl.Y += e.VerticalChange;
            iToControl.Width += e.HorizontalChange;
            iToControl.Height -= (iToControl.Y - iPrevVal);
         }
      }

      private void LDragged(object sender, DragDeltaEventArgs e)
      {
         PositionedMindMapItem iToControl = ToControl;
         if (iToControl != null)
         {
            double iPrevVal = iToControl.X;                                                     //when we set iToControl.X, it might cut the value to what is allowed, if this happens, we need to compensate for this when we adjust the width.
            iToControl.X += e.HorizontalChange;
            iToControl.Width -= (iToControl.X - iPrevVal);
         }
      }

      private void RDragged(object sender, DragDeltaEventArgs e)
      {
         PositionedMindMapItem iToControl = ToControl;
         if (iToControl != null)
         {
            iToControl.Width += e.HorizontalChange;
         }
      }

      private void LBDragged(object sender, DragDeltaEventArgs e)
      {
         PositionedMindMapItem iToControl = ToControl;
         if (iToControl != null)
         {
            double iPrevVal = iToControl.X;                                                     //when we set iToControl.X, it might cut the value to what is allowed, if this happens, we need to compensate for this when we adjust the width.
            iToControl.X += e.HorizontalChange;
            iToControl.Width -= (iToControl.X - iPrevVal);
            iToControl.Height += e.VerticalChange;
         }
      }

      private void BDragged(object sender, DragDeltaEventArgs e)
      {
         PositionedMindMapItem iToControl = ToControl;
         if (iToControl != null)
         {
            iToControl.Height += e.VerticalChange;
         }
      }

      private void RBDragged(object sender, DragDeltaEventArgs e)
      {
         PositionedMindMapItem iToControl = ToControl;
         if (iToControl != null)
         {
            iToControl.Width += e.HorizontalChange;
            iToControl.Height += e.VerticalChange;
         }
      }

      /// <summary>
      /// Handles the DragStarted event of the Thumb control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Controls.Primitives.DragStartedEventArgs"/> instance containing the event data.</param>
      private void Thumb_DragStarted(object sender, DragStartedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup(true);                                                                 //we put a drag in an undo group so cause a drag is a single maneuvre.
      }

      /// <summary>
      /// Handles the DragCompleted event of the Thumb control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Controls.Primitives.DragCompletedEventArgs"/> instance containing the event data.</param>
      private void Thumb_DragCompleted(object sender, DragCompletedEventArgs e)
      {
         WindowMain.UndoStore.EndUndoGroup();
      }
   }

   public class ItemResizeAdorner : Adorner
   {

      ItemResizer fContent;
      VisualCollection visualChildren;


      public ItemResizeAdorner(PositionedMindMapItem toControl, UIElement adornedElt)
         : base(adornedElt)
      {
         visualChildren = new VisualCollection(this);

         fContent = new ItemResizer();
         fContent.ToControl = toControl;
         visualChildren.Add(fContent);
      }

      protected override Size MeasureOverride(Size constraint)
      {
         fContent.Measure(constraint);
         return fContent.DesiredSize;
      }

      protected override Size ArrangeOverride(Size finalSize)
      {
         fContent.Arrange(new Rect(finalSize));
         return finalSize;
      }

      protected override Visual GetVisualChild(int index)
      {
         return fContent;
      }

      protected override int VisualChildrenCount
      {
         get
         {
            return 1;
         }
      }

   }
}
