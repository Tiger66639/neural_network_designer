﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using JaStDev.HAB.Designer.DragDrop;

namespace JaStDev.HAB.Designer.Editors.MindMap
{
   public class MindMapDropTargetAdvisor: DropTargetBase
   {
      MindMapNeuron fFocusedNeuron;                                                                       //stores a reference to the item that we want to drop on for changing a link.

      public override void OnDropCompleted(DragEventArgs obj, Point dropPoint)
      {
         MindMapItem iItem = obj.Data.GetData(Properties.Resources.MindMapItemFormat) as MindMapItem;
         if (iItem == null)                                                                              //we are not moving around an item, so check if there is a neuron for which we can creat a new mindmapneuron.
         {
            List<PositionedMindMapItem> iList = obj.Data.GetData(Properties.Resources.MultiMindMapItemFormat) as List<PositionedMindMapItem>;
            if (iList == null)
               TryCreateNewNeuron(obj.Data, dropPoint);
            else
               TryMoveItems(obj.Data, dropPoint, iList);
         }
         else
            TryMoveItem(obj.Data, dropPoint, iItem);
      }

      /// <summary>
      /// Tries the move all the selected items. If the items
      /// are not part of the mind map (from another one), they are first duplicated.
      /// Please note: you can't add duplicate neurons.
      /// </summary>
      /// <param name="obj">The obj.</param>
      /// <param name="dropPoint">The drop point.</param>
      /// <param name="list">The list.</param>
      private void TryMoveItems(IDataObject obj, Point dropPoint, List<PositionedMindMapItem> list)
      {
         try
         {
            Point iNewPoint;
            Point iFirst = new Point(list[0].X, list[0].Y);                                              //we have to store the pos before the item is moved, otherwise we work with wrong values.
            foreach (PositionedMindMapItem i in list)
            {
               iNewPoint = new Point(dropPoint.X + (i.X - iFirst.X), dropPoint.Y + (i.Y - iFirst.Y));
               TryMoveItem(obj, iNewPoint, i);
            }
         }
         catch (Exception e)
         {
            string iMsg = e.ToString();
            Log.Log.LogError("MindMapDropTargetAdvisor.TryMoveItems", iMsg);
            MessageBox.Show("drag drop failed", string.Format("drag drop operation can't be completed because: {0}.", iMsg), MessageBoxButton.OK, MessageBoxImage.Error);

         }
      }

      /// <summary>
      /// Tries to move the specified item around on the mind map. If the item
      /// is not part of the mind map (from another one), it is first duplicated.
      /// Please note: you can't add duplicate neurons.
      /// </summary>
      /// <param name="dropPoint"></param>
      /// <param name="iItem"></param>
      private void TryMoveItem(IDataObject obj, Point dropPoint, MindMapItem item)
      {
         try
         {
            MindMap iMap = ((FrameworkElement)TargetUI).DataContext as MindMap;
            Debug.Assert(iMap != null);
            if (iMap.Items.Contains(item) == false)                                                   //we are not moving an item on the same canvas, so make a duplicate.
            {
               item = item.Duplicate();
               iMap.Items.Add(item);
            }
            PositionedMindMapItem iPosItem = (PositionedMindMapItem)item;
            if (iPosItem != null)                                                                        //its a note or neuron, so adjust it's pos.
               iPosItem.SetPositionFromDrag(dropPoint.X, dropPoint.Y);
            else
            {
               MindMapLink iLink = (MindMapLink)item;
               if (iLink != null)
               {
                  if (fFocusedNeuron != null)
                  {
                     string iSide = obj.GetData(Properties.Resources.MindMapLinkSide) as string;
                     if (iSide != null)
                     {
                        if (iSide == "start")
                           iLink.FromMindMapItem = fFocusedNeuron;
                        else
                           iLink.ToMindMapItem = fFocusedNeuron;
                     }
                     else
                        MessageBox.Show("Invalid side of link stored in drag operation!");
                  }
                  else
                     MessageBox.Show("No item focused to connect the link with!");
               }
               else
                  MessageBox.Show("Unrecognized mindmap item!");
            }
         }
         catch (Exception e)
         {
            string iMsg = e.ToString();
            Log.Log.LogError("MindMapDropTargetAdvisor.TryMoveItem", iMsg);
            MessageBox.Show("drag drop failed", string.Format("drag drop operation can't be completed because: {0}.", iMsg), MessageBoxButton.OK, MessageBoxImage.Error );

         }
      }

      /// <summary>
      /// Tries to create a new mindmap neuron based on the neuron found in the data.
      /// </summary>
      /// <param name="obj"></param>
      /// <param name="dropPoint"></param>
      private void TryCreateNewNeuron(IDataObject obj, Point dropPoint)
      {
         MindMap iMap = ((FrameworkElement)TargetUI).DataContext as MindMap;
         Debug.Assert(iMap != null);

         ulong iId = (ulong)obj.GetData(Properties.Resources.NeuronIDFormat);
         MindMapNeuron iNew = MindMapNeuron.CreateFor(Brain.Brain.Current[iId]);
         iNew.X = dropPoint.X;
         iNew.Y = dropPoint.Y;
         iNew.Width = 100;
         iNew.Height = 50;
         iMap.Items.Add(iNew);
      }

      public override bool IsValidDataObject(IDataObject obj)
      {
         return obj.GetDataPresent(Properties.Resources.MindMapItemFormat)
                || obj.GetDataPresent(Properties.Resources.MultiMindMapItemFormat)
                || obj.GetDataPresent(Properties.Resources.NeuronIDFormat);
      }

   }
}
