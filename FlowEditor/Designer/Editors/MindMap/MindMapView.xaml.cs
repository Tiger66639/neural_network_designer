﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Threading;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using JaStDev.HAB.Designer.Undo_data;
using JaStDev.HAB.Events;

namespace JaStDev.HAB.Designer.Editors.MindMap
{
   /// <summary>
   /// A view for a <see cref="MindMap"/> object.  The object is expected to be passed as data context.
   /// </summary>
   /// <remarks>
   /// Only supports multi selection for neurons not links.
   /// </remarks>
   public partial class MindMapView : UserControl
   {
      #region Fields

      Point fMousePos;
      SelectionAdorner fSelectionBox;
      double fHorScrollOffset;
      double fVerScrollOffset;

      #endregion

      #region Commands

      /// <summary>
      /// Command to add the selected item(s) to a cluster
      /// </summary>
      public static RoutedCommand AddToClusterCmd = new RoutedCommand();

      /// <summary>
      /// Command to remove the selected item(s) from a cluster it belongs to.
      /// </summary>
      public static RoutedCommand RemoveFromClusterCmd = new RoutedCommand();

      /// <summary>
      /// Command creates a new cluster and adds all the selected items to it.
      /// </summary>
      public static RoutedCommand MakeClusterCmd = new RoutedCommand();

      /// <summary>
      /// Command to select all the visible links for a neuron on a mindmap view.
      /// </summary>
      public static RoutedCommand SelectLinksCmd = new RoutedCommand();
      /// <summary>
      /// Command to show all the incomming links
      /// </summary>
      public static RoutedCommand ShowIncommingLinksCmd = new RoutedCommand();
      /// <summary>
      /// Command to show all the outgoing links.
      /// </summary>
      public static RoutedCommand ShowOutgoingLinksCmd = new RoutedCommand();
      /// <summary>
      /// Command to show all the children of a cluster.
      /// </summary>
      public static RoutedCommand ShowChildrenCmd = new RoutedCommand();
      /// <summary>
      /// Command to show all the clusters a neuron belongs to.
      /// </summary>
      public static RoutedCommand ShowClustersCmd = new RoutedCommand();

      #endregion

      #region ctor
      public MindMapView()
      {
         InitializeComponent();
      } 
      #endregion

      #region CanvasHeight

      /// <summary>
      /// CanvasHeight Dependency Property
      /// </summary>
      public static readonly DependencyProperty CanvasHeightProperty =
          DependencyProperty.Register("CanvasHeight", typeof(double), typeof(MindMapView),
              new FrameworkPropertyMetadata((double)0.0));

      /// <summary>
      /// Gets or sets the CanvasHeight property.  This dependency property 
      /// indicates the height that should be used by the canvas.
      /// </summary>
      public double CanvasHeight
      {
         get { return (double)GetValue(CanvasHeightProperty); }
         set { SetValue(CanvasHeightProperty, value); }
      }

      #endregion


      #region Event handlers

      #region NeuronShape
      /// <summary>
      /// called whenever the shape that represents the neuron is loaded, assigns it to the mindmap neuron object
      /// so it knows it's border for drawing lines.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void NeuronShape_Loaded(object sender, RoutedEventArgs e)
      {
         Shape iSender = (Shape)sender;
         MindMapNeuron iNeuron = (MindMapNeuron)iSender.DataContext;
         iNeuron.Shape = iSender;
      }

      /// <summary>
      /// Handles the Unloaded event of the NeuronShape control.
      /// </summary>
      /// <remarks>
      /// Need to remove the handle to the shape, otherwise, the item might stay in mem.
      /// -> this has been found to be true with a profiler.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void NeuronShape_Unloaded(object sender, RoutedEventArgs e)
      {
         Shape iSender = (Shape)sender;
         MindMapNeuron iNeuron = (MindMapNeuron)iSender.DataContext;
         iNeuron.Shape = null;
      } 
      #endregion


      #region UserControl
      /// <summary>
      /// When the control is loaded, we must check if the width and height of the mindmap aren't smaller than the working
      /// area, if this is so, we need to make it as big as the work area.
      /// Also need to monitor the changes in the data context with respect to the description (non bindable prop).
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void UserControl_Loaded(object sender, RoutedEventArgs e)
      {
         MindMap iDataContext = DataContext as MindMap;
         if (iDataContext != null)
            AdjustMapToVisual(iDataContext);
      }


      /// <summary>
      /// Handles the DataContextChanged event of the UserControl control.
      /// </summary>
      /// <remarks>
      /// If the control is still loaded but gets a new data context, we also need to update the size of the map.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
      private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
      {
         if (IsLoaded == true && e.NewValue is MindMap)                                                 //if it is not yet loaded, can't do much.
            AdjustMapToVisual((MindMap)e.NewValue);
      } 
      #endregion

      /// <summary>
      /// Adjusts the MindMap to the visual representation (for size and others).
      /// </summary>
      /// <param name="iMap">The map.</param>
      private void AdjustMapToVisual(MindMap map)
      {
         if (map != null)
         {
            if (map.Width < LstItems.ActualWidth * 4)
               map.Width = LstItems.ActualWidth * 4;
            if (map.Height < LstItems.ActualHeight * 4)
               map.Height = LstItems.ActualHeight * 4;
         }
      }

      #region Item selection box
      /// <summary>
      /// Draw/remove selection - resize boxes when the selection changes. 
      /// </summary>
      /// <remarks>
      /// If there are one or more links added to the selection, only the first link becomes selected,
      /// all other selected items are removed, this is because we can only operate on 1 link at a time.
      /// </remarks>
      private void LstItems_SelectionChanged(object sender, SelectionChangedEventArgs e)
      {
         if (e.OriginalSource == LstItems)                                                                           //a cluster has a combobox, which could also trigger this event, which we don't want.
         {
            AdornerLayer iLayer = AdornerLayer.GetAdornerLayer(LstItems);
            Debug.Assert(iLayer != null);
            MindMapLink iFirstLink = null;
            foreach (MindMapItem i in e.AddedItems)
            {
               MindMapLink iLink = i as MindMapLink;
               if (iLink != null)
               {
                  if (iFirstLink != null)
                     LstItems.SelectedItem = iFirstLink;
                  else
                     iFirstLink = iLink;
                  ListBoxItem iItem = (ListBoxItem)LstItems.ItemContainerGenerator.ContainerFromItem(i);
                  LinkOverlayAdorner iOverlay = new LinkOverlayAdorner(iLink, iItem);
                  i.Adorner = iOverlay;                                                                              //need to keep a reference to the overlya so we can remove it later, can't store it in the listbox item cause this is already destroyed if the selected item gets removed.
                  iLayer.Add(iOverlay);
               }
               else
               {
                  Debug.Assert(i is PositionedMindMapItem);                                                          //only know links and positional items.
                  ListBoxItem iItem = (ListBoxItem)LstItems.ItemContainerGenerator.ContainerFromItem(i);
                  ItemResizeAdorner iResizer = new ItemResizeAdorner((PositionedMindMapItem)i, iItem);
                  i.Adorner = iResizer;                                                                              //need to keep a reference to the overlya so we can remove it later.
                  iLayer.Add(iResizer);
               }

            }

            foreach (MindMapItem i in e.RemovedItems)
            {
               Adorner iResizer = i.Adorner;
               if (iResizer != null)
               {
                  iLayer.Remove(iResizer);
                  LinkOverlayAdorner iLinkOverlay = iResizer as LinkOverlayAdorner;
                  if (iLinkOverlay != null)
                     iLinkOverlay.Clear();
               }
            }
         }
      } 
      #endregion

      #region RubberBand
      /// <summary>
      /// When mouse down on the listbox, but not a listbox item, make certain there are no more selections
      /// + start drawing a selection box.
      /// </summary>
      private void LstItems_MouseDown(object sender, MouseButtonEventArgs e)
      {
         if (e.OriginalSource is Canvas && e.ChangedButton == MouseButton.Left)                        //we check if the mouse event was done on the canvas used by the listbox, just to make certain we are not on something else.
         {
            AdornerLayer iLayer = AdornerLayer.GetAdornerLayer(LstItems);
            Debug.Assert(iLayer != null);
            fSelectionBox = new SelectionAdorner(Mouse.GetPosition(LstItems), LstItems);
            if (LstItems.CaptureMouse() == true)
            {
               iLayer.Add(fSelectionBox);
               LstItems.SelectedItems.Clear();
            }
         }
      }

      private void LstItems_MouseMove(object sender, MouseEventArgs e)
      {
         if (fSelectionBox != null)
            fSelectionBox.EndPoint = Mouse.GetPosition(LstItems);
      }

      /// <summary>
      /// stops the rubber band drawing and selects all the neurons within range
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void LstItems_MouseUp(object sender, MouseButtonEventArgs e)
      {
         if (fSelectionBox != null)
         {
            LstItems.ReleaseMouseCapture();
            AdornerLayer iLayer = AdornerLayer.GetAdornerLayer(LstItems);
            Debug.Assert(iLayer != null);
            iLayer.Remove(fSelectionBox);

            Rect iSelectArea = fSelectionBox.GetSelectArea();
            iSelectArea.Offset(fHorScrollOffset, fVerScrollOffset);                                         //we need to adjust the rect for scrolling before calculating which items are within the frame.
            MindMap iMap = (MindMap)DataContext;
            foreach (MindMapItem iItem in iMap.Items)
            {
               PositionedMindMapItem i = iItem as PositionedMindMapItem;
               if (i != null)
               {
                  if (iSelectArea.Contains(new Rect(i.X, i.Y, i.Width, i.Height)) == true)
                     LstItems.SelectedItems.Add(i);
               }
            }
            fSelectionBox = null;
            LstItems.Focus();
         }
      } 
      #endregion

      #region Context menu

      /// <summary>
      /// Used by all objects that have a context menu, to store the last mouse position.
      /// </summary>
      private void AllContextMenuOpening(object sender, ContextMenuEventArgs e)
      {
         fMousePos = Mouse.GetPosition(this);
      }

      /// <summary>
      /// Handles the Click event of the MnuNewNote control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuNewNote_Click(object sender, RoutedEventArgs e)
      {
         try
         {
            MindMap iMap = (MindMap)DataContext;

            MindMapNote iNote = new MindMapNote();
            AddNewItem(iNote, 120, 120);
         }
         catch (Exception ex)
         {
            string iMsg = ex.ToString();
            MessageBox.Show(iMsg);
            Log.Log.LogError("New note", iMsg);
         }
      }

      /// <summary>
      /// Handles the Click event of the MnuNewObject menu item.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuNewObject_Click(object sender, RoutedEventArgs e)
      {
         try
         {
            WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
            try
            {
               MindMapCluster iObject = EditorsHelper.MakeObject();
               if (iObject != null)
                  AddNewObject(iObject);
            }
            finally
            {
               Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(WindowMain.UndoStore.EndUndoGroup));          //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled.
            }
         }
         catch (Exception ex)
         {
            string iMsg = ex.ToString();
            MessageBox.Show(iMsg);
            Log.Log.LogError("New Object", iMsg);
         }
      }

      

      /// <summary>
      /// Adds a new cluster as an object (showing all it's children).
      /// </summary>
      /// <param name="cluster">The cluster to show.</param>
      private void AddNewObject(MindMapCluster cluster)
      {
         bool fPrevAutoAddVal = MindMapCluster.AutoAddItemsToMindMapCluter;                           //we use a small trick to make certain that all items are shown when the MMcluster is created.
         MindMapCluster.AutoAddItemsToMindMapCluter = true;
         try
         {
            MindMap iMap = (MindMap)DataContext;
            AddNewItem(cluster, 120, 120);
         }
         finally
         {
            MindMapCluster.AutoAddItemsToMindMapCluter = fPrevAutoAddVal;
         }
      }


      /// <summary>
      /// Handles the Click event of the MnuNewNeuron control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuNewNeuron_Click(object sender, RoutedEventArgs e)
      {
         try
         {
            Neuron iNeuron = new Neuron();
            WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
            try
            {
               WindowMain.AddItemToBrain(iNeuron);                                                 //we use this function cause it takes care of the undo data.
               MindMapNeuron iNew = MindMapNeuron.CreateFor(iNeuron);
               AddNewItem(iNew, 90, 50);
            }
            finally
            {
               Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(WindowMain.UndoStore.EndUndoGroup));          //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled.
            }
         }
         catch (Exception ex)
         {
            string iMsg = ex.ToString();
            MessageBox.Show(iMsg);
            Log.Log.LogError("New neuron", iMsg);
         }
      }

      /// <summary>
      /// Handles the Click event of the MnuNewTextNeuron control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuNewTextNeuron_Click(object sender, RoutedEventArgs e)
      {
         try
         {
            TextNeuron iNeuron = new TextNeuron();
            iNeuron.Text = "new neuron";
            WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
            try
            {
               WindowMain.AddItemToBrain(iNeuron);                                                 //we use this function cause it takes care of the undo data.
               MindMapNeuron iNew = MindMapNeuron.CreateFor(iNeuron);
               AddNewItem(iNew, 90, 50);
            }
            finally
            {
               Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(WindowMain.UndoStore.EndUndoGroup));          //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled.
            }
         }
         catch (Exception ex)
         {
            string iMsg = ex.ToString();
            MessageBox.Show(iMsg);
            Log.Log.LogError("New text neuron", iMsg);
         }
      }

      /// <summary>
      /// Handles the Click event of the MnuNewCluster control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuNewCluster_Click(object sender, RoutedEventArgs e)
      {
         try
         {
            NeuronCluster iNeuron = new NeuronCluster();
            WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
            try
            {
               WindowMain.AddItemToBrain(iNeuron);                                                 //we use this function cause it takes care of the undo data.
               MindMapNeuron iNew = MindMapNeuron.CreateFor(iNeuron);
               AddNewItem(iNew, 90, 50);
            }
            finally
            {
               Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(WindowMain.UndoStore.EndUndoGroup));          //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled. -> 
            }
         }
         catch (Exception ex)
         {
            string iMsg = ex.ToString();
            MessageBox.Show(iMsg);
            Log.Log.LogError("New text neuron", iMsg);
         }
      }

      /// <summary>
      /// Handles the Click event of the MnuNewIntNeuron control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuNewIntNeuron_Click(object sender, RoutedEventArgs e)
      {
         try
         {
            IntNeuron iNeuron = new IntNeuron();
            WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
            try
            {
               WindowMain.AddItemToBrain(iNeuron);                                                 //we use this function cause it takes care of the undo data.
               MindMapNeuron iNew = MindMapNeuron.CreateFor(iNeuron);
               AddNewItem(iNew, 90, 50);
            }
            finally
            {
               Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(WindowMain.UndoStore.EndUndoGroup));          //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled. -> 
            }
         }
         catch (Exception ex)
         {
            string iMsg = ex.ToString();
            MessageBox.Show(iMsg);
            Log.Log.LogError("New IntNeuron", iMsg);
         }
      }

      /// <summary>
      /// Handles the Click event of the MnuNewDoubleNeuron control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void MnuNewDoubleNeuron_Click(object sender, RoutedEventArgs e)
      {
         try
         {
            DoubleNeuron iNeuron = new DoubleNeuron();
            WindowMain.UndoStore.BeginUndoGroup(false);                                                        //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
            try
            {
               WindowMain.AddItemToBrain(iNeuron);                                                 //we use this function cause it takes care of the undo data.
               MindMapNeuron iNew = MindMapNeuron.CreateFor(iNeuron);
               AddNewItem(iNew, 90, 50);
            }
            finally
            {
               Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(WindowMain.UndoStore.EndUndoGroup));          //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled. -> 
            }
         }
         catch (Exception ex)
         {
            string iMsg = ex.ToString();
            MessageBox.Show(iMsg);
            Log.Log.LogError("New DoubleNeuron", iMsg);
         }
      }

      /// <summary>
      /// Adds a new item.
      /// </summary>
      /// <param name="toAdd">To add.</param>
      /// <param name="width">The width.</param>
      /// <param name="height">The height.</param>
      void AddNewItem(PositionedMindMapItem toAdd, double width, double height)
      {
         MindMap iMap = (MindMap)DataContext;

         toAdd.X = fMousePos.X + fHorScrollOffset;
         toAdd.Y = fMousePos.Y + fVerScrollOffset;
         toAdd.Width = width;
         toAdd.Height = height;
         iMap.Items.Add(toAdd);
      }

      private void ZoomClicked(object sender, RoutedEventArgs e)
      {
         MenuItem iSender = e.OriginalSource as MenuItem;
         if (iSender != null && iSender.Tag is double)
         {
            MindMap iMap = (MindMap)DataContext;
            iMap.Zoom = (double)iSender.Tag /100;
         }
      }

      #region Commands
      #region Delete
      /// <summary>
      /// Handles the CanExecute event of the Delete control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void Delete_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         e.CanExecute = LstItems.SelectedItems.Count > 0;
      }

      /// <summary>
      /// Handles the Executed event of the Delete control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup(false);
         MindMap iMap = (MindMap)DataContext;
         List<MindMapItem> iToDelete = (from MindMapItem i in LstItems.SelectedItems
                                        orderby iMap.Items.IndexOf(i) ascending
                                        select i).ToList();                                                       //need to make a copy of the list, otherwise we get an error of trying to modify a list in a loop.

         if (iToDelete.Count > 0)                                                                                 //start the deletion of items, this needs to be done async because the undo system needs to close the undo groups correctly.
            DeleteItemFrom(iToDelete, 0);
         //the undo group is closed by the DeleteItemFrom function cause it needs to be closed in a correct sequence.
      }

      /// <summary>
      /// Deletes the item at the specified index from the list.
      /// </summary>
      /// <remarks>
      /// Used for async walking through the list of selected items.
      /// This is required for the undo system.  It needs to be able to record each item in each undo group.  But since
      /// the brain sends events async, which actually trigger the undo data generation, we also need to close the groups
      /// async, which means we need to loop async.
      /// </remarks>
      /// <param name="list">The list.</param>
      /// <param name="index">The index.</param>
      void DeleteItemFrom(List<MindMapItem> list, int index)
      {
         try
         {
            MindMap iMap = (MindMap)DataContext;
            MindMapItem i = list[index++];
            if (i is MindMapLink)                                                                                 //if link, ask to remove the link from the ui or delete it from the brain.
               DeleteLink((MindMapLink)i);
            else if (i is MindMapNeuron)
               DeleteNeuron(iMap, (MindMapNeuron)i);
            else if (i is MindMapNote)
               iMap.Items.Remove(i);
            else
               MessageBox.Show(string.Format("Unknown mindmap item: {0}, can't delete!", i));
            if (index < list.Count)                                                                                        //if not at end of list, to next item, otherwise, simply do next item.
               Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<List<MindMapItem>, int>(DeleteItemFrom), list, index);
            else
               Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(WindowMain.UndoStore.EndUndoGroup));          //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled.
         }
         catch (Exception e)
         {
            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(WindowMain.UndoStore.EndUndoGroup));          //if there's an exception, we still need to close the group.
            throw e;                                                                                                       //let the general handler deal with it.
         }
      }

      /// <summary>
      /// Deletes the neuron.
      /// </summary>
      /// <param name="map">The map.</param>
      /// <param name="neuron">The neuron.</param>
      private void DeleteNeuron(MindMap map, MindMapNeuron neuron)
      {
         ulong iId = neuron.ItemID;
         MessageBoxResult iPermDelete = MessageBox.Show(string.Format("Permenantly delete the neuron '{0}' from the brain or remove it from the UI (yes to permanently delete it)?", BrainData.BrainData.Current.NeuronInfo[iId]), "Delete neuron", MessageBoxButton.YesNoCancel, MessageBoxImage.Asterisk, MessageBoxResult.Yes);
         if (iPermDelete != MessageBoxResult.Cancel)
         {
            WindowMain.UndoStore.BeginUndoGroup(false);                                                //we group all the data together so a single undo command cand restore the previous state. Also need to reverse order due to remove/delete - create/add operation
            try
            {
               switch (iPermDelete)
               {
                  case MessageBoxResult.No:
                     //it's a neuron, all the visible links that reference it, must also be removed from the ui.
                     List<MindMapLink> iAlsoToRemove = (from i in map.Items
                                                        where i is MindMapLink && (((MindMapLink)i).From == iId || ((MindMapLink)i).To == iId)
                                                        select (MindMapLink)i).ToList();
                     foreach (MindMapLink i in iAlsoToRemove)
                        map.Items.Remove(i);
                     map.Items.Remove(neuron);
                     break;
                  case MessageBoxResult.Yes:
                     if (iId >= (ulong)PredefinedNeurons.Dynamic)                                            //we can only delete dynmic items, others can't.
                     {
                        map.Items.Remove(neuron);                                                           //need to do a remove before the delete, so that the action can be reversed correctly.
                        WindowMain.DeleteItemFromBrain(neuron.Item);                                        //don't need to remove any links seperatly, links are destroyed by the AI engine, which triggers the deletion of the mindmap links.
                     }
                     else
                        MessageBox.Show(string.Format("'{0}' is a static neuron which can't be deleted", neuron.NeuronInfo.DisplayTitle), "Delete neuron", MessageBoxButton.OK);
                     break;
                  default:
                     Log.Log.LogError("MindMapView.DeleteNeuron", "internal error: invalid mbox result");
                     break;
               }
            }
            finally
            {
               WindowMain.UndoStore.EndUndoGroup();
            }
         }
      }


      private void DeleteLink(MindMapLink link)
      {
         MessageBoxResult iPermDelete = MessageBox.Show("Permenantly delete the link from the brain or remove it from the UI (yes to permanently delete it)?", "Delete link", MessageBoxButton.YesNoCancel, MessageBoxImage.Asterisk, MessageBoxResult.Yes);
         switch (iPermDelete)
         {
            case MessageBoxResult.Cancel:
               return;
            case MessageBoxResult.No:
               link.Remove();
               break;
            case MessageBoxResult.Yes:
               link.Destroy();
               break;
            default:
               Log.Log.LogError("MindMapView.DeleteLink", "internal error: invalid mbox result");
               break;
         }
      }

      #endregion


      #region Order

      private void MoveUp_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         MindMap iMap = (MindMap)DataContext;

         if (LstItems.SelectedItems.Count > 0)
         {
            var iFound = (from MindMapItem i in LstItems.SelectedItems
                          where i.ZIndex == iMap.Items.Count - 1
                          select i).FirstOrDefault();
            e.CanExecute = iFound == null;                                                               //we can alsways move up if none of the selected items is on the top.
         }
         else
            e.CanExecute = false;
      }

      private void MoveDown_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         MindMap iMap = (MindMap)DataContext;

         if (LstItems.SelectedItems.Count > 0)
         {
            var iFound = (from MindMapItem i in LstItems.SelectedItems
                          where i.ZIndex == 0
                          select i).FirstOrDefault();
            e.CanExecute = iFound == null;                                                               //we can alsways move up if none of the selected items is on the top.
         }
         else
            e.CanExecute = false;
      }


      private void MoveUp_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         MindMap iMap = (MindMap)DataContext;
         List<MindMapItem> iToMove = (from object i in LstItems.SelectedItems
                                      select (MindMapItem)i).ToList();    //need to make a copy of the list so we can modify it.
         iMap.MoveUp(iToMove);
      }

      private void MoveDown_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         MindMap iMap = (MindMap)DataContext;
         List<MindMapItem> iToMove = (from object i in LstItems.SelectedItems
                                      select (MindMapItem)i).ToList();    //need to make a copy of the list so we can modify it.
         iMap.MoveDown(iToMove);
      }

      private void MoveToEnd_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         MindMap iMap = (MindMap)DataContext;
         iMap.MoveToBack((from MindMapItem i in LstItems.SelectedItems select i).ToList());     //need to convert to MindMapItems
      }

      private void MoveToHome_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         MindMap iMap = (MindMap)DataContext;
         iMap.MoveToFront((from MindMapItem i in LstItems.SelectedItems select i).ToList());
      }

      #endregion


      #region Links
      private void ShowOutgoingLinks_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         MindMap iMap = DataContext as MindMap;
         if (iMap != null)
            iMap.ShowLinksOut(LstItems.SelectedItem as MindMapNeuron);
      }

      private void ShowIncommingLinks_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         MindMap iMap = DataContext as MindMap;
         if (iMap != null)
            iMap.ShowLinksIn(LstItems.SelectedItem as MindMapNeuron);
      }

      #region Select links

      private void SelectLinks_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         DlgSelectLinks iNew = new DlgSelectLinks(LstItems.SelectedItem as MindMapNeuron);
         iNew.Owner = Window.GetWindow(this);
         iNew.ShowDialog();
      }

      private void SelectLinks_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         e.CanExecute = LstItems.SelectedItems.Count == 1 && LstItems.SelectedItem is MindMapNeuron;
      }

      #endregion

      /// <summary>
      /// Shows the link dialog box, while trying to fill in the default values using
      /// the following scheme:
      /// selection[0] -> from
      /// selection[1] -> to
      /// selection[2] -> meaning.
      /// </summary>
      private void MnuLink_Click(object sender, RoutedEventArgs e)
      {
         try
         {
            MindMap iMap = (MindMap)DataContext;
            Dialogs.DlgLink iDlg = new Dialogs.DlgLink();
            iDlg.Owner = Window.GetWindow(this);
            iDlg.ToList = (from i in iMap.Items 
                           where i is MindMapNeuron 
                           orderby ((MindMapNeuron )i).NeuronInfo.DisplayTitle
                           select (MindMapNeuron)i).ToList();          //extract all the neurons because we can make connections between those.
            iDlg.FromList = iDlg.ToList;                                                                             //from list and to list is the same.
            if (LstItems.SelectedItems.Count > 0)
            {
               iDlg.SelectedFrom = LstItems.SelectedItems[0] as MindMapNeuron;
               if (LstItems.SelectedItems.Count > 1)
               {
                  iDlg.SelectedTo = LstItems.SelectedItems[1] as MindMapNeuron;
                  if (LstItems.SelectedItems.Count > 2)
                  {
                     MindMapNeuron iMeaning = (MindMapNeuron)LstItems.SelectedItems[2];
                     iDlg.SelectedMeaning = iMeaning.Item;
                  }
               }
            }
            bool? iRes = iDlg.ShowDialog();
            if (iRes.HasValue && iRes.Value == true)
            {
               MindMapNeuron iTo = iDlg.SelectedTo;                                                               //guaranteed to be valid if dialog closed with ok.
               MindMapNeuron iFrom = iDlg.SelectedFrom;
               Neuron iMeaning = iDlg.SelectedMeaning;
               WindowMain.UndoStore.BeginUndoGroup(false);                                                  //we reverse order cause this action generates 2 undo data items: a collection add / a link create.  When reversed we get collection remove / remove link.
               try
               {
                  Link iLink = new Link(iTo.Item, iFrom.Item, iMeaning);
                  MindMapLink iNew = new MindMapLink();
                  iNew.CreateLink(iLink, iFrom, iTo);
                  LinkUndoItem iUndoData = new LinkUndoItem() { Action = BrainAction.Created, Item = iLink };
                  WindowMain.UndoStore.AddCustomUndoItem(iUndoData);
                  iMap.Items.Add(iNew);                                                                           //very important that we do an add at the end, cause we request the list to be performed in reverse order.
               }
               finally
               {
                  WindowMain.UndoStore.EndUndoGroup();
               }
            }
         }
         catch (Exception iEx)
         {
            string iMsg = iEx.ToString();
            MessageBox.Show("failed to create the link:\n" + iMsg, "Link", MessageBoxButton.OK, MessageBoxImage.Error);
         }
      }
      #endregion

      #region ShowChildren
      /// <summary>
      /// Handles the CanExecute event of the ShowChildren command.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void ShowChildren_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         e.CanExecute = LstItems.SelectedItem is MindMapCluster;
      }

      /// <summary>
      /// Handles the Executed event of the ShowChildren command.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void ShowChildren_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup(false);
         try
         {
            MindMap iMap = DataContext as MindMap;
            if (iMap != null)
               iMap.ShowChildren(LstItems.SelectedItem as MindMapCluster);
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      }
      #endregion

      #region ShowClusters
      /// <summary>
      /// Handles the Executed event of the ShowClusters control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void ShowClusters_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup(false);
         try
         {
            MindMap iMap = DataContext as MindMap;
            if (iMap != null)
               iMap.ShowOwners(LstItems.SelectedItem as MindMapNeuron);
         }
         finally
         {
            WindowMain.UndoStore.EndUndoGroup();
         }
      } 
      #endregion

      #endregion


      #endregion

     

      #region Add To cluster
      private void AddToCluster_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         e.CanExecute = LstItems.SelectedItems.Count >= 1 && LstItems.SelectedItem is MindMapNeuron;
      }

      /// <summary>
      /// Handles the Executed event of the AddToCluster control.
      /// </summary>
      /// <remarks>
      /// Shows a dialog box from which the user can select all the clusters to which the first selected item should be added.
      /// If there are more than 1 items selected, all the others are used as default values for the clusters (if possible).
      /// The List of clusters displayed is limited to the clusters that are visible on the mindmap.
      /// <para>
      /// When a circular reference is detected, a warning is displayed.
      /// </para>
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void AddToCluster_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         MindMap iMap = (MindMap)DataContext;

         var iClusters = from i in iMap.Items where i is MindMapCluster                            //build the data for the dialog
                         select ((MindMapCluster)i).Item;
         var iDefault = from object i in LstItems.SelectedItems
                        where i != LstItems.SelectedItem && i is MindMapCluster
                        select ((MindMapCluster)i).Item;
         Dialogs.DlgSelectNeurons iNew = new Dialogs.DlgSelectNeurons(iClusters, iDefault);
         iNew.ItemTemplate = FindResource("SelectableCluster") as DataTemplate;
         iNew.Owner = Window.GetWindow(this);

         bool? iRes = iNew.ShowDialog();
         if (iRes.HasValue == true && iRes.Value == true)                                          //process all the selected data from the dialog if the user selected 'ok'.
         {
            Neuron iToAdd = ((MindMapNeuron)LstItems.SelectedItem).Item;                           //selectedItem is garanteed MindMapNeuron because of CanExecute check.
            NeuronCluster iToAddAsCluster = iToAdd as NeuronCluster;
            foreach (NeuronCluster i in iNew.SelectedValues)      
            {
               if (iToAddAsCluster != null)
               {
                  if (iToAddAsCluster.Children.Contains(i) == true)                                //we have a circular ref, ask the user what to do.
                  {
                     MessageBoxResult iMBoxRes = MessageBox.Show(string.Format("The operation would create a circular reference between '{0}' and '{1}'. Do you want to create this relationship? (cancel will abort the remainder of the operation)", i, iToAdd),"Add children", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning, MessageBoxResult.No);
                     if (iMBoxRes == MessageBoxResult.No)
                        continue;
                     else if (iMBoxRes == MessageBoxResult.Cancel)
                        return;
                  }
               }
               using (ChildrenAccessor iList = i.ChildrenW)
                  iList.Add(iToAdd);                                                             //when we get here, we want to add the item as a child. This call will also take care of deticting circular references.
            }
         }
      } 
      #endregion


      #region RemoveFrom cluster

      private void RemoveFromCluster_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         e.CanExecute = LstItems.SelectedItems.Count >= 1 && LstItems.SelectedItem is MindMapNeuron;
      }

      private void RemoveFromCluster_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         MindMap iMap = (MindMap)DataContext;
         MindMapNeuron iFirstSel = (MindMapNeuron)LstItems.SelectedItem;                              //selectedItem is garanteed MindMapNeuron because of CanExecute check.

         var iClusters = from i in iMap.Items                                                         //build the data for the dialog
                         where i is MindMapCluster && ((MindMapCluster)i).Children.Contains(iFirstSel)
                         select ((MindMapCluster)i).Item;
         var iDefault = from object i in LstItems.SelectedItems
                        where i != LstItems.SelectedItem && i is MindMapCluster
                        select ((MindMapCluster)i).Item;
         Dialogs.DlgSelectNeurons iNew = new Dialogs.DlgSelectNeurons(iClusters, iDefault);
         iNew.ItemTemplate = FindResource("SelectableCluster") as DataTemplate;
         iNew.Owner = Window.GetWindow(this);

         bool? iRes = iNew.ShowDialog();
         if (iRes.HasValue == true && iRes.Value == true)                                             //process all the selected data from the dialog if the user selected 'ok'.
         {
            Neuron iToRemove = iFirstSel.Item;
            foreach (NeuronCluster i in iNew.SelectedValues)
            {
               using (ChildrenAccessor iList = i.ChildrenW)
                  iList.Remove(iToRemove);                                                          //This will also remove any circular ref indicator
            }
         }

      }

      #endregion


      #region Make cluster

      /// <summary>
      /// Handles the CanExecute event of the MakeCluster control.
      /// </summary>
      /// <remarks>
      /// needs to make certain that each selected item is a Neuron.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
      private void MakeCluster_CanExecute(object sender, CanExecuteRoutedEventArgs e)
      {
         if (LstItems.SelectedItems.Count >= 1)
         {
            var iFound = (from object i in LstItems.SelectedItems where !(i is MindMapNeuron) select i).FirstOrDefault();
            e.CanExecute = iFound == null;
         }
         else
            e.CanExecute = false;
      }

      /// <summary>
      /// Handles the Executed event of the MakeCluster control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
      private void MakeCluster_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         NeuronCluster iCluster = Dialogs.DlgCreateCluster.CreateCluster("New cluster", Window.GetWindow(this));
         if (iCluster != null)
         {
            using (ChildrenAccessor iList = iCluster.ChildrenW)
               foreach (MindMapNeuron i in LstItems.SelectedItems)
                  iList.Add(i.ItemID);
            MindMap iMap = (MindMap)DataContext;
            MindMapCluster iNew = new MindMapCluster(iCluster);

            var iQuery = from MindMapNeuron i in LstItems.SelectedItems select i;
            iNew.X = Math.Max(iQuery.Min(w => w.X) - 4, 0);                                                      //we need to set the size of the cluster to the outer limits of the neurons we encapsulate.
            iNew.Width = iQuery.Max(w => w.X + w.Width) - iNew.X + 4;
            iNew.Y = Math.Max(iQuery.Min(w => w.Y) -4, 0);
            iNew.Height = iQuery.Max(w => w.Y + w.Height) - iNew.Y + 4;
            iMap.Items.Add(iNew);
            //iNew.LoadChildren();
         }
      }

      #endregion

      /// <summary>
      /// Handles the ScrollChanged event of the LstItems control.
      /// </summary>
      /// <remarks>
      /// We must keep track of the current scroll position cause we need to add this offset to the mouse position whenever
      /// we add a new item.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Controls.ScrollChangedEventArgs"/> instance containing the event data.</param>
      private void LstItems_ScrollChanged(object sender, ScrollChangedEventArgs e)
      {
         fVerScrollOffset = e.VerticalOffset;
         fHorScrollOffset = e.HorizontalOffset;
      }

      

      #endregion

      

      

      


      


   }
}
