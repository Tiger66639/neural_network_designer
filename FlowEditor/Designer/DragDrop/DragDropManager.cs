using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace JaStDev.HAB.Designer.DragDrop
{
   /// <summary>
   /// Drag drop engine class.
   /// </summary>
	public static class DragDropManager
	{
		static UIElement fDraggedElt;
		static bool fIsMouseDown = false;
		static Point fDragStartPoint;
      static Point fOffsetPoint;
      static DragSourceBase fCurrentDragSource;
      static DropPreviewAdorner fOverlayElt;

		#region Dependency Properties

        public static readonly DependencyProperty DragSourceProperty =
                DependencyProperty.RegisterAttached("DragSource", typeof(DragSourceBase), typeof(DragDropManager),
                new FrameworkPropertyMetadata(new PropertyChangedCallback(OnDragSourceChanged)));

        public static DragSourceBase GetDragSource(DependencyObject depObj)
        {
            return depObj.GetValue(DragSourceProperty) as DragSourceBase;
        }

        public static void SetDragSource(DependencyObject depObj, bool isSet)
        {
            depObj.SetValue(DragSourceProperty, isSet);
        }

		public static readonly DependencyProperty DropTargetProperty =
			DependencyProperty.RegisterAttached("DropTarget", typeof(DropTargetBase), typeof(DragDropManager),
			new FrameworkPropertyMetadata(new PropertyChangedCallback(OnDropTargetChanged)));

		public static void SetDropTarget(DependencyObject depObj, bool isSet)
		{
			depObj.SetValue(DropTargetProperty, isSet);
		}

		public static DropTargetBase GetDropTarget(DependencyObject depObj)
		{
			return depObj.GetValue(DropTargetProperty) as DropTargetBase;
		}

		#endregion

		#region Property Change handlers

      /// <summary>
      /// Called when the drag source is changed.
      /// </summary>
      /// <param name="depObj">The dep obj.</param>
      /// <param name="args">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
      private static void OnDragSourceChanged(DependencyObject depObj, DependencyPropertyChangedEventArgs args)
      {
         UIElement sourceElt = depObj as UIElement;
         if (args.OldValue != null)
         {
            DragSourceBase advisor = args.OldValue as DragSourceBase;
            if (advisor.UsePreviewEvents == true)
            {
               sourceElt.PreviewMouseLeftButtonDown -= DragSource_PreviewMouseLeftButtonDown;
               sourceElt.PreviewMouseMove -= DragSource_PreviewMouseMove;
               sourceElt.PreviewMouseUp -= DragSource_PreviewMouseUp;
            }
            else
            {
               sourceElt.MouseLeftButtonDown -= DragSource_PreviewMouseLeftButtonDown;
               sourceElt.MouseMove -= DragSource_PreviewMouseMove;
               sourceElt.MouseUp -= DragSource_PreviewMouseUp;
            }
         }
         if (args.NewValue != null)
         {
            // Set the Drag source UI
            DragSourceBase advisor = args.NewValue as DragSourceBase;
            //advisor.SourceUI = sourceElt;                                                                 //don't do this, this can cause items to be kept in memory.  This is assigned dynamically as needed.
            if (advisor.UsePreviewEvents == true)
            {
               sourceElt.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(DragSource_PreviewMouseLeftButtonDown);
               sourceElt.PreviewMouseMove += new MouseEventHandler(DragSource_PreviewMouseMove);
               sourceElt.PreviewMouseUp += new MouseButtonEventHandler(DragSource_PreviewMouseUp);
            }
            else
            {
               sourceElt.MouseLeftButtonDown += new MouseButtonEventHandler(DragSource_PreviewMouseLeftButtonDown);         //we use mousedown instead of MouseLeftButton cause mouseleftbutton is a direct event and doesn't except bubbling events which mousedown, mousemove,.. do support and which we want.
               sourceElt.MouseMove += new MouseEventHandler(DragSource_PreviewMouseMove);
               sourceElt.MouseUp += new MouseButtonEventHandler(DragSource_PreviewMouseUp);
            }
         }
      }

      /// <summary>
      /// Handles the PreviewMouseUp event of the DragSource control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
		static void DragSource_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			fIsMouseDown = false;
			Mouse.Capture(null);
		}

      /// <summary>
      /// Called when [drop target changed].
      /// </summary>
      /// <param name="depObj">The dep obj.</param>
      /// <param name="args">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
		private static void OnDropTargetChanged(DependencyObject depObj, DependencyPropertyChangedEventArgs args)
		{
			UIElement targetElt = depObj as UIElement;

         if (args.OldValue != null)
         {
            DropTargetBase advisor = args.OldValue as DropTargetBase;
            if (advisor.UsePreviewEvents == true)
            {
               targetElt.PreviewDragEnter -= DropTarget_PreviewDragEnter;
               targetElt.PreviewDragOver -= DropTarget_PreviewDragOver;
               targetElt.PreviewDragLeave -= DropTarget_PreviewDragLeave;
               targetElt.PreviewDrop -= DropTarget_PreviewDrop;
            }
            else
            {
               targetElt.DragEnter -= DropTarget_PreviewDragEnter;
               targetElt.DragOver -= DropTarget_PreviewDragOver;
               targetElt.DragLeave -= DropTarget_PreviewDragLeave;
               targetElt.Drop -= DropTarget_PreviewDrop;
            }
         }

         if (args.NewValue != null)
         {
            // Set the Drag source UI
            DropTargetBase advisor = args.NewValue as DropTargetBase;

            if (advisor.UsePreviewEvents == true)
            {
               targetElt.PreviewDragEnter += new DragEventHandler(DropTarget_PreviewDragEnter);
               targetElt.PreviewDragOver += new DragEventHandler(DropTarget_PreviewDragOver);
               targetElt.PreviewDragLeave += new DragEventHandler(DropTarget_PreviewDragLeave);
               targetElt.PreviewDrop += new DragEventHandler(DropTarget_PreviewDrop);
            }
            else
            {
               targetElt.DragEnter += new DragEventHandler(DropTarget_PreviewDragEnter);
               targetElt.DragOver += new DragEventHandler(DropTarget_PreviewDragOver);
               targetElt.DragLeave += new DragEventHandler(DropTarget_PreviewDragLeave);
               targetElt.Drop += new DragEventHandler(DropTarget_PreviewDrop);
            }
            targetElt.AllowDrop = true;

         }
         else
            targetElt.AllowDrop = false;
		}


		#endregion


      #region Drop target
      /// <summary>
      /// Handles the PreviewDrop event of the DropTarget control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
      static void DropTarget_PreviewDrop(object sender, DragEventArgs e)
      {
         if (fOverlayElt != null)
         {
            DropTargetBase iDropTarget = GetDropTarget(sender as DependencyObject);

            if (iDropTarget != null)
            {

               if (UpdateEffects(sender, e) == false) return;

               DropTargetBase advisor = GetDropTarget(sender as DependencyObject);
               Point dropPoint = e.GetPosition(sender as UIElement);

               // Calculate displacement for (Left, Top)
               Point offset = e.GetPosition(fOverlayElt);
               dropPoint.X = dropPoint.X - offset.X;
               dropPoint.Y = dropPoint.Y - offset.Y;

               DragSourceBase iSource = e.Data.GetData(JaStDev.HAB.Designer.Properties.Resources.DelayLoadFormat) as DragSourceBase;
               if (iSource != null)
                  iSource.DelayLoad(e.Data);
               advisor.OnDropCompleted(e, dropPoint);
               RemovePreviewAdorner();
               fOffsetPoint = new Point(0, 0);
               e.Handled = true;
            }
         }
      }

      /// <summary>
      /// Handles the PreviewDragLeave event of the DropTarget control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
      static void DropTarget_PreviewDragLeave(object sender, DragEventArgs e)
      {
         if (fOverlayElt != null)
         {
            DropTargetBase advisor = GetDropTarget(sender as DependencyObject);
            if (UpdateEffects(sender, e) == false)
            {
               if (advisor.UsePreviewEvents == false)
                  e.Handled = true;
               return;
            }
            Point mousePoint = MouseUtilities.GetMousePosition(advisor.TargetUI);

            //Console.WriteLine("Inside DropTarget_PreviewDragLeave1" + mousePoint.X.ToString() + "|" + mousePoint.Y.ToString());
            //giving a tolerance of 2 so that the adorner is removed when the mouse is moved fast.
            //this might still be small...in that case increase the tolerance
            if ((mousePoint.X < 2) || (mousePoint.Y < 2) ||
                (mousePoint.X > ((FrameworkElement)(advisor.TargetUI)).ActualWidth - 2) ||
                (mousePoint.Y > ((FrameworkElement)(advisor.TargetUI)).ActualHeight - 2))
            {
               RemovePreviewAdorner();
            }
            e.Handled = true;
         }
      }

      /// <summary>
      /// Handles the PreviewDragOver event of the DropTarget control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
      static void DropTarget_PreviewDragOver(object sender, DragEventArgs e)
      {
         DropTargetBase iDropTarget = GetDropTarget(sender as DependencyObject);

         if (iDropTarget != null)
         {
            if (UpdateEffects(sender, e) == false)
            {
               if (iDropTarget.UsePreviewEvents == false)
                  e.Handled = true;
               return;
            }
            // Update position of the preview Adorner
            Point position = e.GetPosition(sender as UIElement);

            GetDropTarget(sender as DependencyObject).OnDragOver(ref position, e.Data);               //let the drop advisor know something is being dragged over it.

            fOverlayElt.Left = position.X - fOffsetPoint.X;
            fOverlayElt.Top = position.Y - fOffsetPoint.Y;

            e.Handled = true;
         }
      }

      /// <summary>
      /// Handles the PreviewDragEnter event of the DropTarget control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.DragEventArgs"/> instance containing the event data.</param>
      static void DropTarget_PreviewDragEnter(object sender, DragEventArgs e)
      {
         DropTargetBase iDropTarget = GetDropTarget(sender as DependencyObject);

         if (iDropTarget != null)
         {
            if (UpdateEffects(sender, e) == false)
            {
               if (iDropTarget.UsePreviewEvents == false)
                  e.Handled = true;
               return;
            }

            iDropTarget.TargetUI = (UIElement)sender;                                                    //drop target is variable
            // Setup the preview Adorner
            UIElement feedbackUI = iDropTarget.GetVisualFeedback(e.Data);
            fOffsetPoint = GetOffsetPoint(e.Data);
            // iDropTarget.TargetUI = sender as UIElement;                                            //when a droptarget is declared as resource, it can be assigned to multiple objects, we need to update the target as it it currently being used.
            Point mousePoint = MouseUtilities.GetMousePosition(iDropTarget.TargetUI);
            FrameworkElement iTargetUI = (FrameworkElement)iDropTarget.TargetUI;
            if ((mousePoint.X < 2) || (mousePoint.Y < 2) || (mousePoint.X > iTargetUI.ActualWidth - 2)   //giving a tolerance of 2 so that the adorner is created when the mouse is moved fast. this might still be small...in that case increase the tolerance
               || (mousePoint.Y > iTargetUI.ActualHeight - 2) || (fOverlayElt == null))
               CreatePreviewAdorner(sender as UIElement, feedbackUI);
            e.Handled = true;
         }
      } 
      #endregion

        static Point GetOffsetPoint(IDataObject obj)
        {
            Point p = (Point)obj.GetData("OffsetPoint");
            return p;
        }

		static bool UpdateEffects(object uiObject, DragEventArgs e)
		{
			DropTargetBase advisor = GetDropTarget(uiObject as DependencyObject);
         advisor.TargetUI = uiObject as UIElement;                                                       //we must reassign it cause this can be a shared advisor, for which this value switches
         if (advisor.IsValidDataObject(e.Data) == false)
         {
            e.Effects = DragDropEffects.None;
            return false;
         }

			if ((e.AllowedEffects & DragDropEffects.Move) == 0 &&
				(e.AllowedEffects & DragDropEffects.Copy) == 0)
			{
				e.Effects = DragDropEffects.None;
				return true;
			}

			if ((e.AllowedEffects & DragDropEffects.Move) != 0 &&
				(e.AllowedEffects & DragDropEffects.Copy) != 0)
			{
            e.Effects = advisor.GetEffect(e); 
			}

			return true;
		}


      #region Drag source event handlers
      /// <summary>
      /// Handles the PreviewMouseLeftButtonDown event of the DragSource control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
      static void DragSource_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
      {
         if (fIsMouseDown == false && e.ChangedButton == MouseButton.Left &&
            e.LeftButton == MouseButtonState.Pressed && e.ClickCount == 1)                                             //this allows us to put several drag sources on top of each other.  The topmost dragsource sets this flag, preventing lower drag sources from steeling the handle.
         {
            // Make this the new drag source
            DragSourceBase advisor = GetDragSource(sender as DependencyObject);
            advisor.SourceUI = (UIElement)sender;
            if (advisor.IsDraggable(e.Source as UIElement) == true)
            {
               fCurrentDragSource = advisor;
               fDraggedElt = e.Source as UIElement;
               fDragStartPoint = e.GetPosition(GetTopContainer());

               fOffsetPoint = e.GetPosition(fDraggedElt);
               fIsMouseDown = true;
            }
         }

      }

      static void DragSource_PreviewMouseMove(object sender, MouseEventArgs e)
      {
         DragSourceBase advisor = GetDragSource(sender as DependencyObject);

         if (fIsMouseDown == true && fCurrentDragSource == advisor && IsDragGesture(e.GetPosition(GetTopContainer())))
            DragStarted(sender as UIElement, advisor);
      }

      static void DragStarted(UIElement uiElt, DragSourceBase advisor)
      {
         fIsMouseDown = false;
         Mouse.Capture(uiElt);

         DataObject data = advisor.GetDataObject(fDraggedElt);
         data.SetData("OffsetPoint", fOffsetPoint);
         DragDropEffects supportedEffects = advisor.SupportedEffects;
         try
         {
            WindowMain.UndoStore.BeginUndoGroup(false);                                                                 //we put a drag in an undo group so cause a drag is a single maneuvre.
            try
            {
               DragDropEffects effects = System.Windows.DragDrop.DoDragDrop(fDraggedElt, data, supportedEffects);       // Perform DragDrop
               advisor.FinishDrag(fDraggedElt, effects);
            }
            finally
            {
               WindowMain.UndoStore.EndUndoGroup();
            }
         }
         catch (Exception e)
         {
            MessageBox.Show(e.ToString(), "Drag operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
            Log.LogError("DragDropManager", e.ToString());
         }

         // Clean up
         RemovePreviewAdorner();
         Mouse.Capture(null);
         fDraggedElt = null;
      } 
      #endregion

		static bool IsDragGesture(Point point)
		{
			bool hGesture = Math.Abs(point.X - fDragStartPoint.X) > SystemParameters.MinimumHorizontalDragDistance;
			bool vGesture = Math.Abs(point.Y - fDragStartPoint.Y) > SystemParameters.MinimumVerticalDragDistance;

			return (hGesture | vGesture);
		}

		/* ____________________________________________________________________
		 *		Utility functions
		 * ____________________________________________________________________
		 */
		static UIElement GetTopContainer()
		{
           // return  LogicalTreeHelper.FindLogicalNode(Application.Current.MainWindow, "canvas") as UIElement;

			return Application.Current.MainWindow.Content as UIElement;
		}

		private static void CreatePreviewAdorner(UIElement adornedElt, UIElement feedbackUI)
		{
			// Clear if there is an existing preview adorner
			RemovePreviewAdorner();

			AdornerLayer layer = AdornerLayer.GetAdornerLayer(GetTopContainer());
			fOverlayElt = new DropPreviewAdorner(feedbackUI, adornedElt);
			layer.Add(fOverlayElt);
		}

		private static void RemovePreviewAdorner()
		{
			if (fOverlayElt != null)
			{
				AdornerLayer.GetAdornerLayer(GetTopContainer()).Remove(fOverlayElt);
				fOverlayElt = null;
			}
		}

	}

   public class MouseUtilities
   {
      [System.Runtime.InteropServices.StructLayout(LayoutKind.Sequential)]
      private struct Win32Point
      {
         public Int32 X;
         public Int32 Y;
      };

      [DllImport("user32.dll")]
      private static extern bool GetCursorPos(ref Win32Point pt);

      [DllImport("user32.dll")]
      private static extern bool ScreenToClient(IntPtr hwnd, ref Win32Point pt);

      public static Point GetMousePosition(Visual relativeTo)
      {
         Win32Point mouse = new Win32Point();
         GetCursorPos(ref mouse);

         System.Windows.Interop.HwndSource presentationSource = (System.Windows.Interop.HwndSource)PresentationSource.FromVisual(relativeTo);
         if (presentationSource != null)
            ScreenToClient(presentationSource.Handle, ref mouse);
         else
            throw new InvalidOperationException("Failed to find main application handle.");

         GeneralTransform transform = relativeTo.TransformToAncestor(presentationSource.RootVisual);

         Point offset = transform.Transform(new Point(0, 0));

         return new Point(mouse.X - offset.X, mouse.Y - offset.Y);
      }
   }

}
