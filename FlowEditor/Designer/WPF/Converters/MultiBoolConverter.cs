﻿using System;
using System.Windows.Data;

namespace JaStDev.HAB.Designer.WPF.Converters
{
   //not used
   /// <summary>
   /// A converter that expects all bool values and returns wether all are true or not. this converter works 2 ways: when
   /// converting back, it returns all 'true' or all 'false'
   /// </summary>
   public class MultiBoolConverter : IMultiValueConverter
   {
      #region IMultiValueConverter Members

      public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         foreach (object i in values)
         {
            if (!(i is bool) || (bool)i == false)
               return false;
         }
         return true;
      }

      public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
      {
         object[] iRes = new object[targetTypes.Length];
         for (int i = 0; i < targetTypes.Length; i++)
            iRes[i] = value;
         return iRes;
      }

      #endregion
   }
}
