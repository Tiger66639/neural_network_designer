﻿using System;
using System.Windows.Data;

namespace JaStDev.HAB.Designer.WPF.Converters
{
   /// <summary>
   /// return the resource with key:  'Img' +  the name of the type (passed in through the value). If not found, tries to go up the inheritence tree
   /// of the type untill it finds a resource. Very similar to <see cref="NeuronToImageConverter"/>
   /// </summary>
   ///<remarks>
   /// convert from: Type
   /// convert to: ojbect
   ///</remarks>
   public class TypeToResourceConverter : IValueConverter
   {
      #region IValueConverter Members;

      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         Type iVal = value as Type;
         if (iVal != null)
         {
            object iFound = App.Current.TryFindResource("Img" + iVal.Name);
            if (iFound == null)
            {
               Type iBase = iVal.BaseType;
               while (iBase != null)
               {
                  iFound = App.Current.TryFindResource("Img" + iBase.Name);
                  if (iFound != null)
                     break;
                  iBase = iBase.BaseType;
               }
            }
            return iFound;
         }
         else
            return null;
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         throw new NotImplementedException();
      }

      #endregion
   }
}
