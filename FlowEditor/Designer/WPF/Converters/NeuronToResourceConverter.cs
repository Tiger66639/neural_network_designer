﻿using System;
using System.Globalization;
using System.Windows.Data;
using JaStDev.HAB.Brain;

namespace JaStDev.HAB.Designer.WPF.Converters
{
   /// <summary>
   /// Looks up the corresponding ImageSource for the neuron.
   /// </summary>
   /// <remarks>
   /// <para>
   /// required parameter: non
   /// Uses App.Current to find resources.
   /// </para>
   /// <para>
   /// searches resources for the following key:
   /// -first try the 'Img' + Neuron.id.ToString() 
   /// -next: 'Img' + typeof(neuron).ToString()
   /// </para>
   /// </remarks>
   public class NeuronToResourceConverter:  IValueConverter
   {

      string fPrefix = "Img";
      string fSuffix = "";

      #region Prefix

      /// <summary>
      /// Gets/sets the prefix to use for searching the resource (text to insert in the front).  default is 'Img'.
      /// </summary>
      public string Prefix
      {
         get
         {
            return fPrefix;
         }
         set
         {
            fPrefix = value;
         }
      }

      #endregion

      
      #region Suffix

      /// <summary>
      /// Gets/sets the suffix to use for searching the resource (text to append at the end).  Default is empty.
      /// </summary>
      public string Suffix
      {
         get
         {
            return fSuffix;
         }
         set
         {
            fSuffix = value;
         }
      }

      #endregion


      #region IValueConverter Members

      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         Neuron iVal = value as Neuron;
         if (iVal != null)
         {
            object iFound = App.Current.TryFindResource(Prefix + iVal.ID.ToString() + Suffix);
            if (iFound == null)
            {
               iFound = App.Current.TryFindResource(Prefix + iVal.GetType().Name + Suffix);
               if (iFound == null)
               {
                  Type iBase = iVal.GetType().BaseType;
                  while (iBase != null)
                  {
                     iFound = App.Current.TryFindResource(Prefix + iBase.Name + Suffix);
                     if (iFound != null)
                        break;
                     iBase = iBase.BaseType;
                  }
               }
            }
            return iFound;
         }
         else
            return null;
      }

      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         throw new NotImplementedException();
      }

      #endregion

      //protected override Freezable CreateInstanceCore()
      //{
      //   return new NeuronToResourceConverter();
      //}
   }
}
