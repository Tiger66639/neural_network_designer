﻿using System;
using System.Windows.Data;

namespace JaStDev.HAB.Designer.WPF.Converters
{
   /// <summary>
   /// Takes a double and returns half of the value.
   /// </summary>
   public class DivConverter: IValueConverter
   {
      #region IValueConverter Members

      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         return (double)value / 2;
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         return (double)value * 2;
      }

      #endregion
   }
}
