﻿using System;
using System.Windows;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Events;

namespace JaStDev.HAB.Designer.WPF.BrainEventManagers
{
   /// <summary>
   /// The event manager for the weak event pattern used for the <see cref="Brain.NeuronListChanged"/> event.
   /// </summary>
   /// <remarks>
   /// use this class to prevent mem leaks.
   /// </remarks>
   public class NeuronListChangedEventManager: WeakEventManager
   {

      private static NeuronListChangedEventManager CurrentManager
      {
         get
         {
            Type iType = typeof(NeuronListChangedEventManager);
            NeuronListChangedEventManager currentManager = (NeuronListChangedEventManager)WeakEventManager.GetCurrentManager(iType);
            if (currentManager == null)
            {
               currentManager = new NeuronListChangedEventManager();
               WeakEventManager.SetCurrentManager(iType, currentManager);
            }
            return currentManager;
         }
      }
 


      public static void AddListener(Brain.Brain source, IWeakEventListener listener)
      {
         if (Environment.HasShutdownStarted == false)
            CurrentManager.ProtectedAddListener(source, listener);
      }

      public static void RemoveListener(Brain.Brain source, IWeakEventListener listener)
      {
         if (Environment.HasShutdownStarted == false)
            CurrentManager.ProtectedRemoveListener(source, listener);
      }



      protected override void StartListening(object source)
      {
         Brain.Brain iSource = (Brain.Brain)source;
         iSource.NeuronListChanged += new NeuronListChangedEventHandler(NeuronListChanged);
      }

      protected override void StopListening(object source)
      {
         Brain.Brain iSource = (Brain.Brain)source;
         iSource.NeuronListChanged -= new NeuronListChangedEventHandler(NeuronListChanged);
      }

      void NeuronListChanged(object sender, NeuronListChangedEventArgs e)
      {
         DeliverEvent(sender, e);
      }
   }
}
