﻿using System;
using System.Windows;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Events;

namespace JaStDev.HAB.Designer.WPF.BrainEventManagers
{
   /// <summary>
   /// The event manager for the weak event pattern used for the <see cref="Brain.NeuronChanged"/> event.
   /// </summary>
   /// <remarks>
   /// use this class to prevent mem leaks.
   /// </remarks>
   public class NeuronChangedEventManager : WeakEventManager
   {
      /// <summary>
      /// Gets the current manager.
      /// </summary>
      /// <value>The current manager.</value>
      private static NeuronChangedEventManager CurrentManager
      {
         get
         {
            Type iType = typeof(NeuronChangedEventManager);
            NeuronChangedEventManager currentManager = (NeuronChangedEventManager)WeakEventManager.GetCurrentManager(iType);
            if (currentManager == null)
            {
               currentManager = new NeuronChangedEventManager();
               WeakEventManager.SetCurrentManager(iType, currentManager);
            }
            return currentManager;
         }
      }



      /// <summary>
      /// Adds the listener.
      /// </summary>
      /// <param name="source">The source.</param>
      /// <param name="listener">The listener.</param>
      public static void AddListener(Brain.Brain source, IWeakEventListener listener)
      {
         if (Environment.HasShutdownStarted == false)
            CurrentManager.ProtectedAddListener(source, listener);
      }

      /// <summary>
      /// Removes the listener.
      /// </summary>
      /// <param name="source">The source.</param>
      /// <param name="listener">The listener.</param>
      public static void RemoveListener(Brain.Brain source, IWeakEventListener listener)
      {
         if (Environment.HasShutdownStarted == false)
            CurrentManager.ProtectedRemoveListener(source, listener);
      }


      /// <summary>
      /// When overridden in a derived class, starts listening for the event being managed. After <see cref="M:System.Windows.WeakEventManager.StartListening(System.Object)"/>  is first called, the manager should be in the state of calling <see cref="M:System.Windows.WeakEventManager.DeliverEvent(System.Object,System.EventArgs)"/> or <see cref="M:System.Windows.WeakEventManager.DeliverEventToList(System.Object,System.EventArgs,System.Windows.WeakEventManager.ListenerList)"/> whenever the relevant event from the provided source is handled.
      /// </summary>
      /// <param name="source">The source to begin listening on.</param>
      protected override void StartListening(object source)
      {
         Brain.Brain iSource = (Brain.Brain)source;
         iSource.NeuronChanged += new NeuronChangedEventHandler(NeuronChanged);
      }


      /// <summary>
      /// When overridden in a derived class, stops listening on the provided source for the event being managed.
      /// </summary>
      /// <param name="source">The source to stop listening on.</param>
      protected override void StopListening(object source)
      {
         Brain.Brain iSource = (Brain.Brain)source;
         iSource.NeuronChanged -= new NeuronChangedEventHandler(NeuronChanged);
      }

      /// <summary>
      /// handles the Neurons changed event.
      /// </summary>
      /// <param name="sender">The sender.</param>
      /// <param name="e">The <see cref="NeuronChangedEventArgs"/> instance containing the event data.</param>
      void NeuronChanged(object sender, NeuronChangedEventArgs e)
      {
         base.DeliverEvent(sender, e);
      }
   }
}
