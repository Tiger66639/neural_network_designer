﻿using System;
using System.Windows;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Events;

namespace JaStDev.HAB.Designer.WPF.BrainEventManagers
{
   /// <summary>
   /// The event manager for the weak event pattern used for the <see cref="Brain.LinkChanged"/> event.
   /// </summary>
   /// <remarks>
   /// use this class to prevent mem leaks.
   /// </remarks>
   public class LinkChangedEventManager: WeakEventManager
   {
      private static LinkChangedEventManager CurrentManager
      {
         get
         {
            Type iType = typeof(LinkChangedEventManager);
            LinkChangedEventManager currentManager = (LinkChangedEventManager)WeakEventManager.GetCurrentManager(iType);
            if (currentManager == null)
            {
               currentManager = new LinkChangedEventManager();
               WeakEventManager.SetCurrentManager(iType, currentManager);
            }
            return currentManager;
         }
      }



      public static void AddListener(Brain.Brain source, IWeakEventListener listener)
      {
         if (Environment.HasShutdownStarted == false)
            CurrentManager.ProtectedAddListener(source, listener);
      }

      public static void RemoveListener(Brain.Brain source, IWeakEventListener listener)
      {
         if (Environment.HasShutdownStarted == false)
            CurrentManager.ProtectedRemoveListener(source, listener);
      }

      protected override void StartListening(object source)
      {
         Brain.Brain iSource = (Brain.Brain)source;
         iSource.LinkChanged += new LinkChangedEventHandler(LinkChanged);
      }

      protected override void StopListening(object source)
      {
         Brain.Brain iSource = (Brain.Brain)source;
         iSource.LinkChanged -= new LinkChangedEventHandler(LinkChanged);
      }

      void LinkChanged(object sender, LinkChangedEventArgs e)
      {
         base.DeliverEvent(sender, e);
      }
   }
}
