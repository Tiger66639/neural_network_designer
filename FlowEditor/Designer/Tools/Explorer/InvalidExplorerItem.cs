﻿
namespace JaStDev.HAB.Designer.Tools.Explorer
{
   /// <summary>
   /// An explorer item that indicates an error was encountered while trying to retrieve this value.
   /// has as extar info a description of the error that was encountered + the log item?.
   /// </summary>
   public class InvalidExplorerItem: ExplorerItem
   {
      string fError;


      public InvalidExplorerItem(ulong id, string error): base(id)
      {
         fError = error;
      }

      #region Error

      /// <summary>
      /// Gets the error message that was returned while trying to retrieve the item.
      /// </summary>
      public string Error
      {
         get { return fError; }
      }

      #endregion

   }
}
