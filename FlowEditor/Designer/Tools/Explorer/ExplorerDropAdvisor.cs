﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using JaStDev.HAB.Designer.DragDrop;

namespace JaStDev.HAB.Designer.Tools.Explorer
{
   /// <summary>
   /// provides drop functionality to the children list of a <see cref="NeuronExplorer"/>.
   /// </summary>
   public class ExplorerDropAdvisor: DropTargetBase
   {
      public ExplorerDropAdvisor()
      {
         SupportedFormat = Properties.Resources.NeuronIDFormat;
      }

      public override void OnDropCompleted(DragEventArgs obj, Point dropPoint)
      {
         NeuronExplorerItem iSelected = ((ListBox)TargetUI).Tag as NeuronExplorerItem;
         Debug.Assert(iSelected != null);
         NeuronCluster iCluster = iSelected.Item as NeuronCluster;

         if (iCluster != null)                                                               //we can only add if it is a cluster
         {
            ulong iId = (ulong)obj.Data.GetData(Properties.Resources.NeuronIDFormat);
            using (ChildrenAccessor iList = iCluster.ChildrenW)
               iList.Add(iId);                                                      //the add in the cluster triggers an update in the NeuronExplorer.
         }
      }


      public override bool IsValidDataObject(IDataObject obj)
      {
         bool iRes = base.IsValidDataObject(obj);
         if (iRes == true)
         {
            NeuronExplorerItem iSelected = ((ListBox)TargetUI).Tag as NeuronExplorerItem;
            if (iSelected != null)                                                           //could be that it is not a neuron explorer item.
               return iSelected.Item is NeuronCluster;
            else
               return false;
         }
         return iRes;
      }
   }
}
