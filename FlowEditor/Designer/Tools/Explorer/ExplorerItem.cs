﻿namespace JaStDev.HAB.Designer.Tools.Explorer
{
   /// <summary>
   /// Represents all the info known for an item in the <see cref="NeuronExplorer"/>.
   /// </summary>
   /// <remarks>
   /// This object is never persisted, so it uses the object reference of the neuron directly, without
   /// bothering about the id.
   /// </remarks>
   public class ExplorerItem : ObservableObject
   {

      ulong fID;

      /// <summary>
      /// Initializes a new instance of the <see cref="ExplorerItem"/> class.
      /// </summary>
      /// <param name="index">The index of the item we are wrapping.</param>
      public ExplorerItem(ulong id)
      {
         ID = id;
      }


      #region ID

      /// <summary>
      /// Gets the index nr of this item in the neuron list.  If this item represents an invalid item, the index is also
      /// stored here.
      /// </summary>
      public ulong ID
      {
         get { return fID; }
         internal set { fID = value; }
      }

      #endregion

      

      
   }
}
