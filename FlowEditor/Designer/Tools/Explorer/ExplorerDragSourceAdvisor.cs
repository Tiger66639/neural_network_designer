﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using JaStDev.HAB.Designer.DragDrop;

namespace JaStDev.HAB.Designer.Tools.Explorer
{
   public class ExplorerDragSourceAdvisor : DragSourceBase
   {
      public ExplorerDragSourceAdvisor()
      {
         SupportedFormat = Properties.Resources.NeuronIDFormat;                                                        //this is not really used, simply informtive: this is our main data type.
      }

      /// <summary>
      /// Never do anything after a drag.  Items simply stay where they are in the same order.
      /// </summary>
      public override void FinishDrag(UIElement draggedElt, DragDropEffects finalEffects)
      {
         //throw new NotImplementedException();
      }

      public override bool IsDraggable(UIElement dragElt)
      {
         FrameworkElement iDragged = (FrameworkElement)dragElt;
         if (iDragged != null)
         {
            NeuronExplorerItem iData = iDragged.DataContext as NeuronExplorerItem;
            return iData != null && iData.Item != null;
         }
         return false;
      }

      /// <summary>
      /// we override cause we put the image to use + an ulong if it is a neuron, or a ref to the mind map item.
      /// If the item is a link, we also store which side of the link it was, so we can adjust it again (+ update it).
      /// </summary>
      public override DataObject GetDataObject(UIElement draggedElt)
      {
         FrameworkElement iDragged = (FrameworkElement)draggedElt;
         DataObject iObj = new DataObject();

         Debug.Assert(SourceUI is ListBoxItem);
         ListBox iSource = ItemsControl.ItemsControlFromItemContainer(SourceUI) as ListBox;
         if (iSource.SelectedItems.Count == 1)                                                                 //need to store single data info.
         {
            NeuronExplorerItem iData = iDragged.DataContext as NeuronExplorerItem;
            Debug.Assert(iData != null);

            iObj.SetData(Properties.Resources.UIElementFormat, iDragged);
            iObj.SetData(Properties.Resources.NeuronIDFormat, iData.Item.ID);
         }
         else
         {                                                                                                     //need to store list info of all the dragged items.
            var iSelected = from ExplorerItem i in iSource.SelectedItems
                            where i is NeuronExplorerItem
                            select (NeuronExplorerItem)i;
            List<UIElement> iSelectedUI = new List<UIElement>();
            foreach (NeuronExplorerItem i in iSelected)
               iSelectedUI.Add((UIElement)iSource.ItemContainerGenerator.ContainerFromItem(i));
            iObj.SetData(Properties.Resources.MultiUIElementFormat, iSelectedUI);
            List<ulong> iSelectedID = (from i in iSelected select i.ID).ToList();
            iObj.SetData(Properties.Resources.MultiNeuronIDFormat, iSelectedID);
         }
         return iObj;

      }
   }
}
