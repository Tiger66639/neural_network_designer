﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Processor;

namespace JaStDev.HAB.Designer.Tools.Debugger
{
    /// <summary>
    ///     Used to attach debug data to log items so we can find the neurons that triggered the log event.
    /// </summary>
    internal class LogDebugData
    {
        /// <summary>
        ///     Gets or sets the neuron that was being executed.
        /// </summary>
        /// <value>The neuron being executed.</value>
        public Neuron Executed { get; set; }

        /// <summary>
        ///     Gets or sets the neruon that was being solved. This can be null if the system deleted the neuron.
        /// </summary>
        /// <value>The solved.</value>
        public Neuron Solved { get; set; }

        /// <summary>
        ///     Gets or sets the meaning of the link that was being executed while the log event happened (this allows us to
        ///     display the code).
        /// </summary>
        /// <remarks>
        ///     Not always required: when the actions are being solved at the end of a neuron for instance, or a split resolve.
        /// </remarks>
        /// <value>The meaning.</value>
        public Neuron Meaning { get; set; }

        /// <summary>
        ///     Gets or sets the neuron which is the current execution source.
        /// </summary>
        /// <remarks>
        ///     This value is always valid, even if there is no meaning.  Can be the same as the meaning but if this contains a
        ///     branch or block,
        ///     this value points to the block neuron.
        /// </remarks>
        /// <value>The execution source.</value>
        public Neuron ExecutionSource { get; set; }

        /// <summary>
        ///     Gets or sets the type of the execution list attached to <see cref="LogDebugData.ExecutionSource" />, which allows
        ///     us to show the
        ///     correct tab if we don't have a meaning.
        /// </summary>
        /// <value>The type of the exec list.</value>
        public ExecListType ExecListType { get; set; }
    }
}