﻿using System.Windows.Controls;

namespace JaStDev.HAB.Designer.Tools.Debugger
{
   /// <summary>
   /// Displays the data for 1 <see cref="DebugProcessor"/>.
   /// </summary>
   public partial class ProcessorView : UserControl
   {
      public ProcessorView()
      {
         InitializeComponent();
      }
   }
}
