﻿using System.Xml.Serialization;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.BrainData;
using JaStDev.HAB.Expressions;

namespace JaStDev.HAB.Designer.Tools.Debugger
{
   /// <summary>
   /// A wrapper class for variables that can be observed by the debugger.
   /// </summary>
   public class Watch: ObservableObject, INeuronInfo
   {

      #region Fields
      NeuronData fNeuronInfo; 
      #endregion

      #region ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="Watch"/> class.
      /// </summary>
      public Watch()
      {
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="Watch"/> class.
      /// </summary>
      /// <param name="id">The id.</param>
      public Watch(ulong id)
      {
         ID = id;
      }

      #endregion

      #region ID

      /// <summary>
      /// Gets/sets the id of the variable to wrap.
      /// </summary>
      public ulong ID
      {
         get
         {
            if(fNeuronInfo != null)
               return fNeuronInfo.ID;
            else
               return Neuron.EmptyId;
         }
         set
         {
            fNeuronInfo = BrainData.BrainData.Current.NeuronInfo[value];
            if (fNeuronInfo != null && !(fNeuronInfo.Neuron is Variable))                           //if it's not a var, not allowed.    
            {
               fNeuronInfo = null;
               Log.Log.LogError("Watch.ID", "A watch can only wrap variables!");
            }
         }
      }

      #endregion

      #region INeuronInfo Members

      [XmlIgnore]
      public NeuronData NeuronInfo
      {
         get { return fNeuronInfo; }
      }

      #endregion
   }
}
