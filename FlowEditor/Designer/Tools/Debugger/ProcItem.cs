﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Threading;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Expressions;

namespace JaStDev.HAB.Designer.Tools.Debugger
{
   /// <summary>
   /// A wrapper class for <see cref="Processor"/> objects, updated for WPF displaying.
   /// </summary>
   public class ProcItem: OwnedObject
   {

      #region Fields

      Processor.Processor fToWrap;
      ObservableCollection<DebugNeuron> fValues = new ObservableCollection<DebugNeuron>();
      bool fIsViewOpen;

      #endregion

      #region ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="ProcItem"/> class.
      /// </summary>
      /// <param name="toWrap">The processor to wrap.</param>
      public ProcItem(Processor.Processor toWrap)
      {
         fToWrap = toWrap;
         if (toWrap != null)
         {
            toWrap.Finished += new EventHandler(Processor_Finished);
            if(toWrap is DebugProcessor)
               ((DebugProcessor)toWrap).Paused += new EventHandler(ProcItem_Paused);
         }
         fValues.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(fValues_CollectionChanged);
      }

      #endregion

      #region Prop

      #region HasValues

      /// <summary>
      /// Gets wether there are currently values available for display.
      /// </summary>
      public bool HasValues
      {
         get { return fValues.Count > 0; }
      }

      #endregion

      #region Processor

      /// <summary>
      /// Gets the processor that is wrapped by this item.
      /// </summary>
      public Processor.Processor Processor
      {
         get { return fToWrap; }
      }

      #endregion

      #region Values

      /// <summary>
      /// Gets the list of values that should be displayed for this node.
      /// </summary>
      public ObservableCollection<DebugNeuron> Values
      {
         get { return fValues; }
      }

      #endregion

      #region IsSplit

      /// <summary>
      /// Gets a value indicating whether this instance is split for several other processor items.
      /// </summary>
      /// <value><c>true</c> if this instance is split; otherwise, <c>false</c>.</value>
      public virtual bool IsSplit
      {
         get { return false; }
      }

      #endregion

      #region IsViewOpen

      /// <summary>
      /// Gets/sets the wether the detailed view for this processor is currently opened or not.
      /// </summary>
      public bool IsViewOpen
      {
         get
         {
            return fIsViewOpen;
         }
         set
         {
            if (fIsViewOpen != value)
            {
               fIsViewOpen = value;
               OnPropertyChanged("IsViewOpen");
               if (value == true)
                  BrainData.BrainData.Current.OpenDocuments.Add(this);
               else
                  BrainData.BrainData.Current.OpenDocuments.Remove(this);
            }
         }
      }

      #endregion

      #endregion

      #region Functions
      /// <summary>
      /// Assigns the value(s) stored in the specified variable (for this processor) to <see cref="ProcItem.Values"/>
      /// </summary>
      /// <remarks>
      /// this is thread safe.
      /// </remarks>
      /// <param name="toDisplay">To display.</param>
      public virtual void GetValuesFor(Variable toDisplay)
      {
         List<Neuron> iList = fToWrap.GetValueFor(toDisplay);
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<List<Neuron>>(DisplayValues), iList);
      }

      /// <summary>
      /// Displays the values.
      /// </summary>
      /// <param name="list">The list.</param>
      void DisplayValues(List<Neuron> list)
      {
         Values.Clear();
         foreach (Neuron i in list)
            Values.Add(new DebugNeuron(i));
      }

      /// <summary>
      /// Handles the CollectionChanged event of the fValues control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
      void fValues_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         OnPropertyChanged("HasValues");
      }

      /// <summary>
      /// Handles the Finished event of all the Processors created by the processor manager.
      /// </summary>
      /// <remarks>
      /// Makes certain that processor objects are removed from the list.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
      void Processor_Finished(object sender, EventArgs e)
      {
         //this function will be called from all kinds of threads, so make certain that we udpate the UI from the UI thread.
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(ProcFinished));
      }

      void ProcFinished()
      {
         if (Owner is SplitItem)
            ((SplitItem)Owner).Children.Remove(this);
         else if (Owner is ProcessorManager)
            ((ProcessorManager)Owner).Processors.Remove(this);
         fToWrap.Finished -= new EventHandler(Processor_Finished);
         if (fToWrap is DebugProcessor)
            ((DebugProcessor)fToWrap).Paused -= new EventHandler(ProcItem_Paused);
      }

      /// <summary>
      /// Handles the Paused event of the ProcItem control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
      void ProcItem_Paused(object sender, EventArgs e)
      {
         if (ProcessorManager.Current.SelectedWatchIndex > -1)
         {
            Variable iVar = ProcessorManager.Current.Watches[ProcessorManager.Current.SelectedWatchIndex].NeuronInfo.Neuron as Variable;
            GetValuesFor(iVar);
         }
      } 
      #endregion
   }
}
