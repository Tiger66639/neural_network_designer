﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using JaStDev.HAB.Designer.BrainData;
using JaStDev.HAB.Expressions;

namespace JaStDev.HAB.Designer.Tools.Debugger
{
   /// <summary>
   /// Interaction logic for ProcessorsOverview.xaml
   /// </summary>
   public partial class ProcessorsOverview : UserControl
   {
      public ProcessorsOverview()
      {
         InitializeComponent();
      }

      /// <summary>
      /// Handles the Click event of the RBtnVariable control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void RBtnVariable_Click(object sender, RoutedEventArgs e)
      {
         NeuronData iData = ((ButtonBase)sender).DataContext as NeuronData;
         if (iData != null)
         {
            Variable iVar = (Variable)iData.Neuron;
            ProcessorManager.Current.DisplayValuesFor(iVar);
         }
      }

      private void ToggleVarsClick(object sender, RoutedEventArgs e)
      {
         TrvValues.ItemTemplate = FindResource("ProcItemTemplate") as DataTemplate;
      }

      private void ToggleProcClick(object sender, RoutedEventArgs e)
      {
         TrvValues.ItemTemplate = FindResource("ProcItemTemplate2") as DataTemplate;
      }

      /// <summary>
      /// Handles the SelectedItemChanged event of the TrvValues control.
      /// </summary>
      /// <remarks>
      /// Keeps the selected item of the <see cref="ProcessorManager"/> in sync.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedPropertyChangedEventArgs&lt;System.Object&gt;"/> instance containing the event data.</param>
      private void TrvValues_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
      {
         ProcessorManager.Current.SelectedProcessor = e.NewValue as ProcItem;
      }
   }
}
