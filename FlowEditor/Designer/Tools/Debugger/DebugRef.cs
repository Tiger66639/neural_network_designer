﻿
namespace JaStDev.HAB.Designer.Tools.Debugger
{
   /// <summary>
   /// Base class for items that represent children of clusters or 'to' parts of links.
   /// </summary>
   public class DebugRef: DebugItem
   {
      #region fields

      DebugNeuron fPointsTo;
      DebugNeuron fOwner;

      #endregion


      #region PointsTo

      /// <summary>
      /// Gets the other side of the link compared to the owner of this object.
      /// </summary>
      public DebugNeuron PointsTo
      {
         get { return fPointsTo; }
         internal set
         {
            fPointsTo = value;
            OnPropertyChanged("PointsTo");
         }
      }

      #endregion

      #region Owner

      /// <summary>
      /// Gets the owner of this <see cref="DebugLink"/>.
      /// </summary>
      public DebugNeuron Owner
      {
         get { return fOwner; }
         internal set { fOwner = value; }                                           //don't need to raise prop changed,  should only be set at start.
      }

      #endregion
   }
}
