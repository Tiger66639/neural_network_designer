﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Lists;
using JaStDev.HAB.Brain.Thread_syncing;
using JaStDev.HAB.Designer.BrainData;
using JaStDev.HAB.Designer.WPF.BrainEventManagers;
using JaStDev.HAB.Events;

namespace JaStDev.HAB.Designer.Tools.Debugger
{
   /// <summary>
   /// A wrapper for the <see cref="Neuron"/> that provides support for the <see cref="INotifyPropertyChanged"/> event
   /// and provides observable collections for the links.
   /// </summary>
   /// <remarks>
   /// Make note: This class registers some event handlers with the brain to check for changes,  which can cause memory
   /// leaks if you don't clean up properly.  For this reason, always call <see cref="DebugNeuron.Dispose"/> when done.
   /// </remarks>
   public class DebugNeuron : DebugItem, INeuronInfo, IWeakEventListener
   {

      Neuron fItem;
      bool fIsTemp;
      NeuronData fNeuronInfo;
      DebugNeuronChildren fLinksOut;
      DebugNeuronChildren fLinksIn;
      DebugNeuronChildren fChildren;
      ObservableCollection<DebugNeuronChildren> fChildLists = new ObservableCollection<DebugNeuronChildren>();


      /// <summary>
      /// Initializes a new instance of the <see cref="DebugNeuron"/> class.
      /// </summary>
      /// <param name="toWrap">To wrap.</param>
      public DebugNeuron(Neuron toWrap)
      {
         if (toWrap.ID == Neuron.TempId)                                                                 //if we wrap a temp neuron, we need to check when it becomes real.
         {
            fIsTemp = true;
            NeuronChangedEventManager.AddListener(Brain.Brain.Current, this);
         }
         else
            fIsTemp = false;
         Item = toWrap;
      }


      #region Item

      /// <summary>
      /// Gets the neuron we provide a wrapper for.
      /// </summary>
      public Neuron Item
      {
         get { return fItem; }
         internal set
         {
            if (fItem != value)
            {
               fItem = value;
               if (fItem != null)
               {
                  if (fItem.ID != Neuron.TempId)
                     NeuronInfo = BrainData.BrainData.Current.NeuronInfo[fItem.ID];
                  else
                     NeuronInfo = new NeuronData(fItem);
                  LinksIn = new DebugNeuronChildren(this, "In");
                  LinksOut = new DebugNeuronChildren(this, "Out");
                  ClusteredBy = new DebugNeuronChildren(this, "ClusteredBy");
                  ChildLists.Add(LinksIn);
                  ChildLists.Add(LinksOut);
                  ChildLists.Add(ClusteredBy);
                  if (value is NeuronCluster)
                  {
                     Children = new DebugNeuronChildren(this, "Children");
                     ChildLists.Add(Children);
                  }
               }
               else
               {
                  NeuronInfo = null;
                  LinksIn = null;
                  LinksOut = null;
                  ChildLists.Clear();
               }
               OnPropertyChanged("Item");
            }
         }
      }

      #endregion

      #region NeuronTypeName

      /// <summary>
      /// Gets the name of the type of neuron we wrap.
      /// </summary>
      /// <remarks>
      /// Used as the tooltip of the image
      /// </remarks>
      public string NeuronTypeName
      {
         get
         {
            if (fItem != null)
               return fItem.GetType().Name;
            else
               return null;
         }
      }

      #endregion

      #region ChildLists

      /// <summary>
      /// Gets the lists of all the Child containers for this object.
      /// </summary>
      /// <remarks>
      /// This is a list containing <see cref="DebugNeuron.LinksIn"/>, <see cref="DebugNeuron.LinksOut"/> and/or
      /// <see cref="DebugNeuron.Children"/>.  It is used to fascilitate the WPF treeview control.
      /// </remarks>
      public ObservableCollection<DebugNeuronChildren> ChildLists
      {
         get { return fChildLists; }
      }

      #endregion

      #region NeuronInfo

      /// <summary>
      /// Gets the extra info that is stored for the neuron in the <see cref="BrainData"/>.
      /// </summary>
      /// <remarks>
      /// We use a field and don't dynamically retrieve this info when we need it cause a DebugNeuron can exist for
      /// temporary neurons (have no valid id yet), which don't have neuronData registered yet.  We solve this by
      /// keeping track of the temp status and using a temp neuron info object.
      /// </remarks>
      public NeuronData NeuronInfo
      {
         get { return fNeuronInfo; }
         internal set
         {
            fNeuronInfo = value;
            OnPropertyChanged("NeuronInfo");
         }
      }

      #endregion

      #region LinksOut

      /// <summary>
      /// Gets the list of outgoing links.
      /// </summary>
      public DebugNeuronChildren LinksOut
      {
         get { return fLinksOut; }
         internal set { fLinksOut = value; }
      }

      #endregion
      
      #region LinksIn

      /// <summary>
      /// Gets the list of incomming links.
      /// </summary>
      public DebugNeuronChildren LinksIn
      {
         get { return fLinksIn; }
         internal set { fLinksIn = value; }
      }

      #endregion

      DebugNeuronChildren fClusteredBy;
      #region ClusteredBy

      /// <summary>
      /// Gets the list of clusters that own this neuron.
      /// </summary>
      public DebugNeuronChildren ClusteredBy
      {
         get { return fClusteredBy; }
         internal set { fClusteredBy = value; }
      }

      #endregion

      #region Children

      /// <summary>
      /// Gets the list of children of this cluster.
      /// </summary>
      public DebugNeuronChildren Children
      {
         get { return fChildren; }
         internal set { fChildren = value; }
      }

      #endregion

      #region IsTemp

      /// <summary>
      /// Gets/sets the if this neuron still has a temp id, or if it has been fully registered with the brain.
      /// </summary>
      public bool IsTemp
      {
         get
         {
            return fIsTemp;
         }
         internal set
         {
            if (fIsTemp != value)
            {
               fIsTemp = value;
               if (fIsTemp == false)                                                               //when turned off, we can stop monitoring the event + need to register the neurondata with the braindata manager.
               {
                  NeuronChangedEventManager.RemoveListener(Brain.Brain.Current, this);
                  BrainData.BrainData.Current.NeuronInfo.AddTemp(NeuronInfo);
               }
               OnPropertyChanged("IsTemp");
            }
         }
      }

      #endregion

      #region functions


      /// <summary>
      /// called when <see cref="DebugNeuron.IsLoaded"/> is changed on any of the <see cref="DebugNeuron"/>s.
      /// </summary>
      /// <param name="list">The list manager that requested the change.</param>
      internal void UpdateMonitorLinks(DebugNeuronChildren list)
      {
         if (list == Children)
         {
            if (ClusteredBy.IsLoaded == false)                                                  //only try to update something if there aren't any clusteredBy lists.
               if (list.IsLoaded == true)
                  NeuronListChangedEventManager.AddListener(Brain.Brain.Current, this);
               else
                  NeuronListChangedEventManager.RemoveListener(Brain.Brain.Current, this);
         }
         else if (list == ClusteredBy)
         {
            if ((Children != null && Children.IsLoaded == false) || Children == null)        //only try to update something if children hasn't been updated already.
            {
               if (list.IsLoaded == true)
                  NeuronListChangedEventManager.AddListener(Brain.Brain.Current, this);
               else
                  NeuronListChangedEventManager.RemoveListener(Brain.Brain.Current, this);
            }
         }
         else
         {
            bool iOldValue;
            bool iNewValue;
            if (list == LinksIn)
            {
               iOldValue = LinksOut.IsLoaded || LinksIn.IsLoaded;
               iNewValue = LinksOut.IsLoaded || !LinksIn.IsLoaded;
            }
            else
            {
               iOldValue = LinksIn.IsLoaded || LinksOut.IsLoaded;
               iNewValue = LinksIn.IsLoaded || !LinksOut.IsLoaded;
            }
            if (iOldValue != iNewValue)
            {
               if (iNewValue == true)
                  LinkChangedEventManager.AddListener(Brain.Brain.Current, this);
               else
                  LinkChangedEventManager.RemoveListener(Brain.Brain.Current, this);
            }
         }
      }

      /// <summary>
      /// Creates the children for the specified list.
      /// </summary>
      /// <param name="list">The list to fill with child elements</param>
      internal void CreateChildrenFor(DebugNeuronChildren list)
      {
         if (Item != null)
         {
            if (list == Children)
            {
               using (ChildrenAccessor iList = ((NeuronCluster)Item).Children)
                  foreach (ulong i in iList)
                     list.Children.Add(new DebugChild(i, this));
            }
            else if (list == LinksIn)
            {
               using (LinksAccessor iLinksIn = Item.LinksIn)
               {
                  foreach (Link i in iLinksIn.Items)
                     list.Children.Add(new DebugLink(i, this));
               }
            }
            else if (list == LinksOut)
            {
               using (LinksAccessor iLinksOut = Item.LinksOut)
               {
                  foreach (Link i in iLinksOut.Items)
                     list.Children.Add(new DebugLink(i, this));
               }
            }
            else if (list == ClusteredBy)
            {
               using (NeuronsAccessor iClusteredBy = Item.ClusteredBy)
               {
                  foreach (ulong i in iClusteredBy.Items)
                     list.Children.Add(new DebugChild(i, this));
               }
            }
            else
               throw new ArgumentOutOfRangeException("list");
         }
      }

      /// <summary>
      /// Determines whether the specified list has children.
      /// </summary>
      /// <param name="list">The list.</param>
      /// <returns>
      /// 	<c>true</c> if the specified list has children; otherwise, <c>false</c>.
      /// </returns>
      internal bool HasChildren(DebugNeuronChildren list)
      {
         if (Item != null)
         {
            if (list == Children)
               using (ChildrenAccessor iList = ((NeuronCluster)Item).Children)
                  return iList.Count > 0;
            else if (list == LinksIn)
            {
               using (LinksAccessor iLinksIn = Item.LinksIn)
                  return iLinksIn.Items.Count > 0;
            }
            else if (list == LinksOut)
            {
               using (LinksAccessor iLinksOut = Item.LinksOut)
                  return iLinksOut.Items.Count > 0;
            }
            else if (list == ClusteredBy)
            {
               using (NeuronsAccessor iClusteredBy = Item.ClusteredBy)
                  return iClusteredBy.Items.Count > 0;
            }
            else
               throw new ArgumentOutOfRangeException("list");
         }
         else
            return false;
      }


      #endregion

      #region IWeakEventListener Members

      /// <summary>
      /// Receives events from the centralized event manager.
      /// </summary>
      /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
      /// <param name="sender">Object that originated the event.</param>
      /// <param name="e">Event data.</param>
      /// <returns>
      /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
      /// </returns>
      public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(LinkChangedEventManager))
         {
            Current_LinkChanged(sender, (LinkChangedEventArgs)e);
            return true;
         }
         else if (managerType == typeof(NeuronListChangedEventManager))
         {
            ChildrenChanged(sender, (NeuronListChangedEventArgs)e);
            return true;
         }
         else if (managerType == typeof(NeuronChangedEventManager))
         {
            NeuronChanged(sender, (NeuronChangedEventArgs)e);
            return true;
         }
         else
            return false;
      }

      /// <summary>
      /// Handles the Neuron changed event.
      /// </summary>
      /// <param name="sender">The sender.</param>
      /// <param name="e">The <see cref="NeuronChangedEventArgs"/> instance containing the event data.</param>
      private void NeuronChanged(object sender, NeuronChangedEventArgs e)
      {
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<NeuronChangedEventArgs>(InternalNeuronChanged), e);
      }

      void InternalNeuronChanged(NeuronChangedEventArgs e)
      {
         if (e.Action == BrainAction.Created && e.NewValue == Item)
            IsTemp = false;
      }

      /// <summary>
      /// Handles the children changed event.
      /// </summary>
      /// <param name="sender">The sender.</param>
      /// <param name="e">The <see cref="NeuronListChangedEventArgs"/> instance containing the event data.</param>
      private void ChildrenChanged(object sender, NeuronListChangedEventArgs e)
      {
         if (e.ListType == typeof(ChildList))
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<NeuronListChangedEventArgs>(InternalChildrenChanged), e);
         else
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<NeuronListChangedEventArgs>(InternalClusteredByChanged), e);
      }

      /// <summary>
      /// Internal handler for the children changed event.
      /// </summary>
      /// <param name="e">The <see cref="NeuronListChangedEventArgs"/> instance containing the event data.</param>
      void InternalChildrenChanged(NeuronListChangedEventArgs e)
      {
         if (Children != null)
         {
            if (Children.IsLoaded == true)
            {
               DebugRef iFound = (from i in Children.Children where i.PointsTo != null && i.PointsTo.Item == e.Item select i).FirstOrDefault();
               switch (e.Action)
               {
                  case NeuronListChangeAction.Insert:
                     if (iFound == null)
                        Children.Children.Insert(e.Index, new DebugChild(e.Item, this));
                     break;
                  case NeuronListChangeAction.Remove:
                     if (iFound != null)
                        Children.Children.Remove(iFound);
                     break;
                  default:
                     break;
               }
            }
            else
               Children.UpdateHasChildren();
         }
      }

      /// <summary>
      /// Internal handler for the Clusteredby changed event (is ChildrenChanged, but from the other perspective).
      /// </summary>
      /// <param name="e">The <see cref="NeuronListChangedEventArgs"/> instance containing the event data.</param>
      void InternalClusteredByChanged(NeuronListChangedEventArgs e)
      {
         if (ClusteredBy.IsLoaded == true && e.Item == Item)
         {
            DebugRef iFound;
            using (NeuronsAccessor iList = e.List)
            {
               iFound = (from i in ClusteredBy.Children
                         where i.PointsTo != null && ((NeuronCluster)i.PointsTo.Item).Children == iList
                         select i).FirstOrDefault();
            }
            switch (e.Action)
            {
               case NeuronListChangeAction.Insert:
                  if (iFound == null)
                     Children.Children.Insert(e.Index, new DebugChild(e.Item, this));
                  break;
               case NeuronListChangeAction.Remove:
                  if (iFound != null)
                     Children.Children.Remove(iFound);
                  break;
               default:
                  break;
            }
         }
         else
            ClusteredBy.UpdateHasChildren();

      }

      /// <summary>
      /// Called when a link is changed in te brain.  We check if <see cref="DebugNeuron.Item"/> is involved.
      /// </summary>
      void Current_LinkChanged(object sender, LinkChangedEventArgs e)
      {
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<LinkChangedEventArgs>(InternalLinkChanged), e);
      }

      /// <summary>
      /// Internal handler for the children changed event.
      /// </summary>
      /// <param name="e">The <see cref="LinkChangedEventArgs"/> instance containing the event data.</param>
      void InternalLinkChanged(LinkChangedEventArgs e)
      {
         switch (e.Action)
         {
            case BrainAction.Created:
               UpdateNew(e);
               break;
            case BrainAction.Changed:
               UpdateOld(e);
               UpdateNew(e);
               break;
            case BrainAction.Removed:
               UpdateOld(e);
               break;
            default:
               break;
         }
      }

      /// <summary>
      /// Updates the linksIn and LinksOut data when something was added
      /// </summary>
      /// <param name="e">The <see cref="LinkChangedEventArgs"/> instance containing the event data.</param>
      private void UpdateOld(LinkChangedEventArgs e)
      {
         Link sender = e.OriginalSource;
         if (e.OldFrom == fItem.ID)
         {
            if (LinksOut != null)
            {
               if (LinksOut.IsLoaded == true)
               {
                  DebugRef iFound = (from i in LinksOut.Children where ((DebugLink)i).Item == sender select i).FirstOrDefault();
                  if (iFound != null)
                     LinksOut.Children.Remove(iFound);
               }
               else
                  LinksOut.UpdateHasChildren();
            }
         }
         else if (e.OldTo == fItem.ID)
         {
            if (LinksIn != null)
            {
               if (LinksOut.IsLoaded == true)
               {
                  DebugRef iFound = (from i in LinksIn.Children where ((DebugLink)i).Item == sender select i).FirstOrDefault();
                  if (iFound != null)
                     LinksIn.Children.Remove(iFound);
               }
               else
                  LinksIn.UpdateHasChildren();
            }
         }
      }

      /// <summary>
      /// Updates the linksIn and LinksOut data when something was added
      /// </summary>
      /// <param name="e">The <see cref="LinkChangedEventArgs"/> instance containing the event data.</param>
      private void UpdateNew(LinkChangedEventArgs e)
      {
         Link sender = e.OriginalSource;
         if (e.NewFrom == fItem.ID)
         {
            if (LinksOut != null)
            {
               if (LinksOut.IsLoaded == true)
               {
                  DebugRef iFound = (from i in LinksOut.Children where ((DebugLink)i).Item == sender select i).FirstOrDefault();
                  if (iFound == null)
                     LinksOut.Children.Add(new DebugLink(sender, this));
               }
               else
                  LinksOut.UpdateHasChildren();
            }
         }
         else if (e.NewTo == fItem.ID)
         {
            if (LinksIn != null)
            {
               if (LinksIn.IsLoaded == true)
               {
                  DebugRef iFound = (from i in LinksIn.Children where ((DebugLink)i).Item == sender select i).FirstOrDefault();
                  if (iFound == null)
                     LinksIn.Children.Add(new DebugLink(sender, this));
               }
               else
                  LinksIn.UpdateHasChildren();
            }
         }
      }

      #endregion

     
   }
}
