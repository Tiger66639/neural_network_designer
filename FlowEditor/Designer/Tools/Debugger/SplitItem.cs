﻿using JaStDev.HAB.Expressions;

namespace JaStDev.HAB.Designer.Tools.Debugger
{
   /// <summary>
   /// When a processor is split, we use this type to manage all the <see cref="ProcItem"/>s from the split.
   /// </summary>
   public class SplitItem: ProcItem
   {

      #region Fields
      ObservedCollection<ProcItem> fChildren;  

      #endregion

      #region ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="SplitItem"/> class.
      /// </summary>
      public SplitItem(): base(null)
      {
         fChildren = new ObservedCollection<ProcItem>(this);  
      }

      #endregion

      #region Children

      /// <summary>
      /// Gets the child processors of this one (resulting from a split).
      /// </summary>
      public ObservedCollection<ProcItem> Children
      {
         get { return fChildren; }
      }

      #endregion

      #region IsSplit

      /// <summary>
      /// Gets a value indicating whether this instance is split for several other processor items.
      /// </summary>
      /// <value><c>true</c> if this instance is split; otherwise, <c>false</c>.</value>
      public override bool IsSplit
      {
         get { return true; }
      }

      #endregion

      #region Function

      /// <summary>
      /// Assigns the value(s) stored in the specified variable (for this processor) to <see cref="ProcItem.Values"/>
      /// </summary>
      /// <param name="toDisplay">To display.</param>
      public override void GetValuesFor(Variable toDisplay)
      {
         foreach (ProcItem i in Children)
            i.GetValuesFor(toDisplay);
      }

      #endregion
   }
}
