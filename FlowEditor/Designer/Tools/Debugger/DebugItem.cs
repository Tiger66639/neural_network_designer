﻿namespace JaStDev.HAB.Designer.Tools.Debugger
{
   /// <summary>
   /// Base class for a single debug item in  a <see cref="DebugProcessor"/>.  This is either a link
   /// or a neuron.
   /// </summary>
   /// <remarks>
   /// </remarks>
   public abstract class DebugItem : ObservableObject
   {
   }
}
