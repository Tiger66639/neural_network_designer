﻿using System.Collections.Generic;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.BrainData;
using JaStDev.HAB.Expressions;
using JaStDev.HAB.Processor;

namespace JaStDev.HAB.Designer.Tools.Debugger
{
   /// <summary>
   /// Contains the data of a function that is active in a <see cref="Processor"/>.
   /// </summary>
   /// <remarks>
   /// <para>
   /// This contains:
   /// - the neuron who's code is being executed (usually the '<see cref="Link.Meaning"/>' 
   /// part of a link.
   /// - The code list being executed.
   /// - the current (or last) pos 
   /// </para>
   /// <para>
   /// Because there are a lot of calls to other execution list being made, we need to
   /// keep track of the different values.  That's what this class does.
   /// </para>
   /// </remarks>
   public class ExecutionFrame: ObservableObject, INeuronInfo, INeuronWrapper
   {

      #region Fields
      int fPosition;
      IList<Expression> fCode;
      Neuron fItem;
      ExecListType fCodeListType; 

      #endregion

      #region ctor

      public ExecutionFrame(IList<Expression> code, Neuron item, ExecListType codeListType)
      {
         fCode = code;
         fItem = item;
         fCodeListType = codeListType;
      }

      #endregion

      #region Prop

      #region Position

      /// <summary>
      /// Gets/sets the current position with the code list.
      /// </summary>
      public int Position
      {
         get
         {
            return fPosition;
         }
         internal set
         {
            fPosition = value;
            OnPropertyChanged("Position");
         }
      }

      #endregion


      #region Code

      /// <summary>
      /// Gets the list of execution items being performed by the processor.
      /// </summary>
      public IList<Expression> Code
      {
         get { return fCode; }
      }

      #endregion

      #region Item

      /// <summary>
      /// Gets the Neuron who's code list is being executed.
      /// </summary>
      public Neuron Item
      {
         get { return fItem; }
      }

      #endregion

      #region CodeListType

      /// <summary>
      /// Gets the Neuron that identifies the type of list the code list is for the <see cref="ExecutionFrame.Item"/>.
      /// Possible values: Rules, Actions, Children.
      /// </summary>
      /// <remarks>
      /// A string is easy to display.
      /// </remarks>
      public ExecListType CodeListType
      {
         get { return fCodeListType; }
      }

      #endregion 

      #endregion

      #region INeuronInfo Members

      /// <summary>
      /// Gets the extra info for the specified neuron.  Can be null.
      /// </summary>
      /// <value></value>
      public NeuronData NeuronInfo
      {
         get
         {
            return BrainData.BrainData.Current.NeuronInfo[Item.ID];
         }
      }

      #endregion
   }
}
