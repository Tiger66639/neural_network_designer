﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Threading;
using System.Windows.Threading;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Expressions;
using JaStDev.HAB.Processor;

namespace JaStDev.HAB.Designer.Tools.Debugger
{
   

   /// <summary>
   /// The different types of debug mode available.
   /// </summary>
   public enum DebugMode
   {
      /// <summary>
      /// No debuging is done, no events are send about the state of the processor.
      /// </summary>
      Off,
      /// <summary>
      /// All events are raised and the debugger stops execution at every step.
      /// </summary>
      Normal,
      /// <summary>
      /// All events are raised and the debugger pauses for a short while before executing the next step.
      /// </summary>
      SlowMotion
   }

   /// <summary>
   /// A <see cref="Processor"/> that provides debug information.
   /// </summary> 
   /// <remarks>
   /// Each processor provides its own blocking mechanism.  So debugging is always at the level of
   /// 1 processor, no jumps between threads.  This allows you to debug a single process step after
   /// step much easier without jumping from one process to the other.
   /// </remarks>
   public class DebugProcessor: Processor.Processor, INotifyPropertyChanged
   {
      #region Fields


      DebugMode fDebugMode;                                                                           //value retrieved on ctor, fixed for entire process run, managed by application settings.
      EventWaitHandle fBreakSwitch;                                                                      //used for step processing, so we can wait for the ui thread to signal a continue..
      bool fBreakNextStep = false;                                                                    //switch used for a step to next expression.
      static Timer fTimer;                                                                            //used for auto play debug mode.
      ObservableCollection<DebugProcessor> fSubProcessors = new ObservableCollection<DebugProcessor>();
      ObservableCollection<DebugNeuron> fStack = new ObservableCollection<DebugNeuron>();
      ObservableCollection<ExecutionFrame> fExecutionFrames = new ObservableCollection<ExecutionFrame>();
      bool fIsPaused;
      bool fIsRunning = true;
      #endregion

      #region ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="DebugProcessor"/> class.
      /// </summary>
      public DebugProcessor()
      {
         fDebugMode = BrainData.BrainData.Current.Debugmode;
         if (fDebugMode != DebugMode.Off)
         {
            if (fDebugMode == DebugMode.SlowMotion)
            {
               fBreakSwitch = new AutoResetEvent(true);                                               //for a slow motion, we use an autoreset event, so we don't have to reset after each frame.
               if (fTimer == null)
               {
                  int iMSec = (int)ProcessorManager.Current.PlaySpeed.TotalMilliseconds;
                  fTimer = new Timer(new TimerCallback(OnTimer), null, iMSec, iMSec);
               }
            }else
               fBreakSwitch = new ManualResetEvent(true);                                             //we use manual in all other sits.
         }
         fStack.CollectionChanged += new NotifyCollectionChangedEventHandler(fStack_CollectionChanged);
      }

      /// <summary>
      /// Initializes the <see cref="DebugProcessor"/> class so that everything is set up for debugging.
      /// </summary>
      static public void Init()
      {
         Log.Log.Default.PreviewWriteToLog += new LogItemEventHandler(Log_PreviewWriteToLog);         //we use this event cause this is triggered in the same thread as where the log happened, which we need to check if there is a processor present in the thread.
      }

      #endregion

      #region Events

      /// <summary>
      /// Occurs when the processor is paused because of a breakpoint or new step in a slowmotion.
      /// </summary>
      public event EventHandler Paused;

      #endregion

      #region Prop

      #region ExecutionFrames

      /// <summary>
      /// Gets the list of execution frames for this processor.
      /// </summary>
      /// <remarks>
      /// An <see cref="ExecutionFrame"/> contains all the info of 1
      /// function that is called.  Since a function can call another function, we must keep
      /// a stack of all currently called functions, hence this stack.
      /// </remarks>
      public ObservableCollection<ExecutionFrame> ExecutionFrames
      {
         get { return fExecutionFrames; }
      }

      #endregion

      #region StackDisplay

      /// <summary>
      /// Gets the list of stack items currently used by the processor.
      /// </summary>
      /// <remarks>
      /// This is an observable collection so that the GUI can properly work with it. The actual stack is
      /// managed from another thread.
      /// </remarks>
      public ObservableCollection<DebugNeuron> StackDisplay
      {
         get { return fStack; }
      }

      #endregion

      #region StackSize

      /// <summary>
      /// Gets/sets the number of items currently on the stack.
      /// </summary>
      /// <remarks>
      /// This is used to display in WPF.
      /// </remarks>
      public int StackSize
      {
         get
         {
            return fStack.Count;
         }
      }

      #endregion

      #region IsPaused

      /// <summary>
      /// Indicates if the debugger is curently waiting for the user to continue the execution.
      /// </summary>
      /// <remarks>
      /// This property is set from within a different thread, but property change and UI update is handled
      /// on the UI thread.
      /// </remarks>
      public bool IsPaused
      {
         get
         {
            return fIsPaused;
         }
         internal set
         {
            fIsPaused = value;
            if (value == true)
               OnPaused();
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<string>(OnPropertyChanged), "IsPaused");
         }
      }

      #endregion

      #region IsRunning

      /// <summary>
      /// Gets/sets the the processor is running or not.
      /// </summary>
      public bool IsRunning
      {
         get
         {
            return fIsRunning;
         }
         set
         {
            fIsRunning = value;
            OnPropertyChanged("IsRunning");
         }
      }

      #endregion

      #region SubProcessors

      /// <summary>
      /// Gets the list of sub processors of this processor.
      /// </summary>
      public ObservableCollection<DebugProcessor> SubProcessors
      {
         get { return fSubProcessors; }
      }

      #endregion

      #region CurrentLink
      /// <summary>
      /// The link we are currently trying to solve.
      /// </summary>
      public override int CurrentLink
      {
         get
         {
            return base.CurrentLink;
         }
         set
         {
            base.CurrentLink = value;
            OnPropertyChanged("CurrentLink");                                          //this should be thread save cause property changed works accross threads.
         }
      } 
      #endregion

      #endregion

      #region functions

      /// <summary>
      /// Handles the CollectionChanged event of the fStack control.
      /// </summary>
      /// <remarks>
      /// When the collection changes, we need to raise the event to indicate that the stacksize is also changed.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
      void fStack_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         OnPropertyChanged("StackSize");
      }

      /// <summary>
      /// Called when the processor is paused. Triggers the event.
      /// </summary>
      protected virtual void OnPaused()
      {
         if (Paused != null)
            Paused(this, EventArgs.Empty);
      }

      /// <summary>
      /// signals the debug processor that the next execution step can be performed.
      /// </summary>
      /// <remarks>
      /// This simply sets an auto event.
      /// </remarks>
      public void DebugContinue()
      {
         if (fBreakSwitch != null)
            fBreakSwitch.Set();
      }

      public void DebugPause()
      {
         if (fBreakSwitch != null)
            fBreakSwitch.Reset();
      }

      public void DebugStepNext()
      {
         if (fBreakSwitch != null)
         {
            fBreakNextStep = true;
            fBreakSwitch.Set();
         }
      }

      void OnTimer(object timer)
      {
         fBreakSwitch.Set();
      }

      /// <summary>
      /// Handles the PreviewWriteToLog event of the Log control.
      /// </summary>
      /// <remarks>
      /// This checks if there is a processor defined in the thread that changes the list.  If so, the log item is comming from the
      /// execution engine, so attach a tag to it that allows us to find the neuron again that triggered the log item.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="JaStDev.Log.LogItemEventArgs"/> instance containing the event data.</param>
      /// <returns>True, indicating that the log should always happen.</returns>
      static bool Log_PreviewWriteToLog(object sender, LogItemEventArgs e)
      {
         if (CurrentProcessor != null)
         {
            LogDebugData iLogData = new LogDebugData();
            iLogData.Executed = CurrentProcessor.CurrentExpression;
            iLogData.ExecListType = CurrentProcessor.CurrentExecListType;
            iLogData.Solved = CurrentProcessor.NeuronToSolve;
            iLogData.Meaning = CurrentProcessor.CurrentMeaning;
            iLogData.ExecutionSource = CurrentProcessor.CurrentExecSource;
            e.Item.Tag = iLogData;
         }
         return true;
      }

      #endregion

      #region Overwrites

      /// <summary>
      /// Tries to process the list of neurons.
      /// </summary>
      /// <param name="toExec">The neuron who's code needs executing.  This is not used in this class but is provided
      /// for inheritters, so they can do something with it.</param>
      /// <param name="toProcess"></param>
      /// <param name="listType"></param>
      /// <remarks>
      /// Don't init the pos, this way it can be used to start from an arbitrary pos like with SolveNeuronFrom.
      /// </remarks>
      protected override void Process(Neuron toExec, IList<Expression> toProcess, ExecListType listType)
      {
         if (fDebugMode != DebugMode.Off)
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Input, new Action<Neuron, IList<Expression>, ExecListType>(InternalProcess), toExec, new object[] { toProcess, listType });
         base.Process(toExec, toProcess, listType);
      }

      /// <summary>
      /// Need to update the execution frame stack.
      /// </summary>
      /// <param name="toExec"></param>
      /// <param name="toProcess"></param>
      /// <param name="listType"></param>
      void InternalProcess(Neuron toExec, IList<Expression> toProcess, ExecListType listType)
      {
         ExecutionFrame iFrame = new ExecutionFrame(toProcess, toExec, listType);
         fExecutionFrames.Add(iFrame);
      }

      /// <summary>
      /// If there is a wait event, it means we need to block execution untill we get a signal to continue.
      /// </summary>
      /// <param name="toProcess"></param>
      protected override void Process(Expression toProcess)
      {
         if (fBreakSwitch != null)
         {
            lock (BrainData.BrainData.Current.BreakPoints)                                                                             //we need a lock cause an add/remove can be done from another thread.
            {
               if (BrainData.BrainData.Current.BreakPoints.Contains(toProcess) == true)                                          //if this is a breakpoint, we always need to pause processing.
               {
                  BrainData.BrainData.Current.BreakPoints.OnBreakPointReached(toProcess, this);
                  fBreakSwitch.Reset();
               }
            }
            IsPaused = true;                                                                                                  //just before we pause, let the ui know.
            fBreakSwitch.WaitOne();
            IsPaused = false;
         }
         base.Process(toProcess);
         if (fBreakNextStep == true && fBreakSwitch != null)
         {
            fBreakSwitch.Reset();
            fBreakNextStep = false;
         }
      }

      /// <summary>
      /// Call this method if you want to remove something from the internal stack.
      /// for internal use only.
      /// </summary>
      /// <returns></returns>
      /// <remarks>
      /// This method only removes the item from the stack.  It is provided so that
      /// inheriters can do something more during a stack change, like let observers know.
      /// </remarks>
      public override Neuron Pop()
      {
         if (fDebugMode != DebugMode.Off)
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(InternalPopNeuron));
         return base.Pop();
      }

      /// <summary>
      /// removes the last item from the observable colection. This is so it can be done from the UI thread.
      /// </summary>
      void InternalPopNeuron()
      {
         if (fStack.Count > 0)
            fStack.RemoveAt(fStack.Count - 1);
      }

      /// <summary>
      /// Pushes the specified neuron on it's internal stack.
      /// </summary>
      /// <param name="toPush">The neuron to add.</param>
      public override void Push(Neuron toPush)
      {
         if (fDebugMode != DebugMode.Off)
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<Neuron>(InternalPushNeuron), toPush);
         base.Push(toPush);
      }

      void InternalPushNeuron(Neuron toPush)
      {
         fStack.Add(new DebugNeuron(toPush));
      }

      /// <summary>
      /// Returns an array of the specified size, filled with processors.
      /// </summary>
      /// <param name="count">The nr of processors to create.</param>
      /// <returns>An array of newly created processors (not initialized).</returns>
      /// <remarks>
      /// Descendents should reimplement this so they can give another type (for debugging).
      /// </remarks>
      public override Processor.Processor[] CreateProcessors(int count)
      {
         Processor.Processor[] iRes = new Processor.Processor[count];
         while (count > 0)
         {
            DebugProcessor iSub = new DebugProcessor();
            iRes[count - 1] = iSub;                                                 //-1 cause the index is 1 less than the size.
            fSubProcessors.Add(iSub);                                               //a processor is always created for subprocessors, so we can store them here.
            count--;
         }
         return iRes;
      }


      /// <summary>
      /// Tries to solve the specified neuron.
      /// </summary>
      /// <param name="toSolve"></param>
      /// <remarks>
      /// 	<para>
      /// Solving is done by walking down every 'To' link in the neuron and pushing this to reference on the stack after which it calls
      /// the <see cref="Link.Meaning"/>'s <see cref="Neuron.Actions"/> list.
      /// </para>
      /// 	<para>
      /// this is an internal function, cause the output instruction needs to be able to let a neuron be solved
      /// sequential (<see cref="Processor.Solve"/> is async and solves the intire stack.
      /// </para>
      /// </remarks>
      protected override void SolveNeuron(Neuron toSolve)
      {
         if (fDebugMode != DebugMode.Off)
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<Neuron>(InternalSolveNeuron), toSolve);
         base.SolveNeuron(toSolve);
      }

      /// <summary>
      /// When the neuron being solved changed, we need to raise the PropertyChanged event so that the view can update.
      /// </summary>
      /// <param name="toSolve">Not really needed.</param>
      void InternalSolveNeuron(Neuron toSolve)
      {
         OnPropertyChanged("NeuronToSolve");
      }

      protected override void OnFinished()
      {
         base.OnFinished();
         IsRunning = false;
      }

      #endregion

      #region INotifyPropertyChanged Members

      public event PropertyChangedEventHandler PropertyChanged;

      /// <summary>
      /// Raises the <see cref="DebugProcessor.PropertyChanged"/> event.
      /// </summary>
      /// <param name="name"></param>
      protected virtual void OnPropertyChanged(string name)
      {
         if (PropertyChanged != null)
            PropertyChanged(this, new PropertyChangedEventArgs(name));
      }

      #endregion




      /// <summary>
      /// Updates the debug timer used for the slowmotion.
      /// </summary>
      /// <param name="time">The new time to use for slow motion.</param>
      internal static void UpdateDebugTimer(int time)
      {
         if (fTimer != null)
            fTimer.Change(time, time);
      }
   }
}
