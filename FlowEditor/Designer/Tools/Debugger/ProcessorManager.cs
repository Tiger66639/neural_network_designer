﻿using System;
using System.Collections.Specialized;
using System.Windows.Threading;
using JaStDev.HAB.Expressions;
using JaStDev.HAB.Processor;
using JaStDev.HAB.Sins;

namespace JaStDev.HAB.Designer.Tools.Debugger
{
   /// <summary>
   /// This class stores references to all the running processors in order to provide data for them that can be
   /// used in the designer. Also manages the creation of processors for the designer.
   /// </summary>
   /// <remarks>
   /// This is an entry point through the stati <see cref="ProcessorManager.Current"/>.
   /// </remarks>
   public class ProcessorManager : ObservableObject, IProcessorFactory
   {

      #region Fields
      ProcItem fSelectedProcessor;
      static ProcessorManager fCurrent = new ProcessorManager();
      ObservedCollection<ProcItem> fProcessors;
      int fSelectedWatch = -1;
      #endregion


      #region Ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="ProcessorManager"/> class.
      /// </summary>
      public ProcessorManager()
      {
         fProcessors = new ObservedCollection<ProcItem>(this);
         fProcessors.CollectionChanged += new NotifyCollectionChangedEventHandler(fProcessors_CollectionChanged);
         BrainData.BrainData.Current.AfterLoad += new EventHandler(Current_AfterLoad);
      }


      #endregion

      #region Prop

      #region Current

      /// <summary>
      /// Gets the current processor manager.
      /// </summary>
      static public ProcessorManager Current
      {
         get { return fCurrent; }
      }

      #endregion 

      #region Processors

      /// <summary>
      /// Gets the list of processors currently running.  These are 'root' processors, splits are stored in the proc items.
      /// </summary>
      public ObservedCollection<ProcItem> Processors
      {
         get { return fProcessors; }
      }

      #endregion

      #region SelectedProcessor

      /// <summary>
      /// Gets the curently selected processor item.
      /// </summary>
      /// <remarks>
      /// Is automaically set when the selection is changed on the <see cref="ProcessorsOverview"/> control.
      /// </remarks>
      public ProcItem SelectedProcessor
      {
         get { return fSelectedProcessor; }
         internal set { fSelectedProcessor = value; }
      }

      #endregion

      #region Watches

      /// <summary>
      /// Gets the list of variables which should be displayed in a short list for selecting to monitor.
      /// </summary>
      public ObservedCollection<Watch> Watches
      {
         get 
         {
            if (BrainData.BrainData.Current.DesignerData != null)
               return BrainData.BrainData.Current.DesignerData.Watches;
            else
               return null;
         }
      }

      #endregion

      #region SelectedWatchIndex

      /// <summary>
      /// Gets/sets the index of the selected variable that needs to be monitored.
      /// </summary>
      public int SelectedWatchIndex
      {
         get
         {
            return fSelectedWatch;
         }
         set
         {
            if (value != fSelectedWatch)
            {
               if (value >= -1 && value < Watches.Count)
               {
                  fSelectedWatch = value;
                  OnPropertyChanged("SelectedWatchIndex");
                  if(value > -1)
                  DisplayValuesFor(Watches[value].NeuronInfo.Neuron as Variable);
                  else
                     DisplayValuesFor(null);
               }
               else
                  throw new IndexOutOfRangeException();
            }
         }
      }

      #endregion


      #region PlaySpeed

      /// <summary>
      /// Gets/sets the speed at which processor steps are played during a slow motion debug mode.
      /// </summary>
      public TimeSpan PlaySpeed
      {
         get
         {
            if (BrainData.BrainData.Current.DesignerData != null)
               return BrainData.BrainData.Current.DesignerData.PlaySpeed;
            return TimeSpan.MinValue;
         }
         set
         {
            if (BrainData.BrainData.Current.DesignerData != null)
            {
               BrainData.BrainData.Current.DesignerData.PlaySpeed = value;
               DebugProcessor.UpdateDebugTimer(value.Milliseconds);
               OnPropertyChanged("PlaySpeed");
               OnPropertyChanged("PlaySpeedMSec");
            }
            else
               throw new InvalidOperationException("No data file loaded.");
         }
      }

      #endregion

      #region PlaySpeedMSec
      /// <summary>
      /// Gets or sets the play speed in milli seconds.
      /// </summary>
      /// <value>The play speed M sec.</value>
      public int PlaySpeedMSec
      {
         get { return PlaySpeed.Milliseconds; }
         set
         {
            PlaySpeed = new TimeSpan(0, 0, 0, 0, value);
         }
      } 
      #endregion

      #region HasProcessors

      /// <summary>
      /// Gets if there are curently processors alive.
      /// </summary>
      public bool HasProcessors
      {
         get { return Processors.Count > 0; }
      }

      #endregion


      #endregion

      #region Functions

      /// <summary>
      /// Creates a <see cref="Processor"/> that can be used in calls to <see cref="TextSin.Process"/> or other methods
      /// that start a main input processing thread.
      /// </summary>
      /// <remarks>
      /// This method creates a <see cref="DebugProcessor"/> so that the user can follow the translation process.  It is
      /// also added to the <see cref="BraindData.Processors"/> list so it can be depicted.
      /// Only when we are in debug mode, is a debug processor used and displayed, otherwise it's a regular.
      /// <para>
      /// Can be called from all kinds of threads, so all UI interaction must be done async.
      /// </para>
      /// </remarks>
      /// <returns></returns>
      public Processor.Processor GetProcessor()
      {
         Processor.Processor iRes;
         if (BrainData.BrainData.Current.Debugmode == DebugMode.Off)
            iRes = new Processor.Processor();
         else
         {
            DebugProcessor iProcessor = new DebugProcessor();
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<object>(BrainData.BrainData.Current.OpenDocuments.Add), iProcessor);
            iRes = iProcessor;
         }
         App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<ProcItem>(Processors.Add), new ProcItem(iRes));
         return iRes;
      }


      /// <summary>
      /// Asks each <see cref="ProcItem"/> in <see cref="ProcessorManager.Processors"/> to calculate and display the current
      /// value(s) for the specified variable.
      /// </summary>
      /// <param name="toDisplay">Variable to display values for.</param>
      public void DisplayValuesFor(Variable toDisplay)
      {
         foreach (ProcItem i in Processors)
            i.GetValuesFor(toDisplay);
      }

      /// <summary>
      /// Updates the selected variable value
      /// </summary>
      internal void UpdateSelectedVariable()
      {
         if (SelectedWatchIndex > -1)
            DisplayValuesFor(Watches[SelectedWatchIndex].NeuronInfo.Neuron as Variable);
      }


      #endregion


      #region Event handlers

      /// <summary>
      /// Handles the AfterLoad event of the Current BrainData.
      /// </summary>
      /// <remarks>
      /// When data is loaded, need to raise property changed so that everything gets updated.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
      void Current_AfterLoad(object sender, EventArgs e)
      {
         OnPropertyChanged("Watches");
         OnPropertyChanged("PlaySpeed");
         OnPropertyChanged("PlaySpeedMSec");
      }

      /// <summary>
      /// Handles the CollectionChanged event of the fProcessors control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Collections.Specialized.NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
      void fProcessors_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         OnPropertyChanged("HasProcessors");
      }

      #endregion


   }
}
