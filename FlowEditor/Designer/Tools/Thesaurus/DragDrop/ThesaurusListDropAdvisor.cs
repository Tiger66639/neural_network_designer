﻿using System.Windows;
using System.Windows.Controls;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.DragDrop;

namespace JaStDev.HAB.Designer.Tools.Thesaurus.DragDrop
{
   /// <summary>
   /// Drop advisor for an ItemsControl containing thesaurus elements.
   /// </summary>
   public class ThesaurusListDropAdvisor : DropTargetBase
   {
      #region prop
      #region Target

      public ItemsControl Target
      {
         get
         {
            return TreeHelper.FindInTree<ItemsControl>(TargetUI);
         }
      }

      #endregion

      #region UsePreviewEvents
      /// <summary>
      /// Gets if the preview event versions should be used or not.
      /// </summary>
      /// <remarks>
      /// don't use preview events cause than the sub lists don't get used but only the main list cause this gets the events first,
      /// while we usually want to drop in a sublist.
      /// </remarks>
      public override bool UsePreviewEvents
      {
         get
         {
            return false;
         }
      }
      #endregion

      #region Items

      /// <summary>
      /// Gets the list containing all the code that the UI to which advisor is attached too, displays data for.
      /// </summary>
      public ObservedCollection<ThesaurusItem> Items
      {
         get
         {
            return Target.ItemsSource as ObservedCollection<ThesaurusItem>;
         }
      }

      #endregion
      #endregion

      #region Overrides
      public override void OnDropCompleted(DragEventArgs arg, Point dropPoint)
      {
         ThesaurusItem iOwner = Target.DataContext as ThesaurusItem;                            //we need to make a link between the parent item and the dropped item, so we need to know the already existing item.
         if (iOwner != null)
         {
            ulong iId = (ulong)arg.Data.GetData(Properties.Resources.NeuronIDFormat);
            Neuron iDropped = Brain.Brain.Current[iId];

            var iFound = (from i in Items
                          where
                             i.Item.ID == iId
                          select i).FirstOrDefault();
            if (iFound == null)                                //we check that we don't want to add an item 2 times to the same list.
               WordNetSin.Default.CreateRelationship(iDropped, iOwner.Item, iOwner.Root.SelectedRelationship);
         }
      }


      public override bool IsValidDataObject(IDataObject obj)
      {
         return Items != null && obj.GetDataPresent(Properties.Resources.NeuronIDFormat);
      }

      //public override DragDropEffects GetEffect(DragEventArgs e)
      //{
      //   DragDropEffects iRes = base.GetEffect(e);
      //   if (iRes == DragDropEffects.Move)
      //   {
      //      CodeItem iItem = e.Data.GetData(Properties.Resources.CodeItemFormat) as CodeItem;
      //      if (iItem != null && CodeList.Contains(iItem) == true)
      //         return DragDropEffects.Copy;                                                                 //when we move on the same list, the drag source doesn't have to do anything.
      //   }
      //   return iRes;
      //}

      #endregion
   }
}
