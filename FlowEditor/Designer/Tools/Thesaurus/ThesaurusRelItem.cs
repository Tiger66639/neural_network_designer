﻿using System.Diagnostics;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.BrainData;

namespace JaStDev.HAB.Designer.Tools.Thesaurus
{
   public class ThesaurusRelItem : OwnedObject<ThesaurusItem>, INeuronWrapper, INeuronInfo
   {
      Neuron fToWrap;

      /// <summary>
      /// Initializes a new instance of the <see cref="SynonymItem"/> class.
      /// </summary>
      /// <param name="toWrap">To wrap.</param>
      public ThesaurusRelItem(Neuron toWrap)
      {
         Debug.Assert(toWrap != null);
         fToWrap = toWrap;
      }

      #region Item (INeuronWrapper Members)

      /// <summary>
      /// Gets the item.
      /// </summary>
      /// <value>The item.</value>
      public Neuron Item
      {
         get { return fToWrap; }
      }

      #endregion

      #region NeuronInfo (INeuronInfo Members)

      /// <summary>
      /// Gets the extra info for the specified neuron.  Can be null.
      /// </summary>
      /// <value></value>
      public NeuronData NeuronInfo
      {
         get 
         {
            if (fToWrap != null)
               return BrainData.BrainData.Current.NeuronInfo[fToWrap.ID];
            else
               return null;
         }
      }

      #endregion
   }
}
