﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.BrainData;

namespace JaStDev.HAB.Designer.Tools.Thesaurus
{
   /// <summary>
   /// A collection class that manages the cluster with all the synonyms of a thesaurus item.
   /// </summary>
   public class SynonymsCollection: ClusterCollection<SynonymItem>
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="SynonymsCollection"/> class.
      /// </summary>
      /// <param name="owner">The owner.</param>
      /// <param name="list">The list.</param>
      public SynonymsCollection(INeuronWrapper owner, NeuronCluster list): base(owner, list)
      {

      }

      /// <summary>
      /// Initializes a new instance of the <see cref="SynonymsCollection"/> class.
      /// </summary>
      /// <param name="owner">The owner.</param>
      /// <param name="meaning">The meaning.</param>
      public SynonymsCollection(INeuronWrapper owner, ulong meaning): base(owner, meaning, meaning)
      {

      }

      /// <summary>
      /// Called when a new wrapper object needs to be created for a neuron.
      /// </summary>
      /// <param name="toWrap">To wrap.</param>
      /// <returns></returns>
      /// <remarks>
      /// CodeEditors do: return CodeEditor.CreateCodeItemFor(toWrap)
      /// </remarks>
      public override SynonymItem GetWrapperFor(Neuron toWrap)
      {
         return new SynonymItem(toWrap);
      }

      protected override ulong GetListMeaning(ulong linkMeaning)
      {
         return WordNetSin.Default.GetLinkDef("synonyms");
      }
   }
}
