﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using JaStDev.HAB.Sins;

namespace JaStDev.HAB.Designer.Tools.Timing
{
   /// <summary>
   /// Interaction logic for TimersView.xaml
   /// </summary>
   public partial class TimersView : UserControl
   {
      TimerCollection fTimerList = new TimerCollection();


      public TimersView()
      {
         InitializeComponent();
      }

      #region TimerList

      /// <summary>
      /// Gets the list of all the timers in the brain.
      /// </summary>
      public TimerCollection TimerList
      {
         get { return fTimerList; }
      }

      #endregion

      /// <summary>
      /// Handles the Click event of the BtnNewTimer control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void BtnNewTimer_Click(object sender, RoutedEventArgs e)
      {
         WindowMain.UndoStore.BeginUndoGroup(true);                                                        //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
         try
         {
            TimerSin iNew = new TimerSin();
            iNew.Text = "Timer";
            WindowMain.AddItemToBrain(iNew);                                                                //don't need to add it to our list, this is done through the event handling
         }
         finally
         {
            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(WindowMain.UndoStore.EndUndoGroup));          //not really needed, just for safety.
         }
      }
   }
}
