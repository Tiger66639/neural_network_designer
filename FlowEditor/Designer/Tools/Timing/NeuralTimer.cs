﻿using System;
using System.Windows;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.BrainData;
using JaStDev.HAB.Designer.Tools.Debugger;
using JaStDev.HAB.Designer.WPF.BrainEventManagers;
using JaStDev.HAB.Events;
using JaStDev.HAB.Sins;

namespace JaStDev.HAB.Designer.Tools.Timing
{
   /// <summary>
   /// A wrapper for the <see cref="TimerSin"/>.
   /// </summary>
   /// <remarks>
   /// Also makes certain that the timersin has a correct factory assigned for creating processors.
   /// </remarks>
   public class NeuralTimer : ObservableObject, IWeakEventListener, INeuronWrapper, INeuronInfo
   {

      #region Fields
      TimerSin fItem;
      bool fIsActive = false;
      double fInterval;
      #endregion

      #region ctor

      /// <summary>
      /// Initializes a new instance of the <see cref="NeuralTimer"/> class.
      /// </summary>
      /// <param name="toWrap">The item to wrap.</param>
      public NeuralTimer(TimerSin toWrap)
      {
         if (toWrap == null)
            throw new ArgumentNullException();
         Item = toWrap;
         if (toWrap.Factory == null)
            toWrap.Factory = ProcessorManager.Current;
         InternalCreate();
      }

      private void InternalCreate()
      {
         NeuronChangedEventManager.AddListener(Brain.Brain.Current, this);
      }

      /// <summary>
      /// Releases unmanaged resources and performs other cleanup operations before the
      /// <see cref="NeuralTimer"/> is reclaimed by garbage collection.
      /// </summary>
      ~NeuralTimer()
      {
         NeuronChangedEventManager.RemoveListener(Brain.Brain.Current, this);
      }

      #endregion

      #region Prop

      
      #region Item

      /// <summary>
      /// Gets/sets the Neuron that is wrapped.
      /// </summary>
      public TimerSin Item
      {
         get
         {
            return fItem;
         }
         set
         {
            if (fItem != value)
            {
               fItem = value;
               if (value != null)
               {
                  IsActive = value.IsActive;
                  Interval = value.Interval;
               }
               else
               {
                  IsActive = false;
                  Interval = double.NaN;
               }
               OnPropertyChanged("Item");
            }
         }
      }

      #endregion


      #region IsActive

      /// <summary>
      /// Gets/sets the if the timer is active or not.
      /// </summary>
      public bool IsActive
      {
         get
         {
            return fIsActive;
         }
         set
         {
            if (value != fIsActive)
            {
               OnPropertyChanging("IsActive", fIsActive, value);
               fIsActive = value;
               if (Item != null)
                  Item.IsActive = value;
               //OnProperty is called by event handler of brain.
            }
         }
      }

      #endregion

      #region Interval

      /// <summary>
      /// Gets/sets the timing interval used by the timer between 2 ticks, expressed in milliseconds.
      /// </summary>
      public double Interval
      {
         get
         {
            return fInterval;
         }
         set
         {
            if (fInterval != value)
            {
               OnPropertyChanging("Interval", fInterval, value);
               fInterval = value;
               if (Item != null)
                  Item.Interval = value;
               //OnProperty is called by event handler of brain.
            }
         }
      }

      #endregion

      #endregion

      #region IWeakEventListener Members

      /// <summary>
      /// Receives events from the centralized event manager.
      /// </summary>
      /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
      /// <param name="sender">Object that originated the event.</param>
      /// <param name="e">Event data.</param>
      /// <returns>
      /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
      /// </returns>
      public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(NeuronChangedEventManager))
         {
            NeuronChanged(sender, (NeuronChangedEventArgs)e);
            return true;
         }
         else
            return false;
      }

      /// <summary>
      /// Handles the neuronchanged event handler.
      /// </summary>
      /// <remarks>
      /// Checks if a neuron prop changed, if so, raise propertychanged event.
      /// </remarks>
      /// <param name="sender">The sender.</param>
      /// <param name="neuronChangedEventArgs">The <see cref="NeuronChangedEventArgs"/> instance containing the event data.</param>
      private void NeuronChanged(object sender, NeuronChangedEventArgs e)
      {
         if (e.Action == BrainAction.Changed && e.OriginalSource == Item && e is NeuronPropChangedEventArgs)
            OnPropertyChanged(((NeuronPropChangedEventArgs)e).Property);
      }

      #endregion

      #region INeuronInfo Members

      /// <summary>
      /// Gets the extra info for the specified neuron.  Can be null.
      /// </summary>
      /// <value></value>
      public NeuronData NeuronInfo
      {
         get { return BrainData.BrainData.Current.NeuronInfo[Item.ID]; }
      }

      #endregion

      #region INeuronWrapper Members

      /// <summary>
      /// Gets the item.
      /// </summary>
      /// <value>The item.</value>
      Neuron INeuronWrapper.Item
      {
         get { return Item; }
      }

      #endregion
   }
}
