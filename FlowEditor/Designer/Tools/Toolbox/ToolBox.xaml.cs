﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using JaStDev.HAB.Designer.WPF.BrainEventManagers;
using JaStDev.HAB.Events;

namespace JaStDev.HAB.Designer.Tools.Toolbox
{
   /// <summary>
   /// Interaction logic for ToolBox.xaml
   /// </summary>
   public partial class ToolBox : UserControl, IWeakEventListener
   {
      public ToolBox()
      {
         InitializeComponent();
         NeuronChangedEventManager.AddListener(Brain.Brain.Current, this);
      }

      ~ ToolBox()
      {
         NeuronChangedEventManager.RemoveListener(Brain.Brain.Current, this);
      }


      /// <summary>
      /// When a neuron is removed from the brain, we need to check there is no toolbox item associated with it.  
      /// If so, this needs to be removed.
      /// </summary>
      /// Possible speed optimization here: use single loop for delete
      void Brain_NeuronChanged(object sender, NeuronChangedEventArgs e)
      {
         if (e.Action == BrainAction.Removed)
         {
            //we make a copy of the list of items to remove cause we can't remove items in a loop through the items.
            List<ToolBoxItem> iToRemove = (from i in BrainData.BrainData.Current.ToolBoxItems
                                           where i is NeuronToolBoxItem && ((NeuronToolBoxItem)i).ItemID == e.OriginalSourceID   //we check ID's here cause this is saver for a delete.  This can otherwise give errors.
                                           select i).ToList();
            foreach (ToolBoxItem i in iToRemove)
               BrainData.BrainData.Current.ToolBoxItems.Remove(i);
         }
      }

      #region IWeakEventListener Members

      public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
      {
         if (managerType == typeof(NeuronChangedEventManager))
         {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<object, NeuronChangedEventArgs>(Brain_NeuronChanged), sender, (NeuronChangedEventArgs)e);
            return true;
         }
         else
            return false;
      }

      #endregion
   }
}
