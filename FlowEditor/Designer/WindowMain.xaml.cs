﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.BrainData;
using JaStDev.HAB.Designer.Comm_channels;
using JaStDev.HAB.Designer.Comm_channels.Images;
using JaStDev.HAB.Designer.Comm_channels.Reflection;
using JaStDev.HAB.Designer.Comm_channels.Text;
using JaStDev.HAB.Designer.Dialogs;
using JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems;
using JaStDev.HAB.Designer.Editors.Flow.FlowItems;
using JaStDev.HAB.Designer.Editors.Frames;
using JaStDev.HAB.Designer.Editors.MindMap;
using JaStDev.HAB.Designer.Editors.Overview;
using JaStDev.HAB.Designer.Tools.Debugger;
using JaStDev.HAB.Designer.Tools.Description;
using JaStDev.HAB.Designer.Undo_data;
using JaStDev.HAB.Designer.WPF;
using JaStDev.HAB.Events;
using JaStDev.HAB.Sins;

namespace JaStDev.HAB.Designer
{
    /// <summary>
    ///     Interaction logic for Window1.xaml
    /// </summary>
    public partial class WindowMain : Window, IWeakEventListener
    {
        #region prop

        public static UndoStore UndoStore
        {
            get
            {
                if (fUndoStore == null)
                {
                    fUndoStore = new UndoStore();
                    if (BrainData.BrainData.Current != null)
                        fUndoStore.Register(BrainData.BrainData.Current);
                }
                return fUndoStore;
            }
        }

        #endregion

        #region IWeakEventListener Members

        /// <summary>
        ///     Receives events from the centralized event manager.
        /// </summary>
        /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager" /> calling this method.</param>
        /// <param name="sender">Object that originated the event.</param>
        /// <param name="e">Event data.</param>
        /// <returns>
        ///     true if the listener handled the event. It is considered an error by the
        ///     <see cref="T:System.Windows.WeakEventManager" /> handling in WPF to register a listener for an event that the
        ///     listener does not handle. Regardless, the method should return false if it receives an event that it does not
        ///     recognize or handle.
        /// </returns>
        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            if (managerType == typeof(BeforeSaveEventManager))
            {
                FlushDescriptionData(Keyboard.FocusedElement as RichTextBox);
                    //Whenever the data gets saved, we need to make certain that everything gets flushed ok, like the description box.
                return true;
            }
            return false;
        }

        #endregion

        private void BtnCloseTlFrame_Click(object sender, RoutedEventArgs e)
        {
            var iSender = (FrameworkElement) sender;
            BrainData.BrainData.Current.OpenDocuments.Remove(iSender.DataContext);
        }

        /// <summary>
        ///     Handles the Click event of the MnuRemoveBrokenRef menu item.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs" /> instance containing the event data.</param>
        private void MnuRemoveBrokenRef_Click(object sender, RoutedEventArgs e)
        {
            var iDlg = new DlgFixBrokenRefs();
            iDlg.Owner = this;
            iDlg.ShowDialog();
        }

        private void MnuHelpAbout_Click(object sender, RoutedEventArgs e)
        {
            var iDlg = new DlgAbout();
            iDlg.Owner = this;
            iDlg.ShowDialog();
        }

        private void MnuSearchHelp_Click(object sender, RoutedEventArgs e)
        {
            fHelp.ShowHelpSearch();
        }

        private void MnuContentsHelp_Click(object sender, RoutedEventArgs e)
        {
            fHelp.ShowHelpContents();
        }

        private void MnuIndexHelp_Click(object sender, RoutedEventArgs e)
        {
            fHelp.ShowHelpIndex("");
        }

        private void Help_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Keyboard.FocusedElement is UIElement;
        }

        private void Help_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            fHelp.ShowHelpFor(Keyboard.FocusedElement as UIElement);
        }

        private void MnuItemCloseDocument_Click(object sender, RoutedEventArgs e)
        {
            var iItem = (UIElement) e.OriginalSource;

            ContextMenu iMenu = TreeHelper.FindInTree<ContextMenu>(iItem);
            ToolFrame iFrame = (ToolFrame) iMenu.PlacementTarget;
            var iChannel = iFrame.DataContext as CommChannel; //commchannels need to be closed differently.
            if (iChannel != null)
                iChannel.IsVisible = false;
            else
                BrainData.BrainData.Current.OpenDocuments.Remove(iFrame.DataContext);
        }

        #region fields

        private IDescriptionable fCurrentDesc;
            //a ref to the object currently displayed in the global richtextbox description editor (so we can save the data again to it).

        private INeuronInfo fCurrentNeuronInfo;
            //goes together with fCurrentDoc.  If this field is set, it indicates the neuroninfo from which we are depicting a description (doesn't have to be filled in).

        private static UndoStore fUndoStore; //stores a reference to the undo store that should be used.
        private readonly Help fHelp;

        #endregion

        #region ctor -dtor

        /// <summary>
        ///     Initializes a new instance of the <see cref="WindowMain" /> class.
        /// </summary>
        public WindowMain()
        {
            BeforeSaveEventManager.AddListener(BrainData.BrainData.Current, this);
            fHelp = new Help();
            fHelp.DefaultHelpFile = "NND.chm";
            InitializeComponent();
        }

        /// <summary>
        ///     Releases unmanaged resources and performs other cleanup operations before the
        ///     <see cref="WindowMain" /> is reclaimed by garbage collection.
        /// </summary>
        ~WindowMain()
        {
            BeforeSaveEventManager.RemoveListener(BrainData.BrainData.Current, this);
        }

        #endregion

        #region Helpers

        /// <summary>
        ///     Flushes the description data back to where it came from.
        /// </summary>
        /// <param name="focused">The Richtextbox that was focused, if any.</param>
        private void FlushDescriptionData(RichTextBox focused)
        {
            if (focused != null && VwDescription.IsChanged)
            {
                if (fCurrentDesc != null)
                    fCurrentDesc.Description = VwDescription.Document;
            }
        }

        /// <summary>
        ///     Adds the item to brain taking care of the undo data.
        /// </summary>
        /// <param name="value">The neuron to add to the brain.</param>
        public static void AddItemToBrain(Neuron value)
        {
            Brain.Brain.Current.Add(value);
            var iUndoData = new NeuronUndoItem {Neuron = value, ID = value.ID, Action = BrainAction.Created};
            UndoStore.AddCustomUndoItem(iUndoData);
        }

        /// <summary>
        ///     Deletes the item from brain taking care of the undo data.
        /// </summary>
        /// <param name="toDelete">The neuron to delete from the brain.</param>
        public static void DeleteItemFromBrain(Neuron toDelete)
        {
            var iUndoData = new NeuronUndoItem {Neuron = toDelete, ID = toDelete.ID, Action = BrainAction.Removed};
            UndoStore.AddCustomUndoItem(iUndoData);
            if (Brain.Brain.Current.Delete(toDelete) == false)
            {
                var iMsg = string.Format("Neuron can't be deleted: {0}.", toDelete);
                MessageBox.Show(iMsg, "Delete neuron", MessageBoxButton.OK, MessageBoxImage.Error);
                Log.Log.LogError("WindowMain.DeleteItemFromBrain", iMsg);
            }
        }

        /// <summary>
        ///     Deletes the item from brain recursivelly. All Neurons pointed to from the LinksOut section of the
        ///     neuron being deleted will also be removed.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         Removing a branch (so a neuron and all neurons that link to it) is usefull to remove peaces of code
        ///         for instance.
        ///     </para>
        ///     <para>
        ///         This function always creates an undo block so that all neurons removed by this operation can be recreated.
        ///     </para>
        /// </remarks>
        /// <param name="toDelete">To delete.</param>
        /// <param name="forced">
        ///     if set to <c>false</c> only neurons from the branch will be removed that don't have any references
        ///     anymore (starting from the leaf, so when all leafs are removed, the parent is evaluated.
        /// </param>
        public static void DeleteBranchFromBrain(Neuron toDelete, bool forced)
        {
            UndoStore.BeginUndoGroup(false);
            try
            {
                DeleteBranchChildren(toDelete, forced);
                DeleteNeuron(toDelete);
            }
            finally
            {
                UndoStore.EndUndoGroup();
            }
        }

        /// <summary>
        ///     Deletes a single neuron and generates undo data.
        /// </summary>
        /// <param name="toDelete">The item to delete.</param>
        private static void DeleteNeuron(Neuron toDelete)
        {
            var iUndoData = new NeuronUndoItem {Neuron = toDelete, ID = toDelete.ID, Action = BrainAction.Removed};
            UndoStore.AddCustomUndoItem(iUndoData);
            Brain.Brain.Current.Delete(toDelete);
        }

        /// <summary>
        ///     deletes all the neurons related to 'TodDelete' that should also be deleted (because they are children of 'toDelete'
        ///     or they receive a link from 'toDelete' and have no other incomming links (this is checked recursivelly).
        /// </summary>
        /// <param name="toDelete">To delete.</param>
        /// <param name="forced">
        ///     if set to <c>true</c> linked to items will always be deleted, also if they have more than 1
        ///     incomming link.
        /// </param>
        private static void DeleteBranchChildren(Neuron toDelete, bool forced)
        {
            var iCountOk = false;
            var iClusteredByOk = false;
            List<Link> iLinksOut;
            using (var iToDelLinksOut = toDelete.LinksOut) //only way to access it thread safe
                iLinksOut = new List<Link>(iToDelLinksOut.Items);
                    //we make a copy cause we are going to delete items, if we don't make a copy, we would get an error that we are trying to modify the list inside a foreach.
            foreach (var i in iLinksOut)
            {
                using (var iToLinksIn = i.To.LinksIn)
                    //we need to access this info in a thread safe way. Offcourse, if we want to delete the item, we can't keep the reader lock in the same thread cause that blocks, so we need to do it this way.
                    iCountOk = iToLinksIn.Items.Count == 1;
                using (var iClusteredBy = i.To.ClusteredBy)
                    //we need to access this info in a thread safe way. Offcourse, if we want to delete the item, we can't keep the reader lock in the same thread cause that blocks, so we need to do it this way.            
                    iClusteredByOk = iClusteredBy.Items.Count == 0;
                if (forced || (iCountOk && iClusteredByOk))
                {
                    var iTo = i.To;
                    DeleteBranchChildren(iTo, forced);
                    DeleteNeuron(iTo);
                }
            }
            var iToDelete = toDelete as NeuronCluster;
            if (iToDelete != null)
            {
                List<Neuron> iChildren;
                using (var iList = iToDelete.Children)
                    iChildren = iList.ConvertTo<Neuron>();
                foreach (var i in iChildren)
                {
                    using (var iLinksIn = i.LinksIn)
                        //we need to access this info in a thread safe way. Offcourse, if we want to delete the item, we can't keep the reader lock in the same thread cause that blocks, so we need to do it this way.
                        iCountOk = iLinksIn.Items.Count == 0;
                    using (var iClusteredBy = i.ClusteredBy)
                        //we need to access this info in a thread safe way. Offcourse, if we want to delete the item, we can't keep the reader lock in the same thread cause that blocks, so we need to do it this way.            
                        iClusteredByOk = iClusteredBy.Items.Count == 1;
                    if (i.CanBeDeleted && iCountOk && iClusteredByOk) //only remove children that are nowhere else used.
                    {
                        DeleteBranchChildren(i, forced);
                        DeleteNeuron(i);
                    }
                }
            }
        }

        #endregion

        #region commands

        //#region Redo
        ///// <summary>
        ///// Handles the CanExecute event of the Redo control.
        ///// </summary>
        ///// <remarks>
        ///// We override Undo/Redo so we can change the state of the MindMap to 'Undoing', which is important to block certain 
        ///// automatic data rendering of the <see cref="MindMapItemCollection"/> container. 
        ///// </remarks>
        ///// <param name="sender">The source of the event.</param>
        ///// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        //private void Redo_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        //{
        //   WindowMain.UndoStore.Redo_CanExecute(sender, e);
        //}

        ///// <summary>
        ///// Handles the Executed event of the Redo control.
        ///// </summary>
        ///// <remarks>
        ///// We override Undo/Redo so we can change the state of the MindMap to 'Undoing', which is important to block certain 
        ///// automatic data rendering of the <see cref="MindMapItemCollection"/> container. 
        ///// </remarks>
        ///// <param name="sender">The source of the event.</param>
        ///// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        //private void Redo_Executed(object sender, ExecutedRoutedEventArgs e)
        //{
        //   foreach (MindMap i in BrainData.Current.MindMaps)
        //      i.CurrentState = EditorState.Undoing;
        //   MindMap iMap = (MindMap)DataContext;
        //   try
        //   {
        //      WindowMain.UndoStore.Redo_OnExecuted(sender, e);
        //   }
        //   finally
        //   {
        //      foreach (MindMap i in BrainData.Current.MindMaps)
        //         i.CurrentState = EditorState.Loaded;
        //   }
        //}
        //#endregion

        //#region Undo
        ///// <summary>
        ///// Handles the CanExecute event of the Undo control.
        ///// </summary>
        ///// <remarks>
        ///// We override Undo/Redo so we can change the state of the MindMap to 'Undoing', which is important to block certain 
        ///// automatic data rendering of the <see cref="MindMapItemCollection"/> container. 
        ///// </remarks>
        ///// <param name="sender">The source of the event.</param>
        ///// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        //private void Undo_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        //{
        //   WindowMain.UndoStore.Undo_CanExecute(sender, e);
        //}

        ///// <summary>
        ///// Handles the Executed event of the Undo control.
        ///// </summary>
        ///// <remarks>
        ///// We override Undo/Redo so we can change the state of the MindMap to 'Undoing', which is important to block certain 
        ///// automatic data rendering of the <see cref="MindMapItemCollection"/> container. 
        ///// </remarks>
        ///// <param name="sender">The source of the event.</param>
        ///// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        //private void Undo_Executed(object sender, ExecutedRoutedEventArgs e)
        //{
        //   foreach (MindMap i in BrainData.Current.MindMaps)
        //      i.CurrentState = EditorState.Undoing;
        //   try
        //   {
        //      WindowMain.UndoStore.Undo_OnExecuted(sender, e);
        //   }
        //   finally
        //   {
        //      foreach (MindMap i in BrainData.Current.MindMaps)
        //         i.CurrentState = EditorState.Loaded;
        //   }
        //}
        //#endregion 

        #region ViewCode

        /// <summary>
        ///     Shows the code editor for the specified <see cref="Neuron" />.  If there
        ///     was already an editor opened for this item, it is made active.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs" /> instance containing the event data.</param>
        private void ViewCode_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var iNeuron = (Neuron) e.Parameter;
            if (iNeuron == null)
            {
                var iFocused = Keyboard.FocusedElement as FrameworkElement;
                if (iFocused != null && iFocused.DataContext is INeuronWrapper)
                    iNeuron = ((INeuronWrapper) iFocused.DataContext).Item;
            }
            ViewCodeForNeuron(iNeuron);
        }

        /// <summary>
        ///     Shows the code for the neuron.
        /// </summary>
        /// <param name="neuron">The euron.</param>
        /// <returns>The code editor that represents the code.</returns>
        public CodeEditor ViewCodeForNeuron(Neuron neuron)
        {
            CodeEditor iEditor = null;
            if (neuron != null)
            {
                iEditor = (from i in BrainData.BrainData.Current.CodeEditors
                    where i.Item == neuron
                    select i).FirstOrDefault(); //we first check if the code editor is already open.
                if (iEditor == null)
                {
                    iEditor = new CodeEditor(neuron);
                    BrainData.BrainData.Current.CodeEditors.Add(iEditor);
                        //this will automatically trigger and add to the open docuements list.
                    BrainData.BrainData.Current.OpenDocuments.Add(iEditor);
                }
                else
                {
                    if (BrainData.BrainData.Current.OpenDocuments.Contains(iEditor) == false)
                        BrainData.BrainData.Current.OpenDocuments.Add(iEditor);
                }
                ToolFrame iFrame = TfData.ItemContainerGenerator.ContainerFromItem(iEditor) as ToolFrame;
                if (iFrame != null)
                    iFrame.IsSelected = true;
            }
            else
                MessageBox.Show("Can't find selected neuron!");
            return iEditor;
        }

        /// <summary>
        ///     Handles the CanExecute event of the ViewCode control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs" /> instance containing the event data.</param>
        private void ViewCode_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var iFocused = Keyboard.FocusedElement as FrameworkElement;
            e.CanExecute = e.Parameter is Neuron || (iFocused != null && iFocused.DataContext is INeuronWrapper);
        }

        #endregion

        private void Sync_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var iNeuron = (Neuron) e.Parameter;
            if (iNeuron == null)
            {
                var iFocused = Keyboard.FocusedElement as FrameworkElement;
                if (iFocused != null && iFocused.DataContext is INeuronWrapper)
                    iNeuron = ((INeuronWrapper) iFocused.DataContext).Item;
            }
            SyncExplorerToNeuron(iNeuron);
        }

        /// <summary>
        ///     Syncs the explorer to specified neuron.
        /// </summary>
        /// <param name="iNeuron">The i neuron.</param>
        public void SyncExplorerToNeuron(Neuron iNeuron)
        {
            if (iNeuron != null)
            {
                DockingControl.Controls.DockingControl.SetIsSelected(MainExplorer, true);
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<ulong>(SyncMainExplorerTo), iNeuron.ID);
            }
            else
                MessageBox.Show("Can't find selected neuron!");
        }

        /// <summary>
        ///     Syncs the main explorer to the specified id.  Allows us to do this async.
        /// </summary>
        /// <param name="id">The id.</param>
        private void SyncMainExplorerTo(ulong id)
        {
            MainExplorer.SelectedID = id;
            MainExplorer.LstChildren.Focus();
        }

        #region Change to

        /// <summary>
        ///     Handles the CanExecute event of the ChangeTo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs" /> instance containing the event data.</param>
        private void ChangeTo_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var iFocused = Keyboard.FocusedElement as FrameworkElement;
            var iSource = e.OriginalSource as FrameworkElement;
            e.CanExecute = e.Parameter is Type &&
                           ((iFocused != null && iFocused.DataContext is INeuronWrapper) ||
                            (iSource != null && iSource.DataContext is INeuronWrapper));
        }

        /// <summary>
        ///     Handles the Executed event of the ChangeTo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs" /> instance containing the event data.</param>
        private void ChangeTo_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var iType = (Type) e.Parameter;
            Neuron iNeuron = null;
            var iFocused = Keyboard.FocusedElement as FrameworkElement;
            if (iFocused != null && iFocused.DataContext is INeuronWrapper)
                iNeuron = ((INeuronWrapper) iFocused.DataContext).Item;
            else
            {
                var iSource = e.OriginalSource as FrameworkElement;
                if (iSource != null && iSource.DataContext is INeuronWrapper)
                    iNeuron = ((INeuronWrapper) iSource.DataContext).Item;
            }

            if (iNeuron != null)
            {
                if (AskOkForNeuronChange(iNeuron, iType))
                    iNeuron = iNeuron.ChangeTypeTo(iType);
            }
            else
                MessageBox.Show("Can't find selected neuron!");
        }

        /// <summary>
        ///     Asks the ok from the user to change the neuron to the new type.
        /// </summary>
        /// <remarks>
        ///     We set this in a seperate function cause we need to check to convertion -> if it is allowed + what message text
        ///     to display to the user.
        /// </remarks>
        /// <param name="neuron">The neuron.</param>
        /// <param name="type">The type.</param>
        /// <returns>True if the user said ok, false otherwise.</returns>
        private bool AskOkForNeuronChange(Neuron neuron, Type type)
        {
            string iMsg;
            if (neuron is NeuronCluster || neuron is ValueNeuron)
                //these are all the types that, when changed will loose data.
                iMsg = string.Format("Changing the neuron: {0} to a {1} will loose some data, continue?", neuron,
                    type.Name);
            else
                iMsg = string.Format("Change the neuron: {0} to a {1}?", neuron, type.Name);
            var iRes = MessageBox.Show(iMsg, "Change neuron type", MessageBoxButton.YesNo, MessageBoxImage.Question,
                MessageBoxResult.Yes);
            return iRes == MessageBoxResult.Yes;
        }

        #endregion

        #region DeleteNeuron

        /// <summary>
        ///     Handles the CanExecute event of the DeleteNeuron control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs" /> instance containing the event data.</param>
        private void DeleteNeuron_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var iToDelete = e.Parameter as Neuron;
            if (iToDelete == null)
            {
                var iFocused = Keyboard.FocusedElement as FrameworkElement;
                if (iFocused != null && iFocused.DataContext is INeuronWrapper)
                    iToDelete = ((INeuronWrapper) iFocused.DataContext).Item;
            }
            e.CanExecute = iToDelete != null && iToDelete.CanBeDeleted;
        }

        /// <summary>
        ///     Handles the Executed event of the DeleteNeuron command.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs" /> instance containing the event data.</param>
        private void DeleteNeuron_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var iToDelete = e.Parameter as Neuron;
            if (iToDelete == null)
            {
                var iFocused = Keyboard.FocusedElement as FrameworkElement;
                if (iFocused != null && iFocused.DataContext is INeuronWrapper)
                    iToDelete = ((INeuronWrapper) iFocused.DataContext).Item;
            }
            if (iToDelete != null)
            {
                var iRes = MessageBox.Show(string.Format("Delete neuron: '{0}'?", iToDelete), "Delete neuron",
                    MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.No);
                if (iRes == MessageBoxResult.OK)
                {
                    UndoStore.BeginUndoGroup(false);
                        //we group all the data together so a single undo command cand restore the previous state.
                    try
                    {
                        DeleteItemFromBrain(iToDelete);
                    }
                    finally
                    {
                        Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(UndoStore.EndUndoGroup));
                            //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled.
                    }
                }
            }
        }

        #endregion

        #region RunNeuron

        private void RunNeuron_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var iToRun = e.Parameter as Neuron;
            if (iToRun == null)
            {
                var iFocused = Keyboard.FocusedElement as FrameworkElement;
                if (iFocused != null && iFocused.DataContext is INeuronWrapper)
                    iToRun = ((INeuronWrapper) iFocused.DataContext).Item;
            }
            e.CanExecute = iToRun != null;
        }

        private void RunNeuron_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var iToRun = e.Parameter as Neuron;
            if (iToRun == null)
            {
                var iFocused = Keyboard.FocusedElement as FrameworkElement;
                if (iFocused != null && iFocused.DataContext is INeuronWrapper)
                    iToRun = ((INeuronWrapper) iFocused.DataContext).Item;
            }
            if (iToRun != null)
            {
                var iClusterToRun = iToRun as NeuronCluster;
                Processor.Processor iProc;
                if (iClusterToRun != null)
                {
                    var iRes = MessageBox.Show("Run the cluster as function (no will solve it)?", "Delete neuron",
                        MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
                    if (iRes == MessageBoxResult.Yes)
                    {
                        iProc = ProcessorManager.Current.GetProcessor();
                        iProc.Call(iClusterToRun);
                    }
                    else if (iRes == MessageBoxResult.Cancel)
                        return;
                }
                iProc = ProcessorManager.Current.GetProcessor(); //by default we solve it.
                iProc.Push(iToRun);
                iProc.Solve();
            }
        }

        #endregion

        #region Sandbox

        /// <summary>
        ///     Handles the CanExecute event of the Sandbox command.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs" /> instance containing the event data.</param>
        private void Sandbox_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ProjectManager.Default.SandboxRunning == false &&
                           ProjectManager.Default.IsSandBox == false &&
                           string.IsNullOrEmpty(ProjectManager.Default.Location) == false &&
                           string.IsNullOrEmpty(ProjectManager.Default.SandboxLocation) == false;
        }

        /// <summary>
        ///     Handles the Executed event of the Sandbox command.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs" /> instance containing the event data.</param>
        private void Sandbox_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ProjectManager.Default.StartSandBox();
        }

        #endregion

        #region Continue debug

        private void ContinueDebug_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var iItem = ProcessorManager.Current.SelectedProcessor;
            if (iItem != null)
            {
                var iProc = iItem.Processor as DebugProcessor;
                if (iProc != null)
                    e.CanExecute = iProc.IsPaused;
                else
                    e.CanExecute = false;
            }
        }

        private void ContinueDebug_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var iItem = ProcessorManager.Current.SelectedProcessor;
            if (iItem != null)
            {
                var iProc = iItem.Processor as DebugProcessor;
                iProc.DebugContinue();
            }
        }

        #endregion

        #region Pause debug

        private void PauseDebug_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var iItem = ProcessorManager.Current.SelectedProcessor;
            if (iItem != null)
            {
                var iProc = iItem.Processor as DebugProcessor;
                if (iProc != null)
                    e.CanExecute = !iProc.IsPaused;
                else
                    e.CanExecute = false;
            }
        }

        private void PauseDebug_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var iItem = ProcessorManager.Current.SelectedProcessor;
            if (iItem != null)
            {
                var iProc = iItem.Processor as DebugProcessor;
                iProc.DebugPause();
            }
        }

        #endregion

        #region Step next debug

        private void StepNextDebug_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var iItem = ProcessorManager.Current.SelectedProcessor;
            if (iItem != null)
            {
                var iProc = iItem.Processor as DebugProcessor;
                if (iProc != null)
                    e.CanExecute = iProc.IsPaused;
                else
                    e.CanExecute = false;
            }
        }

        private void StepNextDebug_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var iItem = ProcessorManager.Current.SelectedProcessor;
            if (iItem != null)
            {
                var iProc = iItem.Processor as DebugProcessor;
                iProc.DebugStepNext();
            }
        }

        #endregion

        #region ImportFromFramenet

        /// <summary>
        ///     Handles the Executed event of the ImportFrameNetData control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs" /> instance containing the event data.</param>
        private void ImportFrameNetData_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var iDlg = new DlgImportFrameNet();
            iDlg.Owner = this;
            if (e.Parameter != null)
            {
                iDlg.ImportInto = (FrameEditor) e.Parameter;
                iDlg.ShowImportInto = false;
            }
            iDlg.Show();
        }

        //we don't show dialog, this way, the user can still browse the data while selecting what to import.

        #endregion

        #endregion

        #region Event handlers

        #region Menu

        /// <summary>
        ///     Handles the Click event of the MnuExit menu item.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs" /> instance containing the event data.</param>
        private void MnuExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        ///     Handles the Click event of the MnuRecentProject menu item.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs" /> instance containing the event data.</param>
        private void MnuRecentProject_Click(object sender, RoutedEventArgs e)
        {
            if (e.OriginalSource != MnuRecentProjects)
                //need to make certain that we don't do something when the user clicks on 'Recently opened'.
            {
                var iSender = (MenuItem) e.OriginalSource;
                var iPath = iSender.Header as string;
                ProjectManager.Default.Open(iPath);
            }
        }

        /// <summary>
        ///     Handles the Click event of the MnuOptions control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs" /> instance containing the event data.</param>
        private void MnuOptions_Click(object sender, RoutedEventArgs e)
        {
            var iDlg = new WindowOptions();
            iDlg.Owner = this;
            iDlg.ShowDialog();
        }

        #endregion

        #region General

        /// <summary>
        ///     Checks if the application can be closed, and ask the appropriate questions in cases it doesn't know.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (ProjectManager.Default.ProjectChanged)
            {
                var iRes = MessageBox.Show("The project has been changed, save first?", "Quit application!",
                    MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                if (iRes == MessageBoxResult.Yes)
                    ProjectManager.Default.Save();
                e.Cancel = iRes == MessageBoxResult.Cancel;
            }
            //if (e.Cancel == false)
            //   VwThesaurus.SaveData();
        }

        #endregion

        #region File

        #region Save

        /// <summary>
        ///     Handles the Executed event of the Save command.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs" /> instance containing the event data.</param>
        private void Save_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ProjectManager.Default.Save();
        }


        private void SaveAs_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ProjectManager.Default.SaveAs();
        }

        #endregion

        #region New

        /// <summary>
        ///     Handles the Executed event of the New command.
        /// </summary>
        /// <remarks>
        ///     Simply starts a new process that is the same as this one.
        /// </remarks>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs" /> instance containing the event data.</param>
        private void New_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ProjectManager.Default.CreateNew();
            VwDescription.Document = null;
                //when a new item is created, need to make certain that the desc editor is also empty.
        }

        #endregion

        #region Open

        /// <summary>
        ///     Handles the Executed event of the Open command.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs" /> instance containing the event data.</param>
        private void Open_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ProjectManager.Default.Open();
        }

        /// <summary>
        ///     Handles the CanExecute event of the Open control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs" /> instance containing the event data.</param>
        private void Open_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ProjectManager.Default.SandboxRunning == false;
        }

        #endregion

        #endregion

        #region Insert

        /// <summary>
        ///     Handles the Click event of the MnuNewCommChannel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs" /> instance containing the event data.</param>
        private void MnuNewCommChannel_Click(object sender, RoutedEventArgs e)
        {
            var iNew = new TextSin();
            iNew.Text = "New text channel";
            UndoStore.BeginUndoGroup(true);
                //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
            try
            {
                AddItemToBrain(iNew);
                var iView = new TextChannel();
                iView.NeuronID = iNew.ID;
                iView.IsVisible = true;
                //iView.Name = iNew.Name;
                BrainData.BrainData.Current.CommChannels.Add(iView); //this stores the item in the brain.
                ToolFrame iFrame = TfData.ItemContainerGenerator.ContainerFromItem(iView) as ToolFrame;
                if (iFrame != null)
                    iFrame.IsSelected = true;
            }
            finally
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(UndoStore.EndUndoGroup));
                    //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled.
            }
        }


        /// <summary>
        ///     Handles the Click event of the MnuNewImageChannel menu item.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs" /> instance containing the event data.</param>
        private void MnuNewImageChannel_Click(object sender, RoutedEventArgs e)
        {
            var iNew = new ImageSin();
            iNew.Text = "New Image channel";
            UndoStore.BeginUndoGroup(true);
                //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
            try
            {
                AddItemToBrain(iNew);
                var iView = new ImageChannel();
                iView.NeuronID = iNew.ID;
                iView.IsVisible = true;
                //iView.Name = iNew.Name;
                BrainData.BrainData.Current.CommChannels.Add(iView); //this stores the item in the brain.
                ToolFrame iFrame = TfData.ItemContainerGenerator.ContainerFromItem(iView) as ToolFrame;
                if (iFrame != null)
                    iFrame.IsSelected = true;
            }
            finally
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(UndoStore.EndUndoGroup));
                    //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled.
            }
        }


        /// <summary>
        ///     Handles the Click event of the MnuNewAudioChannel menu item.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs" /> instance containing the event data.</param>
        private void MnuNewAudioChannel_Click(object sender, RoutedEventArgs e)
        {
            var iNew = new TextSin();
            iNew.Text = "New text channel";
            UndoStore.BeginUndoGroup(true);
                //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
            try
            {
                AddItemToBrain(iNew);
                var iView = new TextChannel();
                iView.NeuronID = iNew.ID;
                iView.IsVisible = true;
                //iView.Name = iNew.Name;
                BrainData.BrainData.Current.CommChannels.Add(iView); //this stores the item in the brain.
                ToolFrame iFrame = TfData.ItemContainerGenerator.ContainerFromItem(iView) as ToolFrame;
                if (iFrame != null)
                    iFrame.IsSelected = true;
            }
            finally
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(UndoStore.EndUndoGroup));
                    //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled.
            }
        }


        /// <summary>
        ///     Handles the Click event of the MnuNewReflectionChannel menu item.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs" /> instance containing the event data.</param>
        private void MnuNewReflectionChannel_Click(object sender, RoutedEventArgs e)
        {
            var iNew = new ReflectionSin();
            UndoStore.BeginUndoGroup(true);
                //we begin a group because this action will also remove code items, mindmapitems, ....  to create them correctly (at the correct pos,.., we need to store them as well.
            try
            {
                AddItemToBrain(iNew);
                iNew.Text = "Reflection channel - " + iNew.ID;
                var iView = new ReflectionChannel();
                iView.NeuronID = iNew.ID;
                iView.IsVisible = true;
                BrainData.BrainData.Current.CommChannels.Add(iView); //this stores the item in the brain.
                ToolFrame iFrame = TfData.ItemContainerGenerator.ContainerFromItem(iView) as ToolFrame;
                if (iFrame != null)
                    iFrame.IsSelected = true;
            }
            finally
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(UndoStore.EndUndoGroup));
                    //we call async cause this action triggers some events in the brain which are handled async with the dispatcher, we need to close the undo group after these have been handled.
            }
        }

        /// <summary>
        ///     Handles the Executed event of the NewMindMap control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs" /> instance containing the event data.</param>
        private void NewMindMap_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (BrainData.BrainData.Current.CurrentEditorsList != null)
            {
                var iNew = new MindMap();
                iNew.Name = "New mind map";
                BrainData.BrainData.Current.CurrentEditorsList.Add(iNew);
                BrainData.BrainData.Current.OpenDocuments.Add(iNew); //this actually shows the item
                ToolFrame iFrame = TfData.ItemContainerGenerator.ContainerFromItem(iNew) as ToolFrame;
                if (iFrame != null)
                    iFrame.IsSelected = true;
            }
            else
                throw new InvalidOperationException("There is no editors list selected to put new items in.");
        }

        /// <summary>
        ///     Handles the Executed event of the NewFrameEditor control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs" /> instance containing the event data.</param>
        private void NewFrameEditor_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (BrainData.BrainData.Current.CurrentEditorsList != null)
            {
                var iNew = new FrameEditor();
                iNew.Name = "New frame editor";
                BrainData.BrainData.Current.CurrentEditorsList.Add(iNew);
                BrainData.BrainData.Current.OpenDocuments.Add(iNew); //this actually shows the item
                ToolFrame iFrame = TfData.ItemContainerGenerator.ContainerFromItem(iNew) as ToolFrame;
                if (iFrame != null)
                    iFrame.IsSelected = true;
            }
            else
                throw new InvalidOperationException("There is no editors list selected to put new items in.");
        }

        /// <summary>
        ///     Handles the Executed event of the NewFlow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs" /> instance containing the event data.</param>
        private void NewFlow_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (BrainData.BrainData.Current.CurrentEditorsList != null)
            {
                var iNew = new FlowEditor();
                iNew.Name = "New Flow Editor";
                BrainData.BrainData.Current.CurrentEditorsList.Add(iNew);
                BrainData.BrainData.Current.OpenDocuments.Add(iNew); //this actually shows the item
                ToolFrame iFrame = TfData.ItemContainerGenerator.ContainerFromItem(iNew) as ToolFrame;
                if (iFrame != null)
                    iFrame.IsSelected = true;
            }
            else
                throw new InvalidOperationException("There is no editors list selected to put new items in.");
        }


        /// <summary>
        ///     Handles the Executed event of the NewFolder control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs" /> instance containing the event data.</param>
        private void NewFolder_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (BrainData.BrainData.Current.CurrentEditorsList != null)
            {
                var iNew = new EditorFolder();
                iNew.Name = "New folder";
                BrainData.BrainData.Current.CurrentEditorsList.Add(iNew);
            }
            else
                throw new InvalidOperationException("There is no editors list selected to put new items in.");
        }

        /// <summary>
        ///     Handles the Executed event of the NewCodeCluster control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs" /> instance containing the event data.</param>
        private void NewCodeCluster_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (BrainData.BrainData.Current.CurrentEditorsList != null)
            {
                var iCluster = new NeuronCluster();
                iCluster.Meaning = (ulong) PredefinedNeurons.Code;
                AddItemToBrain(iCluster);
                var iCode = new CodeEditor(iCluster);
                iCode.Name = "New code cluster";
                BrainData.BrainData.Current.CurrentEditorsList.Add(iCode);
                BrainData.BrainData.Current.OpenDocuments.Add(iCode); //this actually shows the item
                ToolFrame iFrame = TfData.ItemContainerGenerator.ContainerFromItem(iCode) as ToolFrame;
                if (iFrame != null)
                    iFrame.IsSelected = true;
            }
            else
                throw new InvalidOperationException("There is no editors list selected to put new items in.");
        }

        #endregion

        #region Docking control

        /// <summary>
        ///     Trigged whenever an item inside the docking control gets keyboard focus.  This is done to check
        ///     if the new selected item has as data context, something who's description can be edited.
        /// </summary>
        /// <remarks>
        ///     This function makes a copy of the DocumentFlow, this way, it can be displayed in multiple editors (like a note and
        ///     the global view).
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DockingControl_GotFocus(object sender, RoutedEventArgs e)
        {
            if (e.OriginalSource != VwDescription.DescriptionEditor)
                //don't do anything if we are moving to the global editor.
            {
                var iSender = e.OriginalSource as FrameworkElement;
                object iData = null;
                if (iSender is ToolFrame)
                    //need to do convertion, in case the editor space (background) is selected, which usually gives the toolframe, so get it's contents.
                    iData = ((ToolFrame) iSender).SelectedContent;
                else if (iSender != null)
                    iData = iSender.DataContext;
                if (iData != null)
                {
                    IDescriptionable iFound = null;

                    fCurrentNeuronInfo = iData as INeuronInfo;
                    if (fCurrentNeuronInfo != null && fCurrentNeuronInfo.NeuronInfo != null)
                        iFound = fCurrentNeuronInfo.NeuronInfo;
                    else
                        iFound = iData as IDescriptionable;
                    if (iFound != null) //if we haven't found anything, we simply leave the previous itemm visible.
                    {
                        VwDescription.Document = iFound.Description;
                        fCurrentDesc = iFound;
                        ToolFrame.SetTitle(VwDescription, "Description: " + iFound.DescriptionTitle);
                    }
                }
                else
                {
                    VwDescription.Document = null;
                    ToolFrame.SetTitle(VwDescription, "Description");
                }
            }
        }


        /// <summary>
        ///     Need to save the data back to the flowdocument when old focus is the global editor.  If old focused is the
        ///     editor of the current document, save to global.
        /// </summary>
        private void DockingControlContent_PrvLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            FlushDescriptionData(e.OldFocus as RichTextBox);
        }

        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            GC.Collect();
        }

        /// <summary>
        ///     Handles the MouseDoubleClick event of the LogItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs" /> instance containing the event data.</param>
        private void LogItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var iSender = sender as ListViewItem;
            if (iSender != null && iSender.Content is LogItem)
            {
                LogItem iItem = (LogItem) iSender.Content;
                var iDebug = iItem.Tag as LogDebugData;
                if (iDebug != null)
                {
                    CodeEditor iEditor;
                    if (iDebug.Meaning != null)
                        iEditor = ViewCodeForNeuron(iDebug.Meaning);
                    else
                        iEditor = ViewCodeForNeuron(iDebug.ExecutionSource);
                    if (iEditor != null)
                        iEditor.SelectedListType = iDebug.ExecListType;
                    SyncExplorerToNeuron(iDebug.Executed);
                }
            }
        }

        #endregion

        //}
        //      TxtTemp.Text = Keyboard.FocusedElement.ToString();
        //   if (Keyboard.FocusedElement != null)
        //{

        //private void StatusBarItem_MouseEnter(object sender, MouseEventArgs e)
    }
}