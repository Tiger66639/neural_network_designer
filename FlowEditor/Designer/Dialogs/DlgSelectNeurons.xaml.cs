﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using JaStDev.HAB.Brain;

namespace JaStDev.HAB.Designer.Dialogs
{
   /// <summary>
   /// Interaction logic for DlgSelectNeurons.xaml
   /// </summary>
   public partial class DlgSelectNeurons : Window
   {
      #region inner types

      /// <summary>
      /// A simple wrapper which provides a selected property, so we know which values to return.
      /// </summary>
      public class NeuronSelectItem: ObservableObject
      {

         #region fields
         bool fIsSelected;
         Neuron fItem; 
         #endregion

         #region ctor
         public NeuronSelectItem(Neuron value, bool isSelected)
         {
            Item = value;
            IsSelected = isSelected;
         } 
         #endregion

         #region IsSelected

         /// <summary>
         /// Gets/sets if the item is selected or not.
         /// </summary>
         public bool IsSelected
         {
            get
            {
               return fIsSelected;
            }
            set
            {
               fIsSelected = value;
               OnPropertyChanged("IsSelected");
            }
         }

         #endregion

         #region Item

         /// <summary>
         /// Gets the item that is wrapped.
         /// </summary>
         public Neuron Item
         {
            get { return fItem; }
            internal set { fItem = value; }
         }

         #endregion
      }

      #endregion

      List<NeuronSelectItem> fItems;

      public DlgSelectNeurons(IEnumerable<Neuron> values, IEnumerable<Neuron> selected)
      {
         List<Neuron> iSelected = selected.ToList();
         fItems = (from i in values 
                   select new NeuronSelectItem(i, iSelected.Contains(i))).ToList();                                     //populate the list.
         InitializeComponent();
      }


      #region prop

      #region ItemTemplate

      /// <summary>
      /// ItemTemplate Dependency Property
      /// </summary>
      public static readonly DependencyProperty ItemTemplateProperty =
          DependencyProperty.Register("ItemTemplate", typeof(DataTemplate), typeof(DlgSelectNeurons),
              new FrameworkPropertyMetadata((DataTemplate)null));

      /// <summary>
      /// Gets or sets the ItemTemplate property.  This dependency property 
      /// indicates the template that should be used to display each neuron that can be selected.
      /// </summary>
      /// <remarks>
      /// </remarks>
      /// <example>
      /// <code lang="xml">
      /// <![CDATA[
      /// <DataTemplate x:Key="SelectableItem">
      ///    <CheckBox Content="{Binding Path=Item}"
      ///              IsChecked="{Binding Path=IsSelected}"/>
      /// </DataTemplate>
      /// ]]>
      /// </code>
      /// </example>
      public DataTemplate ItemTemplate
      {
         get { return (DataTemplate)GetValue(ItemTemplateProperty); }
         set { SetValue(ItemTemplateProperty, value); }
      }

      #endregion



      #region Items

      /// <summary>
      /// Gets the list of items to display
      /// </summary>
      public List<NeuronSelectItem> Items
      {
         get { return fItems; }
      }

      #endregion

      #region SelectedValues

      /// <summary>
      /// Gets the list of neurons that were selected by the user
      /// </summary>
      public IEnumerable<Neuron> SelectedValues
      {
         get { return from i in fItems where i.IsSelected == true select i.Item; }
      }

      #endregion

      #endregion

      #region Functions
      private void OnClickOk(object sender, RoutedEventArgs e)
      {
         DialogResult = true;
      } 

      #endregion
   }
}
