﻿using System.Collections.Generic;
using System.Windows;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Editors.MindMap;

namespace JaStDev.HAB.Designer.Dialogs
{
   /// <summary>
   /// Interaction logic for DlgLink.xaml
   /// </summary>
   public partial class DlgLink : Window
   {
      public DlgLink()
      {
         InitializeComponent();
      }

      /// <summary>
      /// MindMapView depends on the selectedItems from cmbFrom, cmbTo, cmbMeaning to be valid.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void OnClickOk(object sender, RoutedEventArgs e)
      {
         bool iOk = false;
         if (CmbFrom.SelectedItem == null)
            MessageBox.Show("Please provide a valid 'from' value.");
         else if (CmbTo.SelectedItem == null)
            MessageBox.Show("Please provide a valid 'to' value.");
         else if (CmbMeaning.SelectedItem == null)
            MessageBox.Show("Please provide a valid 'meaning' value.");
         else
            iOk = true;
         if (iOk == true)
            DialogResult = true;
      }

      #region ToList

      /// <summary>
      /// ToList Dependency Property
      /// </summary>
      public static readonly DependencyProperty ToListProperty =
          DependencyProperty.Register("ToList", typeof(IList<MindMapNeuron>), typeof(DlgLink),
              new FrameworkPropertyMetadata((IList<MindMapNeuron>)null));

      /// <summary>
      /// Gets or sets the ToList property.  This dependency property 
      /// indicates the list of mindmapneurons which the user can select as to.
      /// </summary>
      public IList<MindMapNeuron> ToList
      {
         get { return (IList<MindMapNeuron>)GetValue(ToListProperty); }
         set { SetValue(ToListProperty, value); }
      }

      #endregion

      #region FromList

      /// <summary>
      /// FromList Dependency Property
      /// </summary>
      public static readonly DependencyProperty FromListProperty =
          DependencyProperty.Register("FromList", typeof(IList<MindMapNeuron>), typeof(DlgLink),
              new FrameworkPropertyMetadata((IList<MindMapNeuron>)null));

      /// <summary>
      /// Gets or sets the FromList property.  This dependency property 
      /// indicates the list of mindmapneurons which the user can select as from.
      /// </summary>
      public IList<MindMapNeuron> FromList
      {
         get { return (IList<MindMapNeuron>)GetValue(FromListProperty); }
         set { SetValue(FromListProperty, value); }
      }

      #endregion

      #region SelectedTo

      /// <summary>
      /// SelectedTo Dependency Property
      /// </summary>
      public static readonly DependencyProperty SelectedToProperty =
          DependencyProperty.Register("SelectedTo", typeof(MindMapNeuron), typeof(DlgLink),
              new FrameworkPropertyMetadata((MindMapNeuron)null));

      /// <summary>
      /// Gets or sets the SelectedTo property.  This dependency property 
      /// indicates the initial value, followed by the mind map neuron that the user selected for to.
      /// </summary>
      public MindMapNeuron SelectedTo
      {
         get { return (MindMapNeuron)GetValue(SelectedToProperty); }
         set { SetValue(SelectedToProperty, value); }
      }

      #endregion


      #region SelectedFrom

      /// <summary>
      /// SelectedFrom Dependency Property
      /// </summary>
      public static readonly DependencyProperty SelectedFromProperty =
          DependencyProperty.Register("SelectedFrom", typeof(MindMapNeuron), typeof(DlgLink),
              new FrameworkPropertyMetadata((MindMapNeuron)null));

      /// <summary>
      /// Gets or sets the SelectedFrom property.  This dependency property 
      /// indicates the initial value, followed by the mind map neuron that the user selected for from.
      /// </summary>
      public MindMapNeuron SelectedFrom
      {
         get { return (MindMapNeuron)GetValue(SelectedFromProperty); }
         set { SetValue(SelectedFromProperty, value); }
      }

      #endregion

      #region SelectedMeaning

      /// <summary>
      /// SelectedMeaning Dependency Property
      /// </summary>
      public static readonly DependencyProperty SelectedMeaningProperty =
          DependencyProperty.Register("SelectedMeaning", typeof(Neuron), typeof(DlgLink),
              new FrameworkPropertyMetadata((Neuron)null));

      /// <summary>
      /// Gets or sets the SelectedMeaning property.  This dependency property 
      /// indicates the initial value, followed by the mm neuron that the user selected as the meaning.
      /// </summary>
      public Neuron SelectedMeaning
      {
         get { return (Neuron)GetValue(SelectedMeaningProperty); }
         set { SetValue(SelectedMeaningProperty, value); }
      }

      #endregion



   }
}
