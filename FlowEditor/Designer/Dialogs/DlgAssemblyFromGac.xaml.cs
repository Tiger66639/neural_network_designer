﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;

namespace JaStDev.HAB.Designer.Dialogs
{
   /// <summary>
   /// Interaction logic for DlgAssemblyFromGac.xaml
   /// </summary>
   public partial class DlgAssemblyFromGac : Window
   {
      List<string> fItems;
      List<string> fLoadedItems;
      string fSelectedItem;

      /// <summary>
      /// Initializes a new instance of the <see cref="DlgAssemblyFromGac"/> class.
      /// </summary>
      public DlgAssemblyFromGac()
      {
         InitializeComponent();
      }

      #region Items

      /// <summary>
      /// Gets the list of assembly names in the gac.
      /// </summary>
      public List<string> Items
      {
         get 
         {
            if (fItems == null)
            {
               fItems = new List<string>();
               string iTemp = Environment.GetFolderPath(Environment.SpecialFolder.System);
               int iPos = iTemp.LastIndexOf(System.IO.Path.DirectorySeparatorChar);
               iTemp = iTemp.Substring(0, iPos);
               string strGacDir =  System.IO.Path.Combine(iTemp, "assembly");
               string[] iGacDirs = System.IO.Directory.GetDirectories(strGacDir);
               foreach (string iGacDir in iGacDirs)
               {
                  string[] strDirs1 = System.IO.Directory.GetDirectories(iGacDir);
                  string[] strDirs2;

                  string[] iFiles;

                  foreach (string strDir1 in strDirs1)
                  {
                     strDirs2 = System.IO.Directory.GetDirectories(strDir1);

                     foreach (string strDir2 in strDirs2)
                     {
                        iFiles = System.IO.Directory.GetFiles(strDir2, "*.dll");
                        foreach (string iDll in iFiles)
                           fItems.Add(iDll);
                     }
                  }

               }
               fItems.Sort();
            }
            return fItems; 
         }
      }

      #endregion

      
      #region LoadedItems

      /// <summary>
      /// Gets the list of loaded assemblies.
      /// </summary>
      public List<string> LoadedItems
      {
         get 
         {
            if (fLoadedItems == null)
            {
               fLoadedItems = new List<string>();
               foreach (Assembly i in AppDomain.CurrentDomain.GetAssemblies())
                  fLoadedItems.Add(i.CodeBase);
               fLoadedItems.Sort();
            }
            return fLoadedItems;
         }
      }

      #endregion

      
      #region SelectedItem

      /// <summary>
      /// Gets the assembly that is currently selected.
      /// </summary>
      public string SelectedItem
      {
         get
         {
            return fSelectedItem;
         }
         internal set
         {
            fSelectedItem = value;
         }
      }

      #endregion

      /// <summary>
      /// Called when [click ok].
      /// </summary>
      /// <param name="sender">The sender.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void OnClickOk(object sender, RoutedEventArgs e)
      {
         if (TabLoaded.IsSelected == true)
            SelectedItem = (string)LstLoaded.SelectedItem;
         else
            SelectedItem = (string)LstGAC.SelectedItem;
         DialogResult = true;
      }
   }
}
