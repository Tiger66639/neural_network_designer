﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Xml;
using System.Xml.Serialization;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using JaStDev.HAB.Designer.BrainData;
using JaStDev.HAB.Designer.Editors.@base;
using JaStDev.HAB.Designer.Editors.Frames;
using JaStDev.HAB.Designer.Editors.Overview;
using JaStDev.HAB.Framenet;
using Frame = JaStDev.HAB.Designer.Editors.Frames.Frame;

namespace JaStDev.HAB.Designer.Dialogs
{
   /// <summary>
   /// Interaction logic for DlgImportFrameNet.xaml
   /// </summary>
   public partial class DlgImportFrameNet : Window
   {
      #region fields
      FrameNet fFrameNet;
      int iSearchIndex = -1;
      List<JaStDev.HAB.Framenet.Frame> fSelected;         //required to reset previous  selection when select all checkbox is indeterminate. 
      ObservableCollection<FrameEditor> fFrameEditors;
      #endregion


      #region ctor
      /// <summary>
      /// Initializes a new instance of the <see cref="DlgImportFrameNet"/> class.
      /// </summary>
      public DlgImportFrameNet()
      {
         InitializeComponent();
         LoadFrameNet();
         DataContext = fFrameNet;
      } 
      #endregion

      #region prop

      #region ShowImportInto

      /// <summary>
      /// ShowImportInto Dependency Property
      /// </summary>
      public static readonly DependencyProperty ShowImportIntoProperty =
          DependencyProperty.Register("ShowImportInto", typeof(bool), typeof(DlgImportFrameNet),
              new FrameworkPropertyMetadata((bool)true));

      /// <summary>
      /// Gets or sets the ShowImportInto property.  This dependency property 
      /// indicates wether the 'Import into' drop down box is displayed or not.
      /// </summary>
      public bool ShowImportInto
      {
         get { return (bool)GetValue(ShowImportIntoProperty); }
         set { SetValue(ShowImportIntoProperty, value); }
      }

      #endregion



      #region ImportInto

      /// <summary>
      /// ImportInto Dependency Property
      /// </summary>
      public static readonly DependencyProperty ImportIntoProperty =
          DependencyProperty.Register("ImportInto", typeof(FrameEditor), typeof(DlgImportFrameNet),
              new FrameworkPropertyMetadata((FrameEditor)null));

      /// <summary>
      /// Gets or sets the ImportInto property.  This dependency property 
      /// indicates into which FrameEditor the frames should be imported.
      /// Not required for import to work.
      /// </summary>
      public FrameEditor ImportInto
      {
         get { return (FrameEditor)GetValue(ImportIntoProperty); }
         set { SetValue(ImportIntoProperty, value); }
      }

      #endregion

      #region FrameEditors

      /// <summary>
      /// Gets the list of available frames into which the item can be imported.
      /// </summary>
      public IList<FrameEditor> FrameEditors
      {
         get 
         {
            if (fFrameEditors == null)
            {
               fFrameEditors = new ObservableCollection<FrameEditor>();
               GetFrames(BrainData.BrainData.Current.Editors, fFrameEditors);
            }
            return fFrameEditors;
         }
      }

      private void GetFrames(IList<EditorBase> list, IList<FrameEditor> res)
      {
         foreach (EditorBase i in list)
         {
            if (i is FrameEditor)
               res.Add((FrameEditor)i);
            else if (i is EditorFolder)
               GetFrames(((EditorFolder)i).Items, res);
         }
      }


      #endregion



      #endregion

      #region load
      void LoadFrameNet()
      {
         try
         {
            if (string.IsNullOrEmpty(Properties.Settings.Default.FrameNetPath) == false)
            {
               LoadFrames();
               LoadLUMaps();
               LoadFEMaps();
            }
            else
               Log.Log.LogError("FrameNetSin.FrameNet", "No location specified where to find the framenet data.");
         }
         catch (Exception e)
         {
            MessageBox.Show("Failed to load FrameNet database!", "Import from FrameNet", MessageBoxButton.OK, MessageBoxImage.Error);
            Log.Log.LogError("DlgImportFrameNet.LoadFrameNet", e.ToString());
            Close();
         }
      }

      /// <summary>
      /// Loads the file: 'BasicXml\\frXML\\frames.xml'
      /// </summary>
      private void LoadFEMaps()
      {
         string iPath = Path.Combine(Properties.Settings.Default.FrameNetPath, "FEMapToWordnet.xml");
         if (File.Exists(iPath) == true)
         {
            XmlSerializer valueSerializer = new XmlSerializer(typeof(SerializableDictionary<int, int>));
            using (XmlReader iReader = XmlReader.Create(iPath))
               fFrameNet.WordNetMapFE = (IDictionary<int, int>)valueSerializer.Deserialize(iReader);
         }
         else
            fFrameNet.WordNetMapFE = new SerializableDictionary<int, int>();
      }

      /// <summary>
      /// Loads the file: 'BasicXml\\frXML\\LUMapToWordnet.xml'
      /// </summary>
      private void LoadLUMaps()
      {
         string iPath = Path.Combine(Properties.Settings.Default.FrameNetPath, "LUMapToWordnet.xml");
         if (File.Exists(iPath) == true)
         {
            XmlSerializer valueSerializer = new XmlSerializer(typeof(SerializableDictionary<int, int>));
            using (XmlReader iReader = XmlReader.Create(iPath))
               fFrameNet.WordNetMapLU = (IDictionary<int, int>)valueSerializer.Deserialize(iReader);
         }
         else
            fFrameNet.WordNetMapLU = new SerializableDictionary<int, int>();
      }

      /// <summary>
      /// Loads the file: 'BasicXml\\frXML\\frames.xml'
      /// </summary>
      private void LoadFrames()
      {
         string iPath = Path.Combine(Properties.Settings.Default.FrameNetPath, "frames.xml");
         if (File.Exists(iPath) == true)
         {
            XmlSerializer valueSerializer = new XmlSerializer(typeof(FrameNet));
            XmlReaderSettings iSettings = new XmlReaderSettings() { ProhibitDtd = false };                     //framenet contains a dtd.  We need to change default reader settings of .net xmlreader to allow for this.
            using (XmlReader iReader = XmlReader.Create(iPath, iSettings))
               fFrameNet = (FrameNet)valueSerializer.Deserialize(iReader);
         }
         else
            Log.Log.LogError("FrameNetSin.FrameNet", string.Format("Can't find {0}.", iPath));
      } 
      #endregion


      private void SearchNext_Click(object sender, RoutedEventArgs e)
      {
         string iToSearch = TxtSearch.Text;
         for (int i = iSearchIndex + 1; i < fFrameNet.Frames.Count; i++)
         {
            JaStDev.HAB.Framenet.Frame iFrame = fFrameNet.Frames[i];
            if (iFrame.Description.Contains(iToSearch) == true || iFrame.Name.Contains(iToSearch) == true)
            {
               LstFrames.SelectedIndex = i;
               LstFrames.ScrollIntoView(LstFrames.SelectedItem);
               iSearchIndex = i;
               return;
            }
         }
         MessageBox.Show("End reached, going to start.");
         iSearchIndex = -1;
      }


      /// <summary>
      /// Called when ok is clicked.
      /// </summary>
      /// <param name="sender">The sender.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void BtnImport_Click(object sender, RoutedEventArgs e)
      {
         var iSelected = (from i in  fFrameNet.Frames
                          where i.IsSelected == true
                          select i).ToList();                                              //we convert to list so that we only have to walk through the intire list 1 time.

         if (CheckWordNetLinks(iSelected) == true)                                        //all items check out ok.
         {
            FrameEditor iImportInto = ImportInto;
            foreach (JaStDev.HAB.Framenet.Frame iFrame in iSelected)
            {
               NeuronCluster iEvokers;
               NeuronCluster iNew = iFrame.Import(out iEvokers);
               if (iImportInto != null)                                                                   //if we
                  iImportInto.Frames.Add(new Frame(iNew));
               NeuronData iInfo = BrainData.BrainData.Current.NeuronInfo[iNew.ID];
               iInfo.StoreDescription(iFrame.Description);                 //set the description
               iInfo.DisplayTitle = iFrame.Name;
               int i = 0;
               foreach (JaStDev.HAB.Framenet.FrameElement iEl in iFrame.Elements)                        //set the info for all the frame elements (objects in neural network)
               {
                  using (ChildrenAccessor iList = iNew.Children)
                     iInfo = BrainData.BrainData.Current.NeuronInfo[iList[i++]];
                  iInfo.StoreDescription(iEl.Description);                 //set the description
                  iInfo.DisplayTitle = iEl.Name;
               }
               i = 0;
               using (ChildrenAccessor iEvokersList = iEvokers.Children)
               {
                  foreach (JaStDev.HAB.Framenet.LexUnit iLex in iFrame.LexUnits)                            //set the info for all the lexical units (objects in neural network)
                  {
                     if (iLex.Lexemes.Count == 1)                                                           //if there is more than 1 lexeme, we always use the lexemes as evokers (the word parts) cause they are easier to search by the neural net.  This means that we must keep the iEvokers list in sync.
                     {
                        iInfo = BrainData.BrainData.Current.NeuronInfo[iEvokersList[i++]];
                        iInfo.StoreDescription(iLex.Description);
                        iInfo.DisplayTitle = iLex.Name;
                     }
                     else
                     {
                        foreach (JaStDev.HAB.Framenet.Lexeme iLexeme in iLex.Lexemes)
                        {
                           iInfo = BrainData.BrainData.Current.NeuronInfo[iEvokersList[i++]];
                           string iDesc = string.Format("Part of Lexical unit {0} ({1}), desc: {2}", iLex.ID, iLex.Name, iLex.Description);
                           iInfo.StoreDescription(iDesc);
                           iInfo.DisplayTitle = iLexeme.Value;
                        }
                     }
                  }
               }
            }
            MessageBox.Show("Import finished successfully!", "Import from FrameNet", MessageBoxButton.OK, MessageBoxImage.Information);
            Close();
         }
      }

      /// <summary>
      /// Checks if all the word net links have been filled in. If not, an mbox is displayed so the user can update, if needed.
      /// </summary>
      /// <param name="list">The list.</param>
      /// <returns>True: all items check out ok or user chose to ignore, false: some</returns>
      private bool CheckWordNetLinks(List<JaStDev.HAB.Framenet.Frame> list)
      {
         foreach (JaStDev.HAB.Framenet.Frame iFrame in list)
         {
            foreach (LexUnit iLu in iFrame.LexUnits)
            {
               foreach (Lexeme iLex in iLu.Lexemes)
               {
                  if (iLex.WordNetID == 0)
                  {
                     MessageBoxResult iRes = MessageBox.Show(string.Format("Lexeme {2} ({3}), in Lexical unit {1} ({0} has no mapping to wordnet, Continue?", iLu.ID, iLu.Name, iLex.Value, iLex.ID), "Missing mapping", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No);
                     if (iRes == MessageBoxResult.No)
                        return false;
                  }
               }
               if (iLu.WordNetID == 0)
               {
                  MessageBoxResult iRes = MessageBox.Show(string.Format("Lexical unit {0} ({1} has no mapping to wordnet, Continue?", iLu.ID, iLu.Name), "Missing mapping", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No);
                  if (iRes == MessageBoxResult.No)
                     return false;
               }
            }
            foreach (JaStDev.HAB.Framenet.FrameElement iEl in iFrame.Elements)
            {
               if (iEl.WordNetID == 0)
               {
                  MessageBoxResult iRes = MessageBox.Show(string.Format("Frame element {0} ({1} has no mapping to wordnet, Continue?", iEl.ID, iEl.Name), "Missing mapping", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No);
                  if (iRes == MessageBoxResult.No)
                     return false;
               }
            }
         }
         return true;
      }


      /// <summary>
      /// Handles the Click event of the BtnCancel control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void BtnClose_Click(object sender, RoutedEventArgs e)
      {
         Close();
      }

      private void TxtSearch_KeyDown(object sender, KeyEventArgs e)
      {
         if (e.Key == Key.Enter)
            SearchNext_Click(sender, null);
         else
            iSearchIndex = -1;                                                      //when we search for a new word,start from the beginning.
      }

      /// <summary>
      /// Handles the Indeterminate event of the ChkSelectAl control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void ChkSelectAl_Indeterminate(object sender, RoutedEventArgs e)
      {
         
         if (fSelected != null)                                         //this event is triggered whenever an 'IsSelected' checkbox is clicked because the ChkSelectAll is reset.  Only need to reset when the sender is Chk
         {
            foreach (JaStDev.HAB.Framenet.Frame i in fFrameNet.Frames)
               i.IsSelected = false;
            foreach (JaStDev.HAB.Framenet.Frame i in fSelected)
               i.IsSelected = true;
            fSelected = null;
         }
      }

      /// <summary>
      /// Handles the Unchecked event of the ChkSelectAl control.
      /// </summary>
      /// <remarks>
      /// Follows indeterminate: so fill the selection  list.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void ChkSelectAl_Unchecked(object sender, RoutedEventArgs e)
      {
         fSelected = (from i in fFrameNet.Frames
                      where i.IsSelected == true
                      select i).ToList();
         foreach (JaStDev.HAB.Framenet.Frame i in fFrameNet.Frames)
            i.IsSelected = false;
      }

      /// <summary>
      /// Handles the Checked event of the ChkSelectAl control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void ChkSelectAl_Checked(object sender, RoutedEventArgs e)
      {
         foreach (JaStDev.HAB.Framenet.Frame i in fFrameNet.Frames)
            i.IsSelected = true;
      }

      /// <summary>
      /// Handles the Click event of the ChkIsSelected control.
      /// </summary>
      /// <remarks>
      /// Must make certain that the ChkSelectAll is kept in sync: whenever an item is changed in the selection: we
      /// set the item to indetermined.
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
      private void ChkIsSelected_Click(object sender, RoutedEventArgs e)
      {
         ChkSelectAll.IsChecked = null;
      }

      /// <summary>
      /// Handles the Closing event of the Window control.
      /// </summary>
      /// <remarks>
      /// Need to save the mappings to wordnet!
      /// </remarks>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
      private void Window_Closing(object sender, CancelEventArgs e)
      {
         if (fFrameNet != null)
         {
            MessageBoxResult iRes = MessageBoxResult.None;                                         //we init to invalid state, this way, we only have to ask 1 time for 2 lists -> we can check if it has already been asked.
            if (fFrameNet.WordNetMapLUChanged == true)
            {
               iRes = MessageBox.Show("The mappings to wordnet have changed, would you like to save them?", "Mapping from FrameNet to WordNet", MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.Yes);
               if (iRes == MessageBoxResult.Yes)
               {
                  string iPath = Path.Combine(Properties.Settings.Default.FrameNetPath, "BasicXml\\frXML\\LUMapToWordnet.xml");
                  XmlSerializer iSer = new XmlSerializer(typeof(SerializableDictionary<int, int>));
                  using (XmlTextWriter iWriter = new XmlTextWriter(iPath, Encoding.Default))
                     iSer.Serialize(iWriter, fFrameNet.WordNetMapLU);
               }
               else if (iRes == MessageBoxResult.Cancel)
               {
                  e.Cancel = true;
                  return;
               }
            }
            if (fFrameNet.WordNetMapFEChanged == true)
            {
               if (iRes == MessageBoxResult.None)
                  iRes = MessageBox.Show("The mappings to wordnet have changed, would you like to save them?", "Mapping from FrameNet to WordNet", MessageBoxButton.YesNoCancel, MessageBoxImage.Question, MessageBoxResult.Yes);
               if (iRes == MessageBoxResult.Yes)
               {
                  string iPath = Path.Combine(Properties.Settings.Default.FrameNetPath, "BasicXml\\frXML\\FEMapToWordnet.xml");
                  XmlSerializer iSer = new XmlSerializer(typeof(SerializableDictionary<int, int>));
                  using (XmlTextWriter iWriter = new XmlTextWriter(iPath, Encoding.Default))
                     iSer.Serialize(iWriter, fFrameNet.WordNetMapFE);
               }
               else if (iRes == MessageBoxResult.Cancel)
                  e.Cancel = true;
            }
         }
      }

      private void NewFrameEditor_Executed(object sender, ExecutedRoutedEventArgs e)
      {
         if (BrainData.BrainData.Current.CurrentEditorsList != null)
         {
            FrameEditor iNew = new FrameEditor();
            iNew.Name = "New frame editor";
            BrainData.BrainData.Current.CurrentEditorsList.Add(iNew);
            if (fFrameEditors != null)                                           //need to make it available to the dialog as well.
               fFrameEditors.Add(iNew);
            ImportInto = iNew;
         }
         else
            throw new InvalidOperationException("There is no editors list selected to put new items in.");
      }
   }

}
