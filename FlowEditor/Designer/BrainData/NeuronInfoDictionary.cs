﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Threading;
using System.Xml.Serialization;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.WPF.BrainEventManagers;
using JaStDev.HAB.Events;

namespace JaStDev.HAB.Designer.BrainData
{
    /// <summary>
    /// a Container for <see cref="BrainData"/> objects.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Makes certain that only <see cref="NeuronData"/> objects that contain data are stored +
    /// that empty objects are available and commited when needed.
    /// </para>
    /// <para>
    /// When an item is requested that does not yet exist, it is created and stored in a temp dict.
    /// The first time that one of the properties of the item is changed, it is commited and moved
    /// to the actual list.
    /// </para>
    /// <para>
    /// For this to work properly, NeuronData objects must be cleaned up after usage, so they must implement
    /// iDisposable + this dict uses a weakreference.
    /// </para>
    /// <para>
    /// This is done so that we only store objects that have data since there can be far more neurons
    /// than there can be nodes stored in a dictionary.  With this technique, we always provide an
    /// object, but don't need to have ulong.max nr of objects.
    /// </para>
    /// <para>
    /// Always call <see cref="NeuronInfoDictionary.Dispose"/> after you are done with it so it can be released from memory,
    /// failing to do so causes a memory leak.
    /// </para>
    /// </remarks>
    public class NeuronInfoDictionary : IWeakEventListener
    {
        #region fields

        private SerializableDictionary<ulong, NeuronData> fItems = new SerializableDictionary<ulong, NeuronData>();
        private Dictionary<ulong, WeakReference> fTempItems = new Dictionary<ulong, WeakReference>();

        /// <summary>
        /// the size that the temp list must have before a clean operatio is issued.
        /// </summary>
        private const int MaxTempCount = 1000;

        #endregion fields

        #region ctor

        /// <summary>
        /// Default constructor.
        /// </summary>
        public NeuronInfoDictionary()
        {
            NeuronChangedEventManager.AddListener(Brain.Brain.Current, this);
        }

        ~NeuronInfoDictionary()
        {
            NeuronChangedEventManager.RemoveListener(Brain.Brain.Current, this);
        }

        #endregion ctor

        /// <summary>
        /// Gets the extra info for the specified neuron, if it doesn't exist, one is created and
        /// added to the temp list.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [XmlIgnore]
        public NeuronData this[ulong id]
        {
            get
            {
                NeuronData iRes;
                if (fItems.TryGetValue(id, out iRes) == false)
                {
                    WeakReference iWeakRes;
                    if (fTempItems.TryGetValue(id, out iWeakRes) == true && iWeakRes.IsAlive == true)
                        iRes = (NeuronData)iWeakRes.Target;
                    else
                    {
                        Neuron iNeuron;
                        if (Brain.Brain.Current.TryFindNeuron(id, out iNeuron) == false)
                            return null;
                        iRes = new NeuronData(iNeuron);                                                  //not found for some reason so create new object..
                        if (iWeakRes == null)                                                            //it could be that we found something in the dict, but was garbage collected, in which case we reuse the object.
                            iWeakRes = new WeakReference(iRes);
                        else
                            iWeakRes.Target = iRes;
                        if (fTempItems.Count > MaxTempCount)
                            CleanTemp();
                        fTempItems[id] = iWeakRes;
                    }
                }
                return iRes;
            }
        }

        /// <summary>
        /// Primarely supplied for streaming.
        /// </summary>
        public SerializableDictionary<ulong, NeuronData> Items
        {
            get
            {
                return fItems;
            }
            set
            {
                if (value == null)
                    throw new NullReferenceException();
                fItems = value;
            }
        }

        /// <summary>
        /// Removes all weak references that are no longer valid.
        /// </summary>
        private void CleanTemp()
        {
            List<ulong> iToRemove = new List<ulong>();
            foreach (KeyValuePair<ulong, WeakReference> i in fTempItems)
            {
                if (i.Value.IsAlive == false)
                    iToRemove.Add(i.Key);
            }
            foreach (ulong i in iToRemove)
                fTempItems.Remove(i);
        }

        /// <summary>
        /// moves a temp data neuron to the full list.
        /// </summary>
        /// <param name="item"></param>
        internal void Commit(NeuronData item)
        {
            if (fItems.ContainsKey(item.ID) == false)
            {
                try
                {
                    fItems.Add(item.ID, item);
                    fTempItems.Remove(item.ID);
                }
                catch (Exception e)
                {
                    throw new ArgumentOutOfRangeException("Failed to store extra neuron data!", e);
                }
            }
        }

        /// <summary>
        /// Called during a garbage collection of the item, so that we can remove if from our intenal list.
        /// </summary>
        /// <param name="item">The item being cleaned up.</param>
        internal void RemoveTemp(NeuronData item)
        {
            if (fTempItems.ContainsKey(item.ID) == true)
                fTempItems.Remove(item.ID);
        }

        /// <summary>
        /// Adds a neuronData object to the list containing all the temporary generated neurondata items (without extra info).
        /// </summary>
        /// <remarks>
        /// This is called when a temp neuron gets registered with the brain.
        /// </remarks>
        /// <param name="NeuronInfo">The neuron info.</param>
        internal void AddTemp(NeuronData NeuronInfo)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Event handler for <see cref="Brain.NeuronChanged"/>
        /// </summary>
        /// <remarks>
        /// Checks if it is a property change, if so, checks if there is an item in our dicts, if so,
        /// raise the correct events on it.
        /// </remarks>
        private void Current_NeuronChanged(object sender, NeuronChangedEventArgs e)
        {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<NeuronChangedEventArgs>(InternalNeuronChanged), e);
        }

        private void InternalNeuronChanged(NeuronChangedEventArgs e)
        {
            Neuron iSender = e.OriginalSource;
            if (e is NeuronPropChangedEventArgs)
            {
                NeuronData iRes = null;
                if (fItems.TryGetValue(iSender.ID, out iRes) == false)
                {
                    WeakReference iWeakRes;
                    if (fTempItems.TryGetValue(iSender.ID, out iWeakRes) == true && iWeakRes.IsAlive == true)
                        iRes = (NeuronData)iWeakRes.Target;
                }
                if (iRes != null)
                    iRes.NeuronPropChanged(((NeuronPropChangedEventArgs)e).Property);
            }
            else if (e.Action == BrainAction.Removed)                                                 //if the neuron was removed, also remove it's extra info.
            {
                if (fItems.Remove(e.OriginalSourceID) == false)
                    fTempItems.Remove(e.OriginalSourceID);
            }
            else if (e.Action == BrainAction.Changed)
            {
                NeuronData iRes = null;
                if (fItems.TryGetValue(e.NewValue.ID, out iRes) == false)                                 //we search on NewValue.Id cause this is assigned, old can be cleaned out.
                {
                    WeakReference iWeakRes;
                    if (fTempItems.TryGetValue(e.NewValue.ID, out iWeakRes) == true && iWeakRes.IsAlive == true)
                        iRes = (NeuronData)iWeakRes.Target;
                }
                if (iRes != null)
                    iRes.NeuronChanged(e.NewValue);
            }
        }

        #region IWeakEventListener Members

        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            if (managerType == typeof(NeuronChangedEventManager))
            {
                Current_NeuronChanged(sender, (NeuronChangedEventArgs)e);
                return true;
            }
            else
                return false;
        }

        #endregion IWeakEventListener Members

        /// <summary>
        /// Finds the next item.
        /// </summary>
        /// <remarks>
        /// Does a case insensitive search.
        /// </remarks>
        /// <param name="id">The id to start searching from.</param>
        /// <param name="value">The value to search in the display title.</param>
        internal ulong FindNext(ulong id, string value)
        {
            value = value.ToLower();
            NeuronData iFound = null;
            while (++id < Brain.Brain.Current.NextID)
            {
                if (id >= (ulong)PredefinedNeurons.EndOfStatic && id <= (ulong)PredefinedNeurons.Dynamic)
                    continue;
                iFound = this[id];
                if (iFound != null && iFound.DisplayTitle != null &&
                   iFound.DisplayTitle.ToLower().Contains(value) == true)
                    break;
            }
            if (iFound != null)
                return iFound.ID;
            else
                return Neuron.EmptyId;
        }
    }
}