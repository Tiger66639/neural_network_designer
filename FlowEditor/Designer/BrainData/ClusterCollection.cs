﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems;
using JaStDev.HAB.Designer.WPF.BrainEventManagers;
using JaStDev.HAB.Events;

namespace JaStDev.HAB.Designer.BrainData
{
    /// <summary>
    /// A base class for all Collections that wrap a <see cref="NeuronCluster"/>.  This collection will automatically keep sync
    /// with the content of a cluster.  The actual item in the collection is generic, as long at it's a neuron wrapper.
    /// </summary>
    /// <remarks>
    /// doesn't raise events when the list is changed cause this confuses the undo system.  Instead, drag
    /// handlers and functions generate their own undo data.
    /// </remarks>
    /// <typeparam name="T">The object type used to wrap the children of the cluster with.</typeparam>
    public abstract class ClusterCollection<T> : ObservedCollection<T>, IWeakEventListener where T : INeuronWrapper
    {
        #region fields

        private NeuronCluster fChildList;
        private ulong fLinkMeaning = Neuron.EmptyId;                                   //if it is a temp cluster, it means we need to add the NeuronCluster to the owner.Item.  Neuron.EmptyId means that it is already added or not needed.
        private bool fInternalChange = false;                                           //keeps track if the change in the underlying neuronlist is done from here or somewhere else.

        #endregion fields

        #region ctor

        /// <summary>
        /// Default constructor to use with a <see cref="NeuronCluster"/> that is already registered with the owner,
        /// and therefor possibly already has code.
        /// </summary>
        /// <param name="owner">The <see cref="CodeEditor"/> that contains this code list. </param>
        /// <param name="childList">The NeuronCluster that contains all the code items.</param>
        public ClusterCollection(INeuronWrapper owner, NeuronCluster childList)
           : base(owner)
        {
            Debug.Assert(childList != null);
            fChildList = childList;

            using (ChildrenAccessor iList = childList.Children)
            {
                IList<Neuron> iItems = iList.ConvertTo<Neuron>();
                if (iItems != null)
                {
                    foreach (Neuron i in iItems)
                        base.InsertItemDirect(Count, GetWrapperFor(i));                   //we take the base, otherwise we would reinsert it again.
                }
            }
            InternalCreate();
        }

        /// <summary>
        /// Default constructor to use for a <see cref="NeuronCluster"/> that is not yet declared (empty,
        /// and therefor still needs to be created, specifically for code or argument lists (will automatically
        /// generate a cluster meaning depending on the linkmeaning).
        /// </summary>
        /// <remarks>
        /// This cluster will only be registered if data is added.  This prevents us from creating clusters that only
        /// to view the code.
        /// </remarks>
        /// <param name="owner">The <see cref="CodeEditor"/> that contains this code list. </param>
        /// <param name="linkMeaning">The id that should be used as meaning if data is added to the list.</param>
        public ClusterCollection(INeuronWrapper owner, ulong linkMeaning)
           : base(owner)
        {
            fChildList = new NeuronCluster();
            fChildList.Meaning = GetListMeaning(linkMeaning);
            Brain.Brain.Current.MakeTemp(fChildList);                                                       //if we don't do this, the cluster has the 0 id, which is not registered with the brain, this will register the neuron, the first time it is used in a link.
            fLinkMeaning = linkMeaning;
            InternalCreate();
        }

        /// <summary>
        /// Returns the meaning that should be assigned to the cluster when it is newly created.
        /// </summary>
        /// <param name="linkMeaning">The meaning of the link between the wrapped cluster and the owner of this collection.</param>
        /// <returns></returns>
        protected abstract ulong GetListMeaning(ulong linkMeaning);

        /// <summary>
        /// Default constructor to use for a <see cref="NeuronCluster"/> that is not yet declared (empty,
        /// and therefor still needs to be created.
        /// </summary>
        /// <param name="owner">The <see cref="CodeEditor"/> that contains this code list.</param>
        /// <param name="linkMeaning">The id that should be used as meaning if data is added to the list.</param>
        /// <param name="clusterMeaning">The id that should be used as the meaning for the cluster.</param>
        /// <remarks>
        /// This cluster will only be registered if data is added.  This prevents us from creating clusters that only
        /// to view the code.
        /// </remarks>
        public ClusterCollection(INeuronWrapper owner, ulong linkMeaning, ulong clusterMeaning)
           : base(owner)
        {
            fChildList = new NeuronCluster();
            fChildList.Meaning = clusterMeaning;
            Brain.Brain.Current.MakeTemp(fChildList);                                                       //if we don't do this, the cluster has the 0 id, which is not registered with the brain, this will register the neuron, the first time it is used in a link.
            fLinkMeaning = linkMeaning;
            InternalCreate();
        }

        /// <summary>
        /// Internally creates the item.
        /// </summary>
        private void InternalCreate()
        {
            NeuronListChangedEventManager.AddListener(Brain.Brain.Current, this);
            NeuronChangedEventManager.AddListener(Brain.Brain.Current, this);
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="CodeItemCollection"/> is reclaimed by garbage collection.
        /// </summary>
        ~ClusterCollection()
        {
            NeuronListChangedEventManager.RemoveListener(Brain.Brain.Current, this);
            NeuronChangedEventManager.RemoveListener(Brain.Brain.Current, this);
        }

        #endregion ctor

        #region prop

        #region Cluster

        /// <summary>
        /// Gets the cluster used to store all the code items in the brain.
        /// </summary>
        public NeuronCluster Cluster
        {
            get { return fChildList; }
        }

        #endregion Cluster

        #region MeaningID

        /// <summary>
        /// Gets the ID of the meaning that is used for this code.
        /// </summary>
        /// <remarks>
        /// This property is provided so that you can easely access this value.  <see cref="CodeItemCollection.Cluster"/> only
        /// gets created when there is data, but the collection already knows which list type to use, so it can return this value.
        /// </remarks>
        /// <value>The meaning.</value>
        public ulong MeaningID
        {
            get
            {
                if (fLinkMeaning != Neuron.EmptyId)                               //this field is also used as a switch, when it is 'EmptyId', it means that there is a cluster created.
                    return fLinkMeaning;
                else
                    return Cluster.Meaning;
            }
        }

        #endregion MeaningID

        /*
   * Possible future extention for virtualization.
   *

        #region ChildrenLoaded

        /// <summary>
        /// Gets/sets if the <see cref="CodeItem.Children"/> list is loaded or not.
        /// </summary>
        /// <remarks>
        /// This allows for dynamic UI loading.
        /// </remarks>
        public bool ChildrenLoaded
        {
           get
           {
              return fChildrenLoaded;
           }
           set
           {
              if (fChildrenLoaded != value)
              {
                 fChildrenLoaded = value;
                 if (fChildren != null)                                            //only try to do something if there is a list to work on, this can be null if HasChildren is false.
                 {
                    if (value == true)
                       LoadChildren();
                    else
                       fChildren.Clear();
                 }
                 OnPropertyChanged("ChildrenLoaded");
              }
           }
        }

        private void LoadChildren()
        {
           ConditionalStatement iGroup = (ConditionalStatement)fItem;
           foreach (ConditionalExpression i in iGroup.Conditions)
              fChildren.Add(new CodeItem(i));
        }

        #endregion ChildrenLoaded

        #region HasChildren

        /// <summary>
        /// Gets if this <see cref="CodeItem.Expression"/> has child expressions.
        /// </summary>
        /// <remarks>
        /// This property is automatically set to true when a <see cref="conditionalGroup"/> or
        /// <see cref="conditionalExpression"/> is wrapped.
        /// </remarks>
        public bool HasChildren
        {
           get
           {
              NeuronCluster iCluster = ((ConditionalStatement)Item).ConditionsCluster;
              return iCluster != null && iCluster.Children.Count > 0;
           }
        }

        #endregion HasChildren

   */

        #endregion prop

        #region abstract

        /// <summary>
        /// Called when a new wrapper object needs to be created for a neuron.
        /// </summary>
        /// <remarks>
        /// CodeEditors do: return CodeEditor.CreateCodeItemFor(toWrap)
        /// </remarks>
        /// <param name="toWrap">To wrap.</param>
        /// <returns></returns>
        abstract public T GetWrapperFor(Neuron toWrap);

        #endregion abstract

        #region Functions

        /// <summary>
        /// Called when the underlying list (fChildList.Children) is changed.  Makes certain that changes
        /// are reflected in this list as well. To make certain that changes made in the neuronlist triggered
        /// by changes in this list don't create a loop, we check for the fModifying field.  This is internally
        /// set whenever we make a change.
        /// <para>
        /// We make certain that all changes to this list are done on the UI Thread. changes to the NeuronList are
        /// usually done from different threads.
        /// </para>
        /// </summary>
        private void Current_NeuronListChanged(object sender, NeuronListChangedEventArgs e)
        {
            if (fInternalChange == false)
                App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<NeuronListChangedEventArgs>(InternalNeuronlistChanged), e);
        }

        /// <summary>
        /// thread local handler of changes to the neuron list.
        /// </summary>
        /// <param name="e">the events arguments</param>
        /// <param name="internalchange">the value of internalChange at the exact time the event was raised, required cause we don't react correctly
        /// to changes done from this class otherwise.</param>
        private void InternalNeuronlistChanged(NeuronListChangedEventArgs e)
        {
            if (e.IsListFrom(fChildList) == true)
            {
                switch (e.Action)
                {
                    case NeuronListChangeAction.Insert:
                        base.InsertItemDirect(e.Index, GetWrapperFor(e.Item));
                        break;

                    case NeuronListChangeAction.Remove:
                        base.RemoveItemDirect(e.Index);
                        break;

                    default:
                        throw new InvalidOperationException("Unkown list change.");
                }
            }
        }

        private void NeuronChanged(object sender, NeuronChangedEventArgs e)
        {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<NeuronChangedEventArgs>(InternalNeuronChanged), e);
        }

        /// <summary>
        /// Handles the neuronchanged event handler.
        /// </summary>
        /// <remarks>
        /// Checks if a neuron changed, if so, and it is contained in our list, than we must update the item in the list
        /// </remarks>
        /// <param name="sender">The sender.</param>
        /// <param name="neuronChangedEventArgs">The <see cref="NeuronChangedEventArgs"/> instance containing the event data.</param>
        private void InternalNeuronChanged(NeuronChangedEventArgs e)
        {
            if (e.Action == BrainAction.Changed && !(e is NeuronPropChangedEventArgs))
            {
                using (ChildrenAccessor iList = fChildList.Children)
                {
                    int iIndex = iList.IndexOf(e.OriginalSource.ID);
                    if (iIndex > -1)
                        this[iIndex] = GetWrapperFor(e.NewValue);
                }
            }
        }

        /// <summary>
        /// We don't call base, this is done by the event handler that monitors changes to the list.
        /// If we don't use this technique, actions performed by the user will be done 2 times: once
        /// normally, once in response to the list change in the neuron.
        /// </summary>
        protected override void ClearItems()
        {
            using (ChildrenAccessor iList = fChildList.ChildrenW)                                                                //we lock before we set fInternalChange to make certain no one can change something in between calls
            {
                fInternalChange = true;
                try
                {
                    iList.Clear();
                    base.ClearItems();
                }
                finally
                {
                    fInternalChange = false;
                }
            }
        }

        /// <summary>
        /// Adds the item without raising events.
        /// </summary>
        /// <param name="item">The item.</param>
        public void AddWithoutEvents(T item)
        {
            using (ChildrenAccessor iList = fChildList.ChildrenW)                                                                //we lock before we set fInternalChange to make certain no one can change something in between calls
            {
                fInternalChange = true;
                try
                {
                    if (fLinkMeaning != Neuron.EmptyId)                                                                //need to register the NeuronCluster
                    {
                        Link iLink = new Link(fChildList, ((INeuronWrapper)Owner).Item, fLinkMeaning);
                        fLinkMeaning = Neuron.EmptyId;                                                                  //need to indicate that the action is completed.
                    }
                    iList.Add(item.Item);
                    base.InsertItemDirect(Count, item);
                }
                finally
                {
                    fInternalChange = false;
                }
            }
        }

        /// <summary>
        /// Inserts the item.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="item">The item.</param>
        protected override void InsertItem(int index, T item)
        {
            using (ChildrenAccessor iList = fChildList.ChildrenW)                                                                //we lock before we set fInternalChange to make certain no one can change something in between calls
            {
                fInternalChange = true;
                try
                {
                    if (fLinkMeaning != Neuron.EmptyId)                                                                //need to register the NeuronCluster
                    {
                        Link iLink = new Link(fChildList, ((INeuronWrapper)Owner).Item, fLinkMeaning);
                        fLinkMeaning = Neuron.EmptyId;                                                                  //need to indicate that the action is completed.
                    }
                    iList.Insert(index, item.Item);
                    base.InsertItem(index, item);
                }
                finally
                {
                    fInternalChange = false;
                }
            }
        }

        /// <summary>
        /// Performs an insert which generates events.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="value">The value.</param>
        public void InsertWithoutEvents(int index, T value)
        {
            if (index > -1 && index < Count)
            {
                using (ChildrenAccessor iList = fChildList.ChildrenW)                                                                //we lock before we set fInternalChange to make certain no one can change something in between calls
                {
                    fInternalChange = true;
                    try
                    {
                        if (fLinkMeaning != Neuron.EmptyId)                                                                //need to register the NeuronCluster
                        {
                            Link iLink = new Link(fChildList, ((INeuronWrapper)Owner).Item, fLinkMeaning);
                            fLinkMeaning = Neuron.EmptyId;                                                                  //need to indicate that the action is completed.
                        }
                        iList.Insert(index, value.Item);
                        base.InsertItemDirect(index, value);
                    }
                    finally
                    {
                        fInternalChange = false;
                    }
                }
            }
            else
                throw new ArgumentOutOfRangeException("index");
        }

        /// <summary>
        /// Moves the item.
        /// </summary>
        /// <remarks>
        /// Always raises the event, so that undo works correctly (through the CodeItemDropAdvisor).
        /// </remarks>
        /// <param name="oldIndex">The old index.</param>
        /// <param name="newIndex">The new index.</param>
        protected override void MoveItem(int oldIndex, int newIndex)
        {
            using (ChildrenAccessor iList = fChildList.ChildrenW)                                                                //we lock before we set fInternalChange to make certain no one can change something in between calls
            {
                fInternalChange = true;
                try
                {
                    Neuron iToMove = this[oldIndex].Item;
                    iList.RemoveAt(oldIndex);
                    iList.Insert(newIndex, iToMove);
                    base.MoveItem(oldIndex, newIndex);
                }
                finally
                {
                    fInternalChange = false;
                }
            }
        }

        /// <summary>
        /// Removes the item.
        /// </summary>
        /// <remarks>
        /// Always raises the event, so that undo works correctly (through the CodeItemDropAdvisor).
        /// </remarks>
        /// <param name="index">The index.</param>
        protected override void RemoveItem(int index)
        {
            using (ChildrenAccessor iList = fChildList.ChildrenW)                                                                //we lock before we set fInternalChange to make certain no one can change something in between calls
            {
                fInternalChange = true;
                try
                {
                    iList.RemoveAt(index);
                    base.RemoveItem(index);
                }
                finally
                {
                    fInternalChange = false;
                }
            }
        }

        /// <summary>
        /// Sets the item.
        /// </summary>
        /// <remarks>
        /// Always raises the event, so that undo works correctly (through the CodeItemDropAdvisor).
        /// </remarks>
        /// <param name="index">The index.</param>
        /// <param name="item">The item.</param>
        protected override void SetItem(int index, T item)
        {
            using (ChildrenAccessor iList = fChildList.ChildrenW)                                                                //we lock before we set fInternalChange to make certain no one can change something in between calls
            {
                fInternalChange = true;
                try
                {
                    iList.RemoveAt(index);
                    iList.Insert(index, item.Item);
                    base.SetItem(index, item);
                }
                finally
                {
                    fInternalChange = false;
                }
            }
        }

        #endregion Functions

        #region IWeakEventListener Members

        /// <summary>
        /// Receives events from the centralized event manager.
        /// </summary>
        /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
        /// <param name="sender">Object that originated the event.</param>
        /// <param name="e">Event data.</param>
        /// <returns>
        /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
        /// </returns>
        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            if (managerType == typeof(NeuronListChangedEventManager))
            {
                Current_NeuronListChanged(sender, (NeuronListChangedEventArgs)e);
                return true;
            }
            else if (managerType == typeof(NeuronChangedEventManager))
            {
                NeuronChanged(sender, (NeuronChangedEventArgs)e);
                return true;
            }
            else
                return false;
        }

        #endregion IWeakEventListener Members
    }
}