﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.Comm_channels;
using JaStDev.HAB.Designer.Comm_channels.Wordnet;
using JaStDev.HAB.Designer.Editors.@base;
using JaStDev.HAB.Designer.Editors.CodeEditor.CodeItems;
using JaStDev.HAB.Designer.Editors.Overview;
using JaStDev.HAB.Designer.Tools.Debugger;
using JaStDev.HAB.Designer.Tools.Thesaurus;
using JaStDev.HAB.Designer.Tools.Toolbox;
using JaStDev.HAB.Expressions;
using JaStDev.HAB.Instructions;
using JaStDev.HAB.Sins;

namespace JaStDev.HAB.Designer.BrainData
{
    /// <summary>
    /// The entry point for the designer regarding data.
    /// </summary>
    public class BrainData : ObservableObject
    {
        #region fields

        private static BrainData fCurrent;
        private DesignerDataFile fDesignerData;
        private object fLastUndoItem;                                                                                          //this is used to check if the data has changed since the last save.

        //ObservableCollection<DebugProcessor> fProcessors = new ObservableCollection<DebugProcessor>();
        private ObservableCollection<CodeEditor> fCodeEditors = new ObservableCollection<CodeEditor>();                     //don't use observedCollection, cause than an add/remove of a codeEditor would also be added to the undo/redo list, which we don't want.

        private ObservableCollection<object> fOpenDocuments = new ObservableCollection<object>();
        private IList<EditorBase> fCurrentEditorsList;
        private List<Neuron> fLinkLists;
        private List<Neuron> fClusterLists;
        private int fLearnCount;

        #endregion fields

        #region Events

        /// <summary>
        /// Raised just before the Brain data specific to the app is saved.
        /// </summary>
        public event EventHandler BeforeSave;

        /// <summary>
        /// Raised just after the Brain data specific to the application is loaded.
        /// </summary>
        public event EventHandler AfterLoad;

        #endregion Events

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="BrainData"/> class.
        /// </summary>
        public BrainData()
        {
            fCodeEditors.CollectionChanged += new NotifyCollectionChangedEventHandler(fCodeEditors_CollectionChanged);
            CollectionChanging += new NotifyCollectionChangingEventHandler(BrainData_CollectionChanging);
        }

        #endregion ctor

        #region Prop

        #region Name

        /// <summary>
        /// Gets/sets the name of the project
        /// </summary>
        public string Name
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.Name;
                return null;
            }
            set
            {
                if (DesignerData != null)
                {
                    OnPropertyChanging("Name", DesignerData.Name, value);
                    DesignerData.Name = value;
                    OnPropertyChanged("Name");
                }
                else
                    throw new InvalidOperationException("No data file loaded.");
            }
        }

        #endregion Name

        #region DesignerData

        /// <summary>
        /// Gets/sets the data specific to the designer.
        /// </summary>
        /// <remarks>
        /// This includes a lot of the properties exposed by this class. This property makes certain that when a new
        /// value is loaded, old data is unloaded and new data is properly registered.
        /// </remarks>
        public DesignerDataFile DesignerData
        {
            get
            {
                return fDesignerData;
            }
            set
            {
                if (value != fDesignerData)
                {
                    if (fDesignerData != null)
                        Unregister(fDesignerData);
                    fDesignerData = value;
                    if (fDesignerData != null)
                        Register(fDesignerData);
                    Type iType = typeof(DesignerDataFile);                                              //for each property in the designerdata, raise the event that it is changed.
                    foreach (PropertyInfo iProp in iType.GetProperties())
                        OnPropertyChanged(iProp.Name);
                }
            }
        }

        #endregion DesignerData

        #region CommChannels

        /// <summary>
        /// Gets the list of communcation channels that are available for the currently loaded <see cref="Brain"/>.
        /// </summary>
        public CommChannelCollection CommChannels
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.CommChannels;
                return null;
            }
        }

        #endregion CommChannels

        #region Debugmode

        /// <summary>
        /// Gets/sets which level of debugging should be used by the <see cref="DebugProcessor"/>s used
        /// by this application.
        /// </summary>
        public DebugMode Debugmode
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.Debugmode;
                return DebugMode.Off;
            }
            set
            {
                if (DesignerData != null)
                {
                    DesignerData.Debugmode = value;
                    OnPropertyChanged("Debugmode");
                }
                else
                    throw new InvalidOperationException("No data file loaded.");
            }
        }

        #endregion Debugmode

        #region BreakPoints

        /// <summary>
        /// Gets the list of all the expressions that are defined as break points.
        /// Remember to lock this field each time you use it cause this is accessed acrorss multiple threads (by the processors and the designer).
        /// </summary>
        public BreakPointCollection BreakPoints
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.BreakPoints;
                return null;
            }
        }

        #endregion BreakPoints

        #region ToolBoxItems

        /// <summary>
        /// Gets the list of toolbox items Loaded in the designer.
        /// </summary>
        /// <remarks>
        /// this only stores the <see cref="NeuronToolBoxItem"/>s, not all of them,
        /// caus <see cref="TypeToolBoxitem"/>s are hardcoded.
        /// </remarks>
        public ObservedCollection<ToolBoxItem> ToolBoxItems
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.ToolBoxItems;
                return null;
            }
        }

        #endregion ToolBoxItems

        #region CodeEditors

        /// <summary>
        /// Gets the list of known code editor objects
        /// </summary>
        /// <remarks>
        /// this seperate list is required and needs to be stored in xml cause the 'OpenDocuments' list uses this
        /// to find the actual object as it only stores references.
        /// </remarks>
        [XmlIgnore]
        public ObservableCollection<CodeEditor> CodeEditors
        {
            get { return fCodeEditors; }
        }

        #endregion CodeEditors

        #region DefaultMeaningIds

        /// <summary>
        /// Gets the list of neuron id's that are frequently used as meaning neurons.
        /// </summary>
        /// <remarks>
        /// This list allows for fast editing of meaning values.
        /// This is the format in which it is saved, it is provided here, in case.
        /// </remarks>
        public NeuronIDCollection DefaultMeaningIds
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.DefaultMeaningIds;
                return null;
            }
        }

        #endregion DefaultMeaningIds

        #region Default meanings

        /// <summary>
        /// Gets a sorted list (on DisplayTitle) with all the neurons that are prefefined as meanings of links.
        /// </summary>
        /// <remarks>
        /// Don't keep this list alife, it gets recreated each time you call the getter,
        /// this is because the brain might unload memory objects, so they shouldn't be
        /// kept to long in mem.
        /// </remarks>
        [XmlIgnore]
        public List<Neuron> DefaultMeanings
        {
            get
            {
                List<Neuron> iRes = (from i in DefaultMeaningIds
                                     orderby BrainData.Current.NeuronInfo[i].DisplayTitle
                                     select Brain.Brain.Current[i]).ToList();
                return iRes;
            }
        }

        #endregion Default meanings

        #region Current

        /// <summary>
        /// Gets the extra data required by the designer for the <see cref="Brain"/>.
        /// </summary>
        public static BrainData Current
        {
            get
            {
                if (fCurrent == null)
                    fCurrent = new BrainData();
                return fCurrent;
            }
            private set
            {
                if (fCurrent != value)
                {
                    fCurrent = null;                                                  //important: when we get the undostore, it tries to create it if it doesn't exist + register this class, so we need  to be null when this is retrieved for the first time.
                    UndoStore iStore = WindowMain.UndoStore;
                    if (iStore != null)
                        WindowMain.UndoStore.Register(value);                          //important: need to do this before fCurrent is assigned
                    fCurrent = value;
                }
            }
        }

        #endregion Current

        #region OpenDocuments

        /// <summary>
        /// Gets the list of currently open documents.
        /// </summary>
        /// <remarks>
        /// We use a <see cref="DocumentCollection"/> so that we also store which objects
        /// were open. Since this is a  DocumentCollection, all other objects must already
        /// hav been loaded before this fields is loaded, we must therefor put it at the
        /// end of the property list.
        /// </remarks>
        public ObservableCollection<object> OpenDocuments
        {
            get
            {
                return fOpenDocuments;
            }
        }

        #endregion OpenDocuments

        #region NeuronInfo

        /// <summary>
        /// Gets the dictionary with all the objects containing extra info for the neurons.
        /// </summary>
        /// <remarks>
        /// When a neuron is created, there is no neurondata automatically created to go with it.
        /// Whenever an object
        /// </remarks>
        public NeuronInfoDictionary NeuronInfo
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.NeuronInfo;
                return null;
            }
        }

        #endregion NeuronInfo

        #region Editors

        /// <summary>
        /// Gets the Tree of editors defined in the current project.
        /// </summary>
        /// <remarks>
        /// This list contains all the root iems.  Some can be folders, so a tree like structure is possible.
        /// This is important for some events.
        /// </remarks>
        public EditorCollection Editors
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.Editors;
                return null;
            }
        }

        #endregion Editors

        #region CurrentEditorsList

        /// <summary>
        /// Gets/sets the currently active editors list.  This can be the root list (see <see cref="BrainData.Editors"/>)
        /// or a child folder node.
        /// </summary>
        public IList<EditorBase> CurrentEditorsList
        {
            get
            {
                return fCurrentEditorsList;
            }
            set
            {
                fCurrentEditorsList = value;
                OnPropertyChanged("CurrentEditorsList");
            }
        }

        #endregion CurrentEditorsList

        #region Instructions

        /// <summary>
        /// Gets the list of all the available instructions in the brain.
        /// </summary>
        /// <remarks>
        /// This property is used by the <see cref="CodeEditors"/>
        /// </remarks>
        public NeuronCollection<Instruction> Instructions
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.Instructions;
                return null;
            }
        }

        #endregion Instructions

        #region Operators

        /// <summary>
        /// Gets the list of all the available operators (as in Add, substract, equals,...) in the brain.
        /// </summary>
        /// <remarks>
        /// This property is used by the <see cref="CodeEditors"/> in some templates.
        /// </remarks>
        public NeuronCollection<Neuron> Operators
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.Operators;
                return null;
            }
        }

        #endregion Operators

        #region FrameElementImportances

        /// <summary>
        /// Gets the list of neurons that can be used as frame element importances.
        /// </summary>
        /// <remarks>
        /// This is a helper prop so that frame element editors provide the same list of values.
        /// </remarks>
        /// <value>The frame element importances.</value>
        [XmlIgnore]
        public IList<Neuron> FrameElementImportances
        {
            get
            {
                List<Neuron> iRes = new List<Neuron>();
                iRes.Add(Brain.Brain.Current[(ulong)PredefinedNeurons.Frame_Core]);
                iRes.Add(Brain.Brain.Current[(ulong)PredefinedNeurons.Frame_peripheral]);
                iRes.Add(Brain.Brain.Current[(ulong)PredefinedNeurons.Frame_extra_thematic]);
                return iRes;
            }
        }

        #endregion FrameElementImportances

        #region PosValues

        /// <summary>
        /// Gets the list of possible pos values.
        /// </summary>
        /// <remarks>
        /// This is a helper prop so that editors can provide a drop down list.
        /// </remarks>
        [XmlIgnore]
        public IList<Neuron> PosValues
        {
            get
            {
                List<Neuron> iRes = new List<Neuron>();
                iRes.Add(Brain.Brain.Current[(ulong)PredefinedNeurons.Verb]);
                iRes.Add(Brain.Brain.Current[(ulong)PredefinedNeurons.Noun]);
                iRes.Add(Brain.Brain.Current[(ulong)PredefinedNeurons.Adverb]);
                iRes.Add(Brain.Brain.Current[(ulong)PredefinedNeurons.Adjective]);
                iRes.Add(Brain.Brain.Current[(ulong)PredefinedNeurons.Preposition]);
                iRes.Add(Brain.Brain.Current[(ulong)PredefinedNeurons.Conjunction]);
                iRes.Add(Brain.Brain.Current[(ulong)PredefinedNeurons.Intersection]);
                iRes.Add(Brain.Brain.Current[(ulong)PredefinedNeurons.Article]);
                iRes.Add(Brain.Brain.Current[(ulong)PredefinedNeurons.Determiner]);
                iRes.Add(Brain.Brain.Current[(ulong)PredefinedNeurons.Complementizer]);
                return iRes;
            }
        }

        #endregion PosValues

        #region Thesaurus

        /// <summary>
        /// Gets the thesaurus root neurons for the network.
        /// </summary>
        public Thesaurus Thesaurus
        {
            get
            {
                if (DesignerData != null)
                    return DesignerData.Thesaurus;
                return null;
            }
        }

        #endregion Thesaurus

        #region LinkLists

        /// <summary>
        /// Gets a list containing all the neurons used to identify lists of links that can be searched.  This is currently
        /// 'In' and 'Out'.
        /// </summary>
        /// <remarks>
        /// This is static info (core feature of the brain), so don't serialize.
        /// This prop is provided for xaml, is used on CodeEditor.
        /// </remarks>
        [XmlIgnore]
        public List<Neuron> LinkLists
        {
            get
            {
                if (fLinkLists == null)
                {
                    fLinkLists = new List<Neuron>();
                    fLinkLists.Add(Brain.Brain.Current[(ulong)PredefinedNeurons.In]);
                    fLinkLists.Add(Brain.Brain.Current[(ulong)PredefinedNeurons.Out]);
                }
                return fLinkLists;
            }
        }

        #endregion LinkLists

        #region ClusterLists

        /// <summary>
        /// Gets the list of neurons that represent lists related to clusters that can be searched. This is currently 'Chilren' and
        /// 'Clusters'.
        /// </summary>
        /// <remarks>
        /// This is static info (core feature of the brain), so don't serialize.
        /// This prop is provided for xaml, is used on CodeEditor.
        /// </remarks>
        [XmlIgnore]
        public List<Neuron> ClusterLists
        {
            get
            {
                if (fClusterLists == null)
                {
                    fClusterLists = new List<Neuron>();
                    fClusterLists.Add(Brain.Brain.Current[(ulong)PredefinedNeurons.Children]);
                    fClusterLists.Add(Brain.Brain.Current[(ulong)PredefinedNeurons.Clusters]);
                }
                return fClusterLists;
            }
        }

        #endregion ClusterLists

        #region IsChanged

        /// <summary>
        /// Gets the if the project data has been changed since the last save.
        /// </summary>
        /// <remarks>
        /// We use a little trick to see if the data is changed: we keep track of the last undo item
        /// </remarks>
        public bool IsChanged
        {
            get
            {
                if (WindowMain.UndoStore.UndoData.Count > 0)
                    return fLastUndoItem != WindowMain.UndoStore.UndoData[WindowMain.UndoStore.UndoData.Count - 1];
                else
                    return fLastUndoItem != null;
            }
        }

        #endregion IsChanged

        #region LearnCount

        /// <summary>
        /// Gets or sets the nr of units that are currently learning.
        /// </summary>
        /// <remarks>
        /// Used to adjust the <see cref="BrainData.IsLearning"/> value.
        /// </remarks>
        /// <value>The learn count.</value>
        internal int LearnCount
        {
            get { return fLearnCount; }
            set
            {
                fLearnCount = value;
                OnPropertyChanged("IsLearning");
            }
        }

        #endregion LearnCount

        #region IsLearning

        /// <summary>
        /// Gets wether the network is currently learning (aqcuiring) data from an external resource like wordnet, framenet, the internet,...
        /// </summary>
        /// <remarks>
        /// To change this value, use <see cref="BrainData.LearnCount"/>.
        /// </remarks>
        public bool IsLearning
        {
            get
            {
                return LearnCount > 0;
            }
        }

        #endregion IsLearning

        #endregion Prop

        #region Functions

        /// <summary>
        /// Unregisters the specified data.
        /// </summary>
        /// <remarks>
        /// Also resets the root <see cref="BrainData.CurrentEditorsList"/>.
        /// </remarks>
        /// <param name="data">The data.</param>
        private void Unregister(DesignerDataFile data)
        {
            data.CommChannels.Owner = null;
            data.ToolBoxItems.Owner = null;
            data.DefaultMeaningIds.Owner = null;
            data.Watches.Owner = null;
            data.Editors.Owner = null;
            CurrentEditorsList = null;
        }

        /// <summary>
        /// Registers the specified data.
        /// </summary>
        /// <remarks>
        /// Also sets the root <see cref="BrainData.CurrentEditorsList"/>.
        /// </remarks>
        /// <param name="data">The data.</param>
        private void Register(DesignerDataFile data)
        {
            data.CommChannels.Owner = this;
            data.ToolBoxItems.Owner = this;
            data.DefaultMeaningIds.Owner = this;
            data.Watches.Owner = this;
            data.Editors.Owner = this;
            if (data.Thesaurus == null)                                                                     //to handle files that were created before the thesaurus was stored there, create a new one.
                data.Thesaurus = new Thesaurus();
            CurrentEditorsList = data.Editors;
            //we sort the instruction list, each time we load. this is saved to ensure the list is sorted.
            List<Instruction> iTemp = data.Instructions.OrderBy(p => BrainData.Current.NeuronInfo[p.ID].DisplayTitle).ToList();       //we need to convert cause we are going to modify the list.
            data.Instructions.Clear();
            foreach (Instruction i in iTemp)
                data.Instructions.Add(i);
            foreach (NeuronEditor i in data.Editors.AllNeuronEditors())                                     //neuronEditors need to be registered as well after loading
            {
                i.RegisterItem();
                if (i is CodeEditor)                                                                          //if we want to retrieve the correct code editors from the project, they also need to be loaded properly.
                    CodeEditors.Add((CodeEditor)i);
            }
        }

        /// <summary>
        /// Creates a new Current object and loads the values for the properties from the specified stream.
        /// </summary>
        public static void Load(Stream aSource)
        {
            XmlSerializer iSer = new XmlSerializer(typeof(DesignerDataFile));
            iSer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);

            Current.DesignerData = (DesignerDataFile)iSer.Deserialize(aSource);
            Current.OnLoaded();
        }

        /// <summary>
        /// Raises the event.
        /// </summary>
        protected virtual void OnLoaded()
        {
            if (AfterLoad != null)
                AfterLoad(this, new EventArgs());
        }

        public void Save(Stream aDest)
        {
            OnBeforeSave();
            try
            {
                XmlSerializer iSer = new XmlSerializer(typeof(DesignerDataFile));
                using (TextWriter iWriter = new StreamWriter(aDest))
                    iSer.Serialize(iWriter, DesignerData);
                if (WindowMain.UndoStore.UndoData.Count > 0)
                    fLastUndoItem = WindowMain.UndoStore.UndoData[WindowMain.UndoStore.UndoData.Count - 1];
                else
                    fLastUndoItem = null;
            }
            catch (Exception e)
            {
                Log.Log.LogError("BrainData.Save", e.Message);
            }
            OnAfterSave();
        }

        /// <summary>
        /// Performs custom actions after saving.
        /// </summary>
        private void OnAfterSave()
        {
            foreach (CodeEditor i in CodeEditors)                                                  //remove all the temp toolboxitems for the variables that can be used in code editors.
                i.UnParkRegisteredVariables();
        }

        /// <summary>
        /// Allows to perform + performs various actions before the data is saved.  Calls the apropriate event handlers.
        /// </summary>
        protected virtual void OnBeforeSave()
        {
            foreach (CodeEditor i in CodeEditors)                                                  //remove all the temp toolboxitems for the variables that can be used in code editors.
                i.ParkRegisteredVariables();

            if (BeforeSave != null)
                BeforeSave(this, new EventArgs());
        }

        #endregion Functions

        #region eventhandlers

        #region List syncing

        /// <summary>
        /// Called when the CodeEditors collection changes.
        /// </summary>
        /// <remarks>
        /// - Whenever an item is removed from this list, we need to make certain
        /// it is also removed from the open documents.
        /// </remarks>
        private void fCodeEditors_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            CleanOpenDocuments<CodeEditor>(e.Action, e.OldItems);
        }

        /// <summary>
        /// Handles the CollectionChanging event of the BrainData control.
        /// </summary>
        /// <remarks>
        /// We handle changes in the lists of the <see cref="DesignerDataFile"/> here so that we can
        /// handle tree structures correctly (if we attach to the list itself, it doesn't get called recurivelly).
        /// This recursiveness is important for the Editors list, cause it is a tree.
        /// </remarks>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="JaStDev.UndoSystem.Interfaces.CollectionChangingEventArgs"/> instance containing the event data.</param>
        private void BrainData_CollectionChanging(object sender, CollectionChangingEventArgs e)
        {
            if (e.OrignalSource == CommChannels)
                CleanOpenDocuments(e);
            else if (e.OrignalSource == DefaultMeaningIds)
                OnPropertyChanged("DefaultMeanings");
            else if (e.OrignalSource == Editors)
                Editors_CollectionChanging(sender, e);
        }

        /// <summary>
        /// Handles the CollectionChanging event of the Editors control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="JaStDev.UndoSystem.Interfaces.CollectionChangingEventArgs"/> instance containing the event data.</param>
        private void Editors_CollectionChanging(object sender, CollectionChangingEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Remove:
                    ((EditorBase)e.Item).RemoveEditorFrom(OpenDocuments);
                    break;

                case NotifyCollectionChangedAction.Replace:
                    ((EditorBase)e.Item).RemoveEditorFrom(OpenDocuments);
                    break;

                case NotifyCollectionChangedAction.Reset:
                    foreach (EditorBase i in e.Items)
                        i.RemoveEditorFrom(OpenDocuments);
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Cleans the list of open documents with regards to the specified action and involved items.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="e">The <see cref="JaStDev.UndoSystem.Interfaces.CollectionChangingEventArgs"/> instance containing the event data.</param>
        private void CleanOpenDocuments(CollectionChangingEventArgs e)
        {
            if (OpenDocuments != null)                                                                   //this can be null if a clean is performed when there was no previous newPRoject called.
            {
                if (e.Action == NotifyCollectionChangedAction.Remove)
                    OpenDocuments.Remove(e.Item);
                else if (e.Action == NotifyCollectionChangedAction.Reset)
                {
                    foreach (object i in e.Items)
                        OpenDocuments.Remove(i);
                }
            }
        }

        /// <summary>
        /// Cleans the list of open documents with regards to the specified action and involved items.
        /// </summary>
        /// <typeparam name="T">The type of neuron that should be cleaned from the open documents.</typeparam>
        /// <param name="action">The action to perform: simple remove or complete reset.</param>
        /// <param name="toRemove">The items to remove.</param>
        private void CleanOpenDocuments<T>(NotifyCollectionChangedAction action, IList toRemove)
        {
            if (OpenDocuments != null)                                                                   //this can be null if a clean is performed when there was no previous newPRoject called.
            {
                if (action == NotifyCollectionChangedAction.Remove)
                {
                    foreach (object i in toRemove)
                        OpenDocuments.Remove(i);
                }
                else if (action == NotifyCollectionChangedAction.Reset)
                {
                    var iToRemove = (from i in OpenDocuments where i is T select i).ToList();
                    foreach (object i in iToRemove)
                        OpenDocuments.Remove(i);
                }
            }
        }

        #endregion List syncing

        private static void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            //We don't show an error for xsi:type attributes which are used to specify the type.
            if (e.Name != "xsi:type")
                Log.Log.LogError("BrainData.Load", "L: " + e.LineNumber.ToString() + "P: " + e.LinePosition.ToString() + ": unknown node in stream: " + e.Name + "/n contains: " + e.Text);
        }

        #endregion eventhandlers

        /// <summary>
        /// Clears all the data from this instance.
        /// </summary>
        public void Clear()
        {
            DesignerData = null;
            fCodeEditors.Clear();
            OpenDocuments.Clear();                                                                       //just to be save that there are no open documents left
        }

        /// <summary>
        /// Creates a new <see cref="BrainData.Current"/>
        /// </summary>
        /// <remarks>
        /// Items created:
        /// -all default toolbox items
        /// -the wordnetSin channel.
        /// </remarks>
        public static void New()
        {
            DesignerDataFile iData = new DesignerDataFile();                                                   //load the default (empty) data.
            iData.Name = "New project";
            iData.CommChannels = new CommChannelCollection();
            iData.BreakPoints = new BreakPointCollection();
            iData.ToolBoxItems = new ObservedCollection<ToolBoxItem>();
            iData.PlaySpeed = new TimeSpan(0, 0, 0, 2, 0);
            iData.DefaultMeaningIds = new NeuronIDCollection();
            iData.NeuronInfo = new NeuronInfoDictionary();
            iData.Instructions = new NeuronCollection<Instruction>();
            iData.Operators = new NeuronCollection<Neuron>();
            iData.Watches = new ObservedCollection<Watch>();
            iData.Editors = new EditorCollection();
            iData.Thesaurus = new Thesaurus();
            Current.DesignerData = iData;

            Current.LoadDefaultToolBoxItems();

            WordNetChannel iWordNet = new WordNetChannel();
            iWordNet.NeuronID = (ulong)PredefinedNeurons.WordNetSin;
            iWordNet.NeuronInfo.DisplayTitle = "WordNet";
            Current.CommChannels.Add(iWordNet);
        }

        /// <summary>
        /// Loads all the default toolbox items, which are the <see cref="TypeToolBoxItem"/>s.
        /// </summary>
        private void LoadDefaultToolBoxItems()
        {
            LoadToolBoxItemFor(typeof(Neuron), "General", "Neuron");
            LoadToolBoxItemFor(typeof(NeuronCluster), "General", "Cluster");
            LoadToolBoxItemFor(typeof(TextNeuron), "General", "Text neuron");
            LoadToolBoxItemFor(typeof(TextSin), "General", "Text Sin");
            LoadToolBoxItemFor(typeof(IntNeuron), "General", "Int neuron");
            LoadToolBoxItemFor(typeof(DoubleNeuron), "General", "Double neuron");
            LoadToolBoxItemFor(typeof(KnoledgeNeuron), "General", "Knoledge neuron");

            LoadToolBoxItemFor(typeof(Assignment), "Code", "Assignment");
            LoadToolBoxItemFor(typeof(Statement), "Code", "Statement");
            LoadToolBoxItemFor(typeof(ResultStatement), "Code", "Result statement");
            LoadToolBoxItemFor(typeof(ExpressionsBlock), "Code", "Code block");
            LoadToolBoxItemFor(typeof(ConditionalStatement), "Code", "Condional statement");
            LoadToolBoxItemFor(typeof(ConditionalExpression), "Code", "condional part");
            LoadToolBoxItemFor(typeof(BoolExpression), "Code", "bool expression");
            LoadToolBoxItemFor(typeof(SearchExpression), "Code", "search expression");
            LoadToolBoxItemFor(typeof(Variable), "Code", "Variable");
            LoadToolBoxItemFor(typeof(ByRefExpression), "Code", "ByRef");

            LoadNeuronToolBoxItemFor((ulong)PredefinedNeurons.Empty, "Global values", "Empty");
            LoadNeuronToolBoxItemFor((ulong)PredefinedNeurons.BeginTextBlock, "Global values", "BeginTextBlock");
            LoadNeuronToolBoxItemFor((ulong)PredefinedNeurons.EndTextBlock, "Global values", "EndTextBlock");
            LoadNeuronToolBoxItemFor((ulong)PredefinedNeurons.CurrentSin, "Global values", "CurrentSin");
            LoadNeuronToolBoxItemFor((ulong)PredefinedNeurons.CurrentFrom, "Global values", "CurrentFrom");
            LoadNeuronToolBoxItemFor((ulong)PredefinedNeurons.CurrentTo, "Global values", "CurrentTo");
            LoadNeuronToolBoxItemFor((ulong)PredefinedNeurons.CurrentMeaning, "Global values", "CurrentMeaning");
            LoadNeuronToolBoxItemFor((ulong)PredefinedNeurons.CurrentInfo, "Global values", "CurrentInfo");

            LoadDefaultInstructions();

            LoadOperatorToolBoxItemFor((ulong)PredefinedNeurons.Equal, "Operators", "==");
            LoadOperatorToolBoxItemFor((ulong)PredefinedNeurons.Smaller, "Operators", "<");
            LoadOperatorToolBoxItemFor((ulong)PredefinedNeurons.Bigger, "Operators", ">");
            LoadOperatorToolBoxItemFor((ulong)PredefinedNeurons.SmallerOrEqual, "Operators", "<=");
            LoadOperatorToolBoxItemFor((ulong)PredefinedNeurons.BiggerOrEqual, "Operators", ">=");
            LoadOperatorToolBoxItemFor((ulong)PredefinedNeurons.Different, "Operators", "!=");
            LoadOperatorToolBoxItemFor((ulong)PredefinedNeurons.Contains, "Operators", "Contains");
            LoadOperatorToolBoxItemFor((ulong)PredefinedNeurons.And, "Operators", "&&");
            LoadOperatorToolBoxItemFor((ulong)PredefinedNeurons.Or, "Operators", "||");
        }

        private void LoadDefaultInstructions()
        {
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.NewInstruction, "Instructions", "New");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.DeleteInstruction, "Instructions", "Delete");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.DuplicateInstruction, "Instructions", "Duplicate");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.TypeOfInstruction, "Instructions", "TypeOf");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ExecuteInstruction, "Instructions", "Execute");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.SToCcInstruction, "Instructions", "SToCc");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.SToCiInstruction, "Instructions", "SToCi");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.CcToSInstruction, "Instructions", "CcToS");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.CiToSInstruction, "Instructions", "CiToS");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.IToSInstruction, "Instructions", "IToS");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.DToSInstruction, "Instructions", "DToS");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.IToDInstruction, "Instructions", "IToD");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.DToIInstruction, "Instructions", "DToI");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.AddChildInstruction, "Instructions", "Add Child");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.AddLinkInstruction, "Instructions", "Add Link");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.AddInfoInstruction, "Instructions", "Add Info");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.InsertChildInstruction, "Instructions", "Insert Child");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.InsertLinkInstruction, "Instructions", "Insert Link");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.InsertInfoInstruction, "Instructions", "Insert info");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.RemoveChildInstruction, "Instructions", "Remove child");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.RemoveLinkInstruction, "Instructions", "Remove link");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.RemoveInfoInstruction, "Instructions", "Remove info");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ClearChildrenInstruction, "Instructions", "Clear children");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ClearInfoInstruction, "Instructions", "Clear info");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ClearLinksInInstruction, "Instructions", "Clear links in");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ClearLinksOutInstruction, "Instructions", "Clear links out");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.CopyChildrenInstruction, "Instructions", "Copy children");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.CopyInfoInstruction, "Instructions", "Copy info");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.MoveChildrenInstruction, "Instructions", "Move children");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.MoveInfoInstruction, "Instructions", "Move info");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.IndexOfChildInstruction, "Instructions", "IndexOf child");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.IndexOfInfoInstruction, "Instructions", "IndexOf info");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.IndexOfLinkInstruction, "Instructions", "IndexOf link");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.CompleteSequenceInstruction, "Instructions", "Complete sequence");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.CountInstruction, "Instructions", "Count");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.PeekInstruction, "Instructions", "Peek");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.PopInstruction, "Instructions", "Pop");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.PushInstruction, "Instructions", "Push");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.MakeClusterInstruction, "Instructions", "Make cluster");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetChildrenInstruction, "Instructions", "Get children");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetChildrenOfTypeInstruction, "Instructions", "Get children of Type X");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetClustersInstruction, "Instructions", "Get clusters");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetFirClusterInstruction, "Instructions", "Get first cluster");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetClusterMeaningInstruction, "Instructions", "Get cluster meaning");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.SetClusterMeaningInstruction, "Instructions", "Set cluster meaning");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetFirstOut, "Instructions", "Get first link Out");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.SetFirstOut, "Instructions", "Set first link Out");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ChangeLinkFrom, "Instructions", "Change link From");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ChangeLinkTo, "Instructions", "Change link To");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ChangeLinkMeaning, "Instructions", "Change link meaning");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ExitLinkInstruction, "Instructions", "Exit Link");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ExitNeuronInstruction, "Instructions", "Exit Neuron");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ExitSolveInstruction, "Instructions", "Exit Solve");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.SplitInstruction, "Instructions", "Split");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetSplitResultsInstruction, "Instructions", "Get Split results");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ClearSplitResultsInstruction, "Instructions", "Clear split results");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.RemoveSplitResultInstruction, "Instructions", "Remove split result");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.AddSplitResultInstruction, "Instructions", "Add split result");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.AwakeInstruction, "Instructions", "Awake");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.SuspendInstruction, "Instructions", "Suspend");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ExecuteInstruction, "Instructions", "Execute");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.OutputInstruction, "Instructions", "Output");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.SolveInstruction, "Instructions", "Solve");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.BlockedSolveInstruction, "Instructions", "Blocked Solve");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetWeightInstruction, "Instructions", "Get Weight");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.GetMaxWeightInstruction, "Instructions", "Get Max Weight");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.IncreaseWeightInstruction, "Instructions", "Increase Weight");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.DecreaseWeightInstruction, "Instructions", "Decrease Weight");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ResetWeightInstruction, "Instructions", "Reset Weight");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.UnionInstruction, "Instructions", "Union");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.IntersectInstruction, "Instructions", "Intersect");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.InterleafInstruction, "Instructions", "Interleaf");

            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.WarningInstruction, "Instructions", "Warning");
            LoadInstructionToolBoxItemFor((ulong)PredefinedNeurons.ErrorInstruction, "Instructions", "Error");
        }

        private void LoadNeuronToolBoxItemFor(ulong item, string category, string title)
        {
            NeuronToolBoxItem iNew = new NeuronToolBoxItem();
            iNew.Item = Brain.Brain.Current[item];
            iNew.NeuronInfo.Category = category;
            iNew.NeuronInfo.DisplayTitle = title;
            ToolBoxItems.Add(iNew);
        }

        private void LoadOperatorToolBoxItemFor(ulong item, string category, string title)
        {
            NeuronToolBoxItem iNew = new NeuronToolBoxItem();
            iNew.Item = Brain.Brain.Current[item];
            iNew.NeuronInfo.Category = category;
            iNew.NeuronInfo.DisplayTitle = title;
            ToolBoxItems.Add(iNew);
            Operators.Add(iNew.Item);
        }

        private void LoadInstructionToolBoxItemFor(ulong item, string category, string title)
        {
            InstructionToolBoxItem iNew = new InstructionToolBoxItem();
            iNew.Item = Brain.Brain.Current[item];
            iNew.NeuronInfo.Category = category;
            iNew.NeuronInfo.DisplayTitle = title;
            ToolBoxItems.Add(iNew);
            Instructions.Add(iNew.Item as Instruction);
        }

        /// <summary>
        /// Creates a typed toolbox item, sets everything up and adds it to the list of toolbox items.
        /// </summary>
        /// <param name="type">The type for which we create a toolbox item.</param>
        /// <param name="category">The category name to assign to the toolbox item.</param>
        private void LoadToolBoxItemFor(Type type, string category, string title)
        {
            TypeToolBoxItem iNew = new TypeToolBoxItem();
            iNew.ItemType = type;
            iNew.DisplayTitle = title;
            iNew.Category = category;
            ToolBoxItems.Add(iNew);
        }
    }
}