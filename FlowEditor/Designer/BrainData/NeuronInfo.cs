﻿using System.Xml.Serialization;
using JaStDev.HAB.Brain;

namespace JaStDev.HAB.Designer.BrainData
{
    /// <summary>
    /// A wrapper class for neurons that provides access to the <see cref="NeuronData"/> for the neuron wrapped by
    /// this object (defined by <see cref="NeuronInfo.ID"/>.
    /// </summary>
    /// <remarks>
    /// This class can be used statically in xaml to create references to neurons for instance in a combobox that always needs to
    /// have the same fixed neurons.
    /// </remarks>
    public class NeuronInfo : INeuronInfo
    {
        #region Fields

        private NeuronData fInfo;
        private PredefinedNeurons fID;
        private Neuron fItem;

        #endregion Fields

        #region Info

        /// <summary>
        /// Gets the infor for this neuron.
        /// </summary>
        [XmlIgnore]
        public NeuronData Info
        {
            get { return fInfo; }
            internal set { fInfo = value; }
        }

        #endregion Info

        #region ID

        /// <summary>
        /// Gets/sets the id of the neuron we should incapsulate. This has to be a statically declared neuron so we can
        /// easely use it from xaml.
        /// </summary>
        public PredefinedNeurons ID
        {
            get
            {
                return fID;
            }
            set
            {
                if (fID != value)
                {
                    fID = value;
                    if (Neuron.IsEmpty((ulong)fID) == false)
                    {
                        fInfo = BrainData.Current.NeuronInfo[(ulong)fID];
                        fItem = Brain.Brain.Current[(ulong)fID];
                    }
                    else
                    {
                        fInfo = null;
                        fItem = null;
                    }
                }
            }
        }

        #endregion ID

        #region Item

        /// <summary>
        /// Gets the actual neuron.
        /// </summary>
        /// <remarks>
        /// This allows us to get the neuron as the result value in a combobox for instance.
        /// </remarks>
        public Neuron Item
        {
            get { return fItem; }
        }

        #endregion Item

        #region INeuronInfo Members

        NeuronData INeuronInfo.NeuronInfo
        {
            get { return fInfo; }
        }

        #endregion INeuronInfo Members
    }
}