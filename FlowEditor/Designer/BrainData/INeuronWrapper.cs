﻿using JaStDev.HAB.Brain;

namespace JaStDev.HAB.Designer.BrainData
{
    /// <summary>
    /// An interface for all objects that are wrappers for a neuron.
    /// </summary>
    public interface INeuronWrapper
    {
        Neuron Item { get; }
    }
}