﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Designer.WPF.BrainEventManagers;
using JaStDev.HAB.Events;

namespace JaStDev.HAB.Designer.BrainData
{
    /// <summary>
    /// A custom observable collection for neurons that provides streaming capabilities by saving the id's of the
    /// neurons, not the actual object. Also provides functionality to monitor changes in the brain.
    /// </summary>
    /// <typeparam name="T">The actual specific neuron type.</typeparam>
    public class NeuronCollection<T> : ObservableCollection<T>, IXmlSerializable, IWeakEventListener where T : Neuron
    {
        #region ctor/dtor

        /// <summary>
        /// Initializes a new instance of the <see cref="NeuronCollection&lt;T&gt;"/> class.
        /// </summary>
        public NeuronCollection()
           : base()
        {
            NeuronChangedEventManager.AddListener(Brain.Brain.Current, this);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NeuronCollection&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="collection">The collection from which the elements are copied.</param>
        /// <exception cref="T:System.ArgumentNullException">
        /// The <paramref name="collection"/> parameter cannot be null.
        /// </exception>
        public NeuronCollection(IEnumerable<T> collection)
           : base(collection)
        {
            NeuronChangedEventManager.AddListener(Brain.Brain.Current, this);
        }

        ~NeuronCollection()
        {
            NeuronChangedEventManager.RemoveListener(Brain.Brain.Current, this);
        }

        #endregion ctor/dtor

        #region IXmlSerializable Members

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            bool wasEmpty = reader.IsEmptyElement;

            reader.Read();
            if (wasEmpty) return;
            while (reader.NodeType != XmlNodeType.EndElement)
            {
                reader.ReadStartElement("ID");
                ulong iId = ulong.Parse(reader.ReadString());
                T ifound = Brain.Brain.Current[iId] as T;
                if (ifound != null)
                    Add(ifound);
                else
                    Log.Log.LogError("NeuronCollection", string.Format("Failed to find item with id: {0}.", iId));
                reader.ReadEndElement();
                //reader.MoveToContent();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            foreach (T i in this)
            {
                writer.WriteStartElement("ID");
                writer.WriteString(i.ID.ToString());
                writer.WriteEndElement();
            }
        }

        #endregion IXmlSerializable Members

        #region IWeakEventListener Members

        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            if (managerType == typeof(NeuronChangedEventManager))
            {
                Current_NeuronChanged(sender, (NeuronChangedEventArgs)e);
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Event handler for <see cref="Brain.NeuronChanged"/>
        /// </summary>
        /// <remarks>
        /// Do asyn cause this event can be raised from different threads.
        /// </remarks>
        private void Current_NeuronChanged(object sender, NeuronChangedEventArgs e)
        {
            App.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<NeuronChangedEventArgs>(InternalNeuronChanged), e);
        }

        /// <summary>
        /// for replace: replaces the item if it is in this list
        /// fore remove: removes the item.
        /// </summary>
        /// <param name="e">The <see cref="NeuronChangedEventArgs"/> instance containing the event data.</param>
        private void InternalNeuronChanged(NeuronChangedEventArgs e)
        {
            T iSender = e.OriginalSource as T;
            if (iSender != null)
            {
                if (e.Action == BrainAction.Removed)
                    this.Remove(iSender);                                                             //simply try to remove the item.
                else if (e.Action == BrainAction.Changed && !(e is NeuronPropChangedEventArgs))     //it could also be that the properties of an item are simply changed, don't need to respond to this.
                {
                    int iIndex = this.IndexOf(iSender);
                    if (iIndex > -1 && e.NewValue is T)
                        this[iIndex] = (T)e.NewValue;
                }
            }
        }

        #endregion IWeakEventListener Members
    }
}