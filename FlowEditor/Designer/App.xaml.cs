﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using JaStDev.HAB.Designer.BrainData;
using JaStDev.HAB.Designer.Editors.MindMap;
using JaStDev.HAB.Designer.Tools.Debugger;

namespace JaStDev.HAB.Designer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    /// <remarks>
    /// Run usage:
    /// exeName [Path]
    /// where Path is the file path to open the DESIGNERFILE from.
    /// </remarks>
    public partial class App : Application
    {
        #region Fields

        private DispatcherTimer fAutoSaveTimer;
        private int fUndoCountAtLastSave;

        private static string FRAMENETPATH = "NND\\Data\\FrameNet";                                 //the default installation path of framenet.
        private static string DEFAULTTEMPLATEPATH = "NND\\Templates\\Default";                                 //the default installation path of framenet.

        #endregion Fields

        #region Commands

        /// <summary>
        /// Command to view the code of the selected neuron.
        /// </summary>
        public static RoutedCommand ViewCodeCmd = new RoutedCommand();

        /// <summary>
        /// Command to rename an item.
        /// </summary>
        public static RoutedCommand RenameCmd = new RoutedCommand();

        /// <summary>
        /// Command to remove an item from a list, without actually deleting it.
        /// </summary>
        public static RoutedCommand RemoveCmd = new RoutedCommand();

        /// <summary>
        /// Command to sync the currently selected neuron with the explorer so that it  is also selected there.
        /// </summary>
        public static RoutedCommand SyncCmd = new RoutedCommand();

        /// <summary>
        /// Command to change an item from one type to another.
        /// </summary>
        public static RoutedCommand ChangeToCmd = new RoutedCommand();

        /// <summary>
        /// Command to let the debug processor continue to the next step or breakpoint.
        /// </summary>
        public static RoutedCommand ContinueDebugCmd = new RoutedCommand();

        /// <summary>
        /// Command to let the debug processor pause at the next step.
        /// </summary>
        public static RoutedCommand PauseDebugCmd = new RoutedCommand();

        /// <summary>
        /// Command to let the debug processor go to the next steps.
        /// </summary>
        public static RoutedCommand StepNextDebugCmd = new RoutedCommand();

        /// <summary>
        /// Command to copy the current project into a sandbox dir and start it in a new designer.
        /// </summary>
        public static RoutedCommand SandboxDebugCmd = new RoutedCommand();

        /// <summary>
        /// Command to initiate an import from the framenet database.
        /// </summary>
        public static RoutedCommand ImportFrameNetDataCmd = new RoutedCommand();

        /// <summary>
        /// Command to create a new mindmap.
        /// </summary>
        public static RoutedCommand NewMindMapCmd = new RoutedCommand();

        /// <summary>
        /// Command to create a new frame editor.
        /// </summary>
        public static RoutedCommand NewFrameEditorCmd = new RoutedCommand();

        /// <summary>
        /// Command to create a new frame editor.
        /// </summary>
        public static RoutedCommand NewCodeClusterCmd = new RoutedCommand();

        /// <summary>
        /// Command to create a new flow.
        /// </summary>
        public static RoutedCommand NewFlowEditorCmd = new RoutedCommand();

        /// <summary>
        /// Command to create a new folder.
        /// </summary>
        public static RoutedCommand NewFolderCmd = new RoutedCommand();

        /// <summary>
        /// Command to insert a new Flow option.
        /// </summary>
        public static RoutedCommand InsertFlowOptionCmd = new RoutedCommand();

        /// <summary>
        /// Command to insert a new Flow loop.
        /// </summary>
        public static RoutedCommand InsertFlowLoopCmd = new RoutedCommand();

        /// <summary>
        /// Command to display a dialog for selecting a static to insert.
        /// </summary>
        public static RoutedCommand InsertFlowStaticCmd = new RoutedCommand();

        /// <summary>
        /// Command to display a dialog for a new object to insert in a flow.
        /// </summary>
        public static RoutedCommand InsertFlowNewObjectCmd = new RoutedCommand();

        /// <summary>
        /// Command to display a dialog for selecting a static to insert.
        /// </summary>
        public static RoutedCommand InsertFlowCondPartCmd = new RoutedCommand();

        /// <summary>
        /// Command to insert a new Flow option.
        /// </summary>
        public static RoutedCommand AddFlowOptionCmd = new RoutedCommand();

        /// <summary>
        /// Command to insert a new Flow loop.
        /// </summary>
        public static RoutedCommand AddFlowLoopCmd = new RoutedCommand();

        /// <summary>
        /// Command to change an option into a loop on a Flow.
        /// </summary>
        public static RoutedCommand ChangeFlowOptionToLoopCmd = new RoutedCommand();

        /// <summary>
        /// Command to change an option into a loop on a Flow.
        /// </summary>
        public static RoutedCommand ChangeFlowLoopToOptionCmd = new RoutedCommand();

        /// <summary>
        /// Command to display a dialog for selecting a static to insert.
        /// </summary>
        public static RoutedCommand AddFlowStaticCmd = new RoutedCommand();

        /// <summary>
        /// Command to display a dialog for a new object to add to a flow.
        /// </summary>
        public static RoutedCommand AddFlowNewObjectCmd = new RoutedCommand();

        /// <summary>
        /// Command to display a dialog for selecting a static to insert.
        /// </summary>
        public static RoutedCommand AddFlowCondPartCmd = new RoutedCommand();

        /// <summary>
        /// Command to add a new flow to the currently openeed flow editor.
        /// </summary>
        public static RoutedCommand AddFlowCmd = new RoutedCommand();

        #endregion Commands

        #region Functions

        #region app Start/stop

        /// <summary>
        /// Called when the application starts
        /// </summary>
        /// <remarks>
        /// Performs the various starup tasks such as loading the default data.
        /// <para>
        /// possible arguments:
        /// 0: the path to open the data from.
        /// </para>
        /// </remarks>
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            CheckInitUserSettings();

            Timeline.DesiredFrameRateProperty.OverrideMetadata(typeof(Timeline), new FrameworkPropertyMetadata { DefaultValue = 25 });       //make certain that animation doesn't consume to much CPU
            MindMapCluster.AutoAddItemsToMindMapCluter = Designer.Properties.Settings.Default.AutoAddItemsToMindMapCluter;                   //we copy this value so that the mindmap is an independent unit.

            Settings.SinAssemblies = CreateAssemblies();
            Settings.StorageMode = NeuronStorageMode.StreamWhenPossible;                                                            //we want to work in a project based fashion, this means no auto store neurons, but only at specific request.
            Settings.LogNeuronNotFoundInLongTermMem = false;
            Settings.TrackNeuronAccess = false;
            if (e.Args.Length == 0)
                ProjectManager.Default.CreateNew();
            else if (Directory.Exists(e.Args[0]) == true)
                ProjectManager.Default.LoadProject(e.Args[0]);
            else
                throw new ArgumentOutOfRangeException("Path", "Invalid path, can't open or create " + ProjectManager.DESIGNERFILE);
            if (BrainData.BrainData.Current == null)                                                                        //we check if the braindata was correctly loaded, if not, we create a new one and reset the load location.  This will allow the app to function without the loaded data (empty mem) withtout overwriting the bad data.
            {
                ProjectManager.Default.DataError = true;
                try
                {
                    BrainData.BrainData.New();                                                                                //we put this in a try/catch cause a create can also fail if the load failed, which we need to take care off.
                    Log.Log.LogError("App.Application_Startup", "Failed to load BrainData, Created new.");
                }
                catch (Exception ex)
                {
                    Log.Log.LogError("App.Application_Startup", string.Format("Failed to load brainData, tried to create new which also failed with the error: {0}.", ex));
                }
            }
            CheckSandBox(e.Args);
            UpdateAutoSave();
            DebugProcessor.Init();
        }

        /// <summary>
        /// Checks if all the user settings are properly initialized and if not, provides a correct value.
        /// </summary>
        private void CheckInitUserSettings()
        {
            if (string.IsNullOrEmpty(Designer.Properties.Settings.Default.FrameNetPath) == true)
                Designer.Properties.Settings.Default.FrameNetPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), FRAMENETPATH);

            if (string.IsNullOrEmpty(Designer.Properties.Settings.Default.DefaultTemplatePath) == true)
                Designer.Properties.Settings.Default.DefaultTemplatePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), DEFAULTTEMPLATEPATH);
            //DEFAULTTEMPLATEPATH
        }

        /// <summary>
        /// Checks if the application is currently the sandbox for another project.
        /// </summary>
        /// <param name="p">The p.</param>
        private void CheckSandBox(string[] p)
        {
            string iFound = (from i in p where i.ToLower() == "sandbox" select i).FirstOrDefault();
            if (iFound != null)
                ProjectManager.Default.IsSandBox = true;
            else
                ProjectManager.Default.IsSandBox = false;
        }

        /// <summary>
        /// Save all the state info + flushes the current brain to disk.
        /// </summary>
        private void Application_Exit(object sender, ExitEventArgs e)
        {
            ProjectManager.Default.SaveSettings();
            Designer.Properties.Settings.Default.Save();
            if (ProjectManager.Default.IsSandBox == true)
                ProjectManager.Default.CleanSandbox();
        }

        #endregion app Start/stop

        /// <summary>
        /// Creates the list of assemblies that contain extra sins used by the designer.
        /// </summary>
        /// <returns>A list of assebmlies, currently this list only contains the 'Sensory Interfaces' and wordnet assembly.</returns>
        private List<Assembly> CreateAssemblies()
        {
            List<Assembly> iRes = new List<Assembly>();
            iRes.Add(Assembly.GetAssembly(typeof(ImageSin)));
            iRes.Add(Assembly.GetAssembly(typeof(WordNetSin)));
            return iRes;
        }

        #region Timer

        /// <summary>
        /// Checks the user settings for the auto save option and updates the timer accordingly.
        /// </summary>
        public void UpdateAutoSave()
        {
            if (Designer.Properties.Settings.Default.AutoSave == true)
            {
                if (fAutoSaveTimer == null)
                {
                    fAutoSaveTimer = new DispatcherTimer(DispatcherPriority.Background);
                    fAutoSaveTimer.Tick += new EventHandler(fAutoSaveTimer_Tick);
                }
                fAutoSaveTimer.Interval = Designer.Properties.Settings.Default.AutoSaveInterval;
                fAutoSaveTimer.Start();
            }
            else if (fAutoSaveTimer != null)
            {
                fAutoSaveTimer.Tick -= new EventHandler(fAutoSaveTimer_Tick);
                fAutoSaveTimer.Stop();
                fAutoSaveTimer = null;
            }
        }

        /// <summary>
        /// Try to save the brain when auto save is on.
        /// </summary>
        private void fAutoSaveTimer_Tick(object sender, EventArgs e)
        {
            if (fUndoCountAtLastSave != WindowMain.UndoStore.UndoData.Count)                       //only try to save if something changed.
            {
                if (string.IsNullOrEmpty(ProjectManager.Default.Location) == false)                         //we only try to save if we have saved it before.
                {
                    ProjectManager.Default.Save();
                    //((WindowMain)MainWindow).VwThesaurus.SaveData();                                 //also need to save the thesaurus if it changed.
                    fUndoCountAtLastSave = WindowMain.UndoStore.UndoData.Count;
                }
            }
        }

        #endregion Timer

        /// <summary>
        /// Handles the DispatcherUnhandledException event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Threading.DispatcherUnhandledExceptionEventArgs"/> instance containing the event data.</param>
        private void Application_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            //if (e.Exception.InnerException is InvalidOperationException)
            //{
            MessageBox.Show(e.Exception.ToString(), "Unhandled exception", MessageBoxButton.OK, MessageBoxImage.Error);
            //if ((e.Exception.InnerException is NullReferenceException) == false)
            if (!(e.Exception is NullReferenceException))
                e.Handled = true;
            //}
        }

        #endregion Functions
    }
}