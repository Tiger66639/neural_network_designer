﻿using Exocortex.DSP;
using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using JaStDev.HAB.Events;
using JaStDev.HAB.Sins;
using System;
using System.Diagnostics;

namespace JaStDev.HAB
{
    /// <summary>
    /// A Sensory interface for audio.
    /// </summary>
    /// <remarks>
    /// Audio input is handled by the <see cref="AudioSin.Process"/> function.
    /// </remarks>
    public class AudioSin : ProcessorDependentSin
    {
        #region Fields

        private bool fIsProcessing = false;                                                                     //when true, the brain is processing
        private ComplexF[] fData;                                                                               //current data being processed.

        #endregion Fields

        #region Events

        /// <summary>
        /// Raised when the <see cref="AudioSin"/> has finished processing a data
        /// block that was provided through <see cref="AudioSin.Process"/>.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Don't try to add data again before this event was raised.
        /// </para>
        /// <para>
        /// The <see cref="ComplexF"/> array that is returned contains the frequency
        /// set that can be released again.
        /// </para>
        /// </remarks>
        public event OutputEventHandler<ComplexF[]> IsReady;

        #endregion Events

        #region Overrides

        public override void Output(Neuron toSend)
        {
            if (toSend == this)
            {
                fIsProcessing = false;
                //if (IsReady != null)
                //   IsReady(this, new OutputEventArgs<ComplexF[]>() { Data = toSend, Value = fData });
                fData = null;                                                                                      //not really required, just clean.
            }
        }

        /// <summary>
        /// Called when the data needs to be saved.
        /// </summary>
        /// <remarks>
        /// nothing to do
        /// </remarks>
        public override void Flush()
        {
            //nothing to do.
        }

        #endregion Overrides

        /// <summary>
        /// Processes the specified data by transforming the list of audio samples into it's representive list
        /// of frequency values which are turned into neurons.
        /// </summary>
        /// <remarks>
        /// To get the frequency values, an FFt function is used.  See: http://www.codeproject.com/KB/recipes/howtofft.aspx
        /// for more info on how to use this.
        /// </remarks>
        /// <param name="data">The data.</param>
        public void Process(ComplexF[] data)
        {
            if (fIsProcessing == true)
                throw new InvalidOperationException("Previous audio sample has not yet been processed.");
            else
                fIsProcessing = true;
            fData = data;

            NeuronCluster iEntry = FindFirstOut((ulong)PredefinedNeurons.EntryPoints) as NeuronCluster;
            if (iEntry == null)                                                                       //if for some reason, the entrypoints have not yet been created, do it now.  Should not be required though.
                ReBuildStartPoints(data.Length);
            using (ChildrenAccessor iList = iEntry.Children)
            {
                for (int i = 0; i < data.Length; i++)
                {
                    ulong iId = iList[i];
                    DoubleNeuron iVal = (DoubleNeuron)Brain.Brain.Current[iId];
                    iVal.Value = data[i].Re;
                }
            }
            Debug.Assert(iEntry != null);
        }

        /// <summary>
        /// rebuild the starting points of this sin so that they have the specified lenght.
        /// </summary>
        /// <remarks>
        /// Only the required neurons are removed or created.
        /// </remarks>
        /// <param name="length">The new nr of sequence slots that this audio sin can handle.</param>
        private void ReBuildStartPoints(int length)
        {
            NeuronCluster iEntry = FindFirstOut((ulong)PredefinedNeurons.EntryPoints) as NeuronCluster;
            if (iEntry == null)
            {
                iEntry = new NeuronCluster();
                Brain.Brain.Current.Add(iEntry);
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.EntryPoints, iEntry);
            }

            using (ChildrenAccessor iList = iEntry.ChildrenW)
            {
                while (iList.Count > length)
                {
                    Brain.Brain.Current.Delete(iList[iList.Count - 1]);
                    iList.RemoveAt(iList.Count);
                }
                while (iList.Count < length)
                {
                    DoubleNeuron iNew = new DoubleNeuron();
                    Brain.Brain.Current.Add(iNew);
                    iList.Add(iNew);
                }
            }
            NeuronCluster iToCall = FindFirstOut((ulong)PredefinedNeurons.EntryPointsCreated) as NeuronCluster;
            if (iToCall != null)
            {
                Processor.Processor iProc = GetProcessor();
                iProc.Call(iToCall);
            }
        }
    }
}