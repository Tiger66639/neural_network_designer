﻿using JaStDev.HAB.Brain;
using JaStDev.HAB.Brain.Thread_syncing;
using JaStDev.HAB.Sins;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Xml;

namespace JaStDev.HAB
{
    /// <summary>
    /// A sensory interface able to communicate with the .net environment.
    /// </summary>
    /// <remarks>
    /// A Reflection sin can be in differnt modes, depending on the value of
    /// <see cref="ReflectionSin.State"/>. You can either call a function or generate code.
    /// <para>
    /// You can currently only call public static methods.
    /// If there are no arguments, you don't have to attach a parameter list.
    /// </para>
    /// <para>
    /// For generating IL code, the sin needs to know which neurons represent which opcode.
    /// Since the opcode list depends on the system that the network runs on, there are no
    /// static defined neurons for this.  Instead, a reflection sin is able to generate these
    /// neurons through <see cref="ReflectionSin.CreateOpcodes"/>.
    /// In the current implementation, IL code generates classes, fields, methods, opcode in the methods.
    /// The generated libraries are stored in a directory, defined by <see cref="Settings.LibraryStore"/>
    /// </para>
    /// <para>
    /// When the reflection sin is loaded into the brain, all the previously rendered libraries are also
    /// loaded into .net so they are available in the host app.
    /// </para>
    /// </remarks>
    [NeuronID((ulong)PredefinedNeurons.ReflectionSinState, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.ReflectionSinRender, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.ReflectionSinCall, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.NameOfMember, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.MemberType, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.BodyOfMember, typeof(Neuron))]
    [NeuronID((ulong)PredefinedNeurons.TypeOfMember, typeof(Neuron))]
    public class ReflectionSin : Sin
    {
        #region Fields

        private Dictionary<ulong, MethodInfo> fEntryPoints;
        private Dictionary<ulong, int> fOpcodes;
        private bool fEntryPointsChanged;                                                        //keeps track if the entrypoints list has changed, so we don't do to many saves.
        private bool fOpcodesChanged;                                                            //keeps track of changed opcodes.
        private List<string> fDynamicAssemblies = new List<string>();

        #endregion Fields

        #region prop

        #region State

        /// <summary>
        /// Gets/sets the the state that the reflection sin is in.
        /// </summary>
        /// <remarks>
        /// When set to <see cref="PredefinedNeurons.ReflectionSinRender"/>, any output will generate cill code.
        /// The textneuron should represent a complete class to generate, so you can store multiple functions
        /// in a single class/neuron.
        /// If it is set to <see cref="PredefinedNeurons.ReflectionSinCall"/>, the output neuron will be called.  Each
        /// neuron sent to the sin should have a link to a textneuron that identifies the function to call
        /// (meaning = <see cref="PredefinedNeurons.FunctionName"/>) + a link to a cluster containing all
        /// the argument values (meaning = (meaning = <see cref="PredefinedNeurons.Arguments"/>)
        /// <para>
        /// When empty, <see cref="PredefinedNeurons.Call"/> is presumed.
        /// </para>
        /// </remarks>
        public Neuron State
        {
            get
            {
                return FindFirstOut((ulong)PredefinedNeurons.ReflectionSinState);
            }
            set
            {
                SetFirstOutgoingLinkTo((ulong)PredefinedNeurons.ReflectionSinState, value);
            }
        }

        #endregion State

        #region Entrypoints

        /// <summary>
        /// Gets the entry points. They are loaded if needed.
        /// </summary>
        /// <value>The entry points.</value>
        public Dictionary<ulong, MethodInfo> EntryPoints
        {
            get
            {
                if (fEntryPoints == null && Brain.Brain.Current != null)
                {
                    fEntryPoints = new Dictionary<ulong, MethodInfo>();
                    List<ReflectionSinEntryPoint> iList = Brain.Brain.Current.Storage.GetProperty<List<ReflectionSinEntryPoint>>(typeof(ReflectionSin), ID.ToString() + Text + "Methods");
                    if (iList != null)
                    {
                        foreach (ReflectionSinEntryPoint i in iList)
                        {
                            Type iType = Type.GetType(i.TypeName);
                            if (iType != null)
                            {
                                Type[] iTypes = (from t in i.ParameterTypes
                                                 let tt = Type.GetType(t)
                                                 select tt).ToArray();
                                MethodInfo iInfo = iType.GetMethod(i.MethodName, iTypes);
                                if (iInfo != null)
                                    fEntryPoints.Add(i.ID, iInfo);
                                else
                                    Log.Log.LogError("ReflectionSin.EntryPoints", string.Format("Can't find method '{0}', in '{1}'.", i.MethodName, i.TypeName));
                            }
                            else
                                Log.Log.LogError("ReflectionSin.EntryPoints", string.Format("Can't find type '{0}'.", i.TypeName));
                        }
                    }
                }
                return fEntryPoints;
            }
        }

        #endregion Entrypoints

        #region OpCodes

        /// <summary>
        /// Gets the dictionary that maps the neurons to the opcode id that they represent.
        /// </summary>
        /// <value>The opcodes.</value>
        public Dictionary<ulong, int> Opcodes
        {
            get
            {
                if (fOpcodes == null && Brain.Brain.Current != null)
                {
                    List<ulong> iFound = Brain.Brain.Current.Storage.GetProperty<List<ulong>>(typeof(ReflectionSin), ID.ToString() + Text + "OpCodes");
                    for (int i = 0; i < iFound.Count; i++)
                        fOpcodes.Add(iFound[i], i + 1);
                }
                return fOpcodes;
            }
        }

        #endregion OpCodes

        #region DynamicAssemblies

        /// <summary>
        /// Gets the list of dynamic assembly names generated by this sin.
        /// </summary>
        public List<string> DynamicAssemblies
        {
            get { return fDynamicAssemblies; }
        }

        #endregion DynamicAssemblies

        #region AssemblyPath

        /// <summary>
        /// Gets the path where all the dynamic assemblies are saved to. The directory is created
        /// if it doesn't already exist.
        /// </summary>
        /// <remarks>
        /// This is a sub directory of the neurons data path.
        /// </remarks>
        /// <value>The assembly path.</value>
        public string AssemblyPath
        {
            get
            {
                string iPath = null;
                if (Brain.Brain.Current != null)
                {
                    iPath = Path.Combine(Brain.Brain.Current.Storage.DataPath, ID.ToString() + Text + "Assemblies");
                    if (Directory.Exists(iPath) == false)
                        Directory.CreateDirectory(iPath);
                }
                return iPath;
            }
        }

        #endregion AssemblyPath

        #endregion prop

        #region Overrides

        /// <summary>
        /// Tries to translate the specified neuron to the output type of the Sin and send it to the outside world.
        /// </summary>
        /// <param name="toSend">output</param>
        /// <remarks>
        /// 	<para>
        /// for processing, so <see cref="ReflectionSin.State"/>.
        /// </para>
        /// 	<para>
        /// This method is called by the <see cref="Brain"/> itself during/after processing of input.
        /// </para>
        /// <para>
        /// Neuron structure for rendering:
        /// new assembly -> text: name of assembly (m: NameOfMember)
        ///              -> cluster: types in assembly (m: MemberBody)
        ///                 foreach in cluster:
        ///                 new type -> text: name of type (m: NameOfMember)
        ///                          -> cluster: members in type (m: memberbody)
        ///                             foreach in cluster
        ///                             new ('method'        -> text: name of method (m: NameOfMember)
        ///                                                  -> cluster: opcode for method (m: bodyofmember)
        ///                                                  -> text: return type of method (m: TypeOfmember)
        ///                                 |'construction'  -> cluster: opcode for method (m: bodyofmember)
        ///                                 |'destructor     -> cluster: opcode for method (m: bodyofmember)
        ///                                 |'event          -> text: name of event (m: NameOfMember)
        ///                                                  -> text: type of event (m: TypeOfmember)
        ///                                 |'field'         -> text: name of event (m: NameOfMember)
        ///                                                  -> text: type of event (m: TypeOfmember)
        ///                                 |'GenericPar'    -> text: name of event (m: NameOfMember)
        ///                                 |'property'      -> text: name of event (m: NameOfMember)
        ///                                                  -> text: type of event (m: TypeOfmember)
        ///                                                  -> cluster: methods to generate (m: bodyOfmember, info 'get')
        ///                                                     foreach method in cluster
        ///                                                     new method -> text: name of method (m: NameOfMember) (should be 'get' or 'set')
        ///                                                                -> cluster: opcode for method (m: bodyofmember)
        ///                                                                -> text: return type of method (m: TypeOfmember)
        ///                                 ) -> 'if' determined by value found at link with meaning MemberType, content is a string.
        /// </para>
        /// </remarks>
        public override void Output(Neuron toSend)
        {
            Neuron iState = State;
            if (iState == null || iState.ID == (ulong)PredefinedNeurons.ReflectionSinCall)
                PerformCall(toSend);
            else if (iState.ID == (ulong)PredefinedNeurons.ReflectionSinRender)
                RenderIL(toSend);
            else
                Log.Log.LogError("ReflectionSin.Output", "The reflection sin is in an invalid state, don't know how to handle output!");
        }

        /// <summary>
        /// Called when the data needs to be saved.
        /// </summary>
        public override void Flush()
        {
            if (Brain.Brain.Current != null)
            {
                if (fEntryPointsChanged == true)
                {
                    List<ReflectionSinEntryPoint> iList = new List<ReflectionSinEntryPoint>();
                    foreach (KeyValuePair<ulong, MethodInfo> i in EntryPoints)
                    {
                        List<string> iParamTypeNames = (from p in i.Value.GetParameters()
                                                        select p.ParameterType.AssemblyQualifiedName).ToList();
                        ReflectionSinEntryPoint iNew = new ReflectionSinEntryPoint()
                        {
                            ID = i.Key,
                            MethodName = i.Value.Name,
                            ParameterTypes = iParamTypeNames,
                            TypeName = i.Value.ReflectedType.AssemblyQualifiedName,
                        };
                        iList.Add(iNew);
                    }
                    Brain.Brain.Current.Storage.SaveProperty(typeof(ReflectionSin), ID.ToString() + Text + "Methods", iList);
                }
                if (fOpcodesChanged == true)
                {
                    List<ulong> iOpcodes = (from i in fOpcodes
                                            orderby i.Value
                                            select i.Key).ToList();
                    Brain.Brain.Current.Storage.SaveProperty(typeof(ReflectionSin), ID.ToString() + Text + "OpCodes", iOpcodes);
                }
                Brain.Brain.Current.Storage.SaveProperty(typeof(ReflectionSin), ID.ToString() + Text + "Assemblies", fDynamicAssemblies);
            }
        }

        /// <summary>
        /// Reads the XML.
        /// </summary>
        /// <remarks>
        /// We override this function cause whenever this neuron is loaded, we want to make
        /// certain that all the dll's are also loaded into the system.
        /// </remarks>
        /// <param name="reader">The reader.</param>
        public override void ReadXml(XmlReader reader)
        {
            base.ReadXml(reader);
            fDynamicAssemblies = Brain.Brain.Current.Storage.GetProperty<List<string>>(typeof(ReflectionSin), ID.ToString() + Text + "Assemblies");
            if (fDynamicAssemblies != null)
            {
                foreach (string i in fDynamicAssemblies)
                {
                    Log.Log.LogInfo("ReflectionSin.ReadXml", "Still need to load dynmacilly generated assemblies.");
                    //Assembly iAssm = Assembly.
                }
            }
            else
            {
                Log.Log.LogError("ReflectionSin.ReadXml", string.Format("Failed to find dynamically generated assemblies list in properties of: {0}.", this));
                fDynamicAssemblies = new List<string>();                                                        //need to recreate the list, cause it was destroyed while loading something invalid.
            }
        }

        #endregion Overrides

        #region Functions

        /// <summary>
        /// Loads the specified method in the dictionary, when possible.
        /// </summary>
        /// <param name="toload">The method info toload.</param>
        public Neuron LoadMethod(MethodInfo toLoad)
        {
            if (toLoad.IsStatic == true)
            {
                if (toLoad.IsPublic == true)
                {
                    Neuron iNew = new Neuron();
                    Brain.Brain.Current.Add(iNew);
                    EntryPoints.Add(iNew.ID, toLoad);
                    fEntryPointsChanged = true;
                    return iNew;
                }
                else
                    Log.Log.LogError("ReflectionSin.Load", "Methods must be public!");
            }
            else
                Log.Log.LogError("ReflectionSin.Load", "Methods must be static!");
            return null;
        }

        /// <summary>
        /// Removes the function map to the specified neuron from the dictionary.
        /// </summary>
        /// <param name="toUnload">The function to unload.</param>
        public void UnloadMethod(Neuron toUnload)
        {
            EntryPoints.Remove(toUnload.ID);
            fEntryPointsChanged = true;
        }

        /// <summary>
        /// Gets the ID for the specified method. If the method is not mapped, the <see cref="Neuron.EmptyID"/> is returned.
        /// </summary>
        /// <param name="value">The ID of the neuron used to identify the method or <see cref="Neuron.EmptyID"/> if invalid.</param>
        public ulong GetMethodId(MethodInfo value)
        {
            foreach (KeyValuePair<ulong, MethodInfo> i in EntryPoints)
            {
                if (i.Value == value)
                    return i.Key;
            }
            return Neuron.EmptyId;
        }

        /// <summary>
        /// Creates all the neurons that represent the opcodes that can be generated when in cill mode.
        /// </summary>
        /// <remarks>
        /// <para>
        /// This implementation loads all the .net cill opcodes (including exceptionblocks/load variables,...).
        /// All previous opcodes are removed but no previous neurons are deleted.
        /// </para>
        /// </remarks>
        /// <returns>The list of objects that wrap the neurons that represent recognisable values to the system.</returns>
        public IEnumerable<NeuronCluster> CreateOpcodes()
        {
            fOpcodesChanged = true;
            if (fOpcodes != null)
                fOpcodes.Clear();
            else
                fOpcodes = new Dictionary<ulong, int>();
            List<NeuronCluster> iRes = new List<NeuronCluster>();
            CreateOpcode("BeginExceptionBlock", iRes);
            CreateOpcode("BeginExceptionFilterBlock", iRes);
            CreateOpcode("BeginCatchBlock", iRes);
            CreateOpcode("BeginFaultBlock", iRes);
            CreateOpcode("BeginFinallyBlock", iRes);
            CreateOpcode("BeginScope", iRes);
            CreateOpcode("DeclareLocal", iRes);
            CreateOpcode("DefineLabel", iRes);

            Type iOpcodeType = typeof(OpCodes);
            foreach (FieldInfo i in iOpcodeType.GetFields(BindingFlags.Static | BindingFlags.Public))
                CreateOpcode(i.Name, iRes);
            return iRes;
        }

        /// <summary>
        /// Creates an object for the opcode and adds it to the list.
        /// </summary>
        /// <param name="value">The value for which to create a new object and opcode for.</param>
        /// <param name="list">The list to add the new object to.</param>
        private void CreateOpcode(string value, List<NeuronCluster> list)
        {
            Neuron iCreated;
            NeuronCluster iRes = BrainHelper.CreateObject(value, out iCreated);
            fOpcodes.Add(iCreated.ID, list.Count);
            list.Add(iRes);
        }

        #endregion Functions

        #region Helpers

        #region Render

        /// <summary>
        /// Creates or loads a dynamic library and renders all the types and the code in the types as defined
        /// in the output neuron.
        /// </summary>
        /// <param name="toSend">The root neuron that contains all the info to generate a single class.</param>
        private void RenderIL(Neuron toSend)
        {
            ModuleBuilder iModule = GetModule(toSend);
        }

        /// <summary>
        /// Extracts the name from the neuron (through Out: NameOfMember) and creates an assembly and module with that name.
        /// </summary>
        /// <param name="toSend">To send.</param>
        /// <returns>a module or null when the operation failed.</returns>
        private ModuleBuilder GetModule(Neuron toSend)
        {
            TextNeuron iName = toSend.FindFirstOut((ulong)PredefinedNeurons.NameOfMember) as TextNeuron;
            if (iName != null)
            {
                AssemblyName iAssemName = new AssemblyName(iName.Text);
                AssemblyBuilder iAssemBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(iAssemName, AssemblyBuilderAccess.RunAndSave);
                string iPath = iName.Text;
                iPath = Path.Combine(AssemblyPath, iPath + ".dll");
                ModuleBuilder iRes = iAssemBuilder.DefineDynamicModule(iName.Text, iPath);
                DynamicAssemblies.Add(iPath);
                return iRes;
            }
            return null;
        }

        #endregion Render

        #region Call

        /// <summary>
        /// Calls the specified function on the specified class.  This must be a static member.
        /// </summary>
        /// <param name="toSend">To send.</param>
        private void PerformCall(Neuron toSend)
        {
            if (toSend != null)
            {
                Neuron iToCall = toSend.FindFirstClusteredBy((ulong)PredefinedNeurons.NameOfMember);
                if (iToCall != null)
                {
                    MethodInfo iFound;
                    if (EntryPoints.TryGetValue(toSend.ID, out iFound) == true)
                    {
                        List<object> iPar = GetParameters(toSend);
                        if (iPar != null)
                            iFound.Invoke(null, iPar.ToArray());
                    }
                    else
                        Log.Log.LogError("ReflectionSin.PerformCall", string.Format("There is no function attached to neuron {0}!", iToCall));
                }
                else
                    Log.Log.LogError("ReflectionSin.PerformCall", "No neuron defined that identifies the function to call!");
            }
            else
                Log.Log.LogError("ReflectionSin.PerformCall", "null send to reflection sin!");
        }

        /// <summary>
        /// Extracts the parameters arguments from the neuron and returns the values as a list of objects.
        /// </summary>
        /// <param name="toSend">To send.</param>
        /// <returns>null if an invalid value was found, otherwise a list of parameter values (in .net value types, not neurons)</returns>
        private List<object> GetParameters(Neuron toSend)
        {
            NeuronCluster iCluster = toSend.FindFirstClusteredBy((ulong)PredefinedNeurons.Arguments) as NeuronCluster;
            List<object> iPar = new List<object>();
            if (iCluster != null)
            {
                using (ChildrenAccessor iList = iCluster.Children)
                {
                    foreach (ulong i in iList)
                    {
                        Neuron iNeuron = Brain.Brain.Current[i];
                        if (iNeuron is TextNeuron)
                            iPar.Add(((TextNeuron)iNeuron).Text);
                        else if (iNeuron is IntNeuron)
                            iPar.Add(((IntNeuron)iNeuron).Value);
                        else if (iNeuron is DoubleNeuron)
                            iPar.Add(((DoubleNeuron)iNeuron).Value);
                        else
                        {
                            Log.Log.LogError("ReflectionSin.GetParameters", string.Format("Invalid neuron type found as parameter value: {0}!", iNeuron.GetType().AssemblyQualifiedName));
                            return null;
                        }
                    }
                }
            }
            return iPar;
        }

        #endregion Call

        #endregion Helpers
    }
}