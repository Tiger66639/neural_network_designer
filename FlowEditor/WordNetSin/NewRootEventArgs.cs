﻿using JaStDev.HAB.Brain;
using System;

namespace JaStDev.HAB
{
    /// <summary>
    /// Event arguments for the NewRoot event.
    /// </summary>
    public class NewRootEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the neuron that represents the relationship for which there was a new root item.
        /// </summary>
        /// <value>The relationship.</value>
        public Neuron Relationship { get; set; }

        /// <summary>
        /// Gets or sets the actual root item.
        /// </summary>
        /// <value>The item.</value>
        public NeuronCluster Item { get; set; }
    }

    /// <summary>
    /// delegate used for NewRoot event of the WordNetSin.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void NewRootHandler(object sender, NewRootEventArgs e);
}