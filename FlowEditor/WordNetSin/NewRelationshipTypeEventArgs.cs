﻿namespace JaStDev.HAB
{
    /// <summary>
    /// Event hanlder for <see cref="WordNetSin.RelationshipTypeCreated"/>
    /// </summary>
    public delegate void NewRelationshipTypeEventHandler(object sender, NewRelationshipTypeEventArgs e);

    /// <summary>
    /// Event arguments for the <see cref="WordNet.RelationshipTypeCreated"/> event.
    /// </summary>
    public class NewRelationshipTypeEventArgs : WordNetEventArgs
    {
        /// <summary>
        /// Gets or sets the relationship as a string value for which a new neuron was created.
        /// </summary>
        public string Relationship { get; set; }
    }
}