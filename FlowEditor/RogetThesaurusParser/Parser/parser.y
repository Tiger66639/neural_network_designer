%namespace JastDev.RogetThesaurus.Parser
%valuetype LexValue
%partial



%union {
    public string str;
}

%token WORD NUMBER NUMBERA                            /* NUMBERA is a number followed by 1 lettter, ususally a ex: 16a */
%token BLOCKTOK ENDBLOCKTOK                                     /* needs to keep a state are we in block: check for keywords, otherwise dont */

%token KWCLASS KWSECTION KWDIVISION
%token KWNOUN KWADJ KWVERB KWADV KWPHRASE /* push scanner: take /n as end + return /n so that we can check for the end of the title. */


%token NEWLINE
%token KWEND
%token REF                                            /*&c.*/


%token maxParseToken 
%token LEX_WHITE LEX_COMMENT LEX_ERROR


%%

Thesaurus : Blocks KWEND;

Blocks   : Block Blocks
         | Block 
         ;
    

Block  :  BLOCKTOK BlockContent ENDBLOCKTOK BlockItems 
       |  BLOCKTOK BlockContent ENDBLOCKTOK
       |  BLOCKTOK ENDBLOCKTOK 
       ;
    
BlockContent : BlockContent_
             | NEWLINE BlockContent_
             ;

BlockContent_  : Class Section Title SubTitle
               | Class Section Title
               | Class Section
               | Class Division Section
               | Class Division Section SubTitle
               | Section Title SubTitle
               | Section Title
               | Section
               | Division Section
               | Title SubTitle
               | Title
               | SubTitle
               ;
               
Class : KWCLASS WORD NEWLINE Words NEWLINE            { AddClass(@0, @4, @2); }                       /* first word is for the number of the class*/
      ; 
      
Division : KWDIVISION '(' WORD ')' Words NEWLINE      { AddDivision(@0, @3, @5); }
         | KWDIVISION '(' WORD ')' Words error        { AddDivision(@0, @3, @5); ReportError("Expected newline ", @6); }
         ;

Section : KWSECTION WORD '.' Words NEWLINE            { AddSection(@0, @4, @2); }                     /* this is the number of the section */
        | KWSECTION WORD '.' Words error              { ReportError("Expected newline ", @5); }
        ;

Title : TitleStart NEWLINE                            /*we use a sentence here cause it can contain words and numbers*/
      | TitleStart error                              { ReportError("newline expected ", @2); }
      ;
      
TitleStart : NUMBER '.' Sentence                      { AddTitle(@0, @3, @1); }                       /*we use a sentence here cause it can contain words and numbers*/
           | NUMBER '.' Sentence ',' Sentence         { AddSubTitle(@0, @3, @1, @5); }  /* the second is setence is the subtitle*/
           ;

SubTitle : Sentence NEWLINE                           { AddSubTitle(@0, @1); }
         ;                         

Words : WORD Words
      | WORD
      ;
      
Sentence : SentenceItem Sentence      
         | SentenceItem
         ;
         
SentenceItem : WORD
             | WORD '-' SentenceItem
             | NUMBER
             | NUMBERA
             ;         
             

BlockItems : BlockItem BlockItems
           | BlockItem
           | error                                    { ReportError("Expected block items ", @1); }
           ;


BlockItem : BlockItemStart BlockItemID BlockItemContent ;  

BlockItemStart : '#'                                  { AddBlockItem(); }
               ;
             
BlockItemID : NUMBER BlockItemIDSubName '-' '-'   { SetBlockItemNumber(@1); }
            | NUMBERA BlockItemIDSubName '-' '-'  { SetBlockItemNumber(@1); }
            ;


BlockItemIDSubName : '.' WORD '.'                                          { SetBlockItemTitle(@2); }
                   | '.' WORD '.' '[' PunctuatedSentence ']'               { SetTitleAndMeaning(@2, @4); }
                   | '.' '[' PunctuatedSentence ']' WORD '.'               { SetTitleAndMeaning(@5, @3); }
                   | '.' '[' PunctuatedSentence REF ']' WORD '.'           { SetTitleAndMeaning(@6, @3); } /*some weird construct somewhere, simply skip the REF. */
                   | '.' '[' PunctuatedSentence error WORD '.'             { ReportError("Expected ']'", @4); }
                   ; 
               

PunctuatedSentence : PunctuatedSentenceContent EndPunctuation
                   | PunctuatedSentenceContent EndPunctuation BracketItems
                   | PunctuatedSentenceContent BracketItems EndPunctuation 
                   ;
BracketItems : '[' Sentence ']' BracketItems
             | '[' Sentence ']'             
             ;     

PunctuatedSentenceContent : SentenceItem PunctuatedSentenceContent
                          | SentenceItem
                          | SentenceItem Punctuation PunctuatedSentenceContent
                          ;
                          
Punctuation : ';'
            | ',' 
            | '-' 
            | '"' 
            ;

EndPunctuation : '?' 
               | '.' 
               | '!' 
               | ';'
               ;
                   
BlockItemContent : BlockItemContentItem BlockItemContent                   
                 | BlockItemContentItem                                    
                 ; 

BlockItemContentItem : BlockItemContentItem_                               { AddValueMeaning(); }
                     ;
                 
BlockItemContentItem_ : KWNOUN WordLists                                   { SetValueMeaningType(ValueType.Noun); }       /* N. */
                     | KWADJ WordLists                                     { SetValueMeaningType(ValueType.Adjective); }  /* Adj. */
                     | KWVERB WordLists                                    { SetValueMeaningType(ValueType.Verb); }       /* V. */
                     | KWADV WordLists                                     { SetValueMeaningType(ValueType.Adverb); }     /* Adv. */
                     | KWPHRASE  Phrases                                                                                  /* Phr. */
                     ;

Phrases : PunctuatedSentence Phrases                                       { AddPhrase(@1); }
        | PunctuatedSentence                                               { AddPhrase(@1); }
        ;

WordLists : WordList '.' WordLists 
          | WordList '.'
          ;             

WordList : WordList_                                                       { AddValueMeaningGroup(); }                                       
         ;

WordList_ : WordListPart ';' WordList_ 
         | WordListPart
         ; 
         
WordListPart : WordListPart_                                               { AddValueMeaningSubGroup(); }      
             ;
         
WordListPart_ : WordListPartItem ',' WordListPart_
             | WordListPartItem
             ;
             
             
WordListPartItem : Sentence                                                { AddValueItem(@1); }
                 | Sentence '[' Sentence ']'                               { AddValueItem(@1);SetExtraInforForLastItem(@3); } 
                 | Sentence RefItem                                        { ChangeLastRefToItem(@1); } 
                 | Sentence '[' Sentence ']' RefItem                       { ChangeLastRefToItem(@1); SetExtraInforForLastItem(@3); } 
                 | RefItem
                 | '[' Sentence ']'                                        /*this is some weird construct I dont understand and skip*/
                 | '[' Sentence ']' Sentence                               { AddValueItem(@4); { SetExtraInforForLastItem(@2); } } 
                 | '[' Sentence ']' Sentence RefItem                       { ChangeLastRefToItem(@4); SetExtraInforForLastItem(@2); } 
                 ;             
RefItem : RefItemFront
        | RefItemFront '[' Sentence ']'                                    { SetExtraInforForLastItem(@3); }
        ;

RefItemFront : REF
        | REF '(' Sentence ')' NUMBER                                      { AddValueItemRef(@5, @3); } 
        | '(' Sentence ')' NUMBER                                          { AddValueItemRef(@4, @2); } 
        | REF '(' Sentence ')' NUMBERA                                     { AddValueItemRef(@5, @3); } 
        | REF '(' Sentence ')'                                             { AddValueItemRef(null, @3); } 
        | REF NUMBER                                                       { AddValueItemRef(@2); } 
        | REF NUMBERA                                                      { AddValueItemRef(@2); }
        | REF KWADJ                                                        { AddValueItemRef(ValueType.Adjective); }
        | REF KWADV                                                        { AddValueItemRef(ValueType.Adverb); }
        | REF KWVERB                                                       { AddValueItemRef(ValueType.Verb); }
        | REF KWNOUN                                                       { AddValueItemRef(ValueType.Noun); }
        
        | REF NUMBER KWADJ                                                 { AddValueItemRef(@2, ValueType.Adjective); } 
        | REF NUMBER KWADV                                                 { AddValueItemRef(@2, ValueType.Adverb); }
        | REF NUMBER KWVERB                                                { AddValueItemRef(@2, ValueType.Verb); }
        | REF NUMBER KWNOUN                                                { AddValueItemRef(@2, ValueType.Noun); }
        
        | REF NUMBERA KWADJ                                                { AddValueItemRef(@2, ValueType.Adjective); }
        | REF NUMBERA KWADV                                                { AddValueItemRef(@2, ValueType.Adverb); }
        | REF NUMBERA KWVERB                                               { AddValueItemRef(@2, ValueType.Verb); }
        | REF NUMBERA KWNOUN                                               { AddValueItemRef(@2, ValueType.Noun); }
        
        ;             
                            

%%