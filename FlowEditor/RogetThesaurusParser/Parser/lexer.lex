%using JastDev.RogetThesaurus.Parser;

%namespace JastDev.RogetThesaurus.Lexer

%x BLOCK
%x COMMENT

%{     
       int GetIdToken(string txt)
         {
         //Console.Error.Write("\nlooking up keyword:" + txt + "-");
            switch (txt[0])
            {
                case 'C':
                    if (txt.Equals("CLASS")== true){
                        //Console.Error.Write("\nfound keyword CLASS");
                       return (int)Tokens.KWCLASS;
                    }
                    else{
                        //Console.Error.Write("\ndid not found keyword CLASS");
                        return (int)Tokens.WORD;
                    }
                    break;
                case 'S':
                    if (txt.Equals("SECTION")) return (int)Tokens.KWSECTION;
                    break;
                case 'D':
                    if (txt.Equals("DIVISION")) return (int)Tokens.KWDIVISION;
                    break;
                default: 
                    break;
            }
            return (int)Tokens.WORD;
       }


       internal void LoadYylval()
       {
           yylval.str = tokTxt;
           yylloc = new LexLocation(tokLin, tokCol, tokLin, tokCol + tokLen);
           //Console.Error.Write("\ncalled LoadYylval: " + tokTxt);
       }
       
       public override void yyerror(string s, params object[] a)
       {
           if (handler != null) handler.AddError(s, tokLin, tokCol, tokLin, tokCol + tokLen);
       }
%}



CmntStart    \<\-\-
CmntEnd      \-\-\>
ABMin        [^\-\n]*

KwNoun      N\.
KwAdj       Adj\.
KwVerb      V\.
KwAdv       Adv\.
KwPhr       Phr\.

KwSNoun      n\.
KwSAdj       adj\.
KwSVerb      v\.
KwSAdv       adv\.
KwSPhr       phr\.

KwRef       \&c\.

White0          [ \t\f\v]
NewLine         \n|\r
White           {White0}|{NewLine}

KwEnd       \-\-end\-\-

%%

[a-zA-Z_][a-zA-Z0-9_]*    { return (int)Tokens.WORD; }
[0-9]+                    { return (int)Tokens.NUMBER; }
[0-9]+[a-zA-Z]+           { return (int)Tokens.NUMBERA; }


{KwEnd}                          { return (int)Tokens.KWEND;  }
;                                { return (int)';';    }
,                                { return (int)',';    }
\(                               { return (int)'(';    }
\)                               { return (int)')';    }
\[                               { return (int)'[';    }
\]                               { return (int)']';    }
\{                               { return (int)'{';    }
\}                               { return (int)'}';    }
=                                { return (int)'=';    }
\^                               { return (int)'^';    }
\+                               { return (int)'+';    }
\-                               { return (int)'-';    }
\*                               { return (int)'*';    }
\/                               { return (int)'/';    }
\!                               { return (int)'!';    }
\&                               { return (int)'&';    }
\|                               { return (int)'|';    }
\.                               { return (int)'.';    }
\#                               { return (int)'#';    }
\%                               { BEGIN(BLOCK);  Console.Error.Write("\nstart BLOCK "); return (int)Tokens.BLOCKTOK;    }
{KwNoun}                         { return (int)Tokens.KWNOUN; }
{KwAdj}                          { return (int)Tokens.KWADJ; }
{KwVerb}                         { return (int)Tokens.KWVERB; }
{KwAdv}                          { return (int)Tokens.KWADV; }
{KwPhr}                          { return (int)Tokens.KWPHRASE; }

{KwSNoun}                         { return (int)Tokens.KWNOUN; }
{KwSAdj}                          { return (int)Tokens.KWADJ; }
{KwSVerb}                         { return (int)Tokens.KWVERB; }
{KwSAdv}                          { return (int)Tokens.KWADV; }
{KwSPhr}                          { return (int)Tokens.KWPHRASE; }

{KwRef}                          { return (int)Tokens.REF;  }
{White0}+                        { return (int)Tokens.LEX_WHITE; }
<BLOCK>[a-zA-Z_][a-zA-Z0-9_]*    { return GetIdToken(yytext); }
<BLOCK>[0-9]+                    { return (int)Tokens.NUMBER; }
<BLOCK>,                         { return (int)',';    }
<BLOCK>\.                        { return (int)'.';    }
<BLOCK>\(                        { return (int)'(';    }
<BLOCK>\)                        { return (int)')';    }
<BLOCK>{NewLine}+{White}+        { Console.Error.Write("\nfound NL "); return (int)Tokens.NEWLINE; }
<BLOCK>{White0}+                 { return (int)Tokens.LEX_WHITE; }
<BLOCK>\%                        { BEGIN(INITIAL); Console.Error.Write("\nend BLOCK "); return (int)Tokens.ENDBLOCKTOK;    }

{CmntStart}{ABMin}\-*{CmntEnd} { return (int)Tokens.LEX_COMMENT; } 

%{
                      LoadYylval();
%}

%%

/* .... */
